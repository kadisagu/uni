package ru.tandemservice.narfu.component.reports.JournalAcademVacations;

import com.ibm.icu.util.Calendar;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.RelTypeGrantView;
import ru.tandemservice.movestudentrmc.entity.RepresentWeekend;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.GrantTypeCodes;
import ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.*;

public class ReportGenerator {
    public static final String TEMPLATE = "narfuJournalAcademVacations";
    private IUniBaseDao dao;

    private HashMap<Student, DocOrdRepresent> studentMap;
    private OrgUnit orgUnit;
    private String title;
    private int number;

    protected WritableWorkbook workbook;
    protected WritableSheet sheet;
    protected ByteArrayOutputStream workbookStream;

    public static Long createReport(OrgUnit orgUnit, String title, HashMap<Student, DocOrdRepresent> studentMap) {
        try {
            ReportGenerator generator = new ReportGenerator(orgUnit, title, studentMap);
            NarfuReportJournalAcademVacations report = generator.generateReport();

            UniDaoFacade.getCoreDao().saveOrUpdate(report.getContent());
            UniDaoFacade.getCoreDao().saveOrUpdate(report);

            return report.getId();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new ApplicationException(ex.getMessage(), ex);
        }
    }

    protected ReportGenerator(OrgUnit orgUnit, String title, HashMap<Student, DocOrdRepresent> studentMap) {
        this.dao = UniDaoFacade.getCoreDao();
        this.orgUnit = orgUnit;
        this.title = title;
        this.studentMap = studentMap;
    }

    protected NarfuReportJournalAcademVacations generateReport() throws Exception {
        NarfuReportJournalAcademVacations report = new NarfuReportJournalAcademVacations();
        //берем последний номер отчета
        MQBuilder builder = new MQBuilder(NarfuReportJournalAcademVacations.ENTITY_CLASS, "report")
                .addOrder("report", NarfuReportJournalAcademVacations.number(), OrderDirection.desc);
        if (!dao.getList(builder).isEmpty())
            number = ((NarfuReportJournalAcademVacations) dao.getList(builder).get(0)).getNumber() + 1;
        else
            number = 1;
        report.setFormingDate(new Date());
        report.setOrgUnit(orgUnit);
        report.setNumber(number);
        //формируем имя отчета
        StringBuilder name = new StringBuilder()
                .append("Отчет №")
                .append(String.valueOf(number))
                .append(" от ")
                .append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(report.getFormingDate()))
                .append(" по ")
                .append(title);
        report.setTitle(name.toString());

        //fill excel
        loadTemplate();
        fillReport();
        this.workbook.write();
        this.workbook.close();

        //put excel
        DatabaseFile reportFile = new DatabaseFile();
        reportFile.setContent(this.workbookStream.toByteArray());
        reportFile.setFilename("Начисление стипендии.xls");
        reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        report.setContent(reportFile);

        return report;
    }

    protected void fillReport() throws Exception {

        // шрифты
        WritableFont times12 = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
        WritableFont times10 = new WritableFont(WritableFont.TIMES, 10);

        // формат ячеек
        WritableCellFormat rowFormat = new WritableCellFormat(times10);
        rowFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        rowFormat.setAlignment(Alignment.LEFT);
        rowFormat.setWrap(true);

        WritableCellFormat rowFirstColFormat = new WritableCellFormat(times12);
        rowFirstColFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFirstColFormat.setAlignment(Alignment.CENTRE);
        rowFirstColFormat.setWrap(true);

        int rows = 9;
        int number = 0;

        sheet.addCell(new Label(0, 3, TopOrgUnit.getInstance().getShortTitle(), rowFirstColFormat));
        List<Student> studentList = new ArrayList<Student>();
        for (Student student : studentMap.keySet()) {
            studentList.add(student);
        }
        //сортируем студентов
        Collections.sort(studentList,
                         new EntityComparator<Student>(new EntityOrder(Student.person().identityCard().fullFio().s())));
        for (Student student : studentList) {

            DocOrdRepresent orderRepresent = studentMap.get(student);
            sheet.addCell(new Label(0, rows, String.valueOf(++number), rowFormat));
            sheet.addCell(new Label(1, rows, student.getPerson().getIdentityCard().getFullFio(), rowFormat));
            sheet.addCell(new Label(2, rows, student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle(), rowFormat));
            sheet.addCell(new Label(3, rows, student.getEducationOrgUnit().getDevelopForm().getTitle(), rowFormat));
            sheet.addCell(new Label(4, rows, student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOksoPrefix(), rowFormat));
            sheet.addCell(new Label(5, rows, student.getCourse().getTitle(), new WritableCellFormat(rowFormat)));
            sheet.addCell(new Label(6, rows, orderRepresent != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(orderRepresent.getOrder().getCreateDate()) : "", rowFormat));
            sheet.addCell(new Label(7, rows, orderRepresent != null ? orderRepresent.getOrder().getNumber() : "", rowFormat));
            sheet.addCell(new Label(8, rows, orderRepresent != null ? orderRepresent.getRepresentation().getReason().getTitle() : "", rowFormat));
            sheet.addCell(new Label(9, rows, orderRepresent != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(((RepresentWeekend) orderRepresent.getRepresentation()).getStartDate()) : "", rowFormat));
            sheet.addCell(new Label(10, rows, orderRepresent != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(((RepresentWeekend) orderRepresent.getRepresentation()).getEndDate()) : "", rowFormat));


            if (orderRepresent != null && orderRepresent.getRepresentation().isSaveGrants()) {
                //выбираем студентов, у которых стипендия до выхода в АО была "Индивидуальные академические" и "Академические и приравненные"
                DQLSelectBuilder grantBuilder = new DQLSelectBuilder()
                        .fromEntity(StudentGrantEntity.class, "sge")
                        .addColumn("sge")
                        .joinEntity("sge", DQLJoinType.left, RelTypeGrantView.class, "rtgv", DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.view().fromAlias("sge")), DQLExpressions.property(RelTypeGrantView.view().fromAlias("rtgv"))))
                        .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.student().fromAlias("sge")), DQLExpressions.value(student)))
                        .where(DQLExpressions.eq(DQLExpressions.property(StudentGrantEntity.month().fromAlias("sge")), DQLExpressions.value(getGrantMonth(((RepresentWeekend) orderRepresent.getRepresentation()).getEndDate()))))
                        .where(DQLExpressions.in(DQLExpressions.property(RelTypeGrantView.type().fromAlias("rtgv")), new String[]{GrantTypeCodes.CODE_1, GrantTypeCodes.CODE_3}));

                List<StudentGrantEntity> granEntityList = dao.getList(grantBuilder);
                if (!granEntityList.isEmpty()) {
                    StudentGrantEntity granEntity = granEntityList.get(0);
                    sheet.addCell(new Label(11, rows, granEntity != null ? granEntity.getView().getShortTitle() : "", rowFormat));
                }
            }
            else
                sheet.addCell(new Label(11, rows, "", rowFormat));

            //проверяем социальный статус студента
            MQBuilder benefitBuilder = new MQBuilder(PersonBenefit.ENTITY_CLASS, "pb")
                    .add(MQExpression.eq("pb", PersonBenefit.benefit().code(), "5")
                                 .and(MQExpression.eq("pb", PersonBenefit.person(), student.getPerson())));
            List<PersonBenefit> benefitList = dao.getList(benefitBuilder);
            sheet.addCell(new Label(12, rows, !benefitList.isEmpty() ? "Сирота" : "", rowFormat));

            rows++;
        }
    }

    protected String getGrantMonth(Date date) {
        //формируем название месяца стипендии в формате "номер_месяца.учебный год"
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, -1);
        MonthWrapper.Data data = MonthWrapper.getInstance(c.getTime());
        return data.monthWrapper.getStringValue(data.educationYear);
    }

    protected void loadTemplate() throws Exception {
        //загружаем и настраиваем шаблон
        TemplateDocument template = dao.getCatalogItem(TemplateDocument.class, TEMPLATE);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        this.workbookStream = new ByteArrayOutputStream();
        this.workbook = Workbook.createWorkbook(this.workbookStream, tempalteWorkbook, ws);
        this.sheet = this.workbook.getSheet(0);
    }


}
