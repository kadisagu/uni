package ru.tandemservice.narfu.component.orgUnit.GrandTab;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.component.orgUnit.GrantTab.MonthWrapper;
import ru.tandemservice.movestudentrmc.entity.catalog.GrantView;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;

/**
 * User: ramaslov
 * Date: 21.03.13
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
public class Model {

    private ISelectModel grantViewModel;
    private List<GrantView> grantViewList = new ArrayList<>();
    private List<GrantView> oldGrantViewList = new ArrayList<>();
    private Boolean checkAllGrantView = Boolean.TRUE;

    private ISelectModel orgUnitModel;
    private List<OrgUnit> orgUnitList = new ArrayList<>();
    private List<OrgUnit> oldOrgUnitList = new ArrayList<>();
    private Boolean checkAllOrgUnit = Boolean.TRUE;

    private List<EducationYear> eduYearList;
    private EducationYear eduYear;

    private List<MonthWrapper> monthBeginModel;
    private MonthWrapper monthBegin;

    private List<MonthWrapper> monthEndModel;
    private MonthWrapper monthEnd;

    private double sum = 0;

    public ISelectModel getGrantViewModel() {
        return grantViewModel;
    }

    public void setGrantViewModel(ISelectModel grantViewModel) {
        this.grantViewModel = grantViewModel;
    }

    public List<GrantView> getGrantViewList() {
        return grantViewList;
    }

    public void setGrantViewList(List<GrantView> grantViewList) {
        this.grantViewList = grantViewList;
    }

    public List<GrantView> getOldGrantViewList() {
        return oldGrantViewList;
    }

    public void setOldGrantViewList(List<GrantView> oldGrantViewList) {
        this.oldGrantViewList = oldGrantViewList;
    }

    public Boolean getCheckAllGrantView() {
        return checkAllGrantView;
    }

    public void setCheckAllGrantView(Boolean checkAllGrantView) {
        this.checkAllGrantView = checkAllGrantView;
    }

    public ISelectModel getOrgUnitModel() {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel) {
        this.orgUnitModel = orgUnitModel;
    }

    public List<OrgUnit> getOrgUnitList() {
        return orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList) {
        this.orgUnitList = orgUnitList;
    }

    public List<OrgUnit> getOldOrgUnitList() {
        return oldOrgUnitList;
    }

    public void setOldOrgUnitList(List<OrgUnit> oldOrgUnitList) {
        this.oldOrgUnitList = oldOrgUnitList;
    }

    public Boolean getCheckAllOrgUnit() {
        return checkAllOrgUnit;
    }

    public void setCheckAllOrgUnit(Boolean checkAllOrgUnit) {
        this.checkAllOrgUnit = checkAllOrgUnit;
    }

    public List<EducationYear> getEduYearList() {
        return eduYearList;
    }

    public void setEduYearList(List<EducationYear> eduYearList) {
        this.eduYearList = eduYearList;
    }

    public EducationYear getEduYear() {
        return eduYear;
    }

    public void setEduYear(EducationYear eduYear) {
        this.eduYear = eduYear;
    }

    public List<MonthWrapper> getMonthBeginModel() {
        return monthBeginModel;
    }

    public void setMonthBeginModel(List<MonthWrapper> monthBeginModel) {
        this.monthBeginModel = monthBeginModel;
    }

    public MonthWrapper getMonthBegin() {
        return monthBegin;
    }

    public void setMonthBegin(MonthWrapper monthBegin) {
        this.monthBegin = monthBegin;
    }

    public List<MonthWrapper> getMonthEndModel() {
        return monthEndModel;
    }

    public void setMonthEndModel(List<MonthWrapper> monthEndModel) {
        this.monthEndModel = monthEndModel;
    }

    public MonthWrapper getMonthEnd() {
        return monthEnd;
    }

    public void setMonthEnd(MonthWrapper monthEnd) {
        this.monthEnd = monthEnd;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }
}
