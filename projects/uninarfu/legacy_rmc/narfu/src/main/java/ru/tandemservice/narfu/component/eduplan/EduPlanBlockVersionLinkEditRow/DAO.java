package ru.tandemservice.narfu.component.eduplan.EduPlanBlockVersionLinkEditRow;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.narfu.entity.EppEduPlanVersionBlockLink;
import ru.tandemservice.narfu.entity.VersionBlockDisciplinesLink;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.ui.EduPlanVersionBlockDataSourceGenerator;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(final Model model) {

        final EppRegistryElement regElement = model.getElement().refresh(EppRegistryElement.class);
        final EppEduPlanVersionBlockLink link = model.getLink().refresh(EppEduPlanVersionBlockLink.class);
        final IEppRegElWrapper elementWrapper = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(regElement.getId())).get(regElement.getId());
        model.setDisciplineList(new DQLFullCheckSelectModel() {
            @Override
            public String getLabelFor(Object obj, int i) {
                EppRegistryElementPartFControlAction action = (EppRegistryElementPartFControlAction) obj;
                return action.getPart().getTitleWithNumber() + ", " + StringUtils.uncapitalize(action.getControlAction().getTitle());
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "er").column(DQLExpressions.property(EppEpvRegistryRow.registryElement().fromAlias("er")));
                sub.where(DQLExpressions.eq(DQLExpressions.property(EppEpvRegistryRow.owner().fromAlias("er")), DQLExpressions.value(model.getLink().getValue().getBlockDst())));
                DQLSelectBuilder dql = new DQLSelectBuilder();

                dql.fromEntity(EppRegistryElementPartFControlAction.class, alias);
                dql.where(DQLExpressions.in(DQLExpressions.property(EppRegistryElementPartFControlAction.part().registryElement().fromAlias(alias)), sub.buildQuery()));
//                dql.where(DQLExpressions.or(
//                        DQLExpressions.like(DQLExpressions.property(EppRegistryElementPartFControlAction.part().registryElement().title().fromAlias(alias)), DQLExpressions.value(CoreStringUtils.escapeLike(filter))),
//                        DQLExpressions.like(DQLExpressions.property(EppRegistryElementPartFControlAction.part().registryElement().number().fromAlias(alias)), DQLExpressions.value(CoreStringUtils.escapeLike(filter)))));

                dql.order(DQLExpressions.property(EppRegistryElementPartFControlAction.part().registryElement().title().fromAlias(alias)));
                FilterUtils.applyLikeFilter(dql, filter, EppRegistryElementPartFControlAction.part().registryElement().title().fromAlias(alias), EppRegistryElementPartFControlAction.part().registryElement().number().fromAlias(alias));
                return dql;

            }
        });

        final List<RowExt> rowList = new ArrayList<RowExt>();
        final RowExt elementRow = new RowExt(elementWrapper, null);
        elementRow.setViewProperty("enabled", false);
        rowList.add(elementRow);

        final StaticListDataSource<RowExt> ds = new StaticListDataSource<RowExt>();

        // кастомизация списка
        ds.setRowCustomizer(new SimpleRowCustomizer<RowExt>() {
            @Override
            public String getRowStyle(final RowExt entity) {
                final IEntity w = entity.getEntity();
                if (w instanceof IEppRegElWrapper) {
                    return "font-weight:bold;background-color: " + UniDefines.COLOR_BLUE + ";";
                }
                if (w instanceof IEppRegElPartWrapper) {
                    return "background-color: " + UniDefines.COLOR_BLUE + ";";
                }
                return "";
            }
        });

        // базовые колонки
        ds.addColumn(UniEppUtils.getStateColumn("item.module"));
        ds.addColumn(new SimpleColumn(EduPlanVersionBlockDataSourceGenerator.TITLE__TITLE, "title").setClickable(false).setOrderable(false).setTreeColumn(true).setWidth(500));

        for (final IEppRegElPartWrapper partWrapper : elementWrapper.getPartMap().values()) {
            final RowExt partRow = new RowExt(partWrapper, elementRow);
            partRow.setViewProperty("enabled", false);
            rowList.add(partRow);
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "c");
            dql.where(DQLExpressions.eq(DQLExpressions.property(EppRegistryElementPartFControlAction.part().fromAlias("c")), DQLExpressions.value(partWrapper.getItem())));
            List<EppRegistryElementPartFControlAction> list = getList(dql);

            //

            for (final EppRegistryElementPartFControlAction moduleWrapper : list) {
                final RowExt moduleRow = new RowExt(moduleWrapper, partRow);
                moduleRow.setViewProperty("enabled", true);
                rowList.add(moduleRow);
            }
        }

        ds.addColumn(new BlockColumn("dstDisc", "", "dstDisciplineColumn").setWidth(1));

        ds.setupRows(rowList);
        model.setDataSource(ds);
        updateBlockColumn(model.getDataSource(), model.getLink().getValue());
    }

    @Override
    public void saveLink(Model model) {
        //   IEntity row = get(id);
        List<RowExt> rowList = model.getDataSource().getRowList();
        BlockColumn column = ((BlockColumn) model.getDataSource().getColumn("dstDisc"));
        Map valueMap = column.getValueMap();
        for (RowExt row : rowList) {
            if (row.getEntity() instanceof EppRegistryElementPartFControlAction) {

                EppRegistryElementPartFControlAction newAction = (EppRegistryElementPartFControlAction) valueMap.get(row.getId());

                DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(VersionBlockDisciplinesLink.class)
                        .where(DQLExpressions.eq(DQLExpressions.property(VersionBlockDisciplinesLink.versionBlockLink()), DQLExpressions.value(model.getLink().getValue())))
                        .where(DQLExpressions.eq(DQLExpressions.property(VersionBlockDisciplinesLink.controlActionSrc()), DQLExpressions.value(row)));
                int rowCount = deleteBuilder.createStatement(getSession()).execute();

                if (newAction != null) {


                    VersionBlockDisciplinesLink link = new VersionBlockDisciplinesLink();
                    link.setVersionBlockLink(model.getLink().getValue());
                    link.setControlActionSrc((EppRegistryElementPartFControlAction) row.getEntity());
                    link.setControlActionDst(newAction);
                    saveOrUpdate(link);
                }
            }
        }
    }

    public void updateBlockColumn(StaticListDataSource<RowExt> dataSource, EppEduPlanVersionBlockLink link)
    {
        BlockColumn column = (BlockColumn) dataSource.getColumn("dstDisc");
        if (column != null) {
            Set<Long> ids = new HashSet<>();
            for (RowExt row : dataSource.getRowList()) {
                ids.add(row.getId());

            }
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(VersionBlockDisciplinesLink.class, "bl")
                    .where(DQLExpressions.eq(DQLExpressions.property(VersionBlockDisciplinesLink.versionBlockLink().fromAlias("bl")), DQLExpressions.value(link)))
                    .where(DQLExpressions.in(DQLExpressions.property(VersionBlockDisciplinesLink.controlActionSrc().fromAlias("bl")), ids));
            List<VersionBlockDisciplinesLink> list = getList(dql);
            Map valueMap = column.getValueMap();
            for (VersionBlockDisciplinesLink item : list) {
                valueMap.put(item.getControlActionSrc().getId(), item.getControlActionDst());
            }

        }
    }


}
