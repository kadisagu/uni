package ru.tandemservice.narfu.uniec.component.report.EntrantExamListPrint;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;

import java.util.Date;
import java.util.List;

public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel {
    private IPrincipalContext _principalContext;
    private MultiEnrollmentDirectionUtil.Parameters _parameters;

    private boolean _formativeOrgUnitActive;
    private ISelectModel _formativeOrgUnitListModel;
    private List<OrgUnit> _formativeOrgUnitList;

    private boolean _entrantListActive;
    private ISelectModel _entrantListModel;
    private List<Entrant> _entrantList;


    private boolean _educationLevelHighSchoolActive;
    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private ISelectModel _educationLevelHighSchoolListModel;
    private Date dateFrom;
    private Date dateTo;
    private EnrollmentCampaign enrollmentCampaign;

    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;


    private boolean page1Print;
    private boolean page2Print;

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return isEducationLevelHighSchoolActive() ? getEducationLevelHighSchoolList() : null;
    }

    // Getters & Setters
    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isEducationLevelHighSchoolActive()
    {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive)
    {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }


    public ISelectModel getEducationLevelHighSchoolListModel()
    {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel)
    {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        this.dateTo = dateTo;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }


    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList()
    {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList)
    {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList() {
        // TODO Auto-generated method stub
        return null;
    }

    public void setEntrantListActive(boolean _entrantListActive) {
        this._entrantListActive = _entrantListActive;
    }

    public boolean isEntrantListActive() {
        return _entrantListActive;
    }

    public void setEntrantListModel(ISelectModel _entrantListModel) {
        this._entrantListModel = _entrantListModel;
    }

    public ISelectModel getEntrantListModel() {
        return _entrantListModel;
    }

    public void setEntrantList(List<Entrant> _entrantList) {
        this._entrantList = _entrantList;
    }

    public List<Entrant> getEntrantList() {
        return _entrantList;
    }

    public void setPage1Print(boolean page1Print) {
        this.page1Print = page1Print;
    }

    public boolean isPage1Print() {
        return page1Print;
    }

    public void setPage2Print(boolean page2Print) {
        this.page2Print = page2Print;
    }

    public boolean isPage2Print() {
        return page2Print;
    }

}
