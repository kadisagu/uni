package ru.tandemservice.narfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_narfu_1x0x0_42to43 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность excludedAspirantsReport

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("excludedAspirantsReport");

        }


    }
}