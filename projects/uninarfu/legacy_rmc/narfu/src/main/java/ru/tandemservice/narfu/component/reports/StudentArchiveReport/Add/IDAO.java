package ru.tandemservice.narfu.component.reports.StudentArchiveReport.Add;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public Long createReport(Model model);
}
