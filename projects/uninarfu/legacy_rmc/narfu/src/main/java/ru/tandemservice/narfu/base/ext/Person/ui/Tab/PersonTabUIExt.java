package ru.tandemservice.narfu.base.ext.Person.ui.Tab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTabUI;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.tapsupport.component.document.DocumentServiceParameter;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.entity.PersonBenefitExt;
import ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU;
import ru.tandemservice.tools.PrivateAccessor;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.*;

@SuppressWarnings("deprecation")
public class PersonTabUIExt extends UIAddon {

    public static String P_MAIN_EDU_INSTITUTION = "mainEduInstitution";
    public static String P_MAIN_EDU_INSTITUTION_DISABLED = "mainEduInstitutionDisabled";
    public static String P_BENEFIT_TITLE_WITH_CAUSE = "titleWithCause";

    private PersonTabUI parentPresenter = null;

    private DynamicListDataSource<PersonEduInstitution> _personEduInstitutionDataSource;

    public PersonTabUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
        parentPresenter = (PersonTabUI) presenter;
    }

    @Override
    public void onComponentRefresh() {

        super.onComponentRefresh();
        prepareEduInstitutionDataSource();
        personNARFU = PersonNarfuUtil.getPersonNARFU(parentPresenter.getPerson());

        PrivateAccessor.setPrivateField(parentPresenter.getPersonBenefitDataSource(), "_delegate", (IListDataSourceDelegate) component -> prepareCustomDataSource(parentPresenter.getPersonBenefitDataSource(), "org.tandemframework.shared.person.base.entity.PersonBenefit", "person"));

        parentPresenter.getPersonBenefitDataSource().getColumn(0).setKey(P_BENEFIT_TITLE_WITH_CAUSE);
    }

    private void prepareEduInstitutionDataSource() {
        DynamicListDataSource<PersonEduInstitution> dataSource = new DynamicListDataSource<>(parentPresenter.getConfig().getBusinessComponent(), component -> {
            prepareCustomDataSource(_personEduInstitutionDataSource, "org.tandemframework.shared.person.base.entity.PersonEduInstitution", "person");
        }, 5L);

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Образовательное учреждение", PersonEduInstitutionNARFU.base().eduInstitution().title().s());
        linkColumn.setResolver(new DefaultPublisherLinkResolver() {
            public Object getParameters(IEntity entity) {
                ParametersMap map = new ParametersMap();
                map.add("publisherId", entity.getId());
                map.add("securedObjectId", parentPresenter.getPersonRoleModel().getSecuredObject().getId());
                map.add("accessible", parentPresenter.getPersonRoleModel().isAccessible());
                map.add("personRoleName", parentPresenter.getPersonRoleModel().getPersonRoleName());
                return map;
            }
        });
        dataSource.addColumn(linkColumn);
        dataSource.addColumn(new SimpleColumn("Вид образовательного учреждения", PersonEduInstitutionNARFU.base().eduInstitutionKind().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Населенный пункт", PersonEduInstitutionNARFU.base().addressItem().fullTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ступень образования", PersonEduInstitutionNARFU.base().educationLevelStage().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип документа", PersonEduInstitutionNARFU.base().documentType().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Год окончания", PersonEduInstitutionNARFU.base().yearEnd().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выдачи", PersonEduInstitutionNARFU.base().issuanceDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата выдачи документа из ЛД", PersonEduInstitutionNARFU.dateIssueCertificate().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Планируемая дата возврата в ЛД", PersonEduInstitutionNARFU.expectedDateReturn().s(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Фактическая дата возврата в ЛД", PersonEduInstitutionNARFU.actualDateReturn(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Основной", P_MAIN_EDU_INSTITUTION).toggleOnListener("onClickSetMain", "Сделать данный документ о полученном образовании основным?").setDisabledProperty(P_MAIN_EDU_INSTITUTION_DISABLED));

        dataSource.addColumn(new ServiceLinkColumn("Печатать", "id", "printer", new IServiceLinkBuilder() {
            public String getServiceName() {
                return "documentService";
            }

            public Object buildParameter(Object data) {
                Map<String, Object> params = new HashMap<>();
                params.put("id", data);
                return new DocumentServiceParameter("class", params);
            }
        }).setPermissionKey(parentPresenter.getSecModel().getPermission("printEduInstitution")));

        if (parentPresenter.getPersonRoleModel().isAccessible()) {
            dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditEduInstitution").setPermissionKey(parentPresenter.getSecModel().getPermission("editEduInstitution")));
            dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteEduInstitution", "Удалить {0} — {1} «{2}»?", new Object[]{PersonEduInstitutionNARFU.base().documentType().title().s(), PersonEduInstitutionNARFU.base().eduInstitution().eduInstitutionKind().title().s(), PersonEduInstitutionNARFU.base().eduInstitution().title().s()}).setPermissionKey(parentPresenter.getSecModel().getPermission("deleteEduInstitution")));
        }
        parentPresenter.setPersonEduInstitutionDataSource(dataSource);
        this._personEduInstitutionDataSource = dataSource;
    }

    private <T extends IEntity> void prepareCustomDataSource(DynamicListDataSource<T> dataSource, String entityClass, String property) {
        DQLSelectBuilder dqlbuilder = new DQLSelectBuilder();
        dqlbuilder.fromEntity(PersonEduInstitution.class, "pei");

        dqlbuilder.joinEntity("pei", DQLJoinType.inner, Person.class, "person", DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.person().fromAlias("pei")), DQLExpressions.property(Person.id().fromAlias("person"))));

        MQBuilder builder = new MQBuilder(entityClass, "o");
        builder.addJoin("o", property, "e");

        if (entityClass.equals("org.tandemframework.shared.person.base.entity.PersonEduInstitution")) {
            builder.addLeftJoin("o", "eduInstitution", "ei");

            dqlbuilder.joinEntity("pei", DQLJoinType.left, EduInstitution.class, "ei", DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.eduInstitution().id().fromAlias("pei")), DQLExpressions.property(EduInstitution.id().fromAlias("ei"))));
        }

        builder.add(MQExpression.eq("e", "id", this.parentPresenter.getPerson().getId()));
        dqlbuilder.where(DQLExpressions.eq(DQLExpressions.property(Person.id().fromAlias("person")), DQLExpressions.value(this.parentPresenter.getPerson().getId())));
        dqlbuilder.column("pei");

        if (entityClass.equals("org.tandemframework.shared.person.base.entity.PersonEduInstitution")) {
            List resultList = new ArrayList();
            List<PersonEduInstitution> entities = dqlbuilder.createStatement(DataAccessServices.dao().getComponentSession()).list();
            for (PersonEduInstitution pei : entities) {
                Wrapper wrapper = new Wrapper();
                wrapper.setBase(pei);
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(PersonEduInstitution.class, "pei");
                dql.joinEntity("pei", DQLJoinType.left, PersonEduInstitutionNARFU.class, "ext", DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.id().fromAlias("pei")), DQLExpressions.property(PersonEduInstitutionNARFU.base().id().fromAlias("ext"))));

                dql.where(DQLExpressions.eq(DQLExpressions.property(PersonEduInstitutionNARFU.base().id().fromAlias("ext")), DQLExpressions.value(pei.getId())));
                dql.column("ext");

                List personEduInstitutionNARFU = dql.createStatement(getSession()).list();
                if (personEduInstitutionNARFU.size() > 0) {
                    wrapper.setActualDateReturn(((PersonEduInstitutionNARFU) personEduInstitutionNARFU.get(0)).getActualDateReturn());
                    wrapper.setDateIssueCertificate(((PersonEduInstitutionNARFU) personEduInstitutionNARFU.get(0)).getDateIssueCertificate());
                    wrapper.setExpectedDateReturn(((PersonEduInstitutionNARFU) personEduInstitutionNARFU.get(0)).getExpectedDateReturn());
                }
                resultList.add(wrapper);
            }
            Collections.sort(resultList, new EntityComparator(dataSource.getEntityOrder()));
            CommonBaseSearchListUtil.createPage(dataSource, resultList);

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(dataSource)) {
                boolean isMain = viewWrapper.getEntity().equals(this.parentPresenter.getPerson().getPersonEduInstitution());
                viewWrapper.setViewProperty(P_MAIN_EDU_INSTITUTION, isMain);
                viewWrapper.setViewProperty(P_MAIN_EDU_INSTITUTION_DISABLED, isMain);
            }
        } else if (entityClass.equals("org.tandemframework.shared.person.base.entity.PersonBenefit")) {

            List<PersonBenefit> entities = builder.getResultList(getSession());
            int count = Math.max(entities.size() + 1, 2);
            dataSource.setTotalSize(count);
            dataSource.setCountRow(count);

            CommonBaseSearchListUtil.createPage(dataSource, builder, getSession());

            Map<Long, PersonBenefitExt> personBenefitMap = new HashMap<>();

            DQLSelectBuilder benefitBuilder = new DQLSelectBuilder().fromEntity(PersonBenefitExt.class, "ext")
                    .where(DQLExpressions.in(DQLExpressions.property(PersonBenefitExt.personBenefit().fromAlias("ext")), entities));

            List<PersonBenefitExt> extList = IUniBaseDao.instance.get().getList(benefitBuilder);

            for (PersonBenefitExt ext : extList) {
                personBenefitMap.put(ext.getPersonBenefit().getId(), ext);
            }
            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(dataSource)) {
                PersonBenefitExt ext = personBenefitMap.get(viewWrapper.getEntity().getId());
                PersonBenefit benefit = (PersonBenefit) viewWrapper.getEntity();
                viewWrapper.setViewProperty(P_BENEFIT_TITLE_WITH_CAUSE, ext == null ? benefit.getTitle() : ext.getTitleWithCauseDisability());
            }

        } else {
            List<T> entities = builder.getResultList(getSession());
            int count = Math.max(entities.size() + 1, 2);
            dataSource.setTotalSize(count);
            dataSource.setCountRow(count);
            dataSource.createPage(entities);
        }
    }


    public DynamicListDataSource<PersonEduInstitution> getPersonEduInstitutionDataSource() {
        return this._personEduInstitutionDataSource;
    }

    public void setPersonEduInstitutionDataSourceExt(DynamicListDataSource<PersonEduInstitution> personEduInstitutionDataSource) {
        this._personEduInstitutionDataSource = personEduInstitutionDataSource;
    }


    public void onClickSocialPaymentData() {
        Person person = parentPresenter.getPerson();
        parentPresenter.getActivationBuilder().asRegion("ru.tandemservice.narfu.base.ext.Person.ui.SocialPaymentEdit", "personTabPanel").parameter("person", person).activate();
    }

    public void onClickAddressTempEdit() {
        IBusinessComponent component = parentPresenter.getConfig().getBusinessComponent();

        component.createChildRegion("personTabPanel", new ComponentActivator("ru.tandemservice.narfu.component.person.AddressTempRegistrationEdit", new ParametersMap()
                .add("publisherId", personNARFU.getId())
                .add("addressProperty", "addressTempRegistration")
                .add("detailLevel", Integer.parseInt("4"))
                .add("showAddressInfo", Boolean.TRUE)
                .add("showContactInfo", Boolean.FALSE)
                .add("office", Boolean.FALSE)));
    }

    private PersonNARFU personNARFU;


    public PersonNARFU getPersonNARFU() {
        return personNARFU;
    }

    public void setPersonNARFU(PersonNARFU personNARFU) {
        this.personNARFU = personNARFU;
    }


}
