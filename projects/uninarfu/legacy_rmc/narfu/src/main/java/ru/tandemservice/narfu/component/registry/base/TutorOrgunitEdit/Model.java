package ru.tandemservice.narfu.component.registry.base.TutorOrgunitEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.List;

@Input({@org.tandemframework.core.component.Bind(key = "registryIds", binding = "registryIds")})
public class Model {

    private List<Long> registryIds;
    private OrgUnit tutorOU;
    private ISelectModel tutorOuModel;

    public List<Long> getRegistryIds() {
        return registryIds;
    }

    public void setRegistryIds(List<Long> registryIds) {
        this.registryIds = registryIds;
    }

    public OrgUnit getTutorOU() {
        return tutorOU;
    }

    public void setTutorOU(OrgUnit tutorOU) {
        this.tutorOU = tutorOU;
    }

    public ISelectModel getTutorOuModel() {
        return tutorOuModel;
    }

    public void setTutorOuModel(ISelectModel tutorOuModel) {
        this.tutorOuModel = tutorOuModel;
    }

}
