package ru.tandemservice.narfu.utils;

import org.apache.commons.lang.StringUtils;

public class NarfuEntrantRequestNumberFormatter extends ru.tandemservice.uniec.ui.EntrantRequestNumberFormatter {


    @Override
    public String format(Integer number) {

        if (number != null) {
            return StringUtils.leftPad(String.valueOf(number), 8, "0");
            /*int length = number.toString().length();
			String numb = "";
			if(length<7){
				for(int i=0; i<7-length;i++){
					numb += "0";
				}
			}
			return numb+number.toString();*/
        }
        return "";
    }
}
