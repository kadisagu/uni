package ru.tandemservice.narfu.component.entrant.EntrantRequestTab;


import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.eventssecrmc.entity.EntrantRequestSecExt;
import ru.tandemservice.narfu.entity.EntrantRequestNARFU;
import ru.tandemservice.narfu.utils.NarfuEntrantUtil;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;

import java.util.*;


public class DAO
        extends ru.tandemservice.eventssecrmc.component.entrant.EntrantRequestTab.DAO
        implements IDAO
{

    @Override
    public void prepare(
            ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantRequestSecExt.class, "ext")
                .joinEntity("ext", DQLJoinType.left, RequestedEnrollmentDirection.class, "red",
                            DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias("red")),
                                              DQLExpressions.property(EntrantRequestSecExt.entrantRequest().id().fromAlias("ext"))))
                .where(DQLExpressions.ne(DQLExpressions.property(RequestedEnrollmentDirection.state().code().fromAlias("red")), EntrantStateCodes.TAKE_DOCUMENTS_AWAY))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EntrantRequestSecExt.entrantRequest().entrant().fromAlias("ext")), myModel.getEntrant()))
                .setPredicate(DQLPredicateType.distinct)
                .column(DQLExpressions.property(EntrantRequestSecExt.orgUnit().title().fromAlias("ext")));

        List<String> res = getList(builder);

        if (!res.isEmpty()) {
            List<String> lst = new ArrayList<String>();
            for (String item : res) {
                lst.add(StringUtils.uncapitalize(item));
            }
            myModel.setHasEnrollmentComission(true);
            myModel.setEnrollmentComissionStr((new StringBuilder()).append("Место хранения личного дела: ").append(StringUtils.join(lst, ", ")).toString());
        }

    }

    /**
     * @param documents                документы абитуриента
     * @param lastPersonEduInstitution документ о последнем образовательном учреждении
     *
     * @return список строк с информацией об этих документах
     */
    public List<String[]> getEnrollmentDocuments(List<EntrantEnrollmentDocument> documents, PersonEduInstitution lastPersonEduInstitution, Entrant entrant)
    {
        List<String[]> result = new ArrayList<String[]>();

        String entrantRequestDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(documents.get(0).getEntrantRequest().getRegDate());
        Map<EnrollmentDocument, UsedEnrollmentDocument> settingsMap = new HashMap<EnrollmentDocument, UsedEnrollmentDocument>();
        if (!documents.isEmpty())
            for (UsedEnrollmentDocument usedEnrollmentDocument : getList(UsedEnrollmentDocument.class, UsedEnrollmentDocument.enrollmentCampaign().s(), documents.get(0).getEntrantRequest().getEntrant().getEnrollmentCampaign()))
                settingsMap.put(usedEnrollmentDocument.getEnrollmentDocument(), usedEnrollmentDocument);

        for (EntrantEnrollmentDocument entrantEnrollmentDocument : documents) {
            EnrollmentDocument enrollmentDocument = entrantEnrollmentDocument.getEnrollmentDocument();
            UsedEnrollmentDocument settings = settingsMap.get(enrollmentDocument);
            boolean printEduDocInfo = (null != settings && settings.isPrintEducationDocumentInfo());
            boolean printOriginalityInfo = (null != settings && settings.isPrintOriginalityInfo());
            StringBuilder title = new StringBuilder(enrollmentDocument.getTitle());
            String copyStr = "";

            if (printOriginalityInfo && entrantEnrollmentDocument.isCopy())
                copyStr = " (копия)";

            // Удостоверение личности
            if ((title.toString().toLowerCase().contains("паспорт") || title.toString().toLowerCase().contains("удостоверен")) && printEduDocInfo) {
                Criteria criteria = getSession().createCriteria(IdentityCard.class);
                criteria.add(Restrictions.eq(IdentityCard.L_PERSON, entrant.getPerson()));

                if (title.toString().toLowerCase().contains("удостоверен")) {
                    for (Object selectedObject : criteria.list()) {
                        IdentityCard identityCard = (IdentityCard) selectedObject;
                        StringBuilder title2 = new StringBuilder(title).append(copyStr);
                        result.add(new String[]{identityCard.getIssuanceDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getIssuanceDate()) : " - ", title2.toString()});
                    }
                    continue;
                }

                if (criteria.list().size() == 0) {
                    StringBuilder title2 = new StringBuilder(title).append(copyStr);
                    title2.append(" ").append("НЕТ ДАННЫХ");
                    result.add(new String[]{" - ", title2.toString()});
                    continue;
                }

                for (Object selectedObject : criteria.list()) {
                    IdentityCard identityCard = (IdentityCard) selectedObject;
                    StringBuilder title2 = new StringBuilder(identityCard.getTitle()).append(copyStr);

                    //title2.append(" ").append(identityCard.getSeria() != null ? identityCard.getSeria() : " - ");
                    //title2.append(" ").append(identityCard.getNumber() != null ? identityCard.getNumber() : " - ");

                    result.add(new String[]{identityCard.getIssuanceDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getIssuanceDate()) : " - ", title2.toString()});
                }
                continue;
            }
            // Аттестаты и дипломы об образовании с номерами
            if ((title.toString().toLowerCase().contains("аттестат") || title.toString().toLowerCase().contains("диплом") || title.toString().toLowerCase().contains("академическая справка")) && printEduDocInfo) {
                Criteria criteria = getSession().createCriteria(PersonEduInstitution.class);
                criteria.add(Restrictions.eq(PersonEduInstitution.L_PERSON, entrant.getPerson()));

                if (criteria.list().size() == 0) {
                    StringBuilder title2 = new StringBuilder(title).append(copyStr);
                    title2.append(" ").append("НЕТ ДАННЫХ");
                    result.add(new String[]{" - ", title2.toString()});
                    continue;
                }

                for (Object selectedObject : criteria.list()) {
                    PersonEduInstitution inst = (PersonEduInstitution) selectedObject;
                    StringBuilder title2 = new StringBuilder(inst.getDocumentType().getTitle());

                    title2.append(" ").append(inst.getSeria() != null ? inst.getSeria() : " - ");
                    title2.append(" ").append(inst.getNumber() != null ? inst.getNumber() : " - ");

                    title2.append(copyStr);
                    result.add(new String[]{inst.getIssuanceDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(inst.getIssuanceDate()) : " - ", title2.toString()});
                }
                continue;
            }


            // Свидетельства ЕГЭ с номерами
            if (title.toString().contains("ЕГЭ") && printEduDocInfo) {
                Criteria criteria = getSession().createCriteria(EntrantStateExamCertificate.class);
                criteria.add(Restrictions.eq(EntrantStateExamCertificate.L_ENTRANT, entrant));

                if (criteria.list().size() == 0) {
                    StringBuilder title2 = new StringBuilder(title).append(copyStr);
                    title2.append(" ").append("НЕТ ДАННЫХ");
                    result.add(new String[]{" - ", title2.toString()});
                    continue;
                }
                for (Object selectedObject : criteria.list()) {
                    StringBuilder title2 = new StringBuilder(title);

                    EntrantStateExamCertificate cert = (EntrantStateExamCertificate) selectedObject;
                    title2.append(" №").append(cert.getTitle());
                    title2.append(copyStr);
                    result.add(new String[]{cert.getIssuanceDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(cert.getIssuanceDate()) : " - ", title2.toString()});
                }
                continue;
            }

            // Дипломы олимпиад с номерами
            if (title.toString().toLowerCase().contains("олимпиады") && printEduDocInfo) {
                Criteria criteria = getSession().createCriteria(OlympiadDiploma.class);
                criteria.add(Restrictions.eq(OlympiadDiploma.L_ENTRANT, entrant));

                if (criteria.list().size() == 0) {
                    StringBuilder title2 = new StringBuilder(title).append(copyStr);
                    title2.append(" ").append("НЕТ ДАННЫХ");
                    result.add(new String[]{" - ", title2.toString()});
                    continue;
                }

                for (Object selectedObject : criteria.list()) {
                    StringBuilder title2 = new StringBuilder(title);
                    OlympiadDiploma diploma = (OlympiadDiploma) selectedObject;
                    title2.append(" ").append(diploma.getDegree().getTitle()).append(" \"").append(diploma.getOlympiad()).append("\"");

                    if (diploma.getSubject() != null) title2.append(" (").append(diploma.getSubject()).append(")");
                    if (diploma.getNumber() != null) title2.append(" №").append(diploma.getNumber());
                    if (diploma.getSettlement() != null) title2.append(" ").append(diploma.getSettlement());

                    title2.append(copyStr);

                    result.add(new String[]{diploma.getIssuanceDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(diploma.getIssuanceDate()) : " - ", title2.toString()});
                }
                continue;
            }

			/*
            if (printEduDocInfo && lastPersonEduInstitution != null)
			{
				title.append(" - ");
				if (lastPersonEduInstitution.getEduInstitution() != null)
				{
					title.append(lastPersonEduInstitution.getEduInstitution().getTitle()).append(" ");
				}
				title.append(lastPersonEduInstitution.getAddressItem().getTitleWithType()).append(" ");
				// title.append("в ").append(DateUtils.getYear(lastPersonEduInstitution.getYearEnd())).append("г. ");
				title.append("в ").append(Integer.toString(lastPersonEduInstitution.getYearEnd())).append("г. ");

				if (lastPersonEduInstitution.getSeria() != null || lastPersonEduInstitution.getNumber() != null)
				{
					title.append("документ №");
					if (lastPersonEduInstitution.getSeria() != null)
					{
						title.append(lastPersonEduInstitution.getSeria()).append(" ");
					}
					if (lastPersonEduInstitution.getNumber() != null)
					{
						title.append(lastPersonEduInstitution.getNumber());
					}
				}
			}
			*/
            result.add(new String[]{entrantRequestDate, title.append(copyStr).toString()});
        }
        // Убрано как дублирующее значение
        //result.add(new String[] {entrantRequestDate, "Расписка о получении документов"});

        // Договора с абитуриентом
        List<UniscEduAgreementBase> agreements = getAgreements(entrant);

        // Для зачисленных абитуриентов выводим "Выписка из приказа о зачислении"

        if (entrant.getState().getCode().equals(EntrantStateCodes.ENROLED) || entrant.getState().getCode().equals(EntrantStateCodes.IN_ORDER)) {

            List<AbstractEntrantExtract> extractList = getList(AbstractEntrantExtract.class, AbstractEntrantExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().id(), entrant.getId());
            AbstractEntrantExtract extract = null;
            if (extractList.size() > 1)
                for (AbstractEntrantExtract currentExtract : extractList) {
                    if (currentExtract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getState().getCode().equals(EntrantStateCodes.ENROLED))
                        extract = currentExtract;
                }
            else if (extractList.size() == 1)
                extract = extractList.get(0);


            String orderDateTitle = "";
            StringBuilder orderNumber = new StringBuilder("");

            if (extract != null) {
                if (extract.getParagraph().getOrder().getCommitDate() != null)
                    orderDateTitle = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate());
                String number = extract.getParagraph().getOrder().getNumber();
                if (!StringUtils.isEmpty(number))
                    orderNumber.append("№ ").append(number);
            }

            result.add(new String[]{orderDateTitle, "Выписка из приказа о зачислении " + orderNumber.toString()});
        }

        for (UniscEduAgreementBase agreement : agreements) {
            result.add(new String[]{
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(agreement.getLastModificationDate()),
                    "Договор на оказание платных образовательных услуг"
            });
        }

        MQBuilder directionBuilder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection", "r");
        directionBuilder.add(documents.get(0).getEntrantRequest() != null ? MQExpression.eq("r", "entrantRequest", documents.get(0).getEntrantRequest()) : MQExpression.eq("r", "entrantRequest.entrant", entrant));
        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), entrant.getEnrollmentCampaign(), directionBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        List<RequestedEnrollmentDirection> list = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest(), documents.get(0).getEntrantRequest());

        Set<ChosenEntranceDiscipline> chosenEntranceDisciplineSet = dataUtil.getChosenEntranceDisciplineSet(list.get(0));

        boolean examList = false;
        int countInternalExams = 0;

        for (ChosenEntranceDiscipline discipline : chosenEntranceDisciplineSet) {
            int internalExam = dataUtil.getMarkStatistic(discipline).getInternalMarkMap().size();
            if (internalExam > 0) {
                examList = true;
                countInternalExams++;
            }
        }
        if (examList) {
            result.add(new String[]{entrantRequestDate, "Экзаменационный лист"});
        }

        if (countInternalExams > 0) {
            if (countInternalExams == 1) {
                result.add(new String[]{entrantRequestDate, "Работа абитуриента на вступительных испытаниях"});
            }
            else if (countInternalExams > 1) {
                result.add(new String[]{entrantRequestDate, "Работы абитуриента на вступительных испытаниях"});
            }
        }

        return result;
    }


    @Override
    public void prepareRequestedEnrollmentDirectionDataSource(ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Model model, EntrantRequest request) {
        super.prepareRequestedEnrollmentDirectionDataSource(model, request);

        EntrantRequestNARFU requestNARFU = get(EntrantRequestNARFU.class, EntrantRequestNARFU.entrantRequest(), request);
        DynamicListDataSource dataSource = model.getRequestedEnrollmentDirectionDataSourceByRequest().get(request);
        for (Object obj : dataSource.getEntityList()) {
            ViewWrapper<RequestedEnrollmentDirection> viewWrapper = (ViewWrapper<RequestedEnrollmentDirection>) obj;
            viewWrapper.setViewProperty("requestNARFU", requestNARFU);
        }
    }


    @Override
    public void updateDocumentsReturn(Long requestId) {
        super.updateDocumentsReturn(requestId);

        updateEntrantRequestNARFUDate(requestId, null);
    }

    @Override
    public void updateDocumentsTakeAway(Long requestId) {
        super.updateDocumentsTakeAway(requestId);

        updateEntrantRequestNARFUDate(requestId, new Date());
    }

    private void updateEntrantRequestNARFUDate(Long requestId, Date date) {
        EntrantRequest entrantRequest = (EntrantRequest) getNotNull(EntrantRequest.class, requestId);
        EntrantRequestNARFU entrantRequestNARFU = get(EntrantRequestNARFU.class, EntrantRequestNARFU.entrantRequest(), entrantRequest);
        if (entrantRequestNARFU == null) {
            entrantRequestNARFU = new EntrantRequestNARFU();
            entrantRequestNARFU.setEntrantRequest(entrantRequest);
        }
        entrantRequestNARFU.setDateDocumentsTakeAway(date);
        saveOrUpdate(entrantRequestNARFU);
    }

    /**
     * Список договоров, заключенных с абитуриентом
     *
     * @param entrant
     *
     * @return
     */
    private List<UniscEduAgreementBase> getAgreements(Entrant entrant) {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(UniscEduAgreement2Entrant.class, "a2e")
                .where(DQLExpressions.eqValue(
                        DQLExpressions.property(UniscEduAgreement2Entrant.entrant().id().fromAlias("a2e")),
                        entrant.getId()
                ))
                .column(DQLExpressions.property(UniscEduAgreement2Entrant.agreement().fromAlias("a2e")));

        List<UniscEduAgreementBase> agreements = dql.createStatement(getSession()).list();

        return agreements;

    }

    @Override
    public void updateEntrantPersonalNumber(Model model, boolean isChangePriority) {

        if (!isChangePriority) {
            EntrantRequest currentEntrantRequest = get(EntrantRequest.class, model.getCurrentEntrantRequest().getId());

            String entrantUniqueNumber = NarfuEntrantUtil.getEntrantUniqueNumber(model.getCurrentEntrantRequest());

            currentEntrantRequest.setRegNumber(Integer.valueOf(entrantUniqueNumber));
            currentEntrantRequest.getEntrant().setPersonalNumber(entrantUniqueNumber);
            update(currentEntrantRequest);
        }
    }
}
