package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.FormingOrgUnit2Commission;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь формирующих подразделений и отборочных комиссий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FormingOrgUnit2CommissionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.FormingOrgUnit2Commission";
    public static final String ENTITY_NAME = "formingOrgUnit2Commission";
    public static final int VERSION_HASH = -599545508;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_FORMING_ORG_UNIT = "formingOrgUnit";
    public static final String L_COMMISSION = "commission";

    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private OrgUnit _formingOrgUnit;     // Формирующее подразделение
    private OrgUnit _commission;     // Отборочная комиссия

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormingOrgUnit()
    {
        return _formingOrgUnit;
    }

    /**
     * @param formingOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormingOrgUnit(OrgUnit formingOrgUnit)
    {
        dirty(_formingOrgUnit, formingOrgUnit);
        _formingOrgUnit = formingOrgUnit;
    }

    /**
     * @return Отборочная комиссия.
     */
    public OrgUnit getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Отборочная комиссия.
     */
    public void setCommission(OrgUnit commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FormingOrgUnit2CommissionGen)
        {
            setEnrollmentCampaign(((FormingOrgUnit2Commission)another).getEnrollmentCampaign());
            setFormingOrgUnit(((FormingOrgUnit2Commission)another).getFormingOrgUnit());
            setCommission(((FormingOrgUnit2Commission)another).getCommission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FormingOrgUnit2CommissionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FormingOrgUnit2Commission.class;
        }

        public T newInstance()
        {
            return (T) new FormingOrgUnit2Commission();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "formingOrgUnit":
                    return obj.getFormingOrgUnit();
                case "commission":
                    return obj.getCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "formingOrgUnit":
                    obj.setFormingOrgUnit((OrgUnit) value);
                    return;
                case "commission":
                    obj.setCommission((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "formingOrgUnit":
                        return true;
                case "commission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "formingOrgUnit":
                    return true;
                case "commission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "formingOrgUnit":
                    return OrgUnit.class;
                case "commission":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FormingOrgUnit2Commission> _dslPath = new Path<FormingOrgUnit2Commission>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FormingOrgUnit2Commission");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.FormingOrgUnit2Commission#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.FormingOrgUnit2Commission#getFormingOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formingOrgUnit()
    {
        return _dslPath.formingOrgUnit();
    }

    /**
     * @return Отборочная комиссия.
     * @see ru.tandemservice.narfu.entity.FormingOrgUnit2Commission#getCommission()
     */
    public static OrgUnit.Path<OrgUnit> commission()
    {
        return _dslPath.commission();
    }

    public static class Path<E extends FormingOrgUnit2Commission> extends EntityPath<E>
    {
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private OrgUnit.Path<OrgUnit> _formingOrgUnit;
        private OrgUnit.Path<OrgUnit> _commission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.FormingOrgUnit2Commission#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.FormingOrgUnit2Commission#getFormingOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formingOrgUnit()
        {
            if(_formingOrgUnit == null )
                _formingOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMING_ORG_UNIT, this);
            return _formingOrgUnit;
        }

    /**
     * @return Отборочная комиссия.
     * @see ru.tandemservice.narfu.entity.FormingOrgUnit2Commission#getCommission()
     */
        public OrgUnit.Path<OrgUnit> commission()
        {
            if(_commission == null )
                _commission = new OrgUnit.Path<OrgUnit>(L_COMMISSION, this);
            return _commission;
        }

        public Class getEntityClass()
        {
            return FormingOrgUnit2Commission.class;
        }

        public String getEntityName()
        {
            return "formingOrgUnit2Commission";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
