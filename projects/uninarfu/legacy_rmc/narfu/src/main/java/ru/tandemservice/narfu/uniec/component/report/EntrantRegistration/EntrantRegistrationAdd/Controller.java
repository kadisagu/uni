package ru.tandemservice.narfu.uniec.component.report.EntrantRegistration.EntrantRegistrationAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    public Controller()
    {
    }

    public void onRefreshComponent(IBusinessComponent component)
    {
        ((IDAO) getDao()).prepare(getModel(component));
        ((Model) getModel(component)).setPrincipalContext(component.getUserContext().getPrincipalContext());
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);
        deactivate(component);
        activateInRoot(component, new PublisherActivator(model.getReport()));
    }

}