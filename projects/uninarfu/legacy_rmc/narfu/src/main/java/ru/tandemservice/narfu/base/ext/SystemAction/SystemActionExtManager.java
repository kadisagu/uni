package ru.tandemservice.narfu.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.narfu.base.ext.SystemAction.ui.Pub.SystemActionPubExt;


@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager {


    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("narfu_importStudentsTo1C", new SystemActionDefinition("narfu", "importStudentsTo1C", "onImportStudentsTo1C", SystemActionPubExt.NARFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("narfu_demonStudentsTo1CState", new SystemActionDefinition("narfu", "demonStudentsTo1CState", "onDemonStudentsTo1CState", SystemActionPubExt.NARFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("narfu_runDemonStudentsTo1C", new SystemActionDefinition("narfu", "runDemonStudentsTo1C", "onRunDemonStudentsTo1C", SystemActionPubExt.NARFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("narfu_runClearGrant", new SystemActionDefinition("narfu", "runClearGrant", "onRunClearGrant", SystemActionPubExt.NARFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("narfu_copyOldAgreementToNew", new SystemActionDefinition("narfu", "copyOldAgreementToNew", "onCopyOldAgreementToNew", SystemActionPubExt.NARFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("movestudent_copyOrders", new SystemActionDefinition("movestudent", "copyOrders", "onClickCopyOrders", SystemActionPubExt.NARFU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
