package ru.tandemservice.narfu.component.reports.StudentArchiveReport.List;

import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.narfu.entity.NarfuReportStudentArchive;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private IDataSettings settings;
    private ISelectModel educationYearModel;

    private Long orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel secModel;

    private DynamicListDataSource<NarfuReportStudentArchive> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<NarfuReportStudentArchive> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<NarfuReportStudentArchive> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getEducationYearModel() {
        return educationYearModel;
    }

    public void setEducationYearModel(ISelectModel educationYearModel) {
        this.educationYearModel = educationYearModel;
    }

    public CommonPostfixPermissionModel getSecModel() {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        this.secModel = secModel;
    }

    public Object getSecuredObject() {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public String getViewKey() {

        return null == getOrgUnit() ? "viewStudentArchiveReport" : getSecModel().getPermission("orgUnit_viewStudentArchiveReport");
    }

    public String getAddKey()
    {
        return null == getOrgUnit() ? "addGlobalStoredReport" : getSecModel().getPermission("addStorableReport");
    }

    public String getPrintKey()
    {
        return null == getOrgUnit() ? "printGlobalStoredReport" : getSecModel().getPermission("printStorableReport");
    }

    public String getDeleteKey()
    {
        return null != getOrgUnit() ? getSecModel().getPermission("deleteStorableReport") : "deleteUniContingentStorableReport";
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

}
