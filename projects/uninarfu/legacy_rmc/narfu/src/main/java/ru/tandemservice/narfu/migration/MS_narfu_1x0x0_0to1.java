package ru.tandemservice.narfu.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

import java.sql.ResultSet;
import java.sql.Statement;


public class MS_narfu_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("personnarfu_t") && tool.tableExists("person_t")) {
            short code = EntityRuntime.getMeta(PersonNARFU.class).getEntityCode();

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select id from person_t");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                tool.executeUpdate("insert into personnarfu_t (id, discriminator, person_id, needsocialpayment_p) values (?,?,?,?)",
                                   EntityIDGenerator.generateNewId(code), code, rs.getLong(1), false
                );
            }
        }
    }
}