package ru.tandemservice.narfu.base.bo.SessionReportNarfu.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinList.SessionSummaryBulletinDSHandler;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SessionSummaryNarfuBulletinDSHandler extends SessionSummaryBulletinDSHandler
{

    public static final String PARAM_GROUPS_OBJECT = "groupsKey";

    public SessionSummaryNarfuBulletinDSHandler(String arg0)
    {
        super(arg0);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context)
    {

        Object sessionObject = context.get(PARAM_SESSION_OBJECT);
        if (null == sessionObject)
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UnisessionSummaryBulletinReport.class, "r").column("r");

        FilterUtils.applySelectFilter(dql, "r", UnisessionSummaryBulletinReport.sessionObject(), sessionObject);

        List<Group> groups = context.get(PARAM_GROUPS_OBJECT);
        if (groups != null && !groups.isEmpty())
        {
            IDQLExpression[] groupsExpr = groups.stream()
                    .map(group -> like(property("r", UnisessionSummaryBulletinReport.groups()), value(CoreStringUtils.escapeLike(group.getTitle(), true))))
                    .toArray(IDQLExpression[]::new);
            dql.where(or(groupsExpr));
        }

        buildOrderRegistry().applyOrder(dql, dsInput.getEntityOrder());

        return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

}
