package ru.tandemservice.narfu.military.dao;

import com.google.common.collect.Lists;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.militaryrmc.dao.OrderDAO;
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

import java.util.*;


public class OrderDAONarfu extends OrderDAO
{

    @Override
    public Map<Long, List<StudenOrderInfo>> getOrdersInfo(List<Long> studentsIds, boolean useManualInput)
    {
        Map<Long, List<StudenOrderInfo>> studentOrderInfoMap = super.getOrdersInfo(studentsIds, useManualInput);

        // ограничения возможностей sql запроса
        for (List<Long> subStudentsIds : Lists.partition(studentsIds, DQL.MAX_VALUES_ROW_NUMBER))
        {
            // приказы о зачислении
            putEnrollmentOrders(subStudentsIds, studentOrderInfoMap);

            // ручной ввод (не добавляем, если есть совпадение по номеру и дате, на тип внимания не обращаем)
            if (useManualInput) checkManualInput(subStudentsIds, studentOrderInfoMap);
        }

        studentOrderInfoMap.values().forEach(Collections::sort);

        return studentOrderInfoMap;
    }

    private void putEnrollmentOrders(List<Long> studentsIds, Map<Long, List<StudenOrderInfo>> studentOrderInfoMap)
    {
        String ext_alias = "enrext";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, ext_alias)
                .where(DQLExpressions.eqValue(DQLExpressions.property(ext_alias, EnrEnrollmentExtract.paragraph().order().state().code()), OrderStatesCodes.FINISHED))
                .where(DQLExpressions.in(DQLExpressions.property(ext_alias, EnrEnrollmentExtract.student().id()), studentsIds))
                .column(DQLExpressions.property(ext_alias, EnrEnrollmentExtract.student().id()))
                .column(DQLExpressions.property(ext_alias, EnrEnrollmentExtract.paragraph().order()))
                .column(DQLExpressions.property(ext_alias, EnrEnrollmentExtract.paragraph()));

        List<Object[]> enrOrders = getList(dql);
        for (Object[] enrOrder : enrOrders)
        {
            Long studentId = (Long) enrOrder[0];
            EnrOrder order = (EnrOrder) enrOrder[1];
            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) enrOrder[2];

            StudenOrderInfo studentOrderInfo = new StudenOrderInfo(
                    order.getId(),
                    order.getCommitDate(),
                    order.getNumber(),
                    getByCode(StudentExtractType.class, StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER), // приказ - о зачислении,
                    paragraph.getFormativeOrgUnit().getTitle(),
                    StudenOrderInfo.FROM_ENTRANT_ORDERS,
                    paragraph.getFormativeOrgUnit().getShortTitle()
            );

            addOrder(studentOrderInfoMap, null, studentOrderInfo, studentId);
        }
    }

    private void checkManualInput(List<Long> studentsIds, Map<Long, List<StudenOrderInfo>> studentOrderInfoMap)
    {
        // выделяем ручной ввод (то что набили в таблицу приказов)
        DQLSelectBuilder handBuilder = new DQLSelectBuilder()
                .fromEntity(OrderList.class, "ol")
                .where(DQLExpressions.in(DQLExpressions.property(OrderList.student().id().fromAlias("ol")), studentsIds))
                .column("ol")
                .column(OrderList.student().fromAlias("ol").s());

        List<Object[]> orderManual = getList(handBuilder);

        for (Object[] oM : orderManual)
        {

            OrderList orderList = (OrderList) oM[0];
            Student student = (Student) oM[1];

            String orgUnitString = "";
            String orgShortString = "";

            if (student.getEducationOrgUnit() != null && student.getEducationOrgUnit().getFormativeOrgUnit() != null)
            {
                orgUnitString = student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();
                orgShortString = student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();
            }

            StudenOrderInfo o = new StudenOrderInfo(
                    orderList.getId(),
                    orderList.getOrderDate(),
                    orderList.getOrderNumber(),
                    orderList.getType(),
                    orgUnitString,
                    StudenOrderInfo.FROM_LIST_ORDERS,
                    orgShortString
            );
            o.setBeginDate(orderList.getOrderDateStart());

            addOrder(studentOrderInfoMap, null, o, student.getId());
        }
    }

    @Override
    protected void _fillOrderInfoByMoveRmc(Set<String> orderKeySet,
                                           Map<Long, List<StudenOrderInfo>> studentOrderInfoMap,
                                           List<Long> studentIds
    )
    {
        //по проведенным представлениям RMC
        //по сборным
        for (Object[] data : getStudentsFromDocRepresent(studentIds))
        {
            StudentExtractType orderType = new StudentExtractType();
            String title = (String) data[2];
            title = title.equalsIgnoreCase("О выходе из ао") ? "О выходе из академического отпуска" : title;
            title = title.equalsIgnoreCase("О продлении ао") ? "О продлении академического отпуска" : title;
            orderType.setTitle(title);
            orderType.setCode((String) data[3]);

            StudenOrderInfo studentOrderInfo = new StudenOrderInfo((Long) data[1], (Date) data[5], (String) data[6], orderType,
                                                                   (String) data[4], StudenOrderInfo.FROM_MOVESTUDENTRMC);

            studentOrderInfo.setOrderReason(((String) data[7]).toLowerCase());

            addOrder(studentOrderInfoMap, orderKeySet, studentOrderInfo, (Long) data[0]);
        }

        //по проведенным представлениям RMC
        //по списочным
        for (Object[] row : getStudentsFromRelListRepresent(studentIds))
        {
            ListRepresent rep = (ListRepresent) row[0];
            ListOrder order = (ListOrder) row[1];

            StudentExtractType orderType = new StudentExtractType();
            String title = rep.getRepresentationType().getTitle();

            title = title.equalsIgnoreCase("О выходе из ао") ? "О выходе из академического отпуска" : title;
            title = title.equalsIgnoreCase("О продлении ао") ? "О продлении академического отпуска" : title;
            orderType.setTitle(title);
            orderType.setCode(rep.getRepresentationType().getCode());

            String orgUnitString = "";

            if (((Student) row[4]).getEducationOrgUnit() != null && ((Student) row[4]).getEducationOrgUnit().getFormativeOrgUnit() != null)
                orgUnitString = ((Student) row[4]).getEducationOrgUnit().getFormativeOrgUnit().getTitle();

            RelListRepresentStudentsOldData oldData = (RelListRepresentStudentsOldData) row[2];
            if (oldData != null && oldData.getOldGroup() != null)
                orgUnitString = oldData.getOldGroup().getEducationOrgUnit().getFormativeOrgUnit().getTitle();

            StudenOrderInfo studentOrderInfo = new StudenOrderInfo(rep.getId(), order.getCommitDate(), order.getNumber(), orderType,
                                                                   orgUnitString, StudenOrderInfo.FROM_MOVESTUDENTRMC);
            studentOrderInfo.setOrderReason(((String) row[3]).toLowerCase());

            addOrder(studentOrderInfoMap, orderKeySet, studentOrderInfo, ((Student) row[4]).getId());
        }
    }

    List<Object[]> getStudentsFromDocRepresent(List<Long> studentIds)
    {
        String dr_alias = "dr";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, dr_alias);

        String dor_alias = "dor";
        dql.joinEntity(dr_alias, DQLJoinType.left, DocOrdRepresent.class, dor_alias,
                       DQLExpressions.eq(
                               DQLExpressions.property(dr_alias, DocRepresentStudentBase.representation().id()),
                               DQLExpressions.property(dor_alias, DocOrdRepresent.representation().id())))
                .where(DQLExpressions.in(DQLExpressions.property(dr_alias, DocRepresentStudentBase.student().id()), studentIds))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(dr_alias, DocRepresentStudentBase.representation().state().code()),
                        DQLExpressions.value(MovestudentExtractStatesCodes.CODE_6))); // НЕ в приказах, а проведенные

        dql.column(DocRepresentStudentBase.student().id().fromAlias(dr_alias).s())
                .column(DocRepresentStudentBase.id().fromAlias(dr_alias).s())
                .column(DocRepresentStudentBase.representation().type().title().fromAlias(dr_alias).s())
                .column(DocRepresentStudentBase.representation().type().code().fromAlias(dr_alias).s())
                .column(DocRepresentStudentBase.formativeOrgUnitStr().fromAlias(dr_alias).s())
                .column(DocOrdRepresent.order().commitDate().fromAlias(dor_alias).s())
                .column(DocOrdRepresent.order().number().fromAlias(dor_alias).s())
                .column(DocOrdRepresent.representation().reason().title().fromAlias(dor_alias).s());

        return dql.createStatement(getSession()).list();
    }

    List<Object[]> getStudentsFromRelListRepresent(List<Long> studentIds)
    {
        String rlrs_alias = "rlrs";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RelListRepresentStudents.class, rlrs_alias);

        String oldrr_alias = "oldrr";
        dql.joinEntity(rlrs_alias, DQLJoinType.left, RelListRepresentStudentsOldData.class, oldrr_alias,
                       DQLExpressions.eq(
                               DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias(rlrs_alias)),
                               DQLExpressions.property(RelListRepresentStudentsOldData.representation().id().fromAlias(oldrr_alias))))
                .where(DQLExpressions.in(
                        DQLExpressions.property(RelListRepresentStudents.student().id().fromAlias(rlrs_alias)),
                        studentIds
                ))
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RelListRepresentStudents.representation().state().code().fromAlias(rlrs_alias)),
                        DQLExpressions.value(MovestudentExtractStatesCodes.CODE_6)// Проведено
                ));

        String lolr_alias = "lolr";
        dql.fromEntity(ListOrdListRepresent.class, lolr_alias)
                .where(DQLExpressions.eq(
                        DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias(rlrs_alias)),
                        DQLExpressions.property(ListOrdListRepresent.representation().id().fromAlias(lolr_alias))
                ))
                .column(ListOrdListRepresent.representation().fromAlias(lolr_alias).s())
                .column(ListOrdListRepresent.order().fromAlias(lolr_alias).s())
                .column(oldrr_alias)
                .column(ListOrdListRepresent.representation().representationReason().title().fromAlias(lolr_alias).s())
                .column(RelListRepresentStudents.student().fromAlias(rlrs_alias).s());

        return dql.createStatement(getSession()).list();
    }

    private void addOrder(Map<Long, List<StudenOrderInfo>> studentOrderInfoMap, Set<String> orderKeySet, StudenOrderInfo studentOrderInfo, Long studentId)
    {
        if (orderKeySet != null)
        {
            String key = getOrderKey(studentOrderInfo, studentId);
            if (orderKeySet.contains(key))
                return;
            orderKeySet.add(key);
        }

        List<StudenOrderInfo> studentOrderInfoList = studentOrderInfoMap.get(studentId);
        if (studentOrderInfoList != null)
        {
            if (orderKeySet == null && hasSimilarOrder(studentOrderInfo, studentOrderInfoList))
                return;
        }
        else
        {
            studentOrderInfoList = new ArrayList<>();
            studentOrderInfoMap.put(studentId, studentOrderInfoList);
        }
        studentOrderInfoList.add(studentOrderInfo);
    }

    private static boolean hasSimilarOrder(StudenOrderInfo orderInfo, List<StudenOrderInfo> orderInfoList)
    {
        return orderInfoList.stream().anyMatch(
                info -> info.getId().equals(orderInfo.getId())
                        || (info.getOrderNumber().equals(orderInfo.getOrderNumber()) && info.getOrderDate().equals(orderInfo.getOrderDate())));
    }
}
