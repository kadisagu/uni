package ru.tandemservice.narfu.base.ext.EcOrder.logic;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.narfu.entity.EntrHighShortExtractExt;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.IEnrollmentExtractFactory;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentParagraph;
import ru.tandemservice.uniec.entity.orders.EntrHighShortExtract;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.*;


public class EcOrderDaoExt extends CommonDAO implements IEcOrderDaoExt {

    @Override
    public RtfDocument getDownloadPrintPersonalInfo(Long orderId) {
        return EcOrderPrintDAO.getInstance().getDownloadPrintPersonalInfo(orderId);
    }


    @Override
    public RtfDocument getDownloadPrintExtracts(Long orderId) {
        return EcOrderPrintDAO.getInstance().getDownloadPrintExtracts(orderId);
    }

    @Override
    public RtfDocument getDownloadPrintParagraphExtracts(List<IAbstractParagraph<IAbstractOrder>> paragraphList) {
        return EcOrderPrintDAO.getInstance().getDownloadPrintParagraphExtracts(paragraphList);
    }

    @Override
    public RtfDocument getDownloadPrintExtract(EnrollmentExtract extract) {
        return EcOrderPrintDAO.getInstance().getDownloadPrintExtract(extract);
    }

    @Override
    public RtfDocument getDownloadPrintParagraphPersonalInfo(List<IAbstractParagraph<IAbstractOrder>> paragraphList) {
        return EcOrderPrintDAO.getInstance().getDownloadPrintParagraphPersonalInfo(paragraphList);
    }

    @Override
    public void saveOrUpdateAspirantParagraphExt(EnrollmentOrder order,
                                                 EnrollmentParagraph paragraph,
                                                 IEnrollmentExtractFactory extractFactory,
                                                 boolean enrollParagraphPerEduOrgUnit,
                                                 Collection<PreliminaryEnrollmentStudent> preStudents,
                                                 PreliminaryEnrollmentStudent manager, OrgUnit formativeOrgUnit,
                                                 EducationLevelsHighSchool educationLevelsHighSchool,
                                                 DevelopForm developForm, Course course, String groupTitle,
                                                 EducationOrgUnit educationOrgUnit)
    {


        NamedSyncInTransactionCheckLocker.register(getSession(), new StringBuilder().append("EnrollmentOrder").append(order.getId()).toString());

        if ((preStudents == null) || (preStudents.isEmpty())) {
            throw new ApplicationException("Параграф не может быть пустым.");
        }
        Session session = getSession();
        session.refresh(order);

        if (!"1".equals(order.getState().getCode())) {
            throw new ApplicationException("Приказ уже не в состоянии формирования. Изменение приказа невозможно.");
        }
        List<PreliminaryEnrollmentStudent> list = new ArrayList<PreliminaryEnrollmentStudent>(preStudents);
        Collections.sort(list, ITitled.TITLED_COMPARATOR);

        if ((manager != null) && (!list.contains(manager))) {
            throw new ApplicationException("Старостой может быть только один из предварительно зачисленных абитуриентов.");
        }

        for (PreliminaryEnrollmentStudent preStudent : list) {
            Entrant entrant = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant();

            EnrollmentExtract enrollmentExtract = (EnrollmentExtract) get(EnrollmentExtract.class, "entity", preStudent);
            if ((enrollmentExtract != null) && ((paragraph == null) || (!paragraph.equals(enrollmentExtract.getParagraph())))) {
                String orderNumber = enrollmentExtract.getOrder().getNumber();
                throw new ApplicationException(new StringBuilder().append("Абитуриент «").append(entrant.getPerson().getFullFio()).append("» уже в приказе").append(StringUtils.isEmpty(orderNumber) ? "" : new StringBuilder().append(" №").append(orderNumber).toString()).append(".").toString());
            }

            if (entrant.isArchival()) {
                throw new ApplicationException(new StringBuilder().append("Абитуриент «").append(entrant.getPerson().getFullFio()).append("» уже в архиве.").toString());
            }

            if (!order.getEnrollmentCampaign().equals(entrant.getEnrollmentCampaign())) {
                throw new ApplicationException(new StringBuilder().append("Приемная кампания у предварительно зачисленного абитуриента «").append(entrant.getPerson().getFullFio()).append("» отличается от указанной в приказе.").toString());
            }

            if (!order.getType().equals(preStudent.getEntrantEnrollmentOrderType())) {
                throw new ApplicationException(new StringBuilder().append("Тип приказа у предварительно зачисленного абитуриента «").append(entrant.getPerson().getFullFio()).append("» отличается от типа приказа.").toString());
            }

            if (!formativeOrgUnit.equals(preStudent.getEducationOrgUnit().getFormativeOrgUnit())) {
                throw new ApplicationException(new StringBuilder().append("Формирующее подразделение у предварительно зачисленного абитуриента «").append(entrant.getPerson().getFullFio()).append("» отличается от указанного в приказе.").toString());
            }

            if (!educationLevelsHighSchool.equals(preStudent.getEducationOrgUnit().getEducationLevelHighSchool())) {
                throw new ApplicationException(new StringBuilder().append("Направление подготовки (специальность) у предварительно зачисленного абитуриента «").append(entrant.getPerson().getFullFio()).append("» отличается от указанного в приказе.").toString());
            }

            if (!developForm.equals(preStudent.getEducationOrgUnit().getDevelopForm())) {
                throw new ApplicationException(new StringBuilder().append("Форма освоения у предварительно зачисленного абитуриента «").append(entrant.getPerson().getFullFio()).append("» отличается от указанной в приказе.").toString());
            }

            if (order.getEnrollmentCampaign().isNeedOriginDocForOrder()) {
                if ((preStudent.getStudentCategory().getCode().equals("1")) && (!preStudent.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())) {
                    throw new ApplicationException(new StringBuilder().append("Абитуриент «").append(entrant.getPerson().getFullFio()).append("» не сдал оригиналы документов.").toString());
                }
                Criteria criteria = getSession().createCriteria(RequestedEnrollmentDirection.class);
                criteria.createAlias("entrantRequest", "entrantRequest");
                criteria.add(Restrictions.eq("entrantRequest.entrant", entrant));
                criteria.add(Restrictions.eq("originalDocumentHandedIn", Boolean.TRUE));

                if (criteria.list().isEmpty()) {
                    throw new ApplicationException(new StringBuilder().append("Абитуриент «").append(entrant.getPerson().getFullFio()).append("» не сдал оригиналы документов ни по одному выбранному направлению подготовки (специальности).").toString());
                }
            }
        }
        if (paragraph != null) {
            for (IAbstractExtract item : paragraph.getExtractList()) {
                EnrollmentExtract enrollmentExtract = (EnrollmentExtract) item;
                if (!list.remove(enrollmentExtract.getEntity())) {
                    if ((paragraph.getGroupManager() != null) && (paragraph.getGroupManager().equals(enrollmentExtract))) {
                        paragraph.setGroupManager(null);
                        session.update(paragraph);
                    }
                    session.delete(enrollmentExtract);
                }
            }

        }
        else {
            paragraph = new EnrollmentParagraph();
            paragraph.setEnrollParagraphPerEduOrgUnit(enrollParagraphPerEduOrgUnit);
            paragraph.setOrder(order);
            paragraph.setNumber(order.getParagraphCount() + 1);
            session.save(paragraph);
        }

        int counter = -1;
        for (PreliminaryEnrollmentStudent preStudent : list) {
            EnrollmentExtract extract = extractFactory.createEnrollmentExtract(preStudent);

            extract.setEntity(preStudent);
            extract.setParagraph(paragraph);
            extract.setCreateDate(order.getCreateDate());
            extract.setNumber(Integer.valueOf(counter--));
            extract.setState((ExtractStates) getByCode(ExtractStates.class, "5"));
            extract.setCourse(course);
            extract.setGroupTitle(groupTitle);

            extract.getEntity().getRequestedEnrollmentDirection().setState((EntrantState) getByCode(EntrantState.class, "6"));
            session.update(extract.getEntity().getRequestedEnrollmentDirection());

            session.save(extract);
        }

        session.flush();
        List<EnrollmentExtract> extractList = getList(EnrollmentExtract.class, "paragraph", paragraph, new String[0]);
        Collections.sort(extractList, new Comparator<EnrollmentExtract>() {
            public int compare(EnrollmentExtract o1, EnrollmentExtract o2)
            {
                String fullFio1 = o1.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();
                String fullFio2 = o2.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard().getFullFio();
                return fullFio1.compareTo(fullFio2);
            }
        });
        counter = -1 - extractList.size();
        for (EnrollmentExtract enrollmentExtract : extractList) {
            enrollmentExtract.setNumber(Integer.valueOf(counter--));
            session.update(enrollmentExtract);
        }
        session.flush();

        counter = 1;
        EnrollmentExtract groupManager = null;
        for (EnrollmentExtract enrollmentExtract : extractList) {
            enrollmentExtract.setNumber(Integer.valueOf(counter++));
            enrollmentExtract.setGroupTitle(groupTitle);

            if (enrollmentExtract.getEntity().equals(manager)) groupManager = enrollmentExtract;

            session.update(enrollmentExtract);
        }

        paragraph.setGroupManager(groupManager);
        session.update(paragraph);

        List<EntrHighShortExtract> extractsList = getList(EntrHighShortExtract.class, EntrHighShortExtract.paragraph(), paragraph, new String[0]);
        //удалим сохраненные ранее расширение
        new DQLDeleteBuilder(EntrHighShortExtractExt.class)
                .where(DQLExpressions.in(DQLExpressions.property(EntrHighShortExtractExt.extract()), extractList));

        for (EntrHighShortExtract extract : extractsList) {
            EntrHighShortExtractExt extractExt = new EntrHighShortExtractExt();
            extractExt.setExtract(extract);
            extractExt.setEducationOrgUnit(educationOrgUnit);
            session.save(extractExt);
        }

        session.flush();
    }

}
