package ru.tandemservice.unisession.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.List;

public class SessionListDocumentPrintDAO extends ru.tandemservice.unisession.print.SessionListDocumentPrintDAO implements ISessionListDocumentPrintDAO
{
    @Override
    protected RtfDocument printSessionListDocument(SessionListDocument sessionListDocument)
    {

        RtfDocument document = super.printSessionListDocument(sessionListDocument);

        RtfInjectModifier im = getInjectModifier(sessionListDocument);
        im.modify(document);

        return document;
    }

    protected RtfInjectModifier getInjectModifier(SessionListDocument sessionListDocument) {

        RtfInjectModifier im = new RtfInjectModifier();

        List<SessionDocumentSlot> slotList = getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), sessionListDocument);

        if (slotList.size() == 0)
            return im;

        SessionDocumentSlot slot = slotList.get(0);

        String orgUnitTitle = StringUtils.isEmpty(sessionListDocument.getOrgUnit().getNominativeCaseTitle()) ?
                StringUtils.capitalize(sessionListDocument.getOrgUnit().getNominativeCaseTitle()) :
                StringUtils.capitalize(sessionListDocument.getOrgUnit().getTitle());

        EmployeePost employeePost = EmployeeManager.instance().dao().getHead(sessionListDocument.getOrgUnit());

        im.put("orgUnitTitle", orgUnitTitle);
        im.put("bookNumber", StringUtils.trimToEmpty(slot.getActualStudent().getBookNumber()));
        im.put("orgUnitHead", employeePost != null ? employeePost.getPerson().getFullFio() : "");

        return im;
    }
}
