package ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.print.personnarfu;

import org.tandemframework.core.entity.dsl.IPropertyPath;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintColumnIndex;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintMapColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.narfu.base.bo.ReportPersonNarfu.ui.Add.ReportPersonNarfuAddUI;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.util.EcReportPersonUtil;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;

import java.util.ArrayList;
import java.util.List;


public class PersonnarfuPrintBlock
        implements IReportPrintBlock
{

    private IReportPrintCheckbox _block = new ReportPrintCheckbox();
    private IReportPrintCheckbox dormitoryInfo = new ReportPrintCheckbox();
    private IReportPrintCheckbox enrolmentDirectionAdvansedInfo = new ReportPrintCheckbox();
    private IReportPrintCheckbox orderInfo = new ReportPrintCheckbox();

    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<String>();
        for (int i = 1; i <= 3; i++)
            ids.add("chPersonnarfu" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, final IReportPrintInfo printInfo)
    {

        int i = dql.addLastAggregateColumn(Person.identityCard());

        //  колонки персоны
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintColumnIndex());
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("fullFio", i, IdentityCard.fullFio()));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("sex", i, IdentityCard.sex().shortTitle()));

        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("birthDate", i, IdentityCard.birthDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("birthplace", i, IdentityCard.birthPlace()));

        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("cardType", i, IdentityCard.cardType().shortTitle()));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("seria", i, IdentityCard.seria()));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("number", i, IdentityCard.number()));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("issuanceDate", i, IdentityCard.issuanceDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("issuancePlace", i, IdentityCard.issuancePlace()));

        // место работы, должность
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("workplace", i, IdentityCard.person().workPlace()));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("workplaceposition", i, IdentityCard.person().workPlacePosition()));


        String personNarfuAlias = dql.leftJoinEntity(PersonNARFU.class, PersonNARFU.person());
        int personNarfuIndex = dql.addLastAggregateColumn(personNarfuAlias);
        //int personNarfuIndex = dql.addListAggregateColumn(personNarfuAlias);


        // инн, пенс свидетельство
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("inn", i, IdentityCard.person().innNumber()));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("pensionInsuranceNumber", i, IdentityCard.person().snilsNumber()));

        // данные о полученном образовании
        String PersonEduInstitutionAlias = dql.leftJoinEntity(PersonEduInstitution.class, PersonEduInstitution.person());
        int PersonEduInstitutionIndex = dql.addListAggregateColumn(PersonEduInstitutionAlias);
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("pei_issuanceDate", PersonEduInstitutionIndex, PersonEduInstitution.issuanceDate()));
        printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("pei_yearEnd", PersonEduInstitutionIndex, PersonEduInstitution.yearEnd()));


        // дополнительная информация
        if (dormitoryInfo.isActive()) {
            printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("needDormitoryOnReceiptPeriod", personNarfuIndex, PersonNARFU.needDormitoryOnReceiptPeriod(), YesNoFormatter.INSTANCE));
            printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintPathColumn("needDormitoryOnStudingPeriod", personNarfuIndex, PersonNARFU.person().needDormitory(), YesNoFormatter.INSTANCE));
        }

        boolean entrantsInfo = false;
        if (enrolmentDirectionAdvansedInfo.isActive()
                || getOrderInfo().isActive())
            entrantsInfo = true;

        // на большом количестве записей вылезает косяк (>1000)
        // поставлено для отладки
        if (entrantsInfo) {
            String entrantAlias = dql.leftJoinEntity(Entrant.class, Entrant.person());
            String requestAlias = dql.leftJoinEntity(entrantAlias, EntrantRequest.class, EntrantRequest.entrant());
            String directionAlias = dql.leftJoinEntity(requestAlias, RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest());

            int entrantIndex = dql.addListAggregateColumn(entrantAlias);
            int requestIndex = dql.addMapAggregateColumn(requestAlias, new int[]{
                    entrantIndex
            });
            int directionIndex = dql.addMapAggregateColumn(directionAlias, new int[]{
                    entrantIndex, requestIndex
            });

            // всегда выводим приемку и статус
            printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintListColumn("enrollmentCampaign", entrantIndex, Entrant.enrollmentCampaign().title()));
            printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintListColumn("entrantstate", entrantIndex, Entrant.state().title()));

            if (enrolmentDirectionAdvansedInfo.isActive()) {

                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("regNumber", requestIndex, EntrantRequest.regNumber()).number());
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("priority", directionIndex, RequestedEnrollmentDirection.priority()).number());
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("requestTitle", directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                    @Override
                    public String format(RequestedEnrollmentDirection requestedEnrollmentDirection)
                    {
                        String retVal = "";
                        if (requestedEnrollmentDirection == null)
                            return "";
                        else {
                            retVal = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle();
                        }
                        return retVal;
                    }
                }));

                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("developForm", directionIndex, RequestedEnrollmentDirection.compensationType().shortTitle()));
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("developCondition", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().title()));
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("developForm", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().title()));
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("developPeriod", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().title()));
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new ReportPrintMapColumn("developTech", directionIndex, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developTech().title()));
            }

            // данные приказа о зачислении
            if (getOrderInfo().isActive()) {
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new EnrollmentExtractReportPrintMapColumn(printInfo, "enrollmentOrderPreNumber", entrantIndex, directionIndex, EnrollmentExtract.paragraph().order().number(), null));
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new EnrollmentExtractReportPrintMapColumn(printInfo, "enrollmentOrderPreDate", entrantIndex, directionIndex, EnrollmentExtract.paragraph().order().commitDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
                printInfo.addPrintColumn(ReportPersonNarfuAddUI.PERSONNARFU_SCHEET, new EnrollmentExtractReportPrintMapColumn(printInfo, "enrollmentOrderTypePre", entrantIndex, directionIndex, new PropertyPath(EnrollmentExtract.paragraph().order().s() + ".type.shortTitle"), null));
            }
        }
    }

    public IReportPrintCheckbox getDormitoryInfo() {
        return dormitoryInfo;
    }

    public IReportPrintCheckbox getEnrolmentDirectionAdvansedInfo() {
        return enrolmentDirectionAdvansedInfo;
    }

    public IReportPrintCheckbox getBlock()
    {
        return _block;
    }


    public void setOrderInfo(IReportPrintCheckbox orderInfo)
    {
        this.orderInfo = orderInfo;
    }

    public IReportPrintCheckbox getOrderInfo() {
        return orderInfo;
    }


    private static class EnrollmentExtractReportPrintMapColumn extends ReportPrintMapColumn {
        private IReportPrintInfo _printInfo;
        private int _entrantIndex;

        public EnrollmentExtractReportPrintMapColumn(final IReportPrintInfo printInfo, String name, int entrantIndex, int directionIndex, final IPropertyPath extractPath, final IFormatter formatter)
        {
            super(name, directionIndex, null, new IFormatter<RequestedEnrollmentDirection>() {
                @Override
                public String format(RequestedEnrollmentDirection source)
                {
                    if (source == null) return null;
                    EnrollmentExtract extract = EcReportPersonUtil.getEnrollmentExtract(printInfo, source);
                    if (extract == null) return null;
                    Object obj = extract.getProperty(extractPath);
                    if (obj == null) return null;
                    return formatter == null ? obj.toString() : formatter.format(obj);
                }
            });

            _printInfo = printInfo;
            _entrantIndex = entrantIndex;
        }

        @Override
        public void prefetch(List<Object[]> rows)
        {
            EcReportPersonUtil.prefetchEnrollmentExtractMap(_printInfo, rows, _entrantIndex);
        }
    }

}
