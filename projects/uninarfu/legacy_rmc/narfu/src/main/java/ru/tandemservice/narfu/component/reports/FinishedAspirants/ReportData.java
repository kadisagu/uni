package ru.tandemservice.narfu.component.reports.FinishedAspirants;


import jxl.write.WritableWorkbook;
import ru.tandemservice.narfu.component.reports.FinishedAspirants.Add.Model;
import ru.tandemservice.narfu.uniec.entity.report.FinishedAspirantsReport;

import java.io.ByteArrayOutputStream;

public class ReportData {
    protected WritableWorkbook workbook;
    protected ByteArrayOutputStream workbookStream;
    protected FinishedAspirantsReport report;
    Model model;


    public WritableWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(WritableWorkbook workbook) {
        this.workbook = workbook;
    }

    public ByteArrayOutputStream getWorkbookStream() {
        return workbookStream;
    }

    public void setWorkbookStream(ByteArrayOutputStream workbookStream) {
        this.workbookStream = workbookStream;
    }

    public FinishedAspirantsReport getReport() {
        return report;
    }

    public void setReport(FinishedAspirantsReport report) {
        this.report = report;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }


}
