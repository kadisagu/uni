package ru.tandemservice.narfu.component.reports.JournalAcademVacations.Add;

import org.tandemframework.core.component.State;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentrmc.entity.catalog.RepresentationReason;
import ru.tandemservice.narfu.entity.NarfuReportJournalAcademVacations;
import ru.tandemservice.uni.entity.catalog.*;

import java.util.Date;
import java.util.List;

@State(keys = "orgUnitId", bindings = "orgUnitId")
public class Model {

    private Long orgUnitId;
    private OrgUnit orgUnit;
    private CommonPostfixPermissionModel secModel;
    private NarfuReportJournalAcademVacations report = new NarfuReportJournalAcademVacations();

    private boolean orgUnitActive;
    private ISelectModel orgUnitModel;
    private List<OrgUnit> orgUnitList;

    private boolean developFormActive;
    private ISelectModel developFormModel;
    private List<DevelopForm> developFormList;

    private boolean educationLevelsActive;
    private ISelectModel educationLevelsModel;
    private List<EducationLevels> educationLevelsList;

    private boolean courseActive;
    private ISelectModel courseModel;
    private List<Course> courseList;

    private boolean compensationTypeActive;
    private ISelectModel compensationTypeModel;
    private List<CompensationType> compensationTypeList;

    private boolean reasonActive;
    private ISelectModel reasonModel;
    private List<RepresentationReason> reasonList;

    private boolean developPeriodActive;
    private ISelectModel developPeriodModel;
    private List<DevelopPeriod> developPeriodList;

    private boolean startActive;
    private Date startDateFrom;
    private Date startDateTo;

    private boolean endActive;
    private Date endDateFrom;
    private Date endDateTo;


    public NarfuReportJournalAcademVacations getReport() {
        return report;
    }

    public void setReport(NarfuReportJournalAcademVacations report) {
        this.report = report;
    }

    public Object getSecuredObject() {
        return null == getOrgUnit() ? SecurityRuntime.getInstance().getCommonSecurityObject() : getOrgUnit();
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public CommonPostfixPermissionModel getSecModel() {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel) {
        this.secModel = secModel;
    }

    public boolean isReasonActive() {
        return reasonActive;
    }

    public void setReasonActive(boolean reasonActive) {
        this.reasonActive = reasonActive;
    }

    public ISelectModel getReasonModel() {
        return reasonModel;
    }

    public void setReasonModel(ISelectModel reasonModel) {
        this.reasonModel = reasonModel;
    }

    public List<RepresentationReason> getReasonList() {
        return reasonList;
    }

    public void setReasonList(List<RepresentationReason> reasonList) {
        this.reasonList = reasonList;
    }

    public boolean isOrgUnitActive() {
        return orgUnitActive;
    }

    public void setOrgUnitActive(boolean orgUnitActive) {
        this.orgUnitActive = orgUnitActive;
    }

    public ISelectModel getOrgUnitModel() {
        return orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel) {
        this.orgUnitModel = orgUnitModel;
    }

    public List<OrgUnit> getOrgUnitList() {
        return orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList) {
        this.orgUnitList = orgUnitList;
    }

    public boolean isDevelopFormActive() {
        return developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive) {
        this.developFormActive = developFormActive;
    }

    public ISelectModel getDevelopFormModel() {
        return developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public List<DevelopForm> getDevelopFormList() {
        return developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList) {
        this.developFormList = developFormList;
    }

    public boolean isEducationLevelsActive() {
        return educationLevelsActive;
    }

    public void setEducationLevelsActive(boolean educationLevelsActive) {
        this.educationLevelsActive = educationLevelsActive;
    }

    public ISelectModel getEducationLevelsModel() {
        return educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel) {
        this.educationLevelsModel = educationLevelsModel;
    }

    public List<EducationLevels> getEducationLevelsList() {
        return educationLevelsList;
    }

    public void setEducationLevelsList(List<EducationLevels> educationLevelsList) {
        this.educationLevelsList = educationLevelsList;
    }

    public boolean isCourseActive() {
        return courseActive;
    }

    public void setCourseActive(boolean courseActive) {
        this.courseActive = courseActive;
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    public void setCourseList(List<Course> courseList) {
        this.courseList = courseList;
    }

    public boolean isCompensationTypeActive() {
        return compensationTypeActive;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive) {
        this.compensationTypeActive = compensationTypeActive;
    }

    public ISelectModel getCompensationTypeModel() {
        return compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel) {
        this.compensationTypeModel = compensationTypeModel;
    }

    public List<CompensationType> getCompensationTypeList() {
        return compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList) {
        this.compensationTypeList = compensationTypeList;
    }

    public boolean isDevelopPeriodActive() {
        return developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive) {
        this.developPeriodActive = developPeriodActive;
    }

    public ISelectModel getDevelopPeriodModel() {
        return developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel) {
        this.developPeriodModel = developPeriodModel;
    }

    public List<DevelopPeriod> getDevelopPeriodList() {
        return developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList) {
        this.developPeriodList = developPeriodList;
    }

    public boolean isStartActive() {
        return startActive;
    }

    public void setStartActive(boolean startActive) {
        this.startActive = startActive;
    }

    public Date getStartDateFrom() {
        return startDateFrom;
    }

    public void setStartDateFrom(Date startDateFrom) {
        this.startDateFrom = startDateFrom;
    }

    public Date getStartDateTo() {
        return startDateTo;
    }

    public void setStartDateTo(Date startDateTo) {
        this.startDateTo = startDateTo;
    }

    public boolean isEndActive() {
        return endActive;
    }

    public void setEndActive(boolean endActive) {
        this.endActive = endActive;
    }

    public Date getEndDateFrom() {
        return endDateFrom;
    }

    public void setEndDateFrom(Date endDateFrom) {
        this.endDateFrom = endDateFrom;
    }

    public Date getEndDateTo() {
        return endDateTo;
    }

    public void setEndDateTo(Date endDateTo) {
        this.endDateTo = endDateTo;
    }


}
