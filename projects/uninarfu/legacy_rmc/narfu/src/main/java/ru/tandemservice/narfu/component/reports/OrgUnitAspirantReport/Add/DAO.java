package ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.Add;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.ReportData;
import ru.tandemservice.narfu.uniec.entity.report.OrgUnitAspirantReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantOriginalDocumentRelation;
import ru.tandemservice.uniedu.base.bo.EducationYear.EducationYearManager;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.*;

@SuppressWarnings("deprecation")
public class DAO extends UniDao<Model> implements IDAO {
    public static final String TEMPLATE_NAME = "narfuOrgUnitAspirantReport";
    public static final String RUSSIA_ROW = "1";
    public static final String TARGET_ROW = "2";
    public static final String CIS_ROW = "3";
    public static final String OTHER_ROW = "4";
    public static final String TOTAL_RUSSIA_ROW = "5";
    public static final String TOTAL_ROW = "6";
    public static final List<Integer> CIS_CODES = Arrays.asList(31, 51, 112, 398, 417, 498, 0, 762, 795, 860, 804);
    public static final List<String> NARFU_EDU_INSTITUTION_CODES = Arrays.asList("137", "18");

    @Override
    public void prepare(final Model model) {

        OrgUnit orgUnit = model.getOrgUnitId() != null ? getNotNull(OrgUnit.class, model.getOrgUnitId()) : null;
        model.setOrgUnit(orgUnit);

        model.setEnrollmentCampaignModel(new LazySimpleSelectModel<>(EnrollmentCampaign.class));
        model.setEducationLevelsHighSchoolModel(new DQLFullCheckSelectModel("fullTitle") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationOrgUnit.class, "e");
                if (model.getOrgUnit() != null)
                    dql.where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().fromAlias("e")), DQLExpressions.value(model.getOrgUnit())));
                dql.column(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fromAlias("e")));
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, alias);

                //     dql.order(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().fullTitle().fromAlias("e")));
                builder.where(DQLExpressions.in(DQLExpressions.property(alias), dql.buildQuery()));
                builder.order(DQLExpressions.property(EducationLevelsHighSchool.fullTitle().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle().s(), filter);
                return builder;
            }
        });


    }

    @Override
    public OrgUnitAspirantReport createReport(Model model) {
        //  OrgUnitAspirantReport report = new OrgUnitAspirantReport();
        ReportData reportData = new ReportData();

//        DQLSelectBuilder dql = new DQLSelectBuilder();
//        dql.fromEntity(NarfuReportStudentsByUGS.class, "report");
//        dql.addColumn(DQLFunctions.count(NarfuReportStudentsByUGS.id().fromAlias("report").s()));
//        Long num = (Long)dql.createStatement(getSession()).list().get(0);
//
//
//
//        report.setNumber(num.intValue() + 1);
        model.getReport().setFormingDate(new Date());
        //«Отчет № 1 от 09.11.2012
//        String title = new StringBuilder()
//                .append("Отчет № ")
//                .append(String.valueOf(report.getNumber()))
//                .append(" от ")
//                .append(DateFormattingUtil.STANDARD_DATE_FORMAT.format(report.getFormingDate()))
//                .toString();
//        report.setTitle(title);

        try {
            reportData.setReport(model.getReport());
            reportData.setModel(model);
            loadTemplate(reportData);
            fillReport(reportData, model);

            reportData.getWorkbook().write();
            reportData.getWorkbook().close();

            DatabaseFile reportFile = new DatabaseFile();
            reportFile.setContent(reportData.getWorkbookStream().toByteArray());
            reportFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
            reportFile.setFilename("Report.xls");

            save(reportFile);
            model.getReport().setContent(reportFile);
            save(model.getReport());
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationException(e.getMessage(), e);
        }

        return model.getReport();
    }

    protected void fillReport(ReportData reportData, Model model) throws Exception {

        List<Entrant> entrantList = getEntrantList(model);
        Map<String, Integer[]> sortedMap = sortEntrantsByCitizenship(entrantList);
        fillPlanCount(sortedMap, model);
        print(reportData, sortedMap);

    }

    private void fillPlanCount(Map<String, Integer[]> sortedMap, Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e");
        dql.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("e")), DQLExpressions.value(model.getReport().getEnrollmentCampaign())));
        if (model.getOrgUnit() != null)
            dql.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("e")), DQLExpressions.value(model.getOrgUnit())));
        if (model.getReport().getEducationLevelHighSchool() != null)
            dql.where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().fromAlias("e")), DQLExpressions.value(model.getReport().getEducationLevelHighSchool())));
        List<EnrollmentDirection> enrollmentDirections = getList(dql);
        for (EnrollmentDirection direction : enrollmentDirections) {
            if (direction.getMinisterialPlan() != null) {
                Integer[] mass = sortedMap.get(RUSSIA_ROW);
                mass[0] += direction.getMinisterialPlan();
                if (direction.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM)) {
                    mass[1] += direction.getMinisterialPlan();
                }
                sortedMap.put(RUSSIA_ROW, mass);
            }
        }
        Integer[] totalRussiaRow = sortedMap.get(TOTAL_RUSSIA_ROW);
        Integer[] totalRow = sortedMap.get(TOTAL_ROW);
        for (int i = 0; i < totalRussiaRow.length; i++) {
            for (Map.Entry<String, Integer[]> entry : sortedMap.entrySet()) {
                if (entry.getKey().equals(RUSSIA_ROW) || entry.getKey().equals(TARGET_ROW))
                    totalRussiaRow[i] += entry.getValue()[i];
                if ((i > 1 && i < 6) && (
                        entry.getKey().equals(CIS_ROW) ||
                                entry.getKey().equals(OTHER_ROW)
                ))
                    totalRow[i] += entry.getValue()[i];
            }

        }
        sortedMap.put(TOTAL_RUSSIA_ROW, totalRussiaRow);
        sortedMap.put(TOTAL_ROW, totalRow);

    }

    private void print(ReportData reportData, Map<String, Integer[]> sortedMap)
    {

        int firstRow = 7;
        for (Map.Entry<String, Integer[]> entry : sortedMap.entrySet()) {
            int firstColumn = 2;
            try {
                appendRow(firstColumn, firstRow, entry, reportData.getWorkbook().getSheet(0));
                firstRow++;
            }
            catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    Map<String, Integer[]> sortEntrantsByCitizenship(List<Entrant> entrantList)
    {
        Map<String, Integer[]> result = new TreeMap<>();
        result.put(RUSSIA_ROW, new Integer[9]);
        Arrays.fill(result.get(RUSSIA_ROW), 0);
        result.put(TARGET_ROW, new Integer[9]);
        Arrays.fill(result.get(TARGET_ROW), 0);
        result.put(CIS_ROW, new Integer[9]);
        Arrays.fill(result.get(CIS_ROW), 0);
        result.put(OTHER_ROW, new Integer[9]);
        Arrays.fill(result.get(OTHER_ROW), 0);
        result.put(TOTAL_RUSSIA_ROW, new Integer[9]);
        Arrays.fill(result.get(TOTAL_RUSSIA_ROW), 0);
        result.put(TOTAL_ROW, new Integer[9]);
        Arrays.fill(result.get(TOTAL_ROW), 0);


        for (Entrant entrant : entrantList) {
            if (entrant.getPerson().getIdentityCard().getCitizenship().getCode() == 0) {
                addAspirant(result.get(RUSSIA_ROW), entrant);
            }
            if (CIS_CODES.contains(entrant.getPerson().getIdentityCard().getCitizenship().getCode())) {
                addAspirant(result.get(CIS_ROW), entrant);
                continue;
            }
            addAspirant(result.get(OTHER_ROW), entrant);
        }
        return result;

    }

    private void addAspirant(Integer[] mass, Entrant entrant)
    {
        EducationOrgUnit educationOrgUnit = getEntrantEducationOrgUnit(entrant);
        List<String> eduStages = CommonBaseEntityUtil.getPropertiesList(getEduInstitutionList(entrant), PersonEduInstitution.educationLevelStage().code());
        List<PersonEduInstitution> personEduInstitutions = getCurrentYearEduInstitution(entrant);
        mass[2]++;
        if (educationOrgUnit.getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            mass[3]++;
        if (!entrant.getPerson().isMale())
            mass[4]++;
        if (educationOrgUnit.getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM) && !entrant.getPerson().isMale())
            mass[5]++;
        if (eduStages.contains(EducationLevelStageCodes.MAGISTRATURA))
            mass[6]++;

        if (!CollectionUtils.isEmpty(personEduInstitutions)) {
            for (PersonEduInstitution item : personEduInstitutions) {
                if (item.getEduInstitution().getTitle().contains("САФУ")
                        || item.getEduInstitution().getTitle().contains("Северный(Арктический)"))
                {
                    mass[7]++;
                    return;
                }
            }
            mass[8]++;
        }


    }

    private EducationOrgUnit getEntrantEducationOrgUnit(Entrant entrant)
    {
        return get(EntrantOriginalDocumentRelation.class, EntrantOriginalDocumentRelation.entrant().s(), entrant).getRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit();
    }

    private List<PersonEduInstitution> getEduInstitutionList(Entrant entrant)
    {
        return getList(PersonEduInstitution.class, PersonEduInstitution.person().s(), entrant.getPerson());
    }

    private List<PersonEduInstitution> getCurrentYearEduInstitution(Entrant entrant)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PersonEduInstitution.class, "e");
        EducationYear current = EducationYearManager.instance().dao().getCurrent();
        builder.where(DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.yearEnd().fromAlias("e")), DQLExpressions.value(current.getIntValue())));
        builder.where(DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.eduInstitutionKind().code().fromAlias("e")), DQLExpressions.value("17.3")));
        builder.where(DQLExpressions.eq(DQLExpressions.property(PersonEduInstitution.person().fromAlias("e")), DQLExpressions.value(entrant.getPerson())));
        return getList(builder);
    }

    List<Entrant> getEntrantList(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantOriginalDocumentRelation.class, "e");
        dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantOriginalDocumentRelation.entrant().state().code().fromAlias("e")), DQLExpressions.value(EntrantStateCodes.ENROLED)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().studentCategory().code().fromAlias("e")), DQLExpressions.value(StudentCategoryCodes.STUDENT_CATEGORY_ASPIRANT)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().compensationType().code().fromAlias("e")), DQLExpressions.value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantOriginalDocumentRelation.entrant().enrollmentCampaign().fromAlias("e")), DQLExpressions.value(model.getReport().getEnrollmentCampaign())));
        if (model.getReport().getEducationLevelHighSchool() != null)
            dql.where(DQLExpressions.eq(DQLExpressions.property(EntrantOriginalDocumentRelation.requestedEnrollmentDirection().enrollmentDirection().educationOrgUnit().educationLevelHighSchool().fromAlias("e")), DQLExpressions.value(model.getReport().getEducationLevelHighSchool())));
        dql.column(DQLExpressions.property(EntrantOriginalDocumentRelation.entrant().fromAlias("e")));
        return getList(dql);
    }

    private void appendRow(int column, int row, Map.Entry<String, Integer[]> rowData, WritableSheet sheet) throws Exception {

        WritableFont font = new WritableFont(WritableFont.TIMES, 9);
        WritableCellFormat format = new WritableCellFormat(font);
        format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        format.setAlignment(Alignment.CENTRE);
        format.setWrap(true);
        format.setBorder(Border.ALL, BorderLineStyle.THIN);
        for (int i = 0; i < rowData.getValue().length; i++) {
            if ((i < 2 || i > 5) &&
                    (rowData.getKey().equals(CIS_ROW) ||
                            rowData.getKey().equals(TOTAL_ROW) ||
                            rowData.getKey().equals(OTHER_ROW))
                    )
                continue;
            sheet.addCell(new jxl.write.Number(column + i, row, rowData.getValue()[i], format));
        }


    }

    protected void loadTemplate(ReportData reportData) throws Exception {
        TemplateDocument template = getCatalogItem(TemplateDocument.class, TEMPLATE_NAME);
        ByteArrayInputStream in = new ByteArrayInputStream(template.getContent());

        Workbook templateWBook = Workbook.getWorkbook(in);
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);

        ByteArrayOutputStream wbOS = new ByteArrayOutputStream();
        WritableWorkbook writableWorkbook = Workbook.createWorkbook(wbOS, templateWBook, ws);

        reportData.setWorkbookStream(wbOS);
        reportData.setWorkbook(writableWorkbook);
    }

//	public RtfDocument loadTemplate() {
//		TemplateDocument templateDocument = getCatalogItem(TemplateDocument.class, "studentArchiveReport");
//		return new RtfReader().read(templateDocument.getContent());
//	}
}
