package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt;
import ru.tandemservice.uniec.entity.entrant.EntrantStateExamCertificate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сертификата ЕГЭ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantStateExamCertificateExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt";
    public static final String ENTITY_NAME = "entrantStateExamCertificateExt";
    public static final int VERSION_HASH = 1945412895;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT_STATE_EXAM_CERTIFICATE = "entrantStateExamCertificate";
    public static final String P_EXAM_PLACE = "examPlace";

    private EntrantStateExamCertificate _entrantStateExamCertificate;     // Сертификат ЕГЭ
    private String _examPlace;     // Место сдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     */
    @NotNull
    public EntrantStateExamCertificate getEntrantStateExamCertificate()
    {
        return _entrantStateExamCertificate;
    }

    /**
     * @param entrantStateExamCertificate Сертификат ЕГЭ. Свойство не может быть null.
     */
    public void setEntrantStateExamCertificate(EntrantStateExamCertificate entrantStateExamCertificate)
    {
        dirty(_entrantStateExamCertificate, entrantStateExamCertificate);
        _entrantStateExamCertificate = entrantStateExamCertificate;
    }

    /**
     * @return Место сдачи.
     */
    @Length(max=255)
    public String getExamPlace()
    {
        return _examPlace;
    }

    /**
     * @param examPlace Место сдачи.
     */
    public void setExamPlace(String examPlace)
    {
        dirty(_examPlace, examPlace);
        _examPlace = examPlace;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantStateExamCertificateExtGen)
        {
            setEntrantStateExamCertificate(((EntrantStateExamCertificateExt)another).getEntrantStateExamCertificate());
            setExamPlace(((EntrantStateExamCertificateExt)another).getExamPlace());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantStateExamCertificateExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantStateExamCertificateExt.class;
        }

        public T newInstance()
        {
            return (T) new EntrantStateExamCertificateExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrantStateExamCertificate":
                    return obj.getEntrantStateExamCertificate();
                case "examPlace":
                    return obj.getExamPlace();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrantStateExamCertificate":
                    obj.setEntrantStateExamCertificate((EntrantStateExamCertificate) value);
                    return;
                case "examPlace":
                    obj.setExamPlace((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrantStateExamCertificate":
                        return true;
                case "examPlace":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrantStateExamCertificate":
                    return true;
                case "examPlace":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrantStateExamCertificate":
                    return EntrantStateExamCertificate.class;
                case "examPlace":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantStateExamCertificateExt> _dslPath = new Path<EntrantStateExamCertificateExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantStateExamCertificateExt");
    }
            

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt#getEntrantStateExamCertificate()
     */
    public static EntrantStateExamCertificate.Path<EntrantStateExamCertificate> entrantStateExamCertificate()
    {
        return _dslPath.entrantStateExamCertificate();
    }

    /**
     * @return Место сдачи.
     * @see ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt#getExamPlace()
     */
    public static PropertyPath<String> examPlace()
    {
        return _dslPath.examPlace();
    }

    public static class Path<E extends EntrantStateExamCertificateExt> extends EntityPath<E>
    {
        private EntrantStateExamCertificate.Path<EntrantStateExamCertificate> _entrantStateExamCertificate;
        private PropertyPath<String> _examPlace;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сертификат ЕГЭ. Свойство не может быть null.
     * @see ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt#getEntrantStateExamCertificate()
     */
        public EntrantStateExamCertificate.Path<EntrantStateExamCertificate> entrantStateExamCertificate()
        {
            if(_entrantStateExamCertificate == null )
                _entrantStateExamCertificate = new EntrantStateExamCertificate.Path<EntrantStateExamCertificate>(L_ENTRANT_STATE_EXAM_CERTIFICATE, this);
            return _entrantStateExamCertificate;
        }

    /**
     * @return Место сдачи.
     * @see ru.tandemservice.narfu.entity.EntrantStateExamCertificateExt#getExamPlace()
     */
        public PropertyPath<String> examPlace()
        {
            if(_examPlace == null )
                _examPlace = new PropertyPath<String>(EntrantStateExamCertificateExtGen.P_EXAM_PLACE, this);
            return _examPlace;
        }

        public Class getEntityClass()
        {
            return EntrantStateExamCertificateExt.class;
        }

        public String getEntityName()
        {
            return "entrantStateExamCertificateExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
