package ru.tandemservice.narfu.base.ext.Person.ui.DormitoryEdit;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.Model
{

    private boolean _needDormitoryOnReceiptPeriod = false;


    public boolean isNeedDormitoryOnReceiptPeriod()
    {
        return _needDormitoryOnReceiptPeriod;
    }

    public void setNeedDormitoryOnReceiptPeriod(boolean needDormitoryOnReceiptPeriod)
    {
        this._needDormitoryOnReceiptPeriod = needDormitoryOnReceiptPeriod;
    }
}
