package ru.tandemservice.narfu.component.entrant.EntrantRequestTab;

import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;

import java.util.List;

public interface IDAO extends ru.tandemservice.uniecrmc.component.entrant.EntrantRequestTab.IDAO {
    public abstract List<String[]> getEnrollmentDocuments(List<EntrantEnrollmentDocument> documents, PersonEduInstitution lastPersonEduInstitution, Entrant entrant);

    public void updateEntrantPersonalNumber(Model model, boolean isChangePriority);
}
