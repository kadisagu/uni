package ru.tandemservice.narfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.narfu.entity.PersonBenefitExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение льготы для персоны
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonBenefitExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.narfu.entity.PersonBenefitExt";
    public static final String ENTITY_NAME = "personBenefitExt";
    public static final int VERSION_HASH = 1697717696;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON_BENEFIT = "personBenefit";
    public static final String P_CAUSE_DISABILITY = "causeDisability";
    public static final String P_TITLE_WITH_CAUSE_DISABILITY = "titleWithCauseDisability";

    private PersonBenefit _personBenefit;     // Льготы для персоны
    private String _causeDisability;     // Причина инвалидности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Льготы для персоны. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PersonBenefit getPersonBenefit()
    {
        return _personBenefit;
    }

    /**
     * @param personBenefit Льготы для персоны. Свойство не может быть null и должно быть уникальным.
     */
    public void setPersonBenefit(PersonBenefit personBenefit)
    {
        dirty(_personBenefit, personBenefit);
        _personBenefit = personBenefit;
    }

    /**
     * @return Причина инвалидности.
     */
    @Length(max=255)
    public String getCauseDisability()
    {
        return _causeDisability;
    }

    /**
     * @param causeDisability Причина инвалидности.
     */
    public void setCauseDisability(String causeDisability)
    {
        dirty(_causeDisability, causeDisability);
        _causeDisability = causeDisability;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonBenefitExtGen)
        {
            setPersonBenefit(((PersonBenefitExt)another).getPersonBenefit());
            setCauseDisability(((PersonBenefitExt)another).getCauseDisability());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonBenefitExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonBenefitExt.class;
        }

        public T newInstance()
        {
            return (T) new PersonBenefitExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "personBenefit":
                    return obj.getPersonBenefit();
                case "causeDisability":
                    return obj.getCauseDisability();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "personBenefit":
                    obj.setPersonBenefit((PersonBenefit) value);
                    return;
                case "causeDisability":
                    obj.setCauseDisability((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "personBenefit":
                        return true;
                case "causeDisability":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "personBenefit":
                    return true;
                case "causeDisability":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "personBenefit":
                    return PersonBenefit.class;
                case "causeDisability":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonBenefitExt> _dslPath = new Path<PersonBenefitExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonBenefitExt");
    }
            

    /**
     * @return Льготы для персоны. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.PersonBenefitExt#getPersonBenefit()
     */
    public static PersonBenefit.Path<PersonBenefit> personBenefit()
    {
        return _dslPath.personBenefit();
    }

    /**
     * @return Причина инвалидности.
     * @see ru.tandemservice.narfu.entity.PersonBenefitExt#getCauseDisability()
     */
    public static PropertyPath<String> causeDisability()
    {
        return _dslPath.causeDisability();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.narfu.entity.PersonBenefitExt#getTitleWithCauseDisability()
     */
    public static SupportedPropertyPath<String> titleWithCauseDisability()
    {
        return _dslPath.titleWithCauseDisability();
    }

    public static class Path<E extends PersonBenefitExt> extends EntityPath<E>
    {
        private PersonBenefit.Path<PersonBenefit> _personBenefit;
        private PropertyPath<String> _causeDisability;
        private SupportedPropertyPath<String> _titleWithCauseDisability;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Льготы для персоны. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.narfu.entity.PersonBenefitExt#getPersonBenefit()
     */
        public PersonBenefit.Path<PersonBenefit> personBenefit()
        {
            if(_personBenefit == null )
                _personBenefit = new PersonBenefit.Path<PersonBenefit>(L_PERSON_BENEFIT, this);
            return _personBenefit;
        }

    /**
     * @return Причина инвалидности.
     * @see ru.tandemservice.narfu.entity.PersonBenefitExt#getCauseDisability()
     */
        public PropertyPath<String> causeDisability()
        {
            if(_causeDisability == null )
                _causeDisability = new PropertyPath<String>(PersonBenefitExtGen.P_CAUSE_DISABILITY, this);
            return _causeDisability;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.narfu.entity.PersonBenefitExt#getTitleWithCauseDisability()
     */
        public SupportedPropertyPath<String> titleWithCauseDisability()
        {
            if(_titleWithCauseDisability == null )
                _titleWithCauseDisability = new SupportedPropertyPath<String>(PersonBenefitExtGen.P_TITLE_WITH_CAUSE_DISABILITY, this);
            return _titleWithCauseDisability;
        }

        public Class getEntityClass()
        {
            return PersonBenefitExt.class;
        }

        public String getEntityName()
        {
            return "personBenefitExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitleWithCauseDisability();
}
