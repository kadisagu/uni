package ru.tandemservice.movestudentbasermc.movestudent.listextract;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.employee.Student;

public interface IStudentListParagraphPrintFormatterRMC {
    String formatSingleStudent(ListStudentExtract enrollmentExtract, Student student, int extractNumber);
}
