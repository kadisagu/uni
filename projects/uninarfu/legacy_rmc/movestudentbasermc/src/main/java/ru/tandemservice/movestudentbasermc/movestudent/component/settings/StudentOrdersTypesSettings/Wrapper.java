package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings;

import org.tandemframework.core.bean.IFastBean;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT;

public class Wrapper extends StdExtractT2OrdersT implements IEntity {

    @SuppressWarnings("rawtypes")
    @Override
    public IFastBean getFastBean()
    {
        return null;
    }

    @Override
    public Long getId()
    {
        return super.getId();
    }

    ;

    @Override
    public void setId(Long arg0)
    {
        super.setId(arg0);
    }

    ;

    private StdExtractT2OrdersT extractT2OrdersT;

    public Wrapper(StdExtractT2OrdersT _extractT2OrdersT)
    {
        super.setId(_extractT2OrdersT.getId());
        super.update(_extractT2OrdersT);
        this.extractT2OrdersT = _extractT2OrdersT;
    }

    public StdExtractT2OrdersT getExtractT2OrdersT() {
        return extractT2OrdersT;
    }

    public String getOrderType()
    {
        return extractT2OrdersT.getStudentOrdersTypes().getShortTitle();
    }

    public String getOrderName()
    {
        return extractT2OrdersT.getStudentExtractType().getTitle();
    }

}
