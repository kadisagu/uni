package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings;


import org.hibernate.Session;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StudentOrdersTypes;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {

        model.setOrderTypesList(new LazySimpleSelectModel<>(getList(StudentOrdersTypes.class, StudentOrdersTypes.shortTitle().s())));

    }


    @SuppressWarnings("unchecked")
    public <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource)
    {

        // StudentOrdersTypes st;

        EntityOrder eo = dataSource.getEntityOrder();

        List<Wrapper> itemList = model.getWraper(eo, getSession());

        int count = Math.max(itemList.size() + 1, 2);
        dataSource.setTotalSize(count);
        dataSource.setCountRow(count);
        dataSource.createPage((List<T>) itemList);

    }


    public void restoreDefaults(Model model)
    {
        model.getSettings().set("StudentOrdersTypes", null);

    }

    public void validateDelete(List<Long> entityIds, ErrorCollector errors)
    {
        Session session = getSession();
        for (Long id : entityIds) {
            StdExtractT2OrdersT ent = (StdExtractT2OrdersT) session.get(StdExtractT2OrdersT.class, id);

            if (ent.isIsSystem())
                errors.add("Системную запись удалить нельзя");
        }


    }

    @Override
    public void update(Model model)
    {
        if (model.getDroppedID() != null) {
            Session session = getSession();

            StdExtractT2OrdersT obj = (StdExtractT2OrdersT) session.get(StdExtractT2OrdersT.class, model.getDroppedID());

            if (obj != null)
                session.delete(obj);
        }
    }

}
