package ru.tandemservice.movestudentbasermc.component.student.OrderListPub.Add;

import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model)
    {

        if (model.getOrderListId() != null) {
            // мы в режиме редактирования
            OrderList _ol = getNotNull(OrderList.class, model.getOrderListId());

            model.setStudent(_ol.getStudent());
            model.setSecond(_ol.getType());
            model.setOrderDate(_ol.getOrderDate());
            model.setOrderNumber(_ol.getOrderNumber());
            model.setOrderDsk(_ol.getOrderDsk());
            model.setOrderDateStart(_ol.getOrderDateStart());
            model.setOrderDateStop(_ol.getOrderDateStop());
            model.setOrderText(_ol.getOrderText());
            if (_ol.getFile() != null)
                model.setFile(_ol.getFile());
        }
        else {
            model.setStudent(getNotNull(Student.class, model.getStudent().getId()));
        }

        if (model.getFile() == null)
            model.setEditFile(false);
        else
            model.setEditFile(true);

        prepareFile(model);

        MQBuilder builder = new MQBuilder(StudentExtractType.ENTITY_CLASS, "s");
        builder.add(MQExpression.eq("s", StudentExtractType.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.isNull("s", StudentExtractType.L_PARENT + "." + StudentExtractType.L_PARENT));
        List<StudentExtractType> list = builder.getResultList(getSession());

        Collections.sort(list, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

        model.setSecondHierarchyList(HierarchyUtil.listHierarchyNodesWithParents(list, false));
    }


    @Override
    public void update(Model model)
    {

        Session session = getSession();

        prepareFile(model);
        OrderList _order;

        if (model.getOrderListId() != null) {
            _order = getNotNull(OrderList.class, model.getOrderListId());

            if (_order == null)
                return;
        }
        else
            _order = new OrderList();

        _order.setOrderNumber(model.getOrderNumber());
        _order.setOrderDate(model.getOrderDate());
        _order.setOrderDsk(model.getOrderDsk());
        _order.setOrderDateStart(model.getOrderDateStart());
        _order.setOrderDateStop(model.getOrderDateStop());

        _order.setStudent(model.getStudent());

        IEntity ent = model.getSecond();
        StudentExtractType et = getNotNull(StudentExtractType.class, ent.getId());
        _order.setType(et);

        if (model.getUploadFile() != null) {

            InputStream input = model.getUploadFile().getStream();
            try {
                IOUtils.toByteArray(input);
            }
            catch (IOException e) {
                throw new ApplicationException("Ошибка загрузки файла");
            }
            if (_order.getFile() != null) {
                DatabaseFile file = _order.getFile();
                getSession().delete(file);
            }
            _order.setFile(model.getFile());

        }

        _order.setOrderText(model.getOrderText());


        session.saveOrUpdate(_order);
    }

    private void prepareFile(Model model)
    {
        if (model.getUploadFile() != null) {
            InputStream input = model.getUploadFile().getStream();
            byte[] content;
            try {
                content = IOUtils.toByteArray(input);
            }
            catch (IOException e) {
                throw new ApplicationException("Ошибка загрузки файла");
            }
            if (model.getFile() != null) {
                DatabaseFile file = model.getFile();
                getSession().delete(file);
            }
            model.setFile(new DatabaseFile());
            model.getFile().setContent(content);
            model.getFile().setFilename(model.getUploadFile().getFileName());
            save(model.getFile());
        }
    }

    @Override
    public void deleteFile(Model model)
    {
        DatabaseFile file = model.getFile();
        if (file == null)
            return;

        model.setFile(null);

        if (model.getOrderListId() != null) {
            OrderList order = getNotNull(OrderList.class, model.getOrderListId());

            if (order == null)
                return;
            order.setFile(null);
            saveOrUpdate(order);
        }

        delete(file);
    }


}