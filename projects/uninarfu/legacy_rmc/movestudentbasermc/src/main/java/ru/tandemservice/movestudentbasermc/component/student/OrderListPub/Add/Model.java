package ru.tandemservice.movestudentbasermc.component.student.OrderListPub.Add;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

@Input({
        @Bind(key = "studentId", binding = "student.id"),
        @Bind(key = "orderListId", binding = "orderListId")

})
@Output({
        @Bind(key = "studentId", binding = "student.id")
})
public class Model {
    private Student _student = new Student();

    private IEntity _second;

    private List<HSelectOption> _secondHierarchyList;

    private String orderText;
    private org.apache.tapestry.request.IUploadFile uploadFile;


    private Date _orderDate;
    private String _orderNumber;
    private String _orderDsk;
    private Date _orderDateStart;
    private Date _orderDateStop;

    private Long _orderListId;

    private Boolean editFile;
    private DatabaseFile file;

    public DatabaseFile getFile() {
        return file;
    }

    public void setFile(DatabaseFile file) {
        this.file = file;
    }

    public Boolean getEditFile() {
        return editFile;
    }

    public void setEditFile(Boolean editFile) {
        this.editFile = editFile;
    }

    public IUploadFile getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getOrderText() {
        return orderText;
    }

    public void setOrderText(String orderText) {
        this.orderText = orderText;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public IEntity getSecond()
    {
        return _second;
    }

    public void setSecond(IEntity second)
    {
        _second = second;
    }

    public List<HSelectOption> getSecondHierarchyList()
    {
        return _secondHierarchyList;
    }

    public void setSecondHierarchyList(List<HSelectOption> secondHierarchyList)
    {
        _secondHierarchyList = secondHierarchyList;
    }

    /**
     * @return Дата приказа. Свойство не может быть null.
     */
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа. Свойство не может быть null.
     */
    public void setOrderDate(Date orderDate)
    {
        _orderDate = orderDate;
    }

    /**
     * @return Номер приказа. Свойство не может быть null.
     */
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа. Свойство не может быть null.
     */
    public void setOrderNumber(String orderNumber)
    {
        _orderNumber = orderNumber;
    }

    /**
     * @return Примечание к приказу.
     */
    public String getOrderDsk()
    {
        return _orderDsk;
    }

    /**
     * @param orderDsk Примечание к приказу.
     */
    public void setOrderDsk(String orderDsk)
    {
        _orderDsk = orderDsk;
    }

    /**
     * @return Дата начала действия приказа.
     */
    public Date getOrderDateStart()
    {
        return _orderDateStart;
    }

    /**
     * @param orderDateStart Дата начала действия приказа.
     */
    public void setOrderDateStart(Date orderDateStart)
    {
        _orderDateStart = orderDateStart;
    }

    /**
     * @return Дата окончания действия приказа.
     */
    public Date getOrderDateStop()
    {
        return _orderDateStop;
    }

    /**
     * @param orderDateStop Дата окончания действия приказа.
     */
    public void setOrderDateStop(Date orderDateStop)
    {
        _orderDateStop = orderDateStop;
    }

    public void setOrderListId(Long _orderListId) {
        this._orderListId = _orderListId;
    }

    public Long getOrderListId() {
        return _orderListId;
    }


}
