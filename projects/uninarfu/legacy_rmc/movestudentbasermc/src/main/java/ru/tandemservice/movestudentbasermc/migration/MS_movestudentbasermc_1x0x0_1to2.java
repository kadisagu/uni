package ru.tandemservice.movestudentbasermc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Пропуск миграции в версии 2.3.3
 * <p/>
 * MS_movestudent_2x3x3_0to1.java
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentbasermc_1x0x0_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность excludeStateExamStuExtract
        // создано обязательное свойство pluralForStateExam
        if (
                tool.tableExists("excludestateexamstuextract_t")
                ) {
            if (!tool.columnExists("excludestateexamstuextract_t", "pluralforstateexam_p")) {
                // создать колонку
                tool.createColumn("excludestateexamstuextract_t", new DBColumn("pluralforstateexam_p", DBType.BOOLEAN));
            }
            // задать значение по умолчанию
            java.lang.Boolean defaultPluralForStateExam = false;
            tool.executeUpdate("update excludestateexamstuextract_t set pluralforstateexam_p=? where pluralforstateexam_p is null", defaultPluralForStateExam);

            // сделать колонку NOT NULL
            if (tool.isColumnNullable("excludestateexamstuextract_t", "pluralforstateexam_p"))
                tool.setColumnNullable("excludestateexamstuextract_t", "pluralforstateexam_p", false);
        }
    }
}