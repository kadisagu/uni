package ru.tandemservice.movestudentbasermc.movestudent.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.util.system.UniSystemUtils;

import java.util.Date;

public class ListExtractComponentGenerator {
    private static String _localPath = UniSystemUtils.getModulesDir().getAbsolutePath();
    private static String _tandemUniPath = System.getProperty("tandemPath");
    private static String _templatePath = _tandemUniPath + "/ru/tandemservice/movestudent/utils/system/listordertemplates";
    private static String _ramecProject = System.getProperty("ramecProject");

    public static void main(String[] args) throws Exception
    {
        if (StringUtils.isEmpty(_ramecProject))
            throw new RuntimeException("Plz specify printforms ramecProject: for example 'spbstu'");

        for (String index : args) {
            int extractIndex = Integer.parseInt(index);
            String[] extractData = ListExtractData_Rmc.EXTRACT_LIST[extractIndex - 1000];
            String entityName = extractData[0];
            boolean multipleParagraphAddEnabled = "true".equals(extractData[2]);

            if (!extractData[3].equalsIgnoreCase(_ramecProject))
                continue;

            if (entityName.endsWith("Order"))
                createOrderComponent(entityName, extractIndex);
            else
                createExtractComponent(entityName, extractIndex, multipleParagraphAddEnabled);
        }
    }

    private static void createOrderComponent(String entityName, int extractIndex) throws Exception
    {
        String orderSimpleClass = StringUtils.capitalize(entityName);

        String componentPath = _localPath + "/" + _ramecProject + "/src/main/java/ru/tandemservice/movestudent/component/listextract/e" + extractIndex;

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", orderSimpleClass);
        context.put("extractIndex", extractIndex);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ListOrderAddEdit", ListExtractComponentGenerator._templatePath + "/ListOrderAddEdit", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ListOrderPub", ListExtractComponentGenerator._templatePath + "/ListOrderPub", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + orderSimpleClass + "Print.java", ListExtractComponentGenerator._templatePath + "/orderPrint.vm", context, false);
    }

    private static void createExtractComponent(String entityName, int extractIndex, boolean multipleParagraphAddEnabled) throws Exception
    {
        String extractSimpleClass = StringUtils.capitalize(entityName);

        String componentPath = _localPath + "/" + _ramecProject + "/src/main/java/ru/tandemservice/movestudent/component/listextract/e" + extractIndex;

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", extractSimpleClass);
        context.put("extractIndex", extractIndex);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ParagraphAddEdit", _templatePath + "/ParagraphAddEdit", context, false);

        if (multipleParagraphAddEnabled)
            UniSystemUtils.createComponentByTemplate(componentPath + "/MultipleParagraphAdd", _templatePath + "/MultipleParagraphAdd", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ParagraphPub", _templatePath + "/ParagraphPub", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/ListExtractPub", _templatePath + "/ListExtractPub", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Dao.java", _templatePath + "/extractDao.vm", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Print.java", _templatePath + "/extractPrint.vm", context, false);
    }
}
