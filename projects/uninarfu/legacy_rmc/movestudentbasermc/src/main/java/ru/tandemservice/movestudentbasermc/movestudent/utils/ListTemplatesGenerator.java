package ru.tandemservice.movestudentbasermc.movestudent.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.SystemUtil;
import ru.tandemservice.uni.util.system.UniSystemUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Генератор печатных форм для списочных приказов
 * <p/>
 *
 * @author vip_delete
 * @since 09.12.2008
 */
public class ListTemplatesGenerator {
    private final static String ORDER_HEADER = "@listOrderHeader.txt";
    private final static String ORDER_TOP = "@listOrderTop.txt";
    private final static String ORDER_BOTTOM = "@listOrderBottom.txt";

    private final static String PARAGRAPH_HEADER = "@paragraphHeader.txt";

    private final static String EXTRACT_HEADER = "@extractHeader.txt";
    private final static String EXTRACT_BOTTOM = "@extractBottom.txt";

    private final static String LIST_ORDER_TEMPLATE_FILENAME_PREFIX = "listorder_";
    private final static String PARAGRAPH_TEMPLATE_FILENAME_PREFIX = "paragraph_";
    private final static String EXTRACT_TEMPLATE_FILENAME_PREFIX = "extract_";

    public static void main(String[] args) throws IOException
    {
        File modules = UniSystemUtils.getModulesDir();
//        String moduleOwner = System.getProperty("moduleOwner");
        String ramecProject = System.getProperty("ramecProject");

//        if (StringUtils.isEmpty(moduleOwner))
//            throw new RuntimeException("Plz specify printforms moduleOwner: for example 'movestudent'");

        if (StringUtils.isEmpty(ramecProject))
            throw new RuntimeException("Plz specify printforms ramecProject: for example 'spbstu'");


        //String printformsPath = modules.getAbsolutePath() + ("movestudent".equals(moduleOwner) ? "/" : "/projects/") + moduleOwner + "/src/main/java/ru/tandemservice/" + moduleOwner + "/utils/system/listordertemplates/printforms/";
        //String templatesPath = modules.getAbsolutePath() + ("movestudent".equals(moduleOwner) ? "/" : "/projects/") + moduleOwner + "/src/main/resources/" + moduleOwner + "/templates/listorder/";

        String printformsPath = modules.getAbsolutePath() + "/" + ramecProject + "/src/main/java/ru/ramec/" + ramecProject + "/utils/system/listordertemplates/printforms/";
        String templatesPath = modules.getAbsolutePath() + "/" + ramecProject + "/src/main/resources/" + ramecProject + "/templates/listorder/";


        byte[] orderHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + ORDER_HEADER));
        byte[] orderTopUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + ORDER_TOP));
        byte[] orderBottomUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + ORDER_BOTTOM));
        byte[] paragraphHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + PARAGRAPH_HEADER));

        byte[] extractHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_HEADER));
        byte[] extractBottomUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_BOTTOM));

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // create templates
        File path = new File(printformsPath);
        for (File file : path.listFiles()) {
            if (file.getName().endsWith(".txt") && !file.getName().startsWith("@")) {
                if (file.getName().startsWith(LIST_ORDER_TEMPLATE_FILENAME_PREFIX)) {
                    // create listorder template
                    out.reset();
                    out.write(getRtfData(orderHeaderUTF8));
                    out.write(getRtfData(orderTopUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write(getRtfData(orderBottomUTF8));
                    File newFile = new File(templatesPath + "/" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                    FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                    System.out.println("[+] " + newFile.getAbsolutePath());
                }
                else if (file.getName().startsWith(PARAGRAPH_TEMPLATE_FILENAME_PREFIX)) {
                    // create paragraph template
                    out.reset();
                    out.write(getRtfData(paragraphHeaderUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write("\\par}".getBytes());
                    File newFile = new File(templatesPath + "/" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                    FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                    System.out.println("[+] " + newFile.getAbsolutePath());
                }
                else if (file.getName().startsWith(EXTRACT_TEMPLATE_FILENAME_PREFIX)) {
                    // create extract template
                    out.reset();
                    out.write(getRtfData(extractHeaderUTF8));
                    out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                    out.write(getRtfData(extractBottomUTF8));
                    out.write("\\par}".getBytes());
                    File newFile = new File(templatesPath + "/" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                    FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                    System.out.println("[+] " + newFile.getAbsolutePath());
                }
                else {

                }
            }
        }
    }

    /**
     * My_Param} -> {\field{\*\fldinst{COMMENTS My_Param \\* MERGEFORMAT}}{\fldrslt{My_Param}}}
     * \n -> \par
     *
     * @param readableDataUTF - массив байт в кодировке UTF-8 шаблона выписки
     *
     * @return массив получившихся байт после подстановки параметров
     *
     * @throws java.io.IOException если будет ошибка записи в ByteArrayOutputStream
     */
    private static byte[] getRtfData(byte[] readableDataUTF) throws IOException
    {
        byte[] readableData = new String(readableDataUTF, "UTF-8").getBytes("CP1251");

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        for (int i = 0; i < readableData.length; i++) {
            byte c = readableData[i];
            if (c < 0) {
                // русские символы превращаются в rtf-escape-последовательность
                result.write(new byte[]{'\\', '\''});
                result.write(SystemUtil.getBytes(c));
            }
            else if (c == '{') {
                i++;
                boolean isParameter = true;
                for (; i < readableData.length && readableData[i] != '}'; i++) {
                    byte b = readableData[i];
                    isParameter = isParameter && ((b >= 'A' && b <= 'Z') || (b >= 'a' && b <= 'z') || b == '_');
                    buffer.write(b);
                }
                if (isParameter)
                    result.write(("{\\field{\\*\\fldinst{COMMENTS " + buffer.toString() + "}}{\\fldrslt{" + buffer.toString() + "}}}").getBytes());
                else {
                    result.write('{');
                    result.write(buffer.toByteArray());
                    result.write('}');
                }
                buffer.reset();
            }
            else
                result.write(c);
        }
        return result.toByteArray();
    }

}