package ru.tandemservice.movestudentbasermc.movestudent.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.VelocityContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.util.system.UniSystemUtils;

import java.util.Date;

//import ru.tandemservice.movestudent.utils.system.ExtractData;

/**
 * Генератор для создания компонентов выписки в component/modularextract
 * <p/>
 * 1. В файле ExtractData_Rmc.java в конец добавить новый тип выписки
 * 2. Запустить файл ModularStudentExtractComponentGenerator, указав -Dmodules=<путь до tandem.uni> и в program-parameters указав индекс(ы) нового типа выписки.
 * 3. Выполнить Organize Imports в созданных классах.
 */
public class ModularStudentExtractComponentGenerator {

    private static String _localPath = UniSystemUtils.getModulesDir().getAbsolutePath(); //"D:/Work/TandemUNI/SPBSTU";
    private static String _tandemUniPath = "D:/Work/TandemUNI/tandemservice/movestudent-1.2.8-uni-r2-sources";
    private static String _templatePath = _tandemUniPath + "/ru/tandemservice/movestudent/utils/system/templates";
    private static String _ramecProject = System.getProperty("ramecProject");

    public static void main(String[] args) throws Exception
    {
        if (StringUtils.isEmpty(_ramecProject))
            throw new RuntimeException("Plz specify printforms ramecProject: for example 'spbstu'");

        for (String index : args) {
            int extractIndex = Integer.parseInt(index);
            String[] extractData = ExtractData_Rmc.EXTRACT_LIST[extractIndex - 1000];
            String entityName = extractData[0];

            createExtractComponent(entityName, extractIndex);
        }

//      int extractIndex = Integer.parseInt(args[0]);
//      String entityName = args[1];
//    	createExtractComponent(entityName, extractIndex);
    }

    private static void createExtractComponent(String entityName, int extractIndex) throws Exception
    {
        String extractSimpleClass = StringUtils.capitalize(entityName);

        String componentPath = _localPath + "/" + _ramecProject + "/src/main/java/ru/tandemservice/movestudent/component/modularextract/e" + extractIndex;

        UniSystemUtils.initVelocity();

        VelocityContext context = new VelocityContext();
        context.put("entityName", entityName);
        context.put("creationDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        context.put("entityClassName", extractSimpleClass);
        context.put("extractIndex", extractIndex);

        UniSystemUtils.createComponentByTemplate(componentPath + "/AddEdit", _templatePath + "/AddEdit", context, false);

        UniSystemUtils.createComponentByTemplate(componentPath + "/Pub", _templatePath + "/Pub", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Dao.java", _templatePath + "/extractDao.vm", context, false);

        UniSystemUtils.createFileByTemplate(componentPath + "/" + extractSimpleClass + "Print.java", _templatePath + "/extractPrint.vm", context, false);
    }


}
