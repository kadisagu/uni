package ru.tandemservice.movestudentbasermc.movestudent.listextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IListParagraphTextFormatter;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StuListOrderToBasicRelation;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 *  Нестатичная копия ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint
 *  чтобы можно было переопределять основные методы
 * 
 * */
public class CommonListOrderPrintRMC {
    public static final String PARAGRAPHS = ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.PARAGRAPHS;
    public static final String STUDENT_LIST = ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.STUDENT_LIST;

    public static final IStudentListParagraphPrintFormatterRMC STUDENT_LIST_FORMATTER = new IStudentListParagraphPrintFormatterRMC() {
        @Override
        public String formatSingleStudent(ListStudentExtract enrollmentExtract, Student student, int extractNumber)
        {
            StringBuffer buffer = new StringBuffer();
            buffer.append("\\par ").append(extractNumber).append(".  ");
            buffer.append(student.getPerson().getFullFio()).append(" (");
            buffer.append(student.getCompensationType().getShortTitle()).append(")");
            return buffer.toString();
        }
    };

    public static RtfInjectModifier createListOrderInjectModifier(StudentListOrder order)
    {
        RtfInjectModifier modifier = ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.createListOrderInjectModifier(order);
        modifier.put("basics_label", StringUtils.isEmpty(order.getBasicListStr()) ? "" : "Основание:");
        return modifier;
    }

    public static RtfTableModifier createListOrderTableModifier(StudentListOrder order)
    {
        RtfTableModifier tableModifier = ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.createListOrderTableModifier(order);

        //Список оснований в алфавитном порядке
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StuListOrderToBasicRelation.class, "r");
        builder.addColumn("r")
                .where(DQLExpressions.eq(DQLExpressions.property(StuListOrderToBasicRelation.order().fromAlias("r")), DQLExpressions.value(order)))
                .order(DQLExpressions.property(StuListOrderToBasicRelation.basic().title().fromAlias("r")), OrderDirection.asc);

        List<StuListOrderToBasicRelation> list = UniDaoFacade.getCoreDao().getList(builder);
        String[][] baseTbl = new String[list.size()][2];
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                baseTbl[i][0] = Integer.toString(i + 1) + ". ";
                baseTbl[i][1] = list.get(i).getBasic().getTitle() + (StringUtils.isEmpty(list.get(i).getComment()) ? "" : " " + list.get(i).getComment());
            }

        }
        tableModifier.put("BASICS", baseTbl);

        return tableModifier;
    }

    @SuppressWarnings("rawtypes")
    public void injectParagraphs(final RtfDocument document, StudentListOrder order, IStudentListParagraphPrintFormatterRMC formatter)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound()) {
            List<IRtfElement> parList = new ArrayList<IRtfElement>();

            for (IAbstractParagraph paragraph : order.getParagraphList()) {
                RtfDocument paragraphPart = getParagraphPart(paragraph, null != formatter ? formatter : STUDENT_LIST_FORMATTER);

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    @SuppressWarnings("rawtypes")
    public String createParagraphText(IAbstractParagraph paragraph)
    {
        return createParagraphText(paragraph, STUDENT_LIST_FORMATTER);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public String createParagraphText(IAbstractParagraph paragraph, IStudentListParagraphPrintFormatterRMC formatter)
    {
        // получаем студентов из параграфа
        StringBuffer buffer = new StringBuffer();
        int counter = 1;
        for (ListStudentExtract enrollmentExtract : (List<ListStudentExtract>) paragraph.getExtractList())
            buffer.append(formatter.formatSingleStudent(enrollmentExtract, enrollmentExtract.getEntity(), counter++));
        //buffer.append("\\par");
        return buffer.toString();
    }

    @SuppressWarnings("rawtypes")
    public static RtfInjectModifier createListOrderParagraphInjectModifier(IAbstractParagraph paragraph, ListStudentExtract firstExtract)
    {
        return ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public RtfDocument getParagraphPart(IAbstractParagraph paragraph, IStudentListParagraphPrintFormatterRMC formatter)
    {
        List<ListStudentExtract> extractList = paragraph.getExtractList();
        if (extractList.size() == 0)
            throw new ApplicationException("Невозможно распечатать пустой параграф (№" + paragraph.getNumber() + ").");

        // первая выписка из параграфа
        ListStudentExtract extract = extractList.get(0);

        byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(extract.getType(), 1);

        RtfDocument paragraphPart = new RtfReader().read(paragraphTemplate);

        IListParagraphPrintFormCreator printForm = (IListParagraphPrintFormCreator) ApplicationRuntime.getBean(EntityRuntime.getMeta(extract).getName() + "_extractPrint");
        RtfInjectModifier injectModifier = printForm.createParagraphInjectModifier(paragraph, extract);
        injectModifier.modify(paragraphPart);
        RtfTableModifier injectTableModifier = printForm.createParagraphTableModifier(paragraph, extract);
        if (injectTableModifier != null) injectTableModifier.modify(paragraphPart);


        RtfSearchResult searchResult = UniRtfUtil.findRtfMark(paragraphPart, STUDENT_LIST);
        if (searchResult.isFound()) {
            IListParagraphTextFormatter paragraphTextFormatter = (printForm instanceof IListParagraphTextFormatter) ? (IListParagraphTextFormatter) printForm : null;
            String paragraphText = null != paragraphTextFormatter ? paragraphTextFormatter.createParagraphText(paragraph) : createParagraphText(paragraph, printForm instanceof IStudentListParagraphPrintFormatterRMC ? (IStudentListParagraphPrintFormatterRMC) printForm : formatter);
            IRtfText text = RtfBean.getElementFactory().createRtfText(paragraphText);
            text.setRaw(true);

            searchResult.getElementList().set(searchResult.getIndex(), text);
        }

        return paragraphPart;
    }

    public static void initOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit, String prefix, String postfix)
    {
//    	ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.initOrgUnit(modifier, orgUnit, "orgUnit", postfix);
    }

    public static void initDevelopForm(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, String postfix)
    {
//    	ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.initDevelopForm(modifier, educationOrgUnit, postfix);
    }


    /**
     * Формирует ФИО для приказов в заданном падеже. Фамилия в верхнем регистре
     *
     * @param rusCase - Падеж для формирования ФИО
     *
     * @return
     */
    public static String prepareStudentName(GrammaCase rusCase, Student student)
    {
        return prepareStudentName(rusCase, student, true);
    }

    /**
     * Формирует ФИО для приказов в заданном падеже.
     *
     * @param rusCase    - Падеж для формирования ФИО
     * @param upLastName - Фамилия в верхнем регистре
     *
     * @return
     */
    public static String prepareStudentName(GrammaCase rusCase, Student student, boolean upLastName)
    {
        if (rusCase != null) {
            IdentityCard identityCard = student.getPerson().getIdentityCard();
            boolean isMaleSex = UniDefines.CATALOG_SEX_MALE.equals(identityCard.getSex().getCode());

            IDeclinationDao dao = PersonManager.instance().declinationDao();

            String lastName = dao.getDeclinationLastName(identityCard.getLastName(), rusCase, isMaleSex);
            if (upLastName)
                lastName = lastName.toUpperCase();

            StringBuffer str = new StringBuffer(lastName);
            str.append(" ").append(dao.getDeclinationFirstName(identityCard.getFirstName(), rusCase, isMaleSex));
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                str.append(" ").append(dao.getDeclinationMiddleName(identityCard.getMiddleName(), rusCase, isMaleSex));

            return str.toString();

        }
        else return "";
    }

    public static RtfInjectModifier createModularExtractInjectModifier(ListStudentExtract extract)
    {
        IEntityMeta meta = EntityRuntime.getMeta(extract.getId());

        final RtfInjectModifier modifier = new RtfInjectModifier();
        final String[] postfixList = new String[]{"Old", "New"};

        StudentListOrder order = (StudentListOrder) extract.getParagraph().getOrder();
        modifier.put("orderNum", order.getNumber());
        modifier.put("parNumber", Integer.toString(extract.getParagraph().getNumber()));
        modifier.put("eduYear", order.getEducationYear().getTitle());
        modifier.put("extractNumber", Integer.toString(extract.getNumber()));

        modifier.put("studentPersonalNumber", extract.getEntity().getPerNumber());
        modifier.put("studentPersonalFileNumber", null != extract.getEntity().getPersonalFileNumber() ? extract.getEntity().getPersonalFileNumber() : "");
        modifier.put("studentBookNumber", null != extract.getEntity().getBookNumber() ? extract.getEntity().getBookNumber() : "");

        if (extract.getEntity().getPerson().getIdentityCard().getSex().getCode().equals(UniDefines.CATALOG_SEX_MALE)) {
            // student (male)
            modifier.put("student", "студент");
            modifier.put("student_G", "студента");
            modifier.put("student_D", "студенту");
            modifier.put("student_A", "студента");
            modifier.put("student_I", "студентом");
            modifier.put("student_P", "студенте");

            // student (male) capitalized
            modifier.put("Student", "Студент");
            modifier.put("Student_G", "Студента");
            modifier.put("Student_D", "Студенту");
            modifier.put("Student_A", "Студента");
            modifier.put("Student_I", "Студентом");
            modifier.put("Student_P", "Студенте");

            // learned (male)
            modifier.put("learned", "обучающийся");
            modifier.put("learned_G", "обучающегося");
            modifier.put("learned_D", "обучающемуся");
            modifier.put("learned_A", "обучающегося");
            modifier.put("learned_I", "обучающимся");
            modifier.put("learned_P", "обучающимся");

            // learned in the past (male)
            modifier.put("learned_past", "обучался");
            modifier.put("learned_past_G", "обучавшемуся");
            modifier.put("learned_past_D", "обучавшемуся");
            modifier.put("learned_past_A", "обучавшегося");
            modifier.put("learned_past_I", "обучавшимся");
            modifier.put("learned_past_P", "обучавшемуся");

            // toBegin (male)
            modifier.put("toBegin", "приступивший");
            modifier.put("toBegin_G", "приступившего");
            modifier.put("toBegin_D", "приступившему");
            modifier.put("toBegin_A", "приступившего");
            modifier.put("toBegin_I", "приступившим");
            modifier.put("toBegin_P", "приступившем");
        }
        else {
            // student (female)
            modifier.put("student", "студентка");
            modifier.put("student_G", "студентки");
            modifier.put("student_D", "студентке");
            modifier.put("student_A", "студентку");
            modifier.put("student_I", "студенткой");
            modifier.put("student_P", "студентке");

            // student (female) capitalized
            modifier.put("Student", "Студентка");
            modifier.put("Student_G", "Студентки");
            modifier.put("Student_D", "Студентке");
            modifier.put("Student_A", "Студентку");
            modifier.put("Student_I", "Студенткой");
            modifier.put("Student_P", "Студентке");

            // learned (female)
            modifier.put("learned", "обучающаяся");
            modifier.put("learned_G", "обучающейся");
            modifier.put("learned_D", "обучающейся");
            modifier.put("learned_A", "обучающуюся");
            modifier.put("learned_I", "обучающейся");
            modifier.put("learned_P", "обучающейся");

            // learned in the past (female)
            modifier.put("learned_past", "обучалась");
            modifier.put("learned_past_G", "обучавшейся");
            modifier.put("learned_past_D", "обучавшейся");
            modifier.put("learned_past_A", "обучавшуюся");
            modifier.put("learned_past_I", "обучавшейся");
            modifier.put("learned_past_P", "обучавшейся");

            // toBegin (female)
            modifier.put("toBegin", "приступившая");
            modifier.put("toBegin_G", "приступившей");
            modifier.put("toBegin_D", "приступившей");
            modifier.put("toBegin_A", "приступившую");
            modifier.put("toBegin_I", "приступившей");
            modifier.put("toBegin_P", "приступившем");
        }

        // extractType
        modifier.put("extractType", extract.getParagraph() == null ? "проект приказа" : "выписка");

        // compensationTypeStr (*)
        for (String postfix : postfixList)
            if (meta.getProperty("compensationType" + postfix) != null) {
                modifier.put("compensationTypeStr" + postfix, ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget() ? "за счет средств федерального бюджета" : "на договорной основе с оплатой стоимости обучения");
                modifier.put("compensationTypeStr" + postfix + "_A", ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget() ? "на бюджетную основу" : "на контрактную основу");
                modifier.put("compensationTypeStr" + postfix + "_G", ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget() ? "бюджетной основы" : "контрактной основы");
                modifier.put("compensationTypeStr" + postfix + "_D_Alt", ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget() ? "бюджетной основе" : "внебюджетной основе");
                modifier.put("compensationTypeStr" + postfix + "_G_Alt", ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget() ? "бюджетной основы" : "внебюджетной основы");
                modifier.put("compensationTypeStr" + postfix + "_A_Alt", ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget() ? "бюджетную основу" : "внебюджетную основу");
            }
        modifier.put("compensationTypeStr", extract.getEntity().getCompensationType().isBudget() ? "за счет средств федерального бюджета" : "на договорной основе с оплатой стоимости обучения");
        modifier.put("compensationTypeStr" + "_A", extract.getEntity().getCompensationType().isBudget() ? "на бюджетную основу" : "на контрактную основу");
        modifier.put("compensationTypeStr" + "_G", extract.getEntity().getCompensationType().isBudget() ? "бюджетной основы" : "контрактной основы");
        modifier.put("compensationTypeStr" + "_Alt", extract.getEntity().getCompensationType().isBudget() ? "на бюджетной основе" : "на внебюджетной основе");
        modifier.put("compensationTypeStr" + "_A_Alt", extract.getEntity().getCompensationType().isBudget() ? "на бюджетную основу" : "на внебюджетную основу");
        modifier.put("compensationTypeStr" + "_D_Alt", extract.getEntity().getCompensationType().isBudget() ? "бюджетной основе" : "внебюджетной основе");
        modifier.put("compensationTypeStr" + "_G_Alt", extract.getEntity().getCompensationType().isBudget() ? "бюджетной основы" : "внебюджетной основы");

        // educationType (+*)
        for (String postfix : postfixList)
            if (meta.getProperty("educationOrgUnit" + postfix) != null)
                UniRtfUtil.initEducationType(modifier, ((EducationOrgUnit) extract.getProperty("educationOrgUnit" + postfix)).getEducationLevelHighSchool(), postfix);
        UniRtfUtil.initEducationType(modifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(), "");

        // fio
        modifier.put("fio", extract.getStudentTitle());
        modifier.put("FIO", extract.getStudentTitle().toUpperCase());

        // fio in Nominative
        modifier.put("fio_N", extract.getEntity().getPerson().getFullFio());
        modifier.put("FIO_N", extract.getEntity().getPerson().getFullFio().toUpperCase());

        // course
        for (String postfix : postfixList)
            if (meta.getProperty("course" + postfix) != null)
                modifier.put("course" + postfix, ((Course) extract.getProperty("course" + postfix)).getTitle());
        modifier.put("course", extract.getEntity().getCourse().getTitle());

        // group
        for (String postfix : postfixList)
            if (meta.getProperty("group" + postfix) != null)
                modifier.put("group" + postfix, ((Group) extract.getProperty("group" + postfix)).getTitle());
        modifier.put("group", extract.getEntity().getGroup() == null ? "" : extract.getEntity().getGroup().getTitle());

        // developForm
        for (String postfix : postfixList)
            if (meta.getProperty("educationOrgUnit" + postfix) != null)
                initDevelopForm(modifier, (EducationOrgUnit) extract.getProperty("educationOrgUnit" + postfix), postfix);
        initDevelopForm(modifier, extract.getEntity().getEducationOrgUnit(), "");

        // educationOrgUnit
        for (String postfix : postfixList)
            if (meta.getProperty("educationOrgUnit" + postfix) != null)
                modifier.put("educationOrgUnit" + postfix, ((EducationOrgUnit) extract.getProperty("educationOrgUnit" + postfix)).getEducationLevelHighSchool().getPrintTitle());
        modifier.put("educationOrgUnit", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());

        initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "", "");

        //educationOrgUnitWithLevel
        //направление магистров
        if (QualificationsCodes.MAGISTR.equals(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode())) {
            modifier.put("educationOrgUnitWithLevel", "по направлению магистратуры \"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle() + "\"");
        }
        //направление бакалавров
        else if (QualificationsCodes.BAKALAVR.equals(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode())) {
            modifier.put("educationOrgUnitWithLevel", "по направлению бакалавриата \"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle() + "\"");
        }
        //направление специальностей
        else if (QualificationsCodes.SPETSIALIST.equals(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode())) {
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle() + "\"");
        }
        //специальность (базовый уровень)
        else if (QualificationsCodes.BAZOVYY_UROVEN_S_P_O.equals(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode())) {
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle() + " (базовый уровень)\"");
        }
        //специальность (повышенный уровень)
        else if (QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O.equals(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification().getCode())) {
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle() + " (повышенный уровень)\"");
        }
        else
            modifier.put("educationOrgUnitWithLevel", "по специальности \"" + extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle() + "\"");

        // orderType
        modifier.put("orderType", extract.getType().getTitle());

        // createDate
        modifier.put("createDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getCreateDate()));

        // commitDate
        Date commitDate;
        if (extract.getParagraph() == null || (commitDate = extract.getParagraph().getOrder().getCommitDate()) == null)
            modifier.put("commitDate", "        ");
        else
            modifier.put("commitDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate));

        // listBasics
        modifier.put("listBasics", order.getBasicListStr());

        // reason
        if (order.getReason() != null)
            modifier.put("reason", order.getReason().getPrintTitle() == null ? order.getReason().getTitle() : order.getReason().getPrintTitle());
        else
            modifier.put("reason", "");

        //       modifier.put("Reason", StringUtils.capitalize(extract.getReason().getPrintTitle() == null ? extract.getReason().getTitle() : extract.getReason().getPrintTitle()));
        //       modifier.put("reasonPrint", extract.getReason().getPrintTitle() == null ? extract.getReason().getTitle() : extract.getReason().getPrintTitle());
        //       modifier.put("ReasonPrint", StringUtils.capitalize(extract.getReason().getPrintTitle() == null ? extract.getReason().getTitle() : extract.getReason().getPrintTitle()));

        modifier.put("birthDate", extract.getEntity().getPerson().getBirthDateStr());

        if (meta.getProperty("beginDate") != null) {
            Date beginDate = (Date) extract.getProperty("beginDate");
            modifier.put("beginDate", beginDate == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate));
        }
        if (meta.getProperty("endDate") != null) {
            Date endDate = (Date) extract.getProperty("endDate");
            modifier.put("endDate", endDate == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
        }

        TopOrgUnit academy = TopOrgUnit.getInstance();
        modifier.put("rector", academy.getHead() == null ? "" : ((Person) academy.getHead().getEmployee().getPerson()).getFio());

        return modifier;
    }

}
