package ru.tandemservice.movestudentbasermc.movestudent.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StudentOrdersTypes;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь между приказами по движению студентов и типами приказов (для аналитики)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StdExtractT2OrdersTGen extends EntityBase
 implements INaturalIdentifiable<StdExtractT2OrdersTGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT";
    public static final String ENTITY_NAME = "stdExtractT2OrdersT";
    public static final int VERSION_HASH = 1498338608;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_IS_SYSTEM = "isSystem";
    public static final String L_STUDENT_ORDERS_TYPES = "studentOrdersTypes";
    public static final String L_STUDENT_EXTRACT_TYPE = "studentExtractType";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private boolean _isSystem;     // Системная запись
    private StudentOrdersTypes _studentOrdersTypes;     // Типы приказов по движению студентов (для аналитики)
    private StudentExtractType _studentExtractType;     // Тип выписки по студенту
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Системная запись. Свойство не может быть null.
     */
    @NotNull
    public boolean isIsSystem()
    {
        return _isSystem;
    }

    /**
     * @param isSystem Системная запись. Свойство не может быть null.
     */
    public void setIsSystem(boolean isSystem)
    {
        dirty(_isSystem, isSystem);
        _isSystem = isSystem;
    }

    /**
     * @return Типы приказов по движению студентов (для аналитики). Свойство не может быть null.
     */
    @NotNull
    public StudentOrdersTypes getStudentOrdersTypes()
    {
        return _studentOrdersTypes;
    }

    /**
     * @param studentOrdersTypes Типы приказов по движению студентов (для аналитики). Свойство не может быть null.
     */
    public void setStudentOrdersTypes(StudentOrdersTypes studentOrdersTypes)
    {
        dirty(_studentOrdersTypes, studentOrdersTypes);
        _studentOrdersTypes = studentOrdersTypes;
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getStudentExtractType()
    {
        return _studentExtractType;
    }

    /**
     * @param studentExtractType Тип выписки по студенту. Свойство не может быть null.
     */
    public void setStudentExtractType(StudentExtractType studentExtractType)
    {
        dirty(_studentExtractType, studentExtractType);
        _studentExtractType = studentExtractType;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StdExtractT2OrdersTGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StdExtractT2OrdersT)another).getCode());
            }
            setIsSystem(((StdExtractT2OrdersT)another).isIsSystem());
            setStudentOrdersTypes(((StdExtractT2OrdersT)another).getStudentOrdersTypes());
            setStudentExtractType(((StdExtractT2OrdersT)another).getStudentExtractType());
            setTitle(((StdExtractT2OrdersT)another).getTitle());
        }
    }

    public INaturalId<StdExtractT2OrdersTGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StdExtractT2OrdersTGen>
    {
        private static final String PROXY_NAME = "StdExtractT2OrdersTNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StdExtractT2OrdersTGen.NaturalId) ) return false;

            StdExtractT2OrdersTGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StdExtractT2OrdersTGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StdExtractT2OrdersT.class;
        }

        public T newInstance()
        {
            return (T) new StdExtractT2OrdersT();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "isSystem":
                    return obj.isIsSystem();
                case "studentOrdersTypes":
                    return obj.getStudentOrdersTypes();
                case "studentExtractType":
                    return obj.getStudentExtractType();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "isSystem":
                    obj.setIsSystem((Boolean) value);
                    return;
                case "studentOrdersTypes":
                    obj.setStudentOrdersTypes((StudentOrdersTypes) value);
                    return;
                case "studentExtractType":
                    obj.setStudentExtractType((StudentExtractType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "isSystem":
                        return true;
                case "studentOrdersTypes":
                        return true;
                case "studentExtractType":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "isSystem":
                    return true;
                case "studentOrdersTypes":
                    return true;
                case "studentExtractType":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "isSystem":
                    return Boolean.class;
                case "studentOrdersTypes":
                    return StudentOrdersTypes.class;
                case "studentExtractType":
                    return StudentExtractType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StdExtractT2OrdersT> _dslPath = new Path<StdExtractT2OrdersT>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StdExtractT2OrdersT");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Системная запись. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#isIsSystem()
     */
    public static PropertyPath<Boolean> isSystem()
    {
        return _dslPath.isSystem();
    }

    /**
     * @return Типы приказов по движению студентов (для аналитики). Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getStudentOrdersTypes()
     */
    public static StudentOrdersTypes.Path<StudentOrdersTypes> studentOrdersTypes()
    {
        return _dslPath.studentOrdersTypes();
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getStudentExtractType()
     */
    public static StudentExtractType.Path<StudentExtractType> studentExtractType()
    {
        return _dslPath.studentExtractType();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends StdExtractT2OrdersT> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _isSystem;
        private StudentOrdersTypes.Path<StudentOrdersTypes> _studentOrdersTypes;
        private StudentExtractType.Path<StudentExtractType> _studentExtractType;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StdExtractT2OrdersTGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Системная запись. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#isIsSystem()
     */
        public PropertyPath<Boolean> isSystem()
        {
            if(_isSystem == null )
                _isSystem = new PropertyPath<Boolean>(StdExtractT2OrdersTGen.P_IS_SYSTEM, this);
            return _isSystem;
        }

    /**
     * @return Типы приказов по движению студентов (для аналитики). Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getStudentOrdersTypes()
     */
        public StudentOrdersTypes.Path<StudentOrdersTypes> studentOrdersTypes()
        {
            if(_studentOrdersTypes == null )
                _studentOrdersTypes = new StudentOrdersTypes.Path<StudentOrdersTypes>(L_STUDENT_ORDERS_TYPES, this);
            return _studentOrdersTypes;
        }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getStudentExtractType()
     */
        public StudentExtractType.Path<StudentExtractType> studentExtractType()
        {
            if(_studentExtractType == null )
                _studentExtractType = new StudentExtractType.Path<StudentExtractType>(L_STUDENT_EXTRACT_TYPE, this);
            return _studentExtractType;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StdExtractT2OrdersTGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return StdExtractT2OrdersT.class;
        }

        public String getEntityName()
        {
            return "stdExtractT2OrdersT";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
