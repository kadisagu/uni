package ru.tandemservice.movestudentbasermc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * Пропуск миграции в версии 2.3.3
 * <p/>
 * MS_movestudent_2x3x3_2to3.java
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentbasermc_1x0x0_3to4 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        int counter = 0;

        PreparedStatement st4 = tool.prepareStatement("update movestudenttemplate_t set title_p=? where title_p=? and index_p=4");

        st4.setString(1, "Шаблон подподпараграфа списочного приказа. О допуске к сдаче государственного экзамена. Допустить к сдаче государственного экзамена");
        st4.setString(2, "Выписка из списочного приказа. О допуске к сдаче государственного экзамена. Допустить к сдаче государственного экзамена");

        counter += st4.executeUpdate();

        System.out.println(getClass().getSimpleName() + ": fixed " + counter + " from 1");
    }
}