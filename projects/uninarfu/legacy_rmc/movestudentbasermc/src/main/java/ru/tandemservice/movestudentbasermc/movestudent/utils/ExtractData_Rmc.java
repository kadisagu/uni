package ru.tandemservice.movestudentbasermc.movestudent.utils;

/**
 * @author Mike
 * @since 21.11.2008
 */
public class ExtractData_Rmc {
    //список всех entity выписок из сборного приказа
    public static final String[][] EXTRACT_LIST = new String[][]{
        /*1000*/{"transferStuExtractSPBSTU", "О переводе", "spbstu"},
        /*1001*/{"eduEnrolmentStuExtractV3", "О зачислении", "spbstu"},
        /*1002*/{"eduEnrAsTransferStuExtractV2", "О зачислении в порядке перевода", "spbstu"},
        /*1003*/{"transferEduTypeStuExtractSPBSTU", "О переводе на другое направление подготовки (специальность)", "spbstu"},
        /*1004*/{"restorationStuExtractSPBSTU", "О восстановлении", "spbstu"},
        /*1005*/{"weekendStuExtractSPBSTU", "О предоставлении академического отпуска ", "spbstu"},
        /*1006*/{"weekendOutStuExtractSPBSTU", "О выходе из академического отпускае", "spbstu"},
        /*1007*/{"excludeStuExtractSPBSTU", "Об отчислении", "spbstu"},
        /*1008*/{"changeFioStuExtractSPBSTU", "О смене фамилии (имени)", "spbstu"}
    };
}
    
