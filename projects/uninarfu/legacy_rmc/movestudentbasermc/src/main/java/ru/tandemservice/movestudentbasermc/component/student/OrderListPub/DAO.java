package ru.tandemservice.movestudentbasermc.component.student.OrderListPub;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Collection;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("lst");

    static {

    }


    @Override
    public void delete(IEntity entity)
    {
        super.delete(entity);
    }

    ;

    @Override
    public void delete(Long entityId)
    {
        super.delete(entityId);
    }

    ;

    @Override
    public void prepare(Model model)
    {
        if (null != model.getStudent().getId()) {
            Student s2 = get(Student.class, model.getStudent().getId());
            model.setStudent(s2);
        }
    }

    public <T extends EntityBase> List<T> getList(Class<T> clazz,
                                                  Collection<Long> ids, String... sortProperties)
    {
        return null;
    }


    public <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource)
    {

        Session session = this.getSession();

        MQBuilder builder = new MQBuilder(OrderList.ENTITY_CLASS, "lst");
        builder.addJoin("lst", OrderList.L_STUDENT, "stu");
        builder.add(MQExpression.eq("stu", "id", model.getStudent().getId()));
        // builder.addOrder("lst", OrderList.PROPERTY_ORDER_DATE);

        _orderSettings.applyOrder(builder, dataSource.getEntityOrder());

        List<T> itemList = builder.getResultList(session);

        int count = Math.max(itemList.size() + 1, 2);
        dataSource.setTotalSize(count);
        dataSource.setCountRow(count);
        dataSource.createPage(itemList);

    }


}
