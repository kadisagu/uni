package ru.tandemservice.movestudentbasermc.entity;

import ru.tandemservice.movestudentbasermc.entity.gen.OrderListGen;

/**
 * Данные о приказах по студенту (для начальной загрузки)
 */
public class OrderList extends OrderListGen {
    private static final long serialVersionUID = 801955181L;
    /**/

    public boolean isAttachmentExist() {
        return this.getFile() != null;
    }
}