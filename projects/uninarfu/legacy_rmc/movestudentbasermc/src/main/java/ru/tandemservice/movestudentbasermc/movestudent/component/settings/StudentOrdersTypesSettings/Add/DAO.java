package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings.Add;

import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StudentOrdersTypes;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;

import java.util.Collections;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {

        MQBuilder builder = new MQBuilder(StudentExtractType.ENTITY_CLASS, "s");

        builder.add(MQExpression.eq("s", StudentExtractType.P_ACTIVE, Boolean.TRUE));
        builder.add(MQExpression.isNull("s", StudentExtractType.L_PARENT + "." + StudentExtractType.L_PARENT));
        List<StudentExtractType> list = builder.getResultList(getSession());

        Collections.sort(list, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        model.setStudentExtractTypeList(HierarchyUtil.listHierarchyNodesWithParents(list, false));

        model.setOrderTypesList(new LazySimpleSelectModel<>(getList(StudentOrdersTypes.class, StudentOrdersTypes.shortTitle().s())));
    }


    @Override
    public void update(Model model)
    {

        Session session = getSession();

        StudentOrdersTypes sot = model.getOrdersTypes();
        StudentExtractType set = model.getStudentExtractType();

        // если такое есть - ничего не делаем

        MQBuilder builder = new MQBuilder(StdExtractT2OrdersT.ENTITY_CLASS, "slot");
        builder.addJoin("slot", StdExtractT2OrdersT.L_STUDENT_ORDERS_TYPES, "sot");
        builder.addJoin("slot", StdExtractT2OrdersT.L_STUDENT_EXTRACT_TYPE, "stuet");


        builder.add(MQExpression.eq("sot", StudentOrdersTypes.P_ID, sot.getId()));
        builder.add(MQExpression.eq("stuet", StudentExtractType.P_ID, set.getId()));


        List<StdExtractT2OrdersT> result = builder.getResultList(session);

        if (result.size() == 0) {
            // имеет смысл
            StdExtractT2OrdersT _new = new StdExtractT2OrdersT();

            _new.setStudentExtractType(set);
            _new.setStudentOrdersTypes(sot);
            _new.setIsSystem(false);
            _new.setCode(set.getCode() + "_" + sot.getCode());
            session.saveOrUpdate(_new);
        }
    }


    public void validateDelete(List<Long> entityIds, ErrorCollector errors)
    {


    }
}
