package ru.tandemservice.movestudentbasermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные о приказах по студенту (для начальной загрузки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrderListGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.movestudentbasermc.entity.OrderList";
    public static final String ENTITY_NAME = "orderList";
    public static final int VERSION_HASH = -1309645836;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_TYPE = "type";
    public static final String P_ORDER_DATE = "orderDate";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_ORDER_DSK = "orderDsk";
    public static final String P_ORDER_DATE_START = "orderDateStart";
    public static final String P_ORDER_DATE_STOP = "orderDateStop";
    public static final String P_ORDER_TEXT = "orderText";
    public static final String L_FILE = "file";

    private Student _student;     // Студент
    private StudentExtractType _type;     // Тип выписки по студенту
    private Date _orderDate;     // Дата приказа
    private String _orderNumber;     // Номер приказа
    private String _orderDsk;     // Примечание к приказу
    private Date _orderDateStart;     // Дата начала действия приказа
    private Date _orderDateStop;     // Дата окончания действия приказа
    private String _orderText;     // Текст приказа
    private DatabaseFile _file;     // Файл

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип выписки по студенту. Свойство не может быть null.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Дата приказа. Свойство не может быть null.
     */
    @NotNull
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа. Свойство не может быть null.
     */
    public void setOrderDate(Date orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    /**
     * @return Номер приказа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа. Свойство не может быть null.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Примечание к приказу.
     */
    @Length(max=255)
    public String getOrderDsk()
    {
        return _orderDsk;
    }

    /**
     * @param orderDsk Примечание к приказу.
     */
    public void setOrderDsk(String orderDsk)
    {
        dirty(_orderDsk, orderDsk);
        _orderDsk = orderDsk;
    }

    /**
     * @return Дата начала действия приказа.
     */
    public Date getOrderDateStart()
    {
        return _orderDateStart;
    }

    /**
     * @param orderDateStart Дата начала действия приказа.
     */
    public void setOrderDateStart(Date orderDateStart)
    {
        dirty(_orderDateStart, orderDateStart);
        _orderDateStart = orderDateStart;
    }

    /**
     * @return Дата окончания действия приказа.
     */
    public Date getOrderDateStop()
    {
        return _orderDateStop;
    }

    /**
     * @param orderDateStop Дата окончания действия приказа.
     */
    public void setOrderDateStop(Date orderDateStop)
    {
        dirty(_orderDateStop, orderDateStop);
        _orderDateStop = orderDateStop;
    }

    /**
     * @return Текст приказа.
     */
    public String getOrderText()
    {
        return _orderText;
    }

    /**
     * @param orderText Текст приказа.
     */
    public void setOrderText(String orderText)
    {
        dirty(_orderText, orderText);
        _orderText = orderText;
    }

    /**
     * @return Файл.
     */
    public DatabaseFile getFile()
    {
        return _file;
    }

    /**
     * @param file Файл.
     */
    public void setFile(DatabaseFile file)
    {
        dirty(_file, file);
        _file = file;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrderListGen)
        {
            setStudent(((OrderList)another).getStudent());
            setType(((OrderList)another).getType());
            setOrderDate(((OrderList)another).getOrderDate());
            setOrderNumber(((OrderList)another).getOrderNumber());
            setOrderDsk(((OrderList)another).getOrderDsk());
            setOrderDateStart(((OrderList)another).getOrderDateStart());
            setOrderDateStop(((OrderList)another).getOrderDateStop());
            setOrderText(((OrderList)another).getOrderText());
            setFile(((OrderList)another).getFile());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrderListGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrderList.class;
        }

        public T newInstance()
        {
            return (T) new OrderList();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "type":
                    return obj.getType();
                case "orderDate":
                    return obj.getOrderDate();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "orderDsk":
                    return obj.getOrderDsk();
                case "orderDateStart":
                    return obj.getOrderDateStart();
                case "orderDateStop":
                    return obj.getOrderDateStop();
                case "orderText":
                    return obj.getOrderText();
                case "file":
                    return obj.getFile();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((Date) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "orderDsk":
                    obj.setOrderDsk((String) value);
                    return;
                case "orderDateStart":
                    obj.setOrderDateStart((Date) value);
                    return;
                case "orderDateStop":
                    obj.setOrderDateStop((Date) value);
                    return;
                case "orderText":
                    obj.setOrderText((String) value);
                    return;
                case "file":
                    obj.setFile((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "type":
                        return true;
                case "orderDate":
                        return true;
                case "orderNumber":
                        return true;
                case "orderDsk":
                        return true;
                case "orderDateStart":
                        return true;
                case "orderDateStop":
                        return true;
                case "orderText":
                        return true;
                case "file":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "type":
                    return true;
                case "orderDate":
                    return true;
                case "orderNumber":
                    return true;
                case "orderDsk":
                    return true;
                case "orderDateStart":
                    return true;
                case "orderDateStop":
                    return true;
                case "orderText":
                    return true;
                case "file":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "type":
                    return StudentExtractType.class;
                case "orderDate":
                    return Date.class;
                case "orderNumber":
                    return String.class;
                case "orderDsk":
                    return String.class;
                case "orderDateStart":
                    return Date.class;
                case "orderDateStop":
                    return Date.class;
                case "orderText":
                    return String.class;
                case "file":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrderList> _dslPath = new Path<OrderList>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrderList");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Дата приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDate()
     */
    public static PropertyPath<Date> orderDate()
    {
        return _dslPath.orderDate();
    }

    /**
     * @return Номер приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Примечание к приказу.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDsk()
     */
    public static PropertyPath<String> orderDsk()
    {
        return _dslPath.orderDsk();
    }

    /**
     * @return Дата начала действия приказа.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDateStart()
     */
    public static PropertyPath<Date> orderDateStart()
    {
        return _dslPath.orderDateStart();
    }

    /**
     * @return Дата окончания действия приказа.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDateStop()
     */
    public static PropertyPath<Date> orderDateStop()
    {
        return _dslPath.orderDateStop();
    }

    /**
     * @return Текст приказа.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderText()
     */
    public static PropertyPath<String> orderText()
    {
        return _dslPath.orderText();
    }

    /**
     * @return Файл.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getFile()
     */
    public static DatabaseFile.Path<DatabaseFile> file()
    {
        return _dslPath.file();
    }

    public static class Path<E extends OrderList> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private StudentExtractType.Path<StudentExtractType> _type;
        private PropertyPath<Date> _orderDate;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<String> _orderDsk;
        private PropertyPath<Date> _orderDateStart;
        private PropertyPath<Date> _orderDateStop;
        private PropertyPath<String> _orderText;
        private DatabaseFile.Path<DatabaseFile> _file;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Тип выписки по студенту. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Дата приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDate()
     */
        public PropertyPath<Date> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<Date>(OrderListGen.P_ORDER_DATE, this);
            return _orderDate;
        }

    /**
     * @return Номер приказа. Свойство не может быть null.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(OrderListGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Примечание к приказу.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDsk()
     */
        public PropertyPath<String> orderDsk()
        {
            if(_orderDsk == null )
                _orderDsk = new PropertyPath<String>(OrderListGen.P_ORDER_DSK, this);
            return _orderDsk;
        }

    /**
     * @return Дата начала действия приказа.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDateStart()
     */
        public PropertyPath<Date> orderDateStart()
        {
            if(_orderDateStart == null )
                _orderDateStart = new PropertyPath<Date>(OrderListGen.P_ORDER_DATE_START, this);
            return _orderDateStart;
        }

    /**
     * @return Дата окончания действия приказа.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderDateStop()
     */
        public PropertyPath<Date> orderDateStop()
        {
            if(_orderDateStop == null )
                _orderDateStop = new PropertyPath<Date>(OrderListGen.P_ORDER_DATE_STOP, this);
            return _orderDateStop;
        }

    /**
     * @return Текст приказа.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getOrderText()
     */
        public PropertyPath<String> orderText()
        {
            if(_orderText == null )
                _orderText = new PropertyPath<String>(OrderListGen.P_ORDER_TEXT, this);
            return _orderText;
        }

    /**
     * @return Файл.
     * @see ru.tandemservice.movestudentbasermc.entity.OrderList#getFile()
     */
        public DatabaseFile.Path<DatabaseFile> file()
        {
            if(_file == null )
                _file = new DatabaseFile.Path<DatabaseFile>(L_FILE, this);
            return _file;
        }

        public Class getEntityClass()
        {
            return OrderList.class;
        }

        public String getEntityName()
        {
            return "orderList";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
