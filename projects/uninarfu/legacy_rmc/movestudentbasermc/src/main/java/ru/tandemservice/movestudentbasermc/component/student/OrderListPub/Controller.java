package ru.tandemservice.movestudentbasermc.component.student.OrderListPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.movestudentbasermc.IMovestudentBaseRmcComponents;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uni.entity.employee.Student;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onAddOrder(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(IMovestudentBaseRmcComponents.STUDENT_ORDERLIST_PUB_ADD, new UniMap()
                .add("studentId", getModel(component).getStudent().getId())
        ));
    }


    public void onClickEditOrder(IBusinessComponent component)
    {
        Long _idEdit = component.getListenerParameter();

        activateInRoot(component, new ComponentActivator(IMovestudentBaseRmcComponents.STUDENT_ORDERLIST_PUB_ADD, new UniMap()
                .add("orderListId", _idEdit)
        ));
    }


    public void onClickDeleteOrder(IBusinessComponent component)
    {
        Long _idDel = component.getListenerParameter();
        getDao().delete(_idDel);

        getModel(component).getOrderListDataSource().refresh();
    }


    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        // через модель расширяемого компонента получим студента (почему не срабатывает биндинг - хрен его знает)
        // от тандема вестей нет
        final Student student = ((ru.tandemservice.uni.component.student.StudentPub.Model) component.getModel(component.getName())).getStudent();

        model.setStudent(student);


        // создаем источник данных для списка приказов
        prepareOrderList(component);

        getDao().prepare(getModel(component));
    }

    private void prepareOrderList(IBusinessComponent component) {

        Model model = getModel(component);

        if (model.getOrderListDataSource() != null)
            return;

        DynamicListDataSource<OrderList> dataSource = new DynamicListDataSource<>(component,
                                                                                  (_component) -> getDao().prepareCustomDataSource(getModel(_component), getModel(_component).getOrderListDataSource())
        , 10);

        dataSource.addColumn(new SimpleColumn("Номер приказа", OrderList.P_ORDER_NUMBER));
        dataSource.addColumn(new SimpleColumn("Дата приказа", OrderList.P_ORDER_DATE, DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new SimpleColumn("Тип приказа", OrderList.type().title().s()));
        dataSource.addColumn(new SimpleColumn("Примечание", OrderList.P_ORDER_DSK));
        dataSource.addColumn(new SimpleColumn("Дата с", OrderList.P_ORDER_DATE_START, DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new SimpleColumn("Дата По", OrderList.P_ORDER_DATE_STOP, DateFormatter.DEFAULT_DATE_FORMATTER));

        dataSource.addColumn(CommonBaseUtil.getPrintColumn("orderListPub:onClickPrint", "Печатать файл").setEnabledProperty("attachmentExist").setPermissionKey("rmcActionTabStudentOrdersList"));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "orderListPub:onClickEditOrder").setPermissionKey("rmcActionTabStudentOrdersList"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "orderListPub:onClickDeleteOrder", "Удалить ").setPermissionKey("rmcActionTabStudentOrdersList"));


        model.setOrderListDataSource(dataSource);


    }

    public void onClickPrint(IBusinessComponent component)
    {
        Model model = getModel(component);

        OrderList order = model.getOrderListDataSource().getRecordById((Long) component.getListenerParameter());

        byte[] content = order.getFile().getContent();
        if (content == null)
            throw new ApplicationException("Файл приказа пуст.");
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, order.getFile().getFilename());
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", id)));
    }


}
