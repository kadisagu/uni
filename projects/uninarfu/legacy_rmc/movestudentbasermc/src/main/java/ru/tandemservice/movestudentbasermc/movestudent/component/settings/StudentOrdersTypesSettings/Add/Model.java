package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings.Add;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StudentOrdersTypes;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

import java.util.List;

public class Model {

    // studentExtractType
    // ordersTypes
    private ISelectModel orderTypesList;

    private List<HSelectOption> studentExtractTypeList;

    private StudentOrdersTypes ordersTypes;

    private StudentExtractType studentExtractType;

    public void setStudentExtractTypeList(List<HSelectOption> studentExtractTypeList) {
        this.studentExtractTypeList = studentExtractTypeList;
    }

    public List<HSelectOption> getStudentExtractTypeList() {
        return studentExtractTypeList;
    }

    public void setOrdersTypes(StudentOrdersTypes ordersTypes) {
        this.ordersTypes = ordersTypes;
    }

    public StudentOrdersTypes getOrdersTypes() {
        return ordersTypes;
    }

    public void setStudentExtractType(StudentExtractType studentExtractType) {
        this.studentExtractType = studentExtractType;
    }

    public StudentExtractType getStudentExtractType() {
        return studentExtractType;
    }

    public void setOrderTypesList(ISelectModel orderTypesList) {
        this.orderTypesList = orderTypesList;
    }

    public ISelectModel getOrderTypesList() {
        return orderTypesList;
    }


}
