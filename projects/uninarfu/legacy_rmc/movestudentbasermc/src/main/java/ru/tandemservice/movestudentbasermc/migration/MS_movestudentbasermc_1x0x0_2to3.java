package ru.tandemservice.movestudentbasermc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * Пропуск миграции в версии 2.3.3
 * <p/>
 * MS_movestudent_2x3x3_1to2.java
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_movestudentbasermc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        String[] badParagraphTitles = {
                "Выписка из списочного приказа. О допуске к сдаче государственного экзамена. Допустить к сдаче государственного экзамена",
                "Выписка из списочного приказа. О допуске к защите выпускной квалификационной работы. Допустить к защите выпускной квалификационной работы",
                "Выписка из списочного приказа. О назначении старостами учебных групп. Назначить старостой учебной группы",
                "Выписка из списочного приказа. О переводе с курса на следующий курс (по направлениям подготовки). Перевести на следующий курс",
                "Выписка из списочного приказа. О назначении академической стипендии. Назначить академическую стипендию",
                "Выписка из списочного приказа. О назначении социальной стипендии. Назначить социальную стипендию",
                "Выписка из списочного приказа. Об оказании материальной помощи. Оказать материальную помощь",
                "Выписка из списочного приказа. Об однократном повышении государственной академической стипендии. Однократно повысить государственную академическую стипендию",
                "Выписка из списочного приказа. О назначении надбавки к государственной академической стипендии. Назначить надбавку к государственной академической стипендии"
        };

        String[] goodParagraphTitles = {
                "Шаблон подпараграфа списочного приказа. О допуске к сдаче государственного экзамена. Допустить к сдаче государственного экзамена",
                "Шаблон подпараграфа списочного приказа. О допуске к защите выпускной квалификационной работы. Допустить к защите выпускной квалификационной работы",
                "Шаблон подпараграфа списочного приказа. О назначении старостами учебных групп. Назначить старостой учебной группы",
                "Шаблон подпараграфа списочного приказа. О переводе с курса на следующий курс (по направлениям подготовки). Перевести на следующий курс",
                "Шаблон подпараграфа списочного приказа. О назначении академической стипендии. Назначить академическую стипендию",
                "Шаблон подпараграфа списочного приказа. О назначении социальной стипендии. Назначить социальную стипендию",
                "Шаблон подпараграфа списочного приказа. Об оказании материальной помощи. Оказать материальную помощь",
                "Шаблон подпараграфа списочного приказа. Об однократном повышении государственной академической стипендии. Однократно повысить государственную академическую стипендию",
                "Шаблон подпараграфа списочного приказа. О назначении надбавки к государственной академической стипендии. Назначить надбавку к государственной академической стипендии"
        };

        PreparedStatement st3 = tool.prepareStatement("update movestudenttemplate_t set title_p=? where title_p=? and index_p=3");

        int counter = 0;
        for (int i = 0; i < badParagraphTitles.length; i++) {
            st3.setString(1, goodParagraphTitles[i]);
            st3.setString(2, badParagraphTitles[i]);
            counter += st3.executeUpdate();
        }

        PreparedStatement st4 = tool.prepareStatement("update movestudenttemplate_t set title_p=? where title_p=? and index_p=4");

        st4.setString(1, "Выписка из списочного приказа. О допуске к сдаче государственного экзамена. Допустить к сдаче государственного экзамена");
        st4.setString(2, "Шаблон подподпараграфа списочного приказа. О допуске к сдаче государственного экзамена. Допустить к сдаче государственного экзамена");

        counter += st4.executeUpdate();

        System.out.println(getClass().getSimpleName() + ": fixed " + counter + " from " + (badParagraphTitles.length + 1));
    }
}