package ru.tandemservice.movestudentbasermc.movestudent.utils;

/**
 * @author Mike
 * @since 01.12.2011
 */
public class ListExtractData_Rmc {
    //список всех entity выписок из списочных приказов (разработанных Ramec)
    public static final String[][] EXTRACT_LIST = new String[][]{
            /*1000*/{"excludeStuListExtractVGNA", "Об отчислении 2", "false", "vgna"},
            /*1001*/{"restorationStuListExtractVGNA", "О восстановлении", "false", "vgna"},
            /*1002*/{"eduEnrAsTransferStuListExtractVGNA", "О переводе из другого вуза", "false", "vgna"},
            /*1003*/{"transferDevConditionStuListExtractVGNA", "О переводе внутри вуза", "false", "vgna"},
            /*1004*/{"changeFioStuListExtractVGNA", "О смене ФИО", "false", "vgna"},
            /*1005*/{"weekendOutStuListExtractVGNA", "О выходе из академического отпуска", "false", "vgna"},
            /*1006*/{"weekendStuListExtractVGNA", "Об академическом отпуске", "false", "vgna"},
            /*1007*/{"transferCompTypeStuListExtractVGNA", "О переводе на другую основу оплаты обучения", "false", "vgna"},      
            /*1008*/{"weekendChildStuListExtractVGNA", "О предоставлении отпуска по уходу за ребенком", "false", "vgna"},
            /*1009*/{"weekendChildOutStuListExtractVGNA", "О выходе из отпуска по уходу за ребенком", "false", "vgna"},
            /*1010*/{"weekendPregnancyStuListExtractVGNA", "О предоставлении отпуска по беременности и родам", "false", "vgna"},
            
            /*1011*/{"excludeAcademStuListExtractVGNA", "Об отчислении за акад.задолж.", "false", "vgna"},
            /*1012*/{"excludeDebtStuListExtractVGNA", "Об отчислении за задолж.оплаты", "false", "vgna"},
            /*1013*/{"excludeDebtAcademStuListExtractVGNA", "Об отчислении за задолж. Акад. и оплаты", "false", "vgna"},
            /*1014*/{"weekendPregnancyOutListExtractVGNA", "О выходе из отпуска по беременности и родам", "false", "vgna"}
    };
}
