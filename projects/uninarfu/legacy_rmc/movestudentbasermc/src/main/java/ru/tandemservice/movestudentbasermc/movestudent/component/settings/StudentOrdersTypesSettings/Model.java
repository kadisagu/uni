package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings;

import org.hibernate.Session;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StdExtractT2OrdersT;
import ru.tandemservice.movestudentbasermc.movestudent.entity.StudentOrdersTypes;

import java.util.ArrayList;
import java.util.List;


public class Model {
    private ISelectModel orderTypesList;

    private IDataSettings _settings;


    private Long dropedID;

    private DynamicListDataSource<Wrapper> _dataSource;

    public void setDataSource(DynamicListDataSource<Wrapper> _dataSource)
    {
        this._dataSource = _dataSource;
    }

    public DynamicListDataSource<Wrapper> getDataSource()
    {
        return _dataSource;
    }


    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public StudentOrdersTypes getOrdersTypes()
    {
        StudentOrdersTypes st = (StudentOrdersTypes) getSettings().get("StudentOrdersTypes");
        return st;
    }

    public void setOrdersTypes(StudentOrdersTypes st)
    {
        getSettings().set("StudentOrdersTypes", st);
    }

    public List<Wrapper> getWraper(
            EntityOrder eo, Session session)
    {

        List<Wrapper> _retval = new ArrayList<Wrapper>();

        // загоним во враппер слоты
        MQBuilder builder = new MQBuilder(StdExtractT2OrdersT.ENTITY_CLASS, "slot");
        builder.addJoin("slot", StdExtractT2OrdersT.L_STUDENT_ORDERS_TYPES, "doc");

        if (this.getOrdersTypes() != null)
            builder.add(MQExpression.eq("doc", StudentOrdersTypes.P_ID, this.getOrdersTypes().getId()));


        List<StdExtractT2OrdersT> result = builder.getResultList(session);

        if (result != null) {
            for (StdExtractT2OrdersT w : result) {
                _retval.add(new Wrapper(w));
            }
        }

        return _retval;
    }

    public void setOrderTypesList(ISelectModel orderTypesList) {
        this.orderTypesList = orderTypesList;
    }

    public ISelectModel getOrderTypesList() {
        return orderTypesList;
    }

    public void setDropedID(Long id)
    {
        dropedID = id;

    }

    public Long getDroppedID()
    {
        return dropedID;
    }
}
