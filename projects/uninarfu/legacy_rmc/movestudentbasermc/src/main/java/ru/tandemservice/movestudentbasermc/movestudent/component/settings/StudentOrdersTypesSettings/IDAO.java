package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IUniDao;

import java.util.List;

public interface IDAO extends IUniDao<Model> {
    void restoreDefaults(Model model);

    <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource);

    void validateDelete(List<Long> entityIds, ErrorCollector errors);
}
