package ru.tandemservice.movestudentbasermc.movestudent.utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.SystemUtil;
import ru.tandemservice.uni.util.system.UniSystemUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Генератор печатных форм
 * <p/>
 * WORD разрывает метки! ХЗ как править в общем случае поэтому сгенерим все печ. формы (:
 * при запуске указывать:
 * -Dmodules=<путь до tandem.uni> , -DmoduleOwner=movestudent  и -DramecProject=spbstu.
 */
public class TemplatesGenerator {
    private final static String EXTRACT_HEADER = "@extractHeader.txt";
    private final static String EXTRACT_TOP = "@extractTop.txt";
    private final static String EXTRACT_BOTTOM = "@extractBottom.txt";
    private final static String MODULAR_ORDER = "@modularOrder.txt";

    public static void main(String[] args) throws IOException
    {
        File modules = UniSystemUtils.getModulesDir();
        String moduleOwner = System.getProperty("moduleOwner");
        String ramecProject = System.getProperty("ramecProject");

        if (StringUtils.isEmpty(ramecProject))
            throw new RuntimeException("Plz specify printforms ramecProject: for example 'spbstu'");

        if (StringUtils.isEmpty(moduleOwner))
            throw new RuntimeException("Plz specify printforms moduleOwner: for example 'movestudent'");

        String printformsPath = modules.getAbsolutePath() + "/" + ramecProject + "/src/main/java/ru/ramec/" + ramecProject + "/" + moduleOwner + "/utils/system/templates/printforms/";
        String templatesPath = modules.getAbsolutePath() + "/" + ramecProject + "/src/main/resources/" + ramecProject + "/" + moduleOwner + "/templates/modularorder/";

        byte[] extractHeaderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_HEADER));
        byte[] extractTopUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_TOP));
        byte[] extractBottomUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + EXTRACT_BOTTOM));
        byte[] modularOrderUTF8 = FileUtils.readFileToByteArray(new File(printformsPath + "/" + MODULAR_ORDER));

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        // create modularorder

        FileUtils.writeByteArrayToFile(new File(templatesPath + "/modularOrder.rtf"), getRtfData(modularOrderUTF8));

        // create extracts
        File path = new File(printformsPath);
        for (File file : path.listFiles()) {
            if (file.getName().endsWith(".txt") && !file.getName().startsWith("@")) {
                // create extract printform
                out.reset();
                out.write(getRtfData(extractHeaderUTF8));
                out.write(getRtfData(extractTopUTF8));
                out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                out.write(getRtfData(extractBottomUTF8));
                out.write("\\par}".getBytes());
                File newFile = new File(templatesPath + "/" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                System.out.println("[+] " + newFile.getAbsolutePath());
                // create extract in modular order print form

                out.reset();
                out.write(getRtfData(extractHeaderUTF8));
                out.write(getRtfData(FileUtils.readFileToByteArray(file)));
                out.write("\\par}".getBytes());
                newFile = new File(templatesPath + "/modular_" + FilenameUtils.getBaseName(file.getName()) + ".rtf");
                FileUtils.writeByteArrayToFile(newFile, out.toByteArray());
                System.out.println("[+] " + newFile.getAbsolutePath());
            }
        }
    }

    /**
     * My_Param} -> {\field{\*\fldinst{COMMENTS My_Param \\* MERGEFORMAT}}{\fldrslt{My_Param}}}
     * \n -> \par
     *
     * @param readableDataUTF - массив байт в кодировке UTF-8 шаблона выписки
     *
     * @return массив получившихся байт после подстановки параметров
     *
     * @throws IOException если будет ошибка записи в ByteArrayOutputStream
     */
    private static byte[] getRtfData(byte[] readableDataUTF) throws IOException
    {
        byte[] readableData = new String(readableDataUTF, "UTF-8").getBytes("CP1251");

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        for (int i = 0; i < readableData.length; i++) {
            byte c = readableData[i];
            if (c < 0) {
                // русские символы превращаются в rtf-escape-последовательность
                result.write(new byte[]{'\\', '\''});
                result.write(SystemUtil.getBytes(c));
            }
            else if (c == '{') {
                i++;
                boolean isParameter = true;
                for (; i < readableData.length && readableData[i] != '}'; i++) {
                    byte b = readableData[i];
                    isParameter = isParameter && ((b >= 'A' && b <= 'Z') || (b >= 'a' && b <= 'z') || b == '_');
                    buffer.write(b);
                }
                if (isParameter)
                    result.write(("{\\field{\\*\\fldinst{COMMENTS " + buffer.toString() + "}}{\\fldrslt{" + buffer.toString() + "}}}").getBytes());
                else {
                    result.write('{');
                    result.write(buffer.toByteArray());
                    result.write('}');
                }
                buffer.reset();
            }
            else
                result.write(c);
        }
        return result.toByteArray();
    }
}

