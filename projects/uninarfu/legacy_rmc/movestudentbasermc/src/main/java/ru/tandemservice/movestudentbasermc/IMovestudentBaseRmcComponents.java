package ru.tandemservice.movestudentbasermc;


// ru.tandemservice.movestudentbasermc.IMovestudentBaseRmcComponents
public interface IMovestudentBaseRmcComponents {
    String STUDENT_ORDERLIST_PUB_ADD = "ru.tandemservice.movestudentbasermc.component.student.OrderListPub.Add";
    String UNIDS_StudentOrdersTypesSettings_ADD = "ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings.Add";
}
