package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentbasermc.IMovestudentBaseRmcComponents;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;
import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        // прежде всего в модели активируем орг юнит
        getDao().prepare(model);

        createDataList(component);

        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey(model)));

    }


    private void createDataList(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<Wrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareCustomDataSource(getModel(component1), getModel(component1).getDataSource());
        }, 10);

        // dataSource.addColumn(new CheckboxColumn().setSelectCaption("Выбор").setClickable(false).setOrderable(false));

        dataSource.addColumn(new SimpleColumn("Тип", "orderType").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Выписка", "orderName").setClickable(false).setOrderable(false));


        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить "));


        model.setDataSource(dataSource);


    }


    protected String getSettingsKey(Model model)
    {
        return "StudentOrdersTypeSettings_" + 100 + ".filter";
    }

    public void onClickSearch(IBusinessComponent context)
    {
        Model model = getModel(context);
        DataSettingsFacade.saveSettings(model.getSettings());
        refreshData(context);
    }

    private void refreshData(final IBusinessComponent context)
    {
        Model model = getModel(context);
        model.getDataSource().refresh();
    }


    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).getSettings().clear();

        getDao().restoreDefaults(getModel(context));

        onClickSearch(context);

    }

    public void onClickAdd(IBusinessComponent component)
    {

        activateInRoot(
                component,
                new ComponentActivator
                        (
                                IMovestudentBaseRmcComponents.UNIDS_StudentOrdersTypesSettings_ADD,
                                new ParametersMap()
                        )
        );

    }


    public void onClickDelete(IBusinessComponent component)
    {
        // удалялка
        List<Long> selectedIds = new ArrayList<>();

        Long _idDel = (Long) component.getListenerParameter();
        selectedIds.add(_idDel);

        ErrorCollector errors = component.getUserContext().getErrorCollector();

        _tryDelete(selectedIds, errors, this.getModel(component));

    }


    private void _tryDelete(List<Long> selectedIds, ErrorCollector errors,
                            Model model)
    {

        getDao().validateDelete(selectedIds, errors);

        if (!errors.hasErrors())
            for (Long id : selectedIds) {
                model.setDropedID(id);
                getDao().update(model);
            }
        model.getDataSource().refresh();

    }
}
