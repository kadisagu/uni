package ru.tandemservice.movestudentbasermc.component.student.OrderListPub;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IUniDao;


/**
 * @author vch
 * @since 16.11.2010
 */
public interface IDAO extends IUniDao<Model> {
    <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource);
}
