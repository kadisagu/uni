package ru.tandemservice.movestudentbasermc.component.student.OrderListPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.uni.entity.employee.Student;

@Output({
        @Bind(key = "studentId", binding = "student.id")
})
public class Model {
    private Student _student = new Student();

    private DynamicListDataSource<OrderList> _orderListDataSource;


    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }


    public String getTestMessage()
    {

        return "it is test message";
    }


    public DynamicListDataSource<OrderList> getOrderListDataSource()
    {
        return _orderListDataSource;
    }

    public void setOrderListDataSource(DynamicListDataSource<OrderList> orderListDataSource)
    {
        _orderListDataSource = orderListDataSource;
    }

}
