package ru.tandemservice.movestudentbasermc.component.student.OrderListPub.Add;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);
    }


    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        getDao().validate(model, errors);
        if (errors.hasErrors()) {
            ContextLocal.getErrorCollector().add("Форма заполнена с ошибками");
            return;
        }

        getDao().update(model);

        deactivate(component);
    }

    public void onClickPrint(IBusinessComponent component)
    {
        Model model = component.getModel();

        DatabaseFile file = model.getFile();
        byte[] content = file.getContent();
        if (content == null)
            throw new ApplicationException("Файл приказа пуст.");

        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, file.getFilename());
        activateInRoot(component, new ComponentActivator(IUniComponents.PRINT_REPORT, new UniMap().add("id", id)));
    }

    public void onClickDelFile(IBusinessComponent component)
    {
        Model model = component.getModel();
        getDao().deleteFile(model);
        onRefreshComponent(component);
    }


}
