package ru.tandemservice.movestudentbasermc.movestudent.component.settings.StudentOrdersTypesSettings.Add;

import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.dao.IUniDao;

import java.util.List;

public interface IDAO extends IUniDao<Model> {
    void validateDelete(List<Long> entityIds, ErrorCollector errors);
}
