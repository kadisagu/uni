package ru.tandemservice.competencermc.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.competencermc.entity.gen.EppEpvRegistryRowExtGen;
import ru.tandemservice.uniepp.UniEppUtils;

/**
 * Расширение сущности запись в УП(в)
 */
public class EppEpvRegistryRowExt extends EppEpvRegistryRowExtGen {
    public static Double wrap(long value)
    {
        return UniEppUtils.wrap(value >= 0L ? Long.valueOf(value) : null);
    }

    public static long unwrap(Double value)
    {
        Long load = UniEppUtils.unwrap(value);
        return null != load ? load.longValue() : -1L;
    }

    @EntityDSLSupport(parts = {P_VARIATION_LOAD})
    public Double getVariationLoadAsDouble()
    {
        return wrap(getVariationLoad() == null ? -1L : getVariationLoad());
    }

    public void setVariationLoadAsDouble(Double value)
    {
        setVariationLoad(unwrap(value));
    }

    @EntityDSLSupport(parts = {P_MANDATORY_LOAD})
    public Double getMandatoryLoadAsDouble()
    {
        return wrap(getMandatoryLoad() == null ? -1L : getMandatoryLoad());
    }

    public void setMandatoryLoadAsDouble(Double value)
    {
        setMandatoryLoad(unwrap(value));
    }
}