package ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.ExplanatoryNoteTab;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {

        RelEduPlanVersionToExplanatoryNote explanatoryNote = get(RelEduPlanVersionToExplanatoryNote.class, RelEduPlanVersionToExplanatoryNote.eduPlanVersion(), model.getEduplanVersion());

        if (explanatoryNote == null) {
            explanatoryNote = new RelEduPlanVersionToExplanatoryNote();
            explanatoryNote.setEduPlanVersion(model.getEduplanVersion());
        }

        model.setExplanatoryNote(explanatoryNote);
    }

    @Override
    public void prepareListDataSource(Model model) {

        DynamicListDataSource<RelEduPlanVersionToUsedAudience> dataSource = model.getDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelEduPlanVersionToUsedAudience.class, "rel")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RelEduPlanVersionToUsedAudience.eduPlanVersion().fromAlias("rel")), model.getEduplanVersion()))
                .order(DQLExpressions.property(RelEduPlanVersionToUsedAudience.usedAudience().audienceType().priority().fromAlias("rel")));

        dataSource.setCountRow(getList(builder).size());

        UniBaseUtils.createPage(dataSource, builder, getSession());
    }

    @Override
    public void update(Model model) {
        saveOrUpdate(model.getExplanatoryNote());
    }
}
