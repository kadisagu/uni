package ru.tandemservice.competencermc.component.catalog.audienceType.AudienceTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.competencermc.entity.catalog.AudienceType;

public interface IDAO extends IDefaultCatalogAddEditDAO<AudienceType, Model> {

}
