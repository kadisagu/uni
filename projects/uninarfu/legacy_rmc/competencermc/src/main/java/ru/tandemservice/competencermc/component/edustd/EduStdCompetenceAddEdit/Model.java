package ru.tandemservice.competencermc.component.edustd.EduStdCompetenceAddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

@Input({
        @org.tandemframework.core.component.Bind(key = "publisherId", binding = "eppStateEduStandard.id"),
        @org.tandemframework.core.component.Bind(key = "skillId", binding = "skill.id"),
})
public class Model {
    private EppStateEduStandard eppStateEduStandard = new EppStateEduStandard();
    private EppStateEduStandardSkill skill = new EppStateEduStandardSkill();
    private String code;

    private ISelectModel typeModel;
    private ISelectModel groupModel;

    public EppStateEduStandard getEppStateEduStandard() {
        return eppStateEduStandard;
    }

    public void setEppStateEduStandard(EppStateEduStandard eppStateEduStandard) {
        this.eppStateEduStandard = eppStateEduStandard;
    }

    public EppStateEduStandardSkill getSkill() {
        return skill;
    }

    public void setSkill(EppStateEduStandardSkill skill) {
        this.skill = skill;
    }

    public ISelectModel getTypeModel() {
        return typeModel;
    }

    public void setTypeModel(ISelectModel typeModel) {
        this.typeModel = typeModel;
    }

    public ISelectModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel) {
        this.groupModel = groupModel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
