package ru.tandemservice.competencermc.component.catalog.usedAudience.UsedAudiencePub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;

public class Controller extends DefaultCatalogPubController<UsedAudience, Model, IDAO> {

    @Override
    protected void addColumnsBeforeEditColumn(IBusinessComponent component,
                                              DynamicListDataSource<UsedAudience> dataSource)
    {

        dataSource.addColumn(new SimpleColumn("Тип аудитории", UsedAudience.audienceType().title().s()).setClickable(false));
        if (dataSource.getColumn("code") != null)
            dataSource.getColumn("code").setVisible(false);
    }
}
