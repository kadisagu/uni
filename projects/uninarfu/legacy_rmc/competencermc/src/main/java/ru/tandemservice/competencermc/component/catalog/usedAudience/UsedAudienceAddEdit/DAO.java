package ru.tandemservice.competencermc.component.catalog.usedAudience.UsedAudienceAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.competencermc.entity.catalog.AudienceType;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;

public class DAO extends DefaultCatalogAddEditDAO<UsedAudience, Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        model.setAudienceTypeList(getList(AudienceType.class));
        if (model.getTypeId() != null)
            model.getCatalogItem().setAudienceType((AudienceType) getNotNull(model.getTypeId()));
    }
}
