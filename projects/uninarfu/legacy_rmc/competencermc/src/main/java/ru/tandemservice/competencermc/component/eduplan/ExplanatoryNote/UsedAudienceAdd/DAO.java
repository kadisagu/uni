package ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.UsedAudienceAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience;
import ru.tandemservice.competencermc.entity.catalog.AudienceType;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {

        if (model.getTypeId() != null) {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RelEduPlanVersionToUsedAudience.class, "rel")
                    .where(DQLExpressions.eqValue(DQLExpressions.property(RelEduPlanVersionToUsedAudience.eduPlanVersion().id().fromAlias("rel")), model.getEduplanVersionId()))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(RelEduPlanVersionToUsedAudience.usedAudience().audienceType().id().fromAlias("rel")), model.getTypeId()))
                    .column(DQLExpressions.property(RelEduPlanVersionToUsedAudience.usedAudience().fromAlias("rel")));

            model.setAudienceType((AudienceType) getNotNull(model.getTypeId()));
            List<UsedAudience> list = getList(builder);
            model.setUsedAudienceList(list);
        }

        model.setAudienceTypeModel(new LazySimpleSelectModel<AudienceType>(AudienceType.class));
        model.setUsedAudienceModel(new UniQueryFullCheckSelectModel(new String[]{UsedAudience.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(UsedAudience.ENTITY_CLASS, alias)

                        .addOrder(alias, UsedAudience.title());
                if (model.getAudienceType() != null)
                    builder.add(MQExpression.eq(alias, UsedAudience.audienceType(), model.getAudienceType()));

                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, UsedAudience.title(), filter));

                return builder;
            }
        });

        if (model.getCreatedCatalogItemId() != null) {

            Object entity = getNotNull(model.getCreatedCatalogItemId());

            if (entity instanceof AudienceType)
                model.setAudienceType((AudienceType) entity);
            if (entity instanceof UsedAudience) {
                List<UsedAudience> list = new ArrayList<UsedAudience>();
                list.add((UsedAudience) entity);
                if (model.getUsedAudienceList().isEmpty())
                    model.setUsedAudienceList(list);
                else
                    model.getUsedAudienceList().addAll(list);
            }
        }
    }

    @Override
    public void update(Model model) {

        if (!model.getUsedAudienceList().isEmpty()) {

            if (model.getTypeId() != null)
                new DQLDeleteBuilder(RelEduPlanVersionToUsedAudience.class)
                        .where(DQLExpressions.eqValue(DQLExpressions.property(RelEduPlanVersionToUsedAudience.eduPlanVersion().id()), model.getEduplanVersionId()))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(RelEduPlanVersionToUsedAudience.usedAudience().audienceType().id()), model.getTypeId()))
                        .createStatement(getSession()).execute();

            for (UsedAudience audience : model.getUsedAudienceList()) {
                RelEduPlanVersionToUsedAudience usedAudience = new RelEduPlanVersionToUsedAudience();
                usedAudience.setEduPlanVersion((EppEduPlanVersion) get(model.getEduplanVersionId()));
                usedAudience.setUsedAudience(audience);
                saveOrUpdate(usedAudience);
            }
        }

    }

}
