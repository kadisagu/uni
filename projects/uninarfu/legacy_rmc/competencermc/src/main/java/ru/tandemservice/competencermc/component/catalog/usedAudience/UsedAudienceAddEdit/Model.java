package ru.tandemservice.competencermc.component.catalog.usedAudience.UsedAudienceAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import ru.tandemservice.competencermc.entity.catalog.AudienceType;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;

import java.util.List;


@Input({
        @Bind(key = "typeId", binding = "typeId")
})
public class Model extends DefaultCatalogAddEditModel<UsedAudience> {

    private Long typeId;
    private List<AudienceType> audienceTypeList;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public List<AudienceType> getAudienceTypeList() {
        return audienceTypeList;
    }

    public void setAudienceTypeList(List<AudienceType> audienceTypeList) {
        this.audienceTypeList = audienceTypeList;
    }

}
