package ru.tandemservice.competencermc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.competencermc.entity.catalog.AudienceType;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Используемые аудитории
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsedAudienceGen extends EntityBase
 implements INaturalIdentifiable<UsedAudienceGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.competencermc.entity.catalog.UsedAudience";
    public static final String ENTITY_NAME = "usedAudience";
    public static final int VERSION_HASH = 1428967936;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_AUDIENCE_TYPE = "audienceType";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private AudienceType _audienceType;     // Тип аудитории
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Тип аудитории.
     */
    public AudienceType getAudienceType()
    {
        return _audienceType;
    }

    /**
     * @param audienceType Тип аудитории.
     */
    public void setAudienceType(AudienceType audienceType)
    {
        dirty(_audienceType, audienceType);
        _audienceType = audienceType;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsedAudienceGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((UsedAudience)another).getCode());
            }
            setAudienceType(((UsedAudience)another).getAudienceType());
            setTitle(((UsedAudience)another).getTitle());
        }
    }

    public INaturalId<UsedAudienceGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<UsedAudienceGen>
    {
        private static final String PROXY_NAME = "UsedAudienceNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsedAudienceGen.NaturalId) ) return false;

            UsedAudienceGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsedAudienceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsedAudience.class;
        }

        public T newInstance()
        {
            return (T) new UsedAudience();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "audienceType":
                    return obj.getAudienceType();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "audienceType":
                    obj.setAudienceType((AudienceType) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "audienceType":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "audienceType":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "audienceType":
                    return AudienceType.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsedAudience> _dslPath = new Path<UsedAudience>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsedAudience");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.competencermc.entity.catalog.UsedAudience#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Тип аудитории.
     * @see ru.tandemservice.competencermc.entity.catalog.UsedAudience#getAudienceType()
     */
    public static AudienceType.Path<AudienceType> audienceType()
    {
        return _dslPath.audienceType();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.competencermc.entity.catalog.UsedAudience#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends UsedAudience> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private AudienceType.Path<AudienceType> _audienceType;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.competencermc.entity.catalog.UsedAudience#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(UsedAudienceGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Тип аудитории.
     * @see ru.tandemservice.competencermc.entity.catalog.UsedAudience#getAudienceType()
     */
        public AudienceType.Path<AudienceType> audienceType()
        {
            if(_audienceType == null )
                _audienceType = new AudienceType.Path<AudienceType>(L_AUDIENCE_TYPE, this);
            return _audienceType;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.competencermc.entity.catalog.UsedAudience#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UsedAudienceGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return UsedAudience.class;
        }

        public String getEntityName()
        {
            return "usedAudience";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
