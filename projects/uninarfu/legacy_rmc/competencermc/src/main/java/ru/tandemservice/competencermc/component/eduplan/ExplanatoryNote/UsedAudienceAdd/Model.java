package ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.UsedAudienceAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.competencermc.entity.catalog.AudienceType;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;

import java.util.ArrayList;
import java.util.List;

@Input({
        @Bind(key = "publisherId", binding = "eduplanVersionId"),
        @Bind(key = "typeId", binding = "typeId"),
        @Bind(key = "createdCatalogItemId", binding = "createdCatalogItemId")
})
public class Model {

    private Long eduplanVersionId;
    private IMultiSelectModel usedAudienceModel;
    private ISelectModel audienceTypeModel;
    private AudienceType audienceType;
    private Long typeId;
    private Long createdCatalogItemId;
    private List<UsedAudience> usedAudienceList = new ArrayList<UsedAudience>();

    public Long getCreatedCatalogItemId() {
        return createdCatalogItemId;
    }

    public void setCreatedCatalogItemId(Long createdCatalogItemId) {
        this.createdCatalogItemId = createdCatalogItemId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public ISelectModel getAudienceTypeModel() {
        return audienceTypeModel;
    }

    public void setAudienceTypeModel(ISelectModel audienceTypeModel) {
        this.audienceTypeModel = audienceTypeModel;
    }

    public AudienceType getAudienceType() {
        return audienceType;
    }

    public void setAudienceType(AudienceType audienceType) {
        this.audienceType = audienceType;
    }

    public List<UsedAudience> getUsedAudienceList() {
        return usedAudienceList;
    }

    public void setUsedAudienceList(List<UsedAudience> usedAudienceList) {
        this.usedAudienceList = usedAudienceList;
    }

    public Long getEduplanVersionId() {
        return eduplanVersionId;
    }

    public void setEduplanVersionId(Long eduplanVersionId) {
        this.eduplanVersionId = eduplanVersionId;
    }

    public IMultiSelectModel getUsedAudienceModel() {
        return usedAudienceModel;
    }

    public void setUsedAudienceModel(IMultiSelectModel usedAudienceModel) {
        this.usedAudienceModel = usedAudienceModel;
    }

}
