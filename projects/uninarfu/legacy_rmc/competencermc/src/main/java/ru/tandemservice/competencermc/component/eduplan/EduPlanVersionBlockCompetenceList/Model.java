package ru.tandemservice.competencermc.component.eduplan.EduPlanVersionBlockCompetenceList;

import org.tandemframework.core.component.State;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@State({@org.tandemframework.core.component.Bind(key = "blockId", binding = "blockId")})
public class Model {
    private Long blockId;
    private EppEduPlanVersionBlock block;
    private DynamicListDataSource<RowWrapper> dataSource;
    private ISelectModel skillModel;
    private Set<Long> editSet = new HashSet<Long>();
    private Map<Long, EppEpvRegistryRowExt> extMap = new HashMap<Long, EppEpvRegistryRowExt>();

    public boolean isVisibleLoad() {
        return false;//TODO DEV-6870 this.getBlock().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMiddle() || this.getBlock().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBasic();
    }

    public Map<Long, EppEpvRegistryRowExt> getExtMap() {
        return extMap;
    }

    public void setExtMap(Map<Long, EppEpvRegistryRowExt> extMap) {
        this.extMap = extMap;
    }

    public EppEduPlanVersion getVersion() {
        return getBlock().getEduPlanVersion();
    }

    public EppEduPlanVersionBlock getBlock() {
        return block;
    }

    public void setBlock(EppEduPlanVersionBlock block) {
        this.block = block;
    }

    public DynamicListDataSource<RowWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<RowWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getSkillModel() {
        return skillModel;
    }

    public void setSkillModel(ISelectModel skillModel) {
        this.skillModel = skillModel;
    }

    public Set<Long> getEditSet() {
        return editSet;
    }

    public void setEditSet(Set<Long> editSet) {
        this.editSet = editSet;
    }

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }
}
