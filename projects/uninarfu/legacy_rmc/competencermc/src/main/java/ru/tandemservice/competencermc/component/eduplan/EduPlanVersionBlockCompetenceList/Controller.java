package ru.tandemservice.competencermc.component.eduplan.EduPlanVersionBlockCompetenceList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<RowWrapper> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Индекс", "index").setOrderable(false).setWidth(5));
        dataSource.addColumn(new BlockColumn<String>("editIndex", "Индекс для печати").setOrderable(false).setEnabledProperty("editable").setWidth(5));
        dataSource.addColumn(new SimpleColumn("Наименование дисциплины", "row.title").setOrderable(false).setTreeColumn(true));

        //формы контроля
        HeadColumn col = new HeadColumn("Формы контроля", "Формы контроля");
        List<EppFControlActionType> actionList = IUniBaseDao.instance.get().getList(
                EppFControlActionType.class,
                EppFControlActionType.activeInEduPlan(),
                true,
                EppFControlActionType.shortTitle().s());
        for (EppFControlActionType action : actionList)
            col.addColumn(new SimpleColumn(action.getShortTitle(), "action" + action.getCode()).setOrderable(false).setWidth(1));
        dataSource.addColumn(col);

        dataSource.addColumn(new SimpleColumn("Трудоемкость", "totalLabor").setOrderable(false).setWidth(5));

        col = new HeadColumn("Часов", "Часов");
        col.addColumn(new SimpleColumn("Всего", "totalSize").setOrderable(false).setWidth(5));
        col.addColumn(new SimpleColumn("Аудиаторных", "totalASize").setOrderable(false).setWidth(5));
        dataSource.addColumn(col);

        dataSource.addColumn(new BlockColumn<List<EppStateEduStandardSkill>>("editSkill", "Компетенции").setOrderable(false).setEnabledProperty("editable").setWidth(5));

        col = new HeadColumn("Распределение максимальной нагрузки для СПО/НПО", "Распределение максимальной нагрузки для СПО/НПО");
        col.addColumn(new BlockColumn<Number>("editManLoad", "Обязательная").setOrderable(false).setEnabledProperty("editable").setWidth(3).setVisible(model.isVisibleLoad()));
        col.addColumn(new BlockColumn<Number>("editVarLoad", "Вариативная").setOrderable(false).setEnabledProperty("editable").setWidth(3).setVisible(model.isVisibleLoad()));

        dataSource.addColumn(col);

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit").setEnabledProperty("canEdit"));
        dataSource.addColumn(new ActionColumn("Сохранить", "save", "onClickSave").setEnabledProperty("canSave"));

        model.setDataSource(dataSource);
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();

        RowWrapper wrapper = find(model, id);
        model.getEditSet().add(wrapper.getId());
    }

    public void onClickSave(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();

        RowWrapper wrapper = find(model, id);
        model.getEditSet().remove(wrapper.getId());
        ((IDAO) getDao()).save(model, wrapper);
        model.getDataSource().refresh();
    }

    private RowWrapper find(Model model, Long id) {
        for (RowWrapper wrapper : model.getDataSource().getEntityList())
            if (wrapper.getId().equals(id)) return wrapper;

        return null;
    }
}
