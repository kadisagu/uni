package ru.tandemservice.competencermc.component.catalog.usedAudience.UsedAudienceAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;

public interface IDAO extends IDefaultCatalogAddEditDAO<UsedAudience, Model> {

}
