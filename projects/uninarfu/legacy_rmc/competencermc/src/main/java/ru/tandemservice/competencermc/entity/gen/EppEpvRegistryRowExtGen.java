package ru.tandemservice.competencermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности запись в УП(в)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRegistryRowExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt";
    public static final String ENTITY_NAME = "eppEpvRegistryRowExt";
    public static final int VERSION_HASH = 1696026564;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_PRINT_INDEX = "printIndex";
    public static final String P_VARIATION_LOAD = "variationLoad";
    public static final String P_MANDATORY_LOAD = "mandatoryLoad";
    public static final String P_MANDATORY_LOAD_AS_DOUBLE = "mandatoryLoadAsDouble";
    public static final String P_VARIATION_LOAD_AS_DOUBLE = "variationLoadAsDouble";

    private EppEpvRegistryRow _base;     // Запись в УП
    private String _printIndex;     // индекс для печати
    private Long _variationLoad;     // Вариативная нагрузка
    private Long _mandatoryLoad;     // Обязательная нагрузка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись в УП. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEpvRegistryRow getBase()
    {
        return _base;
    }

    /**
     * @param base Запись в УП. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(EppEpvRegistryRow base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return индекс для печати.
     */
    @Length(max=255)
    public String getPrintIndex()
    {
        return _printIndex;
    }

    /**
     * @param printIndex индекс для печати.
     */
    public void setPrintIndex(String printIndex)
    {
        dirty(_printIndex, printIndex);
        _printIndex = printIndex;
    }

    /**
     * @return Вариативная нагрузка.
     */
    public Long getVariationLoad()
    {
        return _variationLoad;
    }

    /**
     * @param variationLoad Вариативная нагрузка.
     */
    public void setVariationLoad(Long variationLoad)
    {
        dirty(_variationLoad, variationLoad);
        _variationLoad = variationLoad;
    }

    /**
     * @return Обязательная нагрузка.
     */
    public Long getMandatoryLoad()
    {
        return _mandatoryLoad;
    }

    /**
     * @param mandatoryLoad Обязательная нагрузка.
     */
    public void setMandatoryLoad(Long mandatoryLoad)
    {
        dirty(_mandatoryLoad, mandatoryLoad);
        _mandatoryLoad = mandatoryLoad;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRegistryRowExtGen)
        {
            setBase(((EppEpvRegistryRowExt)another).getBase());
            setPrintIndex(((EppEpvRegistryRowExt)another).getPrintIndex());
            setVariationLoad(((EppEpvRegistryRowExt)another).getVariationLoad());
            setMandatoryLoad(((EppEpvRegistryRowExt)another).getMandatoryLoad());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRegistryRowExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRegistryRowExt.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRegistryRowExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "printIndex":
                    return obj.getPrintIndex();
                case "variationLoad":
                    return obj.getVariationLoad();
                case "mandatoryLoad":
                    return obj.getMandatoryLoad();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((EppEpvRegistryRow) value);
                    return;
                case "printIndex":
                    obj.setPrintIndex((String) value);
                    return;
                case "variationLoad":
                    obj.setVariationLoad((Long) value);
                    return;
                case "mandatoryLoad":
                    obj.setMandatoryLoad((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "printIndex":
                        return true;
                case "variationLoad":
                        return true;
                case "mandatoryLoad":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "printIndex":
                    return true;
                case "variationLoad":
                    return true;
                case "mandatoryLoad":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return EppEpvRegistryRow.class;
                case "printIndex":
                    return String.class;
                case "variationLoad":
                    return Long.class;
                case "mandatoryLoad":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRegistryRowExt> _dslPath = new Path<EppEpvRegistryRowExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRegistryRowExt");
    }
            

    /**
     * @return Запись в УП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getBase()
     */
    public static EppEpvRegistryRow.Path<EppEpvRegistryRow> base()
    {
        return _dslPath.base();
    }

    /**
     * @return индекс для печати.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getPrintIndex()
     */
    public static PropertyPath<String> printIndex()
    {
        return _dslPath.printIndex();
    }

    /**
     * @return Вариативная нагрузка.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getVariationLoad()
     */
    public static PropertyPath<Long> variationLoad()
    {
        return _dslPath.variationLoad();
    }

    /**
     * @return Обязательная нагрузка.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getMandatoryLoad()
     */
    public static PropertyPath<Long> mandatoryLoad()
    {
        return _dslPath.mandatoryLoad();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getMandatoryLoadAsDouble()
     */
    public static SupportedPropertyPath<Double> mandatoryLoadAsDouble()
    {
        return _dslPath.mandatoryLoadAsDouble();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getVariationLoadAsDouble()
     */
    public static SupportedPropertyPath<Double> variationLoadAsDouble()
    {
        return _dslPath.variationLoadAsDouble();
    }

    public static class Path<E extends EppEpvRegistryRowExt> extends EntityPath<E>
    {
        private EppEpvRegistryRow.Path<EppEpvRegistryRow> _base;
        private PropertyPath<String> _printIndex;
        private PropertyPath<Long> _variationLoad;
        private PropertyPath<Long> _mandatoryLoad;
        private SupportedPropertyPath<Double> _mandatoryLoadAsDouble;
        private SupportedPropertyPath<Double> _variationLoadAsDouble;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись в УП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getBase()
     */
        public EppEpvRegistryRow.Path<EppEpvRegistryRow> base()
        {
            if(_base == null )
                _base = new EppEpvRegistryRow.Path<EppEpvRegistryRow>(L_BASE, this);
            return _base;
        }

    /**
     * @return индекс для печати.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getPrintIndex()
     */
        public PropertyPath<String> printIndex()
        {
            if(_printIndex == null )
                _printIndex = new PropertyPath<String>(EppEpvRegistryRowExtGen.P_PRINT_INDEX, this);
            return _printIndex;
        }

    /**
     * @return Вариативная нагрузка.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getVariationLoad()
     */
        public PropertyPath<Long> variationLoad()
        {
            if(_variationLoad == null )
                _variationLoad = new PropertyPath<Long>(EppEpvRegistryRowExtGen.P_VARIATION_LOAD, this);
            return _variationLoad;
        }

    /**
     * @return Обязательная нагрузка.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getMandatoryLoad()
     */
        public PropertyPath<Long> mandatoryLoad()
        {
            if(_mandatoryLoad == null )
                _mandatoryLoad = new PropertyPath<Long>(EppEpvRegistryRowExtGen.P_MANDATORY_LOAD, this);
            return _mandatoryLoad;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getMandatoryLoadAsDouble()
     */
        public SupportedPropertyPath<Double> mandatoryLoadAsDouble()
        {
            if(_mandatoryLoadAsDouble == null )
                _mandatoryLoadAsDouble = new SupportedPropertyPath<Double>(EppEpvRegistryRowExtGen.P_MANDATORY_LOAD_AS_DOUBLE, this);
            return _mandatoryLoadAsDouble;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt#getVariationLoadAsDouble()
     */
        public SupportedPropertyPath<Double> variationLoadAsDouble()
        {
            if(_variationLoadAsDouble == null )
                _variationLoadAsDouble = new SupportedPropertyPath<Double>(EppEpvRegistryRowExtGen.P_VARIATION_LOAD_AS_DOUBLE, this);
            return _variationLoadAsDouble;
        }

        public Class getEntityClass()
        {
            return EppEpvRegistryRowExt.class;
        }

        public String getEntityName()
        {
            return "eppEpvRegistryRowExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract Double getMandatoryLoadAsDouble();

    public abstract Double getVariationLoadAsDouble();
}
