package ru.tandemservice.competencermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность связь версии УП и пояснительной записки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelEduPlanVersionToExplanatoryNoteGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote";
    public static final String ENTITY_NAME = "relEduPlanVersionToExplanatoryNote";
    public static final int VERSION_HASH = 303867067;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String P_EXPLANATORY_NOTE = "explanatoryNote";

    private EppEduPlanVersion _eduPlanVersion;     // Версия УП
    private String _explanatoryNote;     // Пояснительная записка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия УП. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия УП. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Пояснительная записка.
     */
    public String getExplanatoryNote()
    {
        return _explanatoryNote;
    }

    /**
     * @param explanatoryNote Пояснительная записка.
     */
    public void setExplanatoryNote(String explanatoryNote)
    {
        dirty(_explanatoryNote, explanatoryNote);
        _explanatoryNote = explanatoryNote;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelEduPlanVersionToExplanatoryNoteGen)
        {
            setEduPlanVersion(((RelEduPlanVersionToExplanatoryNote)another).getEduPlanVersion());
            setExplanatoryNote(((RelEduPlanVersionToExplanatoryNote)another).getExplanatoryNote());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelEduPlanVersionToExplanatoryNoteGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelEduPlanVersionToExplanatoryNote.class;
        }

        public T newInstance()
        {
            return (T) new RelEduPlanVersionToExplanatoryNote();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "explanatoryNote":
                    return obj.getExplanatoryNote();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "explanatoryNote":
                    obj.setExplanatoryNote((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "explanatoryNote":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "explanatoryNote":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "explanatoryNote":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelEduPlanVersionToExplanatoryNote> _dslPath = new Path<RelEduPlanVersionToExplanatoryNote>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelEduPlanVersionToExplanatoryNote");
    }
            

    /**
     * @return Версия УП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Пояснительная записка.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote#getExplanatoryNote()
     */
    public static PropertyPath<String> explanatoryNote()
    {
        return _dslPath.explanatoryNote();
    }

    public static class Path<E extends RelEduPlanVersionToExplanatoryNote> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private PropertyPath<String> _explanatoryNote;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия УП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Пояснительная записка.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote#getExplanatoryNote()
     */
        public PropertyPath<String> explanatoryNote()
        {
            if(_explanatoryNote == null )
                _explanatoryNote = new PropertyPath<String>(RelEduPlanVersionToExplanatoryNoteGen.P_EXPLANATORY_NOTE, this);
            return _explanatoryNote;
        }

        public Class getEntityClass()
        {
            return RelEduPlanVersionToExplanatoryNote.class;
        }

        public String getEntityName()
        {
            return "relEduPlanVersionToExplanatoryNote";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
