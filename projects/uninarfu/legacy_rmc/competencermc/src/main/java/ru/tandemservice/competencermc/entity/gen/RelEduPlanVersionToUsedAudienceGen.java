package ru.tandemservice.competencermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность связь версии УП и используемых аудиторий
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelEduPlanVersionToUsedAudienceGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience";
    public static final String ENTITY_NAME = "relEduPlanVersionToUsedAudience";
    public static final int VERSION_HASH = 809175721;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VERSION = "eduPlanVersion";
    public static final String L_USED_AUDIENCE = "usedAudience";

    private EppEduPlanVersion _eduPlanVersion;     // Версия УП
    private UsedAudience _usedAudience;     // Используемые аудитории

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия УП. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersion getEduPlanVersion()
    {
        return _eduPlanVersion;
    }

    /**
     * @param eduPlanVersion Версия УП. Свойство не может быть null.
     */
    public void setEduPlanVersion(EppEduPlanVersion eduPlanVersion)
    {
        dirty(_eduPlanVersion, eduPlanVersion);
        _eduPlanVersion = eduPlanVersion;
    }

    /**
     * @return Используемые аудитории.
     */
    public UsedAudience getUsedAudience()
    {
        return _usedAudience;
    }

    /**
     * @param usedAudience Используемые аудитории.
     */
    public void setUsedAudience(UsedAudience usedAudience)
    {
        dirty(_usedAudience, usedAudience);
        _usedAudience = usedAudience;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelEduPlanVersionToUsedAudienceGen)
        {
            setEduPlanVersion(((RelEduPlanVersionToUsedAudience)another).getEduPlanVersion());
            setUsedAudience(((RelEduPlanVersionToUsedAudience)another).getUsedAudience());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelEduPlanVersionToUsedAudienceGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelEduPlanVersionToUsedAudience.class;
        }

        public T newInstance()
        {
            return (T) new RelEduPlanVersionToUsedAudience();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVersion":
                    return obj.getEduPlanVersion();
                case "usedAudience":
                    return obj.getUsedAudience();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVersion":
                    obj.setEduPlanVersion((EppEduPlanVersion) value);
                    return;
                case "usedAudience":
                    obj.setUsedAudience((UsedAudience) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVersion":
                        return true;
                case "usedAudience":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVersion":
                    return true;
                case "usedAudience":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVersion":
                    return EppEduPlanVersion.class;
                case "usedAudience":
                    return UsedAudience.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelEduPlanVersionToUsedAudience> _dslPath = new Path<RelEduPlanVersionToUsedAudience>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelEduPlanVersionToUsedAudience");
    }
            

    /**
     * @return Версия УП. Свойство не может быть null.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience#getEduPlanVersion()
     */
    public static EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
    {
        return _dslPath.eduPlanVersion();
    }

    /**
     * @return Используемые аудитории.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience#getUsedAudience()
     */
    public static UsedAudience.Path<UsedAudience> usedAudience()
    {
        return _dslPath.usedAudience();
    }

    public static class Path<E extends RelEduPlanVersionToUsedAudience> extends EntityPath<E>
    {
        private EppEduPlanVersion.Path<EppEduPlanVersion> _eduPlanVersion;
        private UsedAudience.Path<UsedAudience> _usedAudience;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия УП. Свойство не может быть null.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience#getEduPlanVersion()
     */
        public EppEduPlanVersion.Path<EppEduPlanVersion> eduPlanVersion()
        {
            if(_eduPlanVersion == null )
                _eduPlanVersion = new EppEduPlanVersion.Path<EppEduPlanVersion>(L_EDU_PLAN_VERSION, this);
            return _eduPlanVersion;
        }

    /**
     * @return Используемые аудитории.
     * @see ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience#getUsedAudience()
     */
        public UsedAudience.Path<UsedAudience> usedAudience()
        {
            if(_usedAudience == null )
                _usedAudience = new UsedAudience.Path<UsedAudience>(L_USED_AUDIENCE, this);
            return _usedAudience;
        }

        public Class getEntityClass()
        {
            return RelEduPlanVersionToUsedAudience.class;
        }

        public String getEntityName()
        {
            return "relEduPlanVersionToUsedAudience";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
