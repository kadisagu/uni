package ru.tandemservice.competencermc.component.eduplan.EduPlanVersionBlockCompetenceList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RowWrapper extends EntityBase implements IHierarchyItem, ITitled {
    private IEppEpvRowWrapper row;
    private Model model;
    private RowWrapper parent;

    private String index;
    private String printIndex;
    private List<EppStateEduStandardSkill> skillList;
    private Double variationLoad;
    private Double mandatoryLoad;
    private boolean pct;

    public RowWrapper(Model model, boolean pct) {
        this.model = model;
        this.pct = pct;
    }

    public Double getVariationLoad() {
        return variationLoad;
    }

    public void setVariationLoad(Double variationLoad) {
        this.variationLoad = variationLoad;
    }

    public Double getMandatoryLoad() {
        return mandatoryLoad;
    }

    public void setMandatoryLoad(Double mandatoryLoad) {
        this.mandatoryLoad = mandatoryLoad;
    }

    public RowWrapper(Model model, IEppEpvRowWrapper row) {
        this.model = model;
        this.row = row;
    }

    public boolean isCanEdit() {
        return isEditable() && !model.getEditSet().contains(getId());
    }

    public boolean isCanSave() {
        return isEditable() && model.getEditSet().contains(getId());
    }

    @Override
    public Long getId() {
        return row == null ? System.nanoTime() : row.getRow().getId();
    }

    public boolean isHasChildren() {
        return !isEditable();
    }

    public boolean isEditable() {
        return (row != null && row.getRow() instanceof EppEpvRegistryRow) ? true : false;
    }

    public IEppEpvRowWrapper getRow() {
        return row;
    }

    public void setRow(IEppEpvRowWrapper row) {
        this.row = row;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getPrintIndex() {
        return printIndex;
    }

    public void setPrintIndex(String printIndex) {
        this.printIndex = printIndex;
    }

    public List<EppStateEduStandardSkill> getSkillList() {
        return skillList;
    }

    public void setSkillList(List<EppStateEduStandardSkill> skillList) {
        this.skillList = skillList;
    }

    /////// формы контроля ///////////
    public String getAction01() {
        return getAction("01");
    }

    public String getAction02() {
        return getAction("02");
    }

    public String getAction03() {
        return getAction("03");
    }

    public String getAction04() {
        return getAction("04");
    }

    public String getAction05() {
        return getAction("05");
    }

    public String getAction06() {
        return getAction("06");
    }

    public String getAction07() {
        return getAction("07");
    }

    public String getAction08() {
        return getAction("08");
    }

    private String getAction(String code) {
        if (!(row != null && row.getRow() instanceof EppEpvRegistryRow))
            return null;

        EppEpvRegistryRow row = (EppEpvRegistryRow) this.row.getRow();

        MQBuilder builder = new MQBuilder(EppEpvRowTermAction.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", EppEpvRowTermAction.rowTerm().row(), row))
                .add(MQExpression.eq("e", EppEpvRowTermAction.controlActionType().catalogCode(), "eppFControlActionType"))
                .add(MQExpression.eq("e", EppEpvRowTermAction.controlActionType().code(), code));
        List<EppEpvRowTermAction> list = IUniBaseDao.instance.get().getList(builder);

        List<String> termList = new ArrayList<String>();
        for (EppEpvRowTermAction action : list) {
            termList.add("" + action.getRowTerm().getTerm().getIntValue());
        }

        return StringUtils.join(termList, ",");
    }

    @Override
    public String getTitle() {
        return row != null ? row.getRow().getTitle() : "";
    }

    public void setParent(RowWrapper parent) {
        this.parent = parent;
    }

    @Override
    public IHierarchyItem getHierarhyParent() {
        return this.parent;
    }

    public Double getTotalLabor() {
        return (row != null && row.getRow() instanceof EppEpvRegistryRow) ? ((EppEpvRegistryRow) row.getRow()).getTotalLaborAsDouble() : null;
    }

    public Double getTotalSize() {
        return (row != null && row.getRow() instanceof EppEpvRegistryRow) ? ((EppEpvRegistryRow) row.getRow()).getTotalSizeAsDouble() : null;
    }

    public Double getTotalASize() {
        if (!(row != null && row.getRow() instanceof EppEpvRegistryRow))
            return null;

        MQBuilder builder = new MQBuilder(EppEpvRowLoad.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", EppEpvRowLoad.row(), row.getRow()))
                .add(MQExpression.eq("e", EppEpvRowLoad.loadType().catalogCode(), "eppELoadType"))
                .add(MQExpression.eq("e", EppEpvRowLoad.loadType().code(), "1"));

        List<EppEpvRowLoad> list = IUniBaseDao.instance.get().getList(builder);
        return !list.isEmpty() ? list.get(0).getLoadAsDouble() : null;
    }

    public Double getTotalVarLoad() {
        Double sum = 0d;

        if (row != null) {
            Set<IEppEpvRowWrapper> lst = getAllChildren(new HashSet<>(this.row.getChilds()));

            for (IEppEpvRowWrapper row : lst) {
                EppEpvRegistryRowExt ext = model.getExtMap().get(row.getId());

                if (ext != null)
                    sum += ext.getVariationLoadAsDouble() == null ? 0 : ext.getVariationLoadAsDouble();
            }
        }
        else if (!pct) {
            for (EppEpvRegistryRowExt ext : model.getExtMap().values()) {
                sum += ext.getVariationLoadAsDouble() == null ? 0 : ext.getVariationLoadAsDouble();
            }
        }
        else {
            return getTotalSum(true).doubleValue();
        }
        return sum;
    }

    public String getTotalVarLoadStr() {
        return pct ? getTotalVarLoad() == 0d ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(getTotalVarLoad()) + "%" : getTotalVarLoad() == 0d ? "" : getTotalVarLoad() + "";
    }

    public String getTotalManLoadStr() {
        return pct ? getTotalManLoad() == 0d ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(getTotalManLoad()) + "%" : getTotalManLoad() == 0d ? "" : getTotalManLoad() + "";
    }

    private Long getTotalSum(boolean isVarLoad) {
        Double sum = 0d;
        Double varSum = 0d;
        for (EppEpvRegistryRowExt ext : model.getExtMap().values()) {
            varSum += ext.getVariationLoadAsDouble() == null ? 0 : ext.getVariationLoadAsDouble();
            sum += ext.getMandatoryLoadAsDouble() == null ? 0 : ext.getMandatoryLoadAsDouble();
        }

        if (varSum + sum == 0)
            return 0L;

        double pct = varSum * 100 / (sum + varSum);

        if (isVarLoad)
            return Math.round(pct);
        else
            return 100 - Math.round(pct);
    }

    public Double getTotalManLoad() {
        Double sum = 0d;

        if (row != null) {
            Set<IEppEpvRowWrapper> lst = getAllChildren(new HashSet<>(this.row.getChilds()));

            for (IEppEpvRowWrapper row : lst) {
                EppEpvRegistryRowExt ext = model.getExtMap().get(row.getId());

                if (ext != null)
                    sum += ext.getMandatoryLoadAsDouble() == null ? 0 : ext.getMandatoryLoadAsDouble();
            }
        }
        else if (!pct) {
            for (EppEpvRegistryRowExt ext : model.getExtMap().values()) {
                sum += ext.getMandatoryLoadAsDouble() == null ? 0 : ext.getMandatoryLoadAsDouble();
            }
        }
        else {
            return getTotalSum(false).doubleValue();
        }
        return sum;
    }

    protected Set<IEppEpvRowWrapper> getAllChildren(Set<IEppEpvRowWrapper> set) {
        Set<IEppEpvRowWrapper> result = new HashSet<>();

        for (IEppEpvRowWrapper row : set) {
            if (row.getChilds() != null && !row.getChilds().isEmpty())
                result.addAll(getAllChildren(new HashSet<>(row.getChilds())));
            result.add(row);
        }

        return result;
    }

}
