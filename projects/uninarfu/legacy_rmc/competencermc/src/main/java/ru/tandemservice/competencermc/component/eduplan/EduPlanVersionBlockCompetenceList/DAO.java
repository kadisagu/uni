package ru.tandemservice.competencermc.component.eduplan.EduPlanVersionBlockCompetenceList;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.competencermc.entity.EppEpvRegistryRowExt;
import ru.tandemservice.competencermc.entity.EppEpvRegistryRow2Skill;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        model.setBlock(getNotNull(EppEduPlanVersionBlock.class, model.getBlockId()));
        model.setEditSet(new HashSet<>());
        model.setSkillModel(new DQLFullCheckSelectModel("code") {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EppStateEduStandardSkill.class, alias);
                dql.where(DQLExpressions.eq(DQLExpressions.property(EppStateEduStandardSkill.parent().fromAlias(alias)), DQLExpressions.value(model.getVersion().getEduPlan().getParent())));
                dql.order(DQLExpressions.property(EppStateEduStandardSkill.code().fromAlias(alias)));
                FilterUtils.applyLikeFilter(dql, filter, EppStateEduStandardSkill.code().fromAlias(alias));
                return dql;
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model) {
        Map<Long, IEppEpvRowWrapper> map = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(model.getBlock().getId(), true);

        Map<Long, EppEpvRegistryRowExt> extMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEpvRegistryRowExt.class, "ext")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRegistryRowExt.base().id().fromAlias("ext")), map.keySet()));

        List<EppEpvRegistryRowExt> lst = getList(builder);

        for (EppEpvRegistryRowExt row : lst)
            extMap.put(row.getBase().getId(), row);

        List<RowWrapper> resultList = new ArrayList<>();
        Map<Long, RowWrapper> resultMap = new LinkedHashMap<>();

        for (IEppEpvRowWrapper iwrapper : map.values()) {
            RowWrapper wrapper = new RowWrapper(model, iwrapper);
            wrapper.setIndex(iwrapper.getIndex());

            EppEpvRegistryRowExt ext = extMap.get(wrapper.getId());
            if (ext != null) {
                wrapper.setPrintIndex(ext.getPrintIndex());
                wrapper.setVariationLoad(ext.getVariationLoadAsDouble());
                wrapper.setMandatoryLoad(ext.getMandatoryLoadAsDouble());
            }
            else
                wrapper.setPrintIndex(wrapper.getIndex());

            List<EppEpvRegistryRow2Skill> list = getList(EppEpvRegistryRow2Skill.class, EppEpvRegistryRow2Skill.row().id(), wrapper.getRow().getRow().getId());
            List<EppStateEduStandardSkill> skillList = CommonBaseEntityUtil.getPropertiesList(list, EppEpvRegistryRow2Skill.skill());
            wrapper.setSkillList(skillList);

            resultList.add(wrapper);
            resultMap.put(wrapper.getId(), wrapper);
        }

        for (RowWrapper wrapper : resultList)
            if (wrapper.getRow().getHierarhyParent() != null)
                wrapper.setParent(resultMap.get(wrapper.getRow().getHierarhyParent().getId()));

        Map<Long, String> indexMap = new HashMap<>();
        Map<Long, Number> varLoadMap = new HashMap<>();
        Map<Long, Number> manLoadMap = new HashMap<>();
        Map<Long, List<EppStateEduStandardSkill>> skillMap = new HashMap<>();
        for (RowWrapper wrapper : resultList) {
            indexMap.put(wrapper.getId(), wrapper.getPrintIndex());
            skillMap.put(wrapper.getId(), wrapper.getSkillList());
            varLoadMap.put(wrapper.getId(), wrapper.getVariationLoad());
            manLoadMap.put(wrapper.getId(), wrapper.getMandatoryLoad());
        }
        ((BlockColumn<String>) model.getDataSource().getColumn("editIndex")).setValueMap(indexMap);
        ((BlockColumn<List<EppStateEduStandardSkill>>) model.getDataSource().getColumn("editSkill")).setValueMap(skillMap);
        ((BlockColumn<Number>) model.getDataSource().getColumn("editManLoad")).setValueMap(manLoadMap);
        ((BlockColumn<Number>) model.getDataSource().getColumn("editVarLoad")).setValueMap(varLoadMap);

        RowWrapper wrapper = new RowWrapper(model, false);
        resultList.add(wrapper);
        wrapper = new RowWrapper(model, true);
        resultList.add(wrapper);

        model.getDataSource().setCountRow(resultList.size());
        UniBaseUtils.createPage(model.getDataSource(), resultList);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void save(Model model, RowWrapper wrapper) {
        Map<Long, String> indexMap = ((BlockColumn<String>) model.getDataSource().getColumn("editIndex")).getValueMap();
        Map<Long, Number> manLoadMap = ((BlockColumn<Number>) model.getDataSource().getColumn("editManLoad")).getValueMap();
        Map<Long, Number> varLoadMap = ((BlockColumn<Number>) model.getDataSource().getColumn("editVarLoad")).getValueMap();

        EppEpvRegistryRowExt ext = get(EppEpvRegistryRowExt.class, EppEpvRegistryRowExt.base().id(), wrapper.getRow().getRow().getId());
        if (ext == null) {
            ext = new EppEpvRegistryRowExt();
            ext.setBase((EppEpvRegistryRow) wrapper.getRow().getRow());
        }
        ext.setPrintIndex(indexMap.get(wrapper.getId()));
        if (model.isVisibleLoad()) {
            Double manValue = null;
            if (manLoadMap.get(wrapper.getId()) != null)
                manValue = manLoadMap.get(wrapper.getId()).doubleValue();
            Double varValue;
            if (varLoadMap.get(wrapper.getId()) != null)
                varValue = varLoadMap.get(wrapper.getId()).doubleValue();
            else if (manValue != null)
                varValue = wrapper.getTotalSize() - manValue;
            else
                varValue = 0d;

            ext.setMandatoryLoadAsDouble(manValue);
            ext.setVariationLoadAsDouble(varValue);
        }
        getSession().saveOrUpdate(ext);

        Map<Long, List<EppStateEduStandardSkill>> skillMap = ((BlockColumn<List<EppStateEduStandardSkill>>) model.getDataSource().getColumn("editSkill")).getValueMap();
        new DQLDeleteBuilder(EppEpvRegistryRow2Skill.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEpvRegistryRow2Skill.row().id()), wrapper.getId()))
                .createStatement(getSession())
                .execute();
        for (EppStateEduStandardSkill skill : skillMap.get(wrapper.getId())) {
            EppEpvRegistryRow2Skill rel = new EppEpvRegistryRow2Skill();
            rel.setRow((EppEpvRegistryRow) wrapper.getRow().getRow());
            rel.setSkill(skill);

            getSession().saveOrUpdate(rel);
        }
    }
}
