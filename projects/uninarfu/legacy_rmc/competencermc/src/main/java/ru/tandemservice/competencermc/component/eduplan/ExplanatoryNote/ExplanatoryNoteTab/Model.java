package ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.ExplanatoryNoteTab;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToExplanatoryNote;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;


@Input({@org.tandemframework.core.component.Bind(key = "publisherId", binding = "eduplanVersionHolder.id")})
public class Model {

    private final EntityHolder<EppEduPlanVersion> eduplanVersionHolder = new EntityHolder<EppEduPlanVersion>();
    private RelEduPlanVersionToExplanatoryNote explanatoryNote;
    private DynamicListDataSource<RelEduPlanVersionToUsedAudience> dataSource;

    public DynamicListDataSource<RelEduPlanVersionToUsedAudience> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<RelEduPlanVersionToUsedAudience> dataSource)
    {
        this.dataSource = dataSource;
    }

    public RelEduPlanVersionToExplanatoryNote getExplanatoryNote() {
        return explanatoryNote;
    }

    public void setExplanatoryNote(
            RelEduPlanVersionToExplanatoryNote explanatoryNote)
    {
        this.explanatoryNote = explanatoryNote;
    }

    public EntityHolder<EppEduPlanVersion> getEduplanVersionHolder() {
        return this.eduplanVersionHolder;
    }

    public EppEduPlanVersion getEduplanVersion() {
        return (EppEduPlanVersion) getEduplanVersionHolder().getValue();
    }

}
