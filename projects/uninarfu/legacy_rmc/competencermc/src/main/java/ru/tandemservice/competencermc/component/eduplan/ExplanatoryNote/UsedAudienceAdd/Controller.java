package ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.UsedAudienceAdd;

import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ((IDAO) getDao()).prepare((Model) getModel(component));
    }

    public void onClickApply(IBusinessComponent component) {
        ((IDAO) getDao()).update((Model) getModel(component));
        deactivate(component);
    }

    public void onClickAddType(IBusinessComponent component) {
        Activator activator = new ComponentActivator("org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit",
                                                     new UniMap().add(DefaultCatalogAddEditModel.CATALOG_CODE, "audienceType"));
        ContextLocal.createDesktop("PersonShellDialog", activator);
    }

    public void onClickAddItem(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        Activator activator = new ComponentActivator("ru.tandemservice.competencermc.component.catalog.usedAudience.UsedAudienceAddEdit",
                                                     new UniMap().add(DefaultCatalogAddEditModel.CATALOG_CODE, "usedAudience")
                                                             .add("typeId", model.getAudienceType() != null ? model.getAudienceType().getId() : null)
        );
        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
    }
}
