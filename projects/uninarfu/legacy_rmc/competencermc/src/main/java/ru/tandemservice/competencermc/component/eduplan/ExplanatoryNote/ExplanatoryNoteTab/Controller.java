package ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.ExplanatoryNoteTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.competencermc.entity.RelEduPlanVersionToUsedAudience;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = (Model) getModel(component);

        if (model.getDataSource() != null) return;

        DynamicListDataSource<RelEduPlanVersionToUsedAudience> dataSource = UniBaseUtils.createDataSource(component, ((IDAO) getDao()));

        SimpleMergeIdResolver merge = new SimpleMergeIdResolver(RelEduPlanVersionToUsedAudience.usedAudience().audienceType().id());

        dataSource.addColumn(new SimpleColumn("Тип аудитории", RelEduPlanVersionToUsedAudience.usedAudience().audienceType().title()).setMergeRowIdResolver(merge).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Наименование аудитории", RelEduPlanVersionToUsedAudience.usedAudience().title()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEdit").setMergeRowIdResolver(merge));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Удалить аудиторию?"));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        activate(component, new ComponentActivator("ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.UsedAudienceAdd",
                                                   new UniMap().add("publisherId", model.getEduplanVersion().getId())));
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        Long id = component.getListenerParameter();
        RelEduPlanVersionToUsedAudience rel = UniDaoFacade.getCoreDao().get(id);
        activate(component, new ComponentActivator("ru.tandemservice.competencermc.component.eduplan.ExplanatoryNote.UsedAudienceAdd",
                                                   new UniMap().add("publisherId", model.getEduplanVersion().getId())
                                                           .add("typeId", rel.getUsedAudience().getAudienceType().getId())
        ));
    }

    public void onClickDelete(IBusinessComponent component) {
        ((IDAO) getDao()).deleteRow(component);
    }

    public void onClickSave(IBusinessComponent component) {
        ((IDAO) getDao()).update((Model) getModel(component));
    }
}
