package ru.tandemservice.competencermc.component.eduplan.EduPlanVersionBlockCompetenceList;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    @Transactional
    public void save(Model model, RowWrapper wrapper);
}
