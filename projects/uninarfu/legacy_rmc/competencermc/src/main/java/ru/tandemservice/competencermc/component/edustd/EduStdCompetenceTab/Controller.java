package ru.tandemservice.competencermc.component.edustd.EduStdCompetenceTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.component.edustd.EduStdCompetenceTab.Model;
import ru.tandemservice.uniepp.component.edustd.EduStdCompetenceTab.Model.SkillGroupBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

public class Controller extends ru.tandemservice.uniepp.component.edustd.EduStdCompetenceTab.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);
        Model model = getModel(component);

        for (SkillGroupBlock block : model.getBlockList())
            fillTable(block);
    }

    protected void fillTable(SkillGroupBlock block) {
        StaticListDataSource<EppStateEduStandardSkill> dataSource = block.getDataSource();
        if (dataSource == null || dataSource.getColumn("edit") != null)
            return;

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEdit"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete"));
    }

    public void onClickEdit(IBusinessComponent component) {
        Model model = getModel(component);
        Long id = component.getListenerParameter();
        activate(component, new ComponentActivator(
                ru.tandemservice.competencermc.component.edustd.EduStdCompetenceAddEdit.Model.class.getPackage().getName(),
                new UniMap().add("publisherId", model.getEppStateEduStandard().getId()).add("skillId", id)
        ));
    }

    public void onClickAdd(IBusinessComponent component) {
        Model model = getModel(component);
        activate(component, new ComponentActivator(
                ru.tandemservice.competencermc.component.edustd.EduStdCompetenceAddEdit.Model.class.getPackage().getName(),
                new UniMap().add("publisherId", model.getEppStateEduStandard().getId()).add("skillId", null)
        ));
    }

    public void onClickDelete(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        IUniBaseDao.instance.get().delete(id);

        onRefreshComponent(component);
    }
}
