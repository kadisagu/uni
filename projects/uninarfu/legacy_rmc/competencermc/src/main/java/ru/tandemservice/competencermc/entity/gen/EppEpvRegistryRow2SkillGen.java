package ru.tandemservice.competencermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.competencermc.entity.EppEpvRegistryRow2Skill;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь между записью УП(в) и компетенцией
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEpvRegistryRow2SkillGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.competencermc.entity.EppEpvRegistryRow2Skill";
    public static final String ENTITY_NAME = "eppEpvRegistryRow2Skill";
    public static final int VERSION_HASH = 885913751;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String L_SKILL = "skill";

    private EppEpvRegistryRow _row;     // Запись в УП
    private EppStateEduStandardSkill _skill;     // Компетенция

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Запись в УП. Свойство не может быть null.
     */
    @NotNull
    public EppEpvRegistryRow getRow()
    {
        return _row;
    }

    /**
     * @param row Запись в УП. Свойство не может быть null.
     */
    public void setRow(EppEpvRegistryRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Компетенция. Свойство не может быть null.
     */
    @NotNull
    public EppStateEduStandardSkill getSkill()
    {
        return _skill;
    }

    /**
     * @param skill Компетенция. Свойство не может быть null.
     */
    public void setSkill(EppStateEduStandardSkill skill)
    {
        dirty(_skill, skill);
        _skill = skill;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEpvRegistryRow2SkillGen)
        {
            setRow(((EppEpvRegistryRow2Skill)another).getRow());
            setSkill(((EppEpvRegistryRow2Skill)another).getSkill());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEpvRegistryRow2SkillGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEpvRegistryRow2Skill.class;
        }

        public T newInstance()
        {
            return (T) new EppEpvRegistryRow2Skill();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "skill":
                    return obj.getSkill();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppEpvRegistryRow) value);
                    return;
                case "skill":
                    obj.setSkill((EppStateEduStandardSkill) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "skill":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "skill":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppEpvRegistryRow.class;
                case "skill":
                    return EppStateEduStandardSkill.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEpvRegistryRow2Skill> _dslPath = new Path<EppEpvRegistryRow2Skill>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEpvRegistryRow2Skill");
    }
            

    /**
     * @return Запись в УП. Свойство не может быть null.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRow2Skill#getRow()
     */
    public static EppEpvRegistryRow.Path<EppEpvRegistryRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Компетенция. Свойство не может быть null.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRow2Skill#getSkill()
     */
    public static EppStateEduStandardSkill.Path<EppStateEduStandardSkill> skill()
    {
        return _dslPath.skill();
    }

    public static class Path<E extends EppEpvRegistryRow2Skill> extends EntityPath<E>
    {
        private EppEpvRegistryRow.Path<EppEpvRegistryRow> _row;
        private EppStateEduStandardSkill.Path<EppStateEduStandardSkill> _skill;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Запись в УП. Свойство не может быть null.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRow2Skill#getRow()
     */
        public EppEpvRegistryRow.Path<EppEpvRegistryRow> row()
        {
            if(_row == null )
                _row = new EppEpvRegistryRow.Path<EppEpvRegistryRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Компетенция. Свойство не может быть null.
     * @see ru.tandemservice.competencermc.entity.EppEpvRegistryRow2Skill#getSkill()
     */
        public EppStateEduStandardSkill.Path<EppStateEduStandardSkill> skill()
        {
            if(_skill == null )
                _skill = new EppStateEduStandardSkill.Path<EppStateEduStandardSkill>(L_SKILL, this);
            return _skill;
        }

        public Class getEntityClass()
        {
            return EppEpvRegistryRow2Skill.class;
        }

        public String getEntityName()
        {
            return "eppEpvRegistryRow2Skill";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
