package ru.tandemservice.competencermc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_competencermc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность audienceType

        // создана новая сущность
        if (!tool.tableExists("audiencetype_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("audiencetype_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("priority_p", DBType.INTEGER),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("audienceType");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность relEduPlanVersionToExplanatoryNote

        // создана новая сущность
        if (!tool.tableExists("rledplnvrsntexplntrynt_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("rledplnvrsntexplntrynt_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("eduplanversion_id", DBType.LONG).setNullable(false),
                                      new DBColumn("explanatorynote_p", DBType.TEXT)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("relEduPlanVersionToExplanatoryNote");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность relEduPlanVersionToUsedAudience

        // создана новая сущность
        if (!tool.tableExists("rledplnvrsntusdadnc_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("rledplnvrsntusdadnc_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("eduplanversion_id", DBType.LONG).setNullable(false),
                                      new DBColumn("usedaudience_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("relEduPlanVersionToUsedAudience");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность usedAudience

        // создана новая сущность
        if (!tool.tableExists("usedaudience_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("usedaudience_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("audiencetype_id", DBType.LONG),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usedAudience");

        }


    }
}