package ru.tandemservice.competencermc.component.catalog.usedAudience.UsedAudienceItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;

public interface IDAO extends IDefaultCatalogItemPubDAO<UsedAudience, Model> {

}
