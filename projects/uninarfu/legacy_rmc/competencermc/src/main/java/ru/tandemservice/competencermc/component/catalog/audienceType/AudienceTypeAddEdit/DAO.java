package ru.tandemservice.competencermc.component.catalog.audienceType.AudienceTypeAddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import ru.tandemservice.competencermc.entity.catalog.AudienceType;

public class DAO extends DefaultCatalogAddEditDAO<AudienceType, Model> implements IDAO {

    @Override
    public void validate(Model model, ErrorCollector errors) {
        super.validate(model, errors);

        AudienceType type = get(AudienceType.class, AudienceType.priority(), model.getCatalogItem().getPriority());

        if (type != null) {
            errors.add("Элемент с приоритетом «" + model.getCatalogItem().getPriority() + "» уже существует.", "priority");
        }

    }
}
