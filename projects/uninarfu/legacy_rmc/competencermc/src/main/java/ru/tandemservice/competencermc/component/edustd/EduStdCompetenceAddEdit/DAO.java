package ru.tandemservice.competencermc.component.edustd.EduStdCompetenceAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.uniepp.entity.catalog.EppSkillType;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardSkill;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setEppStateEduStandard(getNotNull(EppStateEduStandard.class, model.getEppStateEduStandard().getId()));

        EppStateEduStandardSkill skill = model.getSkill();
        if (skill.getId() != null) {
            skill = getNotNull(EppStateEduStandardSkill.class, skill.getId());
            model.setSkill(skill);
        }
        else
            skill.setSkillType(getCatalogItem(EppSkillType.class, "4"));

        if (!StringUtils.isEmpty(skill.getCode())) {
            String[] arr = skill.getCode().split("-");
            model.setCode(arr[1]);
        }

        model.setGroupModel(new LazySimpleSelectModel<EppSkillGroup>(EppSkillGroup.class));
        model.setTypeModel(new LazySimpleSelectModel<EppSkillType>(EppSkillType.class));
    }

    @Override
    public void update(Model model) {
        String code = model.getSkill().getSkillGroup().getShortTitle() + "-" + model.getCode();
        model.getSkill().setCode(code);

        model.getSkill().setParent(model.getEppStateEduStandard());

        getSession().saveOrUpdate(model.getSkill());
    }
}
