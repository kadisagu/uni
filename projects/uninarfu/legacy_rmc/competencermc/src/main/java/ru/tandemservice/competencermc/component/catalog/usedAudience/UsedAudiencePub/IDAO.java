package ru.tandemservice.competencermc.component.catalog.usedAudience.UsedAudiencePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.competencermc.entity.catalog.UsedAudience;

public interface IDAO extends IDefaultCatalogPubDAO<UsedAudience, Model> {

}
