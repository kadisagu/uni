package ru.tandemservice.unibasermc.component.settings.StudentStatusSettings;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

public class Wrapper extends EntityBase {

    private StudentStatus studentStatus;
    private boolean archiveable;
    private Long Id;

    @Override
    public Long getId() {
        return this.Id;
    }

    @Override
    public void setId(Long id) {
        this.Id = id;
    }

    public StudentStatus getStudentStatus() {
        return studentStatus;
    }

    public void setStudentStatus(StudentStatus studentStatus) {
        this.studentStatus = studentStatus;
    }

    public boolean isArchiveable() {
        return archiveable;
    }

    public void setArchiveable(boolean archiveable) {
        this.archiveable = archiveable;
    }
}