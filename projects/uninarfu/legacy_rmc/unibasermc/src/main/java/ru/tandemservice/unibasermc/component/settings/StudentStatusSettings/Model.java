package ru.tandemservice.unibasermc.component.settings.StudentStatusSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Model extends ru.tandemservice.uni.component.settings.StudentStatusSettings.Model {
    DynamicListDataSource<Wrapper> dataSourceExt;

    public DynamicListDataSource<Wrapper> getDataSourceExt() {
        return dataSourceExt;
    }

    public void setDataSourceExt(DynamicListDataSource<Wrapper> dataSourceExt) {
        this.dataSourceExt = dataSourceExt;
    }
}
