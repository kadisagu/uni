package ru.tandemservice.unibasermc.component.student.LogView;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.StudentGrantEntity;
import ru.tandemservice.uni.component.log.EntityLogViewBase.Model;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

public class DAO extends ru.tandemservice.uni.component.log.EntityLogViewBase.DAO implements IDAO {

    //формируем список объектов для фильтра "Тип объекта"
    private static final List<String> _entityClassNames = Arrays.asList(
            ru.tandemservice.uni.entity.employee.Student.class.getName(),
            org.tandemframework.shared.person.base.entity.Person.class.getName(),
            org.tandemframework.shared.person.base.entity.IdentityCard.class.getName(),
            org.tandemframework.shared.person.base.entity.PersonEduInstitution.class.getName(),
            org.tandemframework.shared.person.base.entity.PersonBenefit.class.getName(),
            org.tandemframework.shared.person.base.entity.PersonNextOfKin.class.getName(),
            org.tandemframework.shared.person.base.entity.PersonSportAchievement.class.getName(),
            org.tandemframework.shared.person.base.entity.PersonForeignLanguage.class.getName(),
            org.tandemframework.shared.fias.base.entity.Address.class.getName(),
            ru.tandemservice.uni.entity.employee.OrderData.class.getName(),
            ru.tandemservice.uni.entity.employee.StudentDocument.class.getName(),
            StudentGrantEntity.class.getName(),
            org.tandemframework.shared.person.base.entity.PersonMilitaryStatus.class.getName(),
            ru.tandemservice.unisession.entity.mark.SessionMark.class.getName(),
            OrderList.class.getName()/*приказы списком*/,
            ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement.class.getName()/*мероприятия*/,
            ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion.class.getName()/*УП*/,
            ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan.class.getName()/*РУП*/,
            PersonNARFU.class.getName(),
            PersonAcademicDegree.class.getName() /*ученая степень персоны*/,
            PersonAcademicStatus.class.getName() /*Ученое звание персоны*/,
            IdentityCardDeclinability.class.getName(),
            DocumentOrder.class.getName(),
            ListOrder.class.getName());

    @Override
    protected List<String> getEntityClassNames() {
        return _entityClassNames;
    }

    @Override
    protected Collection<Long> getEntityIds(Model model) {
        Student student = get(Student.class, model.getEntityId());
        Set<Long> entityIds = new HashSet<>();
        entityIds.add(student.getId());
        entityIds.add(student.getPerson().getId());
        entityIds.add(student.getPerson().getIdentityCard().getId());
        //добавляем сущности для логирования
        addEntityId(entityIds, OrderData.class.getName(), OrderData.student().id().s(), student.getId());
        addEntityId(entityIds, StudentDocument.class.getName(), StudentDocument.student().id().s(), student.getId());
        addEntityId(entityIds, StudentGrantEntity.class.getName(), StudentGrantEntity.student().id().s(), student.getId());
        addEntityId(entityIds, PersonMilitaryStatus.class.getName(), PersonMilitaryStatus.person().id().s(), student.getPerson().getId());
        addEntityId(entityIds, SessionMark.class.getName(), SessionMark.slot().studentWpeCAction().studentWpe().student().id().s(), student.getId());
        addEntityId(entityIds, OrderList.class.getName(), OrderList.student().id().s(), student.getId());
        addEntityId(entityIds, EppStudentWorkPlanElement.class.getName(), EppStudentWorkPlanElement.student().id().s(), student.getId());
        addEntityId(entityIds, EppStudent2EduPlanVersion.class.getName(), EppStudent2EduPlanVersion.student().id().s(), student.getId());
        addEntityId(entityIds, EppStudent2WorkPlan.class.getName(), EppStudent2WorkPlan.studentEduPlanVersion().student().id().s(), student.getId());
        addEntityId(entityIds, PersonNARFU.class.getName(), PersonNARFU.person().id().s(), student.getPerson().getId());
        addEntityId(entityIds, PersonAcademicDegree.class.getName(), PersonAcademicDegree.person().id().s(), student.getPerson().getId());
        addEntityId(entityIds, PersonAcademicStatus.class.getName(), PersonAcademicStatus.person().id().s(), student.getPerson().getId());
        addEntityId(entityIds, IdentityCardDeclinability.class.getName(), IdentityCardDeclinability.identityCard().id().s(), student.getPerson().getIdentityCard().getId());
        //падежные склонения
        addDeclinablePropertyId(entityIds,
                org.tandemframework.shared.person.base.entity.IdentityCardDeclinability.class.getName(),
                org.tandemframework.shared.commonbase.base.entity.DeclinableProperty.class.getName(),
                IdentityCardDeclinability.identityCard().id().s(),
                student.getPerson().getIdentityCard().getId());

//	     добавляем id индивидуальных представлений
        addRepresentationId(entityIds,
                DocRepresentStudentBase.class.getName(),
                DocOrdRepresent.class.getName(),
                student.getId());
//	     добавляем id списочных представлений
        addRepresentationId(entityIds,
                RelListRepresentStudents.class.getName(),
                ListOrdListRepresent.class.getName(),
                student.getId());

        return entityIds;
    }

    public void addDeclinablePropertyId(Set<Long> entityIds, String subClassName, String className, String property, Long id) {
        if (EntityRuntime.isEntity(className)) {
            List<Long> ids = getEntityId(subClassName, property, id);
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntityRuntime.getMetaNullSafe(className).getClassName(), "e")
                    .where(DQLExpressions.in(DQLExpressions.property("e", "declinable.id"), ids))
                    .column(DQLExpressions.property("e", "id"));
            List<Long> listId = getList(builder);
            if (!listId.isEmpty())
                entityIds.addAll(listId);
        }
    }

    public void addEntityId(Set<Long> entityIds, String className, String property, Long id) {
        List<Long> listId = getEntityId(className, property, id);
        if (!listId.isEmpty())
            entityIds.addAll(listId);
    }


    public List<Long> getEntityId(String className, String property, Long id) {
        //проверяем наличие сущности в базе
        if (EntityRuntime.isEntity(className)) {
            //если такая сущность есть, то берем те, которые принадлежат текущему студенту
            MQBuilder builder = new MQBuilder(EntityRuntime.getMetaNullSafe(className).getClassName(), "e")
                    .add(MQExpression.eq("e", property, id));
            builder.getSelectAliasList().clear();
            builder.addSelect("id");
            return getList(builder);
        } else
            return Collections.emptyList();
    }

    public void addRepresentationId(Set<Long> entityIds, String studentRelClassName, String orderToRepresentClassName, Long id) {

        if (EntityRuntime.isEntity(studentRelClassName) && EntityRuntime.isEntity(orderToRepresentClassName)) {
            DQLSelectBuilder representationBuilder = new DQLSelectBuilder().fromEntity(EntityRuntime.getMetaNullSafe(studentRelClassName).getClassName(), "e")
                    .where(DQLExpressions.eqValue(DQLExpressions.property("e", "student.id"), id))
                    .column(DQLExpressions.property("e", "representation.id"));
            List<Long> representationId = getList(representationBuilder);
            if (!representationId.isEmpty())
                entityIds.addAll(representationId);
            DQLSelectBuilder orderBuilder = new DQLSelectBuilder().fromEntity(EntityRuntime.getMetaNullSafe(orderToRepresentClassName).getClassName(), "o")
                    .where(DQLExpressions.in(DQLExpressions.property("o", "representation.id"), representationId))
                    .column(DQLExpressions.property("o", "order.id"));
            List<Long> orderIds = getList(orderBuilder);
            if (!orderIds.isEmpty())
                entityIds.addAll(orderIds);
        }
    }

}
