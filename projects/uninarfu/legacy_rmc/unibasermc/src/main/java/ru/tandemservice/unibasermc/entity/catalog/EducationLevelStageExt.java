package ru.tandemservice.unibasermc.entity.catalog;

import ru.tandemservice.unibasermc.entity.catalog.gen.EducationLevelStageExtGen;

import java.util.Arrays;
import java.util.Collection;

/**
 * Расширение сущности уровень образования
 */
public class EducationLevelStageExt extends EducationLevelStageExtGen {

    @Override
    public String getTitle() {
        if (this.getBase() != null) return this.getBase().getTitle();

        return super.getTitle();
    }

    @Override
    public Collection<String> getDeclinableProperties() {
        return Arrays.asList(P_TITLE);
    }
}