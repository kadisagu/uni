package ru.tandemservice.unibasermc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * Пропуск миграции в версии 2.3.3
 * <p/>
 * MS_uniec_2x3x3_0to1.java
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unibasermc_1x0x0_5to6 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("scriptitem_t")) {
            PreparedStatement st = tool.prepareStatement("update scriptitem_t set title_p=? where (code_p=?) and (title_p=?)");
            st.setString(1, "Шаблон параграфа приказа. Об изменении приказа о зачислении");
            st.setString(2, "revert.paragraph");
            st.setString(3, "Выписка из приказа об изменении приказа о зачислении");
            st.executeUpdate();

            st.setString(1, "Шаблон выписки из приказа. Об изменении приказа о зачислении");
            st.setString(2, "revert.extract");
            st.setString(3, "Выписка из приказа об изменении приказа о зачислении");
            st.executeUpdate();
        }
    }
}