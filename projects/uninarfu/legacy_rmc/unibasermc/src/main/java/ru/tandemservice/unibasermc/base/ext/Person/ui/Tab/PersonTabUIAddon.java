package ru.tandemservice.unibasermc.base.ext.Person.ui.Tab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseSearchListUtil;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTabUI;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt;

import java.util.List;

public class PersonTabUIAddon extends UIAddon {
    public static String P_SECOND_LANGUAGE = "second";
    private PersonTabUI parentPresenter = getPresenter();

    public PersonTabUIAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();
        prepareLanguageDataSourceExt();
    }

    private void prepareLanguageDataSourceExt() {
        DynamicListDataSource<PersonForeignLanguage> dataSource = new DynamicListDataSource<>(parentPresenter.getConfig().getBusinessComponent(), component -> {
            prepareCustomDataSourceExt(parentPresenter.getPersonForeignLanguageDataSource());
        }, 5);

        dataSource.addColumn(new SimpleColumn("Язык", PersonForeignLanguage.language().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Степень владения", PersonForeignLanguage.skill().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Основной", PersonForeignLanguage.P_MAIN).setFormatter(YesNoFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Второй", P_SECOND_LANGUAGE).setFormatter(YesNoFormatter.INSTANCE).setClickable(false));

        if (parentPresenter.getPersonRoleModel().isAccessible()) {
            dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditForeignLanguage").setPermissionKey(parentPresenter.getSecModel().getPermission("editPersonForeignLanguage")));
            dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteForeignLanguage", "Удалить иностранный язык «{0}»?", PersonForeignLanguage.L_LANGUAGE + "." + ICatalogItem.CATALOG_ITEM_TITLE).setPermissionKey(parentPresenter.getSecModel().getPermission("deletePersonForeignLanguage")));
        }
        parentPresenter.setPersonForeignLanguageDataSource(dataSource);
    }

    private <T extends IEntity> void prepareCustomDataSourceExt(DynamicListDataSource<T> dataSource) {
        MQBuilder builder = new MQBuilder(PersonForeignLanguage.ENTITY_CLASS, "o");
        builder.addJoin("o", PersonForeignLanguage.L_PERSON, "e");

        builder.add(MQExpression.eq("e", "id", parentPresenter.getPerson().getId()));
        CommonBaseSearchListUtil.createPage(dataSource, builder);

        for (ViewWrapper<PersonForeignLanguage> viewWrapper : ViewWrapper.<PersonForeignLanguage>getPatchedList(dataSource)) {
            PersonForeignLanguageExt languageExt = DataAccessServices.dao().get(PersonForeignLanguageExt.class, PersonForeignLanguageExt.base().s(), viewWrapper.getEntity());
            boolean isSecond = languageExt != null && languageExt.isSecondary();
            viewWrapper.setViewProperty(P_SECOND_LANGUAGE, isSecond);
        }


        List<T> entities = DataAccessServices.dao().getList(builder);
        int count = Math.max(entities.size() + 1, 2);
        dataSource.setTotalSize(count);
        dataSource.setCountRow(count);
    }


    private class Wrapper extends EntityBase {

        private PersonForeignLanguage personForeignLanguage;
        private boolean second;


        @Override
        public Long getId() {
            return personForeignLanguage.getId();
        }

        public PersonForeignLanguage getPersonForeignLanguage() {
            return personForeignLanguage;
        }

        public void setPersonForeignLanguage(PersonForeignLanguage personForeignLanguage) {
            this.personForeignLanguage = personForeignLanguage;
        }

        public boolean isSecond() {
            return second;
        }

        public void setSecond(boolean second) {
            this.second = second;
        }
    }
}
