package ru.tandemservice.unibasermc.base.bo.StudentReport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class StudentReport extends BusinessObjectManager {

    public static StudentReport instance() {
        return new StudentReport();
    }
}
