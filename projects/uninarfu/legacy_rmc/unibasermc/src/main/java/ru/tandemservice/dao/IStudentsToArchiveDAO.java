package ru.tandemservice.dao;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.List;

public interface IStudentsToArchiveDAO {
    /**
     * Перемещает в архив студентов формирующего подразделения в указанных состояниях
     *
     * @param orgUnit    формирующее
     * @param statusList список статусов
     */
    public void moveStudentsToArchive(OrgUnit orgUnit, List<StudentStatus> statusList);
}
