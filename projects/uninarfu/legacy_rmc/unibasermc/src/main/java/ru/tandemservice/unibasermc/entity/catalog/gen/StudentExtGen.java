package ru.tandemservice.unibasermc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibasermc.entity.catalog.StudentExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности студент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unibasermc.entity.catalog.StudentExt";
    public static final String ENTITY_NAME = "studentExt";
    public static final int VERSION_HASH = -1810356479;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_PASSED_ENTANCE_TEST = "passedEntanceTest";
    public static final String P_DIRECTION_DEPARTMENT = "directionDepartment";

    private Student _student;     // Студент
    private Boolean _passedEntanceTest;     // Проходил вступительные испытания
    private Boolean _directionDepartment;     // По направлению международного департамента Минобрнауки России

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Проходил вступительные испытания.
     */
    public Boolean getPassedEntanceTest()
    {
        return _passedEntanceTest;
    }

    /**
     * @param passedEntanceTest Проходил вступительные испытания.
     */
    public void setPassedEntanceTest(Boolean passedEntanceTest)
    {
        dirty(_passedEntanceTest, passedEntanceTest);
        _passedEntanceTest = passedEntanceTest;
    }

    /**
     * @return По направлению международного департамента Минобрнауки России.
     */
    public Boolean getDirectionDepartment()
    {
        return _directionDepartment;
    }

    /**
     * @param directionDepartment По направлению международного департамента Минобрнауки России.
     */
    public void setDirectionDepartment(Boolean directionDepartment)
    {
        dirty(_directionDepartment, directionDepartment);
        _directionDepartment = directionDepartment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentExtGen)
        {
            setStudent(((StudentExt)another).getStudent());
            setPassedEntanceTest(((StudentExt)another).getPassedEntanceTest());
            setDirectionDepartment(((StudentExt)another).getDirectionDepartment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentExt.class;
        }

        public T newInstance()
        {
            return (T) new StudentExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "passedEntanceTest":
                    return obj.getPassedEntanceTest();
                case "directionDepartment":
                    return obj.getDirectionDepartment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "passedEntanceTest":
                    obj.setPassedEntanceTest((Boolean) value);
                    return;
                case "directionDepartment":
                    obj.setDirectionDepartment((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "passedEntanceTest":
                        return true;
                case "directionDepartment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "passedEntanceTest":
                    return true;
                case "directionDepartment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "passedEntanceTest":
                    return Boolean.class;
                case "directionDepartment":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentExt> _dslPath = new Path<StudentExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentExt");
    }
            

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentExt#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Проходил вступительные испытания.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentExt#getPassedEntanceTest()
     */
    public static PropertyPath<Boolean> passedEntanceTest()
    {
        return _dslPath.passedEntanceTest();
    }

    /**
     * @return По направлению международного департамента Минобрнауки России.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentExt#getDirectionDepartment()
     */
    public static PropertyPath<Boolean> directionDepartment()
    {
        return _dslPath.directionDepartment();
    }

    public static class Path<E extends StudentExt> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<Boolean> _passedEntanceTest;
        private PropertyPath<Boolean> _directionDepartment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentExt#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Проходил вступительные испытания.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentExt#getPassedEntanceTest()
     */
        public PropertyPath<Boolean> passedEntanceTest()
        {
            if(_passedEntanceTest == null )
                _passedEntanceTest = new PropertyPath<Boolean>(StudentExtGen.P_PASSED_ENTANCE_TEST, this);
            return _passedEntanceTest;
        }

    /**
     * @return По направлению международного департамента Минобрнауки России.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentExt#getDirectionDepartment()
     */
        public PropertyPath<Boolean> directionDepartment()
        {
            if(_directionDepartment == null )
                _directionDepartment = new PropertyPath<Boolean>(StudentExtGen.P_DIRECTION_DEPARTMENT, this);
            return _directionDepartment;
        }

        public Class getEntityClass()
        {
            return StudentExt.class;
        }

        public String getEntityName()
        {
            return "studentExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
