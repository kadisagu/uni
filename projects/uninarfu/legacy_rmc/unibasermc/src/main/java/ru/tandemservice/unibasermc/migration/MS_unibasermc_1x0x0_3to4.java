package ru.tandemservice.unibasermc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.person.base.entity.PersonContactData;

import java.sql.ResultSet;
import java.sql.Statement;

public class MS_unibasermc_1x0x0_3to4 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        {
            IEntityMeta meta = EntityRuntime.getMeta(PersonContactData.class);
            short discriminator = meta.getEntityCode().shortValue();

            Statement stmtMain = tool.getConnection().createStatement();
            stmtMain.execute("select t.id from person_t as t where t.CONTACTDATA_ID is null");
            ResultSet rs = stmtMain.getResultSet();
            while (rs.next()) {
                Long id = rs.getLong(1);
                String insertQuery = "insert into " + meta.getTableName() + "(id, discriminator) values(?, ?)";
                Long cpId = EntityIDGenerator.generateNewId(discriminator);
                tool.executeUpdate(insertQuery, cpId, discriminator);

                tool.executeUpdate("update person_t set CONTACTDATA_ID=? where id=?", cpId, id);

            }
        }
    }
}