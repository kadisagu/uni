package ru.tandemservice.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;


public class DaoRmcFacade {

    private static IDaoRmc _daoRmc;


    public static IDaoRmc getDaoRmc()
    {
        if (_daoRmc == null)
            _daoRmc = (IDaoRmc) ApplicationRuntime.getBean("daoRmc");
        return _daoRmc;
    }


}
