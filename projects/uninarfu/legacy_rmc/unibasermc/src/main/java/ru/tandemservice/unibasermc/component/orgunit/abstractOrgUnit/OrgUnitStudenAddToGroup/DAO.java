package ru.tandemservice.unibasermc.component.orgunit.abstractOrgUnit.OrgUnitStudenAddToGroup;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {
    private static final OrderDescriptionRegistry _groupOrderSettings = new OrderDescriptionRegistry("g");

    static {
        _groupOrderSettings.setOrders(Group.COURSE_KEY, new OrderDescription("g", new String[]{Group.L_COURSE, ICatalogItem.CATALOG_ITEM_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
        _groupOrderSettings.setOrders(Group.FORMATIVE_ORGUNIT_KEY, new OrderDescription("g", new String[]{Group.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_FORMATIVE_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
        _groupOrderSettings.setOrders(Group.TERRITORIAL_ORGUNIT_KEY, new OrderDescription("g", new String[]{Group.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, OrgUnit.P_TERRITORIAL_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
        _groupOrderSettings.setOrders(Group.PRODUCTIVE_ORGUNIT_KEY, new OrderDescription("g", new String[]{Group.L_EDUCATION_ORG_UNIT, EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, EducationLevelsHighSchool.L_ORG_UNIT, OrgUnit.P_TITLE}), new OrderDescription("g", new String[]{Group.P_TITLE}));
    }


    @Override
    public void prepare(Model model)
    {


        model.setOrgUnit(getNotNull(model.getOrgUnitId()));

        final OrgUnit _ou = model.getOrgUnit();

        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setDevelopFormListModel(new LazySimpleSelectModel<>(DevelopForm.class).setSortProperty(DevelopForm.P_CODE));
        model.setEducationLevelsModel(new FullCheckSelectModel(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE) {
            @SuppressWarnings("rawtypes")
            @Override
            public ListResult findValues(String filter)
            {
                List<OrgUnit> formativeOrgUnitList = new ArrayList<>(); //(List<OrgUnit>)model.getSettings().get("formativeOrgUnitList");
                formativeOrgUnitList.add(_ou);

                List<OrgUnit> territorialOrgUnitList = null; //(List<OrgUnit>)model.getSettings().get("territorialOrgUnitList");

                List<EducationLevelsHighSchool> list = UniDaoFacade.getEducationLevelDao().getEducationLevelsHighSchoolList(formativeOrgUnitList, territorialOrgUnitList, filter);
                return new ListResult<>(list);
            }
        });
    }


    @Override
    public void prepareListDataSource(Model model)
    {
        List<Group> itemList = getListData(model);

        DynamicListDataSource<Group> dataSource = model.getDataSource();
        UniUtils.createPage(dataSource, itemList);
    }


    /**
     * Список групп студентов
     *
     * @param model
     *
     * @return
     */
    @SuppressWarnings("rawtypes")
    private List<Group> getListData(Model model)
    {

        IDataSettings settings = model.getSettings();

        Object title = settings.get("groupName");
        Object courseList = settings.get("courseList");
        Object eduLevelHighSchool = settings.get("eduLevelHighSchool");
        Object developFormList = settings.get("developFormList");

        MQBuilder builder = new MQBuilder(Group.ENTITY_CLASS, "g");
        if (model.getOrgUnitId() != null)
            builder.add(MQExpression.in("g", Group.P_ID, getGroupIds(model)));

        builder.add(MQExpression.eq("g", Group.P_ARCHIVAL, false));

        if (title != null)
            builder.add(MQExpression.like("g", Group.title().s(), "%" + title));
        if (courseList != null && !((List) courseList).isEmpty())
            builder.add(MQExpression.in("g", Group.course().s(), (Collection) courseList));

        if (eduLevelHighSchool != null)
            builder.add(MQExpression.eq("g", Group.educationOrgUnit().educationLevelHighSchool().s(), eduLevelHighSchool));
        if (developFormList != null && !((List) developFormList).isEmpty())
            builder.add(MQExpression.in("g", Group.educationOrgUnit().developForm().s(), (Collection) developFormList));


        // место для фильтра
        if (model.getDataSource() != null) {
            _groupOrderSettings.applyOrder(builder, model.getDataSource().getEntityOrder());
        }

        return builder.getResultList(getSession());
    }

    @SuppressWarnings("unchecked")
    private Set<Long> getGroupIds(Model model)
    {
        Set<String> kindSet = new HashSet<>();
        for (OrgUnitKind orgUnitKind : UniDaoFacade.getOrgstructDao().getOrgUnitKindList(model.getOrgUnit()))
            if (orgUnitKind.isAllowGroups())
                kindSet.add(orgUnitKind.getCode());

        //сводный список групп (их id-шники)
        Set<Long> dataList = new HashSet<>();

        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING)) {
            //Для выпускающих подразделений – группы, для которых текущее подразделение является выпускающим через связь с направлением подготовки (специальностью) ОУ.
            Criteria c = getSession().createCriteria(Group.class, "gr");
            c.createAlias("gr." + Group.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
            c.createAlias("educationOrgUnit." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "highSchool");
            c.add(Restrictions.eq("highSchool." + EducationLevelsHighSchool.L_ORG_UNIT, model.getOrgUnit()));
            c.setProjection(Projections.property(Group.P_ID));
            dataList.addAll(c.list());
        }

        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)) {
            //Для формирующих подразделений будет отображаться перечень групп, для которых текущее подразделение является формирующим через связь с направления подготовки (специальностью) подразделения.
            Criteria c = getSession().createCriteria(Group.class, "gr");
            c.createAlias("gr." + Group.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
            c.add(Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getOrgUnit()));
            c.setProjection(Projections.property(Group.P_ID));
            dataList.addAll(c.list());
        }

        if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL)) {
            //Для территориальных подразделений – группы, для которых текущее подразделение является территориальным подразделением через связь с направлением подготовки (специальностью) подразделения.
            Criteria c = getSession().createCriteria(Group.class, "gr");
            c.createAlias("gr." + Group.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
            c.add(Restrictions.eq("educationOrgUnit." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getOrgUnit()));
            c.setProjection(Projections.property(Group.P_ID));
            dataList.addAll(c.list());
        }
        return dataList;
    }

    @Override
    public void update(Group group, Model model)
    {
        if (group == null)
            return;

        List<Long> lst = model.getStudentIds();

        for (Long id : lst) {
            Student st = getNotNull(id);
            st.setGroup(group);
        }
    }
}
