package ru.tandemservice.unibasermc.sec.ext.Sec.ui.TemplateLocalRoleList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.shared.organization.sec.bo.Sec.ui.TemplateLocalRoleList.SecTemplateLocalRoleList;
import ru.tandemservice.unibasermc.base.ext.StudentReportPerson.ui.Add.StudentReportPersonAddUIAddon;

@Configuration
public class SecTemplateLocalRoleListExt extends BusinessComponentExtensionManager {

    @Autowired
    private SecTemplateLocalRoleList _secTemplateLocalRoleList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(_secTemplateLocalRoleList.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", StudentReportPersonAddUIAddon.class));

        return pi.create();
    }

    @Bean
    public ColumnListExtension templateLocalRoleDSExtension() {
        return this.columnListExtensionBuilder(_secTemplateLocalRoleList.templateRoleDS())
                .overwriteColumn(actionColumn("export", CommonDefines.ICON_IMPORT, "onClickExportRole").disabled(Boolean.FALSE))
                .create();
    }

}
