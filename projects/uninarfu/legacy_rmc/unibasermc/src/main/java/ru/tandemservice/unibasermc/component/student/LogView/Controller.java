package ru.tandemservice.unibasermc.component.student.LogView;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uni.UniUtils;

public class Controller extends ru.tandemservice.uni.component.log.EntityLogViewBase.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(UniUtils.getDataSettings(component, "logView.student.filter."));
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
    }
}
