package ru.tandemservice.unibasermc.component.student.StudentAdditionalEdit;

import ru.tandemservice.unibasermc.entity.catalog.StudentExt;

public class Model extends ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model {

    private StudentExt studentExt;

    public StudentExt getStudentExt() {
        return studentExt;
    }

    public void setStudentExt(StudentExt studentExt) {
        this.studentExt = studentExt;
    }


}
