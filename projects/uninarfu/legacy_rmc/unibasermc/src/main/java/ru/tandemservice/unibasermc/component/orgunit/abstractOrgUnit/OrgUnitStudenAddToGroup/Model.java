package ru.tandemservice.unibasermc.component.orgunit.abstractOrgUnit.OrgUnitStudenAddToGroup;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.orgstruct.Group;

import java.util.Collections;
import java.util.List;


@Input(
        {
                @Bind(key = "ids", binding = "studentIds", required = true),
                @Bind(key = "orgUnitId", binding = "orgUnitId", required = true)
        }
)

public class Model {

    private ISelectModel _courseListModel;
    private ISelectModel _developFormListModel;
    private ISelectModel _educationLevelsModel;
    private OrgUnit _orgUnit;
    private Long _orgUnitId;
    private List<Long> studentIds = Collections.emptyList();
    private IDataSettings _settings;

    private DynamicListDataSource<Group> _dataSource;


    public void setCourseListModel(ISelectModel _courseListModel) {
        this._courseListModel = _courseListModel;
    }

    public ISelectModel getCourseListModel() {
        return _courseListModel;
    }

    public void setDevelopFormListModel(ISelectModel _developFormListModel) {
        this._developFormListModel = _developFormListModel;
    }

    public ISelectModel getDevelopFormListModel() {
        return _developFormListModel;
    }

    public void setEducationLevelsModel(ISelectModel _educationLevelsModel) {
        this._educationLevelsModel = _educationLevelsModel;
    }

    public ISelectModel getEducationLevelsModel() {
        return _educationLevelsModel;
    }

    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public void setStudentIds(List<Long> studentIds) {
        this.studentIds = studentIds;
    }

    public List<Long> getStudentIds() {
        return studentIds;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public DynamicListDataSource<Group> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<Group> dataSource)
    {
        _dataSource = dataSource;
    }

}
