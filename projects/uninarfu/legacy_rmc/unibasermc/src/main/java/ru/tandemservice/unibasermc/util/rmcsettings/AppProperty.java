package ru.tandemservice.unibasermc.util.rmcsettings;

import org.tandemframework.core.runtime.ApplicationRuntime;

public class AppProperty {

    /**
     * Запускать демонов в режиме отладки или нет
     *
     * @return
     */
    public static boolean RUN_DEMON()
    {
        if (DEBUG_MODE()) {
            // если ключ не указан - исполнять
            if (ApplicationRuntime.getProperty("debug.rundemon") == null)
                return true;
            else {
                // ключ указан
                boolean runDemon = Boolean.parseBoolean(ApplicationRuntime.getProperty("debug.rundemon"));
                return runDemon;
            }
        }
        else
            // в боевом режиме - исполняем
            return true;

    }

    public static boolean DEBUG_MODE()
    {
        // по умолчанию - false
        boolean retVal = Boolean.parseBoolean(ApplicationRuntime.getProperty("debug.mode"));
        return retVal;
    }

}
