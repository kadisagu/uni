package ru.tandemservice.unibasermc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Статус студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentStatusExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt";
    public static final String ENTITY_NAME = "studentStatusExt";
    public static final int VERSION_HASH = -1380809087;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_STATUS = "studentStatus";
    public static final String P_ARCHIEVABLE = "archievable";

    private StudentStatus _studentStatus;     // Статус студента
    private boolean _archievable; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Статус студента. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentStatus getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Статус студента. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudentStatus(StudentStatus studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isArchievable()
    {
        return _archievable;
    }

    /**
     * @param archievable  Свойство не может быть null.
     */
    public void setArchievable(boolean archievable)
    {
        dirty(_archievable, archievable);
        _archievable = archievable;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentStatusExtGen)
        {
            setStudentStatus(((StudentStatusExt)another).getStudentStatus());
            setArchievable(((StudentStatusExt)another).isArchievable());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentStatusExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentStatusExt.class;
        }

        public T newInstance()
        {
            return (T) new StudentStatusExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentStatus":
                    return obj.getStudentStatus();
                case "archievable":
                    return obj.isArchievable();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((StudentStatus) value);
                    return;
                case "archievable":
                    obj.setArchievable((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentStatus":
                        return true;
                case "archievable":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentStatus":
                    return true;
                case "archievable":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentStatus":
                    return StudentStatus.class;
                case "archievable":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentStatusExt> _dslPath = new Path<StudentStatusExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentStatusExt");
    }
            

    /**
     * @return Статус студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt#getStudentStatus()
     */
    public static StudentStatus.Path<StudentStatus> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt#isArchievable()
     */
    public static PropertyPath<Boolean> archievable()
    {
        return _dslPath.archievable();
    }

    public static class Path<E extends StudentStatusExt> extends EntityPath<E>
    {
        private StudentStatus.Path<StudentStatus> _studentStatus;
        private PropertyPath<Boolean> _archievable;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Статус студента. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt#getStudentStatus()
     */
        public StudentStatus.Path<StudentStatus> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS, this);
            return _studentStatus;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt#isArchievable()
     */
        public PropertyPath<Boolean> archievable()
        {
            if(_archievable == null )
                _archievable = new PropertyPath<Boolean>(StudentStatusExtGen.P_ARCHIEVABLE, this);
            return _archievable;
        }

        public Class getEntityClass()
        {
            return StudentStatusExt.class;
        }

        public String getEntityName()
        {
            return "studentStatusExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
