package ru.tandemservice.unibasermc.component.settings.StudentStatusSettings;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.ArrayList;
import java.util.List;

public class DAO extends ru.tandemservice.uni.component.settings.StudentStatusSettings.DAO implements IDAO {
    @Override
    public void prepare(ru.tandemservice.uni.component.settings.StudentStatusSettings.Model model) {
        prepareListDataSource(model);


    }

    @Override
    public void prepareListDataSource(ru.tandemservice.uni.component.settings.StudentStatusSettings.Model model) {
        super.prepareListDataSource(model);
        Model modelExt = (Model) model;

        DynamicListDataSource dataSource = model.getDataSource();
        List<StudentStatus> entityList = dataSource.getEntityList();
        List<Wrapper> wrpList = new ArrayList<>();
        for (StudentStatus status : entityList) {
            Wrapper wrp = new Wrapper();
            StudentStatusExt studentStatusNarfu = get(StudentStatusExt.class, StudentStatusExt.studentStatus().s(), status);
            if (studentStatusNarfu != null) {

                wrp.setId(status.getId());
                wrp.setArchiveable(studentStatusNarfu.isArchievable());
                wrp.setStudentStatus(status);
                wrpList.add(wrp);
            }
            else {
                wrp.setId(status.getId());
                wrp.setStudentStatus(status);
                wrp.setArchiveable(false);
                wrpList.add(wrp);
            }
        }
        DynamicListDataSource<Wrapper> ds = modelExt.getDataSourceExt();
        ds.setCountRow(wrpList.size());
        ds.setTotalSize(wrpList.size());
        UniUtils.createPage(ds, wrpList);
    }

    @Override
    public void doToggleArchiveable(Long studentStatusId) {
        StudentStatus studentStatus = (StudentStatus) get(StudentStatus.class, studentStatusId);
        StudentStatusExt studentStatusExt = get(StudentStatusExt.class, StudentStatusExt.studentStatus().s(), studentStatus);
        if (studentStatusExt == null) {
            studentStatusExt = new StudentStatusExt();
            studentStatusExt.setStudentStatus(studentStatus);
        }
        studentStatusExt.setArchievable(!studentStatusExt.isArchievable());
        saveOrUpdate(studentStatusExt);
    }
}
