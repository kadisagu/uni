package ru.tandemservice.unibasermc.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unibasermc.base.ext.SystemAction.logic.ISystemActionExtDAO;
import ru.tandemservice.unibasermc.base.ext.SystemAction.logic.SystemActionExtDAO;
import ru.tandemservice.unibasermc.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

@Configuration
public class SystemActionManagerExt extends BusinessObjectExtensionManager {
    public static SystemActionManagerExt instance() {
        return instance(SystemActionManagerExt.class);
    }

    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("uni_setMainForeignLanguage", new SystemActionDefinition("uni", "setMainForeignLanguage", "onSetMainForeignLanguage", SystemActionPubExt.SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("uni_moveStudentsToArchive", new SystemActionDefinition("uni", "moveStudentsToArchive", "onMoveStudentsToArchive", SystemActionPubExt.SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }

    @Bean
    public ISystemActionExtDAO dao() {
        return new SystemActionExtDAO();
    }
}
