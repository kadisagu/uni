package ru.tandemservice.unibasermc.component.orgunit.abstractOrgUnit.OrgUnitStudenAddToGroup;

import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.entity.orgstruct.Group;

public interface IDAO extends IUniDao<Model> {
    public void update(Group group, Model model);
}
