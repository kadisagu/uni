package ru.tandemservice.tools;

import java.sql.SQLException;

public class ObjectToStr {

    public static String ObjToStr(Object o)
    {
        if (o == null)
            return "";
        else {
            String retVal = "";

            if (o instanceof java.sql.Clob) {
                java.sql.Clob clobInData = (java.sql.Clob) o;
                long i = 1;
                int clobLength;
                try {
                    clobLength = (int) clobInData.length();
                    retVal = clobInData.getSubString(i, clobLength);

                }
                catch (SQLException e) {
                    retVal = "error convert";
                }

            }
            else {

                retVal = o.toString();
            }
            return retVal;
        }
    }

    public static String ObjToStr(Object o, String nullval)
    {
        if (o == null)
            return nullval;
        else {
            String retVal = "";

            retVal = o.toString();
            return retVal;
        }
    }

}
