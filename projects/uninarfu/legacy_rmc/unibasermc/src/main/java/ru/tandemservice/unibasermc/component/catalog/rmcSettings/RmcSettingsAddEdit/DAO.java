package ru.tandemservice.unibasermc.component.catalog.rmcSettings.RmcSettingsAddEdit;

import org.hibernate.Session;
import ru.tandemservice.unibasermc.entity.catalog.RmcSettings;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        if (model.getRmcSettingsId() != null) {
            RmcSettings setting = (RmcSettings) this.getNotNull(model.getRmcSettingsId());
            model.setRmcSettings(setting);
        }
        else
            model.setRmcSettings(null);
    }


    @Override
    public void update(Model model)
    {
        Session session = getSession();
        if (model.getRmcSettings() != null)
            session.update(model.getRmcSettings());
    }

}
