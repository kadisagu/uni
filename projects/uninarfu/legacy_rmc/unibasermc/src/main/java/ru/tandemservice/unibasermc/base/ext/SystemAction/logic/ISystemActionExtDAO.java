package ru.tandemservice.unibasermc.base.ext.SystemAction.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

public interface ISystemActionExtDAO extends INeedPersistenceSupport {
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public void setMainForeignLanguage();
}
