package ru.tandemservice.unibasermc.base.ext.Person.ui.LanguageAddEdit;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.LanguageAddEdit.DAO
{
    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.LanguageAddEdit.Model model)
    {
        ForeignLanguage english = getByCode(ForeignLanguage.class, "12");
        ForeignLanguage language = model.getForeignLanguage().getLanguage();
        // if(!model.getForeignLanguage().getLanguage().equals(english))
        super.prepare(model);
        if (language != null)
            model.getForeignLanguage().setLanguage(language);
        Model myModel = (Model) model;
        if (model.getForeignLanguage().getId() != null) {
            PersonForeignLanguageExt languageExt = get(PersonForeignLanguageExt.class, PersonForeignLanguageExt.base().s(), model.getForeignLanguage());
            if (languageExt != null)
                myModel.setLanguageExt(languageExt);
        }

        // myModel.setPersonEduInstitutionNARFU(PersonNarfuUtil.getPersonEduinstitution(model.getPersonEduInstitution()));
    }

    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.LanguageAddEdit.Model model)
    {
        Model m = (Model) model;
        List<Object[]> languages = getLanguages(model.getPerson().getId());

        if (m.getForeignLanguage().isMain()) {
            for (Object[] language : languages) {
                PersonForeignLanguage base = (PersonForeignLanguage) language[0];
                PersonForeignLanguageExt ext = (PersonForeignLanguageExt) language[1];

                if (base.getId().equals(m.getForeignLanguage().getId())) {
                    if (ext != null && ext.isSecondary()) {
                        ext.setSecondary(false);
                        update(ext);
                    }
                } else {
                    base.setMain(false);
                    update(base);
                }
            }
        } else if (!m.getLanguageExt().isSecondary())
            if (languages.isEmpty() || languages.stream().noneMatch(l -> ((PersonForeignLanguage) l[0]).isMain()))
                m.getForeignLanguage().setMain(true);

        saveOrUpdate(m.getForeignLanguage());

        if (m.getLanguageExt().isSecondary()) {
            m.getLanguageExt().setBase(model.getForeignLanguage());
            saveOrUpdate(m.getLanguageExt());
        }
    }


    /**
     * @param personId - person ID
     * @return List<Object[]> - List of [PersonForeignLanguage, PersonForeignLanguageExt] arrays </>
     */
    private List<Object[]> getLanguages(Long personId)
    {
        String base_alias = "base";
        DQLSelectBuilder dql = new DQLSelectBuilder();

        dql.fromEntity(PersonForeignLanguage.class, base_alias)
                .column(property(base_alias))
                .where(eq(property(base_alias, PersonForeignLanguage.person().id()), value(personId)));

        String ext_alias = "ext";
        dql.joinEntity(base_alias, DQLJoinType.left, PersonForeignLanguageExt.class, ext_alias,
                eq(property(ext_alias, PersonForeignLanguageExt.base()), property(base_alias)))
                .column(property(ext_alias));

        return getList(dql);
    }
}
