package ru.tandemservice.unibasermc.catalog.bo.EducationLevelStage.ui.List;

import org.tandemframework.common.CommonDefines;
import org.tandemframework.common.catalog.entity.DefaultCatalogItem;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEdit;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.crud.AbstractCatalogItemListUI;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorLanguage;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt;
import ru.tandemservice.uni.dao.IUniBaseDao;


public class EducationLevelStageListUI extends AbstractCatalogItemListUI<DefaultCatalogItem> {

    @Override
    protected void addActionColumns() {
        this._dataSource.addColumn(EducationLevelStageList.actionColumn("editDeclinable", CommonDefines.ICON_EDIT, "onEditDeclinable").permissionKey(getEditPermKey()).create());

        super.addActionColumns();
    }

    public void onEditDeclinable() {
        IUniBaseDao dao = IUniBaseDao.instance.get();
        Long baseId = getListenerParameterAsLong();

        EducationLevelStageExt ext = dao.get(EducationLevelStageExt.class, EducationLevelStageExt.base().id(), baseId);
        if (ext == null) {
            ext = new EducationLevelStageExt();
            ext.setBase(dao.getNotNull(EducationLevelStage.class, baseId));
            ext.setTitle(ext.getBase().getTitle());

            dao.saveOrUpdate(ext);
        }

        InflectorLanguage lang = dao.get(InflectorLanguage.class, InflectorLanguage.enabled().s(), Boolean.TRUE);
        if (null == lang)
            throw new ApplicationException("Не установлен активный язык склонений.");

        _uiActivation.asRegion(DeclinableEdit.class)
                .parameter("publisherId", ext.getId())
                .parameter("languageId", lang.getId())
                .activate();
    }
}
