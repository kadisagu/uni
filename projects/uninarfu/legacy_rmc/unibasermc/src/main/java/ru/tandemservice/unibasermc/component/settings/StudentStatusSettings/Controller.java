package ru.tandemservice.unibasermc.component.settings.StudentStatusSettings;


import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends ru.tandemservice.uni.component.settings.StudentStatusSettings.Controller {

    public void onRefreshComponent(IBusinessComponent context) {
        super.onRefreshComponent(context);
        Model model = (Model) getModel(context);
        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(context, (IListDataSourceDao) getDao());
        dataSource.addColumn(new SimpleColumn("Название", "studentStatus.title").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Использовать", "studentStatus.usedInSystem").setListener("onClickUsedInSystem"));
        dataSource.addColumn(new ToggleColumn("Активное состояние", "studentStatus.active", "Состояние считается активным", "Состояние считается не активным").setListener("onClickActive"));
        dataSource.addColumn(new ToggleColumn("Можно переместить в архив", "archiveable", "Можно перемещать в архив студентов с таким статусом", "Можно перемещать в архив студентов с таким статусом").setListener("onClickArchiveable"));
        dataSource.addColumn(new ActionColumn("Повысить приоритет", "up", "onClickUp").setOrderable(false));
        dataSource.addColumn(new ActionColumn("Понизить приоритет", "down", "onClickDown").setOrderable(false));
        model.setDataSourceExt(dataSource);

    }

    public void onClickArchiveable(IBusinessComponent context) {
        ((IDAO) getDao()).doToggleArchiveable((Long) context.getListenerParameter());
    }

}
