package ru.tandemservice.unibasermc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.IDeclinable;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности уровень образования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EducationLevelStageExtGen extends EntityBase
 implements IDeclinable{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt";
    public static final String ENTITY_NAME = "educationLevelStageExt";
    public static final int VERSION_HASH = 1647123846;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String L_BASE = "base";

    private String _title;     // Название
    private EducationLevelStage _base;     // Уровень/ступень образования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Уровень/ступень образования. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EducationLevelStage getBase()
    {
        return _base;
    }

    /**
     * @param base Уровень/ступень образования. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(EducationLevelStage base)
    {
        dirty(_base, base);
        _base = base;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EducationLevelStageExtGen)
        {
            setTitle(((EducationLevelStageExt)another).getTitle());
            setBase(((EducationLevelStageExt)another).getBase());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EducationLevelStageExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EducationLevelStageExt.class;
        }

        public T newInstance()
        {
            return (T) new EducationLevelStageExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "base":
                    return obj.getBase();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "base":
                    obj.setBase((EducationLevelStage) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "base":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "base":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "base":
                    return EducationLevelStage.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EducationLevelStageExt> _dslPath = new Path<EducationLevelStageExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EducationLevelStageExt");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Уровень/ступень образования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt#getBase()
     */
    public static EducationLevelStage.Path<EducationLevelStage> base()
    {
        return _dslPath.base();
    }

    public static class Path<E extends EducationLevelStageExt> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private EducationLevelStage.Path<EducationLevelStage> _base;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(EducationLevelStageExtGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Уровень/ступень образования. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unibasermc.entity.catalog.EducationLevelStageExt#getBase()
     */
        public EducationLevelStage.Path<EducationLevelStage> base()
        {
            if(_base == null )
                _base = new EducationLevelStage.Path<EducationLevelStage>(L_BASE, this);
            return _base;
        }

        public Class getEntityClass()
        {
            return EducationLevelStageExt.class;
        }

        public String getEntityName()
        {
            return "educationLevelStageExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
