package ru.tandemservice.unibasermc.component.student.StudentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt;
import ru.tandemservice.uni.component.student.StudentPub.IDAO;
import ru.tandemservice.uni.component.student.StudentPub.Model;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Calendar;

public class Controller extends AbstractBusinessController<IDAO, Model> {


    public void onClickArchiveExt(IBusinessComponent component) {
        Model model = (Model) component.getModel();
        IUniBaseDao dao = IUniBaseDao.instance.get();
        Student student = model.getStudent();
        StudentStatusExt studentStatusExt = dao.get(StudentStatusExt.class, StudentStatusExt.studentStatus().s(), student.getStatus());
        if (studentStatusExt == null || !studentStatusExt.isArchievable()) {
            throw new ApplicationException("Студент " + student.getPerson().getFullFio() + " числится в статусе " + student.getStatus().getTitle() + ". Cписать в архив студента в таком статусе нельзя.");
        }
        else {

            Calendar calendar = Calendar.getInstance();
            student.setArchivingDate(calendar.getTime());
            student.setArchival(true);
            dao.update(student);
        }

    }
}