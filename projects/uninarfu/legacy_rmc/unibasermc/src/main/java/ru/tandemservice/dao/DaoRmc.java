package ru.tandemservice.dao;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.HashMap;
import java.util.Map;


public class DaoRmc extends UniBaseDao implements IDaoRmc {
    private static Map<String, Object> map = new HashMap<String, Object>();

    @SuppressWarnings("unchecked")
    @Override
    public <T extends ICatalogItem> T getCatalogItemHash
            (
                    Class<T> catalogItemClass
                    , String itemCode
            )
    {
        // 1 - ишем в кеше, если там нет, лезем в базу
        String key = catalogItemClass.getName() + itemCode;
        if (map.containsKey(key))
            return (T) map.get(key);
        else {
            T val = getCatalogItem(catalogItemClass, itemCode);
            map.put(key, val);
            return val;
        }
    }
}
