package ru.tandemservice.unibasermc.component.orgunit.abstractOrgUnit.OrgUnitStudenAddToGroup;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.FormatterColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.RadioButtonColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.Collections;
import java.util.Map;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public static String KEY = "unirmc.StudentToGroup";

    public static String GET_KEY(Long id)
    {
        return KEY + "" + Long.toString(id);
    }


    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, GET_KEY(model.getOrgUnitId())));


        getDao().prepare(model);
        prepareDataSource(component);
    }


    public void onClickExit(final IBusinessComponent context)
    {
        deactivate(context);
    }

    @SuppressWarnings("rawtypes")
    public void onClickSearch(final IBusinessComponent component)
    {
        DynamicListDataSource dataSource = getModel(component).getDataSource();
        ((RadioButtonColumn) dataSource.getColumn("radio")).setSelectedEntity(null);

        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
        DataSettingsFacade.saveSettings(model.getSettings());

    }

    public void onStudentToGroup(final IBusinessComponent component)
    {
        Model model = component.getModel();
        // найдем выделение
        Group group = ((Group) ((RadioButtonColumn) model.getDataSource().getColumn("radio")).getSelectedEntity());
        if (group != null) {
            getDao().update(group, model);

            // перейдем на группу
            Map<String, Object> map = Collections.singletonMap(PublisherActivator.PUBLISHER_ID_KEY, (Object) group.getId());
            // map.put(key, value)
            activateInRoot(component,
                           new ComponentActivator("ru.tandemservice.uni.component.group.GroupPub", map));
        }
        else
            throw new ApplicationException("Не выбрана группа");

    }

    protected void prepareDataSource(final IBusinessComponent component)
    {

        final Model model = getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        final DynamicListDataSource<Group> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        }, 30);

        prepareDataSourceColumns(model, dataSource);

        model.setDataSource(dataSource);
    }


    private void prepareDataSourceColumns(Model model,
                                          DynamicListDataSource<Group> dataSource)
    {

        dataSource.addColumn(new RadioButtonColumn("radio", "Группа для включения студентов", true).setWidth(1));

        dataSource.addColumn(IndicatorColumn.createIconColumn("group", "Группа"));
        dataSource.addColumn(buildGroupTitleClickableColumn());
        dataSource.addColumn(new SimpleColumn("Курс", Group.COURSE_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", Group.FORMATIVE_ORGUNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", Group.TERRITORIAL_ORGUNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Выпускающее подр.", Group.PRODUCTIVE_ORGUNIT_KEY).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", Group.educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Квалификация", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", Group.educationOrgUnit().developForm().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", Group.educationOrgUnit().developCondition().title().s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Срок освоения", Group.educationOrgUnit().developPeriod().title().s()).setOrderable(false).setClickable(false));
        // dataSource.addColumn(new SimpleColumn("Активных студентов", new String[]{Group.P_ACTIVE_STUDENT_COUNT}).setOrderable(false).setClickable(false));

    }

    protected FormatterColumn buildGroupTitleClickableColumn() {
        return new SimpleColumn("Группа", Group.P_TITLE, NoWrapFormatter.INSTANCE);
    }

}
