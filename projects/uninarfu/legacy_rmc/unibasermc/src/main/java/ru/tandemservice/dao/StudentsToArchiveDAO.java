package ru.tandemservice.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.*;

public class StudentsToArchiveDAO extends UniBaseDao implements IStudentsToArchiveDAO {
    private static IStudentsToArchiveDAO instance;

    public static IStudentsToArchiveDAO getInstance() {
        if (instance == null)
            instance = (IStudentsToArchiveDAO) ApplicationRuntime.getBean(IStudentsToArchiveDAO.class.getName());
        return instance;
    }

    @Override
    public void moveStudentsToArchive(OrgUnit orgUnit, List<StudentStatus> statusList) {
        Map<Group, List<Student>> groupStudentMap = new HashMap<>();
        //Список всех не архивных студентов формирующего подразделения
        List<Student> studentList = getStudentList(orgUnit);
        //Отсортируем по группам
        sortStudents(groupStudentMap, studentList);
        //Загоняем в архив всех со статусом из списка
        moveToArchive(groupStudentMap, statusList);
    }

    private List<Student> getStudentList(OrgUnit orgUnit)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Student.class, "s");
        dql.where(DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("s")), DQLExpressions.value(orgUnit)));
        dql.where(DQLExpressions.eq(DQLExpressions.property(Student.archival().fromAlias("s")), DQLExpressions.value(false)));
        return getList(dql);
    }

    private void sortStudents(Map<Group, List<Student>> groupStudentMap, List<Student> studentList)
    {
        for (Student student : studentList) {
            Group group = student.getGroup();
            if (groupStudentMap.get(group) == null)
                groupStudentMap.put(group, new ArrayList<>());
            groupStudentMap.get(group).add(student);
        }
    }

    private void moveToArchive(Map<Group, List<Student>> groupStudentMap, List<StudentStatus> statusList)
    {
        for (Map.Entry<Group, List<Student>> entry : groupStudentMap.entrySet()) {
            boolean archiveGroup = true;
            for (Student student : entry.getValue()) {
                if (statusList.contains(student.getStatus()))
                    setArchivalStudent(student);
                else
                    archiveGroup = false;
            }
            if (archiveGroup && entry.getKey() != null) {
                Group group = entry.getKey();
                Calendar calendar = Calendar.getInstance();
                EducationYear endingYear = get(EducationYear.class, "intValue", calendar.get(Calendar.YEAR));

                group.setEndingYear(endingYear);
                group.setArchivingDate(calendar.getTime());
                group.setArchival(true);
                update(group);
            }
        }
    }

    private void setArchivalStudent(Student student)
    {
        Calendar calendar = Calendar.getInstance();
        student.setArchivingDate(calendar.getTime());
        student.setArchival(true);
        update(student);
    }
}
