package ru.tandemservice.tools;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class PrivateAccessor {

    public static Object getPrivateField(Object o, String fieldName)
    {
        final Field fields[] = o.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            if (fieldName.equals(fields[i].getName())) {
                try {
                    fields[i].setAccessible(true);
                    return fields[i].get(o);
                }
                catch (IllegalAccessException ex) {
                    // пока по простому
                    return null;
                }
            }
        }
        return null;
    }

    public static Object getPrivateField(Object o, Class<?> cls, String fieldName)
    {
        Field fields[] = cls.getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            if (fieldName.equals(fields[i].getName())) {
                try {
                    fields[i].setAccessible(true);
                    return fields[i].get(o);
                }
                catch (IllegalAccessException ex) {
                    // пока по простому
                    return null;
                }
            }
        }
        return null;
    }

    public static boolean setPrivateField(Object o, String fieldName, Object value)
    {
        final Field fields[] = o.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            if (fieldName.equals(fields[i].getName())) {
                try {
                    fields[i].setAccessible(true);
                    fields[i].set(o, value);
                    return true;

                }
                catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean setPrivateField(Object o, Class<?> cls, String fieldName, Object value)
    {
        final Field fields[] = cls.getDeclaredFields();
        for (int i = 0; i < fields.length; ++i) {
            if (fieldName.equals(fields[i].getName())) {
                try {
                    fields[i].setAccessible(true);
                    fields[i].set(o, value);
                    return true;

                }
                catch (IllegalAccessException ex) {
                    ex.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    public static Object invokePrivateMethod(Object o, String methodName, Object[] params) {
        final Method methods[] = o.getClass().getDeclaredMethods();
        for (int i = 0; i < methods.length; ++i) {
            if (methodName.equals(methods[i].getName())) {
                try {
                    methods[i].setAccessible(true);
                    return methods[i].invoke(o, params);
                }
                catch (IllegalAccessException ex) {
                    // пока по простому
                    return null;
                }
                catch (InvocationTargetException ite) {
                    // пока по простому
                    return null;
                }
            }
        }
        return null;
    }

    @SuppressWarnings("rawtypes")
    public static Object invokePrivateMethod(Object o, Class cls, String methodName, Object[] params) {


        final Method methods[] = cls.getDeclaredMethods();

        for (int i = 0; i < methods.length; ++i) {
            if (methodName.equals(methods[i].getName())) {
                try {
                    methods[i].setAccessible(true);
                    return methods[i].invoke(o, params);
                }
                catch (IllegalAccessException ex) {
                    // пока по простому
                    return null;
                }
                catch (InvocationTargetException ite) {
                    // пока по простому
                    return null;
                }
            }
        }
        return null;
    }

}
