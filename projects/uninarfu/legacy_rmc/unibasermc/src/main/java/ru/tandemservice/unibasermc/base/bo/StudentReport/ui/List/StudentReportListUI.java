package ru.tandemservice.unibasermc.base.bo.StudentReport.ui.List;

import org.tandemframework.shared.commonbase.base.bo.GlobalReport.ui.List.GlobalReportListUI;

public class StudentReportListUI extends GlobalReportListUI {
    public static final String STUDENT_BLOCK = "student";

    public void onComponentActivate() {
        super.onComponentActivate();

        //удалим все блоки кроме студента
        for (int i = 0; i < getBlockList().size(); i++)
            if (!STUDENT_BLOCK.equals(getBlockList().get(i))) {
                getBlockList().remove(i);
                i--;
            }
    }
}
