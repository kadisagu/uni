package ru.tandemservice.unibasermc.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class DeclinationUtil {


    /**
     * Возвращает ФИО согласно склонению, настраиваемому в пользовательском интерфейсе в личном удостоверии
     *
     * @param variantCode
     * @param card
     *
     * @return
     */
    public static String getDeclinableFIO(GrammaCase grammaCase, IdentityCard card) {

        InflectorVariant variant = (InflectorVariant) DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase);
        IdentityCardDeclinability cardDeclinability = UniDaoFacade.getCoreDao().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), card.getId());

        String surname = null;
        String name = null;
        String patronymic = null;
        if (cardDeclinability != null) {
            if (cardDeclinability.isLastNameDeclinable())
                surname = DeclinableManager.instance().dao().getPropertyValue(cardDeclinability, "lastName", variant);
            else surname = card.getLastName();
            if (cardDeclinability.isFirstNameDeclinable())
                name = DeclinableManager.instance().dao().getPropertyValue(cardDeclinability, "firstName", variant);
            else name = card.getFirstName();
            if (cardDeclinability.isMiddleNameDeclinable())
                patronymic = DeclinableManager.instance().dao().getPropertyValue(cardDeclinability, "middleName", variant);
            else patronymic = card.getMiddleName();
        }

        StringBuilder builder = new StringBuilder(StringUtils.trimToEmpty(surname != null ? surname : PersonManager.instance().declinationDao().getDeclinationLastName(card.getLastName(), grammaCase, card.getPerson().isMale())))
                .append(" ")
                .append(StringUtils.trimToEmpty(name != null ? name : PersonManager.instance().declinationDao().getDeclinationFirstName(card.getFirstName(), grammaCase, card.getPerson().isMale())))
                .append(" ")
                .append(StringUtils.trimToEmpty(patronymic != null ? patronymic : PersonManager.instance().declinationDao().getDeclinationMiddleName(card.getMiddleName(), grammaCase, card.getPerson().isMale())));

        return builder.toString();
    }

    /**
     * Возвращает ИМЯ согласно склонению, настраиваемому в пользовательском интерфейсе в личном удостоверии
     *
     * @param grammaCase
     * @param card
     *
     * @return
     */
    public static String getDeclinableFirstName(GrammaCase grammaCase, IdentityCard card) {
        InflectorVariant variant = (InflectorVariant) DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase);
        IdentityCardDeclinability cardDeclinability = UniDaoFacade.getCoreDao().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), card.getId());

        String firstName = null;

        if (cardDeclinability != null) {
            if (cardDeclinability.isFirstNameDeclinable())
                firstName = DeclinableManager.instance().dao().getPropertyValue(cardDeclinability, "firstName", variant);
            else
                firstName = card.getFirstName();
        }

        return StringUtils.trimToEmpty(firstName != null ? firstName : PersonManager.instance().declinationDao().getDeclinationFirstName(card.getFirstName(), grammaCase, card.getPerson().isMale()));
    }

    /**
     * Возвращает ФАМИЛИЮ согласно склонению, настраиваемому в пользовательском интерфейсе в личном удостоверии
     *
     * @param grammaCase
     * @param card
     *
     * @return
     */
    public static String getDeclinableLastName(GrammaCase grammaCase, IdentityCard card) {
        InflectorVariant variant = (InflectorVariant) DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase);
        IdentityCardDeclinability cardDeclinability = UniDaoFacade.getCoreDao().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), card.getId());

        String lastName = null;

        if (cardDeclinability != null) {
            if (cardDeclinability.isLastNameDeclinable())
                lastName = DeclinableManager.instance().dao().getPropertyValue(cardDeclinability, "lastName", variant);
            else
                lastName = card.getLastName();
        }

        return StringUtils.trimToEmpty(lastName != null ? lastName : PersonManager.instance().declinationDao().getDeclinationLastName(card.getLastName(), grammaCase, card.getPerson().isMale()));
    }

    /**
     * Возвращает ОТЧЕСТВО согласно склонению, настраиваемому в пользовательском интерфейсе в личном удостоверии
     *
     * @param grammaCase
     * @param card
     *
     * @return
     */
    public static String getDeclinableMiddleName(GrammaCase grammaCase, IdentityCard card) {
        InflectorVariant variant = (InflectorVariant) DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(grammaCase);
        IdentityCardDeclinability cardDeclinability = UniDaoFacade.getCoreDao().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), card.getId());

        String middleName = null;

        if (cardDeclinability != null) {
            if (cardDeclinability.isMiddleNameDeclinable())
                middleName = DeclinableManager.instance().dao().getPropertyValue(cardDeclinability, "middleName", variant);
            else
                middleName = card.getMiddleName();
        }

        return StringUtils.trimToEmpty(middleName != null ? middleName : PersonManager.instance().declinationDao().getDeclinationMiddleName(card.getMiddleName(), grammaCase, card.getPerson().isMale()));
    }
}
