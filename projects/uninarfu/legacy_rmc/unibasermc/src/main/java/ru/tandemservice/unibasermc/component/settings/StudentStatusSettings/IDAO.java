package ru.tandemservice.unibasermc.component.settings.StudentStatusSettings;

public interface IDAO extends ru.tandemservice.uni.component.settings.StudentStatusSettings.IDAO {
    /**
     * @param studentStatusId
     */
    public void doToggleArchiveable(Long studentStatusId);
}
