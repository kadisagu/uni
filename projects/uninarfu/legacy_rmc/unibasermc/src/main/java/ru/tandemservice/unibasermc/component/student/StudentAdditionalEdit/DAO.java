package ru.tandemservice.unibasermc.component.student.StudentAdditionalEdit;

import ru.tandemservice.unibasermc.entity.catalog.StudentExt;


public class DAO extends ru.tandemservice.uni.component.student.StudentAdditionalEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        StudentExt studentExt = get(StudentExt.class, StudentExt.student(), model.getStudent());
        if (studentExt == null) {
            studentExt = new StudentExt();
            studentExt.setStudent(model.getStudent());
        }
        myModel.setStudentExt(studentExt);
    }

    @Override
    public void update(ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;
        StudentExt entity = myModel.getStudentExt();
        getSession().saveOrUpdate(entity);
    }
}
