package ru.tandemservice.unibasermc.component.catalog.rmcSettings.RmcSettingsPub;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unibasermc.entity.catalog.RmcSettings;
import ru.tandemservice.unibasermc.entity.catalog.RmcSettingsType;

import java.util.List;

public class Model {
    private IDataSettings _settings;
    private ISelectModel _uniSakaiRmcSettingsTypeModel;
    private List<RmcSettingsType> _uniSakaiRmcSettingsTypes;
    private DynamicListDataSource<RmcSettings> _dataSource;


    public void setDataSource(DynamicListDataSource<RmcSettings> _dataSource)
    {
        this._dataSource = _dataSource;
    }

    public DynamicListDataSource<RmcSettings> getDataSource()
    {
        return _dataSource;
    }


    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public void setRmcSettingsTypeModel(
            ISelectModel _uniSakaiRmcSettingsTypeModel)
    {
        this._uniSakaiRmcSettingsTypeModel = _uniSakaiRmcSettingsTypeModel;
    }

    public ISelectModel getRmcSettingsTypeModel() {
        return _uniSakaiRmcSettingsTypeModel;
    }

    public void setRmcSettingsTypes(
            List<RmcSettingsType> _uniSakaiRmcSettingsTypes)
    {
        this._uniSakaiRmcSettingsTypes = _uniSakaiRmcSettingsTypes;
    }

    public List<RmcSettingsType> getRmcSettingsTypes() {
        return _uniSakaiRmcSettingsTypes;
    }

}
