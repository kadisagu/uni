package ru.tandemservice.unibasermc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности иностранный язык
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonForeignLanguageExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt";
    public static final String ENTITY_NAME = "personForeignLanguageExt";
    public static final int VERSION_HASH = -1657315329;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_SECONDARY = "secondary";

    private PersonForeignLanguage _base;     // Иностранный язык персоны
    private boolean _secondary;     // Второй язык

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Иностранный язык персоны. Свойство не может быть null.
     */
    @NotNull
    public PersonForeignLanguage getBase()
    {
        return _base;
    }

    /**
     * @param base Иностранный язык персоны. Свойство не может быть null.
     */
    public void setBase(PersonForeignLanguage base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Второй язык. Свойство не может быть null.
     */
    @NotNull
    public boolean isSecondary()
    {
        return _secondary;
    }

    /**
     * @param secondary Второй язык. Свойство не может быть null.
     */
    public void setSecondary(boolean secondary)
    {
        dirty(_secondary, secondary);
        _secondary = secondary;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonForeignLanguageExtGen)
        {
            setBase(((PersonForeignLanguageExt)another).getBase());
            setSecondary(((PersonForeignLanguageExt)another).isSecondary());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonForeignLanguageExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonForeignLanguageExt.class;
        }

        public T newInstance()
        {
            return (T) new PersonForeignLanguageExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "secondary":
                    return obj.isSecondary();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((PersonForeignLanguage) value);
                    return;
                case "secondary":
                    obj.setSecondary((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "secondary":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "secondary":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return PersonForeignLanguage.class;
                case "secondary":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonForeignLanguageExt> _dslPath = new Path<PersonForeignLanguageExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonForeignLanguageExt");
    }
            

    /**
     * @return Иностранный язык персоны. Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt#getBase()
     */
    public static PersonForeignLanguage.Path<PersonForeignLanguage> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Второй язык. Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt#isSecondary()
     */
    public static PropertyPath<Boolean> secondary()
    {
        return _dslPath.secondary();
    }

    public static class Path<E extends PersonForeignLanguageExt> extends EntityPath<E>
    {
        private PersonForeignLanguage.Path<PersonForeignLanguage> _base;
        private PropertyPath<Boolean> _secondary;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Иностранный язык персоны. Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt#getBase()
     */
        public PersonForeignLanguage.Path<PersonForeignLanguage> base()
        {
            if(_base == null )
                _base = new PersonForeignLanguage.Path<PersonForeignLanguage>(L_BASE, this);
            return _base;
        }

    /**
     * @return Второй язык. Свойство не может быть null.
     * @see ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt#isSecondary()
     */
        public PropertyPath<Boolean> secondary()
        {
            if(_secondary == null )
                _secondary = new PropertyPath<Boolean>(PersonForeignLanguageExtGen.P_SECONDARY, this);
            return _secondary;
        }

        public Class getEntityClass()
        {
            return PersonForeignLanguageExt.class;
        }

        public String getEntityName()
        {
            return "personForeignLanguageExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
