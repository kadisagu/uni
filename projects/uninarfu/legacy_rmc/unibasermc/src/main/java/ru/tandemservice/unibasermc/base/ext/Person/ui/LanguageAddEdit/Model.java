package ru.tandemservice.unibasermc.base.ext.Person.ui.LanguageAddEdit;

import ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.LanguageAddEdit.Model {

    private PersonForeignLanguageExt languageExt = new PersonForeignLanguageExt();

    public PersonForeignLanguageExt getLanguageExt() {
        return languageExt;
    }

    public void setLanguageExt(PersonForeignLanguageExt languageExt) {
        this.languageExt = languageExt;
    }

}
