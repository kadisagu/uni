package ru.tandemservice.unibasermc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MS_unibasermc_1x0x0_0to1 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {
        if (!tool.tableExists("orgunittype_t") || !tool.tableExists("orgunit_t"))
            return;

		/*
SELECT 
  u.id, u.divisioncode_p, ut.code_p 
FROM 
  orgunit_t u, orgunittype_t ut
WHERE
  u.orgunittype_id = ut.id
  AND ut.code_p IN ('institute', 'faculty')
 
		 */
        String sql = "SELECT u.id, u.divisioncode_p, ut.code_p \n"
                + "FROM orgunit_t u, orgunittype_t ut \n"
                + "WHERE \n"
                + "u.orgunittype_id = ut.id \n"
                + "AND ut.code_p IN ('institute', 'faculty')  \n";
        ResultSet rs = tool.getStatement().executeQuery(sql);

        List<Object[]> units = new ArrayList<Object[]>();
        Set<String> existedCodes = new HashSet<String>();
        while (rs.next()) {
            Object[] arr = new Object[3];

            arr[0] = rs.getLong(1);//unit.id
            arr[1] = rs.getString(2);//unit.divisioncode
            if (!StringUtils.isEmpty((String) arr[1]))
                existedCodes.add((String) arr[1]);

            arr[2] = rs.getString(3);//unit.type.code

            units.add(arr);
        }
        rs.close();

        PreparedStatement pst = tool.getConnection().prepareStatement("UPDATE orgunit_t SET divisioncode_p=? WHERE id=?");
        for (Object[] unit : units) {
            String code = (String) unit[1];
            if (!StringUtils.isEmpty(code))
                continue;

            if ("faculty".equals(unit[2]))
                code = generateCode("f", existedCodes);
            else if ("institute".equals(unit[2]))
                code = generateCode("i", existedCodes);
            else continue;

            existedCodes.add(code);

            pst.setString(1, code);
            pst.setLong(2, (Long) unit[0]);
            pst.executeUpdate();
        }
        pst.close();
    }

    private static String generateCode(String prefix, Set<String> existedCodes) {
        int i = 1;
        String code = prefix + "_" + ((i < 10) ? "0" : "") + i;

        while (existedCodes.contains(code)) {
            i++;
            code = prefix + "_" + ((i < 10) ? "0" : "") + i;
        }

        return code;
    }
}
