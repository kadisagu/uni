package ru.tandemservice.unibasermc.component.group.GroupPub;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt;
import ru.tandemservice.uni.component.group.GroupPub.Model;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DAO extends ru.tandemservice.uni.component.group.GroupPub.DAO {
    @Override
    public void setArchival(Model model, boolean archival, ErrorCollector errorCollector) {
        Session session = getSession();
        Group group = model.getGroup();
        Calendar calendar;
        EducationYear endingYear;
        List<StudentStatusExt> archiveableList = getList(StudentStatusExt.class, StudentStatusExt.archievable().s(), true);
        List<StudentStatus> statusList = new ArrayList<>();
        for (StudentStatusExt item : archiveableList) {
            statusList.add(item.getStudentStatus());
        }
        if (archival) {
            calendar = Calendar.getInstance();
            endingYear = get(EducationYear.class, "intValue", calendar.get(Calendar.YEAR));

            group.setEndingYear(endingYear);
            group.setArchivingDate(calendar.getTime());

            List<Student> groupNonArchivalStudents = session.createCriteria(Student.class).add(Restrictions.eq("group", group)).add(Restrictions.eq("archival", false)).list();
            for (Student student : groupNonArchivalStudents) {
                if (!statusList.contains(student.getStatus())) {
                    throw new ApplicationException("Группа " + group.getTitle() + " не может быть списана в архив, поскольку в группе есть студенты в активном состоянии.");
                }
            }
            for (Student student : groupNonArchivalStudents) {
                student.setArchival(archival);
                student.setArchivingDate(calendar.getTime());
            }
        }
        else {
            Criteria c = getSession().createCriteria(Group.class);
            c.add(Restrictions.eq("title", group.getTitle()));
            c.add(Restrictions.eq("archival", Boolean.FALSE));
            c.setProjection(Projections.rowCount());
            Number number = (Number) c.uniqueResult();
            int num = number == null ? 0 : number.intValue();
            if (num > 1) {
                throw new ApplicationException("Нельзя восстановить группу из архива, т.к. уже существует группа с данным названием.");
            }

            List<Long> groupStudentsIds = new ArrayList<>();
            List<Student> groupStudents = getList(Student.class, "group", group);

            for (Student student : groupStudents) {
                student.setArchival(archival);
                student.setPersonalArchivalFileNumber(null);
                student.setDescription(null);

                groupStudentsIds.add(student.getId());
            }

            List<String> bookNumberConflictStudents = new ArrayList<>();
            if (!groupStudentsIds.isEmpty()) {
                DQLSelectBuilder subDql = new DQLSelectBuilder();
                subDql.fromEntity(Student.class, "s");
                subDql.column(DQLExpressions.property(Student.id().fromAlias("s")));
                subDql.where(DQLExpressions.and(
                        DQLExpressions.eq(
                                DQLExpressions.property(Student.bookNumber().fromAlias("s")),
                                DQLExpressions.property(Student.bookNumber().fromAlias("st"))
                        ),
                        DQLExpressions.ne(
                                DQLExpressions.property(Student.id().fromAlias("s")),
                                DQLExpressions.property(Student.id().fromAlias("st"))
                        ),
                        DQLExpressions.eq(
                                DQLExpressions.property(Student.archival().fromAlias("s")),
                                DQLExpressions.value(Boolean.FALSE)
                        )

                ));
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(Student.class, "st");
                dql.column(DQLExpressions.property(Student.person().identityCard().fromAlias("st")));
                dql.where(
                        DQLExpressions.and(
                                DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("st")), groupStudentsIds),
                                DQLExpressions.exists(subDql.getQuery()))
                );
                List<IdentityCard> identityCards = getList(dql);
//                String queryString = "select student.person.identityCard from ru.tandemservice.uni.entity.employee.Student student where student.id in (:students) and exists (select s.id from ru.tandemservice.uni.entity.employee.Student s where s.bookNumber = student.bookNumber and s.archival = :archival and s.id != student.id)";
//                List<IdentityCard> identityCards = session.createQuery(queryString).setParameterList("students", groupStudentsIds).setBoolean("archival", false).list();
                for (IdentityCard identityCard : identityCards)
                    bookNumberConflictStudents.add(identityCard.getFullFio());
                if (!bookNumberConflictStudents.isEmpty())
                    errorCollector.add("Данные из архива восстановлены. Номер зачетной книжки студента(ов) " + StringUtils.join(bookNumberConflictStudents.iterator(), ", ") + " совпадает с одним из номеров обучаемых студентов.");
            }
        }
        model.getGroup().setArchival(archival);
        update(model.getGroup());
    }
}
