package ru.tandemservice.unibasermc.catalog.bo.EducationLevelStage;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

@Configuration
public class EducationLevelStageManager extends BusinessObjectManager {

    public static EducationLevelStageManager instance() {
        return (EducationLevelStageManager) instance(EducationLevelStageManager.class);
    }

}
