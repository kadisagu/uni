package ru.tandemservice.tools;

import java.util.Date;
import java.util.GregorianCalendar;

/*
 * операции с датами
 */
public class DateCalc {
    /*
     * разница в днях между двумя датами
     */
    public static int getDayDiff(Date d1, Date d2)
    {
        int _retVal = 0;
        // разница в милисекундах
        long _diff = d2.getTime() - d1.getTime();
        _retVal = (int) (_diff / (1000 * 60 * 60 * 24));
        return _retVal;
    }

    public static int calcYears(GregorianCalendar birthDay, GregorianCalendar checkDay) {

        int years = checkDay.get(GregorianCalendar.YEAR) - birthDay.get(GregorianCalendar.YEAR);
        // корректируем, если текущий месяц в дате проверки меньше месяца даты рождения
        int checkMonth = checkDay.get(GregorianCalendar.MONTH);
        int birthMonth = birthDay.get(GregorianCalendar.MONTH);
        if (checkMonth < birthMonth) {
            years--;
        }
        else if (checkMonth == birthMonth
                && checkDay.get(GregorianCalendar.DAY_OF_MONTH) < birthDay.get(GregorianCalendar.DAY_OF_MONTH))
        {
            // отдельный случай - в случае равенства месяцев,
            // но меньшего дня месяца в дате проверки - корректируем
            years--;
        }
        return years;
    }


}
