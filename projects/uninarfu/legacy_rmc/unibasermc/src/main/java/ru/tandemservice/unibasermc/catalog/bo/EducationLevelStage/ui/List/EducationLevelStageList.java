package ru.tandemservice.unibasermc.catalog.bo.EducationLevelStage.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.ui.ItemList.CatalogItemList;

@Configuration
public class EducationLevelStageList extends BusinessComponentManager {
    public static final String CATALOG_ITEM_DS = "catalogItemDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .create();
    }

    @Bean
    public BlockListExtPoint mainBlockListExtPoint() {
        return blockListExtPointBuilder("mainBlockListExtPoint")
                .addBlock(htmlBlock("mainBlockListExtPoint", CatalogItemList.class.getPackage().getName() + ".Template"))
                .create();
    }
}
