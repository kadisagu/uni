package ru.tandemservice.unibasermc.base.ext.SystemAction.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemActionExtDAO extends UniBaseDao implements ISystemActionExtDAO {
    @Override
    public void setMainForeignLanguage() {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(PersonForeignLanguage.class, "pl");
        List<PersonForeignLanguage> list = getList(dql);
        Map<Long, List<PersonForeignLanguage>> resultMap = new HashMap<>();
        for (PersonForeignLanguage item : list) {
            Long id = item.getPerson().getId();
            if (!resultMap.containsKey(id)) {
                resultMap.put(item.getPerson().getId(), new ArrayList<PersonForeignLanguage>());
            }
            resultMap.get(id).add(item);
        }
        for (Long key : resultMap.keySet()) {
            if (resultMap.get(key).size() == 1 && !resultMap.get(key).get(0).isMain()) {
                PersonForeignLanguage language = resultMap.get(key).get(0);
                language.setMain(true);
                save(language);
            }
        }

    }
}
