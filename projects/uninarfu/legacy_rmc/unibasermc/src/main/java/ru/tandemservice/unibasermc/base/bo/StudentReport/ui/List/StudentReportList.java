package ru.tandemservice.unibasermc.base.bo.StudentReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.addon.IUIAddonBuilder;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtPointBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.ui.List.GlobalReportList;

import java.util.Collection;

@Configuration
public class StudentReportList extends BusinessComponentManager {

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        IPresenterExtPointBuilder builder = presenterExtPointBuilder();

        //поствляем addons из GlobalReportList в наш компонент
        GlobalReportList bean = GlobalReportManager.instance().getBean(GlobalReportList.class);
        Collection<IUIAddonBuilder> addons = bean.presenterExtPoint().getUiAddonsExtPoint().getItems().values();
        for (IUIAddonBuilder addonBuilder : addons) {
            builder.addAddon(addonBuilder);
        }


        return builder.create();
    }
}
