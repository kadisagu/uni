package ru.tandemservice.unibasermc.util.rmcsettings;

import ru.tandemservice.unibasermc.entity.catalog.RmcSettings;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class KeyValues {

    public static String getStringValue(String key, String fieldsName, boolean errorUp) throws Exception
    {
        RmcSettings val = UniDaoFacade.getCoreDao().get(RmcSettings.class, fieldsName, key);

        if (val == null) {
            if (errorUp)
                throw new Exception("Ключ " + key + " не задан");
            else
                return null;
        }
        else {
            return val.getStringValue();
        }
    }

    public static boolean getBooleanValue(String key, String fieldsName)
    {
        boolean retVal = false;

        try {
            String val = getStringValue(key, fieldsName, false).toLowerCase();

            if (val.equals("true"))
                retVal = true;

        }
        catch (Exception e) {
            retVal = false;
        }

        return retVal;
    }
}
