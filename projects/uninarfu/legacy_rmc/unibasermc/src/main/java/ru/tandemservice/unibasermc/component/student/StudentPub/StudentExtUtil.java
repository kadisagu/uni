package ru.tandemservice.unibasermc.component.student.StudentPub;

import ru.tandemservice.unibasermc.entity.catalog.StudentExt;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

public class StudentExtUtil {

    public static StudentExt getStudentExt(Student student) {

        StudentExt studentExt = UniDaoFacade.getCoreDao().get(StudentExt.class, StudentExt.L_STUDENT, student);

        if (studentExt != null)
            return studentExt;

        studentExt = new StudentExt();
        studentExt.setStudent(student);

        return studentExt;
    }

}
