package ru.tandemservice.unibasermc.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unibasermc.base.ext.SystemAction.SystemActionManagerExt;

public class SystemActionPubUIAddon extends UIAddon {
    public SystemActionPubUIAddon(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public void onSetMainForeignLanguage()
    {
        SystemActionManagerExt.instance().dao().setMainForeignLanguage();
    }

    public void onMoveStudentsToArchive() {

        this.getActivationBuilder().asRegion("ru.tandemservice.unibasermc.base.ext.UniStudent.ui.MoveStudentsToArchive")
                .parameter("orgUnitId", null)
                .activate();

    }
}
