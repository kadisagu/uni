package ru.tandemservice.unibasermc.component.catalog.rmcSettings.RmcSettingsPub;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unibasermc.entity.catalog.RmcSettings;
import ru.tandemservice.unibasermc.entity.catalog.RmcSettingsGroupType;
import ru.tandemservice.unibasermc.entity.catalog.RmcSettingsType;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        if (getSettingsKey() != null) {
            DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                    .fromEntity(RmcSettings.class, "set")
                    .column(property(RmcSettings.rmcSettingsType().id().fromAlias("set")))
                    .where(eq(property(RmcSettings.rmcSettingsGroupType().code().fromAlias("set")), value(getSettingsKey())));

            // только зависимые типы
            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(RmcSettingsType.class, "lst")
                    .column("lst")
                    .order(property(RmcSettingsType.priority().fromAlias("lst")))
                    .where(in(property(RmcSettingsType.id().fromAlias("lst")), dqlIn.buildQuery()));

            List<RmcSettingsType> lst = dql.createStatement(getSession()).list();
            model.setRmcSettingsTypeModel(new LazySimpleSelectModel<>(lst));
        }
        else
            model.setRmcSettingsTypeModel(new LazySimpleSelectModel<>(RmcSettingsType.class).setSortProperty(RmcSettingsType.P_PRIORITY));
    }

    @SuppressWarnings("unchecked")
    public <T extends IEntity> void prepareCustomDataSource(Model model, DynamicListDataSource<T> dataSource)
    {
        List<RmcSettings> itemList = _getDataList(model);

        int count = Math.max(itemList.size() + 1, 2);
        dataSource.setTotalSize(count);
        dataSource.setCountRow(count);
        dataSource.createPage((List<T>) itemList);
    }

    @SuppressWarnings("unchecked")
    private List<RmcSettings> _getDataList(Model model)
    {
        Session _session = getSession();

        Criteria criteria = _session.createCriteria(RmcSettings.class, "tbl");
        criteria.createAlias("tbl." + RmcSettings.L_RMC_SETTINGS_TYPE, "ttype");
        criteria.createAlias("tbl." + RmcSettings.L_RMC_SETTINGS_GROUP_TYPE, "grpType");

        if (model.getRmcSettingsTypes() != null && model.getRmcSettingsTypes().size() != 0)
            criteria.add(Restrictions.in("tbl." + RmcSettings.L_RMC_SETTINGS_TYPE, model.getRmcSettingsTypes()));

        if (getSettingsKey() != null)
            criteria.add(Restrictions.eq("ttype." + RmcSettingsGroupType.P_CODE, getSettingsKey()));

        criteria.addOrder(Order.asc("ttype." + RmcSettingsType.P_PRIORITY));
        criteria.addOrder(Order.asc(RmcSettings.P_PRIORITY));

        return criteria.list();
    }

    protected String getSettingsKey()
    {
        return null;
    }

}
