package ru.tandemservice.unibasermc.sec;

import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.PermissionMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.commonbase.sec.modifier.GlobalReportPermissionModifier;

import java.util.*;
import java.util.stream.Collectors;

public class StudentReportPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> stringSecurityConfigMetaMap)
    {
//        Map<String, PermissionGroupMeta> permissionGroupMetaMap = stringSecurityConfigMetaMap.values().stream()
//                .map(SecurityConfigMeta::getPermissionGroupList)
//                .flatMap(Collection::stream)
//                .collect(Collectors.toMap(PermissionGroupMeta::getName, meta -> meta, (meta1, meta2) -> meta1));
//
//        PermissionGroupMeta globalReportPG = permissionGroupMetaMap.get(GlobalReportPermissionModifier.GLOBAL_REPORT_PG);
//
//        Map<PermissionMeta, PermissionGroupMeta> globalReportPGMap = new HashMap<>();
//        globalReportPG.getPermissionGroupList().stream()
//                .forEach(pgm -> {
//                    for (PermissionMeta pm : pgm.getPermissionList())
//                        globalReportPGMap.put(pm, pgm);
//                });
//
//        final Map<String, GlobalReportDefinition> itemMap = GlobalReportManager.instance().reportListExtPoint().getItems();
//        List<PermissionMeta> toRemove = globalReportPGMap.keySet().stream()
//                .filter(pm -> itemMap.containsKey(pm.getName()))
//                .filter(pm -> itemMap.get(pm.getName()).getBlockName().equals("student"))
//                .collect(Collectors.toList());
//
//
//        PermissionGroupMeta studentReportListPG = PermissionMetaUtil.createPermissionGroup(globalReportPG, "studentReportListPG", "Отчеты модуля «Контингент студентов»");
//
//        List<String> permissionNames = new ArrayList<>();
//        List<String> permissionTitles = new ArrayList<>();
//        studentReportListPG.getPermissionGroupList().stream()
//                .map(PermissionGroupMeta::getPermissionList)
//                .flatMap(Collection::stream)
//                .forEach(meta->{
//                    permissionNames.add(meta.getName());
//                    permissionTitles.add(meta.getTitle());
//                });
//
//        for (PermissionMeta meta : toRemove)
//        {
//            globalReportPGMap.get(meta).getPermissionList().remove(meta);
//            if (!permissionNames.contains(meta.getName()) && !permissionTitles.contains(meta.getTitle()))
//                studentReportListPG.getPermissionList().add(meta);
//        }
    }
}
