package ru.tandemservice.unibasermc.base.ext.StudentReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAdd;

@Configuration
public class StudentReportPersonAddExt extends BusinessComponentExtensionManager {
    @Autowired
    private StudentReportPersonAdd personTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(personTab.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", StudentReportPersonAddUIAddon.class));
        return pi.create();
    }

}
