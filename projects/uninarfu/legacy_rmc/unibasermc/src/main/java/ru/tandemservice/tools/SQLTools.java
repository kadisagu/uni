package ru.tandemservice.tools;

import org.tandemframework.core.entity.EntityBase;


public class SQLTools {
    public static String GetIntCond(Object[] objs)
    {
        StringBuffer result = new StringBuffer();
        if (objs.length > 0) {
            result.append(((EntityBase) objs[0]).getId().toString());
            for (int i = 1; i < objs.length; i++) {
                result.append(", ");
                result.append(((EntityBase) objs[i]).getId().toString());
            }
        }
        return "(" + result.toString() + ")";

    }
}
