package ru.tandemservice.tools.OrgUnit;

import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

import java.util.List;

public class PositionByName {


    public EmployeePost getEmployeePost(Session session, OrgUnit unit, String positinName)
    {

        // EduLoadType t = null;


        EmployeePost _retVal = null;

        MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
        builder.addJoin("ep", EmployeePost.L_ORG_UNIT, "org");
        builder.addJoin("ep", EmployeePost.L_POST_STATUS, "state");

        builder.add(MQExpression.eq("org", OrgUnit.P_ID, unit.getId()));
        builder.add(MQExpression.eq("state", EmployeePostStatus.P_CODE, "1"));

        List<EmployeePost> empls = builder.getResultList(session);
        for (EmployeePost emp : empls) {
            String s_vrem = emp.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle();
            if (s_vrem.toLowerCase().indexOf(positinName.toLowerCase()) != -1) {
                _retVal = emp;
                continue;
            }
        }
        if (_retVal == null) {
            // Рекурсивно вверх
            OrgUnit _parent = unit.getParent();

            if (_parent != null)
                _retVal = getEmployeePost(session, _parent, positinName);
        }
        return _retVal;
    }

    public PositionInfo getEmployeePostInfo(Session session, OrgUnit unit, String positinName)
    {
        EmployeePost emp = getEmployeePost(session, unit, positinName);

        if (emp == null)
            return null;
        else {
            String s_pos = emp.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle();
            String s_fio = emp.getPerson().getFio();
            PositionInfo pi = new PositionInfo(s_pos, s_fio);

            return pi;
        }
    }

    public class PositionInfo {
        private String _position = "";
        private String _fio = "";

        public PositionInfo(String position, String fio)
        {
            setPosition(position);
            setFio(fio);
        }

        public void setPosition(String _position) {
            this._position = _position;
        }

        public String getPosition() {
            return _position;
        }

        public void setFio(String _fio) {
            this._fio = _fio;
        }

        public String getFio() {
            return _fio;
        }

    }

}
