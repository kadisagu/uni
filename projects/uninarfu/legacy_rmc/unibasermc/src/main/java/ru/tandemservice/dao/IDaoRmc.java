package ru.tandemservice.dao;

import org.tandemframework.common.catalog.entity.ICatalogItem;

public interface IDaoRmc {
    /**
     * читаем через хеш таблицу
     *
     * @param <T>
     * @param catalogItemClass
     * @param itemCode
     *
     * @return
     */
    <T extends ICatalogItem> T getCatalogItemHash(final Class<T> catalogItemClass, final String itemCode);
}
