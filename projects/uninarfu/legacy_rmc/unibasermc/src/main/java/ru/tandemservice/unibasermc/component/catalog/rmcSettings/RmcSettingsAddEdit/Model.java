package ru.tandemservice.unibasermc.component.catalog.rmcSettings.RmcSettingsAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import ru.tandemservice.unibasermc.entity.catalog.RmcSettings;

@Input({
        @Bind(key = "rmcSettingsId", binding = "rmcSettingsId")

})
@Output({
        @Bind(key = "rmcSettingsId", binding = "rmcSettingsId")
})
public class Model {
    private Long _rmcSettingsId;
    private RmcSettings _rmcSettings;


    public void setRmcSettingsId(Long _RmcSettingsId) {
        this._rmcSettingsId = _RmcSettingsId;
    }

    public Long getRmcSettingsId() {
        return _rmcSettingsId;
    }

    public void setRmcSettings(RmcSettings _RmcSettings) {
        this._rmcSettings = _RmcSettings;
    }

    public RmcSettings getRmcSettings()
    {
        return _rmcSettings;
    }

    public boolean getLongMessage()
    {
        if (_rmcSettings != null) {
            if (_rmcSettings.getStringValue() != null) {
                if (_rmcSettings.getStringValue().length() > 50)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        else
            return false;

    }

}
