package ru.tandemservice.tools;

import java.util.ArrayList;
import java.util.List;

public class ObjToListLong {
    public static List<Long> ToListLong(List<Object> lst)
    {
        List<Long> ll = new ArrayList<Long>();

        for (Object o : lst) {
            Long l = (long) 0;
            if (o != null)
                l = Long.parseLong(o.toString());

            ll.add(l);
        }
        return ll;
    }

}
