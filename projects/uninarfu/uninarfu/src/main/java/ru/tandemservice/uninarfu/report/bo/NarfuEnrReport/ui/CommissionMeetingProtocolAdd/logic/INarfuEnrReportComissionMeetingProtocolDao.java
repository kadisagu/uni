/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.NarfuEnrReportCommissionMeetingProtocolAddUI;

/**
 * @author Ekaterina Zvereva
 * @since 28.07.2015
 */
public interface INarfuEnrReportComissionMeetingProtocolDao extends INeedPersistenceSupport
{
    Long createReport(NarfuEnrReportCommissionMeetingProtocolAddUI model);
}
