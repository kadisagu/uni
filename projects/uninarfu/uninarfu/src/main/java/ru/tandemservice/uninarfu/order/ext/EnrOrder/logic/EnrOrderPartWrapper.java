/* $Id$ */
package ru.tandemservice.uninarfu.order.ext.EnrOrder.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 05.08.2015
 */
public class EnrOrderPartWrapper implements Comparable<EnrOrderPartWrapper>
{
//    private OrgUnit _formativeOrgUnit;
    private EduProgramForm _programForm;
    private EnrCompetitionType _enrCompetitionType;
    private List<EnrEnrollmentParagraph> _paragraphList = new ArrayList<>();

    public EnrOrderPartWrapper(EduProgramForm programForm, EnrCompetitionType competitionType)
    {
//        _formativeOrgUnit = formativeOrgUnit;
        _programForm = programForm;
        _enrCompetitionType = competitionType;
    }

//    public OrgUnit getFormativeOrgUnit()
//    {
//        return _formativeOrgUnit;
//    }
//
//    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
//    {
//        _formativeOrgUnit = formativeOrgUnit;
//    }

    public EduProgramForm getProgramForm()
    {
        return _programForm;
    }

    public void setProgramForm(EduProgramForm programForm)
    {
        _programForm = programForm;
    }

    public List<EnrEnrollmentParagraph> getParagraphList()
    {
        return _paragraphList;
    }

    public void setParagraphList(List<EnrEnrollmentParagraph> paragraphList)
    {
        _paragraphList = paragraphList;
    }

    public EnrCompetitionType getEnrCompetitionType()
    {
        return _enrCompetitionType;
    }

    public void setEnrCompetitionType(EnrCompetitionType enrCompetitionType)
    {
        _enrCompetitionType = enrCompetitionType;
    }


    @Override
    public int hashCode()
    {
        return  //_formativeOrgUnit.hashCode() &
                _enrCompetitionType.hashCode() &
                _programForm.hashCode();
    }



    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof EnrOrderPartWrapper))
            return false;

        EnrOrderPartWrapper that = (EnrOrderPartWrapper) obj;

        return _programForm.equals(that.getProgramForm())
                && _enrCompetitionType.equals(that.getEnrCompetitionType());
//                && _formativeOrgUnit.equals(that.getFormativeOrgUnit());
    }


    @Override
    public int compareTo(EnrOrderPartWrapper o)
    {
//        int result =  _formativeOrgUnit.getTitle().compareTo(o.getFormativeOrgUnit().getTitle());

//        if (result == 0)
         int result = _enrCompetitionType.getCode().compareTo(o.getEnrCompetitionType().getCode());

        if (result == 0)
            result =  _programForm.getCode().compareTo(o.getProgramForm().getCode());
        return result;
    }
}