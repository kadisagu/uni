/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuWorkPlan.ui.RegistryFromWPAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.EppWorkPlanManager;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;


import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 29.03.2016
 */
@Configuration
public class NarfuWorkPlanRegistryFromWPAdd extends BusinessComponentManager
{
    public static final String YEAR_EDUCATION_PROCESS_DS = "yearEducationProcessDS";
    public static final String PROGRAM_KIND_DS = "programKindDS";
    public static final String WORK_PLAN_DS = "workPlanDS";
    public static final String ELEMENT_LIST_DS = "elementListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(YEAR_EDUCATION_PROCESS_DS, EppWorkPlanManager.instance().yearEducationProcessDSHandler()))
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(PROGRAM_KIND_DS, getName(), EduProgramKind.defaultSelectDSHandler(getName())))
                .addDataSource(EppWorkPlanManager.instance().programSubjectDSConfig())
                .addDataSource(EppWorkPlanManager.instance().termListDSConfig())
                .addDataSource(selectDS(WORK_PLAN_DS, workPlanDSHandler()).addColumn(EppWorkPlan.P_TITLE))
                .addDataSource(searchListDS(ELEMENT_LIST_DS, elementListDSColumnListExtPoint(), elementListDSHandler()))

                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> workPlanDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            FilterUtils.applySelectFilter(dql, EppWorkPlan.year().fromAlias(alias), context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_YEAR_EDUCATION_PROCESS));
            FilterUtils.applySelectFilter(dql, EppWorkPlan.term().fromAlias(alias), context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_TERM));

            String epAlias = "ep";
            dql.fetchPath(DQLJoinType.inner, EppWorkPlan.parent().eduPlanVersion().eduPlan().fromAlias(alias), epAlias)
                    .where(eq(property(epAlias, EppEduPlan.programKind()), commonValue(context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_PROGRAM_KIND))));

            EduProgramSubject programSubject = context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_PROGRAM_SUBJECT);
            if (programSubject != null)
            {
                dql.joinEntity(epAlias, DQLJoinType.inner, EppEduPlanProf.class, "prof", eq(property(epAlias), property("prof")))
                        .where(eq(property("prof", EppEduPlanProf.programSubject()), value(programSubject)));
            }


            dql.where(ne(property(alias), commonValue(context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_TARGET_WORK_PLAN))));

            // Количество частей должно совпадает
            String partsAlias = "parts";
            dql.joinEntity(alias, DQLJoinType.inner, EppWorkPlanPart.class, partsAlias,
                           eq(property(partsAlias, EppWorkPlanPart.workPlan()), property(alias)))
                    .having(eq(DQLFunctions.count(property(partsAlias)),
                               commonValue(context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_NUMBER_WORK_PLAN_PARTS))));

            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), EppWorkPlan.class)
                .customize(customizer)
                .order(EppWorkPlan.number())
                .filter(EppWorkPlan.number());
    }

    @Bean
    public ColumnListExtPoint elementListDSColumnListExtPoint()
    {
        return columnListExtPointBuilder(ELEMENT_LIST_DS)
                .addColumn(checkboxColumn("select"))
                .addColumn(textColumn("index", EppWorkPlanRegistryElementRow.number()))
                .addColumn(textColumn("title", EppWorkPlanRegistryElementRow.title()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> elementListDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            dql.where(eq(property(alias, EppWorkPlanRegistryElementRow.workPlan()),
                         commonValue(context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_SOURCE_WORK_PLAN))))
                    .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias(alias), "el");

            NarfuWorkPlanRegistryFromWPAddUI.ELEMENT_TYPE type = context.get(NarfuWorkPlanRegistryFromWPAddUI.PARAM_ELEMENT_TYPE);
            switch (type)
            {
                case DISCIPLINE:
                    dql.where(instanceOf("el", EppRegistryDiscipline.class));
                    break;
                case PRACTICE:
                    dql.where(instanceOf("el", EppRegistryPractice.class));
                    break;
            }

            return dql;
        };
        return new EntityComboDataSourceHandler(getName(), EppWorkPlanRegistryElementRow.class)
                .customize(customizer)
                .order(EppWorkPlanRegistryElementRow.number());
    }
}