package ru.tandemservice.uninarfu.base.ext.UniStudent.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;

public class DiplomaUniStudentDAO extends CommonDAO implements IDiplomaUniStudentDAO {

    private static final Set<String> STRUCTURE_EDUCATION_LEVELS_CODES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(new String[]{
            StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_PROFILE, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_PROFILE,
//	        StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_SPECIALIZATION, StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY_SPECIALIZATION,
            StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE, StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE, //StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALIZATION
    })));

    @Override
    public EducationLevels getEducationLevels(List<Long> studentIds) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Student.class, "s");
        dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().code().fromAlias("s").s());
        dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().fromAlias("s").s());
        dql.addColumn(Student.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel().fromAlias("s").s());
        dql.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), studentIds));

        Set<EducationLevels> eduLevelIds = new HashSet<EducationLevels>();
        List<Object[]> lines = getList(dql);
        for (Object[] line : lines) {
            if (STRUCTURE_EDUCATION_LEVELS_CODES.contains(line[0].toString())) {
                eduLevelIds.add((EducationLevels) line[2]);
            }
            else {
                eduLevelIds.add((EducationLevels) line[1]);
            }
        }

        if (eduLevelIds.size() == 1) {
            return eduLevelIds.iterator().next();
        }
        throw new ApplicationException("У студентов должно быть одинаковое направление подготоки ГОС");
    }

    @Override
    public Qualifications getQualificationsFromEduPlan(List<Long> studentIds) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EppStudent2EduPlanVersion.class, "s2p");
        String qualifPath = "";//TODO DEV-6870 EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().educationLevelHighSchool().educationLevel().qualification().id().fromAlias("s2p").s();
        dql.addColumn(qualifPath);
        dql.addColumn(DQLFunctions.count(qualifPath));
        dql.where(DQLExpressions.and(
                DQLExpressions.in(DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("s2p")), studentIds),
                DQLExpressions.isNull(EppStudent2EduPlanVersion.removalDate().fromAlias("s2p").s())
        ));
        dql.group(qualifPath);

        int totalLines = 0;
        List<Object[]> lines = getList(dql);
        for (Object[] line : lines) {
            totalLines += ((Number) line[1]).intValue();
        }

        if (lines.size() == 0) {
            //ни у кого нет УП
            return null;
        }
        else if (lines.size() == 1 && totalLines == studentIds.size()) {
            //у всех студентов один и тот же УП
            return get(Qualifications.class, (Long) lines.get(0)[0]);
        }
        //либо разные УП, либо у кого-то есть УП, у кого-то нет
        throw new ApplicationException("Нельзя добавить в один протокол ГАК обучающихся с разной квалификацией");
    }
}
