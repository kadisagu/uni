/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.commonbase.base.util.DefaultNumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * @author Ekaterina Zvereva
 * @since 16.04.2015
 */
@Configuration
public class EnrEntrantExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public INumberGenerationRule<EnrEntrant> entrantNumberGenerationRule()
    {
        return new EnrNumberGenerationRule<EnrEntrant>(EnrEntrant.class, EnrEntrant.personalNumber(), DefaultNumberGenerationRule.toString(EnrEntrant.enrollmentCampaign().educationYear().intValue()))
        {
            @Override
            public String buildCandidate(EnrEntrant entrant, int currentQueueValue)
            {
                int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
                return "??" + String.format("%01d", (year % 10)) + String.format("%03d", currentQueueValue);
            }
        };
    }
}