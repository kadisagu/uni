/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DiplomaIssuance;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Andrey Andreev
 * @since 21.10.2015
 */
@Configuration
public class DiplomaIssuanceExtManager extends BusinessObjectExtensionManager
{
}