/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.HigherProfAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEdit;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.NarfuEduPlanManager;

/**
 * @author Ekaterina Zvereva
 * @since 20.04.2015
 */
@Configuration
public class EppEduPlanHigherProfAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDONE_NAME = "EppEduPlanHigherProfAddEditExt";
    public static final String EDULVLHS_DS = "edulvlHighSchoolDS";

    public static final String PROGRAM_SUBJECT = "programSubject";
    public static final String QUALIFICATION = "qualification";
    public static final String ORIENTATION = "orientation";
    public static final String ORG_UNIT = "orgUnit";

    @Autowired
    private EppEduPlanHigherProfAddEdit _eppEduPlaneAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduPlaneAddEdit.presenterExtPoint())
                .addDataSource(selectDS(EDULVLHS_DS, NarfuEduPlanManager.instance().eduPlanEduLevelHSchoolHandler()))
                .addAddon(uiAddon(ADDONE_NAME, EppEduPlanHigherProfAddEditExtUI.class))
                .addAction(new EppEduPlanHigherProfClickApply("onClickApply"))
                .create();
    }
}
