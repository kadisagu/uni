package ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author Andrey Andreev
 * @since 06.10.2015
 */
public interface INarfuEduPlanPrintDAO extends INeedPersistenceSupport
{
    /**
     * Формирует печатную форму для версии УП
     *
     * @param block Блок УП(в)
     * @return xls
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] formingEduPlanVersionReport(EppEduPlanVersionBlock block);
}
