/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.logic;

import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.List.PersonEduInstitutionWrapper;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 04.04.2015
 */
public interface IPersonEduDocumentDao extends org.tandemframework.shared.person.base.bo.PersonEduDocument.logic.IPersonEduDocumentDao
{

    List<PersonEduInstitutionWrapper> getPersonEduInstitutionWrapperList(Person person, ISecureRoleContext personRoleContext);

    int cleanStudentsWithEduDocument(Long eduDocumentId);
}
