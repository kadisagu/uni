/* $Id$ */
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Ekaterina Zvereva
 * @since 08.05.2015
 */
public class MS_uninarfu_2x8x0_4to5  extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.8.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement requestStatement = tool.getConnection().createStatement();
        requestStatement.execute("select student_id, theme_p  from studentvkrtheme_t");
        ResultSet requestResult = requestStatement.getResultSet();

        PreparedStatement updateStatement = tool.prepareStatement("update student_t set student_t.finalQualifyingWorkTheme_p = ? WHERE student_t.id = ?");

        int counter = 0;
        while (requestResult.next()) {

            updateStatement.setLong(2, requestResult.getLong(1));
            updateStatement.setString(1, requestResult.getString(2));
            updateStatement.addBatch();

            if (++counter % 256 == 0)
                updateStatement.executeBatch();

        }
        updateStatement.executeBatch();
    }

}
