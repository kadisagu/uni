/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlanVersion.ui.BlockListEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.orgunit.entity.EduOwnerOrgUnit;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockListEdit.EppEduPlanVersionBlockListEditUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandardBlock;
import ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 09.04.2015
 */
public class EppEduPlanVersionBlockListEditExtUI extends UIAddon
{
    private EppEduPlanVersionBlockListEditUI _presenter;
    public EppEduPlanVersionBlockListEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private Map<EduProgramSpecialization, EducationLevelsHighSchool> _levelsMap;

    public String getEduLvlHSId()
    {
        return "eduLvlHS." + _presenter.getCurrentSpec().getId();
    }

    public Map<EduProgramSpecialization, EducationLevelsHighSchool> getLevelsMap()
    {
        return _levelsMap;
    }

    public void setLevelsMap(Map<EduProgramSpecialization, EducationLevelsHighSchool> levelsMap)
    {
        _levelsMap = levelsMap;
    }

    public EducationLevelsHighSchool getEduLvlHS()
    {
        EducationLevelsHighSchool selectedItem = getLevelsMap().get(_presenter.getCurrentSpec());
        return selectedItem;
    }

    public void setEduLvlHS(EducationLevelsHighSchool eduLevel)
    {
        if (_presenter.isInclude())
            getLevelsMap().put(_presenter.getCurrentSpec(), eduLevel);
        else
            getLevelsMap().remove(_presenter.getCurrentSpec());
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = getPresenter();
        if (getLevelsMap() == null) {
            setLevelsMap(new HashMap<EduProgramSpecialization, EducationLevelsHighSchool>());
            for (EppEduPlanVersionSpecializationBlock block : DataAccessServices.dao().getList(EppEduPlanVersionSpecializationBlock.class,
                                 EppEduPlanVersionSpecializationBlock.eduPlanVersion(), _presenter.getHolder().getValue(), EppStateEduStandardBlock.programSpecialization().title().s())) {
                if (block.getProgramSpecialization() != null) {
                    NarfuEduPlanVerBlokToEduLVLHSRelation relation = DataAccessServices.dao().get(NarfuEduPlanVerBlokToEduLVLHSRelation.class, NarfuEduPlanVerBlokToEduLVLHSRelation.eduPlanVerBlock(), block);
                    if (relation != null)
                        getLevelsMap().put(block.getProgramSpecialization(), relation.getEducationLevelsHighSchool());
                }
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(EppEduPlanVersionBlockListEditExt.EDULVLHS_DS))
        {
            EduOwnerOrgUnit externalOrgUnit = _presenter.getOrgUnitMap().get(_presenter.getCurrentSpec());
            dataSource.put(EppEduPlanVersionBlockListEditExt.ORG_UNIT_KEY, externalOrgUnit == null ? null : externalOrgUnit.getOrgUnit());
            dataSource.put(EppEduPlanVersionBlockListEditExt.PROGRAM_SUBJECT_KEY, ((EduProgramSpecialization)_presenter.getCurrentSpec()));
            dataSource.put(EppEduPlanVersionBlockListEditExt.VERSION_KEY,_presenter.getHolder().getValue());
        }
    }


public EduOwnerOrgUnit getOrgUnit()
{
    EduOwnerOrgUnit selectedItem = _presenter.getOrgUnitMap().get(_presenter.getCurrentSpec());
    if (selectedItem == null && _presenter.isInclude())
    {
        // в случае единственного подразделения подставлять его автоматически
        List items = _presenter.getOrgUnitModel().findValues("").getObjects();
        if (items.size() == 1)
        {
            selectedItem = (EduOwnerOrgUnit) items.get(0);
            _presenter.getOrgUnitMap().put(_presenter.getCurrentSpec(), selectedItem);
        }
    }
    return selectedItem;
}

    public void setOrgUnit(EduOwnerOrgUnit orgUnit)
    {
        _presenter.setOrgUnit(orgUnit);
    }
}
