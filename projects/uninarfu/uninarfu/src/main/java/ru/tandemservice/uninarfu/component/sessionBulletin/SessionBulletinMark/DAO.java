/* $Id$ */
package ru.tandemservice.uninarfu.component.sessionBulletin.SessionBulletinMark;

import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.BulletinMarkRow;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model;

import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 05.05.2015
 */
public class DAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.DAO implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        ru.tandemservice.uninarfu.component.sessionBulletin.SessionBulletinMark.Model modelExt = (ru.tandemservice.uninarfu.component.sessionBulletin.SessionBulletinMark.Model) model;
        modelExt.setMarkSelectModel(model.getRowList().get(0).getMarkSelectModel());
    }

    @Override
    public void setMassMark(ru.tandemservice.uninarfu.component.sessionBulletin.SessionBulletinMark.Model model)
    {
        for (BulletinMarkRow row : model.getRowList())
        {
            row.setMark(model.getMassMark());
            row.setPerformDate(model.getDocPerformDate());
        }
    }
}
