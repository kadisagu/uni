/* $Id$ */
package ru.tandemservice.uninarfu.order.ext.EnrOrder.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPubUI;
import ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder.ui.PrintExtract.NarfuEnrOrderPrintExtract;

/**
 * @author Ekaterina Zvereva
 * @since 07.08.2015
 */
public class EnrOrderPrintExtractAction extends NamedUIAction
{

    protected EnrOrderPrintExtractAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EnrOrderPubUI)
        {
            EnrOrderPubUI orderPresenter = (EnrOrderPubUI)presenter;
            orderPresenter.getActivationBuilder().asDialog(NarfuEnrOrderPrintExtract.class).parameter("orderId", orderPresenter.getOrder().getId()).activate();
        }
    }
}