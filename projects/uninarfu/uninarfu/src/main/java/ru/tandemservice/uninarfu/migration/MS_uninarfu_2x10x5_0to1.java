/* $Id$ */
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Ekaterina Zvereva
 * @since 20.07.2016
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uninarfu_2x10x5_0to1 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        //берем код аспирантуры
        final Long asp2009subjectIndexId = MigrationUtils.getIdByCode(tool, "edu_c_pr_subject_index_t", "2009.00");
        String[] subjectCodes = new String[] {"01.01.01", "01.01.02", "01.01.05", "01.01.07", "01.01.09", "01.02.04", "01.02.06", "01.04.02", "01.04.03", "01.04.06",
                "01.04.07", "01.04.14", "01.04.16", "01.04.17", "01.04.21", "02.00.02", "02.00.03", "02.00.04", "02.00.08", "02.00.10", "03.01.04", "03.01.06",
                "03.01.09", "03.02.01", "03.02.04", "03.02.05", "03.02.06", "03.02.08", "03.02.10", "03.02.13", "03.03.04", "05.02.08", "05.02.10", "05.02.23",
                "05.08.01", "05.08.03", "05.08.05", "05.08.06", "05.09.03", "05.09.05", "05.13.01", "05.13.06", "05.13.10", "05.13.11", "05.13.15", "05.13.18", "05.14.02",
                "05.14.04", "05.16.01", "05.16.06", "05.16.09", "05.18.01", "05.18.07", "05.18.15", "05.19.01", "05.23.01", "05.23.03", "05.23.04", "05.23.05", "05.23.07",
                "05.23.17", "05.23.20", "05.23.21", "05.23.22", "05.26.01", "05.26.02", "05.26.03", "07.00.02", "07.00.03", "07.00.06", "07.00.07", "07.00.10", "08.00.01",
                "08.00.05", "08.00.10", "08.00.12", "08.00.13", "08.00.14", "09.00.01", "09.00.03", "09.00.08", "09.00.11", "10.01.01", "10.01.02", "10.01.03", "10.01.10",
                "10.02.01", "10.02.04", "10.02.20", "10.02.22", "12.00.01", "12.00.02", "12.00.03", "12.00.05", "12.00.08", "12.00.09", "12.00.14", "12.00.15", "13.00.01",
                "13.00.02", "13.00.08", "17.00.09", "22.00.03", "22.00.04", "22.00.08", "23.00.01", "23.00.02", "23.00.04", "24.00.01", "25.00.10", "25.00.16", "25.00.18",
                "25.00.20", "25.00.22", "25.00.23", "25.00.24", "25.00.25", "25.00.27", "25.00.28", "25.00.30", "25.00.36"};

        // Меняем коды направлений 2009.xx.xx.xx -> 2009.00.xx.xx.xx
        final MigrationUtils.BatchUpdater updater = new MigrationUtils.BatchUpdater("update edu_c_pr_subject_t set code_p='2009.00.'+subjectcode_p where subjectindex_id=? and subjectcode_p=?", DBType.LONG, DBType.EMPTY_STRING);
        for (String code : subjectCodes)
        {
            updater.addBatch(asp2009subjectIndexId, code);
        }

        updater.executeUpdate(tool);


        // Квалификация проф. образования "Кандидат наук". Меняем код "2009.00.PhD" -> "2009.00"
        tool.executeUpdate("update edu_c_pr_qual_t set code_p=? where code_p=?", "2009.00", "2009.00.PhD");

    }
}