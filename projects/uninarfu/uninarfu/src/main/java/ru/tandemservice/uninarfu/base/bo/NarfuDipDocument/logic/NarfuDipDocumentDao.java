/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocument.logic;

import com.beust.jcommander.internal.Lists;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 30.07.2015
 */
public class NarfuDipDocumentDao extends SharedBaseDao implements INarfuDipDocumentDao
{
    @Override
    public List<String> getDivisionCodeEmptyOrgUnitFullTitles(List<Long> studentIds)
    {
        if (null == studentIds || studentIds.isEmpty()) return Lists.newArrayList();
        return new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou")
                .column(property("ou", OrgUnit.fullTitle()))
                .where(exists(new DQLSelectBuilder().fromEntity(Student.class, "s")
                                      .joinPath(DQLJoinType.inner, Student.educationOrgUnit().formativeOrgUnit().fromAlias("s"), "fou")
                                      .where(isNull(property("fou", OrgUnit.divisionCode())))
                                      .where(in(property("s.id"), studentIds))
                                      .where(eq(property("fou"), property("ou")))
                                      .buildQuery()
                ))
                .createStatement(getSession()).list();
    }
}
