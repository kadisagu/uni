/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 24.04.2015
 */
@Configuration
public class EnrEntrantRequestExtManager extends BusinessObjectExtensionManager
{
}