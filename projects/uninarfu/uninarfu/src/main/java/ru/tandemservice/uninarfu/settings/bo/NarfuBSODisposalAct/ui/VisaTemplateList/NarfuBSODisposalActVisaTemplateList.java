/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuBSODisposalAct.ui.VisaTemplateList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem;

/**
 * @author Andrey Andreev
 * @since 21.03.2016
 */
@Configuration
public class NarfuBSODisposalActVisaTemplateList extends BusinessComponentManager
{
    public static final String VISA_TEMPLATE_SEARCH_DS = "visaTemplateSearchDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(VISA_TEMPLATE_SEARCH_DS, visaTemplateSearchDSColumns(), disposalActVisaItemDSHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint visaTemplateSearchDSColumns()
    {
        final SimpleMergeIdResolver visaTemplate = new SimpleMergeIdResolver(NarfuBSODisposalActVisaItem.visaTemplate().id());

        return columnListExtPointBuilder(VISA_TEMPLATE_SEARCH_DS)
                .addColumn(textColumn("title", NarfuBSODisposalActVisaItem.visaTemplate().title()).merger(visaTemplate))
                .addColumn(textColumn("postTitle", NarfuBSODisposalActVisaItem.possibleVisa().title()))
                .addColumn(textColumn("fio", NarfuBSODisposalActVisaItem.possibleVisa().entity().person().fullFio()))
                .addColumn(actionColumn("up", new Icon("up"), "onClickUp"))
                .addColumn(actionColumn("down", new Icon("down"), "onClickDown"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEdit").merger(visaTemplate))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDelete")
                                   .merger(visaTemplate)
                                   .alert(new FormattedMessage("visaTemplateSearchDS.delete.alert", NarfuBSODisposalActVisaItem.visaTemplate().title().s())))
                .create();
    }

    @Bean
    public EntityComboDataSourceHandler disposalActVisaItemDSHandler()
    {
        return NarfuBSODisposalActVisaItem.defaultSelectDSHandler(getName());
    }
}