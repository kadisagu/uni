/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DipDocument.ui.AddInformationAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEdit;

/**
 * @author Andrey Avetisov
 * @since 07.04.2015
 */
@Configuration
public class DipDocumentAddInformationAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uninarfu" + DipDocumentAddInformationAddEditExtUI.class.getSimpleName();

    @Autowired
    private DipDocumentAddInformationAddEdit _dipDocumentAddInformationAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_dipDocumentAddInformationAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, DipDocumentAddInformationAddEditExtUI.class))
                .create();
    }
}
