package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x8x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность enrEnrollmentCommissionExt

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("enrenrollmentcommissionext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_enrenrollmentcommissionext"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("enrenrollmentcommission_id", DBType.LONG).setNullable(false), 
				new DBColumn("employee_id", DBType.LONG), 
				new DBColumn("phonenumber_p", DBType.createVarchar(255)), 
				new DBColumn("scheduleenrollmentcommission_p", DBType.createVarchar(1000))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("enrEnrollmentCommissionExt");

		}


    }
}