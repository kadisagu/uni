package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь блока версии учебного плана и направления подготовки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuEduPlanVerBlokToEduLVLHSRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation";
    public static final String ENTITY_NAME = "narfuEduPlanVerBlokToEduLVLHSRelation";
    public static final int VERSION_HASH = 786681727;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN_VER_BLOCK = "eduPlanVerBlock";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";

    private EppEduPlanVersionSpecializationBlock _eduPlanVerBlock;     // Блок версии учебного плана
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок версии учебного плана. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlanVersionSpecializationBlock getEduPlanVerBlock()
    {
        return _eduPlanVerBlock;
    }

    /**
     * @param eduPlanVerBlock Блок версии учебного плана. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduPlanVerBlock(EppEduPlanVersionSpecializationBlock eduPlanVerBlock)
    {
        dirty(_eduPlanVerBlock, eduPlanVerBlock);
        _eduPlanVerBlock = eduPlanVerBlock;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки. Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NarfuEduPlanVerBlokToEduLVLHSRelationGen)
        {
            setEduPlanVerBlock(((NarfuEduPlanVerBlokToEduLVLHSRelation)another).getEduPlanVerBlock());
            setEducationLevelsHighSchool(((NarfuEduPlanVerBlokToEduLVLHSRelation)another).getEducationLevelsHighSchool());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuEduPlanVerBlokToEduLVLHSRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuEduPlanVerBlokToEduLVLHSRelation.class;
        }

        public T newInstance()
        {
            return (T) new NarfuEduPlanVerBlokToEduLVLHSRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlanVerBlock":
                    return obj.getEduPlanVerBlock();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlanVerBlock":
                    obj.setEduPlanVerBlock((EppEduPlanVersionSpecializationBlock) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlanVerBlock":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlanVerBlock":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlanVerBlock":
                    return EppEduPlanVersionSpecializationBlock.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuEduPlanVerBlokToEduLVLHSRelation> _dslPath = new Path<NarfuEduPlanVerBlokToEduLVLHSRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuEduPlanVerBlokToEduLVLHSRelation");
    }
            

    /**
     * @return Блок версии учебного плана. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation#getEduPlanVerBlock()
     */
    public static EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock> eduPlanVerBlock()
    {
        return _dslPath.eduPlanVerBlock();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    public static class Path<E extends NarfuEduPlanVerBlokToEduLVLHSRelation> extends EntityPath<E>
    {
        private EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock> _eduPlanVerBlock;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок версии учебного плана. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation#getEduPlanVerBlock()
     */
        public EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock> eduPlanVerBlock()
        {
            if(_eduPlanVerBlock == null )
                _eduPlanVerBlock = new EppEduPlanVersionSpecializationBlock.Path<EppEduPlanVersionSpecializationBlock>(L_EDU_PLAN_VER_BLOCK, this);
            return _eduPlanVerBlock;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

        public Class getEntityClass()
        {
            return NarfuEduPlanVerBlokToEduLVLHSRelation.class;
        }

        public String getEntityName()
        {
            return "narfuEduPlanVerBlokToEduLVLHSRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
