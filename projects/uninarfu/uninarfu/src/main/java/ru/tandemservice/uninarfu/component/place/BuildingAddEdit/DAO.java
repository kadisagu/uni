package ru.tandemservice.uninarfu.component.place.BuildingAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.settings.DataSettingsFacade;
import ru.tandemservice.uniplaces.UniplacesDefines;
import ru.tandemservice.uniplaces.component.place.BuildingAddEdit.Model;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.settings.BuildingNamingSettings;

/**
 * @author aandreev
 * @since 15.09.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.BuildingAddEdit.DAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            ((ru.tandemservice.uninarfu.component.place.BuildingAddEdit.Model) model)
                    .setBuildingNumber(
                            String.valueOf(model.getBuilding().getNumber()));
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (model.isEditForm()) {
            UniplacesBuilding building = getUnique(
                    UniplacesBuilding.class,
                    UniplacesBuilding.number().s(),
                    ((ru.tandemservice.uninarfu.component.place.BuildingAddEdit.Model) model).getIntBuildingNumber());
            if (building != null && !building.getId().equals(model.getBuilding().getId()))
                errors.add("Номер должен быть уникален", "number");
        }
    }

    @Override
    public void update(Model model)
    {
        // при редактировании изменение Порядкового номера(number) и генерация нового Номера(fullNumber)
        if (model.isEditForm()) {
            BuildingNamingSettings settings = DataSettingsFacade.getSettings("", UniplacesDefines.MODULE_SETTINGS_KEY).get(UniplacesDefines.SETTINGS_BUILDING_NAMING);
            if (settings == null)
                settings = new BuildingNamingSettings();

            UniplacesBuilding building = model.getBuilding();
            int number = ((ru.tandemservice.uninarfu.component.place.BuildingAddEdit.Model) model).getIntBuildingNumber();
            building.setNumber(number);
            building.setFullNumber(getBuildingNumber(settings.getPrefix(), settings.getDigitsNumber(), number));
        }

        super.update(model);
    }

    /**
     * @param prefix       префикс
     * @param digitsNumber количество цифр в номере
     * @param number       порядковый номер
     * @return полный номер здания
     */
    private String getBuildingNumber(String prefix, int digitsNumber, int number)
    {
        return String.format("%s%0" + digitsNumber + "d", StringUtils.defaultString(prefix), number);
    }
}
