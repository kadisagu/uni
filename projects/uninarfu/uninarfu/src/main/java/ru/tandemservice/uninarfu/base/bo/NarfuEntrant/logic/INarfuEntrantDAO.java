/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEntrant.logic;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
public interface INarfuEntrantDAO extends IUniBaseDao
{

    public void deleteSpecConditionForEntrant(EnrEntrant entrant);

    public void saveOrUpdateSpecConditions(EnrEntrant entrant, List<NarfuSpecConditionsEntTests> listConditions);
}