/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.IssuanceRegBook;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.NarfuDipDocumentReportManager;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.logic.EduLevelSelectModel;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 21.04.2015
 */
public class NarfuDipDocumentReportIssuanceRegBookUI extends UIPresenter
{
    private ISingleSelectModel _eduLevelSelectModel;
    private EduLevel _eduLevel;
    private boolean _showEduLevel;
    private List<DipDocumentType> _dipDocumentTypes;
    private boolean _showDipDocType;
    private List<OrgUnit> _formativeOrgUnitList;
    private boolean _showFormativeOrgUnit;
    private List<EduProgramSubject> _programSubjectList;
    private boolean _showProgramSubject;
    private Date _issueDateFrom;
    private Date _issueDateTo;
    private boolean _showIssueDateFrom;
    private boolean _showIssueDateTo;
    private boolean _duplicate;
    private Date _stateCommissionDateFrom;
    private boolean _showStateCommissionDateFrom;
    private Date _stateCommissionDateTo;
    private boolean _showStateCommissionDateTo;
    private String _stateCommissionProtocolNumber;
    private boolean _showStateCommissionProtocolNumber;

    private byte[] report = null;


    @Override
    public void onComponentPrepareRender()
    {
        if (report != null) {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("DipDocumentReport.rtf").document(report), true);
            report = null;
        }
    }

    @Override
    public void onComponentRefresh()
    {
        setShowEduLevel(false);
        setEduLevel(null);
        setShowDipDocType(false);
        setShowFormativeOrgUnit(false);
        setFormativeOrgUnitList(null);
        setShowProgramSubject(false);
        setProgramSubjectList(null);
        setShowIssueDateFrom(false);
        setIssueDateFrom(null);
        setShowIssueDateTo(false);
        setIssueDateTo(null);
        setDuplicate(false);

        setEduLevelSelectModel(new EduLevelSelectModel(true));
    }

    //GETTERS & SETTERS
    public ISingleSelectModel getEduLevelSelectModel()
    {
        return _eduLevelSelectModel;
    }

    public void setEduLevelSelectModel(ISingleSelectModel eduLevelSelectModel)
    {
        _eduLevelSelectModel = eduLevelSelectModel;
    }

    public EduLevel getEduLevel()
    {
        return _eduLevel;
    }

    public void setEduLevel(EduLevel eduLevel)
    {
        _eduLevel = eduLevel;
    }

    public List<DipDocumentType> getDipDocumentTypes()
    {
        return _dipDocumentTypes;
    }

    public void setDipDocumentTypes(List<DipDocumentType> dipDocumentTypes)
    {
        this._dipDocumentTypes = dipDocumentTypes;
    }

    public boolean isShowEduLevel()
    {
        return _showEduLevel;
    }

    public void setShowEduLevel(boolean showEduLevel)
    {
        _showEduLevel = showEduLevel;
    }

    public boolean isShowDipDocType()
    {
        return _showDipDocType;
    }

    public void setShowDipDocType(boolean showDipDocType)
    {
        _showDipDocType = showDipDocType;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isShowFormativeOrgUnit()
    {
        return _showFormativeOrgUnit;
    }

    public void setShowFormativeOrgUnit(boolean showFormativeOrgUnit)
    {
        _showFormativeOrgUnit = showFormativeOrgUnit;
    }

    public boolean isShowProgramSubject()
    {
        return _showProgramSubject;
    }

    public void setShowProgramSubject(boolean showProgramSubject)
    {
        _showProgramSubject = showProgramSubject;
    }

    public Date getIssueDateFrom()
    {
        return _issueDateFrom;
    }

    public void setIssueDateFrom(Date issueDateFrom)
    {
        _issueDateFrom = issueDateFrom;
    }

    public Date getIssueDateTo()
    {
        return _issueDateTo;
    }

    public void setIssueDateTo(Date issueDateTo)
    {
        _issueDateTo = issueDateTo;
    }

    public boolean isShowIssueDateFrom()
    {
        return _showIssueDateFrom;
    }

    public void setShowIssueDateFrom(boolean showIssueDateFrom)
    {
        _showIssueDateFrom = showIssueDateFrom;
    }

    public boolean isShowIssueDateTo()
    {
        return _showIssueDateTo;
    }

    public void setShowIssueDateTo(boolean showIssueDateTo)
    {
        _showIssueDateTo = showIssueDateTo;
    }

    public boolean isDuplicate()
    {
        return _duplicate;
    }

    public void setDuplicate(boolean duplicate)
    {
        _duplicate = duplicate;
    }

    public List<EduProgramSubject> getProgramSubjectList()
    {
        return _programSubjectList;
    }

    public void setProgramSubjectList(List<EduProgramSubject> programSubjectList)
    {
        _programSubjectList = programSubjectList;
    }

    public Date getStateCommissionDateFrom()
    {
        return _stateCommissionDateFrom;
    }

    public void setStateCommissionDateFrom(Date stateCommissionDateFrom)
    {
        _stateCommissionDateFrom = stateCommissionDateFrom;
    }

    public Date getStateCommissionDateTo()
    {
        return _stateCommissionDateTo;
    }

    public void setStateCommissionDateTo(Date stateCommissionDateTo)
    {
        _stateCommissionDateTo = stateCommissionDateTo;
    }

    public String getStateCommissionProtocolNumber()
    {
        return _stateCommissionProtocolNumber;
    }

    public void setStateCommissionProtocolNumber(String stateCommissionProtocolNumber)
    {
        _stateCommissionProtocolNumber = stateCommissionProtocolNumber;
    }

    public boolean isShowStateCommissionDateFrom()
    {
        return _showStateCommissionDateFrom;
    }

    public void setShowStateCommissionDateFrom(boolean showStateCommissionDateFrom)
    {
        _showStateCommissionDateFrom = showStateCommissionDateFrom;
    }

    public boolean isShowStateCommissionDateTo()
    {
        return _showStateCommissionDateTo;
    }

    public void setShowStateCommissionDateTo(boolean showStateCommissionDateTo)
    {
        _showStateCommissionDateTo = showStateCommissionDateTo;
    }

    public boolean isShowStateCommissionProtocolNumber()
    {
        return _showStateCommissionProtocolNumber;
    }

    public void setShowStateCommissionProtocolNumber(boolean showStateCommissionProtocolNumber)
    {
        _showStateCommissionProtocolNumber = showStateCommissionProtocolNumber;
    }

    public void onClickApply()
    {
        report = NarfuDipDocumentReportManager.instance().dao().createDipDocumentReportIssuanceBookReport(this);
    }
}
