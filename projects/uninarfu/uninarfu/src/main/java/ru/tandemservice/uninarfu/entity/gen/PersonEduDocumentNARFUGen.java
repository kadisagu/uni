package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Документ об образовании и (или) квалификации (расширение САФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonEduDocumentNARFUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU";
    public static final String ENTITY_NAME = "personEduDocumentNARFU";
    public static final int VERSION_HASH = -1456450488;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String P_DATE_ISSUE_CERTIFICATE = "dateIssueCertificate";
    public static final String P_EXPECTED_DATE_RETURN = "expectedDateReturn";
    public static final String P_ACTUAL_DATE_RETURN = "actualDateReturn";

    private PersonEduDocument _eduDocument;     // Документ об образовании и (или) квалификации
    private Date _dateIssueCertificate;     // Дата выдачи аттестата из ЛД
    private Date _expectedDateReturn;     // Планируемая дата возврата в ЛД
    private Date _actualDateReturn;     // Планируемая дата возврата в ЛД

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return Дата выдачи аттестата из ЛД.
     */
    public Date getDateIssueCertificate()
    {
        return _dateIssueCertificate;
    }

    /**
     * @param dateIssueCertificate Дата выдачи аттестата из ЛД.
     */
    public void setDateIssueCertificate(Date dateIssueCertificate)
    {
        dirty(_dateIssueCertificate, dateIssueCertificate);
        _dateIssueCertificate = dateIssueCertificate;
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     */
    public Date getExpectedDateReturn()
    {
        return _expectedDateReturn;
    }

    /**
     * @param expectedDateReturn Планируемая дата возврата в ЛД.
     */
    public void setExpectedDateReturn(Date expectedDateReturn)
    {
        dirty(_expectedDateReturn, expectedDateReturn);
        _expectedDateReturn = expectedDateReturn;
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     */
    public Date getActualDateReturn()
    {
        return _actualDateReturn;
    }

    /**
     * @param actualDateReturn Планируемая дата возврата в ЛД.
     */
    public void setActualDateReturn(Date actualDateReturn)
    {
        dirty(_actualDateReturn, actualDateReturn);
        _actualDateReturn = actualDateReturn;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonEduDocumentNARFUGen)
        {
            setEduDocument(((PersonEduDocumentNARFU)another).getEduDocument());
            setDateIssueCertificate(((PersonEduDocumentNARFU)another).getDateIssueCertificate());
            setExpectedDateReturn(((PersonEduDocumentNARFU)another).getExpectedDateReturn());
            setActualDateReturn(((PersonEduDocumentNARFU)another).getActualDateReturn());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonEduDocumentNARFUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonEduDocumentNARFU.class;
        }

        public T newInstance()
        {
            return (T) new PersonEduDocumentNARFU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduDocument":
                    return obj.getEduDocument();
                case "dateIssueCertificate":
                    return obj.getDateIssueCertificate();
                case "expectedDateReturn":
                    return obj.getExpectedDateReturn();
                case "actualDateReturn":
                    return obj.getActualDateReturn();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "dateIssueCertificate":
                    obj.setDateIssueCertificate((Date) value);
                    return;
                case "expectedDateReturn":
                    obj.setExpectedDateReturn((Date) value);
                    return;
                case "actualDateReturn":
                    obj.setActualDateReturn((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduDocument":
                        return true;
                case "dateIssueCertificate":
                        return true;
                case "expectedDateReturn":
                        return true;
                case "actualDateReturn":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduDocument":
                    return true;
                case "dateIssueCertificate":
                    return true;
                case "expectedDateReturn":
                    return true;
                case "actualDateReturn":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "dateIssueCertificate":
                    return Date.class;
                case "expectedDateReturn":
                    return Date.class;
                case "actualDateReturn":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonEduDocumentNARFU> _dslPath = new Path<PersonEduDocumentNARFU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonEduDocumentNARFU");
    }
            

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return Дата выдачи аттестата из ЛД.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getDateIssueCertificate()
     */
    public static PropertyPath<Date> dateIssueCertificate()
    {
        return _dslPath.dateIssueCertificate();
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getExpectedDateReturn()
     */
    public static PropertyPath<Date> expectedDateReturn()
    {
        return _dslPath.expectedDateReturn();
    }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getActualDateReturn()
     */
    public static PropertyPath<Date> actualDateReturn()
    {
        return _dslPath.actualDateReturn();
    }

    public static class Path<E extends PersonEduDocumentNARFU> extends EntityPath<E>
    {
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private PropertyPath<Date> _dateIssueCertificate;
        private PropertyPath<Date> _expectedDateReturn;
        private PropertyPath<Date> _actualDateReturn;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Документ об образовании и (или) квалификации. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return Дата выдачи аттестата из ЛД.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getDateIssueCertificate()
     */
        public PropertyPath<Date> dateIssueCertificate()
        {
            if(_dateIssueCertificate == null )
                _dateIssueCertificate = new PropertyPath<Date>(PersonEduDocumentNARFUGen.P_DATE_ISSUE_CERTIFICATE, this);
            return _dateIssueCertificate;
        }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getExpectedDateReturn()
     */
        public PropertyPath<Date> expectedDateReturn()
        {
            if(_expectedDateReturn == null )
                _expectedDateReturn = new PropertyPath<Date>(PersonEduDocumentNARFUGen.P_EXPECTED_DATE_RETURN, this);
            return _expectedDateReturn;
        }

    /**
     * @return Планируемая дата возврата в ЛД.
     * @see ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU#getActualDateReturn()
     */
        public PropertyPath<Date> actualDateReturn()
        {
            if(_actualDateReturn == null )
                _actualDateReturn = new PropertyPath<Date>(PersonEduDocumentNARFUGen.P_ACTUAL_DATE_RETURN, this);
            return _actualDateReturn;
        }

        public Class getEntityClass()
        {
            return PersonEduDocumentNARFU.class;
        }

        public String getEntityName()
        {
            return "personEduDocumentNARFU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
