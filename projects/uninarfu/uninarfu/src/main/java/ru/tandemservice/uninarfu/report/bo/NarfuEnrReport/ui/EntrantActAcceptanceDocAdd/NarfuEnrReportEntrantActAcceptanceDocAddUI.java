/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantActAcceptanceDocAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic.NarfuEnrollmentCommissionDSHandler;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 09.04.2015
 */
public class NarfuEnrReportEntrantActAcceptanceDocAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign enrollmentCampaign;

    private List<EnrEnrollmentCommission> _enrollmentCommissionList;
    private EnrRequestType _enrRequestType;
    private List<EduInstitutionOrgUnit> _enrOrgUnitList;
    private List<OrgUnit> _formativeOrgUnitList;

    private Date _date = new Date();
    private Integer _number;


    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    public void onClickApply()
    {
        IUniBaseDao.instance.get().doInTransaction(session -> {
            NarfuEnrReportEntrantActAcceptanceDocAddUI model = NarfuEnrReportEntrantActAcceptanceDocAddUI.this;

            IScriptItem scriptItem = DataAccessServices.dao().getByCode(
                    EnrScriptItem.class, EnrScriptItemCodes.REPORT_NARFU_ENTRANT_ACT_ACCEPTANCE_DOCUMENTS);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                    IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                    "model", model);

            byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == document) {
                throw new ApplicationException("Не найдено абитуриентов для включения в отчет.");
            }

            BusinessComponentUtils.downloadDocument(
                    new CommonBaseRenderer().fileName(filename).document(document), false);

            return 0L;
        });
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EnrEnrollmentCommissionManager.DS_ENROLLMENT_COMMISSION))
            dataSource.put(NarfuEnrollmentCommissionDSHandler.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    // getters and setters
    public List<EnrEnrollmentCommission> getEnrollmentCommissionList()
    {
        return _enrollmentCommissionList;
    }

    public void setEnrollmentCommissionList(List<EnrEnrollmentCommission> enrollmentCommissionList)
    {
        this._enrollmentCommissionList = enrollmentCommissionList;
    }

    public List<EduInstitutionOrgUnit> getEnrOrgUnitList()
    {
        return _enrOrgUnitList;
    }

    public void setEnrOrgUnitList(List<EduInstitutionOrgUnit> enrOrgUnitList)
    {
        this._enrOrgUnitList = enrOrgUnitList;
    }

    public EnrRequestType getEnrRequestType()
    {
        return _enrRequestType;
    }

    public void setEnrRequestType(EnrRequestType enrRequestType)
    {
        this._enrRequestType = enrRequestType;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        this._formativeOrgUnitList = formativeOrgUnitList;
    }


    public Date getDate()
    {
        return _date;
    }

    public Integer getNumber()
    {
        return _number;
    }

    public void setNumber(Integer number)
    {
        this._number = number;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}
