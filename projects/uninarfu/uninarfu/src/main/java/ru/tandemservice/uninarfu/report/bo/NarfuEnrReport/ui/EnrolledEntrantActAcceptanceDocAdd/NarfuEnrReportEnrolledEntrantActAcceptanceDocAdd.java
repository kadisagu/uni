/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EnrolledEntrantActAcceptanceDocAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Andrey Avetisov
 * @since 09.04.2015
 */
@Configuration
public class NarfuEnrReportEnrolledEntrantActAcceptanceDocAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String REQUEST_TYPE = "enrRequestType";
    public static final String ENROLLMENT_ORDER_LIST = "enrOrderList";
    public static final String ENROLLMENT_COMMISSION_LIST = "enrollmentCommissionList";

    public static final String ENR_ORG_UNIT_DS = "enrOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String ENROLLMENT_ORDER_DS = "enrollmentOrderDS";
    public static final String ENROLLMENT_COMMISSION_DS = "enrollmentCommissionDS";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
                .addDataSource(selectDS(ENROLLMENT_ORDER_DS, enrollmentOrderDSHandler()))
                .addDataSource(selectDS(ENROLLMENT_COMMISSION_DS, enrollmentCommissionDSHandler()))
                .addDataSource(selectDS(ENR_ORG_UNIT_DS, enrOrgUnitDSHandler())
                        .addColumn(OrgUnit.fullTitle().s()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler())
                        .addColumn(OrgUnit.fullTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentOrderDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrder.class)
                .where(EnrOrder.type().code(), (Object) EnrOrderTypeCodes.ENROLLMENT)
                .where(EnrOrder.enrollmentCampaign(), ENROLLMENT_CAMPAIGN)
                .where(EnrOrder.requestType(), REQUEST_TYPE)
                .filter(EnrOrder.number())
                .order(EnrOrder.number())
                .order(EnrOrder.commitDate());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentCommissionDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EnrEnrollmentCommission.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                DQLSelectBuilder subDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEnrollmentExtract.class, "r")
                        .column(property("r", EnrEnrollmentExtract.entity().request().enrollmentCommission().id()))
                        .where(in(property("r", EnrEnrollmentExtract.paragraph().order()), (List) ep.context.get(ENROLLMENT_ORDER_LIST)));

                ep.dqlBuilder
                        .where(in(
                                property(EnrEnrollmentCommission.id().fromAlias("e")),
                                subDQL.buildQuery()
                        ))
                        .where(eq(property("e", EnrEnrollmentCommission.enabled()), value(true)));
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrOrgUnitDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                String par_alias = "par";
                DQLSelectBuilder subDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEnrollmentParagraph.class, par_alias)
                        .column(property(par_alias, EnrEnrollmentParagraph.enrOrgUnit().institutionOrgUnit().orgUnit().id()))
                        .where(in(property(par_alias, EnrEnrollmentParagraph.order()), (List) ep.context.get(ENROLLMENT_ORDER_LIST)));

                ep.dqlBuilder
                        .where(in(
                                property(OrgUnit.id().fromAlias("e")),
                                subDQL.buildQuery()
                        ));
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), OrgUnit.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                String eee_alias = "eee";
                String par_alias = "par";
                DQLSelectBuilder subDQL = new DQLSelectBuilder()
                        .fromEntity(EnrEnrollmentExtract.class, eee_alias)
                        .fromEntity(EnrEnrollmentParagraph.class, par_alias)
                        .column(property(par_alias, EnrEnrollmentParagraph.formativeOrgUnit().id()))
                        .where(in(
                                property(eee_alias, EnrEnrollmentExtract.entity().request().enrollmentCommission()),
                                (List) ep.context.get(ENROLLMENT_COMMISSION_LIST)
                        ))
                        .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.paragraph().fromAlias(eee_alias), "abspar")
                        .where(instanceOf("abspar", EnrEnrollmentParagraph.class))
                        .where(eq(
                                property(eee_alias, EnrEnrollmentExtract.paragraph().id()),
                                property(par_alias, EnrEnrollmentParagraph.id())
                        ))
                        .where(in(property(par_alias, EnrEnrollmentParagraph.order()), (List) ep.context.get(ENROLLMENT_ORDER_LIST)));

                ep.dqlBuilder
                        .where(in(
                                property(OrgUnit.id().fromAlias("e")),
                                subDQL.buildQuery()
                        ));
            }
        };
    }

}
