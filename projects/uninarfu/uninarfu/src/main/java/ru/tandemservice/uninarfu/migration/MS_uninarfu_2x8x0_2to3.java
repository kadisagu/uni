/*$Id$*/
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author DMITRY KNYAZEV
 * @since 15.04.2015
 */
@SuppressWarnings({"all"})
public class MS_uninarfu_2x8x0_2to3 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.8.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        int comment_pos = 1;
        int id_pos = 2;
        final PreparedStatement insertStudentStatement = tool.prepareStatement("UPDATE student_t SET student_t.comment_p = ? WHERE student_t.id = ?");

        Statement stmt = tool.getConnection().createStatement();

        ResultSet rs = stmt.executeQuery("SELECT s.id, s.comment_p FROM student_t AS s WHERE s.comment_p IS NOT NULL");
        int counter = 0;
        while (rs.next()) {
            counter++;

            final long studentId = rs.getLong(1);
            final String comment = rs.getString(2);
            final String cleanComment = stripTags(comment);

            insertStudentStatement.setLong(id_pos, studentId);
            insertStudentStatement.setString(comment_pos, cleanComment);

            if (counter > 1000) {
                insertStudentStatement.executeBatch();
                counter = 0;
            }
            insertStudentStatement.addBatch();
        }
        insertStudentStatement.executeBatch();
    }

    private String stripTags(String htmlStr) {
        if(htmlStr == null)
            return htmlStr;
        return (htmlStr.replaceAll("&(\\w+)(\\S)", ""));
    }

}
