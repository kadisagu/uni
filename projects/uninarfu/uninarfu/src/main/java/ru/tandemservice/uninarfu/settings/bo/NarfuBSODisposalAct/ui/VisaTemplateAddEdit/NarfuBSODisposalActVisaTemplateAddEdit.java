/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuBSODisposalAct.ui.VisaTemplateAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 21.03.2016
 */
@Configuration
public class NarfuBSODisposalActVisaTemplateAddEdit extends BusinessComponentManager
{
    public static final String POSSIBLE_VISA_SELECT_DS = "possibleVisaSelectDS";
    public static final String PROPERTY_USED_VISAS_ID = "property_used_visas_id";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {

        return presenterExtPointBuilder()
                .addDataSource(selectDS(POSSIBLE_VISA_SELECT_DS, possibleVisasDSHandler())
                                       .addColumn("visaSelectDS.fullFio", EmployeePostPossibleVisa.entity().person().fullFio().s())
                                       .addColumn("visaSelectDS.title", EmployeePostPossibleVisa.title().s())
                                       .addColumn("visaSelectDS.orgUnit", null, (source) -> ((EmployeePostPossibleVisa) source)
                                               .getEntity().getOrgUnit().getExtendedShortTitle2()))
                .create();
    }


    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> possibleVisasDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EmployeePostPossibleVisa.class)
                .order(EmployeePostPossibleVisa.title())
                .customize((alias, dql, context, filter) -> {
                    List<Long> ids = context.<List<Long>>get(PROPERTY_USED_VISAS_ID);
                    if (!ids.isEmpty())
                        dql.where(notIn(property(alias, EmployeePostPossibleVisa.id()), ids));
                    return dql;
                })
                .pageable(true);
    }
}