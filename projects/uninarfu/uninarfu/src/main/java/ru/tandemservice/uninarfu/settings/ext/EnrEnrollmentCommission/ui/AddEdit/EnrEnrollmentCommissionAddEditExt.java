/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.EnrEnrollmentCommission.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AddEdit.EnrEnrollmentCommissionAddEdit;
import ru.tandemservice.uninarfu.settings.bo.NarfuEnrEnrollmentCommission.NarfuEnrEnrollmentCommissionManager;


/**
 * @author Ekaterina Zvereva
 * @since 10.06.2015
 */
@Configuration
public class EnrEnrollmentCommissionAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "narfuEnrEnrollmentCommissionAddEdit";

    public static final String EMPLOYEE_DS = "employeeDS";

    @Autowired
    private EnrEnrollmentCommissionAddEdit _enrollmentCommissionAddEdit;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(_enrollmentCommissionAddEdit.presenterExtPoint())
                .addDataSource(selectDS(EMPLOYEE_DS, NarfuEnrEnrollmentCommissionManager.instance().employeePostDSHandler()).addColumn(EmployeePost.P_FULL_TITLE))
                .addAddon(uiAddon(ADDON_NAME, EnrEnrollmentCommissionAddEditExtUI.class))
                .addAction(new EnrEnrollmentCommissionClickApply("onClickApply"))
                .create();
    }
}