/* $Id:$ */
package ru.tandemservice.uninarfu.base.ext.OrgUnitReport;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.ReportVisibleResolver;
import ru.tandemservice.uni.component.reports.FormativeOrgUnitReportVisibleResolver;

/**
 * @author rsizonenko
 * @since 05.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    public static final String NARFU_ORG_UNIT_GRANT_REPORT_BLOCK = "narfuOrgUnitGrantReportBlock";
    public static final String NARFU_ORG_UNIT_SCIENCE_WORK_REPORT_BLOCK = "narfuOrgUnitScienceWorkReportBlock";

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {
        return itemListExtension(_orgUnitReportManager.blockListExtPoint())
                .add(NARFU_ORG_UNIT_GRANT_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «Стипендии»", NARFU_ORG_UNIT_GRANT_REPORT_BLOCK, new ReportVisibleResolver()))
                .add(NARFU_ORG_UNIT_SCIENCE_WORK_REPORT_BLOCK, new OrgUnitReportBlockDefinition("Отчеты модуля «Научные работы»", NARFU_ORG_UNIT_SCIENCE_WORK_REPORT_BLOCK, new FormativeOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        final String unisessionOrgUnitReportBlock = ru.tandemservice.unisession.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNISESSION_ORG_UNIT_REPORT_BLOCK;
        final String uniOrgUnitStudentReportBlock = ru.tandemservice.uni.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNI_ORG_UNIT_STUDENT_REPORT_BLOCK;
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                //source: uninarfu/narfu
                .add("sessionSummaryBulletinReportNarfu", new OrgUnitReportDefinition("Сводная ведомость (САФУ)", "sessionSummaryBulletinReportNarfu", unisessionOrgUnitReportBlock, "SessionReportNarfuSummaryBulletinList"))

                //source: uninarfu/legacy_rmc/narfu
                .add("narfuOrgUnitAspirantReport", new OrgUnitReportDefinition("Сведения о приеме в аспирантуру за счет средств федерального бюджета", "orgUnitAspirantReport", NARFU_ORG_UNIT_SCIENCE_WORK_REPORT_BLOCK, "ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.List", "orgUnit_viewAspirantReport"))
                .add("narfuExcludedAspirantsReport", new OrgUnitReportDefinition("Сведения об аспирантах - гражданах РФ, обучавшихся за счет средств федерального бюджета и выбывших до окончания срока обучения", "excludedAspirantsReport", NARFU_ORG_UNIT_SCIENCE_WORK_REPORT_BLOCK, "ru.tandemservice.narfu.component.reports.ExcludedAspirants.List", "orgUnit_viewExcludedAspirantsReport"))
                .add("narfuFinishedAspirantsReport", new OrgUnitReportDefinition("Выпуск аспирантов - граждан РФ, обучавшихся по прямым договорам с оплатой стоимости обучения физическими или юридическими лицами, и защиты диссертаций на соискание ученой степени кандидата наук лицами этой категории, окончившими аспирантуру данного вуза без защиты диссертации, по отраслям науки и специальностям", "finishedAspirantsReport", NARFU_ORG_UNIT_SCIENCE_WORK_REPORT_BLOCK, "ru.tandemservice.narfu.component.reports.FinishedAspirants.List", "orgUnit_viewFinishedAspirantsReport"))


                .add("narfuOrgUnitStudentGrantByInstitute", new OrgUnitReportDefinition("Контингент студентов по институтам (академическая стипендия)", "orgUnitStudentGrantByInstitute", NARFU_ORG_UNIT_GRANT_REPORT_BLOCK, "ru.tandemservice.narfu.component.reports.orgUnitStudentGrantByInstitute.List", "orgUnit_viewGrantByInstituteReport"))


                .add("narfuStudentbookReport", new OrgUnitReportDefinition("Книга учета студ. Билетов и зачетных книжек", "studentbookReport", uniOrgUnitStudentReportBlock, "ru.tandemservice.narfu.component.reports.StudentbookReport",
                        ImmutableMap.<String, Object>builder()
                                .put("tabListVisible", ru.tandemservice.uni.base.ext.ReportPerson.ui.Add.ReportPersonAddExt.STUDENT_TAB)
                                .put("scheetListVisible", ru.tandemservice.uni.base.bo.StudentReportPerson.ui.Add.StudentReportPersonAddUI.STUDENT_SCHEET).build(), "orgUnit_viewStudentbookReport"))

                .add("narfuJournalAcademVacations", new OrgUnitReportDefinition("Журнал учета академических отпусков", "journalAcademVacations", uniOrgUnitStudentReportBlock, "ru.tandemservice.narfu.component.reports.JournalAcademVacations.List", "orgUnit_viewReportJournalAcademVacations"))
                .add("narfuStudentArchiveReport", new OrgUnitReportDefinition("Опись сдачи в архив личных дел отчисленных/выпускников", "studentArchiveReport", uniOrgUnitStudentReportBlock, "ru.tandemservice.narfu.component.reports.StudentArchiveReport.List", "orgUnit_viewStudentArchiveReport"))
                .add("narfuStudentGrant", new OrgUnitReportDefinition("Стипендии - начисление стипендии", "studentGrant", uniOrgUnitStudentReportBlock, "ru.tandemservice.narfu.component.reports.StudentGrant.List", "orgUnit_viewStudentGrantCharging"))
                .add("narfuStudentsAgeSexDistribution", new OrgUnitReportDefinition("Состав обучающихся по возрасту и полу (вариант 1)", "studentsAgeSexDistribution", uniOrgUnitStudentReportBlock, "ru.tandemservice.uni.component.reports.studentsAgeSexDistribution.List", "orgUnit_menuStudentsAgeSexDistribution"))
                .add("narfuStudentsAgeSexDistribution2", new OrgUnitReportDefinition("Состав обучающихся по возрасту и полу (вариант 2)", "studentsAgeSexDistribution2", uniOrgUnitStudentReportBlock, "ru.tandemservice.narfu.component.reports.StudentsAgeSexDistribution2.List", "orgUnit_menuStudentsAgeSexDistribution"))
                .add("narfuOrgUnitAspirantReportInUniBlock", new OrgUnitReportDefinition("Сведения о приеме в аспирантуру за счет средств федерального бюджета", "orgUnitAspirantReport", uniOrgUnitStudentReportBlock, "ru.tandemservice.narfu.component.reports.OrgUnitAspirantReport.List", "orgUnit_menuAspirantReport"))


                .create();
    }

}
