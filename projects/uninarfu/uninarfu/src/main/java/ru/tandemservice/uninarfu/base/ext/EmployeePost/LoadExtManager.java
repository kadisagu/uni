/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.EmployeePost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.IEmployeeAdditionalInfo;
import ru.tandemservice.uninarfu.base.ext.EmployeePost.logic.NarfuEmployeeAdditionalInfo;

/**
 * @author Andrey Andreev
 * @since 05.10.2016
 */
@Configuration
public class LoadExtManager extends BusinessObjectExtensionManager
{
	@Bean
	@BeanOverride
	public IEmployeeAdditionalInfo employeeInfo()
	{
		return new NarfuEmployeeAdditionalInfo();
	}
}
