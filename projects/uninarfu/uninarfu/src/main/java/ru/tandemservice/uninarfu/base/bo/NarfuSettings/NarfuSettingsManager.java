/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.uninarfu.base.bo.NarfuSettings.logic.INarfuFormativeOrgUnitDao;
import ru.tandemservice.uninarfu.base.bo.NarfuSettings.logic.NarfuFormativeOrgUnitDao;
import ru.tandemservice.uninarfu.base.bo.NarfuSettings.logic.NarfuFormativeOrgUnitsCodesDSHandler;

/**
 * @author Ekaterina Zvereva
 * @since 17.06.2015
 */
@Configuration
public class NarfuSettingsManager extends BusinessObjectManager
{
    public static NarfuSettingsManager instance()
    {
        return instance(NarfuSettingsManager.class);
    }

    @Bean
    public IDefaultSearchDataSourceHandler getFormativeOrgUnitsCodesHandler() {return new NarfuFormativeOrgUnitsCodesDSHandler(getName());}

    @Bean
    public INarfuFormativeOrgUnitDao dao()
    {
        return new NarfuFormativeOrgUnitDao();
    }
}
