/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.IUIPresenter;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;

/**
 * @author Ekaterina Zvereva
 * @since 23.04.2015
 */
@Configuration
public class NarfuEntrantRequestActionsAddon extends EnrEntrantRequestActionsAddon
{
    public NarfuEntrantRequestActionsAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

}