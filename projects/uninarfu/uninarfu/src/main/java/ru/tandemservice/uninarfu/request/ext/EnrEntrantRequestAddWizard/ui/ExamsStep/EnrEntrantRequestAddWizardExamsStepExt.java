/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequestAddWizard.ui.ExamsStep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ExamsStep.EnrEntrantRequestAddWizardExamsStep;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.NarfuEntrantManager;

/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
@Configuration
public class EnrEntrantRequestAddWizardExamsStepExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "narfuEnrEntrantRequestAddWizardExamsStepExt";
    public static final String SPEC_COND_DS = "specConditionsDS";

    @Autowired
    private EnrEntrantRequestAddWizardExamsStep _enrEntrantRequestAddWizardExamsStep;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantRequestAddWizardExamsStep.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantRequestAddWizardExamsStepExtUI.class))
                .addDataSource(selectDS(SPEC_COND_DS, NarfuEntrantManager.instance().specialConditionsExamsDSHandler()))
                .create();
    }
}