/* $Id$ */
package ru.tandemservice.uninarfu.order.ext.EnrOrder.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 05.08.2015
 */
public class EnrOrderFilialPartWrapper implements Comparable<EnrOrderFilialPartWrapper>
{
    private OrgUnit _branch;
    private List<EnrOrderPartWrapper> _orderParts = new ArrayList<>();

    public EnrOrderFilialPartWrapper(OrgUnit orgUnit)
    {
        if (orgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) || orgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.COLLEGE))
            _branch = orgUnit;
        else if (orgUnit.getParent().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) || orgUnit.getParent().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.COLLEGE))
            _branch = orgUnit.getParent();
        else
            _branch = TopOrgUnit.getInstance();
    }

    public OrgUnit getBranch()
    {
        return _branch;
    }

    public void setBranch(OrgUnit branch)
    {
        _branch = branch;
    }

    public List<EnrOrderPartWrapper> getOrderParts()
    {
        return _orderParts;
    }

    public void setOrderParts(List<EnrOrderPartWrapper> orderParts)
    {
        _orderParts = orderParts;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EnrOrderFilialPartWrapper that = (EnrOrderFilialPartWrapper) o;

        return (_branch.equals(that._branch));

    }

    @Override
    public int hashCode()
    {
        return _branch.hashCode();
    }

    @Override
    public int compareTo(EnrOrderFilialPartWrapper obj)
    {
        String firstType = _branch.getOrgUnitType().getCode();
        String secondType = obj.getBranch().getOrgUnitType().getCode();

        if (firstType.equals(OrgUnitTypeCodes.BRANCH) && secondType.equals(OrgUnitTypeCodes.BRANCH))
            return _branch.getTitle().compareTo(obj.getBranch().getTitle());
        if (firstType.equals(OrgUnitTypeCodes.TOP))
            return 1;

        return -1;

    }
}