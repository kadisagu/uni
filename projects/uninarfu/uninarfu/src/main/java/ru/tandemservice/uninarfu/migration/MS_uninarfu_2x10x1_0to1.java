package ru.tandemservice.uninarfu.migration;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uninarfu_2x10x1_0to1 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        // Получаем уже существующие элементы ctrPrintTemplate
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        SQLSelectQuery selectCtrPrintTemplateQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_prnt_tmpl_t", "pt").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=pt.id"))
                .column("si.id")
                .column("si.code_p");

        List<Object[]> ctrPrintTemplates = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectCtrPrintTemplateQuery));

        for(Object[] obj : ctrPrintTemplates)
        {
            ctrPrintTemplateMap.put((String) obj[1], (Long) obj[0]);
        }

        fillCatalogs(ctrPrintTemplateMap, contractKindMap, tool);

        clearCatalogs(tool);
    }


    private void fillCatalogs(Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrPrintTemplate

        // гарантировать наличие кода сущности
        short ctrPrintTemplateCode = tool.entityCodes().ensure("ctrPrintTemplate");

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();


        ////////////////////////////////////////////////////////////////////////////////
        // заполняем справочники ctrPrintTemplate ctr_c_prnt_tmpl_t

        // Получаем скрипты ctrTemplateScriptItem которые мигрируем
        SQLSelectQuery selectСtrTemplateScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_tmpl_script_t", "ts").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=ts.id"))
                .column("si.id")
                .column("si.code_p")
                .column("si.usertemplate_p")
                .column("si.usertemplateeditdate_p")
                .column("si.usertemplatecomment_p")
                .column("si.userscript_p")
                .column("si.defaultscript_p")
                .column("si.userscripteditdate_p")
                .column("si.usescript_p");

        List<Object[]> ctrTemplateScriptItems = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class, Blob.class, Timestamp.class, String.class, String.class, String.class, Timestamp.class, Boolean.class),
                translator.toSql(selectСtrTemplateScriptItemQuery));

        final Map<String, Object[]> ctrTemplateScriptItemMap = Maps.newHashMap();
        for(Object[] obj : ctrTemplateScriptItems)
        {
            ctrTemplateScriptItemMap.put((String) obj[1], obj);
        }

        tool.dropConstraint("scriptitem_t", "chk_class_scriptitem");

        // CtrPrintTemplateCodes
        /** Константа кода (code) элемента : Архангельск (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_ARKH = "narfu.edu.ctr.template.vo.2s.arkh";
        /** Константа кода (code) элемента : Архангельск (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_ARKH = "narfu.edu.ctr.template.vo.3sp.arkh";
        /** Константа кода (code) элемента : Архангельск (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_ARKH = "narfu.edu.ctr.template.vo.3so.arkh";
        /** Константа кода (code) элемента : Коряжма (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_KOR = "narfu.edu.ctr.template.vo.2s.kor";
        /** Константа кода (code) элемента : Коряжма (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_KOR = "narfu.edu.ctr.template.vo.3sp.kor";
        /** Константа кода (code) элемента : Коряжма (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_KOR = "narfu.edu.ctr.template.vo.3so.kor";
        /** Константа кода (code) элемента : Северодвинск (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_SEVER = "narfu.edu.ctr.template.vo.2s.sever";
        /** Константа кода (code) элемента : Северодвинск (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_SEVER = "narfu.edu.ctr.template.vo.3sp.sever";
        /** Константа кода (code) элемента : Северодвинск (title) */
        String NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_SEVER = "narfu.edu.ctr.template.vo.3so.sever";


        // CtrContractKindCodes
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
        String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
        String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
        String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";

        // code - "narfu.edu.ctr.template.vo.2s.arkh",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractArkhangelsk.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractArkhangelskPrintScript.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_ARKH))
        {
            String ctrPrintTemplateTitle = "Архангельск";
            String ctrPrintTemplateShortTitle = "Архангельск";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_ARKH;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractArkhangelsk.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractArkhangelskPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Arkh");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_2_SIDES), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.3sp.arkh",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractArkhangelsk.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractArkhangelskPrintScript.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_ARKH))
        {
            String ctrPrintTemplateTitle = "Архангельск";
            String ctrPrintTemplateShortTitle = "Архангельск";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_ARKH;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractArkhangelsk.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractArkhangelskPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Arkh");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.3so.arkh",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractArkhangelsk.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractArkhangelskPrintScript.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_ARKH))
        {
            String ctrPrintTemplateTitle = "Архангельск";
            String ctrPrintTemplateShortTitle = "Архангельск";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_ARKH;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractArkhangelsk.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractArkhangelskPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Arkh");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.2s.kor",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractKorjazhma.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractKorjazhmaPrintScript.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_KOR))
        {
            String ctrPrintTemplateTitle = "Коряжма";
            String ctrPrintTemplateShortTitle = "Коряжма";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_KOR;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractKorjazhma.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractKorjazhmaPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Kor");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_2_SIDES), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.3sp.kor",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractKorjazhma.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractKorjazhmaPrintScript.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_KOR))
        {
            String ctrPrintTemplateTitle = "Коряжма";
            String ctrPrintTemplateShortTitle = "Коряжма";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_KOR;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractKorjazhma.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractKorjazhmaPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Kor");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.3so.kor",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractKorjazhma.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractKorjazhmaPrintScript.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_KOR))
        {
            String ctrPrintTemplateTitle = "Коряжма";
            String ctrPrintTemplateShortTitle = "Коряжма";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_KOR;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractKorjazhma.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractKorjazhmaPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Kor");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.2s.sever",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractSeverodvinsk.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractSeverodvinskPrintScript.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_SEVER))
        {
            String ctrPrintTemplateTitle = "Северодвинск";
            String ctrPrintTemplateShortTitle = "Северодвинск";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_SEVER;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractSeverodvinsk.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractSeverodvinskPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Sever");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_2_SIDES), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.3sp.sever",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractSeverodvinsk.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractSeverodvinskPrintScript.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_SEVER))
        {
            String ctrPrintTemplateTitle = "Северодвинск";
            String ctrPrintTemplateShortTitle = "Северодвинск";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_SEVER;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractSeverodvinsk.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractSeverodvinskPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Sever");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "narfu.edu.ctr.template.vo.3so.sever",
        // templatePath - "uninarfu/templates/ctr/NarfuEduCtrContractSeverodvinsk.rtf"
        // scriptPath - "uninarfu/scripts/ctr/NarfuEduCtrContractSeverodvinskPrintScript.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_SEVER))
        {
            String ctrPrintTemplateTitle = "Северодвинск";
            String ctrPrintTemplateShortTitle = "Северодвинск";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = NARFU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_SEVER;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uninarfu/templates/ctr/NarfuEduCtrContractSeverodvinsk.rtf";
            String scriptPath = "uninarfu/scripts/ctr/NarfuEduCtrContractSeverodvinskPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("narfu.edu.ctrtemplate.Sever");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

    }

    private void addCtrPrintTemplate(Long id, String code, String templatePath, String scriptPath, Object[] templateScriptItem, short ctrPrintTemplateCode, Long kindId, String title, String shortTitle, DBTool tool, ISQLTranslator translator) throws SQLException
    {
        SQLInsertQuery insertScriptItemQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertScriptItemNoBlobQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
//                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertCtrPrintTemplateQuery = new SQLInsertQuery("ctr_c_prnt_tmpl_t")
                .set("id", "?")
                .set("shorttitle_p", "?")
                .set("kind_id", "?");

        String catalogCode = "ctrPrintTemplate";

        Blob userTemplate;
        Timestamp userTemplateEditDate;
        String userTemplateComment;
        String userScript;
        String defaultScript;
        Timestamp userScriptEditDate;
        Boolean useScript;

        if(templateScriptItem != null)
        {
            userTemplate = (Blob) templateScriptItem[2];
            userTemplateEditDate = (Timestamp) templateScriptItem[3];
            userTemplateComment = (String) templateScriptItem[4];
            userScript = (String) templateScriptItem[5];
            defaultScript = (String) templateScriptItem[6];
            userScriptEditDate = (Timestamp) templateScriptItem[7];
            useScript = (Boolean) templateScriptItem[8];
            if(useScript == null) useScript = false;
        }
        else
        {
            userTemplate = null;
            userTemplateEditDate = null;
            userTemplateComment = null;
            userScript = null;
            defaultScript = null;
            userScriptEditDate = null;
            useScript = false;
        }

        if(userTemplate != null)
        {
            tool.executeUpdate(translator.toSql(insertScriptItemQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplate, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        else
        {
            tool.executeUpdate(translator.toSql(insertScriptItemNoBlobQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        tool.executeUpdate(translator.toSql(insertCtrPrintTemplateQuery), id, shortTitle, kindId);
    }

    private void clearCatalogs(DBTool tool) throws SQLException
    {
        // удаляем скрипты
        // "narfu.edu.ctrtemplate.Arkh" Договор на обучение по ОП (Архангельск)
        // "narfu.edu.ctrtemplate.Kor"  Договор на обучение по ОП (Коряжма)
        // "narfu.edu.ctrtemplate.Sever" Договор на обучение по ОП (Северодвинск)
        // "narfu.edu.ctrtemplate.filial" Договор на обучение по ОП (Северодвинск)
        // "narfu.edu.ctrtemplate.head" Договор на обучение по ОП (Северодвинск)

        SQLSelectQuery selectScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("scriptitem_t"))
                .where("code_p in ('narfu.edu.ctrtemplate.Arkh', 'narfu.edu.ctrtemplate.Kor', 'narfu.edu.ctrtemplate.Sever', 'narfu.edu.ctrtemplate.filial', 'narfu.edu.ctrtemplate.head')")
                .column("id");

        List<Object[]> scriptItemIdObjList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectScriptItemQuery)); //, "(" + StringUtils.join(scriptItemCodes, ",") + ")");

        if(!scriptItemIdObjList.isEmpty())
        {
            List<Long> scriptItemIdList = scriptItemIdObjList.stream().map(a -> (Long) a[0]).collect(Collectors.toList());

            SQLDeleteQuery deleteCtrTemplateScriptQuery = new SQLDeleteQuery("ctr_c_tmpl_script_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(deleteCtrTemplateScriptQuery)); // , "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            SQLDeleteQuery deleteScriptItemQuery = new SQLDeleteQuery("scriptitem_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(deleteScriptItemQuery));
        }
    }
}
