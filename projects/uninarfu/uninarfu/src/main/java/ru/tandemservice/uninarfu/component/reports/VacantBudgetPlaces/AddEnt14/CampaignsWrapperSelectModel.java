/* $Id$ */
package ru.tandemservice.uninarfu.component.reports.VacantBudgetPlaces.AddEnt14;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.BaseFullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 13.10.2015
 */
public class CampaignsWrapperSelectModel extends BaseFullCheckSelectModel
{

    @Override
    public CampaignsWrapperSelectModel setPageSize(final int size)
    {
        return (CampaignsWrapperSelectModel) super.setPageSize(size);
    }

    public CampaignsWrapperSelectModel()
    {
        super(new String[]{});
    }

    @Override
    public ListResult findValues(final String filter)
    {
        List<CampaignsWrapper> resultList = getCampaigns(filter, 0, null);
        return new ListResult<>(resultList, resultList.size());
    }

    @Override
    public Object getValue(final Object primaryKey)
    {
        if (primaryKey == null) return null;

        List<CampaignsWrapper> list = getCampaigns(null, 0, Collections.singletonList(primaryKey));
        if (list.isEmpty()) return null;

        return list.get(0);
    }

    @Override
    public List getValues(final Set primaryKeys)
    {
        return getCampaigns(null, 0, primaryKeys);
    }

    private List<CampaignsWrapper> getCampaigns(final String filter, final int pageSize, final Collection ids)
    {
        List<EnrEnrollmentCampaign> enrCampList = getCampaigns(EnrEnrollmentCampaign.class, filter, pageSize, ids);
        List<EnrollmentCampaign> oldCampList = getCampaigns(EnrollmentCampaign.class, filter, pageSize, ids);

        List<CampaignsWrapper> resultList = new ArrayList<>();
        enrCampList.forEach(campaign -> resultList.add(new CampaignsWrapper(campaign)));
        oldCampList.forEach(campaign -> resultList.add(new CampaignsWrapper(campaign)));

        resultList.sort((o1, o2) -> Integer.compare(o2.getEducationYear(), o1.getEducationYear()));

        return resultList;
    }

    private <T extends EntityBase & ITitled> List<T> getCampaigns(
            Class<T> campaignClass, final String filter, final int pageSize, final Collection ids
    )
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(campaignClass, "cmp")
                .column(property("cmp"))
                .order(property("cmp.title"));

        if (ids != null && !ids.isEmpty())
            dql.where(in("cmp.id", ids));

        if (!StringUtils.isEmpty(filter))
            dql.where(likeUpper(property("cmp.title"), value(CoreStringUtils.escapeLike(filter, true))));


        List<T> list = DataAccessServices.dao().getList(dql);

        if (pageSize > 0 && pageSize > list.size())
            return list.subList(0, pageSize);
        else
            return list;
    }

}