package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь учебного плана с направлением подготовки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuEduPlanToEduLVLHSRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation";
    public static final String ENTITY_NAME = "narfuEduPlanToEduLVLHSRelation";
    public static final int VERSION_HASH = 451336785;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PLAN = "eduPlan";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";

    private EppEduPlan _eduPlan;     // Учебный план
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Учебный план. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppEduPlan getEduPlan()
    {
        return _eduPlan;
    }

    /**
     * @param eduPlan Учебный план. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduPlan(EppEduPlan eduPlan)
    {
        dirty(_eduPlan, eduPlan);
        _eduPlan = eduPlan;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки. Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NarfuEduPlanToEduLVLHSRelationGen)
        {
            setEduPlan(((NarfuEduPlanToEduLVLHSRelation)another).getEduPlan());
            setEducationLevelsHighSchool(((NarfuEduPlanToEduLVLHSRelation)another).getEducationLevelsHighSchool());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuEduPlanToEduLVLHSRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuEduPlanToEduLVLHSRelation.class;
        }

        public T newInstance()
        {
            return (T) new NarfuEduPlanToEduLVLHSRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduPlan":
                    return obj.getEduPlan();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduPlan":
                    obj.setEduPlan((EppEduPlan) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduPlan":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduPlan":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduPlan":
                    return EppEduPlan.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuEduPlanToEduLVLHSRelation> _dslPath = new Path<NarfuEduPlanToEduLVLHSRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuEduPlanToEduLVLHSRelation");
    }
            

    /**
     * @return Учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation#getEduPlan()
     */
    public static EppEduPlan.Path<EppEduPlan> eduPlan()
    {
        return _dslPath.eduPlan();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    public static class Path<E extends NarfuEduPlanToEduLVLHSRelation> extends EntityPath<E>
    {
        private EppEduPlan.Path<EppEduPlan> _eduPlan;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation#getEduPlan()
     */
        public EppEduPlan.Path<EppEduPlan> eduPlan()
        {
            if(_eduPlan == null )
                _eduPlan = new EppEduPlan.Path<EppEduPlan>(L_EDU_PLAN, this);
            return _eduPlan;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

        public Class getEntityClass()
        {
            return NarfuEduPlanToEduLVLHSRelation.class;
        }

        public String getEntityName()
        {
            return "narfuEduPlanToEduLVLHSRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
