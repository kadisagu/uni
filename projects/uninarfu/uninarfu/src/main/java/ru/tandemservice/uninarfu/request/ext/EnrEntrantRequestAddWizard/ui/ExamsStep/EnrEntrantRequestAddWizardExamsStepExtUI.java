/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequestAddWizard.ui.ExamsStep;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ExamsStep.EnrEntrantRequestAddWizardExamsStepUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.Wizard.IWizardStep;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.NarfuEntrantManager;
import ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation;
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
public class EnrEntrantRequestAddWizardExamsStepExtUI extends UIAddon implements IWizardStep
{
    public EnrEntrantRequestAddWizardExamsStepExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private List<NarfuSpecConditionsEntTests> _specConditionsList;
    private EnrEntrantRequestAddWizardExamsStepUI _presenter;

    public List<NarfuSpecConditionsEntTests> getSpecConditionsList()
    {
        return _specConditionsList;
    }

    public void setSpecConditionsList(List<NarfuSpecConditionsEntTests> specConditionsList)
    {
        _specConditionsList = specConditionsList;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = (EnrEntrantRequestAddWizardExamsStepUI)getPresenter();
        _specConditionsList = NarfuEntrantManager.instance().dao().getPropertiesList(EntrantSpecConditionRelation.class, EntrantSpecConditionRelation.enrEntrant(),
                                                                                     _presenter.getEntrant(), false, EntrantSpecConditionRelation.specCondition());
    }


    @Override
    public void saveStepData()
    {
        NarfuEntrantManager.instance().dao().deleteSpecConditionForEntrant(_presenter.getEntrant());
        NarfuEntrantManager.instance().dao().saveOrUpdateSpecConditions(_presenter.getEntrant(), getSpecConditionsList());
    }
}