/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.logic.IPersonEduDocumentDao;
import ru.tandemservice.uninarfu.base.ext.PersonEduDocument.logic.PersonEduDocumentDao;

/**
 * @author DMITRY KNYAZEV
 * @since 06.04.2015
 */

@Configuration
public class PersonEduDocumentManagerExt extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public IPersonEduDocumentDao dao()
    {
        return new PersonEduDocumentDao();
    }
}
