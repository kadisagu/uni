package ru.tandemservice.uninarfu.component.student.StudentPersonCard;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.WordUtils;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.narfu.entity.catalog.codes.OrderSettingsCodes;
import ru.tandemservice.narfu.utils.NarfuEntrantUtil;
import ru.tandemservice.ordertypermc.dao.IOrderTypeDAO;
import ru.tandemservice.ordertypermc.dao.OrderTypeDAO;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniplanrmc.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContent;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class StudentPersonCardPrintFactory extends ru.tandemservice.uni.component.student.StudentPersonCard.StudentPersonCardPrintFactory
{

    private static final Set<String> STRUCTURE_EDUCATION_LEVELS_CODES = Collections.unmodifiableSet(
            new LinkedHashSet<>(Arrays.asList(new String[]{
                    StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_PROFILE,
                    StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_PROFILE,
                    StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_SPECIALIZATION,
                    StructureEducationLevelsCodes.HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY_SPECIALIZATION,
                    StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_PROFILE,
                    StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_PROFILE,
                    StructureEducationLevelsCodes.HIGH_GOS3_GROUP_SPECIALIZATION
            })));

    public static final String[] NUMBER_STR = {"первый", "второй", "третий", "четвертый", "пятый", "шестой", "седьмой", "восьмой", "девятый", "десятый", "одиннадцатый", "двенадцатый"};
    public static final RtfString DASH = new RtfString().append("\\ul0-", true);

    private Session _session;
    private Map<EppStudentWpeCAction, WPERow> _wpeRowTreeMap; //коллексция актуальных элементов реестра с оценками и нагрузкой

    @Override
    @SuppressWarnings("deprecation")
    public void initPrintData(Student student, List<PersonEduInstitution> personEduInstitutionList, List<PersonNextOfKin> personNextOfKinList, Session session)
    {
        super.initPrintData(student, personEduInstitutionList, personNextOfKinList, session);
        _session = session;
        _wpeRowTreeMap = getWPERows(student);
    }

    @Override
    protected void modifyByStudent(RtfDocument document)
    {
        RtfInjectModifier textModifier = textModify(new RtfInjectModifier());
        RtfTableModifier tableModifier = new RtfTableModifier();

        Student student = super.getData().getStudent();

        tableModifier
                .put("educationType", super.getData().getPersonEducationData())
                .put("parentType", super.getData().getRelativesData());

        OrderSettings setting = IUniBaseDao.instance.get().getByCode(OrderSettings.class, OrderSettingsCodes.EDU_CARD);
        tableModifier.put("T_ord", getOrders(student, setting));

        fillDisciplesTables(textModifier, tableModifier);
        fillPracticesNAttestationsTables(tableModifier);
        fillEnrollmentData(_session, textModifier, student);
        fillDiploma(textModifier);

        textModifier.put("BD", student.getCompensationType().getShortTitle() == null ? DASH : (new RtfString()).append(student.getCompensationType().getShortTitle()));


        textModifier.modify(document);
        tableModifier.modify(document);
    }

    protected RtfInjectModifier textModify(RtfInjectModifier modifier)
    {
        Student student = super.getData().getStudent();

        EducationLevelsHighSchool educationLevelsHighSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
        OrgUnit formOrgUnit = student.getEducationOrgUnit().getFormativeOrgUnit();

        List<PersonForeignLanguage> langList = UniDaoFacade.getCoreDao().getList(PersonForeignLanguage.class, PersonForeignLanguage.L_PERSON, student.getPerson());
        String strLang = langList.stream().map(lang -> lang.getLanguage().getTitle()).collect(Collectors.joining(", ", "", ""));

        modifier.put("formativeOrgUnit", WordUtils.capitalize(formOrgUnit.getPrintTitle(), new char[]{'.'}))
                .put("groupName", getProperty(student, Student.group().title().s()))
                .put("Num", student.getBookNumber() == null ? DASH : (new RtfString()).append(student.getBookNumber()))
                .put("FormO", student.getEducationOrgUnit().getDevelopForm().getTitle() == null ? DASH
                        : (new RtfString()).append(student.getEducationOrgUnit().getDevelopForm().getTitle()))
                .put("NumZ", super.getData().getStudent().getBookNumber() == null ? DASH : (new RtfString()).append(student.getBookNumber()))
                .put("lastName", getProperty(student, Student.person().identityCard().lastName().s()))
                .put("firstName", getProperty(student, Student.person().identityCard().firstName().s()))
                .put("middleName", getProperty(student, Student.person().identityCard().middleName().s()))
                .put("educationOrgUnit", getProperty(student, Student.educationOrgUnit().title().s()))
                .put("sex", getProperty(student, Student.person().identityCard().sex().shortTitle().s()))
                .put("citizenship", getProperty(student, Student.person().identityCard().citizenship().title().s()))
                .put("nationality", getProperty(student, Student.person().identityCard().nationality().title().s()))

                .put("addressTitle", getProperty(student, Student.person().address().titleWithFlat().s()))
                .put("homePhone", getProperty(student, Student.person().contactData().phoneFact().s()))
                .put("mobilePhone", getProperty(student, Student.person().contactData().phoneMobile().s()))
                .put("fax", "")//getProperty(student, "person.contactData.fax"))
                .put("email", getProperty(student, Student.person().contactData().email().s()))
                .put("icq", "")// getProperty(student, "person.contactData.icq"))
                .put("plW", student.getPerson().getWorkPlace() == null ? DASH
                        : (new RtfString()).append(student.getPerson().getWorkPlace() + ", "))
                .put("dolW", student.getPerson().getWorkPlacePosition() == null ? DASH
                        : (new RtfString()).append(student.getPerson().getWorkPlacePosition()))
                .put("lang", strLang.equals("") ? DASH : (new RtfString()).append(strLang));


        if (educationLevelsHighSchool.getPrintTitle() == null)
            modifier.put("Napr", DASH);
        else if (STRUCTURE_EDUCATION_LEVELS_CODES.contains(educationLevelsHighSchool.getEducationLevel().getLevelType().getCode()))
            modifier.put("Napr", educationLevelsHighSchool.getEducationLevel().getParentLevel().getDisplayableTitle());
        else modifier.put("Napr", educationLevelsHighSchool.getPrintTitle());

        Date birthDate = student.getPerson().getIdentityCard().getBirthDate();
        modifier.put("birthDate", birthDate == null ? DASH : (new RtfString()).append(DateFormatter.DEFAULT_DATE_FORMATTER.format(birthDate)));
        String birthPlace = getProperty(student, Student.person().identityCard().birthPlace().s());
        if (birthPlace == null || birthPlace.length() == 0)
            modifier.put("birthPlace", DASH);
        else modifier.put("birthPlace", birthPlace);

        List<PersonBenefit> benefitList = UniDaoFacade.getCoreDao().getList(PersonBenefit.class, PersonBenefit.L_PERSON, student.getPerson());
        String benefits = benefitList.stream().map(benefit -> benefit.getBenefit().getTitle()).collect(Collectors.joining(", ", "", ""));
        modifier.put("lgot", benefits.length() == 0 ? DASH : (new RtfString()).append(benefits));

        String familyStatus = getProperty(student, Student.person().familyStatus().title().s());
        modifier.put("maritalStatus", familyStatus.length() == 0 ? DASH : (new RtfString()).append(familyStatus));

        PersonEduDocument eduDocument = student.getEduDocument();
        String averageEduDocsMarksStr = "____";
        if (eduDocument == null)
        {
            eduDocument = new DQLSelectBuilder()
                    .fromEntity(PersonEduDocument.class, "ped")
                    .column(property("ped"))
                    .where(eq(property("ped", PersonEduDocument.person()), value(student.getPerson())))
                    .order(property("ped", PersonEduDocument.issuanceDate()), OrderDirection.desc)
                    .top(1)
                    .createStatement(_session).uniqueResult();
        }
        if (eduDocument != null)
        {
            modifier.put("scholl", eduDocument.getEduOrganisationWithAddress())
                    .put("dateS", String.valueOf(eduDocument.getYearEnd()));
            if (eduDocument.getAvgMarkAsDouble() != null)
                averageEduDocsMarksStr = String.valueOf(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(eduDocument.getAvgMarkAsDouble()));
        }
        else
            modifier.put("scholl", DASH).put("dateS", "");
        modifier.put("averageMarks", averageEduDocsMarksStr);

        List<PersonNextOfKin> nextOfKinList = UniDaoFacade.getCoreDao().getList(PersonNextOfKin.class, "person", student.getPerson());
        String parent = nextOfKinList.stream()
                .map(kin -> kin.getFullFio()
                        + " "
                        + (kin.getEmploymentPlace() == null ? "" : kin.getEmploymentPlace())
                        + ", " +
                        (kin.getPost() == null ? "" : kin.getPost())
                        + ", "
                        + (kin.getPhones() == null ? "" : kin.getPhones())
                )
                .collect(Collectors.joining(", ", "", ""));
        if (parent.length() == 0)
            modifier.put("parent", DASH);
        else
            modifier.put("parent", parent);


        if (student.getPerson().getContactData().getPhoneMobile() == null || student.getPerson().getContactData().getPhoneMobile().length() == 0)
            modifier.put("telKon", DASH);
        else
            modifier.put("telKon", student.getPerson().getContactData().getPhoneMobile());

        EmployeePost post = (EmployeePost) student.getEducationOrgUnit().getFormativeOrgUnit().getHead();
        String dirName = post == null ? "" : post.getEmployee().getPerson().getFullFio();
        modifier.put("DirName", dirName);
        String dirPost = "Директор";
        switch (student.getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getCode())
        {
            case (OrgUnitTypeCodes.COLLEGE):
                dirPost += " колледжа";
                break;
            case (OrgUnitTypeCodes.INSTITUTE):
                dirPost += " института";
                break;
            case (OrgUnitTypeCodes.BRANCH):
                dirPost += " филиала";
                break;
        }
        modifier.put("DirPost", dirPost);

        AddressBase address = student.getPerson().getAddressRegistration();
        if (address != null && address.getTitleWithFlat() != null)
            modifier.put("addressRegistration", address.getTitleWithFlat());
        else
            modifier.put("addressRegistration", DASH);


        return modifier;
    }

    private void fillDisciplesTables(RtfInjectModifier textModifier, RtfTableModifier tableModifier)
    {
        //по шаблону 2 таблицы. соответственно формируем 2 списка строк
        Map<EppStudentWpeCAction, String[]> allData = getElementsMarksLoad(_wpeRowTreeMap);
        List<String[]> disTable1 = new ArrayList<>();
        List<String[]> disTable2 = new ArrayList<>();
        TreeMap<String, List<String[]>> map = new TreeMap<>((o1, o2) -> ComparisonChain.start().compare(o1.length(), o2.length()).compare(o1, o2).result());
        map.put("1,1", new ArrayList<>());
        map.put("1,2", new ArrayList<>());
        map.put("2,3", new ArrayList<>());
        map.put("2,4", new ArrayList<>());
        map.put("3,5", new ArrayList<>());
        map.put("3,6", new ArrayList<>());
        map.put("4,7", new ArrayList<>());
        map.put("4,8", new ArrayList<>());
        map.put("5,9", new ArrayList<>());
        map.put("5,10", new ArrayList<>());
        map.put("6,11", new ArrayList<>());
        map.put("6,12", new ArrayList<>());
        allData.entrySet().stream()
                .filter(entry -> entry.getValue() != null && entry.getKey().getStudentWpe().getRegistryElementPart().getRegistryElement() instanceof EppRegistryDiscipline)
                .forEach(entry -> {
                             EppStudentWpeCAction wpe = entry.getKey();
                             String[] row = entry.getValue();
                             String key = wpe.getStudentWpe().getCourse().getTitle() + "," + wpe.getStudentWpe().getTerm().getTitle();
                             map.get(key).add(row);
                         }
                );

        map.keySet().forEach(
                key -> {
                    List<String[]> list = map.get(key);
                    int course = Integer.parseInt(key.split(",")[0]);
                    int term = Integer.parseInt(key.split(",")[1]);
                    while (list.size() < 12)
                        list.add(new String[]{NUMBER_STR[course - 1], NUMBER_STR[term - 1], "", "", "", "", "", "", ""});
                    if (course >= 4)
                        disTable2.addAll(list);
                    else
                        disTable1.addAll(list);
                }
        );

        //Суммируем оценки по элементам реестра
        int[] marksCounts = getMarksCounts(_wpeRowTreeMap);
        textModifier
                .put("totalMarks", String.valueOf(marksCounts[0] + marksCounts[1] + marksCounts[2] + marksCounts[3]))
                .put("otl", String.valueOf(marksCounts[0]))
                .put("hor", String.valueOf(marksCounts[1]))
                .put("udv", String.valueOf(marksCounts[2]));

        tableModifier
                .put("T1", disTable1.toArray(new String[0][]))
                .put("T1", new RtfRowIntercepterBaseExt(disTable1))
                .put("T2", disTable2.toArray(new String[0][]))
                .put("T2", new RtfRowIntercepterBaseExt(disTable2));
    }

    private void fillPracticesNAttestationsTables(RtfTableModifier tableModifier)
    {
        DQLSelectBuilder practiceDql = new DQLSelectBuilder()
                .fromEntity(StudentPracticeData.class, "s")
                .joinEntity("s", DQLJoinType.inner, ListOrdListRepresent.class, "r",
                            eq(property(StudentPracticeData.listRepresent().fromAlias("s")),
                               property(ListOrdListRepresent.representation().fromAlias("r"))))
                .where(eq(property(StudentPracticeData.student().fromAlias("s")), value(getData().getStudent())))
                .where(eq(property(ListOrdListRepresent.order().state().code().fromAlias("r")), value("5")))
                .column("s");
        List<StudentPracticeData> dataList = practiceDql.createStatement(_session).list();

        Map<EppRegistryElement, List<StudentPracticeData>> practiceToDataMap = dataList.stream()
                .filter(data -> (data.getListRepresent() instanceof IStudentPractice
                        && ((IStudentPractice) data.getListRepresent()).getRegistryElementRow() != null))
                .collect(Collectors.groupingBy(
                        data -> {
                            IStudentPractice practiceRepresent = (IStudentPractice) data.getListRepresent();
                            EppWorkPlanRegistryElementRow elementRow = (EppWorkPlanRegistryElementRow) practiceRepresent.getRegistryElementRow();
                            return elementRow.getRegistryElementPart().getRegistryElement();
                        },
                        Collectors.mapping(data -> data, Collectors.toList())));

        final int[] practiceCounter = {1};
        String[][] practices = _wpeRowTreeMap.values().stream()
                .filter(row -> row.part.getRegistryElement() instanceof EppRegistryPractice)
                .map(row -> {
                    ArrayList<String[]> strings = new ArrayList<>();
                    List<StudentPracticeData> data = practiceToDataMap.get(row.part.getRegistryElement());
                    if (CollectionUtils.isEmpty(data))
                    {
                        strings.add(getPracticeStrRow(practiceCounter[0]++, row, null));
                    }
                    else
                    {
                        data.forEach(d -> strings.add(getPracticeStrRow(practiceCounter[0]++, row, d)));
                    }
                    return strings;
                })
                .flatMap(Collection::stream)
                .toArray(String[][]::new);
        if (practices.length == 0)
            practices = new String[][]{{"", "", "", "", ""}};

        //Только госэкзамены
        final int[] attestationCounter = {1};
        String[][] attestations = _wpeRowTreeMap.values().stream()
                .filter(row -> row.part.getRegistryElement() instanceof EppRegistryAttestation
                        && row.part.getRegistryElement().getParent() != null
                        && row.part.getRegistryElement().getParent().getCode().equals(EppRegistryStructureCodes.REGISTRY_ATTESTATION_EXAM))
                .map(row -> {
                    String[] str = new String[5];
                    str[0] = String.valueOf(attestationCounter[0]++);
                    str[1] = row.part.getRegistryElement().getTitle();
                    str[2] = "";
                    str[3] = row.mark == null ? "" : row.mark.getCachedMarkValue().getPrintTitle();
                    return str;
                })
                .toArray(String[][]::new);
        if (attestations.length == 0)
            attestations = new String[][]{{"", "", "", ""}};

        tableModifier.put("T_pr", practices);
        tableModifier.put("T_att", attestations);
    }

    public static String[] getPracticeStrRow(int index, WPERow row, StudentPracticeData datum)
    {
        String[] str = new String[5];
        str[0] = String.valueOf(index);
        str[1] = row.part.getRegistryElement().getTitle();
        str[2] = "";
        str[3] = "";
        if (datum != null)
        {

            if (datum.getPracticeBase() != null)
                str[2] = datum.getPracticeBase().getTitle();
            if (datum.getListRepresent() instanceof IStudentPractice)
                str[3] = DateFormatter.DEFAULT_DATE_FORMATTER.format(((IStudentPractice) datum.getListRepresent()).getDateBeginningPractice())
                        + "-"
                        + DateFormatter.DEFAULT_DATE_FORMATTER.format(((IStudentPractice) datum.getListRepresent()).getDateEndOfPractice());
        }
        str[4] = row.mark == null ? "" : row.mark.getCachedMarkValue().getPrintTitle();
        return str;
    }

    private void fillDiploma(RtfInjectModifier modifier)
    {
        Student student = getData().getStudent();

        String theme = student.getFinalQualifyingWorkTheme();
        String mark = null;
        if (theme == null)
            theme = "______________________________________________________________________________";
        else
        {
            mark = _wpeRowTreeMap.values().stream()
                    .filter(row -> row.part.getRegistryElement() instanceof EppRegistryAttestation
                            && row.mark != null
                            && row.part.getRegistryElement().getParent() != null
                            && row.part.getRegistryElement().getParent().getCode().equals(EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA))
                    .map(row -> row.mark == null ? "" : row.mark.getCachedMarkValue().getPrintTitle())
                    .collect(Collectors.joining(", ", "", ""));
        }
        if (mark == null || mark.length() == 0) mark = "_________________";

        DiplomaIssuance di = new DQLSelectBuilder()
                .fromEntity(DiplomaIssuance.class, "di")
                .column(property("di"))
                .where(eq(property("di", DiplomaIssuance.diplomaObject().student()), value(student)))
                .order(property("di", DiplomaIssuance.issuanceDate()), OrderDirection.desc)
                .top(1)
                .createStatement(_session).uniqueResult();

        String protocolDate = "________________";
        String protocolNumber = "_____";
        String dipQualif = "_________________________________________________";
        String dipType = "_________________________";
        String dipSeria = "_____________";
        String dipNumber = "_____________";
        String dipDate = "__________________";
        String dipRegNumber = "________________";
        if (di != null)
        {
            dipRegNumber = di.getRegistrationNumber();
            if (di.getBlankNumber() != null)
                dipNumber = di.getBlankNumber();
            if (di.getBlankSeria() != null)
                dipSeria = di.getBlankSeria();
            dipDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(di.getIssuanceDate());

            DiplomaObject dio = di.getDiplomaObject();
            if (dio.getStateCommissionDate() != null)
                protocolDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(dio.getStateCommissionDate());
            if (dio.getStateCommissionProtocolNumber() != null)
                protocolNumber = dio.getStateCommissionProtocolNumber();

            DiplomaContent dic = dio.getContent();
            dipType = dic.getType().getTitle();

            EduProgramQualification qualification = student.getEducationOrgUnit().getEducationLevelHighSchool().getAssignedQualification();
            if (qualification != null)
                dipQualif = qualification.getTitle();
        }

        modifier.put("DiplomaTheme", theme)
                .put("DiplomaMark", mark)
                .put("protocolDate", protocolDate)
                .put("protocolNumber", protocolNumber)
                .put("dipQualif", dipQualif.toLowerCase())
                .put("dipType", dipType.toLowerCase())
                .put("dipSeria", dipSeria)
                .put("dipNumber", dipNumber)
                .put("dipDate", dipDate)
                .put("dipRegNumber", dipRegNumber);
    }

    public void fillEnrollmentData(Session session, RtfInjectModifier modifier, Student student)
    {
        String enrollmentOrder = "";
        String specialPurpose = "нет";
        String enrollmentTransfer = "";
        Map<String, Double> marks = null;

        //проверяем есть ли у студента приказ о зачислении в порядке перевода
        String dro_alias = "dro";
        String drs_alias = "drs";
        DQLSelectBuilder dorBuilder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, dro_alias)
                .column(dro_alias)
                .joinEntity(dro_alias, DQLJoinType.left, DocRepresentStudentBase.class, drs_alias,
                            eq(property(drs_alias, DocRepresentStudentBase.representation()),
                               property(dro_alias, DocOrdRepresent.representation())))
                .where(eq(property(drs_alias, DocRepresentStudentBase.student()), value(student)))
                .where(eq(property(dro_alias, DocOrdRepresent.order().state().code()),
                          value(MovestudentOrderStatesCodes.CODE_5)))//приказ проведен
                .where(eq(property(dro_alias, DocOrdRepresent.representation().type().code()),
                          value(RepresentationTypeCodes.ENROLLMENT_TRANSFER)))//представление о зачислении в порядке перевода
                .order(property(dro_alias, DocOrdRepresent.order().commitDate()), OrderDirection.desc)
                .top(1);

        DocOrdRepresent docOrdRepresent = dorBuilder.createStatement(session).uniqueResult();
        if (docOrdRepresent != null)
        {
            String commitDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(docOrdRepresent.getOrder().getCommitDate());
            String number = docOrdRepresent.getOrder().getNumber();
            enrollmentOrder = commitDate + " №" + number;

            enrollmentTransfer = ((RepresentEnrollmentTransfer) docOrdRepresent.getRepresentation())
                    .getFormativeOrgUnitOldText() + ", приказ номер " + number + ", от " + commitDate;
        }
        modifier.put("enrollmentTransfer", enrollmentTransfer);


        // Старый Абитуриент
        List<String> enrollmentCodes = ImmutableList.of(StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER, StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_2_MODULAR_ORDER,
														StudentExtractTypeCodes.EDU_ENROLLMENT_AS_TRANSFER_MODULAR_ORDER);
        DQLSelectBuilder olBuilder = new DQLSelectBuilder()
                .fromEntity(OrderList.class, "ol")
                .where(eq(property(OrderList.student().fromAlias("ol")), value(student)))
                .where(in(property(OrderList.type().code().fromAlias("ol")), enrollmentCodes))
                .order(property(OrderList.orderDate().fromAlias("ol")), OrderDirection.desc)
                .top(1);
        OrderList orderList = olBuilder.createStatement(_session).uniqueResult();
        if (orderList != null)
            enrollmentOrder = DateFormatter.DEFAULT_DATE_FORMATTER.format(orderList.getOrderDate()) + " №" + orderList.getOrderNumber();

        RequestedEnrollmentDirection direction = NarfuEntrantUtil.getRequestedEnrollmentDirectionByStudent(student);
        if (direction != null)
        {
            if (direction.isTargetAdmission())
                specialPurpose = direction.getTargetAdmissionKind().getTitle();

            List<ChosenEntranceDiscipline> disciplineList = new DQLSelectBuilder()
                    .fromEntity(ChosenEntranceDiscipline.class, "ced")
                    .column(property("ced"))
                    .where(eq(property("ced", ChosenEntranceDiscipline.chosenEnrollmentDirection()), value(direction)))
                    .createStatement(session).list();
            marks = disciplineList.stream().collect(Collectors.toMap(ChosenEntranceDiscipline::getTitle, ChosenEntranceDiscipline::getFinalMark));
        }


        // Новый Абитуриент (2014)
        EnrEnrollmentExtract extract = DataAccessServices.dao().get(EnrEnrollmentExtract.class, EnrEnrollmentExtract.student().id(), student.getId());
        if (extract != null)
        {
            EnrAbstractOrder order = extract.getParagraph().getOrder();
            String commitDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate());
            enrollmentOrder = commitDate + " №" + order.getNumber() + " " + extract.getNumber() + "/" + extract.getParagraph().getNumber();

            EnrRequestedCompetition requestedCompetition = extract.getRequestedCompetition();
            if (requestedCompetition instanceof EnrRequestedCompetitionTA)
                specialPurpose = ((EnrRequestedCompetitionTA) requestedCompetition).getTargetAdmissionKind().getTitle();

            marks = DataAccessServices.dao()
                    .getList(EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition().id(), requestedCompetition.getId())
                    .stream()
                    .filter(exam -> exam.getMarkAsDouble() != null)
                    .collect(Collectors.toMap(exam -> exam.getDiscipline().getTitle(), EnrChosenEntranceExam::getMarkAsDouble));
        }


        //Из Данных о последних приказах
        OrderData data = DataAccessServices.dao().get(OrderData.class, OrderData.student(), student);
        if ((data != null)
                && (data.getEduEnrollmentOrderDate() != null
                && data.getEduEnrollmentOrderEnrDate() != null
                && data.getEduEnrollmentOrderNumber() != null))
            enrollmentOrder = DateFormatter.DEFAULT_DATE_FORMATTER.format(data.getEduEnrollmentOrderDate()) + " №" + data.getEduEnrollmentOrderNumber();


        modifier.put("enrollmentOrder", enrollmentOrder);
        modifier.put("specialPurpose", specialPurpose);
        fillEnrollmentMarks(modifier, marks);

    }

    private static void fillEnrollmentMarks(RtfInjectModifier modifier, Map<String, Double> marks)
    {
        String marksStr = "____________________________________________________________";
        String summMarkStr = "______";

        if (marks != null && !marks.isEmpty())
        {
            summMarkStr = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(marks.values().stream().mapToDouble(mark -> mark).sum());
            marksStr = marks.entrySet().stream()
                    .map(entry -> entry.getKey() + " - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(entry.getValue()))
                    .collect(Collectors.joining(", "));
        }

        modifier.put("entrantMarks", marksStr);
        modifier.put("summMarks", summMarkStr);
    }

    /**
     * Количество оценок
     *
     * @param rows коллексция элементов реестра
     * @return {отл, хор, удов, неуд}
     */
    private int[] getMarksCounts(Map<EppStudentWpeCAction, WPERow> rows)
    {
        int[] numbers = new int[]{0, 0, 0, 0};
        rows.values().stream()
                .filter(row -> row.mark != null)
                .forEach(row -> {
                             switch (row.mark.getValueItem().getCode())
                             {
                                 case SessionMarkGradeValueCatalogItemCodes.OTLICHNO:
                                     numbers[0]++;
                                     break;
                                 case SessionMarkGradeValueCatalogItemCodes.HOROSHO:
                                     numbers[1]++;
                                     break;
                                 case SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO:
                                     numbers[2]++;
                                     break;
                                 case SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO:
                                     numbers[3]++;
                                     break;
                             }
                         }
                );

        return numbers;
    }

    public String[][] getOrders(Student student, OrderSettings setting)
    {
        Map<Date, List<String[]>> map = new TreeMap<>();
        IOrderTypeDAO idao = OrderTypeDAO.instance();
        List<String> orderCodes = idao.getOrderTypeCodesIn(setting);
        List<String> codesNotIn = idao.getOrderTypeCodesNotIn(setting);
        orderCodes.removeAll(codesNotIn);

        //Приказы списком
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(OrderList.class, "ol")
                .where(eq(property(OrderList.student().fromAlias("ol")), value(student)))
                .where(in(property(OrderList.type().code().fromAlias("ol")), orderCodes));
        List<OrderList> orderList = dql.createStatement(_session).list();
        for (OrderList order : orderList)
        {
            Date orderDate = order.getOrderDate();
            List<String[]> list = map.computeIfAbsent(order.getOrderDate(), k -> new ArrayList<>());
            list.add(getOrderRow(order.getOrderNumber(),
                                 orderDate,
                                 order.getType().getTitle(),
                                 order.getOrderDateStart() != null ? order.getOrderDateStart() : orderDate,
                                 order.getOrderDateStop()));
        }
        //Индивидуальные преставления
        DQLSelectBuilder dqlRep = new DQLSelectBuilder()
                .fromEntity(DocRepresentStudentBase.class, "r")
                .column(property(DocRepresentStudentBase.representation().fromAlias("r")))
                .where(eq(property(DocRepresentStudentBase.student().fromAlias("r")), value(student)))
                .where(eq(property(DocRepresentStudentBase.representation().committed().fromAlias("r")), value(true)))
                .where(in(property(DocRepresentStudentBase.representation().type().code().fromAlias("r")), orderCodes));
        List<Representation> repList = dqlRep.createStatement(_session).list();
        for (Representation representation : repList)
        {
            Date orderDate = representation.getOrder().getCommitDate();
            List<String[]> list = map.computeIfAbsent(representation.getOrder().getCommitDate(), k -> new ArrayList<>());
            list.add(getOrderRow(representation.getOrder().getNumber(),
                                 orderDate,
                                 representation.getType().getTitle(),
                                 representation.getStartDate() != null ? representation.getStartDate() : orderDate,
                                 null));
        }
        //Списочные представления
        DQLSelectBuilder dqlListRep = new DQLSelectBuilder()
                .fromEntity(RelListRepresentStudents.class, "lr")
                .column(property(RelListRepresentStudents.representation().fromAlias("lr")))
                .where(eq(property(RelListRepresentStudents.student().fromAlias("lr")), value(student)))
                .where(eq(property(RelListRepresentStudents.representation().committed().fromAlias("lr")), value(true)))
                .where(in(property(RelListRepresentStudents.representation().representationType().code().fromAlias("lr")), orderCodes));
        List<ListRepresent> listRep = dqlListRep.createStatement(_session).list();
        for (ListRepresent representation : listRep)
        {
            Date orderDate = representation.getOrder().getCommitDate();
            List<String[]> list = map.computeIfAbsent(orderDate, k -> new ArrayList<>());
            list.add(getOrderRow(representation.getOrder().getNumber(),
                                 orderDate,
                                 representation.getRepresentationType().getTitle(),
                                 representation.getStartDate() != null ? representation.getStartDate() : orderDate,
                                 null));
        }

        return map.values().stream().flatMap(Collection::stream).toArray(String[][]::new);
    }

    private String[] getOrderRow(String number, Date orderDate, String type, Date beginDate, Date endDate)
    {
        String[] str = new String[3];
        str[0] = "№" + number + ", от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate);
        str[1] = type;
        String begin = beginDate != null ? "c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate) : "";
        String sep = beginDate != null && endDate != null ? " " : "";
        String end = endDate != null ? "по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate) : "";
        str[2] = begin + sep + end;
        return str;
    }

    private TreeMap<EppStudentWpeCAction, String[]> getElementsMarksLoad(Map<EppStudentWpeCAction, WPERow> wpeRows)
    {
        Comparator<EppStudentWpeCAction> wpeComparator = (wpe1, wpe2) -> ComparisonChain.start()
                .compare(wpe1.getStudentWpe().getCourse().getIntValue(), wpe2.getStudentWpe().getCourse().getIntValue())
                .compare(wpe1.getStudentWpe().getTerm().getIntValue(), wpe2.getStudentWpe().getTerm().getIntValue())
                .compare(wpe1.getStudentWpe().getRegistryElementPart().getRegistryElement().getTitle(),
                         wpe2.getStudentWpe().getRegistryElementPart().getRegistryElement().getTitle())
                .compare(wpe1.getId(), wpe2.getId())
                .result();

        return wpeRows.values().stream()
                .collect(Collectors.toMap(
                        WPERow::getKey,
                        WPERow::getStrings,
                        (r1, r2) -> r2,
                        () -> new TreeMap<>(wpeComparator)));
    }

    private Map<EppStudentWpeCAction, WPERow> getWPERows(Student student)
    {
        String wpe_alias = "wpe";
        DQLSelectBuilder wpeDql = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, wpe_alias)
                .column(property(wpe_alias))
                .column(property(wpe_alias, EppStudentWpeCAction.studentWpe().registryElementPart()))
                .where(eqValue(property(wpe_alias, EppStudentWpeCAction.studentWpe().student()), student))
                //--------------------------------------------------------------------------------------------------------
                //.where(isNull(property(wpe_alias, EppStudentWpeCAction.studentWpe().removalDate())));

                //Выводятся только актуальные оценки, для отчисленных студентов выводятся все оценки
                .where(or(isNull(property(wpe_alias, EppStudentWpeCAction.studentWpe().removalDate())), eq(property(wpe_alias, EppStudentWpeCAction.studentWpe().student().status().code()), value("8"))));
        		//--------------------------------------------------------------------------------------------------------
        List<Object[]> wpeRawList = wpeDql.createStatement(_session).list();
        List<CoreCollectionUtils.Pair<EppStudentWpeCAction, EppRegistryElementPart>> wpeList = wpeRawList.stream()
                .map(row -> new CoreCollectionUtils.Pair<>((EppStudentWpeCAction) row[0], (EppRegistryElementPart) row[1]))
                .collect(Collectors.toList());

        List<Long> wpeIds = wpeList.stream().map(wpe -> wpe.getX().getId()).collect(Collectors.toList());
        List<Long> partIds = wpeList.stream().map(wpe -> wpe.getY().getId()).collect(Collectors.toList());


        String mark_alias = "mark";
        DQLSelectBuilder markDql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, mark_alias)
                //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                .joinEntity(mark_alias, DQLJoinType.left, SessionMarkGradeValueCatalogItem.class, "grade", eq(property(SessionMark.id().fromAlias(mark_alias)), property(SessionMarkGradeValueCatalogItem.id().fromAlias("grade"))))
                //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                .column(property(mark_alias, SessionMark.slot().studentWpeCAction().id()))
                .column(property(mark_alias))
                //-----------------------------------------------------------------------------------------------------
                .column(property(SessionMarkGradeValueCatalogItem.priority().fromAlias("grade")))	//приоритет оценки
                //-----------------------------------------------------------------------------------------------------
                .where(in(property(mark_alias, SessionMark.slot().studentWpeCAction().id()), wpeIds));
        List<Object[]> markRawList = markDql.createStatement(_session).list();

        Map<Long, SessionMark> markByWpe = new HashMap<>();
        //-------------------------------------------------------------------------------------------------------------------------
        //markRawList.stream().forEach(raw -> markByWpe.put((Long) raw[0], (SessionMark) raw[1]));
        
        //Если оценок несколько, оставляем оценку с более высоким приоритетом
        markRawList.forEach(raw ->
                            {
                                if (!markByWpe.containsKey((Long) raw[0]))
                                {
                                    markByWpe.put((Long) raw[0], (SessionMark) raw[1]);
                                }
                                else
                                {
                                    SessionMark savedMark = markByWpe.get((Long) raw[0]);

                                    if (savedMark.getCachedMarkValue() != null && raw[2] != null)
                                    {

                                        if (savedMark.getCachedMarkValue().getPriority() < (int) raw[2])
                                        {
                                            markByWpe.put((Long) raw[0], (SessionMark) raw[1]);
                                        }
                                    }
                                }
                            }
        );
        //-------------------------------------------------------------------------------------------------------------------------

        String rel_alias = "rel";
        String aload_alias = "aload";
        DQLSelectBuilder loadDql = new DQLSelectBuilder()
                .fromEntity(EppRegistryElementPartModule.class, rel_alias)
                .column(property(rel_alias, EppRegistryElementPartModule.part().id()))
                .where(in(property(rel_alias, EppRegistryElementPartModule.part().id()), partIds))

                .joinEntity(rel_alias, DQLJoinType.inner, EppRegistryModuleALoad.class, aload_alias,
                            eq(property(rel_alias, EppRegistryElementPartModule.module()), property(aload_alias, EppRegistryModuleALoad.module())))
                .column(DQLFunctions.sum(property(aload_alias, EppRegistryModuleALoad.load())))
                .group(property(rel_alias, EppRegistryElementPartModule.part().id()));

        Map<Long, Long> loadByPart = loadDql.createStatement(_session).<Object[]>list().stream()
                .collect(Collectors.toMap(raw -> (Long) raw[0], raw -> (Long) raw[1]));

        return wpeList.stream()
                .collect(Collectors.toMap(
                        CoreCollectionUtils.Pair::getX,
                        raw -> {
                            EppStudentWpeCAction wpe = raw.getX();
                            EppRegistryElementPart part = raw.getY();
                            SessionMark mark = markByWpe.get(wpe.getId());
                            Long load = loadByPart.get(part.getId());
                            return new WPERow(wpe, part, mark, load);
                        }
                ));
    }

    private static class WPERow
    {
        final EppStudentWpeCAction wpe;
        final EppRegistryElementPart part;
        final SessionMark mark;
        final Long load;

        WPERow(EppStudentWpeCAction wpe, EppRegistryElementPart part, SessionMark mark, Long load)
        {
            this.load = load;
            this.wpe = wpe;
            this.part = part;
            this.mark = mark;
        }

        EppStudentWpeCAction getKey()
        {
            return wpe;
        }

        String[] getStrings()
        {
            String[] str = new String[9];
            str[0] = NUMBER_STR[wpe.getStudentWpe().getCourse().getIntValue() - 1];
            str[1] = NUMBER_STR[wpe.getStudentWpe().getTerm().getIntValue() - 1];
            str[2] = part.getRegistryElement().getTitle();
            str[3] = UniEppUtils.formatLoad(part.getSizeAsDouble(), false) + "/" + UniEppUtils.formatLoad(UniEppUtils.wrap(load), false);
            str[4] = "";
            str[5] = "";
            str[6] = "";
            str[7] = "";
            str[8] = "";
            if (mark != null)
            {
                switch (wpe.getType().getCode())
                {
                    case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM:
                    case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM:
                        str[4] = mark.getValueTitle().toLowerCase();
                        break;
                    case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF:
                    case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF:
                        str[5] = mark.getValueTitle().toLowerCase();
                        break;
                    case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK:
                    case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT:
                        str[6] = mark.getValueTitle().toLowerCase();
                        break;
                    //-----------------------------------------------------------
                    case EppGroupTypeFCACodes.CONTROL_ACTION_CONTROL_WORK:
                        str[7] = mark.getValueTitle().toLowerCase();
                        break;                       
                    //-----------------------------------------------------------
                }
                str[8] = DateFormatter.DEFAULT_DATE_FORMATTER.format(mark.getPerformDate());
            }
            return str;
        }
    }
}