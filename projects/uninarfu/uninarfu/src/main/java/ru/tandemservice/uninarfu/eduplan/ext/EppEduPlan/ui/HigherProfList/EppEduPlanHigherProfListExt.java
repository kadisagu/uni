/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.HigherProfList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList.*;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.NarfuEduPlanManager;


/**
 * @author Ekaterina Zvereva
 * @since 27.03.2015
 */
@Configuration
public class EppEduPlanHigherProfListExt extends BusinessComponentExtensionManager
{

    @Autowired
    private EppEduPlanHigherProfList _eppEduPlaneList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduPlaneList.presenterExtPoint())
                .replaceDataSource(searchListDS("eduPlanDS", _eppEduPlaneList.eduPlanDSColumns()).handler(NarfuEduPlanManager.instance().eduPlanHigherProfListDSHandler()))
                .create();
    }


}