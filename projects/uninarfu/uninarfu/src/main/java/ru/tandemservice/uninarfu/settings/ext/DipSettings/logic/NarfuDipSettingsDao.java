/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.DipSettings.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.settings.bo.DipSettings.logic.DipSettingsDao;

import java.util.Comparator;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.substring;

/**
 * @author Alexey Lopatin
 * @since 28.05.2015
 */
public class NarfuDipSettingsDao extends DipSettingsDao implements INarfuDipSettingsDao
{
    @Override
    public String getDipIssuanceNextRegNumberWithinEduYear(DiplomaObject dipObject)
    {
        final int numberLen = 4;
        final int currentYear = UniBaseUtils.getCurrentYear();

        final OrgUnit ou = dipObject.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        final String divisionCode = ou.getDivisionCode();
        if (null == divisionCode) return null;

        // Блокировка для префикса/постфикса
        NamedSyncInTransactionCheckLocker.register(getSession(), DIPLOMA_REG_NUMBER_LOCK + divisionCode + currentYear);


        if (DipDocumentTypeCodes.EDUCATION_REFERENCE.equals(dipObject.getContent().getType().getCode()))
            return getNumber4Reference(ou, divisionCode, numberLen, currentYear);
        else
            return getNumber4Other(ou, divisionCode, numberLen, currentYear);
    }

    private String getNumber4Reference(OrgUnit ou, String divisionCode, int numberLen, int currentYear)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "di")
                .column(substring(property("di", DiplomaIssuance.registrationNumber()), divisionCode.length() + 3, numberLen))
                .where(like(property("di", DiplomaIssuance.registrationNumber()), value("С" + divisionCode + "-____/" + currentYear)));

        final Integer maxNumber = dql.createStatement(getSession()).<String>list().stream()
                .filter(StringUtils::isNumeric)
                .map(Integer::valueOf)
                .max(Comparator.<Integer>naturalOrder())
                .orElse(0);

        if (maxNumber >= ((int) Math.pow(10, numberLen) - 1))
        {
            ContextLocal.getInfoCollector().add("Для подразделения «" + ou.getTitle() + "» на " + currentYear + " год не осталось свободных регистрационных номеров.");
            return null;
        }

        return "С" + divisionCode + "-" + StringUtils.leftPad(String.valueOf(maxNumber + 1), numberLen, '0') + "/" + currentYear;
    }

    private String getNumber4Other(OrgUnit ou, String divisionCode, int numberLen, int currentYear)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, "di")
                .column(substring(property("di", DiplomaIssuance.registrationNumber()), divisionCode.length() + 2, numberLen))
                .where(like(property("di", DiplomaIssuance.registrationNumber()), value(divisionCode + "-____/" + currentYear)));

        final Integer maxNumber = dql.createStatement(getSession()).<String>list().stream()
                .filter(StringUtils::isNumeric)
                .map(Integer::valueOf)
                .max(Comparator.<Integer>naturalOrder())
                .orElse(0);

        if (maxNumber >= ((int) Math.pow(10, numberLen) - 1))
        {
            ContextLocal.getInfoCollector().add("Для подразделения «" + ou.getTitle() + "» на " + currentYear + " год не осталось свободных регистрационных номеров.");
            return null;
        }

        return divisionCode + "-" + StringUtils.leftPad(String.valueOf(maxNumber + 1), numberLen, '0') + "/" + currentYear;
    }
}
