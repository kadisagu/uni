/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantDailyListAdd.NarfuEnrReportEntrantDailyListAddUI;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantRegistrationJournalAdd.NarfuEnrReportEntrantRegistrationJournalAddUI;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCompetitionListAdd.NarfuEnrReportRequestCompetitionListAddUI;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCountAdd.NarfuEnrReportRequestCountAddUI;

/**
 * @author Andrey Avetisov
 * @since 10.04.2015
 */
public interface INarfuEnrReportDao extends INeedPersistenceSupport
{
    /**
     *  Печать отчета "Количество поданных заявлений (САФУ)"
     * @param model модель формы добавления
     * @return идентификатор отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createRequestCountReport(NarfuEnrReportRequestCountAddUI model);

    /**
     * Печать отчета "Списки поступающих (САФУ)"
     * @param model модель формы добавления
     * @return идентификатор отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createRequestCompetitionListReport(NarfuEnrReportRequestCompetitionListAddUI model);

    /**
     *  Печать отчета "Журнал регистрации абитуриентов (САФУ)"
     * @param model модель формы добавления
     * @return идентификатор отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createEntrantRegistrationJournalReport(NarfuEnrReportEntrantRegistrationJournalAddUI model);

    /**
     *  Печать отчета "Журнал регистрации абитуриентов (САФУ)"
     * @param model модель формы добавления
     * @return идентификатор отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createEntrantDailyListReport(NarfuEnrReportEntrantDailyListAddUI model);
}
