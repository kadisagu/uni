/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubExamTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab.EnrEntrantPubExamTabUI;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit.EnrEnrollmentCampaignAddEdit;

/**
 * @author Ekaterina Zvereva
 * @since 14.08.2015
 */
public class NarfuEntrantPrintExamList extends NamedUIAction
{
    protected NarfuEntrantPrintExamList(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        EnrScriptItem scriptItem = null;

        UIAddon addon = (UIAddon) presenter.getConfig().getAddon("narfuEnrEntrantPubExamTabExt");
        if (addon == null) return;

        if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_INTERNAL_EXAMS.equals(((EnrEntrantPubExamTabUI)presenter).getEntrant().getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET);
        }
        else if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_ALL_EXAMS.equals(((EnrEntrantPubExamTabUI)presenter).getEntrant().getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET_ALL_EXAMS);
        }
        else
        {
            throw new UnsupportedOperationException();
        }

        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, ((EnrEntrantPubExamTabUI) presenter).getEntrant().getId()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, ((EnrEntrantPubExamTabUI) presenter).getEntrant().getId());
    }
}