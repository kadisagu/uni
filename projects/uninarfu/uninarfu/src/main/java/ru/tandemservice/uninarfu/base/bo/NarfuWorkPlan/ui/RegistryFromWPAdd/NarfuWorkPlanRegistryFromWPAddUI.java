/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuWorkPlan.ui.RegistryFromWPAdd;

import org.springframework.util.ClassUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.EppWorkPlanManager;
import ru.tandemservice.uniepp.dao.workplan.*;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.workplan.*;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Andreev
 * @since 29.03.2016
 */
@Input({
        @Bind(key = NarfuWorkPlanRegistryFromWPAddUI.PARAM_HOLDER_ID, binding = "holder.id", required = true),
        @Bind(key = NarfuWorkPlanRegistryFromWPAddUI.PARAM_ELEMENT_TYPE, binding = "elementType", required = true)
})
public class NarfuWorkPlanRegistryFromWPAddUI extends UIPresenter
{
    public static final String PARAM_HOLDER_ID = "holderId";
    public static final String PARAM_ELEMENT_TYPE = "elementType";

    public static final String PARAM_PROGRAM_KIND = "programKind";
    public static final String PARAM_TERM = "term";
    public static final String PARAM_YEAR_EDUCATION_PROCESS = "yearEducationProcess";
    public static final String PARAM_PROGRAM_SUBJECT = "programSubject";
    public static final String PARAM_TARGET_WORK_PLAN = "targetWorkPlanId";
    public static final String PARAM_NUMBER_WORK_PLAN_PARTS = "numberWorkPlanParts";
    public static final String PARAM_SOURCE_WORK_PLAN = "sourceWorkPlan";

    public enum ELEMENT_TYPE
    {
        DISCIPLINE, PRACTICE
    }

    private final EntityHolder<EppWorkPlan> _holder = new EntityHolder<>();
    private ELEMENT_TYPE _elementType;
    private EppWorkPlan _targetWorkPlan;
    private Integer _workPlanPartSize;

    @Override
    public void onComponentRefresh()
    {
        _uiSupport.setPageTitle(getWindowTitle());

        _targetWorkPlan = _holder.refresh(EppWorkPlan.class);

        _uiSettings.set(PARAM_TERM, _targetWorkPlan.getTerm());
        _uiSettings.set(PARAM_YEAR_EDUCATION_PROCESS, _targetWorkPlan.getYear());
        _uiSettings.set(PARAM_PROGRAM_KIND, _targetWorkPlan.getEduPlan().getProgramKind());

        Number cnt = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanPart.class, "parts")
                .where(eq(property("parts", EppWorkPlanPart.workPlan()), value(_targetWorkPlan)))
                .column(property("parts"))
                .createCountStatement(new DQLExecutionContext(getSupport().getSession()))
                .uniqueResult();
        _workPlanPartSize = cnt == null ? 0 : cnt.intValue();
    }

    @Override
    public void onComponentPrepareRender()
    {
        _uiSupport.setPageTitle(getWindowTitle());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case EppWorkPlanManager.PROGRAM_SUBJECT_DS:
                dataSource.put(PARAM_PROGRAM_KIND, _uiSettings.get(PARAM_PROGRAM_KIND));
                break;

            case NarfuWorkPlanRegistryFromWPAdd.WORK_PLAN_DS:
                dataSource.put(PARAM_PROGRAM_KIND, _uiSettings.get(PARAM_PROGRAM_KIND));
                dataSource.put(PARAM_YEAR_EDUCATION_PROCESS, _uiSettings.get(PARAM_YEAR_EDUCATION_PROCESS));
                dataSource.put(PARAM_TERM, _uiSettings.get(PARAM_TERM));
                dataSource.put(PARAM_PROGRAM_SUBJECT, _uiSettings.get(PARAM_PROGRAM_SUBJECT));
                dataSource.put(PARAM_TARGET_WORK_PLAN, _targetWorkPlan);
                dataSource.put(PARAM_NUMBER_WORK_PLAN_PARTS, _workPlanPartSize);
                break;

            case NarfuWorkPlanRegistryFromWPAdd.ELEMENT_LIST_DS:
                dataSource.put(PARAM_SOURCE_WORK_PLAN, _uiSettings.get(PARAM_SOURCE_WORK_PLAN));
                dataSource.put(PARAM_ELEMENT_TYPE, _elementType);
                break;
        }
    }

    public ELEMENT_TYPE getElementType()
    {
        return _elementType;
    }

    public void setElementType(ELEMENT_TYPE elementType)
    {
        _elementType = elementType;
    }

    public EntityHolder<EppWorkPlan> getHolder()
    {
        return _holder;
    }

    public String getWindowTitle()
    {
        switch (_elementType)
        {
            case DISCIPLINE:
                return "Добавление дисциплин из РУП";
            case PRACTICE:
                return "Добавление мероприятий из РУП";
            default:
                return "Добавление элементов реестра из РУП";
        }
    }

    public String getElementListTitle()
    {
        switch (_elementType)
        {
            case DISCIPLINE:
                return "Дисциплины из РУП";
            case PRACTICE:
                return "Мероприятия из РУП";
            default:
                return "Элементы реестра из РУП";
        }
    }


    public void onClickApply()
    {
        final ICommonDAO dao = DataAccessServices.dao();
        final IEppWorkPlanDataDAO workPlanDataDAO = IEppWorkPlanDataDAO.instance.get();

        List<Long> targetRows = dao.getList(
                new DQLSelectBuilder()
                        .fromEntity(EppWorkPlanRegistryElementRow.class, "row")
                        .where(eq(property("row", EppWorkPlanRegistryElementRow.workPlan()), value(_targetWorkPlan)))
                        .column(property("row", EppWorkPlanRegistryElementRow.registryElementPart().id())));

        List<Long> newPartIds = ((BaseSearchListDataSource) getConfig().getDataSource(NarfuWorkPlanRegistryFromWPAdd.ELEMENT_LIST_DS))
                .getOptionColumnSelectedObjects("select")
                .stream()
                .map(e -> ((EppWorkPlanRegistryElementRow) e).getRegistryElementPart().getId())
                .filter(partId -> !targetRows.contains(partId))
                .collect(Collectors.toList());

        EppWorkPlan sourceWorkPlan = _uiSettings.get(PARAM_SOURCE_WORK_PLAN);
        final IEppWorkPlanWrapper sourceWrapper = workPlanDataDAO.getWorkPlanDataMap(Collections.singleton(sourceWorkPlan.getId())).get(sourceWorkPlan.getId());

        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> sourceLoadMap =
                workPlanDataDAO.getRowPartLoadDataMap(sourceWrapper.getRowMap().keySet());

        final Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> loadMap = new HashMap<>(16);

        sourceWrapper.getRowMap().values().stream()
                .filter(wrapper -> newPartIds.contains(wrapper.getRegistryElementPart().getId()))
                .forEach(wrapper -> makeClone(wrapper, sourceLoadMap, loadMap));

        workPlanDataDAO.doUpdateRowPartLoadElementsMap(loadMap);
        workPlanDataDAO.doUpdateRowPartLoadPeriodsMap(loadMap);

        deactivate();
    }

    private void makeClone(IEppWorkPlanRowWrapper wrapper,
                           Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> sourceLoadMap,
                           Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> loadMap)
    {
        try
        {
            final EppWorkPlanRow cloneRow = ((Class<EppWorkPlanRow>) ClassUtils.getUserClass(wrapper.getRow())).newInstance();
            cloneRow.update(wrapper.getRow());
            cloneRow.setWorkPlan(_targetWorkPlan);
            DataAccessServices.dao().save(cloneRow);

            final Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map = sourceLoadMap
                    .get(wrapper.getRow().getId())
                    .entrySet().stream()
                    .filter(entry -> entry.getValue() != null)
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            e0 -> e0.getValue().entrySet().stream()
                                    .collect(Collectors.toMap(Map.Entry::getKey, e1 -> new EppWorkPlanRowPartLoad(cloneRow, e1.getValue())))
                    ));
            if (map != null)
                loadMap.put(cloneRow.getId(), map);
        }
        catch (final Exception e)
        {
            throw CoreExceptionUtils.getRuntimeException(e);
        }
    }
}
