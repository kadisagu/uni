package ru.tandemservice.uninarfu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Способ деления потоков"
 * Имя сущности : eppEduGroupSplitVariant
 * Файл data.xml : uninarfu.data.xml
 */
public interface EppEduGroupSplitVariantCodes
{
    /** Константа кода (code) элемента : По полу (title) */
    String PO_POLU = "bySex";
    /** Константа кода (code) элемента : По иностранному языку (title) */
    String PO_INOSTRANNOMU_YAZYKU = "byForeignLang";
    /** Константа кода (code) элемента : По второму иностранному языку (title) */
    String PO_VTOROMU_INOSTRANNOMU_YAZYKU = "bySecForeignLang";

    Set<String> CODES = ImmutableSet.of(PO_POLU, PO_INOSTRANNOMU_YAZYKU, PO_VTOROMU_INOSTRANNOMU_YAZYKU);
}
