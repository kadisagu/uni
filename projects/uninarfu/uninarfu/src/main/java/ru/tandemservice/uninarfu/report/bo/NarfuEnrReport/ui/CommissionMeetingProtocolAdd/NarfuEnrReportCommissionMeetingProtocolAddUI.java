/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportDateSelector;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportParallelSelector;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.NarfuEnrReportManager;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.logic.NarfuExcludeInOrderSelector;

import java.util.Date;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 27.07.2015
 */
public class NarfuEnrReportCommissionMeetingProtocolAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign enrollmentCampaign;
    private EnrReportDateSelector dateSelector = new EnrReportDateSelector();
    private EnrReportParallelSelector parallelSelector = new EnrReportParallelSelector();
    private NarfuExcludeInOrderSelector excludeInOrderSelector = new NarfuExcludeInOrderSelector();

    private List<EnrEnrollmentCommission> _enrollmentCommissionList;
    private boolean _enrollmentCommissionActive;

    private Date _protocolDate;
    private String _protocolNumber;

    private boolean skipEmptyList;
    private boolean skipEmptyCompetition;
//    private boolean replaceProgramSetTitle;
//    private boolean skipProgramSetTitle;
    private boolean doNotShowCommissionResolution;
    private boolean executiveSecretaryActive;

    private boolean includeTookAwayDocuments;

    public EmployeePost getSecretary()
    {
        return _secretary;
    }

    public void setSecretary(EmployeePost secretary)
    {
        _secretary = secretary;
    }

    public NarfuExcludeInOrderSelector getExcludeInOrderSelector()
    {
        return excludeInOrderSelector;
    }

    public void setExcludeInOrderSelector(NarfuExcludeInOrderSelector excludeInOrderSelector)
    {
        this.excludeInOrderSelector = excludeInOrderSelector;
    }

    public boolean isExecutiveSecretaryActive()
    {
        return executiveSecretaryActive;
    }

    public void setExecutiveSecretaryActive(boolean executiveSecretaryActive)
    {
        this.executiveSecretaryActive = executiveSecretaryActive;
    }

    private EmployeePost _secretary;

    private long reportId;

    @Override
    public void onComponentPrepareRender()
    {
        if (reportId != 0)
        {
            activatePublisher();
            reportId = 0L;
        }
    }

    public void setReportId(long reportId)
    {
        this.reportId = reportId;
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        configUtil(getCompetitionFilterAddon());
    }

    private void validate()
    {
        if (dateSelector.getDateFrom().after(dateSelector.getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    public void onClickApply() {
        validate();
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                setReportId(NarfuEnrReportManager.instance().enrollmentCommissionMeetingProtocolDao().createReport(NarfuEnrReportCommissionMeetingProtocolAddUI.this));
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }

    public void activatePublisher()
    {
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
        deactivate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
    }

    // utils

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, new CommonFilterFormConfig(true, false, true,false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, new CommonFilterFormConfig(true, false, true,false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, new CommonFilterFormConfig(true, false, false,false, false, false))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // getters and setters


//    public boolean isReplaceProgramSetTitle()
//    {
//        return replaceProgramSetTitle;
//    }
//
//    public void setReplaceProgramSetTitle(boolean replaceProgramSetTitle)
//    {
//        this.replaceProgramSetTitle = replaceProgramSetTitle;
//    }

    public boolean isSkipEmptyCompetition()
    {
        return skipEmptyCompetition;
    }

    public void setSkipEmptyCompetition(boolean skipEmptyCompetition)
    {
        this.skipEmptyCompetition = skipEmptyCompetition;
    }

    public boolean isSkipEmptyList()
    {
        return skipEmptyList;
    }

    public void setSkipEmptyList(boolean skipEmptyList)
    {
        this.skipEmptyList = skipEmptyList;
    }

//    public boolean isSkipProgramSetTitle()
//    {
//        return skipProgramSetTitle;
//    }
//
//    public void setSkipProgramSetTitle(boolean skipProgramSetTitle)
//    {
//        this.skipProgramSetTitle = skipProgramSetTitle;
//    }

    public EnrReportDateSelector getDateSelector() {
        return dateSelector;
    }

    public EnrReportParallelSelector getParallelSelector() {
        return parallelSelector;
    }

    public boolean isIncludeTookAwayDocuments() {
        return includeTookAwayDocuments;
    }

    public void setIncludeTookAwayDocuments(boolean includeTookAwayDocuments) {
        this.includeTookAwayDocuments = includeTookAwayDocuments;
    }

    public boolean isDoNotShowCommissionResolution() {
        return doNotShowCommissionResolution;
    }

    public void setDoNotShowCommissionResolution(boolean doNotShowCommissionResolution) {
        this.doNotShowCommissionResolution = doNotShowCommissionResolution;
    }

    public List<EnrEnrollmentCommission> getEnrollmentCommissionList()
    {
        return _enrollmentCommissionList;
    }

    public void setEnrollmentCommissionList(List<EnrEnrollmentCommission> enrollmentCommissionList)
    {
        _enrollmentCommissionList = enrollmentCommissionList;
    }

    public boolean isEnrollmentCommissionActive()
    {
        return _enrollmentCommissionActive;
    }

    public void setEnrollmentCommissionActive(boolean enrollmentCommissionActive)
    {
        _enrollmentCommissionActive = enrollmentCommissionActive;
    }

    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    public void setProtocolDate(Date protocolDate)
    {
        _protocolDate = protocolDate;
    }

    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    public void setProtocolNumber(String protocolNumber)
    {
        _protocolNumber = protocolNumber;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        this.enrollmentCampaign = enrollmentCampaign;
    }
}