/* $Id$ */
package ru.tandemservice.uninarfu.component.catalog.relationDegree.RelationDegreePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;

/**
 * @author Ekaterina Zvereva
 * @since 27.04.2015
 */
public class Model extends DefaultCatalogPubModel<RelationDegree>
{
}