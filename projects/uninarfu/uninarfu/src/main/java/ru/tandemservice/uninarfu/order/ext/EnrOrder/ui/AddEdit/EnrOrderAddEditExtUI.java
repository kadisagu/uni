/* $Id$ */
package ru.tandemservice.uninarfu.order.ext.EnrOrder.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit.EnrOrderAddEditUI;
import ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt;

/**
 * @author Ekaterina Zvereva
 * @since 30.07.2015
 */
public class EnrOrderAddEditExtUI extends UIAddon
{

    private boolean _showProtocol;
    private NarfuEnrOrderExt _orderExt;
    private EnrOrderAddEditUI _presenter;

    public EnrOrderAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        _presenter = getPresenter();
        _orderExt = DataAccessServices.dao().get(NarfuEnrOrderExt.class, NarfuEnrOrderExt.enrOrder(), _presenter.getOrder());
        if (_orderExt == null)
            _orderExt = new NarfuEnrOrderExt();
    }

    public boolean isShowProtocol()
    {
        if (_presenter.getOrder().getType() == null) return false;
        return EnrOrderTypeCodes.ENROLLMENT.equals(_presenter.getOrder().getType().getCode());
    }


    public NarfuEnrOrderExt getOrderExt()
    {
        return _orderExt;
    }

    public void setOrderExt(NarfuEnrOrderExt orderExt)
    {
        _orderExt = orderExt;
    }
}
