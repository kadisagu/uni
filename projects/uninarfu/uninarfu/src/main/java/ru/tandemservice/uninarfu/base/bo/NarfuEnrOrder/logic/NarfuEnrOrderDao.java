/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uninarfu.order.ext.EnrOrder.logic.EnrOrderFilialPartWrapper;
import ru.tandemservice.uninarfu.order.ext.EnrOrder.logic.EnrOrderPartWrapper;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 12.08.2015
 */
public class NarfuEnrOrderDao extends UniBaseDao implements INarfuEnrOrderDao
{
    @Override
    public byte[] printEnrOrderExtracts(List<Long> paragraphIds)
    {

        final IRtfControl pageBreak = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        List<EnrEnrollmentParagraph> paragraphList = getList(EnrEnrollmentParagraph.class, EnrEnrollmentParagraph.id(), paragraphIds);
        if (paragraphList.isEmpty())
            throw new ApplicationException("Не найдены параграфы для печати");

        EnrOrder order = (EnrOrder) paragraphList.get(0).getOrder();
        Map<Long, Integer> paragraphsNumberMap = prepareOrderParagraphs(order);

        // Список выписок приказа. Сортируем по ФИО
        List<EnrEnrollmentExtract> extracts = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e")
                .column(property("e"))
                .where(in(property("e", EnrEnrollmentExtract.paragraph().id()), paragraphIds))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fromAlias("e"), "card")
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME))
                .createStatement(getSession()).list();


        List<IRtfElement> mainElementList = null;
        RtfDocument mainDoc = null;
        RtfDocument extractDoc = null;
        final Iterator<EnrEnrollmentExtract> iterator = extracts.iterator();
        do
        {
            EnrEnrollmentExtract extract = iterator.next();
            String scriptCode = getScriptCode(extract);
            IScriptItem scriptItem = getByCode(EnrScriptItem.class, scriptCode);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, extract.getId(), "parNumber", paragraphsNumberMap.get(extract.getParagraph().getId()));
            extractDoc = (RtfDocument)scriptResult.get(IScriptExecutor.DOCUMENT);
            if (extractDoc == null)
                throw new NullPointerException();

            if (mainDoc == null)
            {
                mainDoc = extractDoc.getClone();
                mainElementList = mainDoc.getElementList();
            }
            else
                mainElementList.addAll(extractDoc.getElementList());

            if (iterator.hasNext())
                mainElementList.add(pageBreak);
        }
        while (iterator.hasNext());

        return RtfUtil.toByteArray(mainDoc);

    }

    /**
     * Необходима особая сортировка и группировка параграфов. Сначала группируется по филиалам, затем по форме освоения, виду конкурса...
     * Поэтому производим разбивку параграфов приказа на группы
     */
    private Map<Long, Integer> prepareOrderParagraphs(EnrOrder order)
    {
        List<EnrOrderFilialPartWrapper> orderFilialParts = new ArrayList<>();
        for (IAbstractParagraph paragraph : order.getParagraphList())
        {
            if (!(paragraph instanceof EnrEnrollmentParagraph))
                throw new ApplicationException("Параграф №${paragraph.number} в приказе «" + paragraph.getOrder().getTitle() + "» не может быть напечатан, так как не является параграфом о зачислении.");
            EnrEnrollmentParagraph enrParagraph = (EnrEnrollmentParagraph)paragraph;

            EnrOrderFilialPartWrapper filialPart = new EnrOrderFilialPartWrapper(enrParagraph.getFormativeOrgUnit());
            int ind = orderFilialParts.indexOf(filialPart);
            if (ind == -1)
                orderFilialParts.add(filialPart);
            else
                filialPart = orderFilialParts.get(ind);

            EnrOrderPartWrapper part = new EnrOrderPartWrapper(enrParagraph.getProgramForm(), enrParagraph.getCompetitionType());
            int pos = filialPart.getOrderParts().indexOf(part);
            if (pos == -1)
                filialPart.getOrderParts().add(part);
            else
                part = filialPart.getOrderParts().get(pos);


            part.getParagraphList().add(enrParagraph);
        }

        Collections.sort(orderFilialParts);
        int counter = 0;

        Map<Long, Integer> resultMap = new HashMap<>();
        for (EnrOrderFilialPartWrapper filialPartWrapper : orderFilialParts)
        {
            Collections.sort(filialPartWrapper.getOrderParts());
            for (EnrOrderPartWrapper orderPartWrapper : filialPartWrapper.getOrderParts())
            {
                Collections.sort(orderPartWrapper.getParagraphList(), new Comparator<EnrEnrollmentParagraph>()
                {
                    @Override
                    public int compare(EnrEnrollmentParagraph o1, EnrEnrollmentParagraph o2)
                    {
                        return o1.getProgramSubject().getCode().compareTo(o2.getProgramSubject().getCode());
                    }
                });
                for (EnrEnrollmentParagraph paragraph : orderPartWrapper.getParagraphList())
                    resultMap.put(paragraph.getId(), ++counter);
            }
        }

        return resultMap;
    }

    private String getScriptCode(EnrEnrollmentExtract extract)
    {
        String code = ((EnrEnrollmentParagraph)extract.getParagraph()).getCompetitionType().getCode();
        switch(code)
        {
            case EnrCompetitionTypeCodes.EXCLUSIVE:
                return EnrScriptItemCodes.NARFU_ENR_ORDER_EXTRACT_QUOTA;
            case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                return EnrScriptItemCodes.NARFU_ENR_ORDER_EXTRACT_TA;
        }
       return EnrScriptItemCodes.NARFU_ENR_ORDER_EXTRACT;
    }

}