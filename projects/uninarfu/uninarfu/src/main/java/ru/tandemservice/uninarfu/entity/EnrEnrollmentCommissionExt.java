package ru.tandemservice.uninarfu.entity;

import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.uninarfu.entity.gen.*;

/** @see ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen */
public class EnrEnrollmentCommissionExt extends EnrEnrollmentCommissionExtGen
{
    public EnrEnrollmentCommissionExt() {}

    public EnrEnrollmentCommissionExt(EnrEnrollmentCommission commission)
    {
        setEnrEnrollmentCommission(commission);
    }
}