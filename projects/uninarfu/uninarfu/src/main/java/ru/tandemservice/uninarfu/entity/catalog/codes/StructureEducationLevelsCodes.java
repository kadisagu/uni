package ru.tandemservice.uninarfu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Структура направлений подготовки (специальностей) Минобрнауки РФ"
 * Имя сущности : structureEducationLevels
 * Файл data.xml : uninarfu.data.xml
 */
public interface StructureEducationLevelsCodes
{
    /** Константа кода (code) элемента : группа направлений/специальностей ВПО (ГОС2) (title) */
    String HIGH_GOS2_GROUP = "1";
    /** Константа кода (code) элемента : направление специальностей (title) */
    String HIGH_GOS2_GROUP_SPECIALTY_FIELD = "2";
    /** Константа кода (code) элемента : направление магистров (title) */
    String HIGH_GOS2_GROUP_MASTER_FIELD = "3";
    /** Константа кода (code) элемента : направление бакалавров (title) */
    String HIGH_GOS2_GROUP_BACHELOR_FIELD = "4";
    /** Константа кода (code) элемента : специальность (title) */
    String HIGH_GOS2_GROUP_SPECIALTY = "5";
    /** Константа кода (code) элемента : специальность (title) */
    String HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY = "6";
    /** Константа кода (code) элемента : магистерский профиль (программа) (title) */
    String HIGH_GOS2_GROUP_MASTER_PROFILE = "7";
    /** Константа кода (code) элемента : бакалаврский профиль (программа) (title) */
    String HIGH_GOS2_GROUP_BACHELOR_PROFILE = "8";
    /** Константа кода (code) элемента : специализация (title) */
    String HIGH_GOS2_GROUP_SPECIALTY_SPECIALIZATION = "9";
    /** Константа кода (code) элемента : специализация (title) */
    String HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY_SPECIALIZATION = "10";
    /** Константа кода (code) элемента : группа направлений/специальностей СПО (ГОС2) (title) */
    String MIDDLE_GOS2_GROUP = "11";
    /** Константа кода (code) элемента : направление специальностей (title) */
    String MIDDLE_GOS2_GROUP_SPECIALTY_FIELD = "12";
    /** Константа кода (code) элемента : специальность (базовый уровень) (title) */
    String MIDDLE_GOS2_GROUP_SPECIALTY_BASIC = "13";
    /** Константа кода (code) элемента : специальность (повышенный уровень) (title) */
    String MIDDLE_GOS2_GROUP_SPECIALTY_ADVANCED = "14";
    /** Константа кода (code) элемента : группа профессий/специальностей НПО (title) */
    String BASIC_GROUP = "15";
    /** Константа кода (code) элемента : направление профессий/специальностей (title) */
    String BASIC_GROUP_PROFESSION_FIELD = "16";
    /** Константа кода (code) элемента : профессия/специальность (title) */
    String BASIC_GROUP_PROFESSION = "17";
    /** Константа кода (code) элемента : группа направлений ОО/ДО/ДПО (title) */
    String ADDITIONAL_GROUP = "18";
    /** Константа кода (code) элемента : направление повышения квалификации (title) */
    String ADDITIONAL_GROUP_TRAINING_FIELD = "19";
    /** Константа кода (code) элемента : программа повышения квалификации (title) */
    String ADDITIONAL_GROUP_TRAINING_PROFILE = "20";
    /** Константа кода (code) элемента : направление профессиональной переподготовки (title) */
    String ADDITIONAL_GROUP_RETRAINING_FIELD = "21";
    /** Константа кода (code) элемента : профессиональная переподготовка (доп. квалификация) (title) */
    String ADDITIONAL_GROUP_RETRAINING_ADDITIONAL = "22";
    /** Константа кода (code) элемента : профессиональная переподготовка (новый вид деятельности) (title) */
    String ADDITIONAL_GROUP_RETRAINING_FULL = "23";
    /** Константа кода (code) элемента : общее образование (title) */
    String ADDITIONAL_GROUP_PROBATION = "24";
    /** Константа кода (code) элемента : дополнительные образовательные услуги (title) */
    String ADDITIONAL_GROUP_ADDITIONAL = "25";
    /** Константа кода (code) элемента : группа направлений/специальностей ВПО (ФГОС) (title) */
    String HIGH_GOS3_GROUP = "26";
    /** Константа кода (code) элемента : направление бакалавров (title) */
    String HIGH_GOS3_GROUP_BACHELOR_FIELD = "27";
    /** Константа кода (code) элемента : бакалаврский профиль (программа) (title) */
    String HIGH_GOS3_GROUP_BACHELOR_PROFILE = "28";
    /** Константа кода (code) элемента : направление магистров (title) */
    String HIGH_GOS3_GROUP_MASTER_FIELD = "29";
    /** Константа кода (code) элемента : магистерский профиль (программа) (title) */
    String HIGH_GOS3_GROUP_MASTER_PROFILE = "30";
    /** Константа кода (code) элемента : направление специальностей (title) */
    String HIGH_GOS3_GROUP_SPECIALTY_FIELD = "31";
    /** Константа кода (code) элемента : специальность (title) */
    String HIGH_GOS3_GROUP_SPECIALTY = "32";
    /** Константа кода (code) элемента : специализация (title) */
    String HIGH_GOS3_GROUP_SPECIALIZATION = "33";
    /** Константа кода (code) элемента : группа направлений/специальностей СПО (ФГОС) (title) */
    String MIDDLE_GOS3_GROUP = "34";
    /** Константа кода (code) элемента : направление специальностей (title) */
    String MIDDLE_GOS3_GROUP_SPECIALTY_FIELD = "35";
    /** Константа кода (code) элемента : специальность (базовый уровень) (title) */
    String MIDDLE_GOS3_GROUP_SPECIALTY_BASIC = "36";
    /** Константа кода (code) элемента : специальность (повышенный уровень) (title) */
    String MIDDLE_GOS3_GROUP_SPECIALTY_ADVANCED = "37";
    /** Константа кода (code) элемента : направление подготовки в аспирантуре (title) */
    String HIGHER_SPECIALITY_POSTGRADUATE = "38";
    /** Константа кода (code) элемента : направленность подготовки в аспирантуре (title) */
    String HIGHER_SPECIALIZATION_POSTGRADUATE = "39";
    /** Константа кода (code) элемента : специальность (title) */
    String HIGHER_GOS3_SPECIALTY_AT_BRANCH_OF_SCIENCE = "41";
    /** Константа кода (code) элемента : специальность (интернатура) (title) */
    String HIGHER_SPECIALITY_INTERNSHIP = "higher.speciality.internship";
    /** Константа кода (code) элемента : специализация (интернатура) (title) */
    String HIGHER_SPECIALIZATION_INTERNSHIP = "higher.specialization.internship";
    /** Константа кода (code) элемента : специальность (ординатура) (title) */
    String HIGHER_SPECIALITY_TRAINEESHIP = "higher.speciality.traineeship";
    /** Константа кода (code) элемента : специализация (ординатура) (title) */
    String HIGHER_SPECIALIZATION_TRAINEESHIP = "higher.specialization.traineeship";
    /** Константа кода (code) элемента : направление подготовки в адъюнктуре (title) */
    String HIGHER_SPECIALITY_ADJUNCTURE = "higher.speciality.adjuncture";
    /** Константа кода (code) элемента : направленность подготовки в адъюнктуре (title) */
    String HIGHER_SPECIALIZATION_ADJUNCTURE = "higher.specialization.adjuncture";

    Set<String> CODES = ImmutableSet.of(HIGH_GOS2_GROUP, HIGH_GOS2_GROUP_SPECIALTY_FIELD, HIGH_GOS2_GROUP_MASTER_FIELD, HIGH_GOS2_GROUP_BACHELOR_FIELD, HIGH_GOS2_GROUP_SPECIALTY, HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY, HIGH_GOS2_GROUP_MASTER_PROFILE, HIGH_GOS2_GROUP_BACHELOR_PROFILE, HIGH_GOS2_GROUP_SPECIALTY_SPECIALIZATION, HIGH_GOS2_GROUP_SPECIALTY_FIELD_SPECIALTY_SPECIALIZATION, MIDDLE_GOS2_GROUP, MIDDLE_GOS2_GROUP_SPECIALTY_FIELD, MIDDLE_GOS2_GROUP_SPECIALTY_BASIC, MIDDLE_GOS2_GROUP_SPECIALTY_ADVANCED, BASIC_GROUP, BASIC_GROUP_PROFESSION_FIELD, BASIC_GROUP_PROFESSION, ADDITIONAL_GROUP, ADDITIONAL_GROUP_TRAINING_FIELD, ADDITIONAL_GROUP_TRAINING_PROFILE, ADDITIONAL_GROUP_RETRAINING_FIELD, ADDITIONAL_GROUP_RETRAINING_ADDITIONAL, ADDITIONAL_GROUP_RETRAINING_FULL, ADDITIONAL_GROUP_PROBATION, ADDITIONAL_GROUP_ADDITIONAL, HIGH_GOS3_GROUP, HIGH_GOS3_GROUP_BACHELOR_FIELD, HIGH_GOS3_GROUP_BACHELOR_PROFILE, HIGH_GOS3_GROUP_MASTER_FIELD, HIGH_GOS3_GROUP_MASTER_PROFILE, HIGH_GOS3_GROUP_SPECIALTY_FIELD, HIGH_GOS3_GROUP_SPECIALTY, HIGH_GOS3_GROUP_SPECIALIZATION, MIDDLE_GOS3_GROUP, MIDDLE_GOS3_GROUP_SPECIALTY_FIELD, MIDDLE_GOS3_GROUP_SPECIALTY_BASIC, MIDDLE_GOS3_GROUP_SPECIALTY_ADVANCED, HIGHER_SPECIALITY_POSTGRADUATE, HIGHER_SPECIALIZATION_POSTGRADUATE, HIGHER_GOS3_SPECIALTY_AT_BRANCH_OF_SCIENCE, HIGHER_SPECIALITY_INTERNSHIP, HIGHER_SPECIALIZATION_INTERNSHIP, HIGHER_SPECIALITY_TRAINEESHIP, HIGHER_SPECIALIZATION_TRAINEESHIP, HIGHER_SPECIALITY_ADJUNCTURE, HIGHER_SPECIALIZATION_ADJUNCTURE);
}
