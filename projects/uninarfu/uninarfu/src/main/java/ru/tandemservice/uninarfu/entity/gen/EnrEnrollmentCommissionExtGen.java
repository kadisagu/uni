package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение отборочной комиссии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrEnrollmentCommissionExtGen extends EntityBase
 implements INaturalIdentifiable<EnrEnrollmentCommissionExtGen>, ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt";
    public static final String ENTITY_NAME = "enrEnrollmentCommissionExt";
    public static final int VERSION_HASH = 1251902616;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ENROLLMENT_COMMISSION = "enrEnrollmentCommission";
    public static final String L_EMPLOYEE = "employee";
    public static final String P_PHONE_NUMBER = "phoneNumber";
    public static final String P_SCHEDULE_ENROLLMENT_COMMISSION = "scheduleEnrollmentCommission";

    private EnrEnrollmentCommission _enrEnrollmentCommission;     // Отборочная комиссия
    private EmployeePost _employee;     // Ответственный секретарь отборочной комиссии
    private String _phoneNumber;     // Телефон отборочной комиссии
    private String _scheduleEnrollmentCommission; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrEnrollmentCommission getEnrEnrollmentCommission()
    {
        return _enrEnrollmentCommission;
    }

    /**
     * @param enrEnrollmentCommission Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     */
    public void setEnrEnrollmentCommission(EnrEnrollmentCommission enrEnrollmentCommission)
    {
        dirty(_enrEnrollmentCommission, enrEnrollmentCommission);
        _enrEnrollmentCommission = enrEnrollmentCommission;
    }

    /**
     * @return Ответственный секретарь отборочной комиссии.
     */
    public EmployeePost getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Ответственный секретарь отборочной комиссии.
     */
    public void setEmployee(EmployeePost employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    /**
     * @return Телефон отборочной комиссии.
     */
    @Length(max=255)
    public String getPhoneNumber()
    {
        return _phoneNumber;
    }

    /**
     * @param phoneNumber Телефон отборочной комиссии.
     */
    public void setPhoneNumber(String phoneNumber)
    {
        dirty(_phoneNumber, phoneNumber);
        _phoneNumber = phoneNumber;
    }

    /**
     * @return 
     */
    @Length(max=1000)
    public String getScheduleEnrollmentCommission()
    {
        initLazyForGet("scheduleEnrollmentCommission");
        return _scheduleEnrollmentCommission;
    }

    /**
     * @param scheduleEnrollmentCommission 
     */
    public void setScheduleEnrollmentCommission(String scheduleEnrollmentCommission)
    {
        initLazyForSet("scheduleEnrollmentCommission");
        dirty(_scheduleEnrollmentCommission, scheduleEnrollmentCommission);
        _scheduleEnrollmentCommission = scheduleEnrollmentCommission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrEnrollmentCommissionExtGen)
        {
            if (withNaturalIdProperties)
            {
                setEnrEnrollmentCommission(((EnrEnrollmentCommissionExt)another).getEnrEnrollmentCommission());
            }
            setEmployee(((EnrEnrollmentCommissionExt)another).getEmployee());
            setPhoneNumber(((EnrEnrollmentCommissionExt)another).getPhoneNumber());
            setScheduleEnrollmentCommission(((EnrEnrollmentCommissionExt)another).getScheduleEnrollmentCommission());
        }
    }

    public INaturalId<EnrEnrollmentCommissionExtGen> getNaturalId()
    {
        return new NaturalId(getEnrEnrollmentCommission());
    }

    public static class NaturalId extends NaturalIdBase<EnrEnrollmentCommissionExtGen>
    {
        private static final String PROXY_NAME = "EnrEnrollmentCommissionExtNaturalProxy";

        private Long _enrEnrollmentCommission;

        public NaturalId()
        {}

        public NaturalId(EnrEnrollmentCommission enrEnrollmentCommission)
        {
            _enrEnrollmentCommission = ((IEntity) enrEnrollmentCommission).getId();
        }

        public Long getEnrEnrollmentCommission()
        {
            return _enrEnrollmentCommission;
        }

        public void setEnrEnrollmentCommission(Long enrEnrollmentCommission)
        {
            _enrEnrollmentCommission = enrEnrollmentCommission;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof EnrEnrollmentCommissionExtGen.NaturalId) ) return false;

            EnrEnrollmentCommissionExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getEnrEnrollmentCommission(), that.getEnrEnrollmentCommission()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEnrEnrollmentCommission());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEnrEnrollmentCommission());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrEnrollmentCommissionExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrEnrollmentCommissionExt.class;
        }

        public T newInstance()
        {
            return (T) new EnrEnrollmentCommissionExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrEnrollmentCommission":
                    return obj.getEnrEnrollmentCommission();
                case "employee":
                    return obj.getEmployee();
                case "phoneNumber":
                    return obj.getPhoneNumber();
                case "scheduleEnrollmentCommission":
                    return obj.getScheduleEnrollmentCommission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrEnrollmentCommission":
                    obj.setEnrEnrollmentCommission((EnrEnrollmentCommission) value);
                    return;
                case "employee":
                    obj.setEmployee((EmployeePost) value);
                    return;
                case "phoneNumber":
                    obj.setPhoneNumber((String) value);
                    return;
                case "scheduleEnrollmentCommission":
                    obj.setScheduleEnrollmentCommission((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrEnrollmentCommission":
                        return true;
                case "employee":
                        return true;
                case "phoneNumber":
                        return true;
                case "scheduleEnrollmentCommission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrEnrollmentCommission":
                    return true;
                case "employee":
                    return true;
                case "phoneNumber":
                    return true;
                case "scheduleEnrollmentCommission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrEnrollmentCommission":
                    return EnrEnrollmentCommission.class;
                case "employee":
                    return EmployeePost.class;
                case "phoneNumber":
                    return String.class;
                case "scheduleEnrollmentCommission":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrEnrollmentCommissionExt> _dslPath = new Path<EnrEnrollmentCommissionExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrEnrollmentCommissionExt");
    }
            

    /**
     * @return Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getEnrEnrollmentCommission()
     */
    public static EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrEnrollmentCommission()
    {
        return _dslPath.enrEnrollmentCommission();
    }

    /**
     * @return Ответственный секретарь отборочной комиссии.
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getEmployee()
     */
    public static EmployeePost.Path<EmployeePost> employee()
    {
        return _dslPath.employee();
    }

    /**
     * @return Телефон отборочной комиссии.
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getPhoneNumber()
     */
    public static PropertyPath<String> phoneNumber()
    {
        return _dslPath.phoneNumber();
    }

    /**
     * @return 
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getScheduleEnrollmentCommission()
     */
    public static PropertyPath<String> scheduleEnrollmentCommission()
    {
        return _dslPath.scheduleEnrollmentCommission();
    }

    public static class Path<E extends EnrEnrollmentCommissionExt> extends EntityPath<E>
    {
        private EnrEnrollmentCommission.Path<EnrEnrollmentCommission> _enrEnrollmentCommission;
        private EmployeePost.Path<EmployeePost> _employee;
        private PropertyPath<String> _phoneNumber;
        private PropertyPath<String> _scheduleEnrollmentCommission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Отборочная комиссия. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getEnrEnrollmentCommission()
     */
        public EnrEnrollmentCommission.Path<EnrEnrollmentCommission> enrEnrollmentCommission()
        {
            if(_enrEnrollmentCommission == null )
                _enrEnrollmentCommission = new EnrEnrollmentCommission.Path<EnrEnrollmentCommission>(L_ENR_ENROLLMENT_COMMISSION, this);
            return _enrEnrollmentCommission;
        }

    /**
     * @return Ответственный секретарь отборочной комиссии.
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getEmployee()
     */
        public EmployeePost.Path<EmployeePost> employee()
        {
            if(_employee == null )
                _employee = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE, this);
            return _employee;
        }

    /**
     * @return Телефон отборочной комиссии.
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getPhoneNumber()
     */
        public PropertyPath<String> phoneNumber()
        {
            if(_phoneNumber == null )
                _phoneNumber = new PropertyPath<String>(EnrEnrollmentCommissionExtGen.P_PHONE_NUMBER, this);
            return _phoneNumber;
        }

    /**
     * @return 
     * @see ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt#getScheduleEnrollmentCommission()
     */
        public PropertyPath<String> scheduleEnrollmentCommission()
        {
            if(_scheduleEnrollmentCommission == null )
                _scheduleEnrollmentCommission = new PropertyPath<String>(EnrEnrollmentCommissionExtGen.P_SCHEDULE_ENROLLMENT_COMMISSION, this);
            return _scheduleEnrollmentCommission;
        }

        public Class getEntityClass()
        {
            return EnrEnrollmentCommissionExt.class;
        }

        public String getEntityName()
        {
            return "enrEnrollmentCommissionExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
