package ru.tandemservice.uninarfu.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.uninarfu.entity.gen.*;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCountAdd.NarfuEnrReportRequestCountAdd;

import java.util.Arrays;
import java.util.List;

/**
 * @see ru.tandemservice.uninarfu.entity.gen.NarfuEnrReportRequestCountGen
 */
public class NarfuEnrReportRequestCount extends NarfuEnrReportRequestCountGen implements IEnrReport
{
    public static final String REPORT_KEY = "narfuEnr14ReportRequestCount";

    @SuppressWarnings("unchecked")
    private static List<String> properties = Arrays.asList(P_ENROLLMENT_COMMISSION,
                                                           P_REQUEST_TYPE,
                                                           P_COMPENSATION_TYPE,
                                                           P_COMPETITION_TYPE,
                                                           P_ENR_ORG_UNIT,
                                                           P_FORMATIVE_ORG_UNIT,
                                                           P_PROGRAM_SUBJECT,
                                                           P_EDU_PROGRAM,
                                                           P_PROGRAM_SET,
                                                           P_PARALLEL,
                                                           P_BY_ALL_ORG_UNIT);

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc()
        {
            @Override
            public String getReportKey()
            {
                return REPORT_KEY;
            }

            @Override
            public Class<? extends IEnrReport> getReportClass()
            {
                return NarfuEnrReportRequestCount.class;
            }

            @Override
            public List<String> getPropertyList()
            {
                return properties;
            }

            @Override
            public Class<? extends BusinessComponentManager> getAddFormComponent()
            {
                return NarfuEnrReportRequestCountAdd.class;
            }

            @Override
            public String getPubTitle()
            {
                return "Отчет «Количество поданных заявлений (САФУ)»";
            }

            @Override
            public String getListTitle()
            {
                return "Список отчетов «Количество поданных заявлений (САФУ)»";
            }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc()
    {
        return getDescription();
    }

    @Override
    public String getPeriodTitle()
    {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}