/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequestAddWizard.ui.ExamsStep;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ExamsStep.EnrEntrantRequestAddWizardExamsStepUI;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.NarfuEntrantManager;

/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
public class EnrEntrantRequestAddWizardExamsSaveStep extends NamedUIAction
{
    protected EnrEntrantRequestAddWizardExamsSaveStep(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EnrEntrantRequestAddWizardExamsStepUI)
        {
            EnrEntrantRequestAddWizardExamsStepUI addEditUI = (EnrEntrantRequestAddWizardExamsStepUI) presenter;
            IUIAddon uiAddon = presenter.getConfig().getAddon(EnrEntrantRequestAddWizardExamsStepExt.ADDON_NAME);
            if (null != uiAddon)
            {
                if (uiAddon instanceof EnrEntrantRequestAddWizardExamsStepExtUI)
                {
                    EnrEntrantRequestAddWizardExamsStepExtUI addon = (EnrEntrantRequestAddWizardExamsStepExtUI) uiAddon;
                    NarfuEntrantManager.instance().dao().deleteSpecConditionForEntrant(addEditUI.getEntrant());

                    NarfuEntrantManager.instance().dao().saveOrUpdateSpecConditions(addEditUI.getEntrant(), addon.getSpecConditionsList());
                }

            }
            addEditUI.saveStepData();
        }
    }
}