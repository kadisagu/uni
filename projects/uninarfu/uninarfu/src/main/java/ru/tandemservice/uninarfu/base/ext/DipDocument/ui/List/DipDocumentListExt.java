package ru.tandemservice.uninarfu.base.ext.DipDocument.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.List.DipDocumentList;


/**
 * @author Andrey Andreev
 * @since 15.10.2015
 */
@Configuration
public class DipDocumentListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uninarfu"+DipDocumentListExtUI.class.getSimpleName();

    @Autowired
    private DipDocumentList _dipDocumentList;

    @Bean
    public PresenterExtension presenterExtPoint()
    {
        return presenterExtensionBuilder(_dipDocumentList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, DipDocumentListExtUI.class))
                .create();
    }
}
