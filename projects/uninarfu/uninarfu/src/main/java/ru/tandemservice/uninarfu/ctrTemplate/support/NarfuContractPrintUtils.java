/*$Id$*/
package ru.tandemservice.uninarfu.ctrTemplate.support;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.entity.contactor.*;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.narfu.entity.PersonNextOfKinNARFU;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 23.06.2015
 */
public final class NarfuContractPrintUtils
{

    public static void printNarfuPaymentData(List<CtrPaymentPromice> paymentPromices, RtfInjectModifier modifier)
    {
        if (paymentPromices.isEmpty())
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по оплате.");
        CtrPaymentPromice paymentPromice = paymentPromices.get(0);

        modifier.put("payDay", String.valueOf(CoreDateUtils.getDayOfMonth(paymentPromice.getDeadlineDate())));
        modifier.put("payMonthStr", RussianDateFormatUtils.getMonthName(paymentPromice.getDeadlineDate(), false));
        modifier.put("payYr", RussianDateFormatUtils.getYearString(paymentPromice.getDeadlineDate(), true));

        long sum = 0L;
        for (CtrPaymentPromice promice : paymentPromices)
            sum = sum + promice.getCostAsLong();
        Double sumD = Currency.wrap(sum);

        modifier.put("chargeRate", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(sumD));
        modifier.put("chargeRateInWords", NumberSpellingUtil.spellNumberMasculineGender(sumD.intValue()));
        modifier.put("chargeRateKop", String.valueOf(Math.round(100.0d * sumD) - 100 * sumD.intValue()));

        //нужно ужнать стоимость обучения за год.
        Double firstYearCost = 0D;

        if (!paymentPromices.isEmpty())
        {
            CtrPaymentPromice promice = paymentPromices.get(0);
            final CtrPriceElementCostStage sourceStage = promice.getSourceStage();

            if (sourceStage != null && sourceStage.getNumber() == 1)
            {
                //поэтому берем первую часть платежа и считаем ее как плата за год.
                firstYearCost = Currency.wrap(sourceStage.getStageCostAsLong());
            }
        }

        modifier.put("chargeFirstYearRate", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(firstYearCost));
        modifier.put("chargeFirstYearRateInWords", NumberSpellingUtil.spellNumberMasculineGender(firstYearCost.intValue()));
        modifier.put("chargeFirstYearRateKop", String.valueOf(Math.round(100.0d * firstYearCost) - 100 * firstYearCost.intValue()));
    }

    public static void printNarfuCustomerData(ContactorPerson customer, RtfInjectModifier modifier)
    {
        if (customer instanceof JuridicalContactor)
        {
            JuridicalContactor jCustomer = (JuridicalContactor) customer;
            ContactorPersonActivityBase cActivityBase = ContactorManager.instance().dao().getCurrentCotactorPersonActivity(jCustomer);

            ExternalOrgUnit extOrgUnit = jCustomer.getExternalOrgUnit();
            modifier.put("customerShortInfo", extOrgUnit.getTitle());
            modifier.put("customerPlenipotenriaryPost", jCustomer.getPost());

            modifier.put("customerActivityGround", cActivityBase == null ? "" : cActivityBase.getDisplayableTitle());

            modifier.put("customerTitle", extOrgUnit.getLegalFormWithTitle());
            modifier.put("customerInfoCaption", "Банковские реквизиты");
            modifier.put("customerAddress", extOrgUnit.getLegalAddress() == null ? "" : extOrgUnit.getLegalAddress().getTitleWithFlat());

            modifier.put("customerINNKPP", StringUtils.trimToEmpty(extOrgUnit.getInn()));
            modifier.put("customerBIK", StringUtils.trimToEmpty(extOrgUnit.getBic()));
            modifier.put("customerCurAcc", StringUtils.trimToEmpty(extOrgUnit.getCurAccount()));
            modifier.put("customerCorAcc", StringUtils.trimToEmpty(extOrgUnit.getCorAccount()));
            modifier.put("customerBankTitle", StringUtils.trimToEmpty(extOrgUnit.getBank()));
            modifier.put("customerPhoneNumber", StringUtils.trimToEmpty(extOrgUnit.getPhone()));

            modifier.put("customerPlenipotenriaryFio", customer.getPerson().getFullFio());
            modifier.put("customerSNILS", customer.getPerson().getSnilsNumber());
        } else
        {
            IdentityCard idc = customer.getPerson().getIdentityCard();
            modifier.put("customerInfoCaption", idc.getCardType().getShortTitle());
            modifier.put("customerActivityGround", "личной инициативы");

            //чтобы не мешалось
            modifier.put("customerShortInfo", "");
            modifier.put("customerPlenipotenriaryPost", "");
            modifier.put("customerActivityGround", "");
            modifier.put("customerTitle", "");
            modifier.put("customerInfoCaption", "");
            modifier.put("customerAddress", customer.getPerson().getAddress() == null ? "" : customer.getPerson().getAddress().getTitleWithFlat());


            modifier.put("customerBIK", "");
            modifier.put("customerCurAcc", "");
            modifier.put("customerCorAcc", "");
            modifier.put("customerBankTitle", "");
            modifier.put("customerPhoneNumber", StringUtils.trimToEmpty(customer.getPhone()));

            modifier.put("customerPlenipotenriaryFio", "");

            if (customer instanceof PhysicalContactor)
            {
                final ICommonDAO dao = DataAccessServices.dao();
                PersonNextOfKin2physicalContactorRelation relation = dao.get(PersonNextOfKin2physicalContactorRelation.class, PersonNextOfKin2physicalContactorRelation.physicalContactor(), (PhysicalContactor) customer);
                PersonNextOfKinNARFU nextOfKinNARFU = null;
                if (relation != null)
                {
                    nextOfKinNARFU = dao.get(PersonNextOfKinNARFU.class, PersonNextOfKinNARFU.base(), relation.getPersonNextOfKin());
                }
                if (nextOfKinNARFU != null)
                {
                    modifier.put("customerINNKPP", StringUtils.trimToEmpty(nextOfKinNARFU.getINN()));
                    modifier.put("customerSNILS", StringUtils.trimToEmpty(nextOfKinNARFU.getPensionInsuranceNumber()));
                } else
                {
                    modifier.put("customerINNKPP", "");
                    modifier.put("customerSNILS", "");
                }
            } else
            {
                modifier.put("customerINNKPP", StringUtils.trimToEmpty(customer.getPerson().getInnNumber()));
                modifier.put("customerSNILS", customer.getPerson().getSnilsNumber());
            }
        }

        modifier.put("actedCustomer", customer.getPerson().isMale() ? "действующий" : "действующая");

        {
            IdentityCard idc = customer.getPerson().getIdentityCard();
            modifier.put("customerFio", customer.getPerson().getFullFio());
            modifier.put("customerShortFio", customer.getPerson().getFio());

            String customerPassportSeria = idc.getSeria();
            String customerPassportNumber = idc.getNumber();
            String customerPassportDate = idc.getIssuanceDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(idc.getIssuanceDate());
            String customerPassportInfo = StringUtils.trimToEmpty(idc.getIssuancePlace());
            modifier.put("customerPassportInfo", String.format("Паспортные данные: серия %s №%s, выдан %s %s г",
                    customerPassportSeria, customerPassportNumber, customerPassportInfo, customerPassportDate));

            modifier.put("customerRegAddress", idc.getAddress() == null ? "" : idc.getAddress().getTitleWithFlat());
            modifier.put("customerPhoneNumber", idc.getPerson().getContactData().getMainPhones());

            modifier.put("costumerWorkPlace", customer.getPerson().getWorkPlace());
        }
    }

    public static void printNarfuEduPromiceData(EduCtrEducationPromise eduPromice, RtfInjectModifier modifier)
    {
        modifier.put("apprenticeship", eduPromice.getEduProgram().getDuration().getTitle());
        final String specialityCode = eduPromice.getEduProgram().getProgramSubject().getSubjectCode();
        modifier.put("speciality", String.format("%s (%s)", eduPromice.getEduProgram().getProgramSubject().getTitle(), specialityCode));
        modifier.put("specialityCode", specialityCode);

        modifier.put("educationTypeAlt", eduPromice.getEduProgram().getProgramSubject().getSubjectIndex().getProgramKind().getShortTitle());

        modifier.put("numberOfTerms", getDurationAtTerm(eduPromice.getEduProgram().getDuration()));
        final EduProgramForm form = eduPromice.getEduProgram().getForm();
        modifier.put("developForm_G", getProgramFormDative(form));

        ContactorPerson educationReceiver = eduPromice.getDst().getContactor();
        modifier.put("studentFullFio", educationReceiver.getFullFio());
        modifier.put("studentShortFio", educationReceiver.getFio());

        final IdentityCard identityCard = educationReceiver.getPerson().getIdentityCard();

        modifier.put("studentFullFio_G", getFullFioGrammaCase(identityCard, GrammaCase.GENITIVE));
        modifier.put("studentFullFio_A", getFullFioGrammaCase(identityCard, GrammaCase.ACCUSATIVE));

        modifier.put("phone", educationReceiver.getPerson().getContactData().getPhoneDefault());
        modifier.put("workPlace", educationReceiver.getPerson().getWorkPlace());
        modifier.put("studentINN", educationReceiver.getPerson().getInnNumber());
        modifier.put("SNILS", educationReceiver.getPerson().getSnilsNumber());

        modifier.put("studentFio_G", PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_GENITIVE));
    }

    public static void printNarfuVersionData(IEducationContractVersionTemplateData templateData, CtrContractVersion contractVersion, RtfInjectModifier modifier)
    {
        final TopOrgUnit academy = TopOrgUnit.getInstance(true);
        final EmployeePost headPost = EmployeeManager.instance().dao().getHead(academy);
        modifier.put("headFio", StringUtils.trimToEmpty(headPost == null ? "" : headPost.getPerson().getFio()));

        final OrgUnit formattiveOrgUnit = contractVersion.getContract().getOrgUnit();
        final Employee employeePost = (Employee) formattiveOrgUnit.getHead().getEmployee();
        modifier.put("orgUnitFio", employeePost.getFio());

        modifier.put("accountantFio", getChiefAccountantString());

        modifier.put("post", headPost == null ? "" : headPost.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());

        final List<EmployeePost> postList = employeePost.getPosts();
        modifier.put("orgUnitPost", postList.isEmpty() ? "" : postList.get(0).getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());


        modifier.put("filialAgency", formattiveOrgUnit.getPrintTitle());
        modifier.put("branchAgencyFullTitle", formattiveOrgUnit.getFullTitle());
        modifier.put("formativeOrgUnit_I", formattiveOrgUnit.getPrepositionalCaseTitle());
        String orgUnitSettlement = "";
        if (formattiveOrgUnit.getLegalAddress() != null && formattiveOrgUnit.getLegalAddress().getSettlement() != null)
        {
            orgUnitSettlement = formattiveOrgUnit.getLegalAddress().getSettlement().getTitle();
        }
        modifier.put("orgUnitSettlement", orgUnitSettlement);

        modifier.put("formingYear", RussianDateFormatUtils.getYearString(templateData.getOwner().getDocStartDate(), false));

        modifier.put("startDay", String.valueOf(CoreDateUtils.getDayOfMonth(templateData.getOwner().getDocStartDate())));
        modifier.put("startMonthStr", RussianDateFormatUtils.getMonthName(templateData.getOwner().getDocStartDate(), false));
        modifier.put("startYear", RussianDateFormatUtils.getYearString(templateData.getOwner().getDocStartDate(), false));

    }

    /**
     * Продолжительность обучения в семестрах
     *
     * @param duration Срок освоения (Продолжительность обучения)
     * @return строка с номером семетра
     */
    private static String getDurationAtTerm(EduProgramDuration duration)
    {
        final ICommonDAO dao = DataAccessServices.dao();
        final DevelopPeriod developPeriod = dao.getUnique(DevelopPeriod.class, DevelopPeriod.eduProgramDuration().s(), duration);
        if (developPeriod == null)
            throw new ApplicationException("Для продолжительности обучения \"" + duration.getTitle() + "\" не задан срок обучения");
        final DevelopGrid developGrid = dao.getUnique(DevelopGrid.class, DevelopGrid.developPeriod().s(), developPeriod);
        if (developGrid == null)
            throw new ApplicationException("Для срока освоения \"" + developPeriod.getTitle() + "\" не задана учебная сетка");
        final int termCount = dao.getCount(DevelopGridTerm.class, DevelopGridTerm.developGrid().s(), developGrid.getId());
        return String.valueOf(termCount);
    }

    /**
     * Форма обучения в дательном падеже,
     * а может и не в дательном.
     * а может и в винительном.
     * а может это дворник был
     * он шел по сельской местности
     * к ближайшему орешнику
     * за новою метлой
     *
     * @param programForm Форма обучения
     * @return Форма обучения в N-ом падеже
     */
    private static String getProgramFormDative(EduProgramForm programForm)
    {
        String replaceString = "ой";
        final StringBuilder title = new StringBuilder(programForm.getTitle().toLowerCase());
        final int length = title.length();
        title.replace(length - replaceString.length(), length, replaceString);
        return title.toString();
    }

    /**
     * <p>Фамилий и инициалы главного бухгалтера. Берется из сущьности "Возможная виза сотрудника".
     * Ищется по совпадению заголовка со словосочетанем "Главный бухгалтер"</p>
     *
     * @return ФИО главного бухгалтера либо пустую строку если таковой не найдено
     */
    private static String getChiefAccountantString()
    {
        final EmployeePostPossibleVisa possibleVisa = DataAccessServices.dao().getUnique(EmployeePostPossibleVisa.class, EmployeePostPossibleVisa.title().s(), "Главный бухгалтер");
        return possibleVisa == null ? "" : possibleVisa.getEntity().getFio();
    }

    /**
     * Возвращает ФИО (полное) в соответствующем падеже
     *
     * @param identityCard Удостоверение личности
     * @param grammaCase   падеж
     * @return полное имя в указанном падеже
     */
    private static String getFullFioGrammaCase(IdentityCard identityCard, GrammaCase grammaCase)
    {
        final boolean sex = identityCard.getSex().isMale();
        final String firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), grammaCase, sex);
        final String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), grammaCase, sex);
        String middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), grammaCase, sex);
        if (middleName == null) middleName = "";
        return lastName + " " + firstName + " " + middleName;
    }
}
