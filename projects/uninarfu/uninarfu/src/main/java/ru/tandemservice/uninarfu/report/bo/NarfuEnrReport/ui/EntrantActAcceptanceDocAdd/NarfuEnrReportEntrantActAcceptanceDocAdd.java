/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantActAcceptanceDocAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.EnrEntrantRequestManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;


/**
 * @author Andrey Avetisov
 * @since 09.04.2015
 */
@Configuration
public class NarfuEnrReportEntrantActAcceptanceDocAdd extends BusinessComponentManager
{
    public static final String ENR_ORG_UNIT_DS = "enrOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())
                .addDataSource(EnrEntrantRequestManager.instance().requestTypeDS_1_Config())
                .addDataSource(selectDS(ENR_ORG_UNIT_DS, enrOrgUnitDSHandler())
                        .addColumn(EduInstitutionOrgUnit.orgUnit().fullTitle().s()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler())
                        .addColumn(OrgUnit.fullTitle().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrOrgUnitDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduInstitutionOrgUnit.class)
                .filter(EduInstitutionOrgUnit.orgUnit().fullTitle())
                .order(EduInstitutionOrgUnit.orgUnit().fullTitle())
                .pageable(true);
    }
}
