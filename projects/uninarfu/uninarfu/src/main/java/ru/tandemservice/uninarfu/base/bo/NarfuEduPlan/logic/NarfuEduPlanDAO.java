/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uninarfu.entity.NarfuEduPlanVerBlokToEduLVLHSRelation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 10.04.2015
 */
public class NarfuEduPlanDAO extends UniBaseDao implements INarfuEduPlanDAO
{
    @Override
    public void saveOrUpdateEduPlanToEdLvlHSchoolRelation(Long eduPlanVersionId, Map<EduProgramSpecialization, EducationLevelsHighSchool> levelsMap)
    {
        EppEduPlanVersion eppPlanVersion = getNotNull(EppEduPlanVersion.class, eduPlanVersionId);


        final Map<EppEduPlanVersionBlock, NarfuEduPlanVerBlokToEduLVLHSRelation> relationMap = new HashMap<>();
        final Map<EduProgramSpecialization, EppEduPlanVersionSpecializationBlock> blockMap = new HashMap<>();
        List<EppEduPlanVersionSpecializationBlock> blockList = getList(EppEduPlanVersionSpecializationBlock.class, EppEduPlanVersionSpecializationBlock.eduPlanVersion(), eppPlanVersion);
        for (final NarfuEduPlanVerBlokToEduLVLHSRelation item : getList(NarfuEduPlanVerBlokToEduLVLHSRelation.class, NarfuEduPlanVerBlokToEduLVLHSRelation.eduPlanVerBlock(), blockList)) {
            relationMap.put(item.getEduPlanVerBlock(), item);
            blockMap.put(item.getEduPlanVerBlock().getProgramSpecialization(), item.getEduPlanVerBlock());
            blockList.remove(item.getEduPlanVerBlock());
        }
        //Добавляем блоки без связей
        for (EppEduPlanVersionSpecializationBlock block : blockList)
        {
            if (!blockMap.containsKey(block.getProgramSpecialization()))
                blockMap.put(block.getProgramSpecialization(), block);
        }

        // новые блоки
        for (final Map.Entry<EduProgramSpecialization, EducationLevelsHighSchool> entry : levelsMap.entrySet()) {
            EppEduPlanVersionSpecializationBlock block = blockMap.get(entry.getKey());

            NarfuEduPlanVerBlokToEduLVLHSRelation relation = null;
            if (block != null)
            {
                relation = relationMap.remove(block);
                if (relation == null)
                {
                    relation = new NarfuEduPlanVerBlokToEduLVLHSRelation();
                    relation.setEduPlanVerBlock(block);
                }
                relation.setEducationLevelsHighSchool(entry.getValue());
                getSession().saveOrUpdate(relation);
            }
        }

        // удаляем оставшиеся, если они есть
        for (final Map.Entry<EppEduPlanVersionBlock, NarfuEduPlanVerBlokToEduLVLHSRelation> entry : relationMap.entrySet()) {
            getSession().delete(entry.getValue());
        }
    }
}
