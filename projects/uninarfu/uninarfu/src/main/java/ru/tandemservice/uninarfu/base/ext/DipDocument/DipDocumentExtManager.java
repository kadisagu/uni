/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DipDocument;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Alexey Lopatin
 * @since 28.05.2015
 */
@Configuration
public class DipDocumentExtManager extends BusinessObjectExtensionManager
{
}