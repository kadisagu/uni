/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.EnrEnrollmentCommission.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.List.EnrEnrollmentCommissionList;
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt;
import ru.tandemservice.uninarfu.settings.bo.NarfuEnrEnrollmentCommission.NarfuEnrEnrollmentCommissionManager;

/**
* @author Ekaterina Zvereva
* @since 10.06.2015
*/
@Configuration
public class EnrEnrollmentCommissionListExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEnrollmentCommissionList _enrollmentCommissionList;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(_enrollmentCommissionList.presenterExtPoint())
                .replaceDataSource(searchListDS(EnrEnrollmentCommissionList.ENROLLMENT_COMMISSION_DS, _enrollmentCommissionList.enrollmentCommissionDSColumns(), NarfuEnrEnrollmentCommissionManager.instance().enrollmentCommissionDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtension columnListExtension() {
        return columnListExtensionBuilder(_enrollmentCommissionList.enrollmentCommissionDSColumns())
                .addAllBefore("enabled")
                .addColumn(textColumn(EnrEnrollmentCommissionExt.L_EMPLOYEE, "employee.fio"))
                .addColumn(textColumn(EnrEnrollmentCommissionExt.P_PHONE_NUMBER, EnrEnrollmentCommissionExt.P_PHONE_NUMBER))
                .addColumn(textColumn(EnrEnrollmentCommissionExt.P_SCHEDULE_ENROLLMENT_COMMISSION, EnrEnrollmentCommissionExt.P_SCHEDULE_ENROLLMENT_COMMISSION))
                .create();
    }
}