/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.Pub.PersonEduDocumentPub;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
@Configuration
public class PersonEduDocumentPubExt extends BusinessComponentExtensionManager {

    public static final String ADDON = "narfu_" + PersonEduDocumentPubUIExt.class.getSimpleName();

    @Autowired
    private PersonEduDocumentPub eduDocumentPub;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(eduDocumentPub.presenterExtPoint())
                .addAddon(uiAddon(ADDON, PersonEduDocumentPubUIExt.class))
                .create();
    }
}
