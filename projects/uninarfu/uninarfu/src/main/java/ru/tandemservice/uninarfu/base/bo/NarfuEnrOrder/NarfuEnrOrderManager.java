/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder.logic.INarfuEnrOrderDao;
import ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder.logic.NarfuEnrOrderDao;

/**
 * @author Ekaterina Zvereva
 * @since 07.08.2015
 */
@Configuration
public class NarfuEnrOrderManager extends BusinessObjectManager
{

    public static NarfuEnrOrderManager instance()
    {
        return instance(NarfuEnrOrderManager.class);
    }

    @Bean
    public INarfuEnrOrderDao dao()
    {
        return new NarfuEnrOrderDao();
    }
}