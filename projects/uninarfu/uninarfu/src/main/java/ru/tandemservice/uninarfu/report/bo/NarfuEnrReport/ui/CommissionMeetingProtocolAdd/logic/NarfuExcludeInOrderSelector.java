/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.logic;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 09.03.2016
 */
public class NarfuExcludeInOrderSelector
{

    private boolean _excludeInOrderActive;
    private List<ExtractStates> selectedStates = new ArrayList<>();

    private ISelectModel orderStateModel;

    public NarfuExcludeInOrderSelector()
    {
        setOrderStateModel(new LazySimpleSelectModel<>(ExtractStates.class, ExtractStates.title().s()));
    }

    public boolean isExcludeInOrderActive()
    {
        return _excludeInOrderActive;
    }

    public void setExcludeInOrderActive(boolean excludeInOrderActive)
    {
        _excludeInOrderActive = excludeInOrderActive;
    }

    public List<ExtractStates> getSelectedStates()
    {
        return selectedStates;
    }

    public void setSelectedStates(List<ExtractStates> selectedStates)
    {
        this.selectedStates = selectedStates;
    }

    public ISelectModel getOrderStateModel()
    {
        return orderStateModel;
    }

    public void setOrderStateModel(ISelectModel orderStateModel)
    {
        this.orderStateModel = orderStateModel;
    }
}