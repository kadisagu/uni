/* $Id$ */
package ru.tandemservice.uninarfu.entrant.bo.NarfuEnrEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uninarfu.entrant.bo.logic.INarfuEnrEntrantRequestDao;
import ru.tandemservice.uninarfu.entrant.bo.logic.NarfuEnrEntrantRequestDao;
import ru.tandemservice.uninarfu.entrant.bo.logic.NarfuEntrantNumberGenerationRule;

/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
@Configuration
public class NarfuEnrEntrantManager extends BusinessObjectManager
{
    public static NarfuEnrEntrantManager instance()
    {
        return instance(NarfuEnrEntrantManager.class);
    }

    @Bean
    public INarfuEnrEntrantRequestDao dao()
    {
        return new NarfuEnrEntrantRequestDao();
    }

    @Bean
    public NarfuEntrantNumberGenerationRule entrantNumberGenerationRule()
    {
        return new NarfuEntrantNumberGenerationRule();
    }
}
