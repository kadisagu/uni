/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.EnrEnrollmentCommission.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.Pub.EnrEnrollmentCommissionPub;

/**
* @author Ekaterina Zvereva
* @since 10.06.2015
*/
@Configuration
public class EnrEnrollmentCommissionPubExt extends BusinessComponentExtensionManager
{
    public static String ADDON_NAME = "narfuEnrEnrollmentCommissionPubExt";

    @Autowired
    private EnrEnrollmentCommissionPub _enrollmentCommissionPub;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(_enrollmentCommissionPub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEnrollmentCommissionPubExtUI.class))
                .create();
    }
}