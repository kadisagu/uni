/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequestAddWizard;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;


/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
@Configuration
public class EnrEntrantRequestAddWizardExtManager extends BusinessObjectExtensionManager
{
}