/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder.logic;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 12.08.2015
 */
public interface INarfuEnrOrderDao
{

    byte[] printEnrOrderExtracts(List<Long> paragraphIds);
}