/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuBSODisposalAct;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Andrey Andreev
 * @since 21.03.2016
 */
@Configuration
public class NarfuBSODisposalActManager extends BusinessObjectManager
{
    public static NarfuBSODisposalActManager instance()
    {
        return instance(NarfuBSODisposalActManager.class);
    }
}