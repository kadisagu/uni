/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.EnrEnrollmentCommission.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AddEdit.EnrEnrollmentCommissionAddEditUI;
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt;
import ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen;

/**
 * @author Ekaterina Zvereva
 * @since 10.06.2015
 */
public class EnrEnrollmentCommissionAddEditExtUI extends UIAddon
{
    public EnrEnrollmentCommissionAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private EnrEnrollmentCommissionExt _enrollmentCommissionExt;
    private EnrEnrollmentCommissionAddEditUI _presenter;

    public EnrEnrollmentCommissionExt getEnrollmentCommissionExt()
    {
        return _enrollmentCommissionExt;
    }

    public void setEnrollmentCommissionExt(EnrEnrollmentCommissionExt enrollmentCommissionExt)
    {
        _enrollmentCommissionExt = enrollmentCommissionExt;
    }

    @Override
    public void onComponentRefresh()
    {
        _presenter = getPresenter();
        _enrollmentCommissionExt = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentCommissionExtGen.NaturalId(_presenter.getEnrollmentCommission()));
        if (_enrollmentCommissionExt == null)
            _enrollmentCommissionExt = new EnrEnrollmentCommissionExt(_presenter.getEnrollmentCommission());

    }
}