/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.EnrEnrollmentCommission.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.AddEdit.EnrEnrollmentCommissionAddEditUI;

/**
 * @author Ekaterina Zvereva
 * @since 10.06.2015
 */
public class EnrEnrollmentCommissionClickApply extends NamedUIAction
{
    protected EnrEnrollmentCommissionClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EnrEnrollmentCommissionAddEditUI)
        {
            EnrEnrollmentCommissionAddEditUI addEditUI = (EnrEnrollmentCommissionAddEditUI) presenter;
            addEditUI.onClickApply();

            IUIAddon uiAddon = presenter.getConfig().getAddon(EnrEnrollmentCommissionAddEditExt.ADDON_NAME);
            if (null != uiAddon && (uiAddon instanceof EnrEnrollmentCommissionAddEditExtUI))
                    DataAccessServices.dao().saveOrUpdate(((EnrEnrollmentCommissionAddEditExtUI)uiAddon).getEnrollmentCommissionExt());

            addEditUI.deactivate();

        }
    }
}