package ru.tandemservice.uninarfu.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uninarfu.entity.gen.*;

/** @see ru.tandemservice.uninarfu.entity.gen.NarfuBSODisposalActVisaItemGen */
public class NarfuBSODisposalActVisaItem extends NarfuBSODisposalActVisaItemGen
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, NarfuBSODisposalActVisaItem.class)
                .order(NarfuBSODisposalActVisaItem.visaTemplate().title())
                .order(NarfuBSODisposalActVisaItem.priority())
                .filter(NarfuBSODisposalActVisaItem.visaTemplate().title())
                .pageable(false);
    }
}