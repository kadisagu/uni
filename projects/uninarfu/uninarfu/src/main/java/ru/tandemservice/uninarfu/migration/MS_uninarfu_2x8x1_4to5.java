package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.*;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x8x1_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность relationDegreeDeclinable

        // создана новая сущность
        if (!tool.tableExists("relationdegreedeclinable_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("relationdegreedeclinable_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_relationdegreedeclinable"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("parent_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("relationDegreeDeclinable");
        }

        Statement requestStatement = tool.getConnection().createStatement();
        requestStatement.execute("select id from relationDegree_t");

        ResultSet requestResult = requestStatement.getResultSet();
        short entityCode = tool.entityCodes().get("relationDegreeDeclinable");

        PreparedStatement insertRelSt = tool.getConnection().prepareStatement("insert into relationdegreedeclinable_t (id, discriminator, parent_id) values (?, ?, ?)");
        insertRelSt.setShort(2, entityCode);

        while (requestResult.next())
        {
            insertRelSt.setLong(1, EntityIDGenerator.generateNewId(entityCode));
            insertRelSt.setLong(3, requestResult.getLong(1));
            insertRelSt.execute();

        }
    }

}