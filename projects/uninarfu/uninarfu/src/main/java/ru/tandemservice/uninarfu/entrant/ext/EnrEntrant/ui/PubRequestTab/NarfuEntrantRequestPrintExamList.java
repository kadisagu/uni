/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.ui.AddEdit.EnrEnrollmentCampaignAddEdit;

/**
 * @author Ekaterina Zvereva
 * @since 28.06.2015
 */
public class NarfuEntrantRequestPrintExamList extends NamedUIAction
{
    public NarfuEntrantRequestPrintExamList(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        EnrScriptItem scriptItem = null;

        UIAddon addon = (UIAddon) presenter.getConfig().getAddon("EnrEntrantRequestActionsAddon");
        if (addon == null) return;

        if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_INTERNAL_EXAMS.equals(((EnrEntrantRequestActionsAddon) addon).getEntrant().getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET);
        }
        else if (EnrEnrollmentCampaignAddEdit.EXAM_LIST_PRINT_TYPE_ALL_EXAMS.equals(((EnrEntrantRequestActionsAddon) addon).getEntrant().getEnrollmentCampaign().getSettings().getEnrEntrantExamListPrintType()))
        {
            scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENROLLMENT_EXAM_SHEET_ALL_EXAMS);
        }
        else
        {
            throw new UnsupportedOperationException();
        }

        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, ((EnrEntrantRequestActionsAddon)addon).getEntrant().getId()));

        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, ((EnrEntrantRequestActionsAddon)addon).getEntrant().getId());

    }
}
