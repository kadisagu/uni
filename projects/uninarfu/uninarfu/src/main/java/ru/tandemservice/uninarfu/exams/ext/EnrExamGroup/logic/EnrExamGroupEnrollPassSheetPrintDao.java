/* $Id$ */
package ru.tandemservice.uninarfu.exams.ext.EnrExamGroup.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

/**
 * @author Andrey Andreev
 * @since 05.11.2015
 */
public class EnrExamGroupEnrollPassSheetPrintDao extends ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupEnrollPassSheetPrintDao
{
    @Override
    protected String[] writeEnrPassSheetMarksLine(EnrExamPassDiscipline disc)
    {
        String[] row = new String[5];
        row[1] = disc.getCode();
        row[2] = disc.getEntrant().getPerson().getFullFio();
        row[3] = disc.getAbsenceNote() != null ? disc.getAbsenceNote().getShortTitle() : disc.getMarkAsString();

        Long mark = disc.getMarkAsDouble() == null ? null : Math.round(disc.getMarkAsDouble());
        row[4] = mark == null ? "" : StringUtils.capitalize(NumberSpellingUtil.spellNumberMasculineGender(mark));

        return row;
    }
}