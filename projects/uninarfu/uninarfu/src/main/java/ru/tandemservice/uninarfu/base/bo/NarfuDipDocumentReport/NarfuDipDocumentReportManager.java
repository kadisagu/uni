/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.logic.INarfuDipDocumentReportDao;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.logic.NarfuDipDocumentReportDao;

/**
 * @author Andrey Avetisov
 * @since 21.04.2015
 */

@Configuration
public class NarfuDipDocumentReportManager extends BusinessObjectManager
{
    public static NarfuDipDocumentReportManager instance()
    {
        return instance(NarfuDipDocumentReportManager.class);
    }

    @Bean
    public INarfuDipDocumentReportDao dao()
    {
        return new NarfuDipDocumentReportDao();
    }
}
