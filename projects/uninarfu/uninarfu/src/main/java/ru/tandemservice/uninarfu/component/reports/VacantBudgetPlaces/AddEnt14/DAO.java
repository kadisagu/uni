package ru.tandemservice.uninarfu.component.reports.VacantBudgetPlaces.AddEnt14;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.*;
import ru.tandemservice.narfu.entity.NarfuReportVacantBudgetPlaces;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setCampaignsByCourseList(new ArrayList<>());
        model.setEnrollmentCampaignModel(new CampaignsWrapperSelectModel());
        model.setStudentStatusModel(new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StudentStatus.class, alias)
                        .order(StudentStatus.title().fromAlias(alias).s());
                if (!StringUtils.isEmpty(filter))
                    FilterUtils.applySimpleLikeFilter(builder, alias, StudentStatus.title(), filter);
                return builder;
            }
        });
        model.setStudentStatusList(createActiveStudentStatusList());
        model.setDevelopFormModel(new LazySimpleSelectModel<>(DevelopForm.class));
        model.setFormativeOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        prepareEnrollmentCampaigns(model);
    }

    @Override
    public void createReport(Model model)
    {
        IUniBaseDao.instance.get().doInTransaction(session -> {

            NarfuReportVacantBudgetPlaces report = new NarfuReportVacantBudgetPlaces();
            Integer number = getReportNumber();
            report.setNumber(number);
            Date date = new Date();
            report.setFormingDate(date);
            List<Long> formativeOrgUnitIds = CommonBaseEntityUtil.getIdList(model.getFormativeOrgUnitList());
            List<Long> studentStatusIds = CommonBaseEntityUtil.getIdList(model.getStudentStatusList());
            Long developFormId = model.getDevelopForm().getId();
            Multimap<Long, Long> campaignsByCourse = TreeMultimap.create();
            for (Model.CampaignsByCourse courseToCampaign : model.getCampaignsByCourseList()) {
                courseToCampaign.getCampaigns().forEach(campaignsWrapper ->
                        campaignsByCourse.put(courseToCampaign.getCourse().getId(), campaignsWrapper.getId()));
            }


            IScriptItem scriptItem = DataAccessServices.dao().getByCode(
                    EnrScriptItem.class, EnrScriptItemCodes.REPORT_NARFU_VACANT_BUDGET_PLACES_FOR_ENTRANT14);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                    IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                    "number", number,
                    "date", date,
                    "formativeOrgUnitIds", formativeOrgUnitIds,
                    "studentStatusIds", studentStatusIds,
                    "campaignsByCourse", campaignsByCourse,
                    "developFormId", developFormId);

            byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            report.setTitle(filename);

            //BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename + ".xls").document(document), false);

            DatabaseFile dbFile = new DatabaseFile();
            dbFile.setContent(document);
            dbFile.setFilename(filename + ".xls");
            dbFile.setContentType(CommonBaseUtil.getContentTypeByFileName(filename, DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL));
            session.save(dbFile);

            report.setContent(dbFile);
            session.save(report);

            return report.getId();
        });
    }

    private Integer getReportNumber()
    {
        String report_alias = "report";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(NarfuReportVacantBudgetPlaces.class, report_alias)
                .column(property(report_alias, NarfuReportVacantBudgetPlaces.number()))
                .order(property(report_alias, NarfuReportVacantBudgetPlaces.number()), OrderDirection.desc);
        List<Integer> numbersList = dql.createStatement(getSession()).setMaxResults(1).list();

        if (numbersList.size() > 0 && numbersList.get(0) != null) {
            return numbersList.get(0) + 1;
        } else {
            return 1;
        }
    }


    private List<StudentStatus> createActiveStudentStatusList()
    {
        String alias = "ss";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StudentStatus.class, alias)
                .where(eq(property(alias, StudentStatus.active()), value(true)));
        return getList(dql);
    }

    private void prepareEnrollmentCampaigns(Model model)
    {
        List<Course> courseList = getCourses();
        for (Course item : courseList) {
            Model.CampaignsByCourse c2c = new Model.CampaignsByCourse();
            c2c.setCourse(item);
            c2c.setCampaigns(new ArrayList<>());
            model.getCampaignsByCourseList().add(c2c);
        }
    }

    protected List<Course> getCourses()
    {
        //нам нужна только информация о 1-6 курсах :
        return DevelopGridDAO.getCourseMap().values().stream()
                .filter(c -> c.getIntValue() <= 6)
                .collect(Collectors.toList());
    }
}
