/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic;

import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.entity.EnrReportRatingList;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uninarfu.entity.NarfuEnrEntrantDailyList;
import ru.tandemservice.uninarfu.entity.NarfuEnrReportRegistrationJournal;
import ru.tandemservice.uninarfu.entity.NarfuEnrReportRequestCompetitionList;
import ru.tandemservice.uninarfu.entity.NarfuEnrReportRequestCount;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantDailyListAdd.NarfuEnrReportEntrantDailyListAddUI;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantRegistrationJournalAdd.NarfuEnrReportEntrantRegistrationJournalAddUI;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCompetitionListAdd.NarfuEnrReportRequestCompetitionListAddUI;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCountAdd.NarfuEnrReportRequestCountAddUI;

import java.util.Date;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 10.04.2015
 */
public class NarfuEnrReportDao extends UniBaseDao implements INarfuEnrReportDao
{
    @Override
    public Long createRequestCountReport(NarfuEnrReportRequestCountAddUI model)
    {
        NarfuEnrReportRequestCount report = new NarfuEnrReportRequestCount();
        report.setFormingDate(new Date());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setByAllOrgUnit(YesNoFormatter.INSTANCE.format(model.isByAllOrgUnit()));
        if (model.isEnrollmentCommissionActive())
        {
            report.setEnrollmentCommission(CommonBaseStringUtil.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title(), "; "));
        }

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, NarfuEnrReportRequestCount.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, NarfuEnrReportRequestCount.P_ENR_ORG_UNIT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, NarfuEnrReportRequestCount.P_FORMATIVE_ORG_UNIT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, NarfuEnrReportRequestCount.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, NarfuEnrReportRequestCount.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, NarfuEnrReportRequestCount.P_PROGRAM_SET, "title");


        IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_NARFU_REQUEST_COUNT);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);

        DatabaseFile content = new DatabaseFile();
        content.setContent((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));

        content.setFilename("EnrReportRequestCount.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }

    @Override
    public Long createRequestCompetitionListReport(NarfuEnrReportRequestCompetitionListAddUI model)
    {
        NarfuEnrReportRequestCompetitionList report = new NarfuEnrReportRequestCompetitionList();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());
        if (model.isEnrollmentCommissionActive())
        {
            report.setEnrollmentCommission(CommonBaseStringUtil.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title(), "; "));
        }


        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRatingList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportRatingList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRatingList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportRatingList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRatingList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRatingList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRatingList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRatingList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRatingList.P_PROGRAM_SET, "title");

        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }
        report.setIncludeTookAwayDocuments(model.isIncludeTookAwayDocuments() ? "Включены в отчет" : "Не включены в отчет");



        IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_NARFU_REQUEST_COMP_LIST);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);

        DatabaseFile content = new DatabaseFile();
        content.setContent((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        content.setFilename("EnrReportCompetitionList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    @Override
    public Long createEntrantRegistrationJournalReport(NarfuEnrReportEntrantRegistrationJournalAddUI model)
    {
        NarfuEnrReportRegistrationJournal report = new NarfuEnrReportRegistrationJournal();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());
        if (model.isEnrollmentCommissionActive())
        {
            report.setEnrollmentCommission(CommonBaseStringUtil.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title(), "; "));
        }
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRatingList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportRatingList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRatingList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportRatingList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRatingList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRatingList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRatingList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRatingList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRatingList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }
        report.setIncludeTookAwayDocuments(model.isIncludeTookAwayDocuments() ? "Включены в отчет" : "Не включены в отчет");


        IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_NARFU_ENR_REG_JOURNAL);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);

        DatabaseFile content = new DatabaseFile();
        content.setContent((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        content.setFilename("EnrReportRegistrationReport.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);
        report.setContent(content);
        save(report);

        return report.getId();
    }

    @Override
    public Long createEntrantDailyListReport(NarfuEnrReportEntrantDailyListAddUI model)
    {
        NarfuEnrEntrantDailyList report = new NarfuEnrEntrantDailyList();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());
        if (model.isEnrollmentCommissionActive())
        {
            report.setEnrollmentCommission(CommonBaseStringUtil.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title(), "; "));
        }
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EnrReportRatingList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EnrReportRatingList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EnrReportRatingList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EnrReportRatingList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EnrReportRatingList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EnrReportRatingList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EnrReportRatingList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EnrReportRatingList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EnrReportRatingList.P_PROGRAM_SET, "title");
        if (model.isParallelActive()) {
            report.setParallel(model.getParallel().getTitle());
        }
        report.setIncludeTookAwayDocuments(model.isIncludeTookAwayDocuments() ? "Включены в отчет" : "Не включены в отчет");


        IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.REPORT_NARFU_ENR_DAILY_ENTRANT_LIST);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);

        DatabaseFile content = new DatabaseFile();
        content.setContent((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        content.setFilename("EnrReportDailyEntrantList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);
        report.setContent(content);
        save(report);

        return report.getId();
    }


}
