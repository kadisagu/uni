/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.SecondaryProfAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfAddEdit.EppEduPlanSecondaryProfAddEditUI;


/**
 * @author Ekaterina Zvereva
 * @since 21.04.2015
 */
public class EppEduPlanSecondaryProfClickApply extends NamedUIAction
{
    protected EppEduPlanSecondaryProfClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EppEduPlanSecondaryProfAddEditUI)
        {
            EppEduPlanSecondaryProfAddEditUI addEditUI = (EppEduPlanSecondaryProfAddEditUI) presenter;
            addEditUI.onClickApply();


            IUIAddon uiAddon = presenter.getConfig().getAddon(EppEduPlanSecondaryProfAddEditExt.ADDONE_NAME);
            if (null != uiAddon)
            {
                if (uiAddon instanceof EppEduPlanSecondaryProfAddEditExtUI)
                {
                    EppEduPlanSecondaryProfAddEditExtUI addon = (EppEduPlanSecondaryProfAddEditExtUI) uiAddon;
                    if (addon.getRelObject().getEducationLevelsHighSchool() != null)
                        DataAccessServices.dao().saveOrUpdate(addon.getRelObject());
                }
            }

            addEditUI.deactivate();
        }
    }
}
