package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Номер подразделения для использования в документах абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OrgUnitNumberForEnrEntrantDocumentsGen extends EntityBase
 implements INaturalIdentifiable<OrgUnitNumberForEnrEntrantDocumentsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments";
    public static final String ENTITY_NAME = "orgUnitNumberForEnrEntrantDocuments";
    public static final int VERSION_HASH = -1615381718;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_NUMBER = "number";

    private OrgUnit _orgUnit;     // Формирующее подразделение
    private String _number;     // Номер

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Формирующее подразделение. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OrgUnitNumberForEnrEntrantDocumentsGen)
        {
            if (withNaturalIdProperties)
            {
                setOrgUnit(((OrgUnitNumberForEnrEntrantDocuments)another).getOrgUnit());
            }
            setNumber(((OrgUnitNumberForEnrEntrantDocuments)another).getNumber());
        }
    }

    public INaturalId<OrgUnitNumberForEnrEntrantDocumentsGen> getNaturalId()
    {
        return new NaturalId(getOrgUnit());
    }

    public static class NaturalId extends NaturalIdBase<OrgUnitNumberForEnrEntrantDocumentsGen>
    {
        private static final String PROXY_NAME = "OrgUnitNumberForEnrEntrantDocumentsNaturalProxy";

        private Long _orgUnit;

        public NaturalId()
        {}

        public NaturalId(OrgUnit orgUnit)
        {
            _orgUnit = ((IEntity) orgUnit).getId();
        }

        public Long getOrgUnit()
        {
            return _orgUnit;
        }

        public void setOrgUnit(Long orgUnit)
        {
            _orgUnit = orgUnit;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OrgUnitNumberForEnrEntrantDocumentsGen.NaturalId) ) return false;

            OrgUnitNumberForEnrEntrantDocumentsGen.NaturalId that = (NaturalId) o;

            if( !equals(getOrgUnit(), that.getOrgUnit()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOrgUnit());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOrgUnit());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OrgUnitNumberForEnrEntrantDocumentsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OrgUnitNumberForEnrEntrantDocuments.class;
        }

        public T newInstance()
        {
            return (T) new OrgUnitNumberForEnrEntrantDocuments();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "number":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OrgUnitNumberForEnrEntrantDocuments> _dslPath = new Path<OrgUnitNumberForEnrEntrantDocuments>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OrgUnitNumberForEnrEntrantDocuments");
    }
            

    /**
     * @return Формирующее подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends OrgUnitNumberForEnrEntrantDocuments> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(OrgUnitNumberForEnrEntrantDocumentsGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return OrgUnitNumberForEnrEntrantDocuments.class;
        }

        public String getEntityName()
        {
            return "orgUnitNumberForEnrEntrantDocuments";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
