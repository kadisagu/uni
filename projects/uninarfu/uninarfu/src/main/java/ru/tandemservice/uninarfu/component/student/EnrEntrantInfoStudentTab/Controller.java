package ru.tandemservice.uninarfu.component.student.EnrEntrantInfoStudentTab;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.remoteDocument.dto.RemoteDocumentDTO;
import org.tandemframework.shared.commonbase.remoteDocument.service.ICommonRemoteDocumentService;
import org.tandemframework.shared.commonbase.remoteDocument.service.IRemoteDocumentProvider;
import org.tandemframework.shared.person.remoteDocument.ext.RemoteDocument.RemoteDocumentExtManager;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;


public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        final Student student = ((ru.tandemservice.uni.component.student.StudentPub.Model) component.getParentComponent().getModel()).getStudent();
        model.setStudent(student);
        getDao().prepare(model);

        model.setStateExamResultDS(createEntrantStateExamResultDS(component));
        model.setOlympiadDiplomaDS(createOlympiadDiplomaDS(component));
        model.setDisabledScanCopyHandler(entity -> ((EnrOlympiadDiploma) entity).getScanCopy() == null);

    }


    //Listeners
    @Override
    public void onDeactivateComponent(IBusinessComponent component)
    {
        super.onDeactivateComponent(component);
        getModel(component).setStudent(null);
    }

    public void onChangeEnrollmentCampaign(IBusinessComponent component)
    {
        getDao().prepareRatingData(getModel(component));
    }

    private DynamicListDataSource<EnrEntrantStateExamResult> createEntrantStateExamResultDS(IBusinessComponent component)
    {
        DynamicListDataSource<EnrEntrantStateExamResult> dataSource = new DynamicListDataSource<>(component, comp -> {
            getDao().prepareStateExamResultDS(comp.getModel());
        });

        dataSource.addColumn(
                new SimpleColumn("Год", "year")
                {
                    @Override
                    public String getContent(IEntity entity)
                    {
                        return String.valueOf(((EnrEntrantStateExamResult) entity).getYear());
                    }
                }.setClickable(false));
        dataSource.addColumn(new SimpleColumn("Предмет", EnrEntrantStateExamResult.subject().title().s()).setClickable(false));
        dataSource.addColumn(
                new SimpleColumn("Балл", EnrEntrantStateExamResult.mark())
                {
                    @Override
                    public String getContent(IEntity entity)
                    {
                        return String.valueOf(((EnrEntrantStateExamResult) entity).getMark());
                    }
                }
                        .setClickable(false)
                        .setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Балл выше порогового", EnrEntrantStateExamResult.abovePassingMark()));
        dataSource.addColumn(new SimpleColumn("Свидетельство", EnrEntrantStateExamResult.documentNumber()).setClickable(false));
        dataSource.addColumn(new DateColumn("Дата выдачи", EnrEntrantStateExamResult.documentIssuanceDate()).setClickable(false));
        dataSource.addColumn(
                new SimpleColumn("Статус проверки в ФИС", EnrEntrantStateExamResult.stateCheckStatus())
                        .setClickable(false)
                        .setOrderable(false));
        dataSource.addColumn(new BooleanColumn("В доп. срок", EnrEntrantStateExamResult.stateCheckStatus()).setClickable(false));
        dataSource.addColumn(
                new SimpleColumn("Пункт проведения", EnrEntrantStateExamResult.secondWaveExamPlace().title().s())
                        .setClickable(false));
        dataSource.addColumn(
                new ToggleColumn("Зачтен", EnrEntrantStateExamResult.accepted())
                        .setClickable(false)
                        .setOrderable(false));
        dataSource.addColumn(
                new ToggleColumn("Проверен вручную", EnrEntrantStateExamResult.verifiedByUser()).
                        setClickable(false)
                        .setOrderable(false));
        dataSource.addColumn(
                new SimpleColumn("Комментарий к проверке вручную", EnrEntrantStateExamResult.verifiedByUserComment())
                        .setClickable(false)
                        .setOrderable(false));

        return dataSource;
    }

    private DynamicListDataSource<EnrOlympiadDiploma> createOlympiadDiplomaDS(IBusinessComponent component)
    {
        DynamicListDataSource<EnrOlympiadDiploma> dataSource = new DynamicListDataSource<>(component, comp -> {
            getDao().prepareOlympiadDiplomaDS(comp.getModel());
        });

        dataSource.addColumn(new SimpleColumn("Тип олимпиады", EnrOlympiadDiploma.olympiad().olympiadType().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Степень отличия", EnrOlympiadDiploma.honour().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Олимпиада", EnrOlympiadDiploma.olympiad().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Предмет", EnrOlympiadDiploma.subject().title().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Серия и номер", EnrOlympiadDiploma.seriaAndNumber().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new DateColumn("Дата выдачи", EnrOlympiadDiploma.issuanceDate().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Место проведения", EnrOlympiadDiploma.settlement().title().s()).setClickable(false));
        dataSource.addColumn(new DateColumn("Дата добавления", EnrOlympiadDiploma.registrationDate().s()).setClickable(false));
        dataSource.addColumn(
                new ActionColumn("Скан-копия", "printer", "onClickDownloadScanCopy")
                        .setOrderable(false)
                        .setDisableHandler(getModel(component).getDisabledScanCopyHandler()).setOrderable(false));

        return dataSource;
    }


    public void onClickDownloadScanCopy(IBusinessComponent component)
    {
        EnrOlympiadDiploma diploma = DataAccessServices.dao().get(component.<Long>getListenerParameter());
        Long scanCopyId = diploma.getScanCopy();
        if (scanCopyId != null) {
            try (ICommonRemoteDocumentService service = IRemoteDocumentProvider.instance.get()
                    .taskService(RemoteDocumentExtManager.PERSON_DOCUMENTS_SERVICE_NAME)) {
                RemoteDocumentDTO scanCopy = service.get(scanCopyId);
                if (scanCopy != null) {
                    BusinessComponentUtils.downloadDocument(
                            new CommonBaseRenderer()
                                    .contentType(scanCopy.getFileType())
                                    .fileName(scanCopy.getFileName())
                                    .document(scanCopy.getContent()), true
                    );
                }
            }
        }
    }

    public void onClickRefresh(IBusinessComponent component)
    {
        EnrEntrantDaemonBean.DAEMON.wakeUpAndWaitDaemon(60);
        component.getDesktop().setRefreshScheduled(true);
    }


}
