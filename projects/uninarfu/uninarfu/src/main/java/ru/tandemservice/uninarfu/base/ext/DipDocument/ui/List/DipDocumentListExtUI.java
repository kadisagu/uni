package ru.tandemservice.uninarfu.base.ext.DipDocument.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentrmc.entity.ExtractTextRelation;
import ru.tandemservice.movestudentrmc.entity.IAbstractOrder;
import ru.tandemservice.movestudentrmc.entity.ListOrdListRepresent;
import ru.tandemservice.movestudentrmc.entity.RelListRepresentStudents;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.List.DipDocumentListUI;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Andreev
 * @since 15.10.2015
 */
public class DipDocumentListExtUI extends UIAddon
{
    private IAbstractOrder _order;
    private boolean hasOrder;

    private Long _studentId;
    private Student _student;

    DipDocumentListUI _presenter;

    public DipDocumentListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
        _presenter = (DipDocumentListUI) presenter;
    }

    @Override
    public void onComponentRefresh()
    {
        _student = _presenter.getStudent();

        String etr_alias = "etr";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ExtractTextRelation.class, etr_alias)
                .column(property(etr_alias, ExtractTextRelation.order().order()))
                .where(eq(property(etr_alias, ExtractTextRelation.student().id()), value(_student.getId())))
                .where(eq(property(etr_alias, ExtractTextRelation.order().representation().type().code()),
                        value(RepresentationTypeCodes.DIPLOM_AND_EXCLUDE)))
                .where(eq(property(etr_alias, ExtractTextRelation.order().order().state().code()),
                        value(MovestudentOrderStatesCodes.CODE_5)));

        List<IAbstractOrder> orders = DataAccessServices.dao().getList(dql);


        String lolr_alias = "lolr";
        String rlrs_alias = "rlrs";
        dql = new DQLSelectBuilder().fromEntity(ListOrdListRepresent.class, lolr_alias)
                .column(property(lolr_alias, ListOrdListRepresent.order()))
                .where(eq(property(lolr_alias, ListOrdListRepresent.representation().type().code()),
                        value(RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST)))
                .where(eq(property(lolr_alias, ListOrdListRepresent.order().state().code()),
                        value(MovestudentOrderStatesCodes.CODE_5)))
                .joinEntity(lolr_alias, DQLJoinType.inner, RelListRepresentStudents.class, rlrs_alias,
                        eq(property(RelListRepresentStudents.representation().fromAlias(rlrs_alias)),
                                property(ListOrdListRepresent.representation().fromAlias(lolr_alias))))
                .where(eq(property(RelListRepresentStudents.student().id().fromAlias(rlrs_alias)), value(_student.getId())));

        orders.addAll(DataAccessServices.dao().getList(dql));
        hasOrder = !orders.isEmpty();
        if (hasOrder) {
            orders.sort((o1, o2) -> o2.getCommitDate().compareTo(o1.getCommitDate()));
            setOrder(orders.get(0));
        }
    }

    //Getters & setters

    public boolean isHasOrder()
    {
        return hasOrder;
    }

    public IAbstractOrder getOrder()
    {
        return _order;
    }

    public void setOrder(IAbstractOrder order)
    {
        this._order = order;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        this._student = student;
    }

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        this._studentId = studentId;
    }

}