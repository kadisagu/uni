/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder.ui.PrintExtract;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.MultiValuesColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.order.entity.*;
import ru.tandemservice.uninarfu.base.bo.NarfuEnrOrder.NarfuEnrOrderManager;

import java.util.*;

/**
 * @author Ekaterina Zvereva
 * @since 07.08.2015
 */
@Input({
        @Bind(key = "orderId", binding = "orderId", required = true)
})
public class NarfuEnrOrderPrintExtractUI extends UIPresenter
{
    private DynamicListDataSource _dataSource;
    private Long _orderId;
    private EnrOrder _order;

    @Override
    public void onComponentRefresh()
    {
        _order = DataAccessServices.dao().get(EnrOrder.class, getOrderId());

        switch (getOrder().getType().getCode()) {
            case (EnrOrderTypeCodes.ENROLLMENT): prepareEnrollmentParagraphDataSource(); break;
            case (EnrOrderTypeCodes.ENROLLMENT_MIN): prepareEnrollmentMinisteryParagraphDataSource(); break;
        }

        prepareEnrollmentParagraphDataSource();
    }

    private void prepareEnrollmentParagraphDataSource()
    {
        DynamicListDataSource dataSource = new DynamicListDataSource(this, component -> {
            List<EnrEnrollmentParagraph> paragraphList = new ArrayList<>();
            Map<EnrEnrollmentParagraph, List<EnrEnrollmentExtract>> map = new HashMap<>();

            for (EnrEnrollmentExtract extract : IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph().order(), getOrder())) {
                EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) extract.getParagraph();
                List<EnrEnrollmentExtract> extractList = map.get(paragraph);
                if (extractList == null)
                {
                    paragraphList.add(paragraph);
                    map.put(paragraph, extractList = new ArrayList<>());
                }
                extractList.add(extract);
            }

            Collections.sort(paragraphList);

            getDataSource().setCountRow(paragraphList.size());
            UniBaseUtils.createPage(getDataSource(), paragraphList);

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(getDataSource()))
            {
                EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) viewWrapper.getEntity();
                List<EnrEnrollmentExtract> extractList = map.get(paragraph);
                if (null == extractList || extractList.isEmpty()) {
                    viewWrapper.setViewProperty("competition", null);
                    viewWrapper.setViewProperty("extractCount", 0);
                    continue;
                }

                List<EnrCompetition> competitions = new ArrayList<>();
                for (EnrEnrollmentExtract extract : extractList)
                    if (!competitions.contains(extract.getEntity().getCompetition()))
                        competitions.add(extract.getEntity().getCompetition());

                Collections.sort(competitions, ITitled.TITLED_COMPARATOR);
                viewWrapper.setViewProperty("competition", competitions);
                viewWrapper.setViewProperty("extractCount", extractList.size());
            }
        });

        dataSource.addColumn(new org.tandemframework.core.view.list.column.CheckboxColumn("selected"));
        dataSource.addColumn(new SimpleColumn("Название", "title").setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Конкурс", "title", "competition").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setWidth("270px;").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во выписок", "extractCount").setClickable(false).setOrderable(false));

        setDataSource(dataSource);
    }


    private void prepareEnrollmentMinisteryParagraphDataSource()
    {
        DynamicListDataSource dataSource = new DynamicListDataSource(this, component -> {
            List<EnrEnrollmentMinisteryParagraph> paragraphList = new ArrayList<>();
            Map<EnrEnrollmentMinisteryParagraph, List<EnrEnrollmentMinisteryExtract>> map = new HashMap<>();

            for (EnrEnrollmentMinisteryExtract extract : IUniBaseDao.instance.get().getList(EnrEnrollmentMinisteryExtract.class, EnrEnrollmentMinisteryExtract.paragraph().order(), getOrder())) {
                EnrEnrollmentMinisteryParagraph paragraph = (EnrEnrollmentMinisteryParagraph) extract.getParagraph();
                List<EnrEnrollmentMinisteryExtract> extractList = map.get(paragraph);
                if (extractList == null)
                {
                    paragraphList.add(paragraph);
                    map.put(paragraph, extractList = new ArrayList<>());
                }
                extractList.add(extract);
            }

            Collections.sort(paragraphList);

            getDataSource().setCountRow(paragraphList.size());
            UniBaseUtils.createPage(getDataSource(), paragraphList);

            for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(getDataSource()))
            {
                EnrEnrollmentMinisteryParagraph paragraph = (EnrEnrollmentMinisteryParagraph) viewWrapper.getEntity();
                List<EnrEnrollmentMinisteryExtract> extractList = map.get(paragraph);
                if (null == extractList || extractList.isEmpty()) {
                    viewWrapper.setViewProperty("competition", null);
                    viewWrapper.setViewProperty("extractCount", 0);
                    continue;
                }

                List<EnrCompetition> competitions = new ArrayList<>();
                for (EnrEnrollmentMinisteryExtract extract : extractList)
                    if (!competitions.contains(extract.getEntity().getCompetition()))
                        competitions.add(extract.getEntity().getCompetition());

                Collections.sort(competitions, ITitled.TITLED_COMPARATOR);
                viewWrapper.setViewProperty("competition", competitions);
                viewWrapper.setViewProperty("extractCount", extractList.size());
            }
        });


        dataSource.addColumn(new CheckboxColumn("checked", "Выбрать"));
        dataSource.addColumn(new SimpleColumn("Название", "title").setFormatter(NoWrapFormatter.INSTANCE).setOrderable(false));
        dataSource.addColumn(new MultiValuesColumn("Конкурс", "title", "competition").setFormatter(CollectionFormatter.COLLECTION_FORMATTER).setWidth("270px;").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во выписок", "extractCount").setClickable(false).setOrderable(false));
        setDataSource(dataSource);
    }


    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public EnrOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrOrder order)
    {
        _order = order;
    }

    public void onClickPrintExtracts()
    {
        List<Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) getDataSource().getColumn("selected")).getSelectedObjects());

        final byte[] document = NarfuEnrOrderManager.instance().dao().printEnrOrderExtracts(selectedIds);

        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(document)
                        .fileName("EnrollmentExtractList.rtf")
                        .rtf(),
                false);

    }
}