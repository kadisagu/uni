/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.Pub.PersonEduDocumentPubUI;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
public class PersonEduDocumentPubUIExt extends UIAddon {

    private PersonEduDocumentNARFU documentNarfu;

    public PersonEduDocumentPubUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }


    @Override
    public void onComponentRefresh() {
        documentNarfu = DataAccessServices.dao().get(PersonEduDocumentNARFU.class, PersonEduDocumentNARFU.eduDocument(), getDocument());
        if (documentNarfu == null)
            documentNarfu = new PersonEduDocumentNARFU();
    }

    //getters/setters
    private PersonEduDocumentPubUI getParentPresenter() {
        return getPresenter();
    }

    private PersonEduDocument getDocument() {
        return getParentPresenter().getEduDocument();
    }

    public PersonEduDocumentNARFU getDocumentNarfu() {
        return documentNarfu;
    }
}
