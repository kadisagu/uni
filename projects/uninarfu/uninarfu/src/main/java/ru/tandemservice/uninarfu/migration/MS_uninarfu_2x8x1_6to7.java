/*$Id$*/
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2015
 */
public class MS_uninarfu_2x8x1_6to7 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.1"),
                };
    }

    @Override
    public ScriptDependency[] getAfterDependencies()
    {
        return new ScriptDependency[]{
                new ScriptDependency("uniedu", "2.8.1", 1),
                new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // создать колонку
        if (!tool.columnExists("edu_c_pr_subject_index_t", "edulevelandindexnotation_p"))
        {
            tool.createColumn("edu_c_pr_subject_index_t", new DBColumn("edulevelandindexnotation_p", DBType.createVarchar(255)));
        }

        if (!tool.columnExists("edu_c_pr_subject_index_t", "eduprogramsubjecttype_p"))
        {
            tool.createColumn("edu_c_pr_subject_index_t", new DBColumn("eduprogramsubjecttype_p", DBType.createVarchar(255)));
        }
        //ВО аспирантуре (2013)
        tool.executeUpdate("update edu_c_pr_subject_index_t set edulevelandindexnotation_p=?, eduprogramsubjecttype_p=? where code_p=?", "ВО ФГОС 2013", "направление подготовки в аспирантуре", "2013.96");
    }
}
