/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuBSODisposalAct.ui.VisaTemplateAddEdit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.SimpleListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem;
import ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 21.03.2016
 */
@Input({@Bind(key = "visaTemplateId", binding = "visaTemplateId")})
public class NarfuBSODisposalActVisaTemplateAddEditUI extends UIPresenter
{
    private Long _visaTemplateId;
    private NarfuBSOVisasTemplate _visaTemplate = new NarfuBSOVisasTemplate();

    private List<NarfuBSODisposalActVisaItem> _deletedVisaList = new ArrayList<>();
    private NarfuBSODisposalActVisaItem _currentVisa;

    private SimpleListDataSource<DataWrapper> _visaItemDS = new SimpleListDataSource<>();

    private ArrayList<DataWrapper> _visaList = new ArrayList<>();
    private static final Comparator<DataWrapper> WRAPPER_COMPARATOR = (o1, o2) -> {
        NarfuBSODisposalActVisaItem item1 = o1.getWrapped();
        NarfuBSODisposalActVisaItem item2 = o2.getWrapped();
        return Integer.compare(item1.getPriority(), item2.getPriority());
    };

    @Override

    public void onComponentRefresh()
    {
        if (_visaTemplateId != null)
        {
            _visaTemplate = DataAccessServices.dao().getNotNull(NarfuBSOVisasTemplate.class, _visaTemplateId);

            final long[] id = {0};
            if (_visaTemplate != null)
                _visaList = DataAccessServices.dao().getList(NarfuBSODisposalActVisaItem.class, NarfuBSODisposalActVisaItem.visaTemplate(), _visaTemplate)
                        .stream()
                        .map(item -> wrapItem(++id[0], item))
                        .sorted(WRAPPER_COMPARATOR)
                        .collect(Collectors.toCollection(ArrayList<DataWrapper>::new));

            _visaItemDS.setEntityCollection(_visaList);
            _visaItemDS.setCountRow(_visaList.size() + 1);
        }
        else
        {
            _visaTemplate = new NarfuBSOVisasTemplate();
        }
    }

    @Override
    public void onComponentActivate()
    {
        _visaItemDS = new SimpleListDataSource<>();

        SimpleColumn fio = new SimpleColumn(getConfig().getProperty("ui.visaList.fio"), NarfuBSODisposalActVisaItem.possibleVisa().entity().person().fullFio().s());
        fio.setOrderable(false);
        _visaItemDS.addColumn(fio);

        SimpleColumn title = new SimpleColumn(getConfig().getProperty("ui.visaList.title"), NarfuBSODisposalActVisaItem.possibleVisa().title());
        title.setOrderable(false);
        _visaItemDS.addColumn(title);

        ActionColumn upCol = new ActionColumn("", "up", "onPriorityUp");
        upCol.setParametersResolver((entity, valueEntity) -> entity);
        _visaItemDS.addColumn(upCol);

        ActionColumn downCol = new ActionColumn("", "down", "onPriorityDown");
        downCol.setParametersResolver((entity, valueEntity) -> entity);
        _visaItemDS.addColumn(downCol);

        ActionColumn deleteCol = new ActionColumn("deleteItem", ActionColumn.DELETE, "onDeleteItem");
        deleteCol.setAlertWithParameters(FormattedMessage.with()
                                                 .template(getConfig().getProperty("visaItemDS.delete.alert"))
                                                 .parameter(NarfuBSODisposalActVisaItem.possibleVisa().entity().person().fullFio())
                                                 .parameter(NarfuBSODisposalActVisaItem.possibleVisa().title())
                                                 .create());
        deleteCol.setParametersResolver((entity, valueEntity) -> entity);
        _visaItemDS.addColumn(deleteCol);

        _visaItemDS.setOrder(new SimpleColumn("", NarfuBSODisposalActVisaItem.P_PRIORITY), OrderDirection.asc);
        _visaItemDS.setCountRow(1);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(NarfuBSODisposalActVisaTemplateAddEdit.POSSIBLE_VISA_SELECT_DS))
        {
            List<Long> ids = _visaList.stream()
                    .map(wrapper -> (EmployeePostPossibleVisa) wrapper.getProperty(NarfuBSODisposalActVisaItem.possibleVisa()))
                    .filter(visa -> visa.getId() != null)
                    .map(IIdentifiable::getId)
                    .collect(Collectors.toList());
            dataSource.put(NarfuBSODisposalActVisaTemplateAddEdit.PROPERTY_USED_VISAS_ID, ids);
        }
    }

    // Listeners
    public void onPriorityUp()
    {
        final DataWrapper targetW = getListenerParameter();
        int index = _visaList.indexOf(targetW);
        if (index > 0)
            changePriority(_visaList.get(index - 1).getWrapped(), targetW.getWrapped());
    }

    public void onPriorityDown()
    {
        final DataWrapper targetW = getListenerParameter();
        int index = _visaList.indexOf(targetW);
        if (index < _visaList.size() - 1)
            changePriority(_visaList.get(index + 1).getWrapped(), targetW.getWrapped());
    }

    private void changePriority(NarfuBSODisposalActVisaItem item1, NarfuBSODisposalActVisaItem item2)
    {
        int priority = item1.getPriority();
        item1.setPriority(item2.getPriority());
        item2.setPriority(priority);
        normalizePriority(_visaList);
    }

    public void onDeleteItem()
    {
        DataWrapper wrapper = getListenerParameter();

        NarfuBSODisposalActVisaItem item = wrapper.<NarfuBSODisposalActVisaItem>getWrapped();
        if (item.getId() != null)
            _deletedVisaList.add(item);

        _visaList.remove(wrapper);
        normalizePriority(_visaList);
    }

    public void onClickAddRow()
    {
        EmployeePostPossibleVisa possibleVisa = getSettings().get("possibleVisa", EmployeePostPossibleVisa.class);
        if (possibleVisa == null) return;

        NarfuBSODisposalActVisaItem newItem = new NarfuBSODisposalActVisaItem();
        newItem.setPossibleVisa(possibleVisa);
        newItem.setVisaTemplate(_visaTemplate);

        final int[] priority = {1};
        final long[] id = {1};
        _visaList.forEach(w -> {
            if (w.getId() >= id[0])
                id[0] = w.getId() + 1;

            NarfuBSODisposalActVisaItem item = w.<NarfuBSODisposalActVisaItem>getWrapped();
            if (item.getPriority() >= priority[0])
                priority[0] = item.getPriority() + 1;
        });
        newItem.setPriority(priority[0]);

        _visaList.add(wrapItem(id[0], newItem));
        _visaItemDS.setEntityCollection(_visaList);
        _visaItemDS.setCountRow(_visaList.size() + 1);
    }

    public void onClickApply()
    {
        ICommonDAO dao = DataAccessServices.dao();

        if (!_visaList.isEmpty()) {
            dao.saveOrUpdate(_visaTemplate);

            _deletedVisaList.forEach(dao::delete);
            dao.executeFlush();

            _visaList.forEach(wrapper -> dao.saveOrUpdate(wrapper.<NarfuBSODisposalActVisaItem>getWrapped()));
        }
        else if (_visaTemplate.getId() != null)
            dao.delete(_visaTemplate);

        deactivate();
    }

    //Getters and Setters
    public NarfuBSODisposalActVisaItem getCurrentVisa()
    {
        return _currentVisa;
    }

    public void setCurrentVisa(NarfuBSODisposalActVisaItem currentVisa)
    {
        _currentVisa = currentVisa;
    }

    public NarfuBSOVisasTemplate getVisaTemplate()
    {
        return _visaTemplate;
    }

    public void setVisaTemplate(NarfuBSOVisasTemplate visaTemplate)
    {
        _visaTemplate = visaTemplate;
    }

    public Long getVisaTemplateId()
    {
        return _visaTemplateId;
    }

    public void setVisaTemplateId(Long visaTemplateId)
    {
        _visaTemplateId = visaTemplateId;
    }

    public SimpleListDataSource<DataWrapper> getVisaItemDS()
    {
        return _visaItemDS;
    }

    public boolean isEditMode()
    {
        return _visaTemplateId != null;
    }

    public String getSticker()
    {
        if (isEditMode())
            return getConfig().getProperty("ui.sticker.edit");
        else
            return getConfig().getProperty("ui.sticker.add");
    }


    private DataWrapper wrapItem(long id, NarfuBSODisposalActVisaItem item)
    {
        return new DataWrapper(id, item.getPossibleVisa().getPossibleVisaFullTitle(), item);
    }

    private void normalizePriority(List<DataWrapper> _visaList)
    {
        final int[] priority = {0};
        _visaList.sort(WRAPPER_COMPARATOR);
        _visaList.stream().forEach(w -> w.setProperty(NarfuBSODisposalActVisaItem.P_PRIORITY, ++priority[0]));
        _visaItemDS.setEntityCollection(_visaList);
        _visaItemDS.setCountRow(_visaList.size() + 1);
    }
}