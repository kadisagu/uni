package ru.tandemservice.uninarfu.entity;

import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests;
import ru.tandemservice.uninarfu.entity.gen.*;

/** @see ru.tandemservice.uninarfu.entity.gen.EntrantSpecConditionRelationGen */
public class EntrantSpecConditionRelation extends EntrantSpecConditionRelationGen
{
    public EntrantSpecConditionRelation() {}

    public EntrantSpecConditionRelation(EnrEntrant entrant, NarfuSpecConditionsEntTests specCondition)
    {
        setEnrEntrant(entrant);
        setSpecCondition(specCondition);
    }
}