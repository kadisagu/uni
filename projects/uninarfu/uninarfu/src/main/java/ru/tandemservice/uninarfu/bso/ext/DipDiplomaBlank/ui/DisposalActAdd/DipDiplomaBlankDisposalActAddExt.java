/* $Id$ */
package ru.tandemservice.uninarfu.bso.ext.DipDiplomaBlank.ui.DisposalActAdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalActAdd.DipDiplomaBlankDisposalActAdd;
import ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate;

/**
 * @author Andrey Andreev
 * @since 23.03.2016
 */
@Configuration
public class DipDiplomaBlankDisposalActAddExt extends BusinessComponentExtensionManager
{

    public static final String ADDON = "narfu_" + DipDiplomaBlankDisposalActAddUIExt.class.getSimpleName();

    public static final String VISA_TEMPLATE_DS = "visaTemplateDS";
    public static final String EMPLOYEE_DS = "employeeDS";

    @Autowired
    private DipDiplomaBlankDisposalActAdd _dipDiplomaBlankDisposalActAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_dipDiplomaBlankDisposalActAdd.presenterExtPoint())
                .addAddon(uiAddon(ADDON, DipDiplomaBlankDisposalActAddUIExt.class))
                .addDataSource(selectDS(VISA_TEMPLATE_DS, selectVisasTemplateDSHandler()))
                .addDataSource(selectDS(EMPLOYEE_DS, selectEmployeeDSHandler()))
                .create();
    }

    @Bean
    public EntityComboDataSourceHandler selectEmployeeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Employee.class)
                .order(Employee.person().identityCard().fullFio())
                .filter(Employee.person().identityCard().fullFio());
    }

    @Bean
    public EntityComboDataSourceHandler selectVisasTemplateDSHandler()
    {
        return NarfuBSOVisasTemplate.defaultSelectDSHandler(getName());
    }
}