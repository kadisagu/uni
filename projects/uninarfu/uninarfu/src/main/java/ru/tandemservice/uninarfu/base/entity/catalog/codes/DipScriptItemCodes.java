package ru.tandemservice.uninarfu.base.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные шаблоны и скрипты модуля «Дипломирование»"
 * Имя сущности : dipScriptItem
 * Файл data.xml : uninarfu.data.xml
 */
public interface DipScriptItemCodes
{
    /** Константа кода (code) элемента : Акт списания бланков дипломов и приложений (title) */
    String BLANK_DISPOSAL_ACT = "blankDisposalAct";
    /** Константа кода (code) элемента : Сводный отчет по бланкам дипломов и приложений (title) */
    String DIPLOMA_BLANK_REPORT = "diplomaBlankReport";
    /** Константа кода (code) элемента : Сводный отчет по бланкам дипломов и приложений (по типу) (title) */
    String DIPLOMA_BLANK_REPORT_TYPE = "diplomaBlankReportType";
    /** Константа кода (code) элемента : Сводный отчет по бланкам дипломов и приложений (по месту хранения) (title) */
    String DIPLOMA_BLANK_REPORT_STORAGE_LOCATION = "diplomaBlankReportStorageLocation";
    /** Константа кода (code) элемента : Отчет «Книга регистрации выданных документов об образовании» (title) */
    String NARFU_DIP_ISSUANCE_REG_BOOK_REPORT = "report.narfuDipIssuanceBook";
    /** Константа кода (code) элемента : Отчет «Ведомость сверки данных по выпускникам» (title) */
    String NARFU_CHECKING_GRADUATES_DATA = "report.narfuCheckingGraduatesData";

    Set<String> CODES = ImmutableSet.of(BLANK_DISPOSAL_ACT, DIPLOMA_BLANK_REPORT, DIPLOMA_BLANK_REPORT_TYPE, DIPLOMA_BLANK_REPORT_STORAGE_LOCATION, NARFU_DIP_ISSUANCE_REG_BOOK_REPORT, NARFU_CHECKING_GRADUATES_DATA);
}
