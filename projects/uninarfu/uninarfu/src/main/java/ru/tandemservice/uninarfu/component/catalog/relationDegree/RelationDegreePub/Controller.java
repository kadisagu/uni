/* $Id$ */
package ru.tandemservice.uninarfu.component.catalog.relationDegree.RelationDegreePub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEdit;
import org.tandemframework.shared.commonbase.base.bo.Declinable.ui.Edit.DeclinableEditUI;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorLanguage;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uninarfu.entity.RelationDegreeDeclinable;

/**
 * @author Ekaterina Zvereva
 * @since 27.04.2015
 */
public class Controller  extends DefaultCatalogPubController<RelationDegree, Model, IDAO>
{
    @Override
    protected DynamicListDataSource<RelationDegree> createListDataSource(IBusinessComponent context)
    {

        DynamicListDataSource<RelationDegree> dataSource = super.createListDataSource(context);
       Model model = getModel(context);
        dataSource.addColumn(new ActionColumn("Редактировать падежные формы", UniDefines.ICO_EDIT_CASE, "onClickEditItemDeclination")
                                     .setPermissionKey(model.getCatalogItemEdit()), dataSource.getColumns().size()-1);

        return dataSource;
    }

    public void onClickEditItemDeclination(IBusinessComponent context)
    {
        final InflectorLanguage lang = IUniBaseDao.instance.get().get(InflectorLanguage.class, InflectorLanguage.enabled().s(), Boolean.TRUE);
        if (null == lang)
            throw new ApplicationException("Не установлен активный язык склонений.");

        RelationDegreeDeclinable declinable = DataAccessServices.dao().get(RelationDegreeDeclinable.class, RelationDegreeDeclinable.parent().id(), (Long)context.getListenerParameter());
        context.createDefaultChildRegion(new ComponentActivator(DeclinableEdit.class.getSimpleName(), new ParametersMap()
                .add(UIPresenter.PUBLISHER_ID, declinable.getId())
                .add(DeclinableEditUI.BIND_LANGUAGE_ID, lang.getId())
        ));

    }
}