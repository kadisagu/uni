/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocument.logic.INarfuDipDocumentDao;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocument.logic.NarfuDipDocumentDao;

/**
 * @author Alexey Lopatin
 * @since 30.07.2015
 */
@Configuration
public class NarfuDipDocumentManager extends BusinessObjectManager
{
    public static NarfuDipDocumentManager instance()
    {
        return instance(NarfuDipDocumentManager.class);
    }

    @Bean
    public INarfuDipDocumentDao dao()
    {
        return new NarfuDipDocumentDao();
    }
}
