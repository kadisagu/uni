/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.ui.EditOwner;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;

/**
 * @author Andrey Andreev
 * @since 09.02.2016
 */
@Input(@Bind(key = UIPresenter.PUBLISHER_ID, binding = "eduPlanId", required = true))
public class NarfuEduPlanEditOwnerUI extends UIPresenter
{

    private Long _eduPlanId;
    private EppEduPlan _eppEduPlan;


    @Override
    public void onComponentRefresh()
    {
        _eppEduPlan = DataAccessServices.dao().get(EppEduPlan.class, getEduPlanId());
    }


    public void onClickApply()
    {
        DataAccessServices.dao().update(_eppEduPlan);
        deactivate();
    }

    public Long getEduPlanId()
    {
        return _eduPlanId;
    }

    public void setEduPlanId(Long eduPlanId)
    {
        _eduPlanId = eduPlanId;
    }

    public EppEduPlan getEppEduPlan()
    {
        return _eppEduPlan;
    }

    public void setEppEduPlan(EppEduPlan eppEduPlan)
    {
        _eppEduPlan = eppEduPlan;
    }
}