/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.Edit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Edit.EnrEntrantRequestEdit;

/**
 * @author Andrey Andreev
 * @since 05.04.2016
 */
@Configuration
public class EnrEntrantRequestEditExt extends BusinessComponentExtensionManager
{

    @Autowired
    private EnrEntrantRequestEdit _enrEntrantRequestEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantRequestEdit.presenterExtPoint())
                .create();
    }
}