package ru.tandemservice.uninarfu.component.reports.VacantBudgetPlaces.AddEnt14;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    void createReport(Model model);
}
