/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub.EppEduPlanPub;

/**
 * @author Andrey Andreev
 * @since 09.02.2016
 */
@Configuration
public class EppEduPlanPubExt extends BusinessComponentExtensionManager
{

    @Autowired
    private EppEduPlanPub _eppEduPlanPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduPlanPub.presenterExtPoint())
                .addAction(new EppEduPlanPubClickEditOwner("onClickEditOwner"))
                .create();
    }
}