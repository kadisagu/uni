/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.DipSettings;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uninarfu.settings.ext.DipSettings.logic.INarfuDipSettingsDao;
import ru.tandemservice.uninarfu.settings.ext.DipSettings.logic.NarfuDipSettingsDao;

/**
 * @author Alexey Lopatin
 * @since 28.05.2015
 */
@Configuration
public class DipSettingsExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public INarfuDipSettingsDao dao()
    {
        return new NarfuDipSettingsDao();
    }
}