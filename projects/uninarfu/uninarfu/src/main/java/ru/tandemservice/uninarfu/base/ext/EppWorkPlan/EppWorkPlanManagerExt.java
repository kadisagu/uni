/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.EppWorkPlan;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Denis Katkov
 * @since 10.03.2016
 */
@Configuration
public class EppWorkPlanManagerExt extends BusinessObjectExtensionManager{}