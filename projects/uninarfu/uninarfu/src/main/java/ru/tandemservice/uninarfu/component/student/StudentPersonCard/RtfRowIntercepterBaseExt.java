package ru.tandemservice.uninarfu.component.student.StudentPersonCard;

import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;

import java.util.List;

public class RtfRowIntercepterBaseExt extends RtfRowIntercepterBase {

    private final List<String[]> data;

    public RtfRowIntercepterBaseExt(List<String[]> data) {
        this.data = data;
    }

    @Override
    public void afterModify(RtfTable rtftable, List<RtfRow> list, int i) {
        boolean firstCourse = true;
        boolean firstTerm = true;
        for (int index = 0; index < data.size() - 1; index++) {
            firstCourse = mergeRow(list, index, 0, firstCourse);
            firstTerm = mergeRow(list, index, 1, firstTerm);
        }

    }

    private boolean mergeRow(List<RtfRow> list, int indexRow, int indexColumn, boolean flag) {
        //проверяем значения ячейек соседних строк
        if (data.get(indexRow)[indexColumn].equals(data.get(indexRow + 1)[indexColumn])) {    //до этого мержа небыло, начинаем мерж
            if (flag) {
                //шапка таблицы состоит из 2 строк, следовательно увеличиваем индекс строки на 2
                //нужной нам ячейке указываем направление мержа
                list.get(indexRow + 2).getCellList().get(indexColumn).setMergeType(IRtfData.CLVMGF);
            }
            //соседней ячеке указываем неправление мержа
            list.get(indexRow + 3).getCellList().get(indexColumn).setMergeType(294);
            flag = false;
        }
        else {
            //мерж закончен
            flag = true;
        }

        return flag;
    }
}
