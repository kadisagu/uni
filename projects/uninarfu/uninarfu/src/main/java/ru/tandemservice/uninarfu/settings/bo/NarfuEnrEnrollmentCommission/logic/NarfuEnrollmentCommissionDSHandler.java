/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuEnrEnrollmentCommission.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.List.EnrEnrollmentCommissionListUI;
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 10.06.2015
 */
public class NarfuEnrollmentCommissionDSHandler extends DefaultSearchDataSourceHandler
{


    public NarfuEnrollmentCommissionDSHandler(String ownerId, Class<? extends IEntity> entityClass)
    {
        super(ownerId, entityClass);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String title = context.get(EnrEnrollmentCommissionListUI.PARAM_TITLE);
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentCommission.class, "e").column("e");

        if (null != title)
            builder.where(likeUpper(property("e", EnrEnrollmentCommission.title()), value(CoreStringUtils.escapeLike(title, true))));

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
        final Map<EnrEnrollmentCommission, EnrEnrollmentCommissionExt> relationMap = getExtensionMap(output.getRecordIds());

        return output.transform((EnrEnrollmentCommission enrollmentCommission) -> {
            ViewWrapper<EnrEnrollmentCommission> wrapper = new ViewWrapper<>(enrollmentCommission);
            EnrEnrollmentCommissionExt extension = relationMap.get(enrollmentCommission);
            wrapper.setViewProperty(EnrEnrollmentCommissionExt.L_EMPLOYEE, extension != null ? extension.getEmployee() : null);
            wrapper.setViewProperty(EnrEnrollmentCommissionExt.P_PHONE_NUMBER, extension != null ? extension.getPhoneNumber() : null);
            wrapper.setViewProperty(EnrEnrollmentCommissionExt.P_SCHEDULE_ENROLLMENT_COMMISSION, extension != null ? extension.getScheduleEnrollmentCommission(): null);
            return wrapper;
        });
    }

    private Map<EnrEnrollmentCommission, EnrEnrollmentCommissionExt> getExtensionMap(List<Long> ids)
    {
        List<EnrEnrollmentCommissionExt> extensionList = new DQLSelectBuilder().fromEntity(EnrEnrollmentCommissionExt.class, "ext")
                .where(in(property("ext", EnrEnrollmentCommissionExt.enrEnrollmentCommission().id()), ids)).createStatement(getSession()).list();
        Map<EnrEnrollmentCommission, EnrEnrollmentCommissionExt> resultMap = new HashMap<>(extensionList.size());
        for (EnrEnrollmentCommissionExt item : extensionList)
            resultMap.put(item.getEnrEnrollmentCommission(), item);

        return resultMap;
    }

}