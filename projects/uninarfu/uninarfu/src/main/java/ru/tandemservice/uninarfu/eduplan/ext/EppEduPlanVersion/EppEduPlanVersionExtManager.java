/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlanVersion;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 09.04.2015
 */
@Configuration
public class EppEduPlanVersionExtManager extends BusinessObjectExtensionManager
{
}
