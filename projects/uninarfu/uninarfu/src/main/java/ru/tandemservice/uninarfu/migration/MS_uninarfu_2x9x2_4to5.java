/* $Id$ */
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uninarfu_2x9x2_4to5 extends IndependentMigrationScript
{

        @Override
        public ScriptDependency[] getBoundaryDependencies()
        {
            return new ScriptDependency[]
                    {
                            new ScriptDependency("org.tandemframework", "1.6.18"),
                            new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                            new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
                    };
        }

        @Override
        public void run(DBTool tool) throws Exception
        {
            ////////////////////////////////////////////////////////////////////////////////
            // сущность narfuEnrollmentCommissionMeetingProtocol

            // создано свойство orderStates
            if (!tool.columnExists("narfu_rep_enr_com_meet_prot_t", "orderstates_p"))
            {
                // создать колонку
                tool.createColumn("narfu_rep_enr_com_meet_prot_t", new DBColumn("orderstates_p", DBType.createVarchar(255)));

            }


        }
}