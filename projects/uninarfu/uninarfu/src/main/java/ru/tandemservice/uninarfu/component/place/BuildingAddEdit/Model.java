/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uninarfu.component.place.BuildingAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

/**
 * @author agolubenko
 * @since Aug 16, 2010
 */
@Input({@Bind(key = "buildingId", binding = "building.id"), @Bind(key = "groundId")})
public class Model extends ru.tandemservice.uniplaces.component.place.BuildingAddEdit.Model
{
    private String buildingNumber;

    public Integer getIntBuildingNumber()
    {
        return Integer.parseInt(buildingNumber.length() > 1 ? buildingNumber.replaceFirst("^0*", "") : buildingNumber);
    }

    public String getBuildingNumber()
    {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber)
    {
        this.buildingNumber = buildingNumber;
    }
}
