/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlanVersion.ui.BlockListEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockListEdit.EppEduPlanVersionBlockListEdit;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.NarfuEduPlanManager;

/**
 * @author Ekaterina Zvereva
 * @since 09.04.2015
 */
@Configuration
public class EppEduPlanVersionBlockListEditExt extends BusinessComponentExtensionManager
{

    public static final String ADDON_NAME = "EppEduPlanVersionBlockListEditExt";
    public static final String EDULVLHS_DS = "eduLvlHSchoolDS";

    public static final String ORG_UNIT_KEY = "orgUnit";
    public static final String PROGRAM_SUBJECT_KEY = "programSubject";
    public static final String VERSION_KEY = "version";

    @Autowired
    private EppEduPlanVersionBlockListEdit _eppEduPlanVersionBlockListEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduPlanVersionBlockListEdit.presenterExtPoint())
                .addDataSource(selectDS(EDULVLHS_DS, NarfuEduPlanManager.instance().eduPlanVerBlockLevelHSchoolHandler()))
                .addAddon(uiAddon(ADDON_NAME, EppEduPlanVersionBlockListEditExtUI.class))
                .addAction(new EppEduPlanVersionBlockListEditClickApply("onClickApply"))
                .create();
    }

}
