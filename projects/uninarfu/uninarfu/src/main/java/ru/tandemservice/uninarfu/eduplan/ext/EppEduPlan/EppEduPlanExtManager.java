/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;



/**
 * @author Ekaterina Zvereva
 * @since 27.03.2015
 */
@Configuration
public class EppEduPlanExtManager extends BusinessObjectExtensionManager
{
}
