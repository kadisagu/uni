/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEditUI;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

/**
 * @author DMITRY KNYAZEV
 * @since 06.04.2015
 */
public class PersonEduDocumentAddEditUIExt extends UIAddon {

    private PersonEduDocumentNARFU documentNarfu;
    private final ICommonDAO dao = DataAccessServices.dao();

    public PersonEduDocumentAddEditUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh() {
        if (getParentPresenter().isEditForm())
            documentNarfu = dao.get(PersonEduDocumentNARFU.class, PersonEduDocumentNARFU.eduDocument(), getDocument());

        if (documentNarfu == null)
            documentNarfu = new PersonEduDocumentNARFU();

    }

    //handlers
    public void onClickApply() {
        if (getParentPresenter().validate()) return;
        /** Если не удалить пустой документ то при попытке сохранить его в методе saveOrUpdateDocument()
         * стработает констраинт и ничего не сохранится */
        if (getDocumentNarfu().isEmpty() && getDocumentNarfu().getId() != null)
            dao.delete(getDocumentNarfu());

        PersonEduDocumentManager.instance().dao().saveOrUpdateDocument(getDocument(), getParentPresenter().getScanCopyFile());

        if (getDocumentNarfu().isNotEmpty()) {
            if (getDocumentNarfu().getEduDocument() == null)
                getDocumentNarfu().setEduDocument(getDocument());
            dao.saveOrUpdate(getDocumentNarfu());
        }

        getParentPresenter().deactivate();
    }

    //getters/setters
    PersonEduDocumentAddEditUI getParentPresenter() {
        return getPresenter();
    }

    public PersonEduDocument getDocument() {
        return getParentPresenter().getDocument();
    }

    public PersonEduDocumentNARFU getDocumentNarfu() {
        return documentNarfu;
    }
}
