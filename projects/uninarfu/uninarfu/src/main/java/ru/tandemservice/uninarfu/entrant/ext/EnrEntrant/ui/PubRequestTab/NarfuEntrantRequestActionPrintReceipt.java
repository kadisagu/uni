/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 13.05.2015
 */
public class NarfuEntrantRequestActionPrintReceipt extends NamedUIAction
{
    protected NarfuEntrantRequestActionPrintReceipt(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        UIAddon addon = (UIAddon) presenter.getConfig().getAddon("EnrEntrantRequestActionsAddon");
        if (addon == null) return;
        String templateCode = EnrScriptItemCodes.ENTRANT_REQUEST_RECEIPT_DOCUMENTS;
        if (EnrRequestTypeCodes.SPO.equals(((EnrEntrantRequestActionsAddon) addon).getEntrantRequest().getType().getCode()))
                templateCode = EnrScriptItemCodes.ENTRANT_REQUEST_RECEIPT_DOCUMENTS_SPO;
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, templateCode);

        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, presenter.getListenerParameterAsLong());
        byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        if (document != null)
            scriptResult.put(IScriptExecutor.DOCUMENT,  insertOperatorData(document));

        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(scriptResult);
        else
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT)).rtf().fileName((String) scriptResult.get(IScriptExecutor.FILE_NAME).toString()), false);
    }

    private byte[] insertOperatorData(byte[] document)
    {
        final RtfDocument docRtf = new RtfReader().read(document);
        RtfInjectModifier im = new RtfInjectModifier();
        //Подпись принявшего документы
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();

        im.put("userPost", principalContext == null? "" : principalContext.getContextTitle());
        im.put("userFIO", principalContext == null? ""  : principalContext.getFio());

        im.modify(docRtf);
        return RtfUtil.toByteArray(docRtf);
    }


}