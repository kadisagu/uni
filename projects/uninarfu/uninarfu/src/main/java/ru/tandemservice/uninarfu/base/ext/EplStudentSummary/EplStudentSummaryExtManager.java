/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.EplStudentSummary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.IEplStudentSummaryDao;
import ru.tandemservice.uninarfu.base.ext.EplStudentSummary.logic.NarfuStudentSummaryDao;

/**
 * @author Alexey Lopatin
 * @since 18.03.2016
 */
@Configuration
public class EplStudentSummaryExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEplStudentSummaryDao dao()
    {
        return new NarfuStudentSummaryDao();
    }
}
