/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings.ui.OrgUnitNumbersList;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uninarfu.base.bo.NarfuSettings.NarfuSettingsManager;
import ru.tandemservice.uninarfu.base.bo.NarfuSettings.ui.OrgUnitNumberEdit.NarfuSettingsOrgUnitNumberEdit;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 17.06.2015
 */
public class NarfuSettingsOrgUnitNumbersListUI extends UIPresenter
{

    public void onClickEditCode()
    {
        _uiActivation.asRegionDialog(NarfuSettingsOrgUnitNumberEdit.class).parameter("orgUnitId", getListenerParameterAsLong()).activate();
    }


    public void onClickDeleteCode()
    {
        NarfuSettingsManager.instance().dao().deleteOrgUnitCode(getListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);

    }


}
