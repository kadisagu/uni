package ru.tandemservice.uninarfu.base.ext.UniStudent.ui.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.List.UniStudentList;
import ru.tandemservice.uninarfu.base.ext.UniStudent.UniStudentMangerExt;

@Configuration
public class UniStudentListExt extends BusinessComponentExtensionManager {

    public static final String ADDON = "narfu_" + UniStudentListExtUI.class.getSimpleName();

    @Autowired
    private UniStudentList parent;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder presenter = presenterExtensionBuilder(parent.presenterExtPoint());
        presenter.addAddon(uiAddon(ADDON, UniStudentListExtUI.class));
        presenter.addDataSource(UniStudentMangerExt.instance().citizenshipDSConfig());
        presenter.replaceDataSource(searchListDS(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS, parent.studentSearchListDSColumnExtPoint())
                                     .handler(UniStudentMangerExt.instance().studentSearchListExtDSHandler()));
        return presenter.create();
    }
}
