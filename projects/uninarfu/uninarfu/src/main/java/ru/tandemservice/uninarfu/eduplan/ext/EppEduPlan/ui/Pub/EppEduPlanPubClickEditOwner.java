/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.Pub.EppEduPlanPubUI;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.ui.EditOwner.NarfuEduPlanEditOwner;

/**
 * @author Andrey Andreev
 * @since 09.02.2016
 */
public class EppEduPlanPubClickEditOwner extends NamedUIAction
{


    protected EppEduPlanPubClickEditOwner(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EppEduPlanPubUI)
        {
            presenter.getActivationBuilder()
                    .asRegionDialog(NarfuEduPlanEditOwner.class)
                    .parameter(PublisherActivator.PUBLISHER_ID_KEY, ((EppEduPlanPubUI) presenter).getHolder().getValue().getId())
                    .activate();
        }
    }
}