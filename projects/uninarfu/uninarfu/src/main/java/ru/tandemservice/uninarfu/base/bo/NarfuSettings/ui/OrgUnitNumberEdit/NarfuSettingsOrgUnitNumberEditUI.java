/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings.ui.OrgUnitNumberEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments;
import ru.tandemservice.uninarfu.entity.gen.OrgUnitNumberForEnrEntrantDocumentsGen;

/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
@Input({
               @Bind(key = "orgUnitId", binding = "orgUnitId", required = true)
       })
public class NarfuSettingsOrgUnitNumberEditUI extends UIPresenter
{
    private Long _orgUnitId;
    private OrgUnitNumberForEnrEntrantDocuments _orgUnitNumber;

    @Override
    public void onComponentRefresh()
    {
        OrgUnit orgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _orgUnitId);
        _orgUnitNumber = DataAccessServices.dao().getByNaturalId(new OrgUnitNumberForEnrEntrantDocumentsGen.NaturalId(orgUnit));
        if (_orgUnitNumber == null)
        {
            _orgUnitNumber = new OrgUnitNumberForEnrEntrantDocuments();
            _orgUnitNumber.setOrgUnit(orgUnit);
        }
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnitNumberForEnrEntrantDocuments getOrgUnitNumber()
    {
        return _orgUnitNumber;
    }

    public void setOrgUnitNumber(OrgUnitNumberForEnrEntrantDocuments orgUnitNumber)
    {
        _orgUnitNumber = orgUnitNumber;
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_orgUnitNumber);
        deactivate();
    }
}
