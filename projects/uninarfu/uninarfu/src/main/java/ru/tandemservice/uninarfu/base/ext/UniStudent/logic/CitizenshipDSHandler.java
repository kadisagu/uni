package ru.tandemservice.uninarfu.base.ext.UniStudent.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.shared.fias.base.entity.AddressCountry;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class CitizenshipDSHandler extends DefaultComboDataSourceHandler {

    public CitizenshipDSHandler(String ownerId) {
        super(ownerId, AddressCountry.class, AddressCountry.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep) {

		/*MQBuilder subBuilder =  new MQBuilder(IdentityCard.ENTITY_CLASS, "ic");
		subBuilder.getSelectAliasList().clear();
		subBuilder.addSelect(IdentityCard.citizenship().id().s()); 
		subBuilder.setNeedDistinct(true);
		List<Long> list = UniDaoFacade.getCoreDao().getList(subBuilder);
		
		ep.dqlBuilder.where(DQLExpressions.in(DQLExpressions.property(AddressCountry.id().fromAlias("e")), list));
		UniDaoFacade.getCoreDao().getList(ep.dqlBuilder);
		*/
        String filter = ep.input.getComboFilterByValue();
        if (StringUtils.isNotBlank(filter)) {
            ep.dqlBuilder.where(like(DQLFunctions.upper(property(AddressCountry.title().fromAlias("e"))), value(CoreStringUtils.escapeLike(filter, true))));
        }
    }

    @Override
    protected void prepareOrders(ExecutionParameters<DSInput, DSOutput> ep) {
        ep.dqlBuilder.order("e." + AddressCountry.title());
    }

}
