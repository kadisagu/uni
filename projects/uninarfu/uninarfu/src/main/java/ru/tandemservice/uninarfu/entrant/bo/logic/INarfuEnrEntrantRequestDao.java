/* $Id$ */
package ru.tandemservice.uninarfu.entrant.bo.logic;

import org.hibernate.Session;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.IEnrEntrantRequestDao;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
public interface INarfuEnrEntrantRequestDao extends IEnrEntrantRequestDao
{
    void updateEntrantAndTheirRequests(EnrRequestedCompetition requestedCompetition, Session session);
}
