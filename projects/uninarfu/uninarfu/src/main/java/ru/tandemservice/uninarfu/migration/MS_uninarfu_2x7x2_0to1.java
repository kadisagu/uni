package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность personEduDocumentNARFU

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("personedudocumentnarfu_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_personedudocumentnarfu"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("edudocument_id", DBType.LONG).setNullable(false), 
				new DBColumn("dateissuecertificate_p", DBType.DATE), 
				new DBColumn("expecteddatereturn_p", DBType.DATE), 
				new DBColumn("actualdatereturn_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("personEduDocumentNARFU");

		}


    }
}