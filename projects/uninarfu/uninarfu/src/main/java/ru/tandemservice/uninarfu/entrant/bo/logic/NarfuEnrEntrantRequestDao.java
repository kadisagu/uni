/* $Id$ */
package ru.tandemservice.uninarfu.entrant.bo.logic;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.EnrEntrantRequestDao;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments;
import ru.tandemservice.uninarfu.entity.gen.OrgUnitNumberForEnrEntrantDocumentsGen;
import ru.tandemservice.uninarfu.entrant.bo.NarfuEnrEntrant.NarfuEnrEntrantManager;


/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
public class NarfuEnrEntrantRequestDao extends EnrEntrantRequestDao implements INarfuEnrEntrantRequestDao
{
    @Override
    public void updateEntrantAndTheirRequests(EnrRequestedCompetition requestedCompetition, Session session)
    {
        EnrEntrant entrant = requestedCompetition.getRequest().getEntrant();

        OrgUnitNumberForEnrEntrantDocuments formativeOrgUnitRel = getByNaturalId(new OrgUnitNumberForEnrEntrantDocumentsGen.NaturalId(requestedCompetition.getCompetition().getProgramSetOrgUnit().getFormativeOrgUnit()));

        String formativeOrgUnitCode = formativeOrgUnitRel == null ? "??" : formativeOrgUnitRel.getNumber();

        String programFormCode = null;
        switch (requestedCompetition.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getCode())
        {
            case EduProgramFormCodes.OCHNAYA:
                programFormCode = "0";
                break;
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                programFormCode = "1";
                break;
            case EduProgramFormCodes.ZAOCHNAYA:
                programFormCode = "2";

        }


        if (null != formativeOrgUnitCode && requestedCompetition.getRequest().getEntrant().getPersonalNumber().contains("?"))
        {
            NarfuEntrantNumberGenerationRule entrantNumGenRule = NarfuEnrEntrantManager.instance().entrantNumberGenerationRule().formativeOrgUnitCode(formativeOrgUnitCode).programFormCode(programFormCode);
            entrant.setPersonalNumber(INumberQueueDAO.instance.get().getNextNumber(entrantNumGenRule, entrant));
            session.update(entrant);
        }
    }
}
