/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.EdiSpecialExamConditions;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EdiSpecialExamConditions.EnrEntrantEdiSpecialExamConditionsUI;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.NarfuEntrantManager;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
public class EnrEntrantEdiSpecialExamCondClickApply extends NamedUIAction
{
    protected EnrEntrantEdiSpecialExamCondClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EnrEntrantEdiSpecialExamConditionsUI)
        {
            EnrEntrantEdiSpecialExamConditionsUI addEditUI = (EnrEntrantEdiSpecialExamConditionsUI) presenter;
            addEditUI.onClickApply();


            IUIAddon uiAddon = presenter.getConfig().getAddon(EnrEntrantEdiSpecialExamConditionsExt.ADDON_NAME);
            if (null != uiAddon)
            {
                if (uiAddon instanceof EnrEntrantEdiSpecialExamConditionsExtUI)
                {
                    EnrEntrantEdiSpecialExamConditionsExtUI addon = (EnrEntrantEdiSpecialExamConditionsExtUI) uiAddon;
                    NarfuEntrantManager.instance().dao().deleteSpecConditionForEntrant(addEditUI.getEntrant());

                    NarfuEntrantManager.instance().dao().saveOrUpdateSpecConditions(addEditUI.getEntrant(), addon.getSpecConditionsList());
                }

            }

            addEditUI.deactivate();
        }
    }

}