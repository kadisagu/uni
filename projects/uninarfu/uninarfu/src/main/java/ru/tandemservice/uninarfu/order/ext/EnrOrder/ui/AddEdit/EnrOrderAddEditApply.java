/* $Id$ */
package ru.tandemservice.uninarfu.order.ext.EnrOrder.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit.EnrOrderAddEditUI;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;
import ru.tandemservice.unienr14.order.entity.EnrOrder;

import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 30.07.2015
 */
public class EnrOrderAddEditApply extends NamedUIAction
{
    protected EnrOrderAddEditApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EnrOrderAddEditUI)
        {
            EnrOrderAddEditUI addEditUI = (EnrOrderAddEditUI) presenter;

            EnrOrder order = addEditUI.getOrder();
            Date createDate = addEditUI.getCreateDate();
            boolean addForm = addEditUI.isAddForm();
            String executor = OrderExecutorSelectModel.getExecutor(addEditUI.getEmployeePost());

            EnrOrder enrOrder = EnrOrderManager.instance().dao().saveOrUpdateOrder(order, createDate, addForm, executor);

            IUIAddon uiAddon = presenter.getConfig().getAddon(EnrOrderAddEditExt.ADDONE_NAME);
            if (null != uiAddon)
            {
                if (uiAddon instanceof EnrOrderAddEditExtUI)
                {
                    EnrOrderAddEditExtUI addon = (EnrOrderAddEditExtUI) uiAddon;
                    addon.getOrderExt().setEnrOrder(enrOrder);
                    DataAccessServices.dao().saveOrUpdate(addon.getOrderExt());
                }
            }


            if (addEditUI.isAddForm())
                addEditUI.getActivationBuilder().asDesktopRoot(EnrOrderPub.class)
                        .parameter(UIPresenter.PUBLISHER_ID, enrOrder.getId())
                        .activate();

        }
    }
}
