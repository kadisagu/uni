package ru.tandemservice.uninarfu.entity;

import ru.tandemservice.uninarfu.entity.gen.*;

/**
 * @see ru.tandemservice.uninarfu.entity.gen.PersonEduDocumentNARFUGen
 */
public class PersonEduDocumentNARFU extends PersonEduDocumentNARFUGen {
    public boolean isEmpty() {
        return getDateIssueCertificate() == null && getExpectedDateReturn() == null && getActualDateReturn() == null;
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }
}