package ru.tandemservice.uninarfu.entity;

import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uninarfu.entity.gen.*;

/** @see ru.tandemservice.uninarfu.entity.gen.NarfuBSOVisasTemplateGen */
public class NarfuBSOVisasTemplate extends NarfuBSOVisasTemplateGen
{

    public static EntityComboDataSourceHandler defaultSelectDSHandler(String name)
    {
        return new EntityComboDataSourceHandler(name, NarfuBSOVisasTemplate.class)
                .order(NarfuBSOVisasTemplate.title())
                .filter(NarfuBSOVisasTemplate.title());
    }
}