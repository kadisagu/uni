/*$Id$*/
package ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.logic;

import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
public class JournalIssuanceCertificatesDao extends UniBaseDao implements IJournalIssuanceCertificatesDao {

    @Override
    public Collection<JournalDocumentWrapper> getJournalDocumentWrapperList() {
        final String studentId = "student_id";
        final String personId = "person_id";

        final DQLSelectBuilder studentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(DQLFunctions.min(property("s", Student.id())), studentId)
                .column(property("s", Student.person().id()), personId)
                .group(property("s", Student.person().id()));

        final String alias = "pedn";
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonEduDocumentNARFU.class, alias)
                .column(property(alias))
                .column(property("s"))
                .where(isNotNull(property(alias, PersonEduDocumentNARFU.dateIssueCertificate())))
                .where(isNull(property(alias, PersonEduDocumentNARFU.actualDateReturn())))
                .joinPath(DQLJoinType.inner, PersonEduDocumentNARFU.eduDocument().person().fromAlias(alias), "p")
                .joinDataSource("p", DQLJoinType.inner, studentBuilder.buildQuery(), "t", eq(property("p", Person.id()), property("t", personId)))
                .joinEntity("t", DQLJoinType.inner, Student.class, "s", eq(property("t", studentId), property("s", Student.id())));
        List<Object[]> list = builder.createStatement(getSession()).list();
        if (list.isEmpty())
            return Collections.emptyList();

        List<JournalDocumentWrapper> result = new ArrayList<>(list.size());
        for (Object[] obj : list) {
            PersonEduDocumentNARFU eduDocumentNARFU = (PersonEduDocumentNARFU) obj[0];
            Student student = (Student) obj[1];
            result.add(new JournalDocumentWrapper(eduDocumentNARFU, student));
        }
        return result;
    }

    public EducationYear getCurrentEduYear() {
        return get(EducationYear.class, EducationYear.current(), Boolean.TRUE);
    }
}
