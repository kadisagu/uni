/* $Id$ */
package ru.tandemservice.uninarfu.bso.ext.DipDiplomaBlank;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Andrey Andreev
 * @since 23.03.2016
 */
@Configuration
public class DipDiplomaBlankExtManager extends BusinessObjectExtensionManager
{
}