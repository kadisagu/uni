package ru.tandemservice.uninarfu.settings.ext.DipSettings.logic;

import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.unidip.settings.bo.DipSettings.logic.IDipSettingsDao;

/**
 * @author Alexey Lopatin
 * @since 28.05.2015
 */
public interface INarfuDipSettingsDao extends IDipSettingsDao
{
    /**
     * Формируется регистрационный номер в рамках подразделения с указанием года.
     * Формат номера «[К]-[Н]/[Г]», где
     *      К - внутренний код формирующего подразделения,
     *      Н - порядковый четырехзначный номер факта выдачи внутри подразделения,
     *      Г - год в формате «ГГГГ» из текущей даты.
     * Если в рамках года нет ни одного номера, то выводим НННН = 0001
     *
     * @return номер в формате «К-НННН/ГГГГ»
     */
    String getDipIssuanceNextRegNumberWithinEduYear(DiplomaObject dipObject);
}
