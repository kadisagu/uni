/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.EdiSpecialExamConditions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EdiSpecialExamConditions.EnrEntrantEdiSpecialExamConditions;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.NarfuEntrantManager;

/**
 * @author Ekaterina Zvereva
 * @since 16.04.2015
 */
@Configuration
public class EnrEntrantEdiSpecialExamConditionsExt extends BusinessComponentExtensionManager
{

    public static final String ADDON_NAME = "narfuEnrEntrantEdiSpecialExamConditionsExt";
    public static final String SPEC_COND_DS = "specConditionsDS";

    @Autowired
    private EnrEntrantEdiSpecialExamConditions _enrEntrantEdiSpecialExamConditionsExt;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantEdiSpecialExamConditionsExt.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantEdiSpecialExamConditionsExtUI.class))
                .addDataSource(selectDS(SPEC_COND_DS, NarfuEntrantManager.instance().specialConditionsExamsDSHandler()))
                .addAction(new EnrEntrantEdiSpecialExamCondClickApply("onClickApply"))
                .create();
    }
}