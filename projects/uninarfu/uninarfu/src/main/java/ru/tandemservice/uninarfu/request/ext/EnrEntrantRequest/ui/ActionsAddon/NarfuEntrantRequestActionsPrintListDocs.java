/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;

/**
 * @author Ekaterina Zvereva
 * @since 13.05.2015
 */
public class NarfuEntrantRequestActionsPrintListDocs extends NamedUIAction
{
    public NarfuEntrantRequestActionsPrintListDocs(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.ENTRANT_REQUEST_DOCUMENTS_LIST);
        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(scriptItem, presenter.getListenerParameter()));
        else
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, presenter.getListenerParameterAsLong());
    }
}