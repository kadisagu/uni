/* $Id$ */
package ru.tandemservice.uninarfu.order.ext.EnrOrder;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 30.07.2015
 */
@Configuration
public class EnrOrderExtManager extends BusinessObjectExtensionManager
{
}
