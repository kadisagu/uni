/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.HigherProfAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEditUI;

/**
 * @author Ekaterina Zvereva
 * @since 20.04.2015
 */
public class EppEduPlanHigherProfClickApply extends NamedUIAction
{
    protected EppEduPlanHigherProfClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EppEduPlanHigherProfAddEditUI)
        {
            EppEduPlanHigherProfAddEditUI addEditUI = (EppEduPlanHigherProfAddEditUI) presenter;
            addEditUI.onClickApply();


            IUIAddon uiAddon = presenter.getConfig().getAddon(EppEduPlanHigherProfAddEditExt.ADDONE_NAME);
            if (null != uiAddon)
            {
                if (uiAddon instanceof EppEduPlanHigherProfAddEditExtUI)
                {
                    EppEduPlanHigherProfAddEditExtUI addon = (EppEduPlanHigherProfAddEditExtUI) uiAddon;
                    if (addon.getRelObject().getEducationLevelsHighSchool() != null)
                        DataAccessServices.dao().saveOrUpdate(addon.getRelObject());
                }
            }

            addEditUI.deactivate();

        }
    }
}
