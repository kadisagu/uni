package ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.CheckingGraduatesData;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.unidip.base.entity.catalog.DipScriptItem;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uninarfu.base.entity.catalog.codes.DipScriptItemCodes;

import java.util.List;
import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 28.10.2015
 */
public class NarfuDipDocumentReportCheckingGraduatesDataUI extends UIPresenter
{
    public static final String PARAM_ORG_UNIT = "orgUnit";
    public static final String PARAM_DEVELOP_FORM = "developForm";
    public static final String PARAM_DEVELOP_CONDITION = "developCondition";
    public static final String PARAM_DEVELOP_PERIOD = "developPeriod";
    public static final String PARAM_STUDENTS_STATUS = "studentsStatus";
    public static final String PARAM_PROGRAM_SUBJECTS = "programSubjects";

    @Override
    public void onComponentPrepareRender()
    {

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(NarfuDipDocumentReportCheckingGraduatesData.PROGRAM_SUBJECT_DS)) {
            OrgUnit orgUnit = getSettings().get(PARAM_ORG_UNIT);
            DevelopForm developForm = getSettings().get(PARAM_DEVELOP_FORM);
            DevelopCondition developCondition = getSettings().get(PARAM_DEVELOP_CONDITION);
            DevelopPeriod developPeriod = getSettings().get(PARAM_DEVELOP_PERIOD);

            dataSource.put(PARAM_ORG_UNIT, orgUnit != null ? orgUnit.getId() : null);
            dataSource.put(PARAM_DEVELOP_FORM, developForm != null ? developForm.getId() : null);
            dataSource.put(PARAM_DEVELOP_CONDITION, developCondition != null ? developCondition.getId() : null);
            dataSource.put(PARAM_DEVELOP_PERIOD, developPeriod != null ? developPeriod.getId() : null);
        }

        if (dataSource.getName().equals(NarfuDipDocumentReportCheckingGraduatesData.DEVELOP_PERIOD_DS)) {
            OrgUnit orgUnit = getSettings().get(PARAM_ORG_UNIT);
            dataSource.put(PARAM_ORG_UNIT, orgUnit != null ? orgUnit.getId() : null);
        }
    }

    @Override
    public void onComponentRefresh()
    {

        getSettings().clear();
        super.onComponentRefresh();
    }

    public void onClickPrint()
    {
        getSettings().save();

        OrgUnit orgUnit = getSettings().get(PARAM_ORG_UNIT);
        DevelopForm developForm = getSettings().get(PARAM_DEVELOP_FORM);
        DevelopCondition developCondition = getSettings().get(PARAM_DEVELOP_CONDITION);
        DevelopPeriod developPeriod = getSettings().get(PARAM_DEVELOP_PERIOD);
        List<EduProgramSubject> studentsStatusList = getSettings().get(PARAM_STUDENTS_STATUS);
        List<EduProgramSubject> eduProgramSubjects = getSettings().get(PARAM_PROGRAM_SUBJECTS);


        DipScriptItem scriptItem = DataAccessServices.dao()
                .getByCode(DipScriptItem.class, DipScriptItemCodes.NARFU_CHECKING_GRADUATES_DATA);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                "orgUnitId", orgUnit != null ? orgUnit.getId() : null,
                "developFormId", developForm != null ? developForm.getId() : null,
                "developConditionId", developCondition != null ? developCondition.getId() : null,
                "developPeriodId", developPeriod != null ? developPeriod.getId() : null,
                "studentsStatusIds", CommonBaseEntityUtil.getIdList(studentsStatusList),
                "programSubjectIds", CommonBaseEntityUtil.getIdList(eduProgramSubjects));

        byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

        if (null == document) {
            throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
        }
        if (null == filename || !filename.contains(".")) {
            throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
        }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(document).fileName(filename).rtf(), false);

        deactivate();
    }

    //Getters and Setters

}