package ru.tandemservice.uninarfu.base.ext.UniStudent.ui.OrgUnitList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList.UniStudentOrgUnitListUI;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unibasermc.component.orgunit.abstractOrgUnit.OrgUnitStudenAddToGroup.Controller;
import ru.tandemservice.uninarfu.base.ext.UniStudent.UniStudentMangerExt;
import ru.tandemservice.uninarfu.base.ext.UniStudent.logic.IDiplomaUniStudentDAO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UniStudentOrgUnitListExtUI extends UIAddon
{

    private UniStudentOrgUnitListUI parentUI = getPresenter();

    private String searchListCaption = parentUI.isArchival() ? "Список архивных обучающихся" : "Список обучающихся";
    private boolean _guiSelectStudentMode = false;
    private boolean warning = false;
    private String warningMessage = "";

    private Collection<IEntity> students;

    public UniStudentOrgUnitListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS))
        {
            if (getGuiSelectStudentMode())
            {
                CheckboxColumn selectColumn = new CheckboxColumn("select");
                if (getStudents() != null)
                    selectColumn.setSelectedObjects(getStudents());
                getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS).addColumn(selectColumn, 0);
            }

            dataSource.put("citizenshipList", parentUI.getSettings().get("citizenshipList"));
            dataSource.put("comment", parentUI.getSettings().get("comment"));
        }

        super.onBeforeDataSourceFetch(dataSource);
    }

    // Listeners
    public void onClickArchiveStudents()
    {
        this.getActivationBuilder().asRegion("ru.tandemservice.uninarfu.component.student.MoveStudentsToArchive")
                .parameter("orgUnitId", parentUI.getOrgUnitId())
                .activate();
    }

    /**
     * Отмена выделения студентов
     */
    public void onNotSelectStudent()
    {
        setGuiSelectStudentMode(false);
    }

    public void onSelectStudent()
    {
        setGuiSelectStudentMode(true);
    }

    public void onRemoveStudent()
    {
        setGuiSelectStudentMode(false);
        setStudents(null);
        setWarning(false);
        setWarningMessage("");

        cleanUIDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS);

    }

    public void onIgnoreWarning()
    {
        setWarning(false);
        setWarningMessage("");

        Collection<IEntity> lst = getStudents();

        if (!lst.isEmpty())
        {
            toComponent(lst);
        }

    }

    public void onStudentToGroup()
    {
        setWarning(false);
        setWarningMessage("");

        String warnungMessage = "";

        CheckboxColumn ck = (CheckboxColumn) getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS).getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        if (lst.size() > 0)
        {

            Collection<IEntity> ids = new ArrayList<IEntity>();
            for (IEntity ent : lst)
            {
                Student student = null;
                if (ent instanceof DataWrapper)
                    student = (Student) ((DataWrapper) ent).getWrapped();

                if (ent instanceof Student)
                    student = (Student) ent;

                if (student != null && student.getGroup() != null)
                {
                    warnungMessage += student.getPerson().getFio() + " в группе " + student.getGroup().getTitle() + "\r\n" + " ";
                    setWarning(true);
                    setWarningMessage(warnungMessage);
                }
                if (student != null)
                    ids.add(ent);
            }

            if (!getWarning())
                toComponent(ids);
            else
            {
                setStudents(ids);
                // допишем начало сообщения
                warnungMessage = "Вы выбрали студентов, которые уже числятся в группах: " + getWarningMessage();
                setWarningMessage(warnungMessage);
            }

        }
        else
            throw new ApplicationException("Не выбраны студенты для включения в группу");

    }

    private void toComponent(Collection<IEntity> entities)
    {
        BusinessComponent component = getPresenter().getConfig().getBusinessComponent();

        CheckboxColumn ck = (CheckboxColumn) getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS).getColumn("select");

        // создаем карту контрола
        UniMap map = new UniMap();
        map.add("ids", UniUtils.getIdList(entities));
        map.add("orgUnitId", parentUI.getOrgUnitId());

        String key = Controller.GET_KEY(parentUI.getOrgUnitId());
        IDataSettings settingKeyOther = UniBaseUtils.getDataSettings(component, key);

        // технология и форма освоения через настройки
        settingKeyOther.set("courseList", getPresenter().getSettings().get("courseList"));
        settingKeyOther.set("eduLevelHighSchool", getPresenter().getSettings().get("eduLevelHighSchool"));
        settingKeyOther.set("developFormList", getPresenter().getSettings().get("developFormList"));

        UniDaoFacade.getSettingsManager().saveSettings(settingKeyOther);
        component.createRegion(new ComponentActivator("ru.tandemservice.unibasermc.component.orgunit.abstractOrgUnit.OrgUnitStudenAddToGroup", map));

        List<IEntity> empty = new ArrayList<IEntity>();
        ck.setSelectedObjects(empty);
    }

    public void onStudentToProtocolSAC()
    {
        CheckboxColumn ck = (CheckboxColumn) getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS).getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        if (lst.isEmpty())
            throw new ApplicationException("Не выбраны студенты для ввода данных протокола ГАК");


        List<Long> studentIds = lst.stream()
                .map(entity -> {
                    if (entity instanceof DataWrapper)
                        return ((DataWrapper) entity).getWrapped();
                    if (entity instanceof Student)
                        return (Student) entity;
                    return null;
                })
                .filter(student -> student != null)
                .map(Student::getId)
                .collect(Collectors.toList());

        IDiplomaUniStudentDAO dao = UniStudentMangerExt.instance().diplomaUniStudentDAO();
        EducationLevels el = dao.getEducationLevels(studentIds);
        Qualifications qualif = dao.getQualificationsFromEduPlan(studentIds);

        ParametersMap params = new ParametersMap()
                .add("ids", studentIds)
                .add("orgUnitId", parentUI.getOrgUnitId());
        if (el != null)
            params.add("educationLevel", el);

        if (qualif != null)
            params.add("qualification", qualif);

        Activator activator = new ComponentActivator("ru.tandemservice.diplomarmc.component.orgunit.StudentToProtocolSAC", params);
        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
    }


    private void cleanUIDataSource(String name)
    {
        IUIDataSource dataSource = getPresenter().getConfig().getDataSource(name);
        if (dataSource != null)
            dataSource.doCleanupDataSource();
    }


    // Getter and Setters
    public void setGuiSelectStudentMode(boolean guiSelectStudentMode)
    {
        _guiSelectStudentMode = guiSelectStudentMode;
    }

    public boolean getGuiSelectStudentMode()
    {
        return _guiSelectStudentMode;
    }

    public void setWarning(boolean warning)
    {
        this.warning = warning;
    }

    public boolean getWarning()
    {
        return warning;
    }

    public void setWarningMessage(String varningMessage)
    {
        this.warningMessage = varningMessage;
    }

    public String getWarningMessage()
    {
        return warningMessage;
    }

    public DynamicListDataSource getDataSource(String name)
    {
        return (DynamicListDataSource) getPresenter().getConfig().getDataSource(name).getResult();
    }

    public Collection<IEntity> getStudents()
    {
        return students;
    }

    public void setStudents(Collection<IEntity> students)
    {
        this.students = students;
    }

    public String getSearchListCaption()
    {
        return searchListCaption;
    }

    public void setSearchListCaption(String searchListCaption)
    {
        this.searchListCaption = searchListCaption;
    }

    public boolean isStudentListCommentFilter()
    {
        String commentFilter = ApplicationRuntime.getProperty("unibasermc.studentListCommentFilter");
        return commentFilter != null && commentFilter.equalsIgnoreCase("true");
    }

    public boolean isStudentListAdvansedFilter()
    {
        String commentFilter = ApplicationRuntime.getProperty("unibasermc.studentListAdvansedFilter");
        return commentFilter != null && commentFilter.equalsIgnoreCase("true");
    }

    public boolean isNotStudentListAdvansedFilter()
    {
        return !isStudentListAdvansedFilter();
    }
}
