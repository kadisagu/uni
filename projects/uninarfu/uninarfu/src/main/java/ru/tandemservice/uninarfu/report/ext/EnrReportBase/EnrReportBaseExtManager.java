/* $Id$ */
package ru.tandemservice.uninarfu.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.uninarfu.entity.*;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EnrolledEntrantActAcceptanceDocAdd.NarfuEnrReportEnrolledEntrantActAcceptanceDocAdd;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantActAcceptanceDocAdd.NarfuEnrReportEntrantActAcceptanceDocAdd;

/**
 * @author Andrey Avetisov
 * @since 09.04.2015
 */
@Configuration
public class EnrReportBaseExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
                .add(NarfuEnrReportRequestCount.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(NarfuEnrReportRequestCount.REPORT_KEY))
                .add(NarfuEnrReportRequestCompetitionList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(NarfuEnrReportRequestCompetitionList.REPORT_KEY))
                .add(NarfuEnrReportRegistrationJournal.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(NarfuEnrReportRegistrationJournal.REPORT_KEY))
                .add(NarfuEnrEntrantDailyList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(NarfuEnrEntrantDailyList.REPORT_KEY))
                .add(NarfuEnrollmentCommissionMeetingProtocol.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(NarfuEnrollmentCommissionMeetingProtocol.REPORT_KEY))
                .add("narfuEnr14ReportEntrantActAcceptanceDoc", EnrReportBaseManager.getReportDefinition("narfuEnr14ReportEntrantActAcceptanceDoc", NarfuEnrReportEntrantActAcceptanceDocAdd.class))
                .add("narfuEnr14ReportEnrolledEntrantActAcceptanceDoc", EnrReportBaseManager.getReportDefinition("narfuEnr14ReportEnrolledEntrantActAcceptanceDoc", NarfuEnrReportEnrolledEntrantActAcceptanceDocAdd.class))
                .create();
    }

    @Bean
    public ItemListExtension<IEnrStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(_enrReportBaseManager.storableReportDescExtPoint())
                .add(NarfuEnrReportRequestCount.REPORT_KEY, NarfuEnrReportRequestCount.getDescription())
                .add(NarfuEnrReportRequestCompetitionList.REPORT_KEY, NarfuEnrReportRequestCompetitionList.getDescription())
                .add(NarfuEnrReportRegistrationJournal.REPORT_KEY, NarfuEnrReportRegistrationJournal.getDescription())
                .add(NarfuEnrEntrantDailyList.REPORT_KEY, NarfuEnrEntrantDailyList.getDescription())
                .add(NarfuEnrollmentCommissionMeetingProtocol.REPORT_KEY, NarfuEnrollmentCommissionMeetingProtocol.getDescription())
                .create();
    }
}
