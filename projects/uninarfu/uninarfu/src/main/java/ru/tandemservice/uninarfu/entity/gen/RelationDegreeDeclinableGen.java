package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.base.entity.IDeclinable;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import ru.tandemservice.uninarfu.entity.RelationDegreeDeclinable;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Склоняемые степени родства
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RelationDegreeDeclinableGen extends EntityBase
 implements IDeclinable{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.RelationDegreeDeclinable";
    public static final String ENTITY_NAME = "relationDegreeDeclinable";
    public static final int VERSION_HASH = -1200499374;
    private static IEntityMeta ENTITY_META;

    public static final String L_PARENT = "parent";

    private RelationDegree _parent;     // Степень родства

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Степень родства. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public RelationDegree getParent()
    {
        return _parent;
    }

    /**
     * @param parent Степень родства. Свойство не может быть null и должно быть уникальным.
     */
    public void setParent(RelationDegree parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RelationDegreeDeclinableGen)
        {
            setParent(((RelationDegreeDeclinable)another).getParent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RelationDegreeDeclinableGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RelationDegreeDeclinable.class;
        }

        public T newInstance()
        {
            return (T) new RelationDegreeDeclinable();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "parent":
                    return obj.getParent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "parent":
                    obj.setParent((RelationDegree) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "parent":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "parent":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "parent":
                    return RelationDegree.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RelationDegreeDeclinable> _dslPath = new Path<RelationDegreeDeclinable>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RelationDegreeDeclinable");
    }
            

    /**
     * @return Степень родства. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.RelationDegreeDeclinable#getParent()
     */
    public static RelationDegree.Path<RelationDegree> parent()
    {
        return _dslPath.parent();
    }

    public static class Path<E extends RelationDegreeDeclinable> extends EntityPath<E>
    {
        private RelationDegree.Path<RelationDegree> _parent;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Степень родства. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninarfu.entity.RelationDegreeDeclinable#getParent()
     */
        public RelationDegree.Path<RelationDegree> parent()
        {
            if(_parent == null )
                _parent = new RelationDegree.Path<RelationDegree>(L_PARENT, this);
            return _parent;
        }

        public Class getEntityClass()
        {
            return RelationDegreeDeclinable.class;
        }

        public String getEntityName()
        {
            return "relationDegreeDeclinable";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
