package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение приказа по абитуриентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuEnrOrderExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt";
    public static final String ENTITY_NAME = "narfuEnrOrderExt";
    public static final int VERSION_HASH = 1262281670;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ORDER = "enrOrder";
    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String P_PROTOCOL_NUMBER = "protocolNumber";

    private EnrOrder _enrOrder;     // Приказ на абитуриентов (2014)
    private Date _protocolDate;     // Дата протокола
    private String _protocolNumber;     // Номер протокола

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ на абитуриентов (2014). Свойство не может быть null.
     */
    @NotNull
    public EnrOrder getEnrOrder()
    {
        return _enrOrder;
    }

    /**
     * @param enrOrder Приказ на абитуриентов (2014). Свойство не может быть null.
     */
    public void setEnrOrder(EnrOrder enrOrder)
    {
        dirty(_enrOrder, enrOrder);
        _enrOrder = enrOrder;
    }

    /**
     * @return Дата протокола.
     */
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Номер протокола.
     */
    @Length(max=255)
    public String getProtocolNumber()
    {
        return _protocolNumber;
    }

    /**
     * @param protocolNumber Номер протокола.
     */
    public void setProtocolNumber(String protocolNumber)
    {
        dirty(_protocolNumber, protocolNumber);
        _protocolNumber = protocolNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NarfuEnrOrderExtGen)
        {
            setEnrOrder(((NarfuEnrOrderExt)another).getEnrOrder());
            setProtocolDate(((NarfuEnrOrderExt)another).getProtocolDate());
            setProtocolNumber(((NarfuEnrOrderExt)another).getProtocolNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuEnrOrderExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuEnrOrderExt.class;
        }

        public T newInstance()
        {
            return (T) new NarfuEnrOrderExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrOrder":
                    return obj.getEnrOrder();
                case "protocolDate":
                    return obj.getProtocolDate();
                case "protocolNumber":
                    return obj.getProtocolNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrOrder":
                    obj.setEnrOrder((EnrOrder) value);
                    return;
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "protocolNumber":
                    obj.setProtocolNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrOrder":
                        return true;
                case "protocolDate":
                        return true;
                case "protocolNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrOrder":
                    return true;
                case "protocolDate":
                    return true;
                case "protocolNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrOrder":
                    return EnrOrder.class;
                case "protocolDate":
                    return Date.class;
                case "protocolNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuEnrOrderExt> _dslPath = new Path<NarfuEnrOrderExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuEnrOrderExt");
    }
            

    /**
     * @return Приказ на абитуриентов (2014). Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt#getEnrOrder()
     */
    public static EnrOrder.Path<EnrOrder> enrOrder()
    {
        return _dslPath.enrOrder();
    }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt#getProtocolNumber()
     */
    public static PropertyPath<String> protocolNumber()
    {
        return _dslPath.protocolNumber();
    }

    public static class Path<E extends NarfuEnrOrderExt> extends EntityPath<E>
    {
        private EnrOrder.Path<EnrOrder> _enrOrder;
        private PropertyPath<Date> _protocolDate;
        private PropertyPath<String> _protocolNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ на абитуриентов (2014). Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt#getEnrOrder()
     */
        public EnrOrder.Path<EnrOrder> enrOrder()
        {
            if(_enrOrder == null )
                _enrOrder = new EnrOrder.Path<EnrOrder>(L_ENR_ORDER, this);
            return _enrOrder;
        }

    /**
     * @return Дата протокола.
     * @see ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(NarfuEnrOrderExtGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Номер протокола.
     * @see ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt#getProtocolNumber()
     */
        public PropertyPath<String> protocolNumber()
        {
            if(_protocolNumber == null )
                _protocolNumber = new PropertyPath<String>(NarfuEnrOrderExtGen.P_PROTOCOL_NUMBER, this);
            return _protocolNumber;
        }

        public Class getEntityClass()
        {
            return NarfuEnrOrderExt.class;
        }

        public String getEntityName()
        {
            return "narfuEnrOrderExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
