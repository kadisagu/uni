/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.IssuanceRegBook.NarfuDipDocumentReportIssuanceRegBook;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.CheckingGraduatesData.NarfuDipDocumentReportCheckingGraduatesData;

/**
 * @author Andrey Avetisov
 * @since 21.04.2015
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("diplomaList", new GlobalReportDefinition("dip", "issuanceRegBook", NarfuDipDocumentReportIssuanceRegBook.class.getSimpleName()))
                .add("checkingGraduatesDataReport",
                        new GlobalReportDefinition("dip", "checkingGraduatesData", NarfuDipDocumentReportCheckingGraduatesData.class.getSimpleName()))
                .create();
    }
}
