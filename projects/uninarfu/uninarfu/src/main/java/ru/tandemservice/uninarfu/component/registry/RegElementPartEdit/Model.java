package ru.tandemservice.uninarfu.component.registry.RegElementPartEdit;

/**
 * @author Andrey Andreev
 * @since 27.10.2015
 */
public class Model extends ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Model
{

    private String sakaiEtalonSiteId = "";


    public void setSakaiEtalonSiteId(String sakaiEtalonSiteId) {
        this.sakaiEtalonSiteId = sakaiEtalonSiteId;
    }

    public String getSakaiEtalonSiteId() {
        return sakaiEtalonSiteId;
    }

}
