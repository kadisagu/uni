package ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.CheckingGraduatesData;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 28.10.2015
 */
@Configuration
public class NarfuDipDocumentReportCheckingGraduatesData extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";
    public static final String STUDENTS_STATUS_DS = "studentsStatusDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDS()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDS()))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, developConditionDS()))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, developPeriodDS()))
                .addDataSource(selectDS(STUDENTS_STATUS_DS, studentsStatusDS()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS, programSubjectDS())
                        .addColumn(EduProgramSubject.titleWithCodeIndexAndGen().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> orgUnitDS()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            String eou_alias = "eou";
            dql.where(in(property(alias),
                    new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, eou_alias)
                            .column(property(EducationOrgUnit.formativeOrgUnit().fromAlias(eou_alias)))
                            .buildQuery()
            ));
            dql.where(like(DQLFunctions.upper(property(alias, OrgUnit.title())), value(CoreStringUtils.escapeLike(filter, true))));

            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), OrgUnit.class).customize(customizer);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> developFormDS()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopForm.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> developConditionDS()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopCondition.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> developPeriodDS()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            Long orgUnitId = context.get(NarfuDipDocumentReportCheckingGraduatesDataUI.PARAM_ORG_UNIT);

            if (orgUnitId != null) {
                String sub_alias = "sub";
                DQLSelectBuilder subDQL = new DQLSelectBuilder()
                        .fromEntity(EducationOrgUnit.class, sub_alias)
                        .column(property(sub_alias, EducationOrgUnit.developPeriod().id()))
                        .where(eq(property(sub_alias, EducationOrgUnit.formativeOrgUnit().id()), value(orgUnitId)));

                dql.where(in(property(alias), subDQL.buildQuery()));
            }

            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), DevelopPeriod.class)
                .customize(customizer);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentsStatusDS()
    {
        return new EntityComboDataSourceHandler(getName(), StudentStatus.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> programSubjectDS()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            Long orgUnitId = context.get(NarfuDipDocumentReportCheckingGraduatesDataUI.PARAM_ORG_UNIT);
            Long developFormId = context.get(NarfuDipDocumentReportCheckingGraduatesDataUI.PARAM_DEVELOP_FORM);
            Long developConditionId = context.get(NarfuDipDocumentReportCheckingGraduatesDataUI.PARAM_DEVELOP_CONDITION);
            Long developPeriodId = context.get(NarfuDipDocumentReportCheckingGraduatesDataUI.PARAM_DEVELOP_PERIOD);

            String sub_alias = "sub";
            DQLSelectBuilder subDQL = new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, sub_alias)
                    .column(property(sub_alias, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject().id()));
            if (orgUnitId != null)
                subDQL.where(eq(property(sub_alias, EducationOrgUnit.formativeOrgUnit().id()), value(orgUnitId)));
            if (developFormId != null)
                subDQL.where(eq(property(sub_alias, EducationOrgUnit.developForm().id()), value(developFormId)));
            if (developConditionId != null)
                subDQL.where(eq(property(sub_alias, EducationOrgUnit.developCondition().id()), value(developConditionId)));
            if (developPeriodId != null)
                subDQL.where(eq(property(sub_alias, EducationOrgUnit.developPeriod().id()), value(developPeriodId)));

            String f = CoreStringUtils.escapeLike(filter, true);

            dql.where(in(property(alias), subDQL.buildQuery()))
                    .where(or(like(DQLFunctions.upper(property(alias, EduProgramSubject.title())), value(f)),
                            like(DQLFunctions.upper(property(alias, EduProgramSubject.subjectCode())), value(f))))
                    .order(property(alias, EduProgramSubject.subjectCode()))
                    .order(property(alias, EduProgramSubject.title()));

            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
                .customize(customizer)
                .pageable(true);
    }
}