package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x8x1_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность orgUnitNumberForEnrEntrantDocuments

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("narfu_org_unit_number_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_84a5e3c9"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("orgunit_id", DBType.LONG).setNullable(false),
                                      new DBColumn("number_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("orgUnitNumberForEnrEntrantDocuments");

        }


    }
}
