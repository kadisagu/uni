/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.EnrEnrollmentCommission.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.ui.Pub.EnrEnrollmentCommissionPubUI;
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt;
import ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen;

/**
* @author Ekaterina Zvereva
* @since 10.06.2015
*/
public class EnrEnrollmentCommissionPubExtUI extends UIAddon
{
    private EnrEnrollmentCommissionPubUI _presenter;
    private EnrEnrollmentCommissionExt _commissionExt;

    public EnrEnrollmentCommissionExt getCommissionExt()
    {
        return _commissionExt;
    }

    public void setCommissionExt(EnrEnrollmentCommissionExt commissionExt)
    {
        _commissionExt = commissionExt;
    }

    public EnrEnrollmentCommissionPubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = getPresenter();
        _commissionExt = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentCommissionExtGen.NaturalId(_presenter.getEnrollmentCommission()));
    }
}