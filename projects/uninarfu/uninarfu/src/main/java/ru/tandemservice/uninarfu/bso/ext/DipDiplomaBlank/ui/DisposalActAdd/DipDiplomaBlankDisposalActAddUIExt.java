/* $Id$ */
package ru.tandemservice.uninarfu.bso.ext.DipDiplomaBlank.ui.DisposalActAdd;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.entity.catalog.DipScriptItem;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipScriptItemCodes;
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.DipDiplomaBlankManager;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.logic.IDipDiplomaBlankDao;
import ru.tandemservice.unidip.bso.bo.DipDiplomaBlank.ui.DisposalActAdd.DipDiplomaBlankDisposalActAddUI;
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank;
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes;
import ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 23.03.2016
 */
public class DipDiplomaBlankDisposalActAddUIExt extends UIAddon
{
    public DipDiplomaBlankDisposalActAddUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }


    public void onClickPrint()
    {
        DipDiplomaBlankDisposalActAddUI presenter = getPresenter();

        IDipDiplomaBlankDao dao = DipDiplomaBlankManager.instance().diplomaBlankDao();
        List<DipDiplomaBlank> blanks = dao.getShownBlanks(presenter.getBlankIds(), presenter.getHideIssued());

        Date periodFrom = presenter.getSettings().get("periodFrom", Date.class);
        Date periodTo = presenter.getSettings().get("periodTo", Date.class);

        Employee employee = presenter.getSettings().get("employee", Employee.class);
        NarfuBSOVisasTemplate visaTemplate = presenter.getSettings().get("visaTemplate", NarfuBSOVisasTemplate.class);

        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                IUniBaseDao.instance.get().getCatalogItem(DipScriptItem.class, DipScriptItemCodes.BLANK_DISPOSAL_ACT),
                "blanks", blanks,
                "blanksToExtracts", getBlankToExtracts(blanks),
                "destructionDate", presenter.getDestructionDate(),
                "visaTemplateId", visaTemplate == null ? null : visaTemplate.getId(),
                "employeeId", employee == null ? null : employee.getId(),
                "periodFrom", periodFrom,
                "periodTo", periodTo);

        CommonBaseRenderer renderer = new CommonBaseRenderer().rtf().document((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT)).fileName((String) scriptResult.get(IScriptExecutor.FILE_NAME));
        BusinessComponentUtils.downloadDocument(renderer, true);

        presenter.deactivate();
    }

    private Map<DipDiplomaBlank, DipStuExcludeExtract> getBlankToExtracts(List<DipDiplomaBlank> blanks)
    {
        if (((DipDiplomaBlankDisposalActAddUI) getPresenter()).getHideIssued())
            return null;

        List<DipDiplomaBlank> issuedBlanks = blanks.stream()
                .filter(blank -> blank.getBlankState().getCode().equals(DipBlankStateCodes.ISSUED))
                .collect(Collectors.toList());
        return DipDiplomaBlankManager.instance().diplomaBlankDao().getDiplomaBlanksToExcludeExtracts(issuedBlanks);
    }
}