package ru.tandemservice.uninarfu.ws.sakai.impl;

import com.google.common.primitives.Longs;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.movestudentrmc.entity.*;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentExtractStatesCodes;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes;
import ru.tandemservice.movestudentrmc.util.RepresentationUtil;
import ru.tandemservice.uninarfu.ws.sakai.types.WsOrderWrap;
import ru.tandemservice.uninarfu.ws.sakai.types.WsPaymentWrap;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unitutor.ws.impl.DisciplinesManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class WsSakaiManager extends DisciplinesManager implements IWsSakaiManager {

    public static IWsSakaiManager wsSakaiManager;

    public static IWsSakaiManager getWsSakaiManager() {

        if (wsSakaiManager == null)
            wsSakaiManager = (IWsSakaiManager) ApplicationRuntime.getBean(IWsSakaiManager.class.getName());
        return wsSakaiManager;
    }

    @Override
    public List<WsOrderWrap> getWsOrders(String studentParam) {
        Long studentId = Longs.tryParse(studentParam);

        Student student = (studentId == null)? null : DataAccessServices.dao().get(Student.class, studentId);

        if (student == null)
            return new ArrayList<>();

        List<WsOrderWrap> orderWrapList = new ArrayList<>();
        orderWrapList.addAll(getWrapDocOrderRepresents(student));
        orderWrapList.addAll(getWrapListOrderRepresents(student));
        orderWrapList.addAll(getWrapOrderList(student));

        Collections.sort(orderWrapList, (wrap1, wrap2) -> {
            SimpleDateFormat formatter = new SimpleDateFormat(DateFormatter.PATTERN_DEFAULT);
            try {
                Date orderDate1 = formatter.parse(wrap1.getOrderDate());
                Date orderDate2 = formatter.parse(wrap2.getOrderDate());

                return orderDate2.compareTo(orderDate1);
            }
            catch (ParseException ignored) {
            }

            return 0;
        });

        return orderWrapList;
    }

    /**
     * Представления
     */
    private List<WsOrderWrap> getWrapDocOrderRepresents(Student student) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, DocOrdRepresent.ENTITY_NAME)
                .column(DocOrdRepresent.ENTITY_NAME)
                .joinEntity(DocOrdRepresent.ENTITY_NAME, DQLJoinType.left, DocRepresentStudentBase.class, "stud",
                            eq(property(DocOrdRepresent.representation().id().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                               property(DocRepresentStudentBase.representation().id().fromAlias("stud"))))
                .where(eq(property(DocRepresentStudentBase.student().fromAlias("stud")), value(student)))
                .where(eq(property(DocOrdRepresent.representation().state().code().fromAlias(DocOrdRepresent.ENTITY_NAME)),
                          value(MovestudentExtractStatesCodes.CODE_6)));

        List<DocOrdRepresent> rowList = builder.createStatement(getSession()).list();
        List<WsOrderWrap> resultList = new ArrayList<>();

        for (DocOrdRepresent orderData : rowList) {
            WsOrderWrap wrap = new WsOrderWrap(orderData.getRepresentation().getRepresentationTitle(), orderData.getOrder().getNumber(), DateFormatter.DEFAULT_DATE_FORMATTER.format(orderData.getOrder().getCommitDate()));
            // Вступает в силу
            Date orderStartDate = orderData.getRepresentation().getStartDate();
            if (orderStartDate != null) {
                wrap.setOrderStartDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(orderStartDate));
            }
            // дата с
            Date startDate = RepresentationUtil.getStartDateRepresentation(orderData.getRepresentation());
            if (startDate != null) {
                wrap.setStartDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(startDate));
            }
            // дата по
            Date endDate = RepresentationUtil.getEndDateRepresentation(orderData.getRepresentation());
            if (endDate != null) {
                wrap.setEndDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
            }
            resultList.add(wrap);
        }

        return resultList;
    }

    private List<WsOrderWrap> getWrapListOrderRepresents(Student student) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, "o")
                .joinEntity("o", DQLJoinType.left, RelListRepresentStudents.class, "srep", eq(property(RelListRepresentStudents.representation().fromAlias("srep")), property(ListOrdListRepresent.representation().fromAlias("o"))))
                .column("o")
                .where(eq(property(RelListRepresentStudents.student().fromAlias("srep")), value(student)))
                .where(eqValue(property(ListOrdListRepresent.order().state().code().fromAlias("o")), MovestudentOrderStatesCodes.CODE_5));

        List<ListOrdListRepresent> rowList = builder.createStatement(getSession()).list();
        List<WsOrderWrap> resultList = new ArrayList<>();

        for (ListOrdListRepresent orderData : rowList) {
            WsOrderWrap wrap = new WsOrderWrap(orderData.getRepresentation().getRepresentationTitle(), orderData.getOrder().getNumber(), DateFormatter.DEFAULT_DATE_FORMATTER.format(orderData.getOrder().getCommitDate()));
            // Вступает в силу
            Date orderStartDate = orderData.getOrder().getCommitDate();
            if (orderStartDate != null) {
                wrap.setOrderStartDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(orderStartDate));
            }
            // дата с
            Date startDate = RepresentationUtil.getStartDateListRepresentation(orderData.getRepresentation());
            if (startDate != null) {
                wrap.setStartDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(startDate));
            }
            // дата по
            Date endDate = RepresentationUtil.getEndDateListRepresentation(orderData.getRepresentation());
            if (endDate != null) {
                wrap.setEndDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
            }
            resultList.add(wrap);
        }

        return resultList;
    }

    private List<WsOrderWrap> getWrapOrderList(Student student) {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(OrderList.class, "ol")
                .where(eq(property(OrderList.student().fromAlias("ol")), value(student)))
                .column("ol");

        List<OrderList> rowList = builder.createStatement(getSession()).list();
        List<WsOrderWrap> resultList = new ArrayList<>();

        for (OrderList orderData : rowList) {
            WsOrderWrap wrap = new WsOrderWrap(orderData.getType().getTitle(), orderData.getOrderNumber(), DateFormatter.DEFAULT_DATE_FORMATTER.format(orderData.getOrderDate()));
            // Вступает в силу не заполняем RM#6706
            // дата с
            Date startDate = orderData.getOrderDateStart();
            if (startDate != null) {
                wrap.setStartDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(startDate));
            }
            // дата по
            Date endDate = orderData.getOrderDateStop();
            if (endDate != null) {
                wrap.setEndDate(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate));
            }
            resultList.add(wrap);
        }

        return resultList;
    }

    @Override
    public WsPaymentWrap getPaymentData(String studentParam) {

        Long studentId = Longs.tryParse(studentParam);
        Student student = (studentId == null)? null : DataAccessServices.dao().get(Student.class, studentId);
        if (student == null)
            return null;

        PersonNARFU personNARFU = get(PersonNARFU.class, PersonNARFU.person(), student.getPerson());

        if (personNARFU != null && personNARFU.isNeedSocialPayment())
        {
            WsPaymentWrap paymentWrap = new WsPaymentWrap();
            paymentWrap.setTitle("Срок действия документа для социальной выплаты - ");
            Date expiredDate = personNARFU.getDocumentExpiredDate();
            paymentWrap.setExpiredDate(expiredDate == null? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(expiredDate));

            return paymentWrap;
        }
        return null;
    }

}
