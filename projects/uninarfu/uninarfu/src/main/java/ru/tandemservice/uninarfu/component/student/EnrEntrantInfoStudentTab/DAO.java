package ru.tandemservice.uninarfu.component.student.EnrEntrantInfoStudentTab;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.person.base.entity.PersonDocumentRoleRel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEnrollmentStepStateCodes;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.EnrEntrantManager;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void delete(IEntity entity)
    {
        super.delete(entity);
    }

    @Override
    public void delete(Long entityId)
    {
        super.delete(entityId);
    }

    public void prepare(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrant.class, "e")
                .where(eq(property("e", EnrEntrant.person()), value(model.getStudent().getPerson())))
                .order(property("e", EnrEntrant.enrollmentCampaign().educationYear().intValue()))
                .order(property("e", EnrEntrant.enrollmentCampaign().title()));

        List<EnrEntrant> enrEntrants = dql.createStatement(getSession()).list();

        model.setExistEntrant(!enrEntrants.isEmpty());

        if(model.isExistEntrant())
        {
            model.setEntrant(enrEntrants.get(0));
        }

        model.setEnrCampaignModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrant.class, "b").column("b");

                if(filter != null)
                {
                    builder.where(likeUpper(property(EnrEntrant.enrollmentCampaign().title().fromAlias("b")), value(CoreStringUtils.escapeLike(filter))));
                }

                builder.where(in(property("b", EnrEntrant.id()), CommonBaseEntityUtil.getIdList(enrEntrants)));

                if (o != null)
                {
                    builder.where(eqValue(property(EnrEntrant.id().fromAlias("b")), o));
                }

                builder.order(property(EnrEntrant.enrollmentCampaign().educationYear().intValue().fromAlias("b")), OrderDirection.desc);
                builder.order(property(EnrEntrant.enrollmentCampaign().title().fromAlias("b")), OrderDirection.desc);

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EnrEntrant) value).getEnrollmentCampaign().getTitle();
            }
        });

        prepareRatingData(model);
    }


    @Override
    public void prepareStateExamResultDS(Model model)
    {
        DynamicListDataSource<EnrEntrantStateExamResult> dataSource = model.getStateExamResultDS();

        String alias = "eeser";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrEntrantStateExamResult.class, alias)
                .where(eq(property(alias, EnrEntrantStateExamResult.entrant().id()), value(model.getEntrant().getId())
                ))
                .fetchPath(DQLJoinType.left, EnrEntrantStateExamResult.subject().fromAlias(alias), "subj")
                .fetchPath(DQLJoinType.left, EnrEntrantStateExamResult.secondWaveExamPlace().fromAlias(alias), "swp")
                .order(property(alias, dataSource.getEntityOrder().getKeyString()),
                        dataSource.getEntityOrder().getDirection());

        int count = ISharedBaseDao.instance.get().getCount(dql);
        dataSource.setCountRow(count > 0 ? count : 1);

        UniBaseUtils.createPage(dataSource, dql, getSession());
    }

    @Override
    public void prepareOlympiadDiplomaDS(Model model)
    {
        DynamicListDataSource<EnrOlympiadDiploma> dataSource = model.getOlympiadDiplomaDS();

        String alias = "eod";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrOlympiadDiploma.class, alias)
                .fetchPath(DQLJoinType.left, EnrOlympiadDiploma.olympiad().fromAlias(alias), "oly")
                .fetchPath(DQLJoinType.left, EnrOlympiadDiploma.olympiad().olympiadType().fromAlias(alias), "olyt")
                .fetchPath(DQLJoinType.left, EnrOlympiadDiploma.honour().fromAlias(alias), "hon")
                .fetchPath(DQLJoinType.left, EnrOlympiadDiploma.settlement().fromAlias(alias), "setl")
                .where(exists(new DQLSelectBuilder().fromEntity(PersonDocumentRoleRel.class, "rr")
                        .where(eq(property("oly"), property("rr", PersonDocumentRoleRel.document())))
                        .where(eq(property("rr", PersonDocumentRoleRel.role()), value(model.getEntrant())))
                        .buildQuery()))
                .order(property(alias, dataSource.getEntityOrder().getKeyString()),
                        dataSource.getEntityOrder().getDirection());

        int count = ISharedBaseDao.instance.get().getCount(dql);
        dataSource.setCountRow(count > 0 ? count : 1);

        UniBaseUtils.createPage(dataSource, dql, getSession());
    }

    @Override
    public void prepareRatingData(Model model)
    {
        EnrEntrant entrant = model.getEntrant();

        model.setEntranceExamIncorrect(EnrEntrantManager.instance().dao().isEntranceExamIncorrect(entrant));
        model.setExamListIncorrect(EnrEntrantManager.instance().dao().isExamListIncorrect(entrant));

        model.setRequestTypeMap(Maps.newTreeMap(new Comparator<EnrRequestType>()
        {
            @Override
            public int compare(EnrRequestType t1, EnrRequestType t2)
            {
                return t1.getPriority() - t2.getPriority();
            }
        }));

        List<EnrEntrantAchievement> entrantAchievementList = IUniBaseDao.instance.get()
                .getList(EnrEntrantAchievement.class, EnrEntrantAchievement.entrant(), entrant);
        Map<EnrRequestType, List<EnrEntrantAchievement>> entrantAchievementMap = Maps.newHashMap();
        for (EnrEntrantAchievement achievement : entrantAchievementList) {
            EnrRequestType type = achievement.getType().getAchievementKind().getRequestType();
            if (!entrantAchievementMap.containsKey(type))
                entrantAchievementMap.put(type, Lists.<EnrEntrantAchievement>newArrayList());
            entrantAchievementMap.get(type).add(achievement);
        }

        for (EnrRequestedCompetition requestedCompetition : IUniBaseDao.instance.get()
                .getList(EnrRequestedCompetition.class, EnrRequestedCompetition.request().entrant(), entrant)) {
            if (requestedCompetition.getRequest().isTakeAwayDocument())
                continue;

            Model.RatingRowWrapper wrapper = new Model.RatingRowWrapper(requestedCompetition);

            EnrRequestType type = requestedCompetition.getRequest().getType();
            List<EnrEntrantAchievement> achievements = entrantAchievementMap.get(type);
            if (null != achievements)
                wrapper.setEntrantAchievementList(achievements);

            Map<EnrRequestedCompetition, Model.RatingRowWrapper> map = Maps.newHashMap();
            map.put(requestedCompetition, wrapper);

            if (!model.getRequestTypeMap().containsKey(type))
                model.getRequestTypeMap().put(type, Lists.<Map<EnrRequestedCompetition, Model.RatingRowWrapper>>newArrayList());
            model.getRequestTypeMap().get(type).add(map);
        }

        List<EnrRatingItem> ratingItemList = IUniBaseDao.instance.get().getList(EnrRatingItem.class, EnrRatingItem.entrant(), entrant);
        List<EnrChosenEntranceExam> examList = IUniBaseDao.instance.get().getList(EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition().request().entrant(), entrant);
        List<EnrEnrollmentExtract> extractList = IUniBaseDao.instance.get().getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.entity().request().entrant(), entrant);

        List<EnrEnrollmentStepItem> stepItemList = new DQLSelectBuilder().fromEntity(EnrEnrollmentStepItem.class, "s").column("s")
                .where(eq(property(EnrEnrollmentStepItem.entity().requestedCompetition().request().entrant().fromAlias("s")), value(entrant)))
                .where(ne(property(EnrEnrollmentStepItem.step().state().code().fromAlias("s")), value(EnrEnrollmentStepStateCodes.CLOSED)))
                .createStatement(getSession()).list();

        for (Map.Entry<EnrRequestType, List<Map<EnrRequestedCompetition, Model.RatingRowWrapper>>> entry
                : model.getRequestTypeMap().entrySet()) {
            for (Map<EnrRequestedCompetition, Model.RatingRowWrapper> map : entry.getValue()) {
                for (EnrRatingItem rating : ratingItemList) {
                    Model.RatingRowWrapper wrapper = map.get(rating.getRequestedCompetition());
                    if (null != wrapper) wrapper.setRating(rating);
                }
                for (EnrChosenEntranceExam exam : examList) {
                    Model.RatingRowWrapper wrapper = map.get(exam.getRequestedCompetition());
                    if (null != wrapper) wrapper.getExams().add(exam);
                }
                for (EnrEnrollmentExtract extract : extractList) {
                    Model.RatingRowWrapper wrapper = map.get(extract.getEntity());
                    if (null != wrapper) wrapper.setExtract(extract);
                }
                for (EnrEnrollmentStepItem stepItem : stepItemList) {
                    Model.RatingRowWrapper wrapper = map.get(stepItem.getEntity().getRequestedCompetition());
                    if (null != wrapper) wrapper.getEnrollmentItemList().add(stepItem);
                }
            }
        }
    }
}
