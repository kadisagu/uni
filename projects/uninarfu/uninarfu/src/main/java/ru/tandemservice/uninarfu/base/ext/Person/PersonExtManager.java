/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.Person;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import ru.tandemservice.uninarfu.base.ext.Person.logic.NarfuPersonDAO;

/**
 * @author Andrey Avetisov
 * @since 20.10.2015
 */

@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IPersonDao dao()
    {
        return new NarfuPersonDAO();
    }
}
