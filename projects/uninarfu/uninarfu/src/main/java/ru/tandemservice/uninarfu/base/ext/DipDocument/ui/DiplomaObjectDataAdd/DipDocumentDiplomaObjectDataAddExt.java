/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DipDocument.ui.DiplomaObjectDataAdd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.DiplomaObjectDataAdd.DipDocumentDiplomaObjectDataAdd;

/**
 * @author Alexey Lopatin
 * @since 28.05.2015
 */
@Configuration
public class DipDocumentDiplomaObjectDataAddExt extends BusinessComponentExtensionManager
{
    @Autowired
    private DipDocumentDiplomaObjectDataAdd _diplomaObjectDataAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_diplomaObjectDataAdd.presenterExtPoint())
                .addAction(new DipDocumentOrgUnitStudentTabClickContinueAction("onClickContinue"))
                .create();
    }
}