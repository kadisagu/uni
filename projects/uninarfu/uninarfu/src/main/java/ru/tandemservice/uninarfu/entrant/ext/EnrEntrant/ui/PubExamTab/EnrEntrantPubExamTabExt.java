/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubExamTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab.EnrEntrantPubExamTab;
import ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubRequestTab.NarfuEntrantRequestPrintExamList;


/**
 * @author Ekaterina Zvereva
 * @since 16.04.2015
 */
@Configuration
public class EnrEntrantPubExamTabExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "narfuEnrEntrantPubExamTabExt";

    @Autowired
    private EnrEntrantPubExamTab _enrEntrantPubExamTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantPubExamTab.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantPubExamTabExtUI.class))
                .addAction(new NarfuEntrantPrintExamList("onClickPrintEnrollmentExamSheet"))
                .create();
    }
}