/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic.INarfuEnrReportDao;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic.NarfuEmployeeEnrCommissionDSHandler;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic.NarfuEnrReportDao;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic.NarfuEnrollmentCommissionDSHandler;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.logic.INarfuEnrReportComissionMeetingProtocolDao;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.logic.NarfuEnrReportComissionMeetingProtocolDao;

/**
 * @author Andrey Avetisov
 * @since 09.04.2015
 */
@Configuration
public class NarfuEnrReportManager extends BusinessObjectManager
{
    public static NarfuEnrReportManager instance()
    {
        return instance(NarfuEnrReportManager.class);
    }

    @Bean
    public INarfuEnrReportDao dao()
    {
        return new NarfuEnrReportDao();
    }

    @Bean
    public IDefaultComboDataSourceHandler getEmployeeEnrCommissionDSHandler()
    {
        return new NarfuEmployeeEnrCommissionDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler getEmrollmentCommissionDSHandler()
    {
        return new NarfuEnrollmentCommissionDSHandler(getName());
    }

    @Bean
    public INarfuEnrReportComissionMeetingProtocolDao enrollmentCommissionMeetingProtocolDao()
    {
        return new NarfuEnrReportComissionMeetingProtocolDao();
    }
}
