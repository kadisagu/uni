/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuWorkPlan;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Andrey Andreev
 * @since 29.03.2016
 */
@Configuration
public class NarfuWorkPlanManager extends BusinessObjectManager
{
    public static NarfuWorkPlanManager instance()
    {
        return instance(NarfuWorkPlanManager.class);
    }

}