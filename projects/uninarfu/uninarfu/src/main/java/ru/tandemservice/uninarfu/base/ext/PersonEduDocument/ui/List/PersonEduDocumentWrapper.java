/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.List;

import com.sun.istack.Nullable;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.logic.PersonEduDocumentDSHandler;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

/**
 * @author DMITRY KNYAZEV
 * @since 06.04.2015
 */
public final class PersonEduDocumentWrapper extends ViewWrapper<PersonEduDocument>
{

    public static final String DATE_ISSUE_CERTIFICATE = "dateIssueCertificate";
    public static final String EXPECTED_DATE_RETURN = "expectedDateReturn";
    public static final String ACTUAL_DATE_RETURN = "actualDateReturn";

    public PersonEduDocumentWrapper(PersonEduDocument entity, @Nullable PersonEduDocumentNARFU addon, ISecureRoleContext personRoleContext)
    {
        super(entity);
        String dateIssueCertificate = "";
        String expectedDateReturn = "";
        String actualDateReturn = "";
        if (addon != null)
        {
            dateIssueCertificate = DateFormatter.DEFAULT_DATE_FORMATTER.format(addon.getDateIssueCertificate());
            expectedDateReturn = DateFormatter.DEFAULT_DATE_FORMATTER.format(addon.getExpectedDateReturn());
            actualDateReturn = DateFormatter.DEFAULT_DATE_FORMATTER.format(addon.getActualDateReturn());
        }
        setViewProperty(DATE_ISSUE_CERTIFICATE, dateIssueCertificate);
        setViewProperty(EXPECTED_DATE_RETURN, expectedDateReturn);
        setViewProperty(ACTUAL_DATE_RETURN, actualDateReturn);

        ParametersMap map = new ParametersMap();
        map.add(PublisherActivator.PUBLISHER_ID_KEY, getId());
        map.add("securedObjectId", personRoleContext.getSecuredObject().getId());
        map.add("accessible", personRoleContext.isAccessible());
        map.add("personRoleName", personRoleContext.getPersonRoleName());
        setViewProperty("pubParameters", map);

		boolean isMainDoc = entity.getPerson().getMainEduDocument() != null && entity.getId().equals(entity.getPerson().getMainEduDocument().getId());
		setViewProperty(PersonEduDocumentDSHandler.COLUMN_MAIN_DOCUMENT, isMainDoc);
    }
}
