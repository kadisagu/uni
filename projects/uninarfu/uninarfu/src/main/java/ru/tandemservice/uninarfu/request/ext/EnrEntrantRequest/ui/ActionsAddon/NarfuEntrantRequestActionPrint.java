/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.Map;


/**
 * @author Ekaterina Zvereva
 * @since 06.05.2015
 */
public class NarfuEntrantRequestActionPrint extends NamedUIAction
{
    public NarfuEntrantRequestActionPrint(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        UIAddon addon = (UIAddon) presenter.getConfig().getAddon("EnrEntrantRequestActionsAddon");
        if (addon == null) return;
        String templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_BS;
        switch (((EnrEntrantRequestActionsAddon)addon).getEntrantRequest().getType().getCode())
        {
            case EnrRequestTypeCodes.MASTER:
                templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_MASTER;
                break;
            case EnrRequestTypeCodes.SPO:
                templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_SECONDARY;
                break;
            case EnrRequestTypeCodes.HIGHER:
            case EnrRequestTypeCodes.POSTGRADUATE:
            case EnrRequestTypeCodes.TRAINEESHIP:
            case EnrRequestTypeCodes.INTERNSHIP:
                templateCode = EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_HIGHER;
                break;
        }
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, templateCode);

        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, presenter.getListenerParameterAsLong());
        byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        if (document != null)
            scriptResult.put(IScriptExecutor.DOCUMENT,  insertOperatorData(document));

        if(scriptItem.isPrintPdf())
            UniRtfUtil.downloadRtfScriptResultAsPdf(scriptResult);
        else
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document((byte[])scriptResult.get(IScriptExecutor.DOCUMENT)).rtf().fileName((String) scriptResult.get(IScriptExecutor.FILE_NAME).toString()), false);
    }

    private byte[] insertOperatorData(byte[] document)
    {
        final RtfDocument docRtf = new RtfReader().read(document);
        RtfInjectModifier im = new RtfInjectModifier();
        //Подпись принявшего документы
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();

        im.put("userPost", principalContext == null? "" : principalContext.getContextTitle());
        im.put("userFIO", principalContext == null? ""  : principalContext.getFio());

        im.modify(docRtf);
        return RtfUtil.toByteArray(docRtf);
    }

}