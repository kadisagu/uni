package ru.tandemservice.uninarfu.base.bo.NarfuEduPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramOrientation;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList.EppEduPlanHigherProfDSHandler;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfList.EppEduPlanSecondaryProfDSHandler;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.logic.INarfuEduPlanDAO;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.logic.INarfuEduPlanPrintDAO;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.logic.NarfuEduPlanDAO;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.logic.NarfuEduPlanPrintDAO;
import ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEditExt;
import ru.tandemservice.uninarfu.eduplan.ext.EppEduPlanVersion.ui.BlockListEdit.EppEduPlanVersionBlockListEditExt;


import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 27.03.2015
 */
@Configuration
public class NarfuEduPlanManager extends BusinessObjectManager
{
    public static NarfuEduPlanManager instance()
    {
        return instance(NarfuEduPlanManager.class);
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduPlanHigherProfListDSHandler()
    {
        return new EppEduPlanHigherProfDSHandler(getName())
        {
            @Override
            protected void applyOrgUnitFilter(DQLSelectBuilder builder, String alias, OrgUnit orgUnit)
            {
                if (orgUnit != null) {
                    builder.where(or(
                            eq(property(alias, EppEduPlan.owner()), value(orgUnit)),
                            exists(new DQLSelectBuilder()
                                    .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(orgUnit)))
                                    .buildQuery())

                    ));
                }
            }
        };
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduPlanSecondaryProfListDSHandler()
    {
        return new EppEduPlanSecondaryProfDSHandler(getName())
        {
            @Override
            protected void applyOrgUnitFilter(DQLSelectBuilder builder, String alias, OrgUnit orgUnit)
            {
                if (orgUnit != null) {
                    builder.where(or(
                            eq(property(alias, EppEduPlanSecondaryProf.owner()), value(orgUnit)),
                            eq(property(alias, EppEduPlanSecondaryProf.ownerOrgUnit().orgUnit()), value(orgUnit)),
                            exists(new DQLSelectBuilder()
                                    .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(orgUnit)))
                                    .buildQuery())

                    ));
                }
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduPlanVerBlockLevelHSchoolHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
        {

            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                OrgUnit orgUnitOwner = context.get(EppEduPlanVersionBlockListEditExt.ORG_UNIT_KEY);
                if (orgUnitOwner == null)
                    return new DSOutput(input);
                return super.execute(input, context);
            }

            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);

                OrgUnit orgUnit = ep.context.get(EppEduPlanVersionBlockListEditExt.ORG_UNIT_KEY);
                EduProgramSpecialization programSubject = ep.context.get(EppEduPlanVersionBlockListEditExt.PROGRAM_SUBJECT_KEY);
                EppEduPlanVersion version = ep.context.get(EppEduPlanVersionBlockListEditExt.VERSION_KEY);


                EppEduPlanProf eduPlanProf = (EppEduPlanProf) version.getEduPlan(); // ClassCasException если не проф. образование
                EduProgramQualification qualification = eduPlanProf.getProgramQualification();
                EduProgramOrientation orientation = eduPlanProf instanceof EppEduPlanHigherProf ? ((EppEduPlanHigherProf) eduPlanProf).getProgramOrientation() : null;

                ep.dqlBuilder.where(or(
                        eq(property("e", EducationLevelsHighSchool.educationLevel().eduProgramSpecialization()), value(programSubject)),
                        and(isNull(property("e", EducationLevelsHighSchool.educationLevel().eduProgramSpecialization())),
                                eq(property("e", EducationLevelsHighSchool.educationLevel().eduProgramSubject()), value(eduPlanProf.getProgramSubject())))));
                ep.dqlBuilder.where(eq(property("e", EducationLevelsHighSchool.assignedQualification()), value(qualification)));
                ep.dqlBuilder.where(eq(property("e", EducationLevelsHighSchool.programOrientation()), value(orientation)));
                ep.dqlBuilder.where(eq(property("e", EducationLevelsHighSchool.orgUnit()), value(orgUnit)));

            }
        };
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduPlanEduLevelHSchoolHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                //OrgUnit orgUnit = ep.context.get(EppEduPlanHigherProfAddEditExt.ORG_UNIT);
                EduProgramSubject programSubject = ep.context.get(EppEduPlanHigherProfAddEditExt.PROGRAM_SUBJECT);
                EduProgramQualification qualification = ep.context.get(EppEduPlanHigherProfAddEditExt.QUALIFICATION);
                EduProgramOrientation orientation = ep.context.get(EppEduPlanHigherProfAddEditExt.ORIENTATION);
                ep.dqlBuilder.where(eq(property("e", EducationLevelsHighSchool.educationLevel().eduProgramSubject()), value(programSubject)));
                ep.dqlBuilder.where(eq(property("e", EducationLevelsHighSchool.assignedQualification()), value(qualification)));
                ep.dqlBuilder.where(eq(property("e", EducationLevelsHighSchool.programOrientation()), value(orientation)));
//                if (orgUnit != null)
//                    ep.dqlBuilder.where(eq(property("e", EducationLevelsHighSchool.orgUnit()), value(orgUnit)));

            }
        };
    }

    @Bean
    public INarfuEduPlanDAO getEduPlanDao()
    {
        return new NarfuEduPlanDAO();
    }

    @Bean
    public INarfuEduPlanPrintDAO printDao()
    {
        return new NarfuEduPlanPrintDAO();
    }


}