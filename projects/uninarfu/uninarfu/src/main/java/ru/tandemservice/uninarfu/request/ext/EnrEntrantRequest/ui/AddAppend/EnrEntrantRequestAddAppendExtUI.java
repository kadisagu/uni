/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.AddAppend;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14.catalog.entity.EnrOriginalSubmissionAndReturnWay;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOriginalSubmissionAndReturnWayCodes;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppendUI;

/**
 * @author Ekaterina Zvereva
 * @since 11.06.2015
 */
public class EnrEntrantRequestAddAppendExtUI extends UIAddon
{
    private EnrEntrantRequestAddAppendUI _parent;

    public EnrEntrantRequestAddAppendExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        _parent =  getPresenter();
        EnrOriginalSubmissionAndReturnWay lichno = DataAccessServices.dao().getByCode(EnrOriginalSubmissionAndReturnWay.class, EnrOriginalSubmissionAndReturnWayCodes.LICHNO);
           if (_parent.getEntrantRequest().getOriginalSubmissionWay() == null)
               _parent.getEntrantRequest().setOriginalSubmissionWay(lichno);
        if (_parent.getEntrantRequest().getOriginalReturnWay() == null)
            _parent.getEntrantRequest().setOriginalReturnWay(lichno);
    }
}