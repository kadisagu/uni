/* $Id$ */
package ru.tandemservice.uninarfu.component.sessionBulletin.SessionBulletinMark;

import org.tandemframework.core.component.IBusinessComponent;

/**
 * @author Ekaterina Zvereva
 * @since 05.05.2015
 */
public class Controller extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Controller
{
    public void onClickMassMarkApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);

        ((IDAO)getDao()).setMassMark(model);
    }

}
