package ru.tandemservice.uninarfu.ws.sakai;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.interceptor.InInterceptors;
import ru.tandemservice.uninarfu.ws.sakai.impl.WsSakaiManager;
import ru.tandemservice.uninarfu.ws.sakai.types.WsOrderWrap;
import ru.tandemservice.uninarfu.ws.sakai.types.WsPaymentWrap;
import ru.tandemservice.unitutor.ws.WsDisciplines;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.List;

@WebService
@InInterceptors(interceptors = {"ru.tandemservice.unitutor.ws.interceptors.WsAuthorizationInterceptor"})
public class WsSakai extends WsDisciplines implements IWsSakai {

    @Override
    @WebMethod
    @WebResult(name = "orderWrap")
    public List<WsOrderWrap> getWsOrders(@WebParam(name = "studentParam") String studentParam) throws Exception {
        if (!StringUtils.isEmpty(studentParam)) {
            List<WsOrderWrap> ordersList = WsSakaiManager.getWsSakaiManager().getWsOrders(studentParam);
            return ordersList;
        }

        return null;
    }

    @Override
    @WebResult(name = "paymentWrap")
    public WsPaymentWrap getPaymentData(@WebParam(name = "studentParam") String studentParam) throws Exception {
        if (!StringUtils.isEmpty(studentParam)) {
            return WsSakaiManager.getWsSakaiManager().getPaymentData(studentParam);
        }

        return null;
    }
}
