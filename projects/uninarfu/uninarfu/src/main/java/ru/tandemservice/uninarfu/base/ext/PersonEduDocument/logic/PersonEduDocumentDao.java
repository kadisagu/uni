/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.List.PersonEduInstitutionWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 04.04.2015
 */
@SuppressWarnings("deprecation")
public class PersonEduDocumentDao extends org.tandemframework.shared.person.base.bo.PersonEduDocument.logic.PersonEduDocumentDao implements IPersonEduDocumentDao
{

    @Override
    public List<PersonEduInstitutionWrapper> getPersonEduInstitutionWrapperList(Person person, ISecureRoleContext personRoleContext)
    {

        final String alias = "e";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .column(alias)
                .column("n")
                .fromEntity(PersonEduInstitution.class, alias)
                .where(eq(property(PersonEduInstitution.person().fromAlias(alias)), commonValue(person.getId())))
                .order(property(PersonEduInstitution.yearEnd().fromAlias(alias)), OrderDirection.desc)
                .order(property(PersonEduInstitution.issuanceDate().fromAlias(alias)), OrderDirection.desc)
                .order(property(PersonEduInstitution.creationDate().fromAlias(alias)), OrderDirection.desc)
                .joinEntity(alias, DQLJoinType.left, PersonEduInstitutionNARFU.class, "n", eq(property(alias, PersonEduInstitution.P_ID), property("n", PersonEduInstitutionNARFU.L_BASE)));
        final List<Object[]> list = builder.createStatement(getSession()).list();

        if (list.isEmpty()) return Collections.emptyList();

        List<PersonEduInstitutionWrapper> result = new ArrayList<>();
        for (Object[] obj : list)
        {
            final PersonEduInstitution eduInstitution = (PersonEduInstitution) obj[0];
            final PersonEduInstitutionNARFU eduInstitutionNARFU = (PersonEduInstitutionNARFU) obj[1];
            result.add(new PersonEduInstitutionWrapper(eduInstitution, eduInstitutionNARFU, personRoleContext));
        }
        return result;
    }

    @Transactional
    @Override
    public int cleanStudentsWithEduDocument(Long eduDocumentId)
    {
        final int res = new DQLUpdateBuilder(Student.class).setValue(Student.eduDocument().s(), null)
                .where(eq(property(Student.eduDocument().id()), value(eduDocumentId)))
                .createStatement(getSession())
                .execute();
        return res;
    }
}
