/* $Id$ */
package ru.tandemservice.uninarfu.attestation.ext.AttestationBulletin.ui.ManualMark;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.brsrmc.util.BrsUtil;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.ManualMark.AttestationBulletinManualMarkUI;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.unisession.entity.catalog.SessionAttestationMarkCatalogItem;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 08.12.2015
 */
public class AttestationBulletinManualMarkExtUI extends UIAddon
{
    private SessionAttestationMarkCatalogItem _massMark;
    private AttestationBulletinManualMarkUI _presenter;
    private boolean notUsedBRS;

    public AttestationBulletinManualMarkExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
        _presenter = (AttestationBulletinManualMarkUI) presenter;
    }

    @Override
    public void onComponentRefresh()
    {
        notUsedBRS = !BrsUtil.getUseRecordBookInSessionMode();
    }

    public void onClickMassMarkApply()
    {
        List<SessionAttestationSlot> slotList = _presenter.getSlotList();
        slotList.forEach(slot -> slot.setMark(_massMark));
    }


    //Getters and Setters
    public SessionAttestationMarkCatalogItem getMassMark()
    {
        return _massMark;
    }

    public void setMassMark(SessionAttestationMarkCatalogItem massMark)
    {
        _massMark = massMark;
    }

    public boolean isNotUsedBRS()
    {
        return notUsedBRS;
    }

    public void setNotUsedBRS(boolean notUsedBRS)
    {
        this.notUsedBRS = notUsedBRS;
    }
}