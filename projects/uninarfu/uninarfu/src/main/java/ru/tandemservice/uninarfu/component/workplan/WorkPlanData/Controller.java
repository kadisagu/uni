/* $Id$ */
package ru.tandemservice.uninarfu.component.workplan.WorkPlanData;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.BusinessComponent;
import ru.tandemservice.uninarfu.base.bo.NarfuWorkPlan.ui.RegistryFromWPAdd.NarfuWorkPlanRegistryFromWPAdd;
import ru.tandemservice.uninarfu.base.bo.NarfuWorkPlan.ui.RegistryFromWPAdd.NarfuWorkPlanRegistryFromWPAddUI;

/**
 * @author Andrey Andreev
 * @since 29.03.2016
 */
public class Controller extends ru.tandemservice.uniepp.component.workplan.WorkPlanData.Controller
{

    public void onClickAddDisciplineFromWP(IBusinessComponent component)
    {
        CAFLegacySupportService
                .asRegionDialog(NarfuWorkPlanRegistryFromWPAdd.class.getSimpleName(), (BusinessComponent) component)
                .parameter(NarfuWorkPlanRegistryFromWPAddUI.PARAM_HOLDER_ID, getModel(component).getHolder().getId())
                .parameter(NarfuWorkPlanRegistryFromWPAddUI.PARAM_ELEMENT_TYPE, NarfuWorkPlanRegistryFromWPAddUI.ELEMENT_TYPE.DISCIPLINE)
                .activate();
    }

    public void onClickAddActionFromWP(IBusinessComponent component)
    {
        CAFLegacySupportService
                .asRegionDialog(NarfuWorkPlanRegistryFromWPAdd.class.getSimpleName(), (BusinessComponent) component)
                .parameter(NarfuWorkPlanRegistryFromWPAddUI.PARAM_HOLDER_ID, getModel(component).getHolder().getId())
                .parameter(NarfuWorkPlanRegistryFromWPAddUI.PARAM_ELEMENT_TYPE, NarfuWorkPlanRegistryFromWPAddUI.ELEMENT_TYPE.PRACTICE)
                .activate();
    }
}