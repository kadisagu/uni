/* $Id$ */
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Andrey Andreev
 * @since 26.10.2015
 */
public class MS_uninarfu_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.2"),
                        new ScriptDependency("ru.tandemservice.movestudentrmc", "2.9.2"),
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("UPDATE PRACTICEPAYMENTDATA_T " +
                        "SET TITLE_P = 'с оплатой проезда к месту практики и обратно'" +
                        "WHERE CODE_P = 'travel'"
        );
        tool.executeUpdate("UPDATE PRACTICEPAYMENTDATA_T " +
                        "SET TITLE_P = 'с оплатой проезда к месту практики и обратно, суточных'" +
                        "WHERE CODE_P = 'travelAndDaily'"
        );
    }
}