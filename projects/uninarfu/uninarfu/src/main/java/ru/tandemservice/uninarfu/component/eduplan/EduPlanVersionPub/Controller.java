package ru.tandemservice.uninarfu.component.eduplan.EduPlanVersionPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.dao.uniepp.IDaoUniepp;
import ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.Model;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.ui.VersionBlockPrint.NarfuEduPlanVersionBlockPrint;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 06.10.2015
 */
public class Controller extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        StaticListDataSource<ViewWrapper<EppEduPlanVersionBlock>> ds = model.getBlockDataSource();
        if (ds.getColumn("select") == null)
            ds.addColumn(new CheckboxColumn("select"), 0);
    }

    @Override
    public void onClickPrint(final IBusinessComponent component)
    {
        EppEduPlanVersion version = getModel(component).getEduplanVersion();
        ContextLocal.createDesktop(
                PersonShellDialog.COMPONENT_NAME,
                new ComponentActivator(NarfuEduPlanVersionBlockPrint.class.getSimpleName(),
                                       ParametersMap.createWith(UIPresenter.PUBLISHER_ID, version.getId())));
    }

    public void onClickMakeModule(IBusinessComponent component)
    {
        CheckboxColumn ck = (CheckboxColumn) getModel(component).getBlockDataSource().getColumn("select");
        List<Long> blockIds = ck.getSelectedObjects().stream().map(IIdentifiable::getId).collect(Collectors.toList());
        if (blockIds.isEmpty())
            throw new ApplicationException("Не выделено ни одного блока");

        EppState state = DataAccessServices.dao().getByCode(EppState.class, EppState.STATE_FORMATIVE);
        Set<IEppEpvRowWrapper> rowWrappers = IEppEduPlanVersionDataDAO.instance.get()
                .getEduPlanVersionBlockDataMap(blockIds, true)
                .values().stream()
                .map(block -> block.getRowMap().values())
                .flatMap(Collection::stream)
                .filter(w -> w.getRow() instanceof EppEpvRegistryRow)
                .filter(w -> blockIds.contains(w.getOwner().getId()))
                .collect(Collectors.toCollection(() -> new TreeSet<>((w1, w2) -> Long.compare(w1.getRow().getId(), w2.getRow().getId()))));

        IEppRegistryDAO.instance.get().createAndAttachRegElements(rowWrappers, state, true);
    }

    public void onClickChangeSelfWork(IBusinessComponent component)
    {
        CheckboxColumn ck = (CheckboxColumn) getModel(component).getBlockDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        List<IEntity> blockList = new ArrayList<>();
        for (IEntity entity : lst)
        {
            ViewWrapper<EppEduPlanVersionBlock> wrapper = (ViewWrapper<EppEduPlanVersionBlock>) entity;
            blockList.add(wrapper.getEntity());
        }

        IDaoUniepp.instance.get().doSelfWork(blockList);
    }

    public void onClickChangeModule(IBusinessComponent component)
    {
        CheckboxColumn ck = (CheckboxColumn) getModel(component).getBlockDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();
        List<IEntity> blockList = new ArrayList<>();
        for (IEntity entity : lst)
        {
            ViewWrapper<EppEduPlanVersionBlock> wrapper = (ViewWrapper<EppEduPlanVersionBlock>) entity;
            blockList.add(wrapper.getEntity());
        }

        IDaoUniepp.instance.get().updateRegistryElements(blockList);
    }

    public void onClickConfirmEduplans(IBusinessComponent component)
    {
        CheckboxColumn ck = (CheckboxColumn) getModel(component).getBlockDataSource().getColumn("select");
        List<EppRegistryElement> registryElements = new ArrayList<>();
        Collection<IEntity> lst = ck.getSelectedObjects();
        for (IEntity entity : lst)
        {
            ViewWrapper<EppEduPlanVersionBlock> wrapper = (ViewWrapper<EppEduPlanVersionBlock>) entity;
            EppEduPlanVersionBlock versionBlock = wrapper.getEntity();

            List<EppRegistryElement> eppRegistryElements = IDaoUniepp.instance.get().getEppRegistryElements(versionBlock);
            registryElements.addAll(eppRegistryElements);
        }

        IDaoUniepp.instance.get().confirmEduplans(registryElements);
    }
}
