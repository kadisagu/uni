/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCompetitionListAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.NarfuEnrReportManager;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic.NarfuEmployeeEnrCommissionDSHandler;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic.NarfuEnrollmentCommissionDSHandler;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 24.04.2015
 */
public class NarfuEnrReportRequestCompetitionListAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private List<EnrEnrollmentCommission> _enrollmentCommissionList;
    private Date _dateFrom;
    private Date _dateTo;
    private boolean parallelActive;
    private IdentifiableWrapper parallel;
    private List<IdentifiableWrapper> parallelList;
    private boolean _skipEmptyList;
    private boolean _skipEmptyCompetition;
    private boolean _replaceProgramSetTitle;
    private boolean _skipEmptyOrgUnit;
    private boolean _addBenefitsToComment;
    private boolean _enrollmentCommissionActive;
    private boolean _includeTookAwayDocuments;
    private EmployeePost _secretary;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());

        configUtil(getCompetitionFilterAddon());
        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "не учитывать выбранные параллельно условия поступления"),
                                      new IdentifiableWrapper(1L, "включать в отчет только выбранные параллельно условия поступления")));

        setEnrollmentCommissionActive(false);
        setEnrollmentCommissionList(null);
        setParallel(null);
    }

    // validate
    private void validate()
    {
        if (getDateFrom().after(getDateTo()))
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");

        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }

    //listeners
    public void onClickApply()
    {
        validate();
        Long reportId = NarfuEnrReportManager.instance().dao().createRequestCompetitionListReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
    }

    // utils
    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }


    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.COMPENSATION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // for report builder

    public boolean isParallelOnly() {
        return isParallelActive() && 1L == getParallel().getId();
    }

    public boolean isSkipParallel() {
        return isParallelActive() && 0L == getParallel().getId();
    }

    //getters & setters
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public List<EnrEnrollmentCommission> getEnrollmentCommissionList()
    {
        return _enrollmentCommissionList;
    }

    public void setEnrollmentCommissionList(List<EnrEnrollmentCommission> enrollmentCommissionList)
    {
        _enrollmentCommissionList = enrollmentCommissionList;
    }

    public boolean isEnrollmentCommissionActive()
    {
        return _enrollmentCommissionActive;
    }

    public void setEnrollmentCommissionActive(boolean enrollmentCommissionActive)
    {
        _enrollmentCommissionActive = enrollmentCommissionActive;
    }

    public boolean isParallelActive()
    {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        this.parallelActive = parallelActive;
    }

    public IdentifiableWrapper getParallel()
    {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper parallel)
    {
        this.parallel = parallel;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        this.parallelList = parallelList;
    }

    public boolean isSkipEmptyList()
    {
        return _skipEmptyList;
    }

    public void setSkipEmptyList(boolean skipEmptyList)
    {
        _skipEmptyList = skipEmptyList;
    }

    public boolean isSkipEmptyCompetition()
    {
        return _skipEmptyCompetition;
    }

    public void setSkipEmptyCompetition(boolean skipEmptyCompetition)
    {
        _skipEmptyCompetition = skipEmptyCompetition;
    }

    public boolean isReplaceProgramSetTitle()
    {
        return _replaceProgramSetTitle;
    }

    public void setReplaceProgramSetTitle(boolean replaceProgramSetTitle)
    {
        _replaceProgramSetTitle = replaceProgramSetTitle;
    }

    public boolean isSkipEmptyOrgUnit()
    {
        return _skipEmptyOrgUnit;
    }

    public void setSkipEmptyOrgUnit(boolean skipEmptyOrgUnit)
    {
        _skipEmptyOrgUnit = skipEmptyOrgUnit;
    }

    public boolean isAddBenefitsToComment()
    {
        return _addBenefitsToComment;
    }

    public void setAddBenefitsToComment(boolean addBenefitsToComment)
    {
        _addBenefitsToComment = addBenefitsToComment;
    }

    public boolean isIncludeTookAwayDocuments()
    {
        return _includeTookAwayDocuments;
    }

    public void setIncludeTookAwayDocuments(boolean includeTookAwayDocuments)
    {
        _includeTookAwayDocuments = includeTookAwayDocuments;
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(EnrEnrollmentCommissionManager.DS_ENROLLMENT_COMMISSION))
            dataSource.put(NarfuEnrollmentCommissionDSHandler.ENROLLMENT_CAMPAIGN, _enrollmentCampaign);
        if (dataSource.getName().equals(NarfuEnrReportRequestCompetitionListAdd.EMPLOYEE_DS))
            dataSource.put(NarfuEmployeeEnrCommissionDSHandler.ENROLLMENT_COMMISSIONS, _enrollmentCommissionList);
    }

    public EmployeePost getSecretary()
    {
        return _secretary;
    }

    public void setSecretary(EmployeePost secretary)
    {
        _secretary = secretary;
    }
}
