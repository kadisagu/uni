package ru.tandemservice.uninarfu.base.ext.UniStudent.ui.OrgUnitList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.base.bo.UniStudent.ui.OrgUnitList.UniStudentOrgUnitList;
import ru.tandemservice.uninarfu.base.ext.UniStudent.UniStudentMangerExt;

@Configuration
public class UniStudentOrgUnitListExt extends BusinessComponentExtensionManager {

    public static final String ADDON = "narfu_" + UniStudentOrgUnitListExtUI.class.getSimpleName();

    @Autowired
    private UniStudentOrgUnitList parent;

    @Bean
    public PresenterExtension presenterExtension()
    {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon(ADDON, UniStudentOrgUnitListExtUI.class));
        //Скрываем фильтра гражданство, т.к. после перехода на версию 2.2.0 в базовом функционале добавлен аналогичный фильтр
        pi.addDataSource(UniStudentMangerExt.instance().citizenshipDSConfig());
        pi.replaceDataSource(searchListDS("studentSearchListDS", parent.studentSearchListDSColumnExtPoint())
                                     .handler(UniStudentMangerExt.instance().orgUnitStudentSearchListExtDSHandler()));
        return pi.create();
    }
}
