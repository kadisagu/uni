/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetEnrollmentCommission;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 17.06.2015
 */
public class NarfuEnrollmentCommissionDSHandler extends DefaultComboDataSourceHandler
{
    public static final String ENROLLMENT_CAMPAIGN = EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN;

    public NarfuEnrollmentCommissionDSHandler(String ownerId)
    {
        super(ownerId, EnrEnrollmentCommission.class);
    }


    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        EnrEnrollmentCampaign campaign = ep.context.get(ENROLLMENT_CAMPAIGN);
        DQLSelectBuilder subBuilder =  new DQLSelectBuilder().fromEntity(EnrProgramSetEnrollmentCommission.class, "prSet")
                .column(property("prSet", EnrProgramSetEnrollmentCommission.enrollmentCommission().id()))
                .distinct()
                .where(eq(property("prSet", EnrProgramSetEnrollmentCommission.programSetOrgUnit().programSet().enrollmentCampaign()), value(campaign)));

        ep.dqlBuilder.where(in(property("e", EnrEnrollmentCommission.id()), subBuilder.buildQuery()));
    }
}
