package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uninarfu_2x9x3_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность narfuBSODisposalActVisaItem

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("narfu_bso_dispact_visa_item_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_narfubsodisposalactvisaitem"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("possiblevisa_id", DBType.LONG).setNullable(false), 
				new DBColumn("visatemplate_id", DBType.LONG).setNullable(false), 
				new DBColumn("priority_p", DBType.INTEGER).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("narfuBSODisposalActVisaItem");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность narfuBSOVisasTemplate

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("narfu_bso_visa_template_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_narfubsovisastemplate"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("narfuBSOVisasTemplate");

		}


    }
}