/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab.EnrEntrantPubRequestTab;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon.NarfuEntrantRequestActionPrint;
import ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon.NarfuEntrantRequestActionsAddon;
import ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon.NarfuEntrantRequestActionsPrintListDocs;

/**
 * @author Ekaterina Zvereva
 * @since 24.04.2015
 */
@Configuration
public class EnrEntrantPubRequestTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantPubRequestTab _requestPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_requestPub.presenterExtPoint())
                .replaceAddon(uiAddon(EnrEntrantRequestActionsAddon.NAME, NarfuEntrantRequestActionsAddon.class))
                .addAction(new NarfuEntrantRequestActionsPrintTutor("onClickPrintTutorProfile"))
                .addAction(new NarfuEntrantRequestActionPrint("onClickPrintEntrantRequest"))
                .addAction(new NarfuEntrantRequestActionPrintReceipt("onClickPrintReceiptDocuments"))
                .addAction(new NarfuEntrantRequestActionsPrintListDocs("onClickPrintListDocuments"))
                .addAction(new NarfuEntrantRequestActionsPrintCover("onClickPrintCover"))
                .addAction(new NarfuEntrantRequestPrintExamList("onClickPrintEnrollmentExamSheet"))
                .create();
    }

}