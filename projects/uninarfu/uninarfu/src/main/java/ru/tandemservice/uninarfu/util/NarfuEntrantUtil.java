/* $Id$ */
package ru.tandemservice.uninarfu.util;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 24.04.2015
 */
public class NarfuEntrantUtil
{
    public static Map<CoreCollectionUtils.Pair<String,Boolean>, String> codesMap = new HashMap<>();
    static
    {
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.HUSBAND, false), RelationDegreeCodes.WIFE);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.WIFE, true), RelationDegreeCodes.HUSBAND);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.FATHER, false), RelationDegreeCodes.DAUGHTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.FATHER, true), RelationDegreeCodes.SON);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.MOTHER, false), RelationDegreeCodes.DAUGHTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.MOTHER, true), RelationDegreeCodes.SON);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.DAUGHTER, true), RelationDegreeCodes.FATHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.DAUGHTER, false), RelationDegreeCodes.MOTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.SON, true), RelationDegreeCodes.FATHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.SON, false), RelationDegreeCodes.MOTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDFATHER, false), RelationDegreeCodes.GRANDDAUGHTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDFATHER, true), RelationDegreeCodes.GRANDSON);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDMOTHER, false), RelationDegreeCodes.GRANDDAUGHTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDMOTHER, true), RelationDegreeCodes.GRANDSON);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.BROTHER, false), RelationDegreeCodes.SISTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.BROTHER, true), RelationDegreeCodes.BROTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.SISTER, true), RelationDegreeCodes.BROTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.SISTER, false), RelationDegreeCodes.SISTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDDAUGHTER, true), RelationDegreeCodes.GRANDFATHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDDAUGHTER, false), RelationDegreeCodes.GRANDMOTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDSON, true), RelationDegreeCodes.GRANDFATHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.GRANDSON, false), RelationDegreeCodes.GRANDMOTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPFATHER, false), RelationDegreeCodes.STEPDAUGHTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPFATHER,true), RelationDegreeCodes.STEPSON);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPMOTHER, false), RelationDegreeCodes.STEPDAUGHTER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPMOTHER, true), RelationDegreeCodes.STEPSON);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPSON, true), RelationDegreeCodes.STEPFATHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPSON, false), RelationDegreeCodes.STEPMOTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPDAUGHTER, false), RelationDegreeCodes.STEPMOTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.STEPDAUGHTER, true), RelationDegreeCodes.STEPFATHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.SON_IN_LAW, false), RelationDegreeCodes.MOTHER_IN_LAW_FOR_MALE);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.SON_IN_LAW, true), RelationDegreeCodes.FATHER_IN_LAW_FOR_MALE);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.DAUGHTER_IN_LAW, false), RelationDegreeCodes.MOTHER_IN_LAW_FOR_FEMALE);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.DAUGHTER_IN_LAW, true), RelationDegreeCodes.FATHER_IN_LAW_FOR_FEMALE);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.MOTHER_IN_LAW_FOR_MALE, true), RelationDegreeCodes.SON_IN_LAW);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.FATHER_IN_LAW_FOR_MALE, true), RelationDegreeCodes.SON_IN_LAW);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.MOTHER_IN_LAW_FOR_FEMALE, false), RelationDegreeCodes.DAUGHTER_IN_LAW);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.FATHER_IN_LAW_FOR_FEMALE, false), RelationDegreeCodes.DAUGHTER_IN_LAW);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.TUTOR, false), RelationDegreeCodes.ANOTHER);
        codesMap.put(new CoreCollectionUtils.Pair<String, Boolean>(RelationDegreeCodes.TUTOR, true), RelationDegreeCodes.ANOTHER);

    }

    public static RelationDegree getReverseRelationDegree(String secondCode, boolean firstIsMale)
    {
        String reverseCode = codesMap.get(new CoreCollectionUtils.Pair<String, Boolean>(secondCode, firstIsMale));
        if (reverseCode == null) return null;
        return IUniBaseDao.instance.get().getCatalogItem(RelationDegree.class, reverseCode);
    }

}