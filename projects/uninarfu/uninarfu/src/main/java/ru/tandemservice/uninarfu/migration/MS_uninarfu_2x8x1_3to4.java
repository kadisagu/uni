package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;


/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x8x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {


        ////////////////////////////////////////////////////////////////////////////////
        // сущность narfuSpecConditionsEntTests

        // создана новая сущность
        if (!tool.tableExists("narfuspecconditionsenttests_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("narfuspecconditionsenttests_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_narfuspecconditionsenttests"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("narfuSpecConditionsEntTests");

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantSpecConditionRelation

        // создана новая сущность
        if (!tool.tableExists("entrantspecconditionrelation_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("entrantspecconditionrelation_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_e687c1fc"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("enrentrant_id", DBType.LONG).setNullable(false),
                                      new DBColumn("speccondition_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("entrantSpecConditionRelation");

        }

    }

}