package ru.tandemservice.uninarfu.base.ext.UniStudent.logic.list;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniStudent.logic.list.StudentSearchListDSHandler;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

public class StudentSearchListExtDSHandler extends StudentSearchListDSHandler
{

    public StudentSearchListExtDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {

        super.addAdditionalRestrictions(builder, alias, input, context);
        //Скрываем фильтра гражданство, т.к. после перехода на версию 2.2.0 в базовом функционале добавлен аналогичный фильтр
        Object citizenshipList = context.get("citizenshipList");
        String comment = context.get("comment");

        FilterUtils.applySelectFilter(builder, "s", Student.person().identityCard().citizenship(), citizenshipList);
        if (comment != null)
            builder.where(DQLExpressions.like(DQLExpressions.property(Student.comment().fromAlias("s")), DQLExpressions.value("%" + comment + "%")));
    }

}
