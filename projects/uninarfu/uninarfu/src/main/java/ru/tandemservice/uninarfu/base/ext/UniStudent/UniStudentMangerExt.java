package ru.tandemservice.uninarfu.base.ext.UniStudent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uninarfu.base.ext.UniStudent.logic.CitizenshipDSHandler;
import ru.tandemservice.uninarfu.base.ext.UniStudent.logic.DiplomaUniStudentDAO;
import ru.tandemservice.uninarfu.base.ext.UniStudent.logic.IDiplomaUniStudentDAO;
import ru.tandemservice.uninarfu.base.ext.UniStudent.logic.list.StudentSearchListExtDSHandler;
import ru.tandemservice.uninarfu.base.ext.UniStudent.logic.orgUnitList.OrgUnitStudentSearchListExtDSHandler;

@Configuration
public class UniStudentMangerExt extends BusinessObjectExtensionManager
{

    @Autowired
    private UniStudentManger _uniStudentManger;

    public static UniStudentMangerExt instance() {
        return instance(UniStudentMangerExt.class);
    }

    @Bean
    public UIDataSourceConfig citizenshipDSConfig()
    {
        return SelectDSConfig
                .with("citizenshipRMCDS", getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(citizenshipDSHandler())
                .addColumn(AddressCountry.title().s()).create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> citizenshipDSHandler()
    {
        return new CitizenshipDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> orgUnitStudentSearchListExtDSHandler()
    {
        return new OrgUnitStudentSearchListExtDSHandler(getName());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> studentSearchListExtDSHandler()
    {
        return new StudentSearchListExtDSHandler(getName());
    }

    @Bean
    public IDiplomaUniStudentDAO diplomaUniStudentDAO() {
        return new DiplomaUniStudentDAO();
    }

}
