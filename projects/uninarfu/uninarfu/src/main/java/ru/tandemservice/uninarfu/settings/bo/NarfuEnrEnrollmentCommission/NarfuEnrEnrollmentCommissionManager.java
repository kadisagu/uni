/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuEnrEnrollmentCommission;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.uninarfu.settings.bo.NarfuEnrEnrollmentCommission.logic.NarfuEmployeePostDSHandler;
import ru.tandemservice.uninarfu.settings.bo.NarfuEnrEnrollmentCommission.logic.NarfuEnrollmentCommissionDSHandler;


/**
 * @author Ekaterina Zvereva
 * @since 10.06.2015
 */
@Configuration
public class NarfuEnrEnrollmentCommissionManager extends BusinessObjectManager
{
    public static NarfuEnrEnrollmentCommissionManager instance()
    {
        return instance(NarfuEnrEnrollmentCommissionManager.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> employeePostDSHandler()
    {
        return new NarfuEmployeePostDSHandler(getName(), EmployeePost.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> enrollmentCommissionDSHandler()
    {
        return new NarfuEnrollmentCommissionDSHandler(getName(), EnrEnrollmentCommission.class);
    }
}