/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.List;

import com.sun.istack.Nullable;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.narfu.entity.PersonEduInstitutionNARFU;

/**
 * @author DMITRY KNYAZEV
 * @since 04.04.2015
 */
final public class PersonEduInstitutionWrapper extends ViewWrapper<PersonEduInstitution> {

    public static final String DATE_ISSUE_CERTIFICATE = "dateIssueCertificate";
    public static final String EXPECTED_DATE_RETURN = "expectedDateReturn";
    public static final String ACTUAL_DATE_RETURN = "actualDateReturn";
    public static final String PUB_PARAMETERS = "pubParameters";

    public PersonEduInstitutionWrapper(PersonEduInstitution entity, @Nullable PersonEduInstitutionNARFU addon, ISecureRoleContext personRoleContext) {
        super(entity);
        String dateIssueCertificate = "";
        String expectedDateReturn = "";
        String actualDateReturn = "";
        if (addon != null) {
            dateIssueCertificate = DateFormatter.DEFAULT_DATE_FORMATTER.format(addon.getDateIssueCertificate());
            expectedDateReturn = DateFormatter.DEFAULT_DATE_FORMATTER.format(addon.getExpectedDateReturn());
            actualDateReturn = DateFormatter.DEFAULT_DATE_FORMATTER.format(addon.getActualDateReturn());
        }
        setViewProperty(DATE_ISSUE_CERTIFICATE, dateIssueCertificate);
        setViewProperty(EXPECTED_DATE_RETURN, expectedDateReturn);
        setViewProperty(ACTUAL_DATE_RETURN, actualDateReturn);

        ParametersMap map = new ParametersMap();
        map.add(PublisherActivator.PUBLISHER_ID_KEY, getId());
        map.add("securedObjectId", personRoleContext.getSecuredObject().getId());
        map.add("accessible", personRoleContext.isAccessible());
        map.add("personRoleName", personRoleContext.getPersonRoleName());
        setViewProperty(PUB_PARAMETERS, map);
    }
}
