package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation;
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Потребность абитуриента в специальных условиях прохождения ВИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantSpecConditionRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation";
    public static final String ENTITY_NAME = "entrantSpecConditionRelation";
    public static final int VERSION_HASH = -42522822;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ENTRANT = "enrEntrant";
    public static final String L_SPEC_CONDITION = "specCondition";

    private EnrEntrant _enrEntrant;     // Абитуриент
    private NarfuSpecConditionsEntTests _specCondition;     // Специальные условия прохождения ВИ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEnrEntrant()
    {
        return _enrEntrant;
    }

    /**
     * @param enrEntrant Абитуриент. Свойство не может быть null.
     */
    public void setEnrEntrant(EnrEntrant enrEntrant)
    {
        dirty(_enrEntrant, enrEntrant);
        _enrEntrant = enrEntrant;
    }

    /**
     * @return Специальные условия прохождения ВИ. Свойство не может быть null.
     */
    @NotNull
    public NarfuSpecConditionsEntTests getSpecCondition()
    {
        return _specCondition;
    }

    /**
     * @param specCondition Специальные условия прохождения ВИ. Свойство не может быть null.
     */
    public void setSpecCondition(NarfuSpecConditionsEntTests specCondition)
    {
        dirty(_specCondition, specCondition);
        _specCondition = specCondition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantSpecConditionRelationGen)
        {
            setEnrEntrant(((EntrantSpecConditionRelation)another).getEnrEntrant());
            setSpecCondition(((EntrantSpecConditionRelation)another).getSpecCondition());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantSpecConditionRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantSpecConditionRelation.class;
        }

        public T newInstance()
        {
            return (T) new EntrantSpecConditionRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrEntrant":
                    return obj.getEnrEntrant();
                case "specCondition":
                    return obj.getSpecCondition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrEntrant":
                    obj.setEnrEntrant((EnrEntrant) value);
                    return;
                case "specCondition":
                    obj.setSpecCondition((NarfuSpecConditionsEntTests) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrEntrant":
                        return true;
                case "specCondition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrEntrant":
                    return true;
                case "specCondition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrEntrant":
                    return EnrEntrant.class;
                case "specCondition":
                    return NarfuSpecConditionsEntTests.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantSpecConditionRelation> _dslPath = new Path<EntrantSpecConditionRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantSpecConditionRelation");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation#getEnrEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> enrEntrant()
    {
        return _dslPath.enrEntrant();
    }

    /**
     * @return Специальные условия прохождения ВИ. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation#getSpecCondition()
     */
    public static NarfuSpecConditionsEntTests.Path<NarfuSpecConditionsEntTests> specCondition()
    {
        return _dslPath.specCondition();
    }

    public static class Path<E extends EntrantSpecConditionRelation> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _enrEntrant;
        private NarfuSpecConditionsEntTests.Path<NarfuSpecConditionsEntTests> _specCondition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation#getEnrEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> enrEntrant()
        {
            if(_enrEntrant == null )
                _enrEntrant = new EnrEntrant.Path<EnrEntrant>(L_ENR_ENTRANT, this);
            return _enrEntrant;
        }

    /**
     * @return Специальные условия прохождения ВИ. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation#getSpecCondition()
     */
        public NarfuSpecConditionsEntTests.Path<NarfuSpecConditionsEntTests> specCondition()
        {
            if(_specCondition == null )
                _specCondition = new NarfuSpecConditionsEntTests.Path<NarfuSpecConditionsEntTests>(L_SPEC_CONDITION, this);
            return _specCondition;
        }

        public Class getEntityClass()
        {
            return EntrantSpecConditionRelation.class;
        }

        public String getEntityName()
        {
            return "entrantSpecConditionRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
