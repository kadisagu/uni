/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.EdiSpecialExamConditions;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EdiSpecialExamConditions.EnrEntrantEdiSpecialExamConditionsUI;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.NarfuEntrantManager;
import ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation;
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 16.04.2015
 */
public class EnrEntrantEdiSpecialExamConditionsExtUI extends UIAddon
{
    public EnrEntrantEdiSpecialExamConditionsExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private List<NarfuSpecConditionsEntTests> _specConditionsList;

    private EnrEntrantEdiSpecialExamConditionsUI _presenter;

    public List<NarfuSpecConditionsEntTests> getSpecConditionsList()
    {
        return _specConditionsList;
    }

    public void setSpecConditionsList(List<NarfuSpecConditionsEntTests> specConditionsList)
    {
        _specConditionsList = specConditionsList;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = (EnrEntrantEdiSpecialExamConditionsUI)getPresenter();
        _specConditionsList = NarfuEntrantManager.instance().dao().getPropertiesList(EntrantSpecConditionRelation.class, EntrantSpecConditionRelation.enrEntrant(),
                                                  _presenter.getEntrant(), false, EntrantSpecConditionRelation.specCondition());
    }
}