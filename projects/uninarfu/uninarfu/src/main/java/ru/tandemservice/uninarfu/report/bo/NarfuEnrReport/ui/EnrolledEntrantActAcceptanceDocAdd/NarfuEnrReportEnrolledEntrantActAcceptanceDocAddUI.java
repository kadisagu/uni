/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EnrolledEntrantActAcceptanceDocAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 09.04.2015
 */
public class NarfuEnrReportEnrolledEntrantActAcceptanceDocAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    public void onClickApply()
    {
        Date date = getSettings().get("date");
        String number = getSettings().get("number");
        List<EnrOrder> enrOrderList = getSettings()
                .get(NarfuEnrReportEnrolledEntrantActAcceptanceDocAdd.ENROLLMENT_ORDER_LIST);
        List<EnrEnrollmentCommission> enrollmentCommissionList = getSettings()
                .get(NarfuEnrReportEnrolledEntrantActAcceptanceDocAdd.ENROLLMENT_COMMISSION_LIST);
        EnrRequestType enrRequestType = getSettings().get(NarfuEnrReportEnrolledEntrantActAcceptanceDocAdd.REQUEST_TYPE);
        List<OrgUnit> enrOrgUnitList = getSettings().get("enrOrgUnitList");
        List<OrgUnit> formativeOrgUnitList = getSettings().get("formativeOrgUnitList");


        IUniBaseDao.instance.get().doInTransaction(session -> {
            IScriptItem scriptItem = DataAccessServices.dao().getByCode(
                    EnrScriptItem.class, EnrScriptItemCodes.REPORT_NARFU_ENROLLED_ENTRANT_ACT_ACCEPTANCE_DOCUMENTS);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                    IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                    "date", date,
                    "number", number,
                    "enrollmentCampaignId", getEnrollmentCampaign().getId(),
                    "enrOrderIds", CommonBaseEntityUtil.getIdList(enrOrderList),
                    "enrollmentCommissionIds", CommonBaseEntityUtil.getIdList(enrollmentCommissionList),
                    "enrRequestTypeCode", enrRequestType != null ? enrRequestType.getCode() : null,
                    "enrOrgUnitIds", CommonBaseEntityUtil.getIdList(enrOrgUnitList),
                    "formOrgUnitIds", CommonBaseEntityUtil.getIdList(formativeOrgUnitList)
            );

            byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == document) {
                throw new ApplicationException("Не найдено абитуриентов для включения в отчет.");
            }

            BusinessComponentUtils.downloadDocument(
                    new CommonBaseRenderer().fileName(filename).document(document), false);

            return 0L;
        });
        deactivate();
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll(null));
        dataSource.put(NarfuEnrReportEnrolledEntrantActAcceptanceDocAdd.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    // getters and setters


    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
}
