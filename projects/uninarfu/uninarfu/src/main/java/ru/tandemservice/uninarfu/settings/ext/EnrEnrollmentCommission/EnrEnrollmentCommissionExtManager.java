/* $Id$ */
package ru.tandemservice.uninarfu.settings.ext.EnrEnrollmentCommission;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 10.06.2015
 */
@Configuration
public class EnrEnrollmentCommissionExtManager extends BusinessObjectExtensionManager
{
}