/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DiplomaIssuance.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEdit;

/**
 * @author Andrey Andreev
 * @since 21.10.2015
 */
@Configuration
public class DiplomaIssuanceAddEditExt extends BusinessComponentExtensionManager
{
    @Autowired
    private DiplomaIssuanceAddEdit _diplomaIssuanceAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_diplomaIssuanceAddEdit.presenterExtPoint())
                .addAction(new DiplomaIssuanceAddEditOnClickApply("onClickApply"))
                .create();
    }
}