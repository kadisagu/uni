package ru.tandemservice.uninarfu.ws.sakai;

import ru.tandemservice.uninarfu.ws.sakai.types.WsOrderWrap;
import ru.tandemservice.uninarfu.ws.sakai.types.WsPaymentWrap;
import ru.tandemservice.unitutor.ws.IWsDisciplines;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import java.util.List;

public interface IWsSakai extends IWsDisciplines
{

    /**
     * Данные проведенных представлений, в которые включен текущий студент
     *
     * @param studentParam
     *
     * @return
     *
     * @throws Exception
     */
    @WebMethod
    @WebResult(name = "orderWrap")
    public List<WsOrderWrap> getWsOrders(@WebParam(name = "studentParam") String studentParam) throws Exception;

    @WebMethod
    WsPaymentWrap getPaymentData(@WebParam(name = "studentParam") String studentParam) throws Exception;
}
