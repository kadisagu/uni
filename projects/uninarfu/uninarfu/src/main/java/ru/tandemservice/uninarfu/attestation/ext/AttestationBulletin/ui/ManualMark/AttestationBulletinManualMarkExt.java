/* $Id$ */
package ru.tandemservice.uninarfu.attestation.ext.AttestationBulletin.ui.ManualMark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.ManualMark.AttestationBulletinManualMark;

/**
 * @author Andrey Andreev
 * @since 08.12.2015
 */
@Configuration
public class AttestationBulletinManualMarkExt  extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uninarfu"+AttestationBulletinManualMarkExtUI.class.getSimpleName();

    @Autowired
    private AttestationBulletinManualMark _attestationBulletinManualMark;

    @Bean
    public PresenterExtension presenterExtPoint()
    {
        return presenterExtensionBuilder(_attestationBulletinManualMark.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, AttestationBulletinManualMarkExtUI.class))
                .create();
    }

}