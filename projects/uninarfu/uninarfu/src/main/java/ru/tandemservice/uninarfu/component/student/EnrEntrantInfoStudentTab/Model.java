package ru.tandemservice.uninarfu.component.student.EnrEntrantInfoStudentTab;

import com.google.common.collect.Lists;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.enrollment.bo.EnrEnrollmentStep.ui.CompetitionPub.EnrEnrollmentStepCompetitionPubUI;
import ru.tandemservice.unienr14.enrollment.entity.EnrEnrollmentStepItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.formatter.EnrChosenEntranceExamNumberFormatter;
import ru.tandemservice.unienr14.entrant.daemon.EnrEntrantDaemonBean;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult;
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEntrantAchievementType;

import java.util.*;

public class Model
{
    private ISelectModel _enrCampaignModel;

    private DynamicListDataSource<EnrEntrantStateExamResult> _stateExamResultDS;

    private DynamicListDataSource<EnrOlympiadDiploma> _olympiadDiplomaDS;
    private IEntityHandler _disabledScanCopyHandler;

    public Map parameters;

    private EnrEntrant _entrant;
    private Student _student = new Student();

    private Map<EnrRequestType, List<Map<EnrRequestedCompetition, RatingRowWrapper>>> requestTypeMap;

    private EnrRequestType currentRequestType;
    private RatingRowWrapper currentRow;

    private boolean entranceExamIncorrect;
    private boolean examListIncorrect;

    private boolean existEntrant;


    public Model()
    {
        this._entrant = new EnrEntrant();
    }

    public String getDaemonStatus(final String message)
    {
        final Long date = EnrEntrantDaemonBean.DAEMON.getCompleteStatus();
        if (null != date) {
            return DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date(date));
        }
        return message;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this._entrant = entrant;
    }

    public DynamicListDataSource<EnrEntrantStateExamResult> getStateExamResultDS()
    {
        return _stateExamResultDS;
    }

    public void setStateExamResultDS(DynamicListDataSource<EnrEntrantStateExamResult> stateExamResultDS)
    {
        this._stateExamResultDS = stateExamResultDS;
    }

    public DynamicListDataSource<EnrOlympiadDiploma> getOlympiadDiplomaDS()
    {
        return _olympiadDiplomaDS;
    }

    public void setOlympiadDiplomaDS(DynamicListDataSource<EnrOlympiadDiploma> olympiadDiplomaDS
    )
    {
        this._olympiadDiplomaDS = olympiadDiplomaDS;
    }

    public IEntityHandler getDisabledScanCopyHandler()
    {
        return _disabledScanCopyHandler;
    }

    public void setDisabledScanCopyHandler(IEntityHandler _disabledScanCopyHandler)
    {
        this._disabledScanCopyHandler = _disabledScanCopyHandler;
    }

    public RatingRowWrapper getCurrentRow()
    {
        return currentRow;
    }

    public void setCurrentRow(RatingRowWrapper currentRow)
    {
        this.currentRow = currentRow;
    }

    public EnrRatingItem getCurrentRowRating()
    {
        return getCurrentRow().getRating();
    }

    public boolean isBakSpecMag()
    {
        String code = getCurrentRequestType().getCode();
        return code.equals(EnrRequestTypeCodes.BS) || code.equals(EnrRequestTypeCodes.MASTER);
    }

    public List<RatingRowWrapper> getRowList()
    {
        List<RatingRowWrapper> rows = Lists.newArrayList();
        for (Map<EnrRequestedCompetition, RatingRowWrapper> map : getRequestTypeMap().get(getCurrentRequestType())) {
            rows.addAll(map.values());
        }

        Collections.sort(rows, (o1, o2) -> o1.getRequestedCompetition().getPriority() - o2.getRequestedCompetition().getPriority());
        return rows;
    }

    public Set<EnrRequestType> getRequestTypeList()
    {
        return getRequestTypeMap().keySet();
    }

    public Map<EnrRequestType, List<Map<EnrRequestedCompetition, RatingRowWrapper>>> getRequestTypeMap()
    {
        return requestTypeMap;
    }

    public void setRequestTypeMap(Map<EnrRequestType, List<Map<EnrRequestedCompetition, RatingRowWrapper>>> requestTypeMap)
    {
        this.requestTypeMap = requestTypeMap;
    }

    public EnrRequestType getCurrentRequestType()
    {
        return currentRequestType;
    }

    public void setCurrentRequestType(EnrRequestType currentRequestType)
    {
        this.currentRequestType = currentRequestType;
    }

    public boolean isEntranceExamIncorrect()
    {
        return entranceExamIncorrect;
    }

    public void setEntranceExamIncorrect(boolean entranceExamIncorrect)
    {
        this.entranceExamIncorrect = entranceExamIncorrect;
    }

    public boolean isExamListIncorrect()
    {
        return examListIncorrect;
    }

    public void setExamListIncorrect(boolean examListIncorrect)
    {
        this.examListIncorrect = examListIncorrect;
    }

    public boolean isDisplayAnyError()
    {
        return isEntranceExamIncorrect() || isExamListIncorrect();
    }

    public static class RatingRowWrapper implements ITitled
    {

        private EnrRequestedCompetition requestedCompetition;
        private EnrRatingItem rating;
        private Set<EnrChosenEntranceExam> exams = new HashSet<>();
        private EnrEnrollmentExtract extract;
        private List<EnrEnrollmentStepItem> enrollmentItemList = Lists.newArrayList();
        private List<EnrEntrantAchievement> entrantAchievementList = Lists.newArrayList();
        private EnrEnrollmentStepItem enrollmentItem;

        private String markEntranceExam;
        private String markEntrantAchievement;

        RatingRowWrapper(EnrRequestedCompetition requestedCompetition)
        {
            this.requestedCompetition = requestedCompetition;
        }

        public Set<EnrChosenEntranceExam> getExams() { return exams; }

        public EnrRatingItem getRating() { return rating; }

        public void setRating(EnrRatingItem rating) { this.rating = rating; }

        public List<EnrEnrollmentStepItem> getEnrollmentItemList() { return enrollmentItemList; }

        public void setEnrollmentItemList(List<EnrEnrollmentStepItem> enrollmentItemList) { this.enrollmentItemList = enrollmentItemList; }

        public List<EnrEntrantAchievement> getEntrantAchievementList() { return this.entrantAchievementList; }

        public void setEntrantAchievementList(List<EnrEntrantAchievement> entrantAchievementList
        )
        { this.entrantAchievementList = entrantAchievementList; }

        public EnrEnrollmentStepItem getEnrollmentItem() { return enrollmentItem; }

        public void setEnrollmentItem(EnrEnrollmentStepItem enrollmentItem) { this.enrollmentItem = enrollmentItem; }

        public EnrEnrollmentExtract getExtract() { return extract; }

        public void setExtract(EnrEnrollmentExtract extract) { this.extract = extract; }

        public EnrRequestedCompetition getRequestedCompetition() { return requestedCompetition; }

        public EnrCompetition getCompetition() { return requestedCompetition.getCompetition(); }

        @Override
        public String getTitle() { return requestedCompetition.getTitle(); }

        public String getMarkEntranceExam()
        {
            if (null != markEntranceExam) return markEntranceExam;
            if (getExams().isEmpty()) {
                return markEntranceExam = (getCompetition().getExamSetVariant().getExamSet().isEmptySet() || getCompetition().isNoExams()) ? "не предусмотрены ВИ" : "не выбраны ВИ";
            }

            StringBuilder b = new StringBuilder();
            List<EnrChosenEntranceExam> examList = new ArrayList<>(exams);
            Collections.sort(examList, EnrChosenEntranceExamNumberFormatter.COMPARATOR);
            EnrChosenEntranceExamNumberFormatter numberFormatter = new EnrChosenEntranceExamNumberFormatter(examList);
            for (EnrChosenEntranceExam exam : examList) {
                if (b.length() != 0) b.append("\n");
                b.append(numberFormatter.format(exam));
                b.append(exam.getDiscipline().getTitle()).append(": ");
                b.append(exam.getMarkSource() == null ? "—" : exam.getMarkSource().getInfoString());
            }

            markEntranceExam = NewLineFormatter.SIMPLE.format(b.toString());

            if (getRating() != null && !getRating().isStatusEntranceExamsCorrect()) {
                markEntranceExam = "<div style=\"color: " + UniDefines.COLOR_CELL_ERROR + ";\">" + markEntranceExam + "</div>";
            }

            return markEntranceExam;
        }

        public String getMarkEntrantAchievement()
        {
            if (null != markEntrantAchievement) return markEntrantAchievement;
            if (getEntrantAchievementList().isEmpty()) return markEntrantAchievement = "";

            List<EnrEntrantAchievement> achievements = Lists.newArrayList(entrantAchievementList);
            Collections.sort(achievements, new EntityComparator<EnrEntrantAchievement>(new EntityOrder(EnrEntrantAchievement.mark(), OrderDirection.desc)));

            StringBuilder b = new StringBuilder();

            int i = 0;
            for (EnrEntrantAchievement achievement : achievements) {
                EnrEntrantAchievementType type = achievement.getType();
                long mark = type.isMarked() ? achievement.getRatingMarkAsLong() : type.getAchievementMarkAsLong();

                if (b.length() != 0) b.append("\n");
                b.append(++i);
                b.append(". ");
                b.append(type.getAchievementKind().getShortTitle());
                b.append(" — ");
                b.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark * 0.001d));
            }
            return markEntrantAchievement = NewLineFormatter.SIMPLE.format(b.toString());
        }

        public boolean isShowOrder()
        {
            return getExtract() != null;
        }

        public String getEnrollmentString()
        {
            if (getEnrollmentItem() == null) return "";
            if (getEnrollmentItem().isRecommended()) return "Рекомендован к зачислению на " + getEnrollmentItem().getStep().getDateStr();
            if (getEnrollmentItem().isIncluded()) return "Включен в конкурсный список на " + getEnrollmentItem().getStep().getDateStr();
            return "Не включен в список шага зачисления на " + getEnrollmentItem().getStep().getDateStr();
        }

        public Map getEnrollmentPubParams()
        {
            if (getEnrollmentItem() == null) return null;
            ParametersMap params = new ParametersMap()
                    .add(EnrEnrollmentStepCompetitionPubUI.PARAM_STEP, getEnrollmentItem().getStep().getId())
                    .add(EnrEnrollmentStepCompetitionPubUI.PARAM_COMPETITION, getEnrollmentItem().getEntity().getCompetition().getId());
            if (getCompetition().getProgramSetOrgUnit().getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition()
                    && getEnrollmentItem().getEntity().getRequestedCompetition() instanceof EnrRequestedCompetitionTA) {
                EnrRequestedCompetitionTA reqComp = (EnrRequestedCompetitionTA) getEnrollmentItem().getEntity().getRequestedCompetition();
                params.add(EnrEnrollmentStepCompetitionPubUI.PARAM_TARGET_ADMISSION_KIND, reqComp.getTargetAdmissionKind().getTargetAdmissionKind().getId());
            }
            return params;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RatingRowWrapper wrapper = (RatingRowWrapper) o;
            return requestedCompetition.equals(wrapper.requestedCompetition);
        }
    }

    public boolean isExistEntrant()
    {
        return existEntrant;
    }

    public void setExistEntrant(boolean existEntrant)
    {
        this.existEntrant = existEntrant;
    }

    public ISelectModel getEnrCampaignModel()
    {
        return _enrCampaignModel;
    }

    public void setEnrCampaignModel(ISelectModel enrCampaignModel)
    {
        _enrCampaignModel = enrCampaignModel;
    }

    public String getTooltipInfo()
    {
        return "Начните работу со страницей, выбрав приемную кампанию сверху.";
    }

    public boolean isShowData()
    {
        return getEntrant() != null;
    }
}
