/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.List.PersonEduDocumentList;
import ru.tandemservice.uninarfu.base.bo.narfuEduDocument.NarfuEduDocumentManager;
import ru.tandemservice.uninarfu.base.ext.PersonEduDocument.logic.IPersonEduDocumentDao;

/**
 * @author DMITRY KNYAZEV
 * @since 06.04.2015
 */
@Configuration
public class PersonEduDocumentListExt extends BusinessComponentExtensionManager
{

    @Autowired
    private PersonEduDocumentList personEduDocumentList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(personEduDocumentList.presenterExtPoint())
                .replaceDataSource(searchListDS(PersonEduDocumentList.DS_EDU_DOCUMENT, personEduDocumentList.eduDocumentDSColumns(), NarfuEduDocumentManager.instance().eduDocumentDSHandler()))
                .replaceDataSource(searchListDS(PersonEduDocumentList.DS_EDU_INSTITUTION, personEduDocumentList.eduInstitutionSColumns(), NarfuEduDocumentManager.instance().eduInstitutionDSHandler()))
                .addAction(new DeleteEduDocument("onClickDelete"))
                .create();
    }

    @Bean
    public ColumnListExtension columnListExtension()
    {
        return columnListExtensionBuilder(personEduDocumentList.eduDocumentDSColumns())
                .addAllBefore("print")
                .addColumn(textColumn("dateIssueCertificate", PersonEduDocumentWrapper.DATE_ISSUE_CERTIFICATE))
                .addColumn(textColumn("expectedDateReturn", PersonEduDocumentWrapper.EXPECTED_DATE_RETURN))
                .addColumn(textColumn("actualDateReturn", PersonEduDocumentWrapper.ACTUAL_DATE_RETURN))
                .create();
    }

    @Bean
    public ColumnListExtension eduInstitutionSColumns()
    {
        return columnListExtensionBuilder(personEduDocumentList.eduInstitutionSColumns())
                .addColumn(textColumn("dateIssueCertificate", PersonEduInstitutionWrapper.DATE_ISSUE_CERTIFICATE))
                .addColumn(textColumn("expectedDateReturn", PersonEduInstitutionWrapper.EXPECTED_DATE_RETURN))
                .addColumn(textColumn("actualDateReturn", PersonEduInstitutionWrapper.ACTUAL_DATE_RETURN))
                .create();

    }

    private final class DeleteEduDocument extends NamedUIAction
    {

        protected DeleteEduDocument(String name)
        {
            super(name);
        }

        @Override
        public void execute(IUIPresenter presenter)
        {
            final Long eduDocId = presenter.getListenerParameterAsLong();
            final IPersonEduDocumentDao dao = (IPersonEduDocumentDao) PersonEduDocumentManager.instance().dao();
            dao.cleanStudentsWithEduDocument(eduDocId);
            dao.deleteDocument(eduDocId);
        }
    }
}
