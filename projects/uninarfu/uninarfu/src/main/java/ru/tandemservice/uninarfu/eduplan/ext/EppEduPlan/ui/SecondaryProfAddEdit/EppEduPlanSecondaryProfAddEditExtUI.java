/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.SecondaryProfAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEditUI;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.SecondaryProfAddEdit.EppEduPlanSecondaryProfAddEditUI;
import ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEditExt;
import ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation;

/**
 * @author Ekaterina Zvereva
 * @since 21.04.2015
 */
public class EppEduPlanSecondaryProfAddEditExtUI extends UIAddon
{
    public EppEduPlanSecondaryProfAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private NarfuEduPlanToEduLVLHSRelation relObject;

    private EppEduPlanSecondaryProfAddEditUI _presenter;

    public NarfuEduPlanToEduLVLHSRelation getRelObject()
    {
        return relObject;
    }

    public void setRelObject(NarfuEduPlanToEduLVLHSRelation relObject)
    {
        this.relObject = relObject;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = getPresenter();
        relObject = DataAccessServices.dao().get(NarfuEduPlanToEduLVLHSRelation.class, NarfuEduPlanToEduLVLHSRelation.eduPlan(), _presenter.getElement());
        if (relObject == null)
        {
            relObject = new NarfuEduPlanToEduLVLHSRelation();
            relObject.setEduPlan(_presenter.getElement());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(EppEduPlanSecondaryProfAddEditExt.EDULVLHS_DS))
        {
            dataSource.put(EppEduPlanSecondaryProfAddEditExt.PROGRAM_SUBJECT, _presenter.getElement().getProgramSubject());
            dataSource.put(EppEduPlanSecondaryProfAddEditExt.QUALIFICATION, _presenter.getElement().getProgramQualification());
            dataSource.put(EppEduPlanSecondaryProfAddEditExt.ORG_UNIT, _presenter.getElement().getOwner());
        }
    }
}
