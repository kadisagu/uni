/* $Id$ */
package ru.tandemservice.uninarfu.order.ext.EnrOrder.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.AddEdit.EnrOrderAddEdit;

/**
 * @author Ekaterina Zvereva
 * @since 30.07.2015
 */
@Configuration
public class EnrOrderAddEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDONE_NAME = "EnrOrderAddEditExt";

    @Autowired
    private EnrOrderAddEdit _enrOrderAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrOrderAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDONE_NAME, EnrOrderAddEditExtUI.class))
                .addAction(new EnrOrderAddEditApply("onClickApply"))
                .create();
    }
}
