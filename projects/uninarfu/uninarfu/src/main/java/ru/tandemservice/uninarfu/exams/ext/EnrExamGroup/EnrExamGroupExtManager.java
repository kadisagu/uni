/* $Id$ */
package ru.tandemservice.uninarfu.exams.ext.EnrExamGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.IEnrExamGroupEnrollPassSheetPrintDao;
import ru.tandemservice.uninarfu.exams.ext.EnrExamGroup.logic.EnrExamGroupEnrollPassSheetPrintDao;

/**
 * @author Ekaterina Zvereva
 * @since 19.06.2015
 */
@Configuration
public class EnrExamGroupExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public IEnrExamGroupEnrollPassSheetPrintDao enrPassSheetPrintDao()
    {
        return new EnrExamGroupEnrollPassSheetPrintDao();
    }
}
