/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 17.06.2015
 */
public class NarfuEmployeeEnrCommissionDSHandler extends DefaultComboDataSourceHandler
{

    public static final String ENROLLMENT_COMMISSIONS = "enrollmentCommissions";

    public NarfuEmployeeEnrCommissionDSHandler(String ownerId)
    {
        super(ownerId, EmployeePost.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String filter = input.getComboFilterByValue();
        final Set primaryKeys = input.getPrimaryKeys();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEnrollmentCommissionExt.class, "ext")
                .joinPath(DQLJoinType.inner, EnrEnrollmentCommissionExt.employee().fromAlias("ext"), "e")
                .column(property("e"));

        if (primaryKeys != null &&  !primaryKeys.isEmpty())
            builder.where(in(property("e.id"), primaryKeys));

        // фильтр по фио + должность + подр. + тип подр.
        if (StringUtils.isNotEmpty(filter))
        {
            IDQLExpression expression = DQLFunctions.concat(property("e", EmployeePost.employee().person().identityCard().fullFio().s()),
                                                            DQLFunctions.concat(property("e", EmployeePost.postRelation().postBoundedWithQGandQL().post().title().s()),
                                                                                DQLFunctions.concat(property("e", EmployeePost.orgUnit().shortTitle().s()),
                                                                                                    property("e", EmployeePost.orgUnit().orgUnitType().title().s())
                                                                                )));


            builder.where(DQLExpressions.like(DQLFunctions.upper(expression), value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
        }
        builder.where(eq(property("e", EmployeePost.postStatus().active().s()), value(Boolean.TRUE)));

        // не архивные сотрудники
        builder.where(eq(property("e", EmployeePost.employee().archival().s()), value(Boolean.FALSE)));

        // сортируем по фио
        builder.order(property("e", EmployeePost.employee().person().identityCard().fullFio().s()));

        List<EnrEnrollmentCommission> commissionsList = context.get(ENROLLMENT_COMMISSIONS);

        if (commissionsList != null && !commissionsList.isEmpty())
            builder.where(in(property("ext", EnrEnrollmentCommissionExt.enrEnrollmentCommission()), commissionsList));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(EmployeePost.class, EmployeePost.P_FULL_TITLE).build();
    }


}