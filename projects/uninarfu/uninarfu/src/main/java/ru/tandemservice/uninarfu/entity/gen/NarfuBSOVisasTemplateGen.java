package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Шаблон визирования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuBSOVisasTemplateGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate";
    public static final String ENTITY_NAME = "narfuBSOVisasTemplate";
    public static final int VERSION_HASH = 584830011;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_ORDER = "order";

    private String _title;     // Название
    private String _order;     // Номер и дата приказа утверждения комиссии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер и дата приказа утверждения комиссии.
     */
    @Length(max=255)
    public String getOrder()
    {
        return _order;
    }

    /**
     * @param order Номер и дата приказа утверждения комиссии.
     */
    public void setOrder(String order)
    {
        dirty(_order, order);
        _order = order;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NarfuBSOVisasTemplateGen)
        {
            setTitle(((NarfuBSOVisasTemplate)another).getTitle());
            setOrder(((NarfuBSOVisasTemplate)another).getOrder());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuBSOVisasTemplateGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuBSOVisasTemplate.class;
        }

        public T newInstance()
        {
            return (T) new NarfuBSOVisasTemplate();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "title":
                    return obj.getTitle();
                case "order":
                    return obj.getOrder();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "order":
                    obj.setOrder((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "title":
                        return true;
                case "order":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "title":
                    return true;
                case "order":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "title":
                    return String.class;
                case "order":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuBSOVisasTemplate> _dslPath = new Path<NarfuBSOVisasTemplate>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuBSOVisasTemplate");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер и дата приказа утверждения комиссии.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate#getOrder()
     */
    public static PropertyPath<String> order()
    {
        return _dslPath.order();
    }

    public static class Path<E extends NarfuBSOVisasTemplate> extends EntityPath<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<String> _order;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(NarfuBSOVisasTemplateGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер и дата приказа утверждения комиссии.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate#getOrder()
     */
        public PropertyPath<String> order()
        {
            if(_order == null )
                _order = new PropertyPath<String>(NarfuBSOVisasTemplateGen.P_ORDER, this);
            return _order;
        }

        public Class getEntityClass()
        {
            return NarfuBSOVisasTemplate.class;
        }

        public String getEntityName()
        {
            return "narfuBSOVisasTemplate";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
