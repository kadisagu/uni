/* $Id$ */
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.Statement;

/**
 * @author Ekaterina Zvereva
 * @since 07.08.2015
 */
public class MS_uninarfu_2x8x2_2to3 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.8.2")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        Statement requestStatement = tool.getConnection().createStatement();
        requestStatement.execute("delete from enr14_c_enr_par_form_type_t where CODE_P='narfu_paragraph'");
    }
}