/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings.ui.OrgUnitNumbersList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.util.Icon;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uninarfu.base.bo.NarfuSettings.NarfuSettingsManager;
import ru.tandemservice.uninarfu.base.bo.NarfuSettings.logic.NarfuFormativeOrgUnitsCodesDSHandler;

/**
 * @author Ekaterina Zvereva
 * @since 17.06.2015
 */
@Configuration
public class NarfuSettingsOrgUnitNumbersList extends BusinessComponentManager
{
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitsColumns(), NarfuSettingsManager.instance().getFormativeOrgUnitsCodesHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint formativeOrgUnitsColumns()
    {
        return columnListExtPointBuilder(FORMATIVE_ORG_UNIT_DS)
                .addColumn(textColumn("title", OrgUnit.title()).clickable(false))
                .addColumn(textColumn("code", NarfuFormativeOrgUnitsCodesDSHandler.PROPERTY_CODE))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, new Icon(EDIT_COLUMN_NAME), "onClickEditCode"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, new Icon(DELETE_COLUMN_NAME), "onClickDeleteCode")
                                   .alert(new FormattedMessage("orgUnits.delete.alert", OrgUnit.title().s())).disabled(NarfuFormativeOrgUnitsCodesDSHandler.PROPERTY_NOT_EXIST))
                .create();
    }
}
