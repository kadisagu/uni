/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DipDocument.ui.AddInformationAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEditUI;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 07.04.2015
 */
public class DipDocumentAddInformationAddEditExtUI extends UIAddon
{
    private DipDocumentAddInformationAddEditUI _presenter;
    private EduProgramForm _eduProgramForm;
    private List<EduProgramForm> _studentProgramFormList;

    public DipDocumentAddInformationAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
        _presenter = (DipDocumentAddInformationAddEditUI) presenter;
    }

    @Override
    public void onComponentRefresh()
    {
        _eduProgramForm = _presenter.getDiplomaObject().getStudent().getEducationOrgUnit().getDevelopForm().getProgramForm();
        if (!_presenter.isShowEduProgramForm() && _eduProgramForm != null)
        {
            _studentProgramFormList = new ArrayList<>();
            _studentProgramFormList.add(_eduProgramForm);
            _presenter.setEduFormList(_studentProgramFormList);
        }
    }

    public void clearEduProgramForm()
    {
        if (!_presenter.isShowEduProgramForm() && _eduProgramForm != null)
        {
            _studentProgramFormList = new ArrayList<>();
            _studentProgramFormList.add(_eduProgramForm);
            _presenter.setEduFormList(_studentProgramFormList);
        }
    }

    public void onClickApply()
    {
        if(!_presenter.isShowEduProgramForm())
        {
            _presenter.setEduFormList(null);
        }
        _presenter.onClickApply();
    }
}
