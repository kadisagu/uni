/*$Id$*/
package ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.logic.IJournalIssuanceCertificatesDao;
import ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.logic.JournalIssuanceCertificatesDao;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
@Configuration
public class JournalIssuanceCertificatesManager extends BusinessObjectManager {

    public static JournalIssuanceCertificatesManager instance() {
        return instance(JournalIssuanceCertificatesManager.class);
    }

    @Bean
    public IJournalIssuanceCertificatesDao getDao() {
        return new JournalIssuanceCertificatesDao();
    }
}
