/* $Id$ */
package ru.tandemservice.uninarfu.attestation.ext.AttestationBulletin;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Andrey Andreev
 * @since 08.12.2015
 */
@Configuration
public class AttestationBulletinExtManager extends BusinessObjectExtensionManager
{
}