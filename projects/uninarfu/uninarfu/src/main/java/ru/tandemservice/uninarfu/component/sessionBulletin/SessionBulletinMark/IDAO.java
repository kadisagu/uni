/* $Id$ */
package ru.tandemservice.uninarfu.component.sessionBulletin.SessionBulletinMark;

/**
 * @author Ekaterina Zvereva
 * @since 05.05.2015
 */
public interface IDAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.IDAO
{
    void setMassMark(Model model);
}
