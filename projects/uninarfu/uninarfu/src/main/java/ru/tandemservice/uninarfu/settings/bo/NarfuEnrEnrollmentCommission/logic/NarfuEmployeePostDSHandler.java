/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuEnrEnrollmentCommission.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 10.06.2015
 */
public class NarfuEmployeePostDSHandler extends EntityComboDataSourceHandler
{

    public NarfuEmployeePostDSHandler(String ownerId, Class<? extends IEntity> entityClass)
    {
        super(ownerId, entityClass);
    }


    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);


        // только активные сотрудники
        dql.where(eq(property("e", EmployeePost.postStatus().active().s()), value(Boolean.TRUE)));

        // не архивные сотрудники
        dql.where(eq(property("e", EmployeePost.employee().archival().s()), value(Boolean.FALSE)));

        // сортируем по фио
        dql.order(property(alias, EmployeePost.employee().person().identityCard().fullFio().s()));

    }


    @Override
    protected DQLSelectBuilder query(String alias, String filter)
    {
        DQLSelectBuilder builder = super.query(alias, filter);
        // фильтр по фио + должность + подр. + тип подр.
        if (StringUtils.isNotEmpty(filter))
        {
            IDQLExpression expression = DQLFunctions.concat(property(alias, EmployeePost.employee().person().identityCard().fullFio().s()),
                                                            DQLFunctions.concat(property(alias, EmployeePost.postRelation().postBoundedWithQGandQL().post().title().s()),
                                                                                DQLFunctions.concat(property(alias, EmployeePost.orgUnit().shortTitle().s()),
                                                                                                    property(alias, EmployeePost.orgUnit().orgUnitType().title().s())
                                                                                )));

            builder.where(DQLExpressions.like(DQLFunctions.upper(expression), value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
        }
        return builder;
    }
}