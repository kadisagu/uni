package ru.tandemservice.uninarfu.component.workplan.WorkPlanRowAddEdit;

import ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt;

public class DAO extends ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        EppWorkPlanRowExt rowExt = new EppWorkPlanRowExt();
        rowExt.setRow(model.getRow());
        if (model.getRow().getId() != null &&
                get(EppWorkPlanRowExt.class, EppWorkPlanRowExt.row().id(), model.getRow().getId()) != null)
            rowExt = get(EppWorkPlanRowExt.class, EppWorkPlanRowExt.row().id(), model.getRow().getId());
        myModel.setRowExt(rowExt);
    }

    @Override
    public void update(ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;

        EppWorkPlanRowExt rowExt = get(EppWorkPlanRowExt.class, EppWorkPlanRowExt.row().id(), model.getRow().getId());
        if (rowExt != null) {
            rowExt.setRecertification(myModel.getRowExt().isRecertification());
            getSession().update(rowExt);
        }
        else
            getSession().save(myModel.getRowExt());


    }
}
