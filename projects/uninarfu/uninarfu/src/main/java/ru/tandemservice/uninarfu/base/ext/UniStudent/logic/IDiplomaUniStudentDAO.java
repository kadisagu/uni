package ru.tandemservice.uninarfu.base.ext.UniStudent.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Qualifications;

import java.util.List;

public interface IDiplomaUniStudentDAO extends INeedPersistenceSupport {
    /**
     * Проверяет, одинаковый ли направление подготоки ГОС у студентов из списка.
     * Если одинаковая, то она возвращается.
     * В противном случае возводится исключение
     */
    public EducationLevels getEducationLevels(List<Long> studentIds);

    /**
     * Проверяет, одинаковая ли квалификация из УП у студентов из списка. Если одинаковая, то она возвращается
     * Если у одного из студентов нет УП, а у др. есть, возводится исключение.
     * Если у всех студентов не привязан УП, возвращается null
     */
    public Qualifications getQualificationsFromEduPlan(List<Long> studentIds);
}
