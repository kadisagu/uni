/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.logic.INarfuEntrantDAO;
import ru.tandemservice.uninarfu.base.bo.NarfuEntrant.logic.NarfuEntrantDAO;
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests;

/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
@Configuration
public class NarfuEntrantManager extends BusinessObjectManager
{
    public static NarfuEntrantManager instance()
    {
        return instance(NarfuEntrantManager.class);
    }

    @Bean
    public INarfuEntrantDAO dao() {return new NarfuEntrantDAO();}


    @Bean
    public IBusinessHandler<DSInput, DSOutput> specialConditionsExamsDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), NarfuSpecConditionsEntTests.class);
    }
}