/* $Id$ */
package ru.tandemservice.uninarfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Andrey Andreev
 * @since 06.11.2015
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x9x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.2"),
                        new ScriptDependency("ru.tandemservice.movestudentrmc", "2.9.2"),
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("personnarfu_t")) {
            tool.executeUpdate("UPDATE person_t "
                            + "SET needdormitory_p = pn.NEEDDORMITORYONSTUDINGPERIOD_P "
                            + "FROM personnarfu_t pn "
                            + "WHERE person_t.id = pn.person_id"
            );

            tool.dropColumn("personnarfu_t", "needdormitoryonstudingperiod_p");
        }
    }
}