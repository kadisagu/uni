/*$Id$*/
package ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.logic;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
public class JournalDocumentWrapper extends ViewWrapper<PersonEduDocumentNARFU> {

    public static final String STUDENT = "student";

    public JournalDocumentWrapper(PersonEduDocumentNARFU entity, Student student) {
        super(entity);
        setViewProperty(STUDENT, student);
    }
}
