package ru.tandemservice.uninarfu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem;
import ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Визы для акта списания БСО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NarfuBSODisposalActVisaItemGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem";
    public static final String ENTITY_NAME = "narfuBSODisposalActVisaItem";
    public static final int VERSION_HASH = 1043288836;
    private static IEntityMeta ENTITY_META;

    public static final String L_POSSIBLE_VISA = "possibleVisa";
    public static final String L_VISA_TEMPLATE = "visaTemplate";
    public static final String P_PRIORITY = "priority";

    private EmployeePostPossibleVisa _possibleVisa;     // Возможная виза сотрудника
    private NarfuBSOVisasTemplate _visaTemplate;     // Шаблон визирования
    private int _priority;     // Приоритет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Возможная виза сотрудника. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostPossibleVisa getPossibleVisa()
    {
        return _possibleVisa;
    }

    /**
     * @param possibleVisa Возможная виза сотрудника. Свойство не может быть null.
     */
    public void setPossibleVisa(EmployeePostPossibleVisa possibleVisa)
    {
        dirty(_possibleVisa, possibleVisa);
        _possibleVisa = possibleVisa;
    }

    /**
     * @return Шаблон визирования. Свойство не может быть null.
     */
    @NotNull
    public NarfuBSOVisasTemplate getVisaTemplate()
    {
        return _visaTemplate;
    }

    /**
     * @param visaTemplate Шаблон визирования. Свойство не может быть null.
     */
    public void setVisaTemplate(NarfuBSOVisasTemplate visaTemplate)
    {
        dirty(_visaTemplate, visaTemplate);
        _visaTemplate = visaTemplate;
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NarfuBSODisposalActVisaItemGen)
        {
            setPossibleVisa(((NarfuBSODisposalActVisaItem)another).getPossibleVisa());
            setVisaTemplate(((NarfuBSODisposalActVisaItem)another).getVisaTemplate());
            setPriority(((NarfuBSODisposalActVisaItem)another).getPriority());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NarfuBSODisposalActVisaItemGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NarfuBSODisposalActVisaItem.class;
        }

        public T newInstance()
        {
            return (T) new NarfuBSODisposalActVisaItem();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "possibleVisa":
                    return obj.getPossibleVisa();
                case "visaTemplate":
                    return obj.getVisaTemplate();
                case "priority":
                    return obj.getPriority();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "possibleVisa":
                    obj.setPossibleVisa((EmployeePostPossibleVisa) value);
                    return;
                case "visaTemplate":
                    obj.setVisaTemplate((NarfuBSOVisasTemplate) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "possibleVisa":
                        return true;
                case "visaTemplate":
                        return true;
                case "priority":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "possibleVisa":
                    return true;
                case "visaTemplate":
                    return true;
                case "priority":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "possibleVisa":
                    return EmployeePostPossibleVisa.class;
                case "visaTemplate":
                    return NarfuBSOVisasTemplate.class;
                case "priority":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NarfuBSODisposalActVisaItem> _dslPath = new Path<NarfuBSODisposalActVisaItem>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NarfuBSODisposalActVisaItem");
    }
            

    /**
     * @return Возможная виза сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem#getPossibleVisa()
     */
    public static EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> possibleVisa()
    {
        return _dslPath.possibleVisa();
    }

    /**
     * @return Шаблон визирования. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem#getVisaTemplate()
     */
    public static NarfuBSOVisasTemplate.Path<NarfuBSOVisasTemplate> visaTemplate()
    {
        return _dslPath.visaTemplate();
    }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    public static class Path<E extends NarfuBSODisposalActVisaItem> extends EntityPath<E>
    {
        private EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> _possibleVisa;
        private NarfuBSOVisasTemplate.Path<NarfuBSOVisasTemplate> _visaTemplate;
        private PropertyPath<Integer> _priority;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Возможная виза сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem#getPossibleVisa()
     */
        public EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa> possibleVisa()
        {
            if(_possibleVisa == null )
                _possibleVisa = new EmployeePostPossibleVisa.Path<EmployeePostPossibleVisa>(L_POSSIBLE_VISA, this);
            return _possibleVisa;
        }

    /**
     * @return Шаблон визирования. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem#getVisaTemplate()
     */
        public NarfuBSOVisasTemplate.Path<NarfuBSOVisasTemplate> visaTemplate()
        {
            if(_visaTemplate == null )
                _visaTemplate = new NarfuBSOVisasTemplate.Path<NarfuBSOVisasTemplate>(L_VISA_TEMPLATE, this);
            return _visaTemplate;
        }

    /**
     * @return Приоритет. Свойство не может быть null.
     * @see ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(NarfuBSODisposalActVisaItemGen.P_PRIORITY, this);
            return _priority;
        }

        public Class getEntityClass()
        {
            return NarfuBSODisposalActVisaItem.class;
        }

        public String getEntityName()
        {
            return "narfuBSODisposalActVisaItem";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
