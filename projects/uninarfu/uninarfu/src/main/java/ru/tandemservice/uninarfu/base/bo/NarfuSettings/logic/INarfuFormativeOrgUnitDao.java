/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
public interface INarfuFormativeOrgUnitDao
{
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void deleteOrgUnitCode(Long orgUnitId);

}
