package ru.tandemservice.uninarfu.ws.sakai.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "orderWrap", namespace = "http://sakai.ws.uninarfu.tandemservice.ru/")
public class WsOrderWrap implements Serializable {

    private static final long serialVersionUID = -8289783164084368749L;

    /**
     * Название представления
     */
    private String title;
    /**
     * Номер приказа
     */
    private String number;
    /**
     * Дата приказа
     */
    private String orderDate;
    /**
     * Дата вступления приказа в силу (startDate из представления)
     */
    private String orderStartDate;

    /**
     * Дата с (у представления)
     */
    private String startDate;

    /**
     * Дата по (у представления)
     */
    private String endDate;

    public WsOrderWrap() {
    }

    public WsOrderWrap(String title, String number, String orderDate) {
        this.title = title;
        this.number = number;
        this.orderDate = orderDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStartDate() {
        return orderStartDate;
    }

    public void setOrderStartDate(String orderStartDate) {
        this.orderStartDate = orderStartDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
