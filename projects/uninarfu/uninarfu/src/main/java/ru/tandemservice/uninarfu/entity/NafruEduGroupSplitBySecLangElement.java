package ru.tandemservice.uninarfu.entity;

import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uninarfu.entity.gen.NafruEduGroupSplitBySecLangElementGen;

/** @see ru.tandemservice.uninarfu.entity.gen.NafruEduGroupSplitBySecLangElementGen */
public class NafruEduGroupSplitBySecLangElement extends NafruEduGroupSplitBySecLangElementGen
{
    public NafruEduGroupSplitBySecLangElement()
    {
    }

    public NafruEduGroupSplitBySecLangElement(EppEduGroupSplitVariant splitVariant, ForeignLanguage foreignLanguage)
    {
        setSplitVariant(splitVariant);
        setForeignLanguage(foreignLanguage);
    }

    @Override
    public Long getSplitElementId()
    {
        return getForeignLanguage().getId();
    }

    @Override
    public String getSplitElementShortTitle()
    {
        return getForeignLanguage().getShortTitle();
    }

    @Override
    public String getSplitElementTitle()
    {
        return getForeignLanguage().getTitle();
    }
}