package ru.tandemservice.uninarfu.component.workplan.WorkPlanRowAddEdit;

import ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt;

public class Model extends ru.tandemservice.uniepp.component.workplan.WorkPlanRowAddEdit.Model {

    private EppWorkPlanRowExt rowExt;

    public EppWorkPlanRowExt getRowExt() {
        return rowExt;
    }

    public void setRowExt(EppWorkPlanRowExt rowExt) {
        this.rowExt = rowExt;
    }

}
