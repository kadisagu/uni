/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocument.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 30.07.2015
 */
public interface INarfuDipDocumentDao extends INeedPersistenceSupport
{
    /**
     * Возвращает список подразделений, для которых не задан
     * внутренний код форм. подразделения
     *
     * @param studentIds список id студентов
     * @return список подразделений fullTitle
     */
    List<String> getDivisionCodeEmptyOrgUnitFullTitles(List<Long> studentIds);
}
