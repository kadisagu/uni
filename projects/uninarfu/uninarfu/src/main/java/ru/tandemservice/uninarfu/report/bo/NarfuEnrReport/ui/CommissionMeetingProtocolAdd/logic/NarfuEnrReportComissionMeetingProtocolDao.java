/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.logic;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild;
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.*;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrCancelExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentMinisteryExtract;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uninarfu.entity.NarfuEnrollmentCommissionMeetingProtocol;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.CommissionMeetingProtocolAdd.NarfuEnrReportCommissionMeetingProtocolAddUI;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 28.07.2015
 */
public class NarfuEnrReportComissionMeetingProtocolDao extends UniBaseDao implements INarfuEnrReportComissionMeetingProtocolDao
{
    private static final String T1 = "T1";

    @Override
    public Long createReport(NarfuEnrReportCommissionMeetingProtocolAddUI model)
    {
        NarfuEnrollmentCommissionMeetingProtocol report = new NarfuEnrollmentCommissionMeetingProtocol();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());
        if (model.isEnrollmentCommissionActive())
        {
            report.setEnrollmentCommission(CommonBaseStringUtil.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title(), "; "));
        }

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, NarfuEnrollmentCommissionMeetingProtocol.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, NarfuEnrollmentCommissionMeetingProtocol.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, NarfuEnrollmentCommissionMeetingProtocol.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, NarfuEnrollmentCommissionMeetingProtocol.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, NarfuEnrollmentCommissionMeetingProtocol.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, NarfuEnrollmentCommissionMeetingProtocol.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, NarfuEnrollmentCommissionMeetingProtocol.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, NarfuEnrollmentCommissionMeetingProtocol.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, NarfuEnrollmentCommissionMeetingProtocol.P_PROGRAM_SET, "title");
        if (model.getParallelSelector().isParallelActive()) {
            report.setParallel(model.getParallelSelector().getParallel().getTitle());
        }
        if (model.isDoNotShowCommissionResolution())
            report.setEnrollmentCommissionResolution("Не заполнять");
        else report.setEnrollmentCommissionResolution("Заполнять");

        report.setIncludeTookAwayDocuments(model.isIncludeTookAwayDocuments() ? "Включены в отчет" : "Не включены в отчет");

        if (model.getExcludeInOrderSelector().isExcludeInOrderActive())
            report.setOrderStates(UniStringUtils.join(model.getExcludeInOrderSelector().getSelectedStates(), OrderStates.title(), ", "));

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReport(model, EnrScriptItemCodes.REPORT_NARFU_ENR_COM_MEET_PROTOCOL, model.isIncludeTookAwayDocuments()));
        content.setFilename("EnrReportEnrollmentCommissionMeetingProtocol.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }


    protected byte[] buildReport(final NarfuEnrReportCommissionMeetingProtocolAddUI model, String templateCatalogCode, boolean includeTookAwayDocuments)
    {
        //rtf
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, templateCatalogCode);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        // Выбираем абитуриентов, подходящих под параметры отчета
        final Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();

        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, filterAddon, true, includeTookAwayDocuments);

        requestedCompDQL.order(property(EnrRatingItem.position().fromAlias(requestedCompDQL.rating())));
        requestedCompDQL.column(requestedCompDQL.rating());

        Set<Long> entrantsIds = Sets.newHashSet();

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>();
        for (EnrRatingItem ratingItem : requestedCompDQL.createStatement(getSession()).<EnrRatingItem>list()) {
            SafeMap.safeGet(entrantMap, ratingItem.getCompetition(), ArrayList.class).add(ratingItem);
            nonEmptyBlocks.add(ratingItem.getCompetition().getProgramSetOrgUnit());
            entrantsIds.add(ratingItem.getEntrant().getId());
        }

        //{entrant.id -> {requestedCompetition.id -> priority}}
        final Map<Long, Map<Long, Integer>> recommendedReqCompMap = Maps.newHashMap();

        BatchUtils.execute(entrantsIds, 100, elements -> {
            DQLSelectBuilder rcSubBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rc");
            rcSubBuilder.column(property("rc", EnrRequestedCompetition.request().entrant().id()), "entrantId");
            rcSubBuilder.column(DQLFunctions.max(property("rc", EnrRequestedCompetition.priority())), "minPriority");
            rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.request().entrant().enrollmentCampaign().id()), value(model.getEnrollmentCampaign().getId())));
            rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.RECOMMENDED)));
            rcSubBuilder.where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)));
            rcSubBuilder.where(in(property("rc", EnrRequestedCompetition.request().entrant().id()), elements));
            rcSubBuilder.group(property("rc", EnrRequestedCompetition.request().entrant().id()));


            DQLSelectBuilder rcBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "c");
            rcBuilder.column(property("c", EnrRequestedCompetition.id()));
            rcBuilder.column(property("c", EnrRequestedCompetition.priority()));
            rcBuilder.column(property("c", EnrRequestedCompetition.request().entrant().id()));
            rcBuilder.joinPath(DQLJoinType.inner, EnrRequestedCompetition.request().entrant().fromAlias("c"), "entrant");
            rcBuilder.joinDataSource("c", DQLJoinType.inner, rcSubBuilder.buildQuery(), "ds", eq(property("ds.entrantId"), property("entrant", EnrEntrant.id())));
            rcBuilder.where(eq(property("ds.minPriority"), property("c", EnrRequestedCompetition.priority())));

            for (Object[] row : rcBuilder.createStatement(getSession()).<Object[]>list())
            {
                Long competitionId = (Long) row[0];
                Integer priority = (Integer) row[1];
                Long entrantId = (Long) row[2];

                recommendedReqCompMap.put(entrantId, Maps.<Long, Integer>newHashMap());
                recommendedReqCompMap.get(entrantId).put(competitionId, priority);
            }
        });

        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов (не путать с массивом выбранных конкурсов, о котором уже было выше написано).

        Collection<EnrCompetition> competitions = filterAddon.getFilteredList();

        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition()) {
            competitions = entrantMap.keySet();
        }

        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<>(new EntityOrder(EnrCompetition.type().code().s())));

        // Полученный массив конкурсов мы группируем по ключу: набор ОП, филиал, формирующее подразделение.
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList) {
            // Убираем пустые таблицы
            if (model.isSkipEmptyList() && !nonEmptyBlocks.contains(competition.getProgramSetOrgUnit())) continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        // Сортировка списков -
        // вначале по филиалу (вначале головная организация, потом остальные по названию),
        // затем по форме обучения (по коду),
        // затем по виду ОП,
        // затем по направлению подготовки,
        // затем по набору ОП (по названию)
        List<EnrProgramSetOrgUnit> blockList = new ArrayList<>();
        blockList.addAll(blockMap.keySet());
        Collections.sort(blockList, (o1, o2) -> {
            int result;
            result = - Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
            if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
            if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
            return result;
        });

        // подгрузим образовательные программы наборов
        Collection<EnrProgramSetBase> programSets = CollectionUtils.collect(blockList, EnrProgramSetOrgUnit::getProgramSet);

        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class);
        for (EnrProgramSetItem item : getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets)) {
            programMap.get(item.getProgramSet()).add(item.getProgram());
        }
        for (EnrProgramSetBase programSetBase : programSets) {
            if (programSetBase instanceof EnrProgramSetSecondary) {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary)programSetBase).getProgram());
            }
        }

        // берем из шаблона таблицы
        RtfTable topTable = UniRtfUtil.removeTable(document, "TOP", true);
        RtfTable headerTable = UniRtfUtil.removeTable(document, "HEADER", true);
        RtfTable contentTable = UniRtfUtil.removeTable(document, T1, false);
        RtfTable bottomTable = UniRtfUtil.removeTable(document, "BOTTOM", true);

        // и чистим шаблон, чтобы не было ненужных переводов строк
        document.getElementList().clear();
        document.getElementList().add(topTable.getClone());

        fillTopHeaderData(document, model);

        // выводим блоки отчета
        for (EnrProgramSetOrgUnit programSetOrgUnit : blockList)
        {

            // сначала выводим заголовочную таблицу блока
            List<EnrTargetAdmissionPlan> taPlans = getTaPlans(programSetOrgUnit);

            document.getElementList().add(headerTable.getClone());

            List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());

            List<EnrCompetition> enrCompetitions = blockMap.get(programSetOrgUnit);

            boolean hasRecommendedOther = false;
            for (EnrCompetition competition : enrCompetitions)
            {
                if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0)
                {
                    // если прием по ЦП отдельно, делим списки по видам ЦП
                    for (EnrTargetAdmissionPlan taPlan : taPlans)
                    {
                        List<EnrRatingItem> entrantList = new ArrayList<>();
                        for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class))
                        {
                            if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA) entrant.getRequestedCompetition()).getTargetAdmissionKind()))
                            {
                                entrantList.add(entrant);
                            }
                        }
                        if (model.isSkipEmptyCompetition() && entrantList.isEmpty())
                        {
                            continue;
                        }

                        for (EnrRatingItem entrant : entrantList)
                        {
                            Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                            if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                            {
                                hasRecommendedOther = true;
                            }
                        }
                    }
                }
                else
                {
                    List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                    if (entrantList != null)
                    {
                        for (EnrRatingItem entrant : entrantList)
                        {
                            Map<Long, Integer> recommendedReqComp = recommendedReqCompMap.get(entrant.getEntrant().getId());
                            if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode()) && recommendedReqComp != null && recommendedReqComp.get(entrant.getRequestedCompetition().getId()) == null && recommendedReqComp.get(recommendedReqComp.keySet().iterator().next()) < entrant.getRequestedCompetition().getPriority())
                            {
                                hasRecommendedOther = true;
                            }
                        }
                    }
                }
            }

            fillHeaderData(document, programSetOrgUnit, programs, hasRecommendedOther, model);

            // теперь списки по каждому конкурсу
            MutableInt ministerialRequestTotal = new MutableInt(0); MutableInt contractRequestTotal = new MutableInt(0);
            for (EnrCompetition competition : enrCompetitions) {

                if (competition.isTargetAdmission() && taPlans != null && taPlans.size() > 0) {
                    // если прием по ЦП отдельно, делим списки по видам ЦП
                    for (EnrTargetAdmissionPlan taPlan : taPlans) {
                        List<EnrRatingItem> entrantList = new ArrayList<>();
                        for (EnrRatingItem entrant : SafeMap.safeGet(entrantMap, competition, ArrayList.class)) {
                            if (entrant.getRequestedCompetition() instanceof EnrRequestedCompetitionTA && taPlan.getEnrCampaignTAKind().equals(((EnrRequestedCompetitionTA)entrant.getRequestedCompetition()).getTargetAdmissionKind())) {
                                entrantList.add(entrant);
                            }
                        }
                        if (model.isSkipEmptyCompetition() && entrantList.isEmpty()) {
                            continue;
                        }
                        final String tableHeader = getTableHeader(competition, taPlan, entrantList.size());
                        printContentTable(entrantList, document, tableHeader,
                                          ministerialRequestTotal, contractRequestTotal,
                                          contentTable.getClone(),
                                          !model.isDoNotShowCommissionResolution());
                    }
                } else {
                    List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);
                    final String tableHeader = getTableHeader(competition, null, entrantList.size());
                    printContentTable(entrantList, document, tableHeader,
                                      ministerialRequestTotal, contractRequestTotal,
                                      contentTable.getClone(),!model.isDoNotShowCommissionResolution());
                }
            }


            if (blockList.indexOf(programSetOrgUnit) != blockList.size() - 1) {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
        }

        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(bottomTable.getClone());
        return RtfUtil.toByteArray(document);

    }

    private void fillTopHeaderData(RtfDocument document, NarfuEnrReportCommissionMeetingProtocolAddUI model)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("protocolNumber", model.getProtocolNumber() != null? model.getProtocolNumber() : "______________");
        modifier.put("protocolDate", model.getProtocolDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getProtocolDate()) : "______________");
        modifier.put("executiveSecretary", model.getSecretary() != null ? model.getSecretary().getFio() : "");
        EduProgramForm developForm = (EduProgramForm)model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM).getValue();
        modifier.put("developForm", getProgramFormString(developForm));

        CompensationType compensationType = (CompensationType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue();
        modifier.put("compensationType", getCompensationTypeString(compensationType));
        modifier.modify(document);
    }

    private EnrEntrantDQLSelectBuilder prepareEntrantDQL(NarfuEnrReportCommissionMeetingProtocolAddUI model, EnrCompetitionFilterAddon filterAddon, boolean fetch, boolean includeTookAwayDocuments)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign())
                ;

        if (includeTookAwayDocuments) {
            requestedCompDQL.where(ne(property(EnrRequestedCompetition.state().code().fromAlias(requestedCompDQL.reqComp())), value(EnrEntrantStateCodes.OUT_OF_COMPETITION)));
        } else {
            requestedCompDQL.defaultActiveFilter();
        }

        requestedCompDQL.where(eq(property(requestedCompDQL.rating(), EnrRatingItem.statusRatingPositive()), value(Boolean.TRUE)));

        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.getParallelSelector().isParallelOnly()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)));
        }
        if (model.getParallelSelector().isSkipParallel()) {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)));
        }
        if (model.isEnrollmentCommissionActive())
            requestedCompDQL.where(DQLExpressions.in(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.request().enrollmentCommission()), model.getEnrollmentCommissionList()));

        //Исключим студентов в приказах
        if (model.getExcludeInOrderSelector().isExcludeInOrderActive())
        {
            if (!model.getExcludeInOrderSelector().getSelectedStates().isEmpty())
                requestedCompDQL.where(or(
                        and(DQLExpressions.notExists(new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "ext")
                                                               .where(eq(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.request().entrant()), property("ext", EnrEnrollmentExtract.entity().request().entrant())))
                                                               .where(in(property("ext", EnrEnrollmentExtract.state()), model.getExcludeInOrderSelector().getSelectedStates())).buildQuery()),
                            DQLExpressions.notExists(new DQLSelectBuilder().fromEntity(EnrEnrollmentMinisteryExtract.class, "extM")
                                                             .where(eq(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.request().entrant()),
                                                                       property("extM", EnrEnrollmentMinisteryExtract.entrantRequest().entrant())))
                                                             .where(in(property("extM", EnrEnrollmentExtract.state()), model.getExcludeInOrderSelector().getSelectedStates())).buildQuery())),
                                          DQLExpressions.exists(new DQLSelectBuilder().fromEntity(EnrCancelExtract.class, "extCancel")
                                                                        .where(eq(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.id()), property("extCancel", EnrCancelExtract.requestedCompetition().id())))
                                                                        .buildQuery())

                            ));
        }
        return requestedCompDQL;
    }

    private List<EnrTargetAdmissionPlan> getTaPlans(EnrProgramSetOrgUnit programSetOrgUnit)
    {
        return programSetOrgUnit.getProgramSet().getEnrollmentCampaign().getSettings().isTargetAdmissionCompetition() ?
                getList(EnrTargetAdmissionPlan.class, EnrTargetAdmissionPlan.programSetOrgUnit(), programSetOrgUnit, EnrTargetAdmissionPlan.enrCampaignTAKind().targetAdmissionKind().priority().s())
                : null;
    }

    private String getTableHeader(EnrCompetition competition, EnrTargetAdmissionPlan taPlan, int size)
    {
        StringBuilder header = new StringBuilder();
        header.append(competition.getType().getPrintTitle());
        if (null != taPlan) {
            header.append(" – ").append(taPlan.getEnrCampaignTAKind().getTitle());
        }
        if (!EnrEduLevelRequirementCodes.NO.equals(competition.getEduLevelRequirement().getCode())) {
            header.append(", на базе ").append(competition.getEduLevelRequirement().getShortTitle());
        }
        header.append(" (заявлений – ").append(size);
        if(!EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(competition.getType().getCode()) && !EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(competition.getType().getCode()))
        {
            int plan = 0;
            if (null != taPlan)
            {
                if (EnrEduLevelRequirementCodes.NO.equals(competition.getEduLevelRequirement().getCode()))
                {
                    plan = taPlan.getPlan();
                }
                else if (EnrEduLevelRequirementCodes.SOO.equals(competition.getEduLevelRequirement().getCode()))
                {
                    plan = taPlan.getPlan();
                }
                else if (EnrEduLevelRequirementCodes.PO.equals(competition.getEduLevelRequirement().getCode()))
                {
                    plan = taPlan.getPlan();
                }
            }
            else
                plan = competition.getPlan();

            header.append(", число мест — ").append(plan);
        }
        header.append(")");
        return header.toString();
    }

    private void printContentTable(List<EnrRatingItem> entrantList, RtfDocument document, final String tableHeader,
                                   MutableInt ministerialRequestTotal, MutableInt contractRequestTotal,
                                   RtfTable contentTable,
                                   boolean showResolution)
    {

        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(contentTable);

        final List<String[]> tableContent = new ArrayList<>();
        int number = 1;
        if (entrantList != null) {
            for (EnrRatingItem entrant : entrantList) {
                tableContent.add(printEntrant(number++, entrant, showResolution));

                if (CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    ministerialRequestTotal.increment();
                }
                if (CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(entrant.getCompetition().getType().getCompensationType().getCode())) {
                    contractRequestTotal.increment();
                }
            }
        }

        if(tableContent.isEmpty())
            tableContent.add(new String[1]);

        new RtfTableModifier()
                .put(T1, tableContent.toArray(new String[tableContent.size()][]))
                .put(T1, new RtfRowIntercepterBase()
                {
                    @Override
                    public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
                    {
                        newRowList.get(0).getCellList().get(0).addElements(new RtfString().boldBegin().append(tableHeader).boldEnd().toList());
                    }
                })
                .modify(document);
    }

    private String[] printEntrant(int number,
                                  EnrRatingItem ratingItem,
                                  boolean showResolution)
    {
        EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition();

        List<String> row = new ArrayList<>();
        // номер
        row.add(String.valueOf(number));

        // фио
        row.add(reqComp.getRequest().getEntrant().getFullFio());

        // Личный номер абитуриента
        row.add(reqComp.getRequest().getEntrant().getPersonalNumber());

        //сумма баллов
        row.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ratingItem.getTotalMarkAsDouble()));

        // преимущественное право
        row.add(ratingItem.getRequestedCompetition().getRequest().getBenefitCategory() == null? "" : ratingItem.getRequestedCompetition().getRequest().getBenefitCategory().getShortTitle());

        // Решение комиссии
        if (showResolution)
        {
            switch (reqComp.getState().getCode())
            {
                case EnrEntrantStateCodes.RECOMMENDED :
                case EnrEntrantStateCodes.SELECT_SHOULD_BE_ENROLLED :
                case EnrEntrantStateCodes.IN_ORDER :
                case EnrEntrantStateCodes.ENROLLED : row.add("Зачислить"); break;
                default: row.add("Отказать");
            }
        }

        return row.toArray(new String[row.size()]);
    }

    private void fillHeaderData(RtfDocument document, EnrProgramSetOrgUnit programSetOrgUnit, List<EduProgramProf> programs, boolean hasRecommendedOther, NarfuEnrReportCommissionMeetingProtocolAddUI model)
    {
        Collections.sort(programs, (o1, o2) -> o1.getTitleAndConditionsShort().compareTo(o2.getTitleAndConditionsShort()));

        RtfString builder = new RtfString();
        for (EduProgramProf program : programs) {
            builder.append("направление подготовки: ").append(program.getProgramSubject().getTitleWithCode());
            if (program instanceof EduProgramHigherProf)
            {
                EduProgramSpecialization specialization = ((EduProgramHigherProf) program).getProgramSpecialization();
                if (specialization instanceof EduProgramSpecializationChild)
                    builder.append(", профиль ").append(specialization.getDisplayableTitle());
            }
            builder.append(", ").append(program.getDuration().getTitle()).append(IRtfData.PAR);
        }

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier
                .put("eduSubject", builder)
                .put("year", String.valueOf(model.getEnrollmentCampaign().getEducationYear().getIntValue()))
        ;

        String programForm = getProgramFormString(programSetOrgUnit.getProgramSet().getProgramForm());
        modifier.put("developForm", programForm);

        CompensationType compensationType = (CompensationType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue();
        modifier.put("compensationType", getCompensationTypeString(compensationType));

        OrgUnit  formativeOrgUnit = programSetOrgUnit.getFormativeOrgUnit();
        modifier.put("formativeOrgUnit", formativeOrgUnit.getGenitiveCaseTitle() != null ? formativeOrgUnit.getGenitiveCaseTitle() : formativeOrgUnit.getTitle());
        modifier.modify(document);

    }

    private String getProgramFormString(EduProgramForm programForm)
    {
        switch (programForm.getCode())
        {
            case EduProgramFormCodes.OCHNAYA:
                return "очной";
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                return "очно-заочной";
            case EduProgramFormCodes.ZAOCHNAYA:
                return "заочной";
        }
        return null;
    }

    private String getCompensationTypeString(CompensationType compensationType)
    {
        if (compensationType.isBudget()) return "финансируемые из средств федерального бюджета";
        else return "финансируемые по договору об оказании платных услуг";
    }

//    private RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
//    {
//        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);
//
//        if (removeLabel) {
//            for (RtfRow row : table.getRowList()) {
//                for (RtfCell cell : row.getCellList()) {
//                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null) {
//                        UniRtfUtil.fillCell(cell, "");
//                    }
//                }
//            }
//        }
//
//        document.getElementList().remove(table);
//
//        return table;
//    }

}