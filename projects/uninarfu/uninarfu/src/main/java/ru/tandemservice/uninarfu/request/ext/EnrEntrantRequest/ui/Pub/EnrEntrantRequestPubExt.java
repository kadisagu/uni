/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Pub.EnrEntrantRequestPub;
import ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon.NarfuEntrantRequestActionsPrintListDocs;
import ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.ActionsAddon.NarfuEntrantRequestActionPrint;

/**
 * @author Ekaterina Zvereva
 * @since 11.06.2015
 */
@Configuration
public class EnrEntrantRequestPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantRequestPub _enrEntrantRequestPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantRequestPub.presenterExtPoint())
                .addAction(new NarfuEntrantRequestActionPrint("onClickPrintEntrantRequest"))
                .addAction(new NarfuEntrantRequestActionsPrintListDocs("onClickPrintListAndReceipt"))
                .create();
    }
}
