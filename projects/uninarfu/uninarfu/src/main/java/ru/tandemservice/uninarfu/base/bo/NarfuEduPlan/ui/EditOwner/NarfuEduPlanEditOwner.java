/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.ui.EditOwner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.entity.settings.EppPlanOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 09.02.2016
 */
@Configuration
public class NarfuEduPlanEditOwner extends BusinessComponentManager
{

    public static String EDU_PLAN_OWNER_DS = "eduPlanOwnerDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_PLAN_OWNER_DS, eduPlanOwnerDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduPlanOwnerDSHandler()
    {
        EntityComboDataSourceHandler.IQueryCustomizer customizer = (alias, dql, context, filter) -> {
            dql.where(like(DQLFunctions.upper(property(alias, OrgUnit.title())), value(CoreStringUtils.escapeLike(filter, true))));
            dql.where(in(property(alias), new DQLSelectBuilder()
                    .fromEntity(EppPlanOrgUnit.class, "plo").column(property("plo", EppPlanOrgUnit.orgUnit())).buildQuery()));
            return dql;
        };

        return new EntityComboDataSourceHandler(getName(), OrgUnit.class).customize(customizer);
    }
}