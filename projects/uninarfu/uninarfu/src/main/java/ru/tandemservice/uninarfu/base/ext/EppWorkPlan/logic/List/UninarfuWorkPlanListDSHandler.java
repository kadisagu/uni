/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.EppWorkPlan.logic.List;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.logic.List.EppWorkPlanListDSHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanSecondaryProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 10.03.2016
 */
public class UninarfuWorkPlanListDSHandler extends EppWorkPlanListDSHandler
{
    public UninarfuWorkPlanListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void applyOrgUnitFilter(DQLSelectBuilder builder, OrgUnit orgUnit, String alias)
    {
        if (null != orgUnit) {
            builder.where(or(
                    eq(property(EppWorkPlan.parent().eduPlanVersion().eduPlan().owner().fromAlias(alias)), value(orgUnit)),
                    exists(EppEduPlanVersionSpecializationBlock.class,
                            EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit().s(), orgUnit,
                            EppEduPlanVersionSpecializationBlock.id().s(), property(alias, EppWorkPlan.parent())
                    ),
                    exists(EppEduPlanSecondaryProf.class,
                            EppEduPlanSecondaryProf.ownerOrgUnit().orgUnit().s(), orgUnit,
                            EppEduPlanSecondaryProf.id().s(), property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan().id()))
                    )
            );
        }
    }
}

