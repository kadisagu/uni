/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlanVersion.ui.BlockListEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;

import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockListEdit.EppEduPlanVersionBlockListEditUI;
import ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.NarfuEduPlanManager;

/**
 * @author Ekaterina Zvereva
 * @since 10.04.2015
 */
public class EppEduPlanVersionBlockListEditClickApply extends NamedUIAction
{

    public EppEduPlanVersionBlockListEditClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof EppEduPlanVersionBlockListEditUI)
        {
            EppEduPlanVersionBlockListEditUI addEditUI = (EppEduPlanVersionBlockListEditUI) presenter;

            IUIAddon uiAddon = presenter.getConfig().getAddon(EppEduPlanVersionBlockListEditExt.ADDON_NAME);
            if (uiAddon != null && uiAddon instanceof EppEduPlanVersionBlockListEditExtUI)
            {
                NarfuEduPlanManager.instance().getEduPlanDao()
                        .saveOrUpdateEduPlanToEdLvlHSchoolRelation(addEditUI.getHolder().getId(), ((EppEduPlanVersionBlockListEditExtUI) uiAddon).getLevelsMap());
            }

            addEditUI.onClickApply();
        }
    }
}
