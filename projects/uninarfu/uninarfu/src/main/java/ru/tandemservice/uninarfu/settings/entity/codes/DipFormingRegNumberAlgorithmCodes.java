package ru.tandemservice.uninarfu.settings.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Алгоритм формирования номеров документа об обучении"
 * Имя сущности : dipFormingRegNumberAlgorithm
 * Файл data.xml : uninarfu.data.xml
 */
public interface DipFormingRegNumberAlgorithmCodes
{
    /** Константа кода (code) элемента : Нумерация в рамках года с его указанием (title) */
    String NUMBER_WITHIN_EDU_YEAR = "numberWithinEduYear";

    Set<String> CODES = ImmutableSet.of(NUMBER_WITHIN_EDU_YEAR);
}
