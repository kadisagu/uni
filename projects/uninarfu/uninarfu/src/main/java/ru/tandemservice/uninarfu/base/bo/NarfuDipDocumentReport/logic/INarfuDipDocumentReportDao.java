/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.IssuanceRegBook.NarfuDipDocumentReportIssuanceRegBookUI;

/**
 * @author Andrey Avetisov
 * @since 23.04.2015
 */
public interface INarfuDipDocumentReportDao extends INeedPersistenceSupport
{
    /**
     * Создает отчет «Книга регистрации выданных документов об образовании»
     * @param model модель данных для формирования отчета
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    byte[] createDipDocumentReportIssuanceBookReport(NarfuDipDocumentReportIssuanceRegBookUI model);
}
