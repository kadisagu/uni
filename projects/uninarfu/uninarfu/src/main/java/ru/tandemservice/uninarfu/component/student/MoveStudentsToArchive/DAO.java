package ru.tandemservice.uninarfu.component.student.MoveStudentsToArchive;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.dao.IStudentsToArchiveDAO;
import ru.tandemservice.dao.StudentsToArchiveDAO;
import ru.tandemservice.unibasermc.entity.catalog.StudentStatusExt;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.List;

public class DAO extends UniDao<Model> {
    @Override
    public void prepare(Model model)
    {
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
        model.setStudentStatusModel(new FullCheckSelectModel() {
            @Override
            public ListResult findValues(String s) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(StudentStatusExt.class, "s");
                dql.where(DQLExpressions.eq(DQLExpressions.property(StudentStatusExt.archievable().fromAlias("s")), DQLExpressions.value(true)));
                dql.column(DQLExpressions.property(StudentStatusExt.studentStatus().fromAlias("s")));
                List<StudentStatus> list = getList(dql);
                return new ListResult<>(list);
            }
        });
    }

    @Override
    public void update(Model model)
    {
        IStudentsToArchiveDAO dao = (IStudentsToArchiveDAO) StudentsToArchiveDAO.getInstance();
        if (model.isSystemAction()) {
            for (OrgUnit orgUnit : model.getFormativeOrgUnitList())
                dao.moveStudentsToArchive(orgUnit, model.getStudentStatusList());
        }
        else {
            dao.moveStudentsToArchive(get(OrgUnit.class, model.getOrgUnitId()), model.getStudentStatusList());
        }
    }

}
