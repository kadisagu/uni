package ru.tandemservice.uninarfu.component.reports.VacantBudgetPlaces.AddEnt14;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.ArrayList;
import java.util.List;

public class Model
{
    private ISelectModel enrollmentCampaignModel;
    private ISelectModel studentStatusModel;
    private ISelectModel developFormModel;
    private IMultiSelectModel formativeOrgUnitListModel;

    private DevelopForm developForm;
    private List<OrgUnit> formativeOrgUnitList;
    private List<StudentStatus> studentStatusList;

    private List<CampaignsByCourse> campaignsByCourseList = new ArrayList<>();
    private CampaignsByCourse campaignsByCourse;


    public ISelectModel getEnrollmentCampaignModel()
    {
        return enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel enrollmentCampaignModel)
    {
        this.enrollmentCampaignModel = enrollmentCampaignModel;
    }

    public ISelectModel getStudentStatusModel()
    {
        return studentStatusModel;
    }

    public void setStudentStatusModel(ISelectModel studentStatusModel)
    {
        this.studentStatusModel = studentStatusModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        this.developFormModel = developFormModel;
    }

    public IMultiSelectModel getFormativeOrgUnitListModel()
    {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(IMultiSelectModel formativeOrgUnitListModel)
    {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }


    public DevelopForm getDevelopForm()
    {
        return developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        this.developForm = developForm;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<StudentStatus> getStudentStatusList()
    {
        return studentStatusList;
    }

    public void setStudentStatusList(List<StudentStatus> studentStatusList)
    {
        this.studentStatusList = studentStatusList;
    }

    public List<CampaignsByCourse> getCampaignsByCourseList()
    {
        return campaignsByCourseList;
    }

    public void setCampaignsByCourseList(List<CampaignsByCourse> campaignsByCourses)
    {
        this.campaignsByCourseList = campaignsByCourses;
    }

    public CampaignsByCourse getCampaignsByCourse()
    {
        return campaignsByCourse;
    }

    public void setCampaignsByCourse(CampaignsByCourse course)
    {
        this.campaignsByCourse = course;
    }


    public static class CampaignsByCourse
    {
        private Course course;
        private List<CampaignsWrapper> campaigns = new ArrayList<>();

        public Course getCourse()
        {
            return course;
        }

        public void setCourse(Course course)
        {
            this.course = course;
        }

        public List<CampaignsWrapper> getCampaigns()
        {
            return campaigns;
        }

        public void setCampaigns(List<CampaignsWrapper> campaigns)
        {
            this.campaigns = campaigns;
        }
    }
}