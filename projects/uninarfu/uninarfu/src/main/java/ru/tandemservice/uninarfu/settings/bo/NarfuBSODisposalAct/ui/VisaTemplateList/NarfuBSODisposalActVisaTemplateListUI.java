/* $Id$ */
package ru.tandemservice.uninarfu.settings.bo.NarfuBSODisposalAct.ui.VisaTemplateList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem;
import ru.tandemservice.uninarfu.settings.bo.NarfuBSODisposalAct.ui.VisaTemplateAddEdit.NarfuBSODisposalActVisaTemplateAddEdit;

/**
 * @author Andrey Andreev
 * @since 21.03.2016
 */
public class NarfuBSODisposalActVisaTemplateListUI extends UIPresenter
{
    public void onClickAddVisa()
    {
        getActivationBuilder().asRegion(NarfuBSODisposalActVisaTemplateAddEdit.class).activate();
    }

    public void onClickUp()
    {
        final NarfuBSODisposalActVisaItem entity = getEntityByListenerParameterAsLong();

        CommonManager.instance().commonPriorityDao()
                .doChangePriorityUp(getListenerParameterAsLong(), NarfuBSODisposalActVisaItem.visaTemplate(), entity.getVisaTemplate());
    }

    public void onClickDown()
    {
        final NarfuBSODisposalActVisaItem entity = getEntityByListenerParameterAsLong();

        CommonManager.instance().commonPriorityDao()
                .doChangePriorityDown(getListenerParameterAsLong(), NarfuBSODisposalActVisaItem.visaTemplate(), entity.getVisaTemplate());
    }

    public void onClickEdit()
    {
        final NarfuBSODisposalActVisaItem entity = getEntityByListenerParameterAsLong();

        getActivationBuilder().asRegion(NarfuBSODisposalActVisaTemplateAddEdit.class)
                .parameter("visaTemplateId", entity.getVisaTemplate().getId())
                .activate();
    }

    public void onClickDelete()
    {
        final NarfuBSODisposalActVisaItem entity = getEntityByListenerParameterAsLong();
        DataAccessServices.dao().delete(entity.getVisaTemplate());
    }

}