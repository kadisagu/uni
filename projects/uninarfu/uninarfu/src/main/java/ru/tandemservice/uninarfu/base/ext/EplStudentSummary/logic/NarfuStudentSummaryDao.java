/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.EplStudentSummary.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppEduGroupSplitVariant;
import ru.tandemservice.uniepp.entity.registry.EppEduGroupSplitBaseElement;
import ru.tandemservice.uniepp_load.base.bo.EplStudentSummary.logic.EplStudentSummaryDao;
import ru.tandemservice.uninarfu.entity.NafruEduGroupSplitBySecLangElement;
import ru.tandemservice.uninarfu.entity.catalog.codes.EppEduGroupSplitVariantCodes;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 15.03.2016
 */
public class NarfuStudentSummaryDao extends EplStudentSummaryDao
{
    @Override
    public DQLSelectBuilder getStudent2WorkPlan4SplitBuilder(DQLSelectBuilder dql, String splitAlias, EppEduGroupSplitVariant splitVariant)
    {
        String splitCode = splitVariant.getCode();
        if (splitCode.equals(EppEduGroupSplitVariantCodes.PO_VTOROMU_INOSTRANNOMU_YAZYKU))
        {
            return dql
                    .joinEntity("s", DQLJoinType.inner, PersonForeignLanguage.class, "pfl", eq(property("pfl", PersonForeignLanguage.person()), property("s", Student.person())))
                    .where(in(property("pfl", PersonForeignLanguage.language()),
                              new DQLSelectBuilder()
                                      .fromEntity(PersonForeignLanguageExt.class, "ext")
                                      .joinEntity("ext", DQLJoinType.inner, PersonForeignLanguage.class, "ext_pfl", eq(property("ext", PersonForeignLanguageExt.base()), property("ext_pfl")))
                                      .where(eq(property("ext", PersonForeignLanguageExt.secondary()), value(Boolean.TRUE)))
                                      .where(eq(property("ext_pfl", PersonForeignLanguage.person()), property("s", Student.person())))
                                      .order(property("ext_pfl", PersonForeignLanguage.language().code()))
                                      .column(property("ext_pfl", PersonForeignLanguage.language().id())).top(1)
                                      .buildQuery()
                    ))
                    .group(property("pfl", PersonForeignLanguage.language()))
                    .column(property("pfl", PersonForeignLanguage.language()), splitAlias);
        }
        else
            return super.getStudent2WorkPlan4SplitBuilder(dql, splitAlias, splitVariant);
    }

    @Override
    public EppEduGroupSplitBaseElement saveSplitElement(EppEduGroupSplitVariant splitVariant, IEntity splitValue)
    {
        String splitCode = splitVariant.getCode();

        if (splitCode.equals(EppEduGroupSplitVariantCodes.PO_VTOROMU_INOSTRANNOMU_YAZYKU))
        {
            EppEduGroupSplitBaseElement splitElement = new NafruEduGroupSplitBySecLangElement(splitVariant, (ForeignLanguage) splitValue);
            save(splitElement);
            return splitElement;
        }
        else
            return super.saveSplitElement(splitVariant, splitValue);
    }
}
