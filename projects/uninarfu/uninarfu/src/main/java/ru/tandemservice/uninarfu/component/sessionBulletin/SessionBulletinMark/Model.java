/* $Id$ */
package ru.tandemservice.uninarfu.component.sessionBulletin.SessionBulletinMark;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;

/**
 * @author Ekaterina Zvereva
 * @since 05.05.2015
 */
public class Model extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model
{

    ISelectModel _markSelectModel;
    SessionMarkCatalogItem _massMark;

    public ISelectModel getMarkSelectModel()
    {
        return _markSelectModel;
    }

    public void setMarkSelectModel(ISelectModel markSelectModel)
    {
        _markSelectModel = markSelectModel;
    }

    public SessionMarkCatalogItem getMassMark()
    {
        return _massMark;
    }

    public void setMassMark(SessionMarkCatalogItem massMark)
    {
        _massMark = massMark;
    }

}
