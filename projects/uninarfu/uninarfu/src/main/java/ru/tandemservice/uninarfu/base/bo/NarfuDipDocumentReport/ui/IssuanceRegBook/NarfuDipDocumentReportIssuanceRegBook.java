/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.IssuanceRegBook;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.unidip.base.entity.catalog.DipDocumentType;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 21.04.2015
 */
@Configuration
public class NarfuDipDocumentReportIssuanceRegBook extends BusinessComponentManager
{
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String PROGRAM_SUBJECT_DS = "programSubjectDS";
    public static final String DIP_DOC_TYPE_DS = "dipDocumentTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS).handler(formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(DIP_DOC_TYPE_DS).handler(dipDocumentTypeDSHandler()))
                .addDataSource(selectDS(PROGRAM_SUBJECT_DS).handler(programSubjectDSHandler()).addColumn("titleWithCode", EduProgramSubject.P_TITLE_WITH_CODE_INDEX_AND_GEN))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> dipDocumentTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DipDocumentType.class)
                .order(DipDocumentType.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "p")
                        .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)))
                        .buildQuery()));
            }
        }
                .filter(EduProgramSubject.code())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.code())
                .order(EduProgramKind.title());
    }
}
