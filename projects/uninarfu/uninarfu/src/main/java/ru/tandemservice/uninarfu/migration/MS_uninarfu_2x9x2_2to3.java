package ru.tandemservice.uninarfu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x9x2_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("session_c_script_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("sessionScriptItem");
        }

        final short dipScriptItemCode = tool.entityCodes().ensure("dipScriptItem");
        final Long newId = EntityIDGenerator.generateNewId(dipScriptItemCode);
        tool.executeUpdate("update scriptitem_t set discriminator=?, code_p=?, catalogcode_p=?, id=? where code_p=?",
                dipScriptItemCode, "report.narfuCheckingGraduatesData", "dipScriptItem", newId, "report.checkingGraduatesData");
    }
}