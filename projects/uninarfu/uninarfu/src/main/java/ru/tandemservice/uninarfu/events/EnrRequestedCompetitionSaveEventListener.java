/* $Id$ */
package ru.tandemservice.uninarfu.events;

import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.uninarfu.entrant.bo.NarfuEnrEntrant.NarfuEnrEntrantManager;

/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
public class EnrRequestedCompetitionSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    @Override
    public void onFilteredEvent(HibernateSaveEvent event)
    {
        EnrRequestedCompetition reqComp = (EnrRequestedCompetition) event.getEntity();
        NarfuEnrEntrantManager.instance().dao().updateEntrantAndTheirRequests(reqComp, event.getSession());
    }
}
