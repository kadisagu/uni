/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DipDocument.ui.DiplomaObjectDataAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.InfoCollector;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.DiplomaObjectDataAdd.DipDocumentDiplomaObjectDataAddUI;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocument.NarfuDipDocumentManager;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 28.05.2015
 */
public class DipDocumentOrgUnitStudentTabClickContinueAction extends NamedUIAction
{
    protected DipDocumentOrgUnitStudentTabClickContinueAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if(presenter instanceof DipDocumentDiplomaObjectDataAddUI)
        {
            DipDocumentDiplomaObjectDataAddUI pr = (DipDocumentDiplomaObjectDataAddUI) presenter;
            pr.onClickContinue();

            List<String> orgUnitFullTitles = NarfuDipDocumentManager.instance().dao().getDivisionCodeEmptyOrgUnitFullTitles(pr.getStudentIds());
            if (!orgUnitFullTitles.isEmpty())
            {
                String orgUnitTitles = StringUtils.join(orgUnitFullTitles, ", ");
                InfoCollector info = ContextLocal.getInfoCollector();
                info.add("Для подразделени" + (orgUnitFullTitles.size() > 1 ? "й" : "я") + " (" + orgUnitTitles + ") не созданы факты выдачи, т.к. не задан внутренний код.");
            }
        }
    }
}
