/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.uninarfu.catalog.entity.codes.EnrScriptItemCodes;

/**
 * @author Ekaterina Zvereva
 * @since 24.04.2015
 */
public class NarfuEntrantRequestActionsPrintTutor extends NamedUIAction
{
    protected NarfuEntrantRequestActionsPrintTutor(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        EnrScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.ENTRANT_REQUEST_TUTOR_PROFILE);
        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(scriptItem, presenter.getListenerParameterAsLong());
    }
}