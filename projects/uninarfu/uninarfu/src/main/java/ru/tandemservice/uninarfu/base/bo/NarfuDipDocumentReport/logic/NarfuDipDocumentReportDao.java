/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.unidip.base.entity.catalog.DipScriptItem;
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.IssuanceRegBook.NarfuDipDocumentReportIssuanceRegBookUI;
import ru.tandemservice.uninarfu.base.entity.catalog.codes.DipScriptItemCodes;

import java.util.Map;

/**
 * @author Andrey Avetisov
 * @since 23.04.2015
 */
public class NarfuDipDocumentReportDao extends CommonDAO implements INarfuDipDocumentReportDao
{

    @Override
    public byte[] createDipDocumentReportIssuanceBookReport(NarfuDipDocumentReportIssuanceRegBookUI model)
    {
        IScriptItem scriptItem = getByCode(DipScriptItem.class, DipScriptItemCodes.NARFU_DIP_ISSUANCE_REG_BOOK_REPORT);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);
        return (byte[])scriptResult.get(IScriptExecutor.DOCUMENT);

    }
}
