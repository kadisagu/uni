/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.EppWorkPlan.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.base.bo.EppWorkPlan.ui.List.EppWorkPlanList;
import ru.tandemservice.uninarfu.base.ext.EppWorkPlan.logic.List.UninarfuWorkPlanListDSHandler;

/**
 * @author Denis Katkov
 * @since 10.03.2016
 */
@Configuration
public class EppWorkPlanListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "uninarfu" + EppWorkPlanListExtUI.class.getSimpleName();

    @Autowired
    private EppWorkPlanList _eppWorkPlanList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppWorkPlanList.presenterExtPoint())
                .replaceDataSource(searchListDS(EppWorkPlanList.EPP_WORK_PLAN_LIST_DS, _eppWorkPlanList.workPlanListDSColumns(), uninarfuWorkPlanListDSHandler()))
                .addAddon(uiAddon(ADDON_NAME, EppWorkPlanListExtUI.class))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler uninarfuWorkPlanListDSHandler()
    {
        return new UninarfuWorkPlanListDSHandler(getName());
    }
}