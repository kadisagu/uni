package ru.tandemservice.uninarfu.component.orgUnit.OrgUnitStudentPersonCards;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uninarfu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.IOrgUnitStudentPersonCardPrintFactory;

public class OrgUnitStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IOrgUnitStudentPersonCardPrintFactory {

    @Override
    public void addCard(RtfDocument document) {
        modifyByStudent(document);
    }


}
