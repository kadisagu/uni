package ru.tandemservice.uninarfu.component.student.MoveStudentsToArchive;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

import java.util.List;

@Input(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public class Model {
    private ISelectModel formativeOrgUnitModel;
    private List<OrgUnit> formativeOrgUnitList;
    private ISelectModel studentStatusModel;
    private List<StudentStatus> studentStatusList;
    private boolean systemAction;
    private Long orgUnitId;

    public ISelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList() {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public ISelectModel getStudentStatusModel() {
        return studentStatusModel;
    }

    public void setStudentStatusModel(ISelectModel studentStatusModel) {
        this.studentStatusModel = studentStatusModel;
    }

    public List<StudentStatus> getStudentStatusList() {
        return studentStatusList;
    }

    public void setStudentStatusList(List<StudentStatus> studentStatusList) {
        this.studentStatusList = studentStatusList;
    }

    public boolean isSystemAction() {
        return orgUnitId == null;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }
}
