/* $Id$ */
package ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantDailyListAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCommission.EnrEnrollmentCommissionManager;
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.NarfuEnrReportManager;

/**
 * @author Andrey Avetisov
 * @since 08.05.2015
 */
@Configuration
public class NarfuEnrReportEntrantDailyListAdd extends BusinessComponentManager
{
    public static final String EMPLOYEE_DS = "employeeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(EnrEnrollmentCommissionManager.instance().enrollmentCommissionDSConfig())
                .addDataSource(selectDS(EMPLOYEE_DS, NarfuEnrReportManager.instance().getEmployeeEnrCommissionDSHandler()).addColumn(EmployeePost.P_FULL_TITLE))
                .create();
    }

}
