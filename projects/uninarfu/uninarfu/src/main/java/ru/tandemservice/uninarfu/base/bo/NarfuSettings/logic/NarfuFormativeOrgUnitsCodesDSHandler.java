/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 17.06.2015
 */
public class NarfuFormativeOrgUnitsCodesDSHandler extends DefaultSearchDataSourceHandler
{
    public static final String PROPERTY_CODE = "code";
    public static final String PROPERTY_NOT_EXIST = "notExist";

    public NarfuFormativeOrgUnitsCodesDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
       DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                .column(property("rel", OrgUnitToKindRelation.orgUnit()))
                .joinPath(DQLJoinType.inner, OrgUnitToKindRelation.orgUnitKind().fromAlias("rel"), "kind")
                .where(eq(property("kind", OrgUnitKind.code()), value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING)))
               .order(property("rel", OrgUnitNumberForEnrEntrantDocuments.orgUnit().title()));


        Map<Long, String> settingsMap = getSettingsMap();

        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
        for (DataWrapper wrapper : DataWrapper.wrap(output))
        {
            String code = settingsMap.get(wrapper.getId());
            wrapper.setProperty(PROPERTY_CODE, code);
            wrapper.setProperty(PROPERTY_NOT_EXIST, code == null);
        }

        return output;
    }

    private Map<Long, String> getSettingsMap()
    {
        List<OrgUnitNumberForEnrEntrantDocuments> listRelations = DataAccessServices.dao().getList(OrgUnitNumberForEnrEntrantDocuments.class);
        Map<Long, String> resultMap = new HashMap<>(listRelations.size());
        for(OrgUnitNumberForEnrEntrantDocuments item : listRelations)
        {
            resultMap.put(item.getOrgUnit().getId(), item.getNumber());
        }
        return resultMap;
    }
}
