/* $Id$ */
package ru.tandemservice.uninarfu.component.catalog.relationDegree.RelationDegreePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;

/**
 * @author Ekaterina Zvereva
 * @since 27.04.2015
 */
public class DAO extends DefaultCatalogPubDAO<RelationDegree, Model>
{
}