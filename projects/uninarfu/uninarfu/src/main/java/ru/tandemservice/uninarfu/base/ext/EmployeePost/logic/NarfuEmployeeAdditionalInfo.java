/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.EmployeePost.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.EmployeePost.logic.DefaultEmployeeAdditionalInfo;
import ru.tandemservice.loadrmc.entity.EmployeePostRatePartSource;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 05.10.2016
 */
public class NarfuEmployeeAdditionalInfo extends DefaultEmployeeAdditionalInfo
{
    @Override
    public Integer getRate(Long employeePostId)
    {
        List<EmployeePostRatePartSource> list = DataAccessServices.dao()
                .getList(EmployeePostRatePartSource.class, EmployeePostRatePartSource.employeePost().id().s(), employeePostId);

        double result = 0;
        for (EmployeePostRatePartSource rate : list)
            result += rate.getEmployeeWageRatePart().getValue();

        return convert(result);
    }

    @Override
    public Map<Long, Long> getRateMap(Collection<Long> employeePostIds)
    {
        List<Object[]> list = DataAccessServices.dao().<Object[]>getList(
                new DQLSelectBuilder()
                        .fromEntity(EmployeePostRatePartSource.class, "ext")
                        .column(property("ext", EmployeePostRatePartSource.employeePost().id()))
                        .column(property("ext", EmployeePostRatePartSource.employeeWageRatePart().value()))
                        .where(in(property("ext", EmployeePostRatePartSource.employeePost().id()), employeePostIds))
        );

        return list.stream().collect(Collectors.groupingBy(o -> (Long) o[0], Collectors.mapping(
                o -> (Double) o[1], Collectors.summingLong(value -> convert(value).longValue()))));
    }

    private Integer convert(double value)
    {
        return (int) (value * MULTIPLIER);
    }
}
