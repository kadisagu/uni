/*$Id$*/
package ru.tandemservice.uninarfu.migration;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.log4j.Logger;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import static org.tandemframework.core.view.formatter.DateFormatter.DEFAULT_DATE_FORMATTER;

/**
 * @author DMITRY KNYAZEV
 * @since 08.04.2015
 */
@SuppressWarnings({"all"})
public class MS_uninarfu_2x8x0_1to2 extends IndependentMigrationScript {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.17"),
                        new ScriptDependency("org.tandemframework.shared", "1.8.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.8.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        logger.info("Migration data from personEduInstitutionNARFU started at: " + DEFAULT_DATE_FORMATTER.format(new java.util.Date()));

        {//delete all empty personEduInstitutionNARFU
            final int i = tool.executeUpdate("DELETE FROM personeduinstitutionnarfu_t WHERE ((actualdatereturn_p IS NULL) AND (expecteddatereturn_p IS NULL) AND (dateissuecertificate_p IS NULL))");
            logger.info("Deleted " + i + " empty personEduInstitutionNARFU rows");
        }

        Map<Long, PersonEduInstitutionNarfuWrapper> personEduInstitutionNarfuMap = new HashMap<>(); // key-personEduInstitutionnarfuID
        Map<Long, PersonWrapper> personMap = new HashMap<>();                                       // key-personID
        {//colect all personEduInstitution linked with personEduInstitutionNARFU
            int pei_id = 1;
            int pein_id = 2;
            int person_id = 3;

            int date_issue_certificate = 4;
            int expected_date_return = 5;
            int actual_date_return = 6;

            int last_name = 7;
            int first_name = 8;
            int middle_name = 9;

            Statement statement = tool.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT " +
                    "pei.id, " +
                    "pein.id, " +
                    "pei.person_id, " +

                    "pein.dateissuecertificate_p, " +
                    "pein.expecteddatereturn_p, " +
                    "pein.actualdatereturn_p, " +

                    "ic.lastname_p, " +
                    "ic.firstname_p, " +
                    "ic.middlename_p " +

                    "FROM personeduinstitutionnarfu_t AS pein " +
                    "INNER JOIN personeduinstitution_t AS pei ON pei.id = pein.BASE_ID " +
                    "INNER JOIN person_t AS p ON p.id = pei.person_id " +
                    "INNER JOIN identitycard_t AS ic ON ic.id = p.identitycard_id");

            while (resultSet.next()) {
                final long peiId = resultSet.getLong(pei_id);
                final long peinId = resultSet.getLong(pein_id);
                final long personId = resultSet.getLong(person_id);

                final Date dateIssueCertificate = resultSet.getDate(date_issue_certificate);
                final Date expectedDateReturn = resultSet.getDate(expected_date_return);
                final Date actualDateReturn = resultSet.getDate(actual_date_return);

                final String lastName = resultSet.getString(last_name);
                final String firstName = resultSet.getString(first_name);
                final String middleName = resultSet.getString(middle_name);

                personEduInstitutionNarfuMap.put(peinId, new PersonEduInstitutionNarfuWrapper(peiId, personId, dateIssueCertificate, expectedDateReturn, actualDateReturn));
                personMap.put(personId, new PersonWrapper(lastName, firstName, middleName));
            }
        }

        Set<Long> multipleLinks = new HashSet<>();//personEduInstitutionNarfuID
        Map<Long, Long> eduInstitutionNarfu2EduDocument = new HashMap<>();//key-personEduInstitutionNarfuID value-personEduDocumentId
        {//collect link from personEduInstitutionNARFU to personEduDocument
            Statement stmt = tool.getConnection().createStatement();
            int pein_id = 1;
            int ped_id = 2;
            ResultSet rs = stmt.executeQuery("SELECT" +
                    " pein.id," +
                    " ped.ID" +
                    " FROM personeduinstitutionnarfu_t AS pein" +
                    "  INNER JOIN personeduinstitution_t AS pei ON pei.id = pein.BASE_ID" +
                    "  INNER JOIN person_edu_doc_t AS ped ON (" +
                    "    (pei.person_id = ped.PERSON_ID)" +
                    "    AND ((pei.seria_p IS NULL AND ped.SERIA_P IS NULL) OR (pei.seria_p = ped.SERIA_P))" +
                    "    AND (pei.addressitem_id = ped.EDUORGANIZATIONADDRESSITEM_ID)" +
                    "    AND (pei.yearend_p = ped.YEAREND_P)" +
                    "    AND ((pei.issuancedate_p IS NULL AND ped.ISSUANCEDATE_P IS NULL) OR (pei.issuancedate_p = ped.ISSUANCEDATE_P))" +
                    "    AND ((pei.mark3_p IS NULL AND ped.MARK3_P IS NULL) OR (pei.mark3_p IS NULL AND ped.MARK3_P = 0) OR (pei.mark3_p = ped.MARK3_P))" +
                    "    AND ((pei.mark4_p IS NULL AND ped.MARK4_P IS NULL) OR (pei.mark4_p IS NULL AND ped.MARK4_P = 0) OR (pei.mark4_p = ped.MARK4_P))" +
                    "    AND ((pei.mark5_p IS NULL AND ped.MARK5_P IS NULL) OR (pei.mark5_p IS NULL AND ped.MARK5_P = 0) OR (pei.mark5_p = ped.MARK5_P))" +
                    " )");

            Multimap<Long, Long> helpMultimap = ArrayListMultimap.create();//key-eduDocumentID; value-list of eduInstitutionNARFUId
            while (rs.next()) {
                final Long eduInstitutionNarfuId = rs.getLong(pein_id);
                final Long eduDocumentId = rs.getLong(ped_id);
                eduInstitutionNarfu2EduDocument.put(eduInstitutionNarfuId, eduDocumentId);
                helpMultimap.put(eduDocumentId, eduInstitutionNarfuId);
            }

            for (Long key : helpMultimap.keys()) {
                final Collection<Long> eduInstitutionNarfuIdList = helpMultimap.get(key);
                if (eduInstitutionNarfuIdList.size() > 1)
                    multipleLinks.addAll(eduInstitutionNarfuIdList);
            }
        }

        logger.info("\n\n");

        Set<Long> unprocessedEduInstitutionNarfu = new HashSet<>();
        {//migrate data from personEduInstitutionNARFU to personEduDocumentNARFU
            int id = 1;
            int discriminator = 2;
            int eduDocument_id = 3;
            int dateIssueCertificate_p = 4;
            int expectedDateReturn_p = 5;
            int actualDateReturn_p = 6;

            final PreparedStatement insertEduDocumentStatement = tool.prepareStatement("INSERT INTO personedudocumentnarfu_t (" +
                    "id, " +                    // 1
                    "discriminator, " +         // 2
                    "edudocument_id, " +        // 3
                    "dateissuecertificate_p, " +// 4
                    "expecteddatereturn_p, " +  // 5
                    "actualdatereturn_p " +     // 6
                    ") " +
                    "VALUES (?, ?, ?, ?, ?, ?)");

            final short documentEntityCode = tool.entityCodes().get("personEduDocumentNARFU");

            logger.info("List processed PersonEduInstitutionNARFU:");

            int createdDocuments = 0;
            int counter = 0;
            for (final Long eduInstNarfuId : personEduInstitutionNarfuMap.keySet()) {
                if (multipleLinks.contains(eduInstNarfuId))
                    continue;

                Long eduDocId = eduInstitutionNarfu2EduDocument.get(eduInstNarfuId);
                if (eduDocId == null) {
                    unprocessedEduInstitutionNarfu.add(eduInstNarfuId);
                    continue;
                }

                counter++;
                final PersonEduInstitutionNarfuWrapper personEduInstitutionNarfuWrapper = personEduInstitutionNarfuMap.get(eduInstNarfuId);
                final Long newEduDocumentId = EntityIDGenerator.generateNewId(documentEntityCode);

                insertEduDocumentStatement.setLong(id, newEduDocumentId);
                insertEduDocumentStatement.setShort(discriminator, documentEntityCode);
                insertEduDocumentStatement.setLong(eduDocument_id, eduDocId);

                insertEduDocumentStatement.setDate(dateIssueCertificate_p, personEduInstitutionNarfuWrapper.dateIssueCertificate);
                insertEduDocumentStatement.setDate(expectedDateReturn_p, personEduInstitutionNarfuWrapper.expectedDateReturn);
                insertEduDocumentStatement.setDate(actualDateReturn_p, personEduInstitutionNarfuWrapper.actualDateReturn);

                Long peiId = personEduInstitutionNarfuWrapper.peiId;
                Long personId = personEduInstitutionNarfuWrapper.personId;
                String fullFio = personMap.get(personId).getFullFio();
                logger.info(getCreatedDocLogString(fullFio, personId, newEduDocumentId, eduDocId, peiId));
                createdDocuments++;
                insertEduDocumentStatement.addBatch();

                if (counter > 1000) {
                    insertEduDocumentStatement.executeBatch();
                    counter = 0;
                }
            }
            insertEduDocumentStatement.executeBatch();
            logger.info("Created " + createdDocuments + " PersonEduDocumentNARFU rows");
        }

        logger.info("\n\n");

        {//print all personEduInstitutionNARFU with duplicated links
            logger.warn("List PersonEduInstitutionNARFU with multiple links to PersonEduDocument:");
            for (final Long peinId : multipleLinks) {
                final PersonEduInstitutionNarfuWrapper personEduInstitutionNarfuWrapper = personEduInstitutionNarfuMap.get(peinId);

                final Long pedId = eduInstitutionNarfu2EduDocument.get(peinId);
                final Long peiId = personEduInstitutionNarfuWrapper.peiId;
                final Long personId = personEduInstitutionNarfuWrapper.personId;
                final String fullFio = personMap.get(personId).getFullFio();

                logger.warn(getUnprocessedLogString(fullFio, personId, peinId, pedId, peiId));
            }
            logger.warn("All PersonEduDocumentNARFU rows with multiple links to PersonEduDocument - " + multipleLinks.size());
        }

        logger.warn("\n\n");

        {//print all unprocessed personEduInstitutionNARFU
            logger.warn("List unprocessed PersonEduInstitutionNARFU:");
            for (final Long peinId : unprocessedEduInstitutionNarfu) {
                final PersonEduInstitutionNarfuWrapper personEduInstitutionNarfuWrapper = personEduInstitutionNarfuMap.get(peinId);

                final Long pedId = eduInstitutionNarfu2EduDocument.get(peinId);
                final Long peiId = personEduInstitutionNarfuWrapper.peiId;
                final Long personId = personEduInstitutionNarfuWrapper.personId;
                final String fullFio = personMap.get(personId).getFullFio();

                logger.warn(getUnprocessedLogString(fullFio, personId, peinId, pedId, peiId));
            }
            logger.warn("All unprocessed PersonEduDocumentNARFU rows - " + unprocessedEduInstitutionNarfu.size());
        }
    }

    private String getCreatedDocLogString(String fullFio, Long personId, Long pednId, Long pedId, Long peiId) {
        return String.format("Person:%s id:%d; created PersonEduDocumentNARFU id:%d; to PersonEduDocument id:%d; from PersonEduInstitution id:%d;",
                fullFio, personId, pednId, pedId, peiId);
    }

    private String getUnprocessedLogString(String fullFio, Long personId, Long peinId, Long pedId, Long peiId) {
        String pedToString = "Not found";
        if (pedId != null)
            pedToString = pedId.toString();

        return String.format("Person:%s id:%d; found PersonEduInstitutionNARFU id:%d; to PersonEduDocument id:%s; from PersonEduInstitution id:%d;",
                fullFio, personId, peinId, pedToString, peiId);
    }


    //HELPER CLASSES
    private final class PersonEduInstitutionNarfuWrapper {

        final long peiId;
        final long personId;
        final Date dateIssueCertificate;
        final Date expectedDateReturn;
        final Date actualDateReturn;

        private PersonEduInstitutionNarfuWrapper(long peiId, long personId, Date dateIssueCertificate, Date expectedDateReturn, Date actualDateReturn) {
            this.peiId = peiId;
            this.personId = personId;
            this.dateIssueCertificate = dateIssueCertificate;
            this.expectedDateReturn = expectedDateReturn;
            this.actualDateReturn = actualDateReturn;
        }
    }

    private final class PersonWrapper {

        final String lastName;
        final String firstName;
        final String middleName;

        private PersonWrapper(String lastName, String firstName, String middleName) {
            this.lastName = lastName;
            this.firstName = firstName;
            this.middleName = middleName;
        }

        private String getFullFio() {
            return lastName + " " + firstName + " " + middleName;
        }
    }

}
