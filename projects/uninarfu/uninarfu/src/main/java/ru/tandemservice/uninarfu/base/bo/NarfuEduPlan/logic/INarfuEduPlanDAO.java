/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEduPlan.logic;

import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 10.04.2015
 */
public interface INarfuEduPlanDAO
{
    public void saveOrUpdateEduPlanToEdLvlHSchoolRelation(Long eduPlanVersionId, Map<EduProgramSpecialization, EducationLevelsHighSchool> levelsMap);
}
