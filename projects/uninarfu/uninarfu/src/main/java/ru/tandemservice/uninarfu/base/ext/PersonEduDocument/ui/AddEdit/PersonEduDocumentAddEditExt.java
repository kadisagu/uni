/*$Id$*/
package ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.ui.AddEdit.PersonEduDocumentAddEdit;

/**
 * @author DMITRY KNYAZEV
 * @since 06.04.2015
 */
@Configuration
public class PersonEduDocumentAddEditExt extends BusinessComponentExtensionManager {

    public static final String ADDON = "narfu_" + PersonEduDocumentAddEditUIExt.class.getSimpleName();

    @Autowired
    private PersonEduDocumentAddEdit eduDocumentAddEdit;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(eduDocumentAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON, PersonEduDocumentAddEditUIExt.class))
                .create();
    }
}
