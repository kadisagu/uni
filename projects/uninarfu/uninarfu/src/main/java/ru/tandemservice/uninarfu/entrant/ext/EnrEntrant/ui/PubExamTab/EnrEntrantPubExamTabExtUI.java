/* $Id$ */
package ru.tandemservice.uninarfu.entrant.ext.EnrEntrant.ui.PubExamTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubExamTab.EnrEntrantPubExamTabUI;
import ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 16.04.2015
 */
public class EnrEntrantPubExamTabExtUI extends UIAddon
{
    private String _specialExamConditions;

    private EnrEntrantPubExamTabUI _presenter;

    public String getSpecialExamConditions()
    {
        return _specialExamConditions;
    }

    public void setSpecialExamConditions(String specialExamConditions)
    {
        _specialExamConditions = specialExamConditions;
    }

    public EnrEntrantPubExamTabExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = (EnrEntrantPubExamTabUI)getPresenter();
        List<EntrantSpecConditionRelation> listConditions = DataAccessServices.dao().getList(EntrantSpecConditionRelation.class, EntrantSpecConditionRelation.enrEntrant(), ((EnrEntrantPubExamTabUI) getPresenter()).getEntrant());
        setSpecialExamConditions(CommonBaseStringUtil.join(listConditions, "specCondition.title", ", "));

    }
}