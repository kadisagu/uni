/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.Person.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import org.tandemframework.shared.person.base.bo.Person.logic.PersonDao;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;

/**
 * @author Andrey Avetisov
 * @since 20.10.2015
 */
public class NarfuPersonDAO extends PersonDao implements IPersonDao
{
    @Override
    protected boolean isNeedToRedirectPeronProperty(IEntity entity, Person temlate)
    {
        if (entity instanceof PersonNARFU)
        {
            boolean isEntityExistForTemplate = existsEntity(PersonNARFU.class, PersonNARFU.L_PERSON, temlate);

            if (!isEntityExistForTemplate)
            {
                return true;
            }
            else
            {
                PersonNARFU templatePersonNARFU = get(PersonNARFU.class, PersonNARFU.L_PERSON, temlate);

                IEntityMeta templateMeta = EntityRuntime.getMeta(templatePersonNARFU);
                //заполняем пустые поля PersonNARFU
                for (String prop : templateMeta.getPropertyNames())
                {
                    if ((null == templatePersonNARFU.getProperty(prop) && null != entity.getProperty(prop))
                        || (Boolean.TRUE != templatePersonNARFU.getProperty(prop) && Boolean.TRUE == entity.getProperty(prop)))
                    {
                        templatePersonNARFU.setProperty(prop, entity.getProperty(prop));
                    }

                }
                update(templatePersonNARFU);
                //так как выполненые изменения полей, сущности ссылающейся на персону на базе которой происходит объединение,
                //то редирект PersonNARFU с дубля на базовую персону не нужен.
                return false;
            }


        }
        else
        {
            return super.isNeedToRedirectPeronProperty(entity, temlate);
        }
    }
}
