/* $Id$ */
package ru.tandemservice.uninarfu.component.catalog.relationDegree.RelationDegreePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;

/**
 * @author Ekaterina Zvereva
 * @since 27.04.2015
 */
public interface IDAO extends IDefaultCatalogPubDAO<RelationDegree, Model>
{
}