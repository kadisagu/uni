/* $Id$ */
package ru.tandemservice.uninarfu.eduplan.ext.EppEduPlan.ui.HigherProfAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfAddEdit.EppEduPlanHigherProfAddEditUI;
import ru.tandemservice.uninarfu.entity.NarfuEduPlanToEduLVLHSRelation;

/**
 * @author Ekaterina Zvereva
 * @since 20.04.2015
 */
public class EppEduPlanHigherProfAddEditExtUI extends UIAddon
{
    public EppEduPlanHigherProfAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    private NarfuEduPlanToEduLVLHSRelation relObject;

    private EppEduPlanHigherProfAddEditUI _presenter;

    public NarfuEduPlanToEduLVLHSRelation getRelObject()
    {
        return relObject;
    }

    public void setRelObject(NarfuEduPlanToEduLVLHSRelation relObject)
    {
        this.relObject = relObject;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        _presenter = getPresenter();
        relObject = DataAccessServices.dao().get(NarfuEduPlanToEduLVLHSRelation.class, NarfuEduPlanToEduLVLHSRelation.eduPlan(), _presenter.getElement());
        if (relObject == null)
        {
            relObject = new NarfuEduPlanToEduLVLHSRelation();
            relObject.setEduPlan(_presenter.getElement());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(EppEduPlanHigherProfAddEditExt.EDULVLHS_DS))
        {
            dataSource.put(EppEduPlanHigherProfAddEditExt.PROGRAM_SUBJECT, _presenter.getElement().getProgramSubject());
            dataSource.put(EppEduPlanHigherProfAddEditExt.QUALIFICATION, _presenter.getElement().getProgramQualification());
            dataSource.put(EppEduPlanHigherProfAddEditExt.ORIENTATION, _presenter.getElement().getProgramOrientation());
            dataSource.put(EppEduPlanHigherProfAddEditExt.ORG_UNIT, _presenter.getElement().getOwner());
        }
    }
}
