package ru.tandemservice.uninarfu.entity;

import ru.tandemservice.uninarfu.entity.gen.*;

import java.util.Arrays;
import java.util.Collection;

/** @see ru.tandemservice.uninarfu.entity.gen.RelationDegreeDeclinableGen */
public class RelationDegreeDeclinable extends RelationDegreeDeclinableGen
{
    /**
     * @return список полей, допускающих склонение
     */
    @Override
    public Collection<String> getDeclinableProperties()
    {
        return Arrays.asList(RelationDegreeDeclinable.parent().title().s());
    }

    /**
     * Возвращает наименование объекта
     *
     * @return наименование объекта
     */
    @Override
    public String getTitle()
    {
        return getParent().getTitle();
    }
}