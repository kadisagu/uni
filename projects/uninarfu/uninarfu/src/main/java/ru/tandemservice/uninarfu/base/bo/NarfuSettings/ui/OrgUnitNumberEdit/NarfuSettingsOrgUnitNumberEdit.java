/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings.ui.OrgUnitNumberEdit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
@Configuration
public class NarfuSettingsOrgUnitNumberEdit extends BusinessComponentManager
{
}
