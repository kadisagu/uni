package ru.tandemservice.uninarfu.component.group.StudentPersonCards;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uninarfu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.group.StudentPersonCards.IGrouptStudentPersonCardPrintFactory;

public class GrouptStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IGrouptStudentPersonCardPrintFactory {

    @Override
    public void addCard(RtfDocument document) {
        modifyByStudent(document);

    }

}
