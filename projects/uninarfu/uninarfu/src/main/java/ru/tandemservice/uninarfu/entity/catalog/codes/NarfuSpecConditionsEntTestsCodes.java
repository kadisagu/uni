package ru.tandemservice.uninarfu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Специальные условия для прохождения ВИ"
 * Имя сущности : narfuSpecConditionsEntTests
 * Файл data.xml : uninarfu.data.xml
 */
public interface NarfuSpecConditionsEntTestsCodes
{
    /** Константа кода (code) элемента : для слепых (title) */
    String DLYA_SLEPYH = "specCond_1";
    /** Константа кода (code) элемента : для слабовидящих (title) */
    String DLYA_SLABOVIDYATSHIH = "specCond_2";
    /** Константа кода (code) элемента : для глухих и слабослышащих (title) */
    String DLYA_GLUHIH_I_SLABOSLYSHATSHIH = "specCond_3";
    /** Константа кода (code) элемента : для слепоглухих (title) */
    String DLYA_SLEPOGLUHIH = "specCond_4";
    /** Константа кода (code) элемента : для лиц с тяжелыми нарушениями речи (title) */
    String DLYA_LITS_S_TYAJELYMI_NARUSHENIYAMI_RECHI = "specCond_5";
    /** Константа кода (code) элемента : для лиц с нарушениями двигательных функций верхних конечностей или отсутствием верхних конечностей (title) */
    String DLYA_LITS_S_NARUSHENIYAMI_DVIGATELNYH_FUNKTSIY_VERHNIH_KONECHNOSTEY_ILI_OTSUTSTVIEM_VERHNIH_KONECHNOSTEY = "specCond_6";

    Set<String> CODES = ImmutableSet.of(DLYA_SLEPYH, DLYA_SLABOVIDYATSHIH, DLYA_GLUHIH_I_SLABOSLYSHATSHIH, DLYA_SLEPOGLUHIH, DLYA_LITS_S_TYAJELYMI_NARUSHENIYAMI_RECHI, DLYA_LITS_S_NARUSHENIYAMI_DVIGATELNYH_FUNKTSIY_VERHNIH_KONECHNOSTEY_ILI_OTSUTSTVIEM_VERHNIH_KONECHNOSTEY);
}
