/*$Id$*/
package ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.ui.List;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.*;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.JournalIssuanceCertificatesManager;
import ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.logic.JournalDocumentWrapper;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
public class JournalIssuanceCertificatesListUI extends UIPresenter {

    public void onClickPrintJournal() {
        final IUIDataSource dataSource = getConfig().getDataSource(JournalIssuanceCertificatesList.EDU_DOCUMENT_NARFU_DS);
        final List<JournalDocumentWrapper> records = new ArrayList<>(dataSource.<JournalDocumentWrapper>getRecords());

        try {
            final byte[] content = preparePrintReport(records);
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xml().fileName("JournalIssuanceCertificates.xls").document(content), false);
        } catch (Throwable throwable) {
            throw new ApplicationException("Возникла ошибка при печати.");
        }
    }

    public byte[] preparePrintReport(final List<JournalDocumentWrapper> sources) throws IOException, BiffException, WriteException
    {

        final IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "JournalIssuanceCertificates_narfu");
        final ByteArrayInputStream in = new ByteArrayInputStream(templateDocument.getCurrentTemplate());
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        // создаем печатную форму
        final WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        final WritableWorkbook workbook = Workbook.createWorkbook(out, tempalteWorkbook, ws);

        // шрифты
        final WritableFont times12 = new WritableFont(WritableFont.TIMES, 12);
        final WritableFont times11 = new WritableFont(WritableFont.TIMES, 11);

        // формат ячеек
        final WritableCellFormat rowFormat = new WritableCellFormat(times11);
        rowFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFormat.setAlignment(Alignment.JUSTIFY);
        rowFormat.setWrap(true);

        final WritableCellFormat rowFirstColFormat = new WritableCellFormat(times12);
        rowFirstColFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFirstColFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFirstColFormat.setAlignment(Alignment.CENTRE);
        rowFirstColFormat.setWrap(true);

        final WritableSheet sheet = workbook.getSheet(0);

        //учебный год
        final EducationYear currentEduYear = JournalIssuanceCertificatesManager.instance().getDao().getCurrentEduYear();
        sheet.addCell(new Label(0, 6, currentEduYear.getTitle() + " учебный год", new WritableCellFormat(rowFirstColFormat)));

//        Map<PersonEduInstitutionNARFU, Student> map = getResultMap(model);

        int rows = 8;
        int counter = 0;

        for (JournalDocumentWrapper warpper : sources) {

            final PersonEduDocumentNARFU row = warpper.getEntity();
            final Student student = (Student) warpper.getViewProperty(JournalDocumentWrapper.STUDENT);
            counter++;

            sheet.addCell(new Label(0, rows, String.valueOf(counter), rowFormat));
            sheet.addCell(new Label(1, rows, row.getEduDocument().getPerson().getFullFio(), rowFormat));
            sheet.addCell(new Label(2, rows, student.getEducationOrgUnit().getFormativeOrgUnit().getTitle(), rowFormat));
            sheet.addCell(new Label(3, rows, student.getEducationOrgUnit().getDevelopForm().getTitle(), rowFormat));
            sheet.addCell(new Label(4, rows, student.getCompensationType().isBudget() ? "бюджет" : "по договору", rowFormat));
            sheet.addCell(new Label(5, rows, student.getCourse().getTitle(), rowFormat));
            sheet.addCell(new Label(6, rows, student.getEducationOrgUnit().getEducationLevelHighSchool().getTitle(), rowFormat));
            sheet.addCell(new Label(7, rows, row.getEduDocument().getEduDocumentKind().getTitle(), rowFormat));
            sheet.addCell(new Label(8, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(row.getDateIssueCertificate()), rowFormat));
            sheet.addCell(new Label(9, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(row.getExpectedDateReturn()), rowFormat));

            rows++;
        }

        workbook.write();
        workbook.close();
        return out.toByteArray();
    }
}
