/*$Id$*/
package ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.logic;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Collection;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
public interface IJournalIssuanceCertificatesDao extends IUniBaseDao {

    public Collection<JournalDocumentWrapper> getJournalDocumentWrapperList();

    public EducationYear getCurrentEduYear();
}
