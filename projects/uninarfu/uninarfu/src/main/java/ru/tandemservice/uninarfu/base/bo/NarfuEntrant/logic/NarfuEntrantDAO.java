/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuEntrant.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation;
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 22.04.2015
 */
public class NarfuEntrantDAO extends UniBaseDao implements INarfuEntrantDAO
{
    @Override
    public void deleteSpecConditionForEntrant(EnrEntrant entrant)
    {
        new DQLDeleteBuilder(EntrantSpecConditionRelation.class).where(eq(property(EntrantSpecConditionRelation.enrEntrant()), value(entrant)))
                .createStatement(getSession()).execute();
    }

    @Override
    public void saveOrUpdateSpecConditions(EnrEntrant entrant, List<NarfuSpecConditionsEntTests> listConditions)
    {
        for (NarfuSpecConditionsEntTests item : listConditions)
            DataAccessServices.dao().saveOrUpdate(new EntrantSpecConditionRelation(entrant, item));
    }
}