/* $Id$ */
package ru.tandemservice.uninarfu.base.bo.NarfuSettings.logic;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uninarfu.entity.OrgUnitNumberForEnrEntrantDocuments;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
public class NarfuFormativeOrgUnitDao extends UniBaseDao implements INarfuFormativeOrgUnitDao
{
    @Override
    public void deleteOrgUnitCode(Long orgUnitId)
    {
        new DQLDeleteBuilder(OrgUnitNumberForEnrEntrantDocuments.class)
                .where(eq(property(OrgUnitNumberForEnrEntrantDocuments.orgUnit().id()), value(orgUnitId)))
                .createStatement(getSession()).execute();
    }

}
