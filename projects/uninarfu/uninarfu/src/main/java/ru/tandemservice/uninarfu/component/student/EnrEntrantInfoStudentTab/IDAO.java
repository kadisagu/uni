package ru.tandemservice.uninarfu.component.student.EnrEntrantInfoStudentTab;

import ru.tandemservice.uni.dao.IUniDao;


public interface IDAO extends IUniDao<Model>
{
    void prepareStateExamResultDS(Model model);


    void prepareOlympiadDiplomaDS(Model model);

    void prepareRatingData(Model model);
}
