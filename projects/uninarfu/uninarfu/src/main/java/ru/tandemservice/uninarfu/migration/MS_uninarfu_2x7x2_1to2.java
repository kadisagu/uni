package ru.tandemservice.uninarfu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninarfu_2x7x2_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        /********
         * ЭТО все бесполезно!!
         * Пропустила момент, когда мигрировали id таблицы блоков версий УП...
         */

        /* Данный запрос необходимо выполнить на старой базе 2.5.2 ДО ОБНОВЛЕНИЯ
        /*SELECT verBlok.id, verBlok.educationlevelhighschool_id
            into edu_plan_educationlvlhs_rel
            FROM epp_eduplan_verblock_t verBlok*/

        ////////////////////////////////////////////////////////////////////////////////
        // сущность narfuEduPlanVerBlokToEduLVLHSRelation

        // создана новая сущность
        if (!tool.tableExists("nrfedplnvrblktedlvlhsrltn_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("nrfedplnvrblktedlvlhsrltn_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_7b4ac326"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("eduplanverblock_id", DBType.LONG).setNullable(false),
                                      new DBColumn("educationlevelshighschool_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("narfuEduPlanVerBlokToEduLVLHSRelation");


            //Переносим связь блока версии УП и НПВ из старой базы
            if (tool.tableExists("edu_plan_educationlvlhs_rel"))
            {
                Statement requestStatement = tool.getConnection().createStatement();
                requestStatement.execute("select id, educationlevelhighschool_id from edu_plan_educationlvlhs_rel");

                ResultSet requestResult = requestStatement.getResultSet();

                //Проверяем, что данные все есть
                Statement eduplanIDStatement = tool.getConnection().createStatement();
                eduplanIDStatement.execute("select id from epp_eduplan_verblock_s_t");
                ResultSet eduplanID = eduplanIDStatement.getResultSet();
                Set<Long> eduplanIDSet = new HashSet<>();
                while (eduplanID.next())
                {
                    eduplanIDSet.add(eduplanID.getLong(1));
                }

                Statement edLvlHSStatement = tool.getConnection().createStatement();
                edLvlHSStatement.execute("select id from educationlevelshighschool_t");
                ResultSet edLvlHSID = edLvlHSStatement.getResultSet();
                Set<Long> edLvlHSIDSet = new HashSet<>();
                while (edLvlHSID.next())
                {
                    edLvlHSIDSet.add(edLvlHSID.getLong(1));
                }

                PreparedStatement insertRelSt = tool.getConnection().prepareStatement("insert into nrfedplnvrblktedlvlhsrltn_t (id, discriminator, eduplanverblock_id, educationlevelshighschool_id) values (?, ?, ?, ?)");
                insertRelSt.setShort(2, entityCode);

                while (requestResult.next())
                {
                    if (eduplanIDSet.contains(requestResult.getLong(1)) && edLvlHSIDSet.contains(requestResult.getLong(2)))
                    {
                        insertRelSt.setLong(1, EntityIDGenerator.generateNewId(entityCode));
                        insertRelSt.setLong(3, requestResult.getLong(1));
                        insertRelSt.setLong(4, requestResult.getLong(2));
                        insertRelSt.execute();
                    }
                }
//                tool.dropTable("edu_plan_educationlvlhs_rel");

            }

        }
    }
}
