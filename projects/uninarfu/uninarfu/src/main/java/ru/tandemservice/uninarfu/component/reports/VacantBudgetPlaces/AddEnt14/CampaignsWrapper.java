/* $Id$ */
package ru.tandemservice.uninarfu.component.reports.VacantBudgetPlaces.AddEnt14;

import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

/**
 * @author Andrey Andreev
 * @since 14.10.2015
 */
public class CampaignsWrapper
{
    final EnrEnrollmentCampaign enrCampaign;
    final EnrollmentCampaign oldCampaign;

    public CampaignsWrapper(EnrEnrollmentCampaign enrCampaign)
    {
        this.enrCampaign = enrCampaign;
        this.oldCampaign = null;
    }

    public CampaignsWrapper(EnrollmentCampaign oldCampaign)
    {
        this.enrCampaign = null;
        this.oldCampaign = oldCampaign;
    }

    public Long getId()
    {
        if (enrCampaign != null) return enrCampaign.getId();
        else if (oldCampaign != null) return oldCampaign.getId();
        else return null;
    }

    public String getTitle()
    {
        if (enrCampaign != null) return enrCampaign.getTitle();
        else if (oldCampaign != null) return oldCampaign.getTitle();
        else return null;
    }

    public int getEducationYear()
    {
        if (enrCampaign != null) return enrCampaign.getEducationYear().getIntValue();
        else if (oldCampaign != null) return oldCampaign.getEducationYear().getIntValue();
        else return 0;
    }
}