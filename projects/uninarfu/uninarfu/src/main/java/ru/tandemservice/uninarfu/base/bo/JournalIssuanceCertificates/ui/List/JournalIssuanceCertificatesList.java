/*$Id$*/
package ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.JournalIssuanceCertificatesManager;
import ru.tandemservice.uninarfu.base.bo.JournalIssuanceCertificates.logic.JournalDocumentWrapper;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

import java.util.Collection;

/**
 * @author DMITRY KNYAZEV
 * @since 07.04.2015
 */
@Configuration
public class JournalIssuanceCertificatesList extends BusinessComponentManager {

    public static final String EDU_DOCUMENT_NARFU_DS = "eduDocumentNarfuDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
                .addDataSource(searchListDS(EDU_DOCUMENT_NARFU_DS, eduDocumentDSColumns(), eduDocumentDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduDocumentDSColumns() {
        return columnListExtPointBuilder(EDU_DOCUMENT_NARFU_DS)
                .addColumn(indicatorColumn("student").defaultIndicatorItem(new IndicatorColumn.Item("student")).create())
                .addColumn(textColumn("fio", PersonEduDocumentNARFU.eduDocument().person().fullFio()))

                .addColumn(textColumn("formativeOrgUnit", Student.educationOrgUnit().formativeOrgUnit().title().fromAlias(JournalDocumentWrapper.STUDENT)))
                .addColumn(textColumn("developForm", Student.educationOrgUnit().developForm().title().fromAlias(JournalDocumentWrapper.STUDENT)))
                .addColumn(textColumn("compensationType", Student.compensationType().title().fromAlias(JournalDocumentWrapper.STUDENT)))
                .addColumn(textColumn("course", Student.course().title().fromAlias(JournalDocumentWrapper.STUDENT)))
                .addColumn(textColumn("educationLevelHighSchool", Student.educationOrgUnit().educationLevelHighSchool().title().fromAlias(JournalDocumentWrapper.STUDENT)))

                .addColumn(textColumn("docType", PersonEduDocumentNARFU.eduDocument().eduDocumentKind().title()))
                .addColumn(textColumn("dateIssueCertificate", PersonEduDocumentNARFU.dateIssueCertificate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("expectedDateReturn", PersonEduDocumentNARFU.expectedDateReturn()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduDocumentDSHandler() {
        return new DefaultSearchDataSourceHandler(getName()) {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context) {

                Collection<JournalDocumentWrapper> sourceList = JournalIssuanceCertificatesManager.instance().getDao().getJournalDocumentWrapperList();
                return ListOutputBuilder.get(input, sourceList).pageable(true).build();
            }
        };
    }
}
