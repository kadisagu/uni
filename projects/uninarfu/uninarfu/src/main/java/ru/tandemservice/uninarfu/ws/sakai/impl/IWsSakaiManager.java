package ru.tandemservice.uninarfu.ws.sakai.impl;

import ru.tandemservice.uninarfu.ws.sakai.types.WsOrderWrap;
import ru.tandemservice.uninarfu.ws.sakai.types.WsPaymentWrap;

import java.util.List;

public interface IWsSakaiManager {

    /**
     * Данные проведенных представлений, в которые включен текущий студент
     *
     * @param studentParam
     *
     * @return
     */
    List<WsOrderWrap> getWsOrders(String studentParam);

    WsPaymentWrap getPaymentData(String studentParam);
}
