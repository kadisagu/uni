/* $Id$ */
package ru.tandemservice.uninarfu.entrant.bo.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;
import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.06.2015
 */
public class NarfuEntrantNumberGenerationRule extends SimpleNumberGenerationRule<EnrEntrant>
{

    private String _formativeOrgUnitCode = null;
    private String _programFormCode = null;

    public NarfuEntrantNumberGenerationRule formativeOrgUnitCode(String formativeOrgUnitCode)
    {
        this._formativeOrgUnitCode = formativeOrgUnitCode;
        return this;
    }

    public NarfuEntrantNumberGenerationRule programFormCode(String programFormCode)
    {
        this._programFormCode = programFormCode;
        return this;
    }

    /**
     * @param entrant
     * @return название счетчика (откуда брать следующее значение)
     */
    @Override
    public String getNumberQueueName(EnrEntrant entrant)
    {
        int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();

        StringBuilder builder = new StringBuilder(EnrNumberGenerationRule.PREFIX_ENR14);
        builder.append(EnrEntrant.class.getSimpleName());
        builder.append(null != _formativeOrgUnitCode ? _formativeOrgUnitCode : "??");
        builder.append(String.format("%01d", (year % 10)));
        return builder.toString();
    }

    /**
     * @param entrant
     * @return набор уже используемых номеров из базы
     */
    @Override
    public Set<String> getUsedNumbers(EnrEntrant entrant)
    {
        int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
        StringBuilder builder = new StringBuilder(null != _formativeOrgUnitCode ? _formativeOrgUnitCode : "??");
        builder.append(_programFormCode != null? _programFormCode : "?");
        builder.append(String.format("%01d", (year % 10)));

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrant.class, "e")
                .column(property(EnrEntrant.personalNumber().fromAlias("e")))
                .where(like(property(EnrEntrant.personalNumber().fromAlias("e")), value(builder.toString())));

        return new HashSet<String>(DataAccessServices.dao().<String>getList(dql));
    }

    @Override
    public String buildCandidate(EnrEntrant entrant, int currentQueueValue)
    {
        int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
        StringBuilder builder = new StringBuilder();
        builder.append(null != _formativeOrgUnitCode ? _formativeOrgUnitCode : "??");
        builder.append(_programFormCode != null? _programFormCode : "?");
        builder.append(String.format("%01d", (year % 10)));
        builder.append(String.format("%03d", currentQueueValue));
        return builder.toString();
    }
}
