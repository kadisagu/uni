package ru.tandemservice.uninarfu.base.ext.UniStudent.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.List.UniStudentListUI;

public class UniStudentListUIAddon extends UIAddon {

    private UniStudentListUI parentUI = getPresenter();

    public UniStudentListUIAddon(IUIPresenter presenter, String name,
                                 String componentId)
    {
        super(presenter, name, componentId);
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        super.onBeforeDataSourceFetch(dataSource);

        if (dataSource.getName().equals(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS)) {
            dataSource.put("citizenshipList", parentUI.getSettings().get("citizenshipList"));
            dataSource.put("comment", parentUI.getSettings().get("comment"));
        }
    }

}
