/* $Id$ */
package ru.tandemservice.uninarfu.request.ext.EnrEntrantRequest.ui.AddAppend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppend;

/**
 * @author Ekaterina Zvereva
 * @since 11.06.2015
 */
@Configuration
public class EnrEntrantRequestAddAppendExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "narfuEnrEntrantRequestAddAppend";

    @Autowired
    private EnrEntrantRequestAddAppend entrantRequestAddAppend;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(entrantRequestAddAppend.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantRequestAddAppendExtUI.class))
                .create();
    }
}