/*$Id$*/
package ru.tandemservice.uninarfu.base.bo.narfuEduDocument;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.logic.PersonEduInstitutionDSHandler;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uninarfu.base.ext.PersonEduDocument.logic.IPersonEduDocumentDao;
import ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.List.PersonEduDocumentWrapper;
import ru.tandemservice.uninarfu.base.ext.PersonEduDocument.ui.List.PersonEduInstitutionWrapper;
import ru.tandemservice.uninarfu.entity.PersonEduDocumentNARFU;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 06.04.2015
 */
@Configuration
public class NarfuEduDocumentManager extends BusinessObjectManager
{

    public static NarfuEduDocumentManager instance()
    {
        return instance(NarfuEduDocumentManager.class);
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduInstitutionDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            public DSOutput execute(DSInput input, ExecutionContext context)
            {
                ISecureRoleContext personRoleContext = context.get(PersonEduInstitutionDSHandler.PARAM_PERSON_ROLE_CONTEXT);
                final Person person = context.get(PersonEduInstitutionDSHandler.PARAM_PERSON);
                final List<PersonEduInstitutionWrapper> sourceList = ((IPersonEduDocumentDao) PersonEduDocumentManager.instance().dao()).getPersonEduInstitutionWrapperList(person, personRoleContext);
                input.setCountRecord(Math.max(1, sourceList.size()));
                return ListOutputBuilder.get(input, sourceList).pageable(true).build();
            }
        };
    }

    @Bean
    public IDefaultSearchDataSourceHandler eduDocumentDSHandler()
    {
        return new DefaultSearchDataSourceHandler(getName())
        {
            @Override
            public DSOutput execute(DSInput input, ExecutionContext context)
            {
                ISecureRoleContext personRoleContext = context.get(PersonEduInstitutionDSHandler.PARAM_PERSON_ROLE_CONTEXT);
                final Person person = context.get(PersonEduInstitutionDSHandler.PARAM_PERSON);
                final List<PersonEduDocumentWrapper> sourceList = getPersonEduDocumentWrapperList(person, personRoleContext);
                input.setCountRecord(Math.max(1, sourceList.size()));
                return ListOutputBuilder.get(input, sourceList).pageable(true).build();
            }
        };
    }

    private List<PersonEduDocumentWrapper> getPersonEduDocumentWrapperList(Person person, ISecureRoleContext personRoleContext)
    {
        final String alias = "e";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(PersonEduDocument.class, alias)
                .column(alias)
                .column("n")
                .where(eq(property(PersonEduDocument.person().fromAlias(alias)), value(person)))
                .order(property(PersonEduDocument.yearEnd().fromAlias(alias)), OrderDirection.desc)
                .order(property(PersonEduDocument.issuanceDate().fromAlias(alias)), OrderDirection.desc)
                .joinPath(DQLJoinType.left, PersonEduDocument.eduLevel().fromAlias(alias), "l")
                .order(property(EduLevel.code().fromAlias("l")), OrderDirection.desc)
                .order(property(PersonEduDocument.creationDate().fromAlias(alias)), OrderDirection.desc)
                .joinEntity(alias, DQLJoinType.left, PersonEduDocumentNARFU.class, "n", eq(property(alias, PersonEduDocument.P_ID), property("n", PersonEduDocumentNARFU.L_EDU_DOCUMENT)));

        final List<Object[]> list = DataAccessServices.dao().getList(builder);
        if (list.isEmpty()) return Collections.emptyList();

        List<PersonEduDocumentWrapper> result = new ArrayList<>();
        for (Object[] obj : list)
        {
            final PersonEduDocument eduDocument = (PersonEduDocument) obj[0];
            final PersonEduDocumentNARFU eduDocumentNARFU = (PersonEduDocumentNARFU) obj[1];
            result.add(new PersonEduDocumentWrapper(eduDocument, eduDocumentNARFU, personRoleContext));
        }
        return result;
    }
}
