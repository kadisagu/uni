/* $Id$ */
package ru.tandemservice.uninarfu.base.ext.DiplomaIssuance.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.TapSupportUtils;
import org.tandemframework.tapsupport.confirm.ClickButtonAction;
import org.tandemframework.tapsupport.confirm.ConfirmInfo;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEditUI;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 21.10.2015
 */
public class DiplomaIssuanceAddEditOnClickApply extends NamedUIAction
{
    protected DiplomaIssuanceAddEditOnClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof DiplomaIssuanceAddEditUI) {
            DiplomaIssuanceAddEditUI pr = (DiplomaIssuanceAddEditUI) presenter;

            if (!"ok".equals(presenter.getClientParameter())) {
                String regNumber = pr.getDiplomaIssuance().getRegistrationNumber();

                String alias = "a";
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(DiplomaIssuance.class, alias).column(value(1))
                        .where(eq(property(alias, DiplomaIssuance.registrationNumber()), value(regNumber)))
                        .where(ne(property(alias), value(pr.getDiplomaIssuance())));

                if (IUniBaseDao.instance.get().existsEntity(dql.buildQuery())) {
                    String message = "Регистрационный номер " + regNumber + " уже существует. Сохранить дублирующий номер?";
                    ConfirmInfo confirm = new ConfirmInfo(message, new ClickButtonAction("submit_desktop", "ok", false));
                    TapSupportUtils.displayConfirm(confirm);
                    return;
                }
            }
            pr.onClickApply();
        }
    }
}
