package ru.tandemservice.uninarfu.component.workplan.WorkPlanData;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt;
import ru.tandemservice.uniepp.component.workplan.WorkPlanData.Model;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.ui.WorkPlanDataSourceGenerator;

import java.util.Map;
import java.util.stream.Collectors;

public class DAO extends ru.tandemservice.uniepp.component.workplan.WorkPlanData.DAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        addRecertificationColumn(model.getDisciplineDataSource(), "kind.shortTitle");
        addRecertificationColumn(model.getActionDataSource(), "owner");
    }

    protected void addRecertificationColumn(StaticListDataSource<IEppWorkPlanRowWrapper> dataSource, String insertAfter)
    {
        Map<Long, EppWorkPlanRowExt> mapExt = getList(EppWorkPlanRowExt.class, EppWorkPlanRowExt.row().s(), dataSource.getRowList())
                .stream().collect(Collectors.toMap(ext -> ext.getRow().getId(), ext -> ext));

        BooleanColumn column = new BooleanColumn("Перезачтение", "recertification")
        {
            @Override
            public Item getContent(IEntity ientity)
            {
                IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper) ientity;
                EppWorkPlanRowExt rowExt = mapExt.get(row.getId());

                if (rowExt != null)
                {
                    if (rowExt.isRecertification())
                        return getTrueItem();
                    else
                        return getFalseItem();
                }
                else
                    return null;
            }
        };

        dataSource.addColumn(WorkPlanDataSourceGenerator.vwrap(column), dataSource.getColumn(insertAfter).getNumber() + 1);
    }
}
