package uninarfu.scripts.report

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.entity.EntityComparator
import org.tandemframework.core.entity.EntityOrder
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.MergeType
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfText
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.program.entity.EduProgramProf
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.entity.*
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.OrgUnitPrintTiltleComparator
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantDailyListAdd.NarfuEnrReportEntrantDailyListAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 08.05.2015
 */

return new NarfuEnrEntrantDailyListPrint(                     // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()


class NarfuEnrEntrantDailyListPrint
{
    Session session
    byte[] template
    NarfuEnrReportEntrantDailyListAddUI model
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    private static String HEADER_TABLE = "header"
    private static String ORGUNIT_TABLE = "orgUnit"
    private static String ENTRANTS_TABLE = "T"
    private static String SIGN_TABLE = "delegatePost"

    private static String BUDGET_REQ = "budgetReq"
    private static String EXCLUSIVE_REQ = "exlReq"
    private static String TA_REQ = "taReq"

    def print()
    {
        def document = buildReport(new RtfReader().read(template))
        return [document: RtfUtil.toByteArray(document), fileName: 'NarfuEnrEntrantDailyList.rtf']
    }

    private RtfDocument buildReport(RtfDocument document)
    {
        Map<String, RtfTable> tablesMap = new HashMap<>()
        prepareTemplate(document, tablesMap)

        Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>()
        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class)
        Map<OrgUnit, Map<EnrProgramSetOrgUnit, List<EnrCompetition>>> competitionMap = SafeMap.get(new SafeMap.Callback<OrgUnit, Map<EnrProgramSetOrgUnit, List<EnrCompetition>>>() {
            @Override
            Map<String, List<EnrCompetition>> resolve(OrgUnit key)
            {
                return SafeMap.get(ArrayList.class);
            }
        })

        Map<EnrRequestedCompetition, String> chosenExamFormMap = new HashMap<>()
        prepareReportData(entrantMap, competitionMap, programMap, chosenExamFormMap)
        fillReportTables(document, tablesMap, entrantMap, programMap, competitionMap, chosenExamFormMap)

        return document
    }


    public void fillReportTables(RtfDocument document, Map<String, RtfTable> tablesMap,
                                 Map<EnrCompetition, List<EnrRatingItem>> entrantMap, Map<EnrProgramSetBase, List<EduProgramProf>> programMap,
                                 Map<OrgUnit, Map<EnrProgramSetOrgUnit, List<EnrCompetition>>> competitionMap, Map<EnrRequestedCompetition, String> chosenExamFormMap)
    {
        List<OrgUnit> orgUnitList = new ArrayList<>(competitionMap.keySet())
        orgUnitList.sort(new OrgUnitPrintTiltleComparator())

        for (OrgUnit orgUnit : orgUnitList)
        {
            if (orgUnitList.indexOf(orgUnit) > 0)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            document.getElementList().add(tablesMap.get(ORGUNIT_TABLE).clone);
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            im.put("orgUnit", orgUnit.printTitle.toUpperCase())
            im.put("date", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.dateTo))
            im.modify(document)

            fillProgramSetTables(document, tablesMap.get(ENTRANTS_TABLE), entrantMap, programMap, competitionMap.get(orgUnit), chosenExamFormMap)

            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            fillSecretaryFio(document, tablesMap.get(SIGN_TABLE))


        }
    }

    private void fillProgramSetTables(RtfDocument document, RtfTable table, Map<EnrCompetition, List<EnrRatingItem>> entrantMap,
                                      Map<EnrProgramSetBase, List<EduProgramProf>> programMap,
                                      Map<EnrProgramSetOrgUnit, List<EnrCompetition>> competitionMap, Map<EnrRequestedCompetition, String> chosenExamFormMap)
    {

        if (!CollectionUtils.isEmpty(competitionMap.keySet()))
        {
            List<EnrProgramSetOrgUnit> programSetOrgUnitList = new ArrayList<>(competitionMap.keySet())
            programSetOrgUnitList.removeAll(Collections.singleton(null))
            programSetOrgUnitList.sort(new Comparator<EnrProgramSetOrgUnit>() {
                @Override
                int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2)
                {
                    int result;
                    result = -Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
                    if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
                    if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
                    if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
                    if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
                    return result;
                }
            })

            for (EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnitList)
            {
                List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());
                boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetOrgUnit.getProgramSet() instanceof EnrProgramSetSecondary);

                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
                document.getElementList().add(table.clone)

                im.put("programForm", programSetOrgUnit.programSet.programForm.title)
                im.put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
                im.put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetOrgUnit.getProgramSet().getTitle())
                im.put("budgetCount", Integer.toString(programSetOrgUnit.ministerialPlan))
                im.put("exlCount", Integer.toString(programSetOrgUnit.exclusivePlan))
                im.put("taCount", Integer.toString(programSetOrgUnit.targetAdmPlan))

                List<EnrCompetition> competitionList = new ArrayList<>(competitionMap.get(programSetOrgUnit))

                Map<String, Integer> requestCountMap = new HashMap<>();
                requestCountMap.put(BUDGET_REQ, 0)
                requestCountMap.put(EXCLUSIVE_REQ, 0)
                requestCountMap.put(TA_REQ, 0)

                fillTablesRows(document, competitionList, entrantMap, chosenExamFormMap, requestCountMap)

                im.put(BUDGET_REQ, String.valueOf(requestCountMap.get(BUDGET_REQ)))
                im.put(EXCLUSIVE_REQ, String.valueOf(requestCountMap.get(EXCLUSIVE_REQ)))
                im.put(TA_REQ, String.valueOf(requestCountMap.get(TA_REQ)))

                im.modify(document)
            }
        }
    }

    private void fillTablesRows(RtfDocument document, List<EnrCompetition> competitionList, Map<EnrCompetition, List<EnrRatingItem>> entrantMap,
                                Map<EnrRequestedCompetition, String> chosenExamFormMap, final Map<String, Integer> requestCountMap)
    {
        if (competitionList == null) return
        competitionList.removeAll(Collections.singleton(null))
        Collections.sort(competitionList, new EntityComparator<EnrCompetition>(new EntityOrder(EnrCompetition.type().code().s())))
        int entrantNumber = 1
        boolean breakEntrantCount = false;
        List<String[]> tableContent = new ArrayList<>();
        Set<Integer> compHeaderIdx = new HashSet<>()
        int headerIdx = 0
        for (EnrCompetition competition : competitionList)
        {
            if (!breakEntrantCount
                    && (competition.compTypeCode.equals(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT)
                    || competition.compTypeCode.equals(EnrCompetitionTypeCodes.CONTRACT)))
            {
                breakEntrantCount = true;
                entrantNumber = 1;
            }
            tableContent.add([getFormattedCompetitionType(competition), "", "", "", ""] as String[])
            compHeaderIdx.add(headerIdx)
            headerIdx++
            if (entrantMap.get(competition) == null)
            {
                continue
            }
            List<EnrRatingItem> ratingItemList = new ArrayList<>(entrantMap.get(competition))
            Collections.sort(ratingItemList, new Comparator<EnrRatingItem>() {
                @Override int compare(EnrRatingItem o1, EnrRatingItem o2)
                {
                    return Person.FULL_FIO_AND_ID_COMPARATOR.compare(o1.entrant.person, o2.entrant.person)
                }
            })

            for (EnrRatingItem ratingItem : ratingItemList)
            {
                tableContent.add(getEntrantRow(ratingItem, entrantNumber, chosenExamFormMap, model.isAddBenefitsToComment()) as String[])
                entrantNumber++
                headerIdx++
                if (ratingItem.competition.type.compensationType.budget)
                {
                    int budgetCount = requestCountMap.get(BUDGET_REQ)+1
                    requestCountMap.put(BUDGET_REQ, budgetCount)
                }
                if (ratingItem.competition.exclusive)
                {
                    int exclusiveCount = requestCountMap.get(EXCLUSIVE_REQ)+1
                    requestCountMap.put(EXCLUSIVE_REQ, exclusiveCount)
                }
                if (ratingItem.competition.targetAdmission)
                {
                    int taCount = requestCountMap.get(TA_REQ)+1
                    requestCountMap.put(TA_REQ, taCount)
                }
            }
        }
        tm.put("T", tableContent as String[][])
                .put("T", getBaseIntercepter(compHeaderIdx))
        tm.modify(document)
    }

    private static List<String> getEntrantRow(EnrRatingItem ratingItem, int entrantNumber, Map<EnrRequestedCompetition, String> chosenExamFormMap, boolean isAddBenefitsToComment)
    {
        List<String> row = new ArrayList<>()
        EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition()
        row.add(String.valueOf(entrantNumber))
        row.add(ratingItem.entrant.personalNumber)
        row.add(ratingItem.entrant.fullFio)
        row.add(chosenExamFormMap.get(reqComp))
        row.add(getComment(ratingItem, isAddBenefitsToComment))
        return row
    }


    private void fillSecretaryFio(RtfDocument document, RtfTable table)
    {
        document.getElementList().add(table.clone);
        String postTitle = ""
        if (model.secretary)
        {
            postTitle = StringUtils.capitalize(model.secretary.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
            postTitle += StringUtils.uncapitalize(model.secretary.orgUnit.accusativeCaseTitle? model.secretary.orgUnit.accusativeCaseTitle : model.secretary.orgUnit.title)
        }
        im.put("delegatePost", postTitle)
        im.put("delegateFio", model.secretary ? model.secretary.fio : "")

        im.modify(document)
    }


    /**
     * Подготавливает массивы данных для заполнения отчета
     * @param entrantMap список абитуриентво по конкурсам
     * @param competitionMap список конкурсов, разбитый по формирующим подраздления и наборам ОП
     * @param programMap список опразовательных программ по наборам ОП
     * @param chosenExamForm список выбранных форм сдачи
     */
    private void prepareReportData(Map<EnrCompetition, List<EnrRatingItem>> entrantMap, Map<OrgUnit, Map<EnrProgramSetOrgUnit, List<EnrCompetition>>> competitionMap,
                                   Map<EnrProgramSetBase, List<EduProgramProf>> programMap, Map<EnrRequestedCompetition, String> chosenExamForm)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, true)
        Set<OrgUnit> nonEmptyOrgUnit = new HashSet<>()
        Set<EnrProgramSetOrgUnit> nonEmptyProgramSetOrgUnit = new HashSet<>()
        Set<EnrCompetition> nonEmptyCompetition = new HashSet<>()
        List<EnrRatingItem> ratingItemList = requestedCompDQL.createStatement(getSession()).<EnrRatingItem> list()
        for (EnrRatingItem ratingItem : ratingItemList)
        {
            EnrCompetition competition = ratingItem.competition
            EnrProgramSetOrgUnit programSetOrgUnit = competition.programSetOrgUnit
            OrgUnit orgUnit = programSetOrgUnit.formativeOrgUnit
            nonEmptyCompetition.add(competition)
            nonEmptyProgramSetOrgUnit.add(programSetOrgUnit)
            nonEmptyOrgUnit.add(orgUnit)
            SafeMap.safeGet(entrantMap, competition, ArrayList.class).add(ratingItem)
        }

        Map<EnrRequestedCompetition, String> examFormMap = getChosenExamForm(ratingItemList)
        for (EnrRequestedCompetition requestedCompetition : examFormMap.keySet())
        {
            chosenExamForm.put(requestedCompetition, examFormMap.get(requestedCompetition))
        }

        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов
        Collection<EnrCompetition> competitions = model.competitionFilterAddon.getFilteredList()
        Set<EnrProgramSetBase> programSet = new HashSet<>()
        for (EnrCompetition competition : competitions)
        {
            programSet.add(competition.programSetOrgUnit.programSet)
            if ((model.skipEmptyOrgUnit && !nonEmptyOrgUnit.contains(competition.programSetOrgUnit.formativeOrgUnit)))
            {
                continue
            }
            else if ((model.skipEmptyList && !nonEmptyProgramSetOrgUnit.contains(competition.programSetOrgUnit)))
            {
                competitionMap.get(competition.programSetOrgUnit.formativeOrgUnit).put(null, null)
            }
            else if ((model.skipEmptyCompetition && !nonEmptyCompetition.contains(competition)))
            {
                competitionMap.get(competition.programSetOrgUnit.formativeOrgUnit).get(competition.programSetOrgUnit).add(null)
            }
            else
            {
                competitionMap.get(competition.programSetOrgUnit.formativeOrgUnit).get(competition.programSetOrgUnit).add(competition)
            }
        }

        //подгрузим образовательные программы наборов
        for (EnrProgramSetItem item : DataAccessServices.dao().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSet))
        {
            programMap.get(item.getProgramSet()).add(item.getProgram())
        }
        for (EnrProgramSetBase programSetBase : programSet)
        {
            if (programSetBase instanceof EnrProgramSetSecondary)
            {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary) programSetBase).getProgram())
            }
        }
    }
    /**
     * копирует из шаблона пустые таблицы
     * чистит шаблон
     * вставляет заголовок и отступы после него
     * @param document шаблон
     * @param tablesMap ассоциативный массив, в которые копируются таблицы
     */
    private static void prepareTemplate(RtfDocument document, Map<String, RtfTable> tablesMap)
    {
        tablesMap.put(HEADER_TABLE, copyTable(document, HEADER_TABLE, true))
        tablesMap.put(ORGUNIT_TABLE, copyTable(document, ORGUNIT_TABLE, false))
        tablesMap.put(ENTRANTS_TABLE, copyTable(document, ENTRANTS_TABLE, false))
        tablesMap.put(SIGN_TABLE, copyTable(document, SIGN_TABLE, false))

        // и чистим шаблон, чтобы не было ненужных переводов строк. Предварительно копируем таблицы
        document.getElementList().clear()

        //вставляем заголовок и отступы
        document.getElementList().add(tablesMap.get(HEADER_TABLE).clone)
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
    }


    private static String getFormattedCompetitionType(EnrCompetition competition)
    {
        String formattedCompetitionType = ""
        switch (competition.type.code)
        {
            case EnrCompetitionTypeCodes.EXCLUSIVE: formattedCompetitionType = "Абитуриенты, имеющие право на прием в пределах квоты приема лиц, имеющих особое право"; break
            case EnrCompetitionTypeCodes.MINISTERIAL: formattedCompetitionType = "Абитуриенты, участвующие в конкурсе на общих основаниях"; break
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL: formattedCompetitionType = "Абитуриенты, имеющие право на прием без вступительных испытаний"; break
            case EnrCompetitionTypeCodes.TARGET_ADMISSION: formattedCompetitionType = "Абитуриенты, имеющие право на прием в рамках целевого приема"; break
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT: formattedCompetitionType = "Абитуриенты, участвующие в конкурсе на места с оплатой стоимости обучения (без вступительных испытаний)"; break
            case EnrCompetitionTypeCodes.CONTRACT: formattedCompetitionType = "Абитуриенты, участвующие в конкурсе на места с оплатой стоимости обучения"; break
        }
        return formattedCompetitionType
    }

    private static RtfTable copyTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);
        if (removeLabel)
        {
            for (RtfRow row : table.getRowList())
            {
                for (RtfCell cell : row.getCellList())
                {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null)
                    {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }
        document.getElementList().remove(table);
        return table;
    }


    private Map<EnrRequestedCompetition, String> getChosenExamForm(List<EnrRatingItem> ratingItemList)
    {
        Map<EnrRequestedCompetition, String> chosenExamFormForReqComp = new HashMap<>();
        Set<Long> requestedCompetitionSet = new HashSet<>()

        for (EnrRatingItem ratingItem : ratingItemList)
        {
            requestedCompetitionSet.add(ratingItem.requestedCompetition.id)
        }

        List<EnrChosenEntranceExamForm> chosenEntranceExamFormList = new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExamForm.class, "c").column("c")
                .where(DQLExpressions.in(property("c", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()), requestedCompetitionSet))
                .createStatement(getSession()).list()

        Map<EnrRequestedCompetition, Set<String>> chosenExamForm = new HashMap<>()
        for (EnrChosenEntranceExamForm chosenEntranceExamForm : chosenEntranceExamFormList)
        {
            if (chosenEntranceExamForm.passForm.internal)
            {
                SafeMap.safeGet(chosenExamForm, chosenEntranceExamForm.chosenEntranceExam.requestedCompetition, HashSet.class).add("внутренние испытания")
            }
            else
            {
                SafeMap.safeGet(chosenExamForm, chosenEntranceExamForm.chosenEntranceExam.requestedCompetition, HashSet.class).add(chosenEntranceExamForm.passForm.title)
            }
        }

        for (EnrRequestedCompetition requestedCompetition : chosenExamForm.keySet())
        {
            String forms = StringUtils.join(chosenExamForm.get(requestedCompetition), ",")
            chosenExamFormForReqComp.put(requestedCompetition, forms)
        }

        return chosenExamFormForReqComp
    }

    private static EnrEntrantDQLSelectBuilder prepareEntrantDQL(NarfuEnrReportEntrantDailyListAddUI model, boolean fetch)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(model.dateFrom, model.dateTo)
                .filter(model.enrollmentCampaign)

        if (model.isIncludeTookAwayDocuments())
        {
            requestedCompDQL.where(ne(property(EnrRequestedCompetition.state().code().fromAlias(requestedCompDQL.reqComp())), value(EnrEntrantStateCodes.OUT_OF_COMPETITION)));
        }
        else
        {
            requestedCompDQL.defaultActiveFilter()
        }

        //Отборочные комиссии
        if (model.isEnrollmentCommissionActive())
            requestedCompDQL.where(DQLExpressions.in(property(EnrRequestedCompetition.request().enrollmentCommission().fromAlias(requestedCompDQL.reqComp())), model.enrollmentCommissionList))

        model.getCompetitionFilterAddon().applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelOnly())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)))
        }
        if (model.isSkipParallel())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)))
        }
        return requestedCompDQL;
    }

    private static RtfRowIntercepterBase getBaseIntercepter(Set<Integer> compHeaderIdx)
    {
        return new RtfRowIntercepterBase() {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (compHeaderIdx.contains(rowIndex))
                {
                    cell.setMergeType(colIndex == 0 ? MergeType.HORIZONTAL_MERGED_FIRST : MergeType.HORIZONTAL_MERGED_NEXT)

                    return new RtfString().boldBegin().append(value).boldEnd().toList()
                }

                List<IRtfElement> list = new ArrayList<>();
                IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                text.setRaw(true);
                list.add(text);
                return list;
            }
        }
    }

    private static String getComment(EnrRatingItem ratingItem, boolean addBenefits)
    {
        EnrRequestedCompetition reqComp = ratingItem.requestedCompetition
        StringBuilder info = new StringBuilder();

        if (!StringUtils.isEmpty(reqComp.request.entrant.additionalInfo))
        {
            info.append(reqComp.request.entrant.additionalInfo)
        }
        if (reqComp instanceof EnrRequestedCompetitionNoExams)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Выбранная ОП: ").append(((EnrRequestedCompetitionNoExams) reqComp).programSetItem.program.shortTitle);
        }
        if (addBenefits && reqComp.getCompetition().getType().getCode().equals(EnrCompetitionTypeCodes.EXCLUSIVE) && reqComp instanceof IEnrEntrantBenefitStatement)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Особое право: ").append(((IEnrEntrantBenefitStatement) reqComp).benefitCategory.shortTitle);
        }
        if (reqComp.getRequest().getBenefitCategory() != null)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Преимущественное право: ").append(reqComp.request.benefitCategory.shortTitle);
        }
        // инд. достижения
        if (ratingItem.achievementMarkAsLong > 0)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Индивид. достижения - ").append(ratingItem.achievementMarkAsString);
        }
        return info.toString()
    }
}