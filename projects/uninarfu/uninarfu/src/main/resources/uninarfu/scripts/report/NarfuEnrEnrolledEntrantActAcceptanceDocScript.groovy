/* $Id$ */
package uninarfu.scripts.report

import com.google.common.collect.ComparisonChain
import com.google.common.collect.Multimap
import com.google.common.collect.TreeMultimap
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.fias.base.entity.AddressCountry
import org.tandemframework.shared.fias.base.entity.ICitizenship
import org.tandemframework.shared.fias.base.entity.NoCitizenship
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.PersonBenefit
import org.tandemframework.shared.person.catalog.entity.EduDocumentKind
import org.tandemframework.shared.person.catalog.entity.codes.EduDocumentKindCodes
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt

import java.beans.Introspector

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Andreev
 * @since 07.08.2015
 */
return new NarfuEnrEnrolledEntrantActAcceptanceDocPrint(
        session: session,
        template: template,                               // шаблон
        date: date,
        number: number,
        enrollmentCampaignId: enrollmentCampaignId,
        enrOrderIds: enrOrderIds,
        enrollmentCommissionIds: enrollmentCommissionIds,
        enrRequestTypeCode: enrRequestTypeCode,
        enrOrgUnitIds: enrOrgUnitIds,
        formOrgUnitIds: formOrgUnitIds
).print()

class NarfuEnrEnrolledEntrantActAcceptanceDocPrint {
    // входные параметры
    Session session;
    byte[] template;
    Date date;
    String number;
    Long enrollmentCampaignId;
    List<Long> enrOrderIds;
    List<Long> enrollmentCommissionIds;
    String enrRequestTypeCode;
    List<Long> enrOrgUnitIds;
    List<Long> formOrgUnitIds;


    def print() {
        List<Answer> list = selectData();

        if (list.isEmpty())
            return [document: null, fileName: null]

        RtfDocument mainDocument = printData(prepareData(list));

        String number = number != null ? "№" + (number as String) : "";
        String date = date != null ? " от " + date.format("yyyy.MM.dd") : "";

        return [document: RtfUtil.toByteArray(mainDocument),
                fileName: "Акт передачи зачисленных абитуриентов " + date + " " + number + ".rtf"]
    }

    def List<Answer> selectData() {
        DQLSelectBuilder dql = new DQLSelectBuilder();

        //Extract
        String eee_alias = "eee";
        dql.fromEntity(EnrEnrollmentExtract.class, eee_alias);
        dql.where(eq(
                property(eee_alias, EnrEnrollmentExtract.entity().request().entrant().enrollmentCampaign().id()),
                value(enrollmentCampaignId)
        ));

        //Статус абитуриента
        dql.where(or(
                eq(
                        property(eee_alias, EnrEnrollmentExtract.entity().request().entrant().state().code()),
                        value(EnrEntrantStateCodes.IN_ORDER)
                ),
                eq(
                        property(eee_alias, EnrEnrollmentExtract.entity().request().entrant().state().code()),
                        value(EnrEntrantStateCodes.ENROLLED)
                )
        ));

        FilterUtils.applySelectFilter(dql, eee_alias,
                EnrEnrollmentExtract.entity().request().enrollmentCommission().id(), enrollmentCommissionIds);

        //Order
        dql.joinPath(DQLJoinType.inner, EnrEnrollmentExtract.paragraph().order().fromAlias(eee_alias), "absorder")
        .where(instanceOf("absorder", EnrOrder.class));

        String order_alias = "order";
        dql.fromEntity(EnrOrder.class, order_alias);
        dql.where(and(
                eq(
                        property(eee_alias, EnrEnrollmentExtract.paragraph().order().id()),
                        property(order_alias, EnrOrder.id())
                ),
                eq(
                        property(order_alias, EnrOrder.type().code()),
                        value(EnrOrderTypeCodes.ENROLLMENT)
                )
        ));

        FilterUtils.applySelectFilter(dql, order_alias, EnrOrder.id(), enrOrderIds);
        FilterUtils.applySelectFilter(dql, order_alias, EnrOrder.requestType().code(), enrRequestTypeCode);

        //Paragraph
        dql.joinPath(DQLJoinType.inner, EnrEnrollmentExtract.paragraph().fromAlias(eee_alias), "abspar")
                .where(instanceOf("abspar", EnrEnrollmentParagraph.class));

        String par_alias = "par";
        dql.fromEntity(EnrEnrollmentParagraph.class, par_alias);
        dql.where(eq(
                property(eee_alias, EnrEnrollmentExtract.paragraph().id()),
                property(par_alias, EnrEnrollmentParagraph.id())
        ));

        FilterUtils.applySelectFilter(dql, par_alias, EnrEnrollmentParagraph.enrOrgUnit().institutionOrgUnit().orgUnit().id(), enrOrgUnitIds);
        FilterUtils.applySelectFilter(dql, par_alias, EnrEnrollmentParagraph.formativeOrgUnit().id(), formOrgUnitIds);

        //0 отборочная коммисия
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().enrollmentCommission().title()));

        //1 формирующее подразделение
        dql.column(property(par_alias, EnrEnrollmentParagraph.formativeOrgUnit()));
        //2 Форма обучения
        dql.column(property(par_alias, EnrEnrollmentParagraph.programForm().title()));
        //3 Направление подготовки
        dql.column(property(par_alias, EnrEnrollmentParagraph.programSubject()));

        //4-6 Приказ
        dql.column(property(order_alias, EnrOrder.id()));
        dql.column(property(order_alias, EnrOrder.createDate()));
        dql.column(property(order_alias, EnrOrder.number()));
        //7 Параграф приказа
        dql.column(property(par_alias, EnrEnrollmentParagraph.number()));

        //8-12 Секретарь
        String erce_alias = "erce";
        dql.fromEntity(EnrEnrollmentCommissionExt.class, erce_alias);
        dql.where(eq(
                property(eee_alias, EnrEnrollmentExtract.entity().request().enrollmentCommission()),
                property(erce_alias, EnrEnrollmentCommissionExt.enrEnrollmentCommission())
        ));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().postRelation().postBoundedWithQGandQL().post().title()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().orgUnit().title()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().employee().person().identityCard().firstName()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().person().identityCard().middleName()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().person().identityCard().lastName()));

        //13, 14 Абитуриент
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().entrant().person().id()));
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fullFio()));

        //15-18 Документ об образовании
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().eduInstDocOriginalRef().eduDocument().eduDocumentKind()));
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().eduInstDocOriginalRef().eduDocument().documentKindDetailed()));
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().eduInstDocOriginalRef().eduDocument().seria()));
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().eduInstDocOriginalRef().eduDocument().number()));
        //19 Вид возмещения затрат
        dql.column(property(order_alias, EnrOrder.compensationType().code()));
        //20 Гражданство
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().identityCard().citizenship()));

        //21 Комментарий Заявление
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().request().comment()));
        //22 Вид приема
        dql.column(property(eee_alias, EnrEnrollmentExtract.entity().competition().type()));


        List<Answer> answerList = new ArrayList<>();
        for (Object[] row : dql.createStatement(session).<Object[]> list())
            answerList.add(new Answer(row));

        return answerList;
    }

    def Multimap<Org, Entrant> prepareData(List<Answer> list) {
        Multimap<Org, Entrant> map = TreeMultimap.create();
        for (Answer answer : list)
            map.put(new Org(answer), new Entrant(answer));

        return map;
    }

    def RtfDocument printData(Multimap<Org, Entrant> map) {
        RtfDocument mainDocument = RtfDocument.fromScratch();
        RtfDocument temp = RtfDocument.fromByteArray(template);
        mainDocument.setHeader(temp.getHeader());
        mainDocument.setSettings(temp.getSettings());

        String n = number != null ? "№" + number : "";
        String d = date != null ? DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(date) + " года" : "";
        String dateNnumber = d + " " + n;

        for (Org org : map.keySet())
            for (IRtfElement element : printAct(org, map.get(org), dateNnumber))
                mainDocument.addElement(element);

        return mainDocument;
    }

    def List<IRtfElement> printAct(Org org, Collection<Entrant> persons, String dateNnumber) {
        RtfDocument document = RtfDocument.fromByteArray(template);

        new RtfInjectModifier()
                .put("date_n_number", dateNnumber)
                .put("enrollmentCommission", org.getCommission())
                .put("decEnrollmentCommission", Introspector.decapitalize(org.getCommission()))
                .put("formUnit", org.getFormUnit().getTitle())
                .put("decFormUnit", Introspector.decapitalize(org.getFormUnit().getGenitiveCaseTitle()))
                .put("eduForm", org.getProgramForm())
                .put("eduDir", org.getProgramSubject())
                .put("secretaryPost", Introspector.decapitalize(org.getSecretaryPost() + " " + org.getSecretaryOU()))
                .put("secretaryFIO", org.getSecretaryFIO())
                .modify(document);


        int iBudget = 1;
        int iContract = 1;
        List<String[]> budget = new ArrayList<>();
        List<String[]> contract = new ArrayList<>();
        for (Entrant entrant : persons) {
            if (entrant.isCompensationType(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET)) {
                budget.add(entrant.getRow(iBudget++));
            } else if (entrant.isCompensationType(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)) {
                contract.add(entrant.getRow(iContract++));
            }
        }

        RtfTableModifier rtfTableModifier = new RtfTableModifier();
        if (budget.size() != 0) {
            rtfTableModifier.put("Tbud", budget.toArray() as String[][]);
        } else {
            rtfTableModifier.remove("Tbud", 1, 1);
        }
        if (contract.size() != 0) {
            rtfTableModifier.put("Tcon", contract.toArray() as String[][]);
        } else {
            rtfTableModifier.remove("Tcon", 1, 0);
        }
        rtfTableModifier.modify(document);

        return document.getElementList();
    }


    class Answer {
        String commission;
        OrgUnit formUnit;

        String programForm;
        String programSubject;

        Long orderID;
        String orderTitle;

        String secretaryFIO;
        String secretaryPost;
        String secretaryOU;

        Long entrantID;
        String entrantFIO;
        String entrantDoc;

        String compensationType;
        ICitizenship citizenship;

        String comment;
        EnrCompetitionType competitionType;


        Answer(Object[] row) {
            commission = (String) row[0];
            formUnit = (OrgUnit) row[1];

            programForm = (String) row[2];
            programSubject = ((EduProgramSubject) row[3]).getTitleWithCode();

            orderID = (Long) row[4];
            orderTitle = ((Date) row[5]).format("dd.MM.yyyy") + " №" + (String) row[6] + "\u00a0п." + (row[7] as String);

            secretaryFIO = ((String) row[10])[0] + "." + ((String) row[11])[0] + ".\u00a0" + (String) row[12];
            secretaryPost = (String) row[8];
            secretaryOU = (String) row[9];

            entrantID = (Long) row[13];
            entrantFIO = (String) row[14];
            entrantDoc = makeEduDoc((EduDocumentKind) row[15], (String) row[16], (String) row[17], (String) row[18]);
            compensationType = (String) row[19];
            citizenship = (ICitizenship) row[20];

            comment = (String) row[21];
            competitionType = (EnrCompetitionType) row[22];
        }
    }

    class Org implements Comparable<Org> {
        final Answer answer;

        Org(answer) {
            this.answer = answer;
        }

        String getCommission() {
            return answer.commission;
        }

        OrgUnit getFormUnit() {
            return answer.formUnit;
        }

        String getProgramForm() {
            return answer.programForm;
        }

        String getProgramSubject() {
            return answer.programSubject;
        }

        String getSecretaryPost() {
            return answer.secretaryPost;
        }

        String getSecretaryOU() {
            return answer.secretaryOU;
        }

        String getSecretaryFIO() {
            return answer.secretaryFIO;
        }

        @Override
        int compareTo(Org o) {
            return ComparisonChain.start()
                    .compare(this.answer.commission, o.answer.commission)
                    .compare(this.answer.formUnit.getId(), o.answer.formUnit.getId())
                    .compare(this.answer.programForm, o.answer.programForm)
                    .compare(this.answer.programSubject, o.answer.programSubject)
                    .result()
        }
    }

    class Entrant implements Comparable<Entrant> {
        final Answer answer;

        Entrant(Answer answer) {
            this.answer = answer;
        }

        boolean isCompensationType(String compensationType) {
            return answer.compensationType.equals(compensationType);
        }

        boolean isNotCompetitionType(String competitionType) {
            return !answer.competitionType.getCode().equals(competitionType);
        }

        String[] getRow(int index) {
            String[] table = new String[5];
            table[0] = index as String;
            table[1] = answer.entrantFIO;
            table[2] = answer.entrantDoc;
            table[3] = answer.orderTitle;

            List<String> specials = new ArrayList<>();
            specials.addAll(getBenefits(answer.entrantID));
            if (isNotCompetitionType(EnrCompetitionTypeCodes.MINISTERIAL) && isNotCompetitionType(EnrCompetitionTypeCodes.CONTRACT))
                specials.add(answer.competitionType.getShortTitle());
            specials.add(answer.comment);
            if (answer.citizenship.code != 0) {
                if (answer.citizenship instanceof AddressCountry) {
                    specials.add("гражданин " + PersonManager.instance().declinationDao().getDeclinationCountry((AddressCountry) answer.citizenship, GrammaCase.GENITIVE));
                } else if (answer.citizenship instanceof NoCitizenship) {
                    specials.add(answer.citizenship.title);
                }
            }
            table[4] = CommonBaseStringUtil.joinNotEmpty(specials, ", ",);

            return table;
        }

        @Override
        int compareTo(Entrant o) {
            return ComparisonChain.start()
                    .compare(this.answer.entrantFIO, o.answer.entrantFIO)
                    .compare(this.answer.entrantID, o.answer.entrantID)
                    .compare(this.answer.orderID, o.answer.orderID)
                    .result();
        }
    }


    static def String makeEduDoc(EduDocumentKind docKind, String otherDoc, String serial, String number) {
        String dKind = docKind.getCode().equals(EduDocumentKindCodes.OTHER) ?
                otherDoc.substring(0, 1).toUpperCase() + otherDoc.substring(1)
                : docKind.getShortTitle().substring(0, 1).toUpperCase() + docKind.getShortTitle().substring(1);

        String sn;
        String separator;
        if (serial != null) {
            serial = serial.replace(" ", "\u00a0");
            separator = serial.size() + number.size() < 16 ? "\u00a0" : " ";
            sn = serial + separator + "№" + number;
        } else sn = "№" + number;

        return dKind + " " + sn;
    }

    def List<String> getBenefits(Long entrantID) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        String alias = "alias";
        dql.fromEntity(PersonBenefit.class, alias);
        dql.where(eq(
                property(alias, PersonBenefit.person().id()),
                value(entrantID)
        ));
        dql.column(property(alias, PersonBenefit.benefit().shortTitle()));

        return dql.createStatement(session).<String> list();
    }
}