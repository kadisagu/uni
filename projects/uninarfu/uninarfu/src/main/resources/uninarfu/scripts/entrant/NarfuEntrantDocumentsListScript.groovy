package uninarfu.scripts.entrant

import com.google.common.collect.Sets
import org.hibernate.Session
import org.tandemframework.core.entity.*
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLFunctions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.PersonEduDocument
import ru.tandemservice.uni.dao.UniBaseDao
import ru.tandemservice.unienr14.entrant.bo.EnrEntrantBaseDocument.util.EnrEntrantDocumentViewWrapper
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Ekaterina Zvereva
 * @since 14.05.2015
 */

return new NarfuEntrantDocumentsList(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати

).print()

class NarfuEntrantDocumentsList
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()


    def print()
    {
        def person = entrantRequest.entrant.person
        im.put('bookNumber', entrantRequest.entrant.personalNumber)

        // формирующее подразделение
        def formativeOU = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "comp")
                .top(1)
                .column(property("comp",EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit() ))
                .where(eq(property("comp", EnrRequestedCompetition.request()), value(entrantRequest)))
                .order(property("comp", EnrRequestedCompetition.priority()))
                .createStatement(session).<OrgUnit>uniqueResult()
        fillDocumentsList(formativeOU)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Опись абитуриента ${person.identityCard.fullFio}.rtf"]
    }


    def fillDocumentsList(OrgUnit formativeOU)
    {

        Set<ViewWrapper<IEntity>> documents = new HashSet<>();
        Map<Long, String> copiesMap = new HashMap<>();

        //получаем УЛ и документы об образовании, указанные в заявлениях абитуриента
            documents.add(new EnrEntrantDocumentViewWrapper(entrantRequest.getIdentityCard()))
            copiesMap.put(entrantRequest.identityCard.id, "копия")
            documents.add(new EnrEntrantDocumentViewWrapper(entrantRequest.eduDocument))
            copiesMap.put(entrantRequest.eduDocument.id, entrantRequest.isEduInstDocOriginalHandedIn() ? "оригинал" : "копия")

        List<Long> documentList = new DQLSelectBuilder().fromEntity(EnrEntrantRequestAttachment.class, "e").column(property("e", EnrEntrantRequestAttachment.document().id()))
                .where(eq(property("e", EnrEntrantRequestAttachment.entrantRequest()), value(entrantRequest))).createStatement(getSession()).list()

        for (EnrOlympiadDiploma document : DataAccessServices.dao().getList(EnrOlympiadDiploma.class, documentList, EnrEntrantBaseDocument.registrationDate().s()))
            documents.add(new EnrEntrantDocumentViewWrapper(document));

        for (EnrEntrantBaseDocument document : DataAccessServices.dao().getList(EnrEntrantBaseDocument.class, documentList, EnrEntrantBaseDocument.registrationDate().s()))
            documents.add(new EnrEntrantDocumentViewWrapper(document));

        //Получаем признак оригинал/копия для найденных документов, указанных в приложении к заявлениям
        for (EnrEntrantRequestAttachment attachment : DataAccessServices.dao().getList(EnrEntrantRequestAttachment.class, EnrEntrantRequestAttachment.document().id(), UniBaseDao.ids(documents)))
            copiesMap.put(attachment.document.id, attachment.isOriginalHandedIn() ? "оригинал" : "копия")

        List<EnrEntrantDocumentViewWrapper> docList = new ArrayList(documents)
        Collections.sort(docList, new EntityComparator<IEntity>(new EntityOrder(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE + ".priority"), new EntityOrder(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE), new EntityOrder("id")));

        final List<String[]> rows = new ArrayList<String[]>();

        int i=0;
        rows.add([++i, formativeOU.divisionCode, entrantRequest.regDate.format("dd.MM.yyyy"), "Заявление", "", ""])

        for (def attachment: docList) {
            String[] row = new String[6]
            row[0] = ++i
            row[1] = formativeOU.divisionCode
            row[2] = ((Date)attachment.getProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_REG_DATE))?.format("dd.MM.yyyy")
            String title = new StringBuilder().append(attachment.getProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_DOCUMENT_TYPE_TITLE)).append(" ")
            .append(attachment.getProperty(EnrEntrantDocumentViewWrapper.VIEW_PROPERTY_SERIA_AND_NUMBER)).append(copiesMap.get(attachment.id)? " (" +copiesMap.get(attachment.id) + ")" : "").toString()
            row[3] = title

            rows.add(row)
        }

        // вычисляем дисциплины для сдачи
        List<EnrExamPassDiscipline> examPassDisciplines = new DQLSelectBuilder()
                .fromEntity(EnrExamPassDiscipline.class, "e").column("e")
                .where(eq(property("e", EnrExamPassDiscipline.entrant().id()), value(entrantRequest.entrant.id)))
                .where(eq(property("e", EnrExamPassDiscipline.retake()), value(Boolean.FALSE)))
                .where(isNotNull(property("e", EnrExamPassDiscipline.markAsLong())))
                .order(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
                .order(property("e", EnrExamPassDiscipline.passForm().code()))
                .createStatement(session).list();

        int number = 0
        //Бланки ответов на ВИ
        for (EnrExamPassDiscipline item : examPassDisciplines)
        {
            String[] row = new String[6]
            row[0] = ++i
            row[1] = formativeOU.divisionCode
            row[2] = item.markDate?.format("dd.MM.yyyy")
            row[3] = new StringBuilder("Бланк ответа на вступительном испытании № ").append(++number).toString()

            rows.add(row)
        }

        getContractsData(i, formativeOU.divisionCode, rows)

        tm.put('T1', rows as String[][])
    }

    private void getContractsData(int i, String code, ArrayList<String[]> rows)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromDataSource(
                new DQLSelectBuilder().fromEntity(EnrContractTemplateDataSimple.class, "temp")
                        .column(property("temp"))
                        .column(DQLFunctions.rowNumberBuilder().partition(property("ver", CtrContractVersion.creationDate()))
                            .partition(property(CtrContractVersion.contract().fromAlias("ver")))
                            .order(property("ver", CtrContractVersion.creationDate()), OrderDirection.desc).build(), "num")
                        .joinPath(DQLJoinType.inner, EnrContractTemplateDataSimple.owner().fromAlias("temp"), "ver")
                        .joinPath(DQLJoinType.inner, CtrContractVersion.contract().fromAlias("ver"), "contract")
                        .where(DQLExpressions.in(property("contract.id"), new DQLSelectBuilder().fromEntity(EnrEntrantContract.class, "enc")
                        .column(property("enc", EnrEntrantContract.contractObject().id()))
                        .where(eq(property("enc", EnrEntrantContract.requestedCompetition().request()), value(entrantRequest)))
                        .buildQuery())).buildQuery(), "data"
        )
                .column(property("data.temp"))
                .where(eq(property("data.num"), value(1)))
        List<EnrContractTemplateDataSimple> contractList = builder.createStatement(session).list()

        for (EnrContractTemplateDataSimple item : contractList)
        {

            String[] row = new String[6]
            row[0] = ++i
            row[1] = code
            row[2] = item.owner.docStartDate?.format("dd.MM.yyyy")
            row[3] = "Договор на оказание платных образовательных услуг"

            rows.add(row)
        }
    }
}