package uninarfu.scripts.report

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.util.NumberAsStringComparator
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.movestudentrmc.entity.ExtractTextRelation
import ru.tandemservice.movestudentrmc.entity.IAbstractOrder
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.OrgUnitPrintTiltleComparator
import ru.tandemservice.uniepp.entity.plan.EppEduPlan
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf
import ru.tandemservice.uninarfu.base.bo.NarfuDipDocumentReport.ui.IssuanceRegBook.NarfuDipDocumentReportIssuanceRegBookUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 22.04.2015
 */

return new NarfuDipIssuanceRegBookPrint(// стандартные входные параметры скрипта
                                        session: session, // сессия
                                        template: template, // шаблон
                                        model: model
).print()

class NarfuDipIssuanceRegBookPrint
{
    Session session
    byte[] template
    NarfuDipDocumentReportIssuanceRegBookUI model
    def im = new RtfInjectModifier()

    private static final titleTable = "title"
    private static final headerTable = "orgUnit"
    private static final issuanceTable = "student"
    private static final signTable = "orgUnitHead"

    def print()
    {
        def document = buildReport(new RtfReader().read(template))
        return [document: RtfUtil.toByteArray(document), fileName: 'NarfuDipIssuanceRegBook.rtf']
    }

    private RtfDocument buildReport(RtfDocument document)
    {
        Map<String, RtfTable> contentTables = new HashMap<>();

        contentTables.put(titleTable, copyTable(document, titleTable, true))
        contentTables.put(headerTable, copyTable(document, headerTable, false))
        contentTables.put(issuanceTable, copyTable(document, issuanceTable, false))
        contentTables.put(signTable, copyTable(document, signTable, false))
        document.elementList.clear()
        document.addElement(contentTables.get(titleTable).getClone())
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        createTables(document, contentTables)
        return document
    }

    private void createTables(RtfDocument document, Map<String, RtfTable> contentTables)
    {
        List<DiplomaIssuance> issuanceList = getFilteredDipIssuanceList()
        Map<OrgUnit, List<DiplomaIssuance>> issuanceByOrgUnitMap = getIssuanceByOrgUnitMap(issuanceList)
        Map<DiplomaIssuance, List<DiplomaContentIssuance>> contentIssuanceMap = getContentIssuanceMap(issuanceList)
        Map<Student, String> excludeOrderMap = getExcludeOrderMap(issuanceList)

        List<OrgUnit> orgUnitList = new ArrayList<>(issuanceByOrgUnitMap.keySet())
        orgUnitList.sort(new OrgUnitPrintTiltleComparator())
        for (OrgUnit orgUnit : orgUnitList) {
            if (orgUnitList.indexOf(orgUnit) > 0) {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            document.addElement(contentTables.get(headerTable).getClone())
            im.put("orgUnit", orgUnit.printTitle)

            for (DiplomaIssuance issuance : issuanceByOrgUnitMap.get(orgUnit)) {
                document.addElement(contentTables.get(issuanceTable).getClone())
                fillIssuanceTable(issuance, contentIssuanceMap.get(issuance), document, excludeOrderMap)
            }
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.addElement(contentTables.get(signTable).getClone())

            im.put("orgUnitHead", getOrgUnitHead(orgUnit))
            im.modify(document)

        }
    }

    private void fillIssuanceTable(DiplomaIssuance issuance, List<DiplomaContentIssuance> contentIssuanceList, RtfDocument document,
                                   Map<Student, String> excludeOrderMap)
    {
        im.put("registryNumber", issuance.registrationNumber)
        im.put("issueDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(issuance.issuanceDate))
        im.put("student", issuance.diplomaObject.student.person.fullFio)
        im.put("birthDate", issuance.diplomaObject.student.person.birthDateStr)
        im.put("entranceYear", String.valueOf(issuance.diplomaObject.student.entranceYear))
        im.put("programSubject", issuance.diplomaObject.student.educationOrgUnit.titleWithFormAndCondition)
        im.put("qualification", getQualification(issuance))
        im.put("dipNumber", getDipIssuanceNumbers(issuance));
        String success = issuance.diplomaObject.content.isWithSuccess() ? "с отличием" : "";
        im.put("success", success)
        im.put("gosData", getStateCommissionData(issuance))
        im.put("appNumber", getDipContentIssuanceNumbers(contentIssuanceList))
        Student student = issuance.getDiplomaObject().student
        im.put("excludeOrder", excludeOrderMap.get(student))
        im.modify(document)
    }

    private static RtfTable copyTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);

        if (removeLabel) {
            for (RtfRow row : table.getRowList()) {
                for (RtfCell cell : row.getCellList()) {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null) {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }
        return table
    }

    private static Map<OrgUnit, List<DiplomaIssuance>> getIssuanceByOrgUnitMap(List<DiplomaIssuance> issuanceList)
    {
        Map<OrgUnit, List<DiplomaIssuance>> orgUnitIssuanceMap = new HashMap<>()
        issuanceList.sort(new Comparator<DiplomaIssuance>() {
            @Override
            int compare(DiplomaIssuance o1, DiplomaIssuance o2)
            {
                return NumberAsStringComparator.get().compare(o1.registrationNumber, o2.registrationNumber)
            }
        })

        for (DiplomaIssuance issuance : issuanceList) {
            SafeMap.safeGet(orgUnitIssuanceMap, issuance.diplomaObject.student.educationOrgUnit.formativeOrgUnit,
                            ArrayList.class).add(issuance)
        }
        return orgUnitIssuanceMap
    }

    private Map<DiplomaIssuance, List<DiplomaContentIssuance>> getContentIssuanceMap(List<DiplomaIssuance> issuanceList)
    {
        Map<DiplomaIssuance, List<DiplomaContentIssuance>> contentIssuanceMap = new HashMap<>()
        List<DiplomaContentIssuance> contentIssuanceList = new DQLSelectBuilder()
                .fromEntity(DiplomaContentIssuance, "ci").column("ci")
                .where(DQLExpressions.in(property("ci", DiplomaContentIssuance.diplomaIssuance()), issuanceList))
                .createStatement(getSession()).list()
        contentIssuanceList.sort(new Comparator<DiplomaContentIssuance>() {
            @Override
            int compare(DiplomaContentIssuance o1, DiplomaContentIssuance o2)
            {
                return NumberAsStringComparator.get().compare(o1.blankNumber, o2.blankNumber)
            }
        })

        for (DiplomaContentIssuance contentIssuance : contentIssuanceList) {
            SafeMap.safeGet(contentIssuanceMap, contentIssuance.diplomaIssuance, ArrayList.class).add(contentIssuance)
        }
        return contentIssuanceMap
    }

    private List<DiplomaIssuance> getFilteredDipIssuanceList()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(DiplomaIssuance.class, "i").column("i")
        if (model.showEduLevel) {
            dql.where(
                    eqValue(property("i", DiplomaIssuance.diplomaObject().content().programSubject().subjectIndex().programKind().eduLevel()),
                            model.getEduLevel()))
        }
        if (model.showDipDocType) {
            dql.where(DQLExpressions.in(property("i", DiplomaIssuance.diplomaObject().content().type()), model.getDipDocumentTypes()))
        }
        if (model.showFormativeOrgUnit) {
            dql.where(DQLExpressions.in(
                    property("i", DiplomaIssuance.diplomaObject().student().educationOrgUnit().formativeOrgUnit()),
                    model.getFormativeOrgUnitList()))
        }
        if (model.showProgramSubject) {
            dql.where(DQLExpressions.in(property("i", DiplomaIssuance.diplomaObject().content().programSubject()), model.getProgramSubjectList()))
        }
        if (model.issueDateFrom || model.issueDateTo) {
            FilterUtils.applyBetweenFilter(dql, "i", DiplomaIssuance.issuanceDate().path, model.getIssueDateFrom(), model.getIssueDateTo())
        }
        if (model.isDuplicate()) {
            dql.where(isNotNull(property("i", DiplomaIssuance.replacedIssuance())))
        }
        if (model.showStateCommissionDateFrom || model.showStateCommissionDateTo) {
            FilterUtils.applyBetweenFilter(dql, "i", DiplomaIssuance.diplomaObject().stateCommissionDate().path, model.stateCommissionDateFrom, model.stateCommissionDateTo)
        }
        if (model.showStateCommissionProtocolNumber) {
            FilterUtils.applyLikeFilter(dql, model.stateCommissionProtocolNumber, DiplomaIssuance.diplomaObject().stateCommissionProtocolNumber().fromAlias("i"))
        }

        return dql.createStatement(getSession()).list()
    }

    private static String getStateCommissionData(DiplomaIssuance issuance)
    {
        String stateCommissionData = ""
        if (!StringUtils.isEmpty(issuance.diplomaObject.stateCommissionProtocolNumber)) {
            stateCommissionData += "№" + issuance.diplomaObject.stateCommissionProtocolNumber
        }
        if (issuance.diplomaObject.stateCommissionDate != null) {
            stateCommissionData += " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(issuance.diplomaObject.stateCommissionDate)
        }

        return stateCommissionData
    }

    private static String getQualification(DiplomaIssuance issuance)
    {
		return issuance.diplomaObject.content.programQualification?.title
    }

    private static String getOrgUnitHead(OrgUnit orgUnit)
    {
        String orgUnitHead = StringUtils.capitalize(((EmployeePost) orgUnit.head).postRelation.postBoundedWithQGandQL.title)
        orgUnitHead += " " + ((EmployeePost) orgUnit.head).postRelation.orgUnitType.genitive
        orgUnitHead += " " + ((EmployeePost) orgUnit.head).fio
        return orgUnitHead
    }

    private static String getDipContentIssuanceNumbers(List<DiplomaContentIssuance> contentIssuanceList)
    {
        StringBuilder result = new StringBuilder();

        if (contentIssuanceList != null) {
            Iterator iterator = contentIssuanceList.iterator();
            while (iterator.hasNext()) {
                DiplomaContentIssuance contentIssuance = iterator.next()
                result.append(contentIssuance.blankSeria + " " + contentIssuance.blankNumber);
                if (iterator.hasNext()) result.append(", ");
            }
        }

        return result.toString();
    }

    private static String getDipIssuanceNumbers(DiplomaIssuance issuance)
    {
        String separator = issuance.blankSeria != null && issuance.blankNumber != null ? " " : "";
        String seria = issuance.blankSeria == null ? "" : issuance.blankSeria;
        String number = issuance.blankNumber == null ? "" : issuance.blankNumber;
        return seria + separator + number;
    }

    private Map<Student, String> getExcludeOrderMap(List<DiplomaIssuance> issuanceList)
    {
        Set<Student> students = new HashSet<>()
        Map<Student, String> orderMap = new HashMap<>()

        for (DiplomaIssuance issuance : issuanceList) {
            students.add(issuance.diplomaObject.student)
        }

        List<ExtractTextRelation> relationList = new DQLSelectBuilder()
                .fromEntity(ExtractTextRelation.class, "e").column("e")
                .where(DQLExpressions.in(property("e", ExtractTextRelation.student()), students))
                .where(or(
                eqValue(property("e", ExtractTextRelation.order().representation().type().code()),
                        RepresentationTypeCodes.DIPLOM_AND_EXCLUDE),
                eqValue(property("e", ExtractTextRelation.order().representation().type().code()),
                        RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST)
        ))
                .where(eqValue(property("e", ExtractTextRelation.order().order().state().code()), MovestudentOrderStatesCodes.CODE_5))
                .order(property("e", ExtractTextRelation.order().order().commitDate()))
                .createStatement(getSession()).list()

        for (ExtractTextRelation relation : relationList) {
            IAbstractOrder order = relation.order.order
            String orderData = "№" + order.number + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(order.commitDate)
            orderMap.put(relation.student, orderData)
        }

        return orderMap

    }


}