/* $Id$ */
package uninarfu.scripts.report

import com.google.common.collect.Multimap
import com.google.common.collect.TreeMultimap
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantActAcceptanceDocAdd.NarfuEnrReportEntrantActAcceptanceDocAddUI

import java.beans.Introspector

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Andreev
 * @since 22.07.2015
 */
return new NarfuEnrEntrantActAcceptanceDocPrint(
        session: session,
        template: template, // шаблон
        model: model
).print()

class NarfuEnrEntrantActAcceptanceDocPrint
{
    // входные параметры
    Session session;
    byte[]                                     template
    NarfuEnrReportEntrantActAcceptanceDocAddUI model

    def print()
    {

        List<Object[]> list = getData();

        if (list.isEmpty())
            return [document: null, fileName: null]


        RtfDocument mainDocument = printData(prepareData(list));

        String number = model.getNumber() != null ? "№" + (model.getNumber() as String) : "";
        String date = model.getDate() != null ? " от " + model.getDate().format("yyyy.MM.dd") : "";
        String fileName = "Акт передачи незачисленных абитуриентов в ПК" + date + " " + number + ".rtf";

        return [document: RtfUtil.toByteArray(mainDocument),
                fileName: fileName]
    }

    def List<Object[]> getData()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();

        String erc_alias = "erc";
        dql.fromEntity(EnrRequestedCompetition.class, erc_alias);
        dql.where(eq(
                property(erc_alias, EnrRequestedCompetition.request().entrant().enrollmentCampaign()),
                value(model.enrollmentCampaignSelector.enrollmentCampaign)
        ));
        dql.where(and(
                not(eq(
                        property(erc_alias, EnrRequestedCompetition.request().entrant().state().code()),
                        value(EnrEntrantStateCodes.ENROLLED)
                )),
                not(eq(
                        property(erc_alias, EnrRequestedCompetition.request().entrant().state().code()),
                        value(EnrEntrantStateCodes.IN_ORDER)
                ))
        ));
        FilterUtils.applySelectFilter(dql, erc_alias,
                                      EnrRequestedCompetition.request().enrollmentCommission(), model.enrollmentCommissionList);
        FilterUtils.applySelectFilter(dql, erc_alias,
                                      EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit(),
                                      model.enrOrgUnitList);
        FilterUtils.applySelectFilter(dql, erc_alias,
                                      EnrRequestedCompetition.request().type(), model.enrRequestType);
        FilterUtils.applySelectFilter(dql, erc_alias,
                                      EnrRequestedCompetition.competition().programSetOrgUnit().formativeOrgUnit(),
                                      model.formativeOrgUnitList);

        String erce_alias = "erce";
        dql.fromEntity(EnrEnrollmentCommissionExt.class, erce_alias);
        dql.where(eq(
                property(erc_alias, EnrRequestedCompetition.request().enrollmentCommission()),
                property(erce_alias, EnrEnrollmentCommissionExt.enrEnrollmentCommission())
        ));

        //отборочная коммисия
        dql.column(property(erc_alias, EnrRequestedCompetition.request().enrollmentCommission().title()));
        //Абитуриент
        dql.column(property(erc_alias, EnrRequestedCompetition.request().entrant().person().identityCard().fullFio()));
        //документ об образовании
        dql.column(property(erc_alias, EnrRequestedCompetition.request().eduDocument().eduDocumentKind().shortTitle()));
        dql.column(property(erc_alias, EnrRequestedCompetition.request().eduDocument().seria()));
        dql.column(property(erc_alias, EnrRequestedCompetition.request().eduDocument().number()));
        dql.column(property(erc_alias, EnrRequestedCompetition.request().eduInstDocOriginalHandedIn()));
        //Секретарь
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().postRelation().postBoundedWithQGandQL().post().title()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().orgUnit().title()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().employee().person().identityCard().firstName()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().person().identityCard().middleName()));
        dql.column(property(erce_alias, EnrEnrollmentCommissionExt.employee().person().identityCard().lastName()));
        //другой документ об оюразовании
        dql.column(property(erc_alias, EnrRequestedCompetition.request().eduDocument().documentKindDetailed()));

        return dql.createStatement(session).<Object[]> list();
    }

    def Multimap<Org, String[]> prepareData(List<Object[]> list)
    {
        Multimap<Org, String[]> map = TreeMultimap.create(
                new Comparator<Org>() {
                    @Override
                    int compare(Org o1, Org o2)
                    {
                        return o1.commission.compareTo(o2.commission);
                    }
                },
                new Comparator<String[]>() {
                    @Override
                    int compare(String[] o1, String[] o2)
                    {
                        return o1[1].compareTo(o2[1]);
                    }
                }
        );
        for (Object[] row : list) {

            String[] person = new String[4];
            person[1] = (String) row[1];
            person[2] = makeEduDoc((String) row[2], (String) row[11], (String) row[3], (String) row[4]); ;
            person[3] = ((Boolean) row[5]) ? "оригинал" : "копия";

            map.put(
                    new Org(
                            (String) row[0],
                            ((String) row[8])[0] + "." + ((String) row[9])[0] + ".\u00a0" + (String) row[10],
                            (String) row[6],
                            (String) row[7]),
                    person);
        }
        return map;
    }

    def RtfDocument printData(Multimap<Org, String[]> map)
    {
        RtfDocument mainDocument = RtfDocument.fromScratch();
        RtfDocument temp = RtfDocument.fromByteArray(template);
        mainDocument.setHeader(temp.getHeader());
        mainDocument.setSettings(temp.getSettings());

        for (Org org : map.keySet()) mainDocument.getElementList().addAll(printAct(org, map.get(org)));

        return mainDocument;
    }

    def List<IRtfElement> printAct(Org org, Collection<String[]> persons)
    {
        RtfDocument document = RtfDocument.fromByteArray(template);
        RtfInjectModifier rtfInjectMod = new RtfInjectModifier();

        String number = model.getNumber() != null ? "№" + (model.getNumber() as String) : "";
        String date = model.getDate() != null ? DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDate()) + " года" : "";
        rtfInjectMod.put("date_n_number", date + " " + number);

        String commiss = org.commission;
        rtfInjectMod.put("enrollmentCommission", commiss);

        rtfInjectMod.put("sEnrollmentCommission", Introspector.decapitalize(commiss));

        rtfInjectMod.put("secretaryPost", Introspector.decapitalize(org.secretaryPost + " " + org.secretaryOU));

        rtfInjectMod.put("secretaryFIO", org.secretaryFIO);

        rtfInjectMod.modify(document);
        int index = 1;
        for (String[] person : persons) {
            person[0] = index as String;
            index++;
        }

        new RtfTableModifier()
                .put("T", persons.toArray() as String[][])
                .modify(document);

        return document.getElementList();
    }

    class Org
    {
        String commission;
        String secretaryFIO;
        String secretaryPost;
        String secretaryOU;

        Org(String commission,
            String secretaryFIO,
            String secretaryPost,
            String secretaryOU)
        {
            this.commission = commission;
            this.secretaryFIO = secretaryFIO;
            this.secretaryPost = secretaryPost;
            this.secretaryOU = secretaryOU;
        }
    }

    static def String makeEduDoc(String docKind, String otherDoc, String serial, String number)
    {
        docKind = docKind.equals("иной") ?
                  otherDoc.substring(0, 1).toUpperCase() + otherDoc.substring(1)
                                         : docKind.substring(0, 1).toUpperCase() + docKind.substring(1);

        String sn;
        String separator;
        if (serial != null) {
            serial = serial.replace(" ", "\u00a0");
            separator = serial.size() + number.size() < 16 ? "\u00a0" : " ";
            sn = serial + separator + "№" + number;
        } else sn = "№" + number;

        return docKind + " " + sn;
    }
}
