package uninarfu.scripts.report

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.program.entity.EduProgramProf
import ru.tandemservice.unienr14.catalog.entity.EnrEduLevelRequirement
import ru.tandemservice.unienr14.catalog.entity.EnrRequestType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon
import ru.tandemservice.unienr14.competition.entity.*
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCountAdd.NarfuEnrReportRequestCountAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 10.04.2015
 */
return new NarfuEnrRequestCountPrint(                     // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

class NarfuEnrRequestCountPrint
{
    Session session
    byte[] template
    NarfuEnrReportRequestCountAddUI model
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    private static final vpoTable = "T1"
    private static final spoTable = "T2"
    private static final headerTable = "header"
    private static final personTable = "delegatePost"
    private static final total = "Всего"

    def print()
    {
        def document = createReport(new RtfReader().read(template))
        return [document: RtfUtil.toByteArray(document), fileName: 'NarfuEnrRequestCountPrint.rtf']
    }

    private RtfDocument createReport(RtfDocument document)
    {
        Map<String, RtfTable> contentTables = new HashMap<>();

        contentTables.put(headerTable, removeTable(document, headerTable, true))
        contentTables.put(vpoTable, removeTable(document, vpoTable, false))
        contentTables.put(spoTable, removeTable(document, spoTable, false))
        contentTables.put(personTable, removeTable(document, personTable, false))

        document.elementList.clear()

        RtfTable contentTable = contentTables.get(headerTable).getClone();
        document.getElementList().add(contentTable)
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        printRequestCount(document, contentTables)

        return document
    }


    private void printRequestCount(RtfDocument document, Map<String, RtfTable> contentTables)
    {

        //находим отфильтрованые заявления
        DQLSelectBuilder competitionIdsBuilder = model.competitionFilterAddon.getEntityIdsFilteredBuilder("c")
                .where(eq(property("c", EnrCompetition.P_ID), property("rc", EnrRequestedCompetition.competition().id())))
        DQLSelectBuilder requestedCompetitionIdsBuilder = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "rc")
                .column(property("rc"))
                .where(eq(property("rc", EnrRequestedCompetition.request().entrant().enrollmentCampaign()), value(model.getEnrollmentCampaign())))
                .where(ne(property("rc", EnrRequestedCompetition.state().code()), value(EnrEntrantStateCodes.TAKE_DOCUMENTS_AWAY)))
                .where(exists(competitionIdsBuilder.buildQuery()))
                .where(eq(property("rc", EnrRequestedCompetition.request().entrant().archival()), value(Boolean.FALSE)))
                .where(model.isParallelActive() ? eq(property("rc", EnrRequestedCompetition.parallel()), value(model.isParallelOnly())) : null)
        //Отборочные комиссии
        if (model.isEnrollmentCommissionActive())
            requestedCompetitionIdsBuilder.where(DQLExpressions.in(property("rc", EnrRequestedCompetition.request().enrollmentCommission()), model.enrollmentCommissionList))

        FilterUtils.applyBetweenFilter(requestedCompetitionIdsBuilder, "rc", EnrRequestedCompetition.request().regDate().getPath(), model.getDateFrom(), model.getDateTo())
        List<EnrRequestedCompetition> requestedCompetitionList = requestedCompetitionIdsBuilder.createStatement(getSession()).<EnrRequestedCompetition> list()

        //делим полученные заявления на порции. каждая порция - ячейка таблицы
        Map<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>>> requestByCellMap = getRequestByCellMap(requestedCompetitionList)
        Map<EnrProgramSetBase, List<EduProgramProf>> programSet2SingleProgramMap = getProgramSet2SingleProgramMap(requestedCompetitionList)

        List<EnrOrgUnit> enrOrgUnitList = new ArrayList<>(requestByCellMap.keySet())
        enrOrgUnitList.sort(new Comparator<EnrOrgUnit>() {
            @Override
            int compare(EnrOrgUnit o1, EnrOrgUnit o2)
            {
                return o1.isTop() || o2.isTop() ? -Boolean.compare(o1.isTop(), o2.isTop()) : o1.getTitle().compareTo(o2.getTitle())
            }
        })

        //Создаем таблицу для каждого филиала
        for (EnrOrgUnit enrOrgUnit : enrOrgUnitList)
        {
            List<OrgUnit> orgUnitList = new ArrayList<>(requestByCellMap.get(enrOrgUnit).keySet())
            orgUnitList.sort(new Comparator<OrgUnit>() {
                @Override
                int compare(OrgUnit o1, OrgUnit o2)
                {
                    return o1.isTop() || o2.isTop() ? -Boolean.compare(o1.isTop(), o2.isTop()) : o1.getTitle().compareTo(o2.getTitle())
                }
            })

            if (model.isByAllOrgUnit())
            {
                //если выбран чекбокс "По всем подразделениям", то число таблиц для каждого филиала будет равно числу формирующих подразделений
                for (OrgUnit orgUnit : orgUnitList)
                {
                    boolean hasNextTable = enrOrgUnitList.indexOf(enrOrgUnit) < enrOrgUnitList.size() - 1 || orgUnitList.indexOf(orgUnit) < orgUnitList.size() - 1
                    createTable(document, contentTables, hasNextTable)
                    fillTable(document, enrOrgUnit, orgUnit, orgUnitList, requestByCellMap, programSet2SingleProgramMap)
                }
            }

            else
            {
                boolean hasNextTable = enrOrgUnitList.indexOf(enrOrgUnit) < enrOrgUnitList.size() - 1
                createTable(document, contentTables, hasNextTable)
                fillTable(document, enrOrgUnit, null, orgUnitList, requestByCellMap, programSet2SingleProgramMap)
            }

        }
    }

    private void createTable(RtfDocument document, Map<String, RtfTable> contentTables, boolean hasNextTable)
    {
        isHigh() ? document.addElement(contentTables.get(vpoTable).getClone()) : document.addElement(contentTables.get(spoTable).getClone())
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.addElement(contentTables.get(personTable).getClone())
        if (hasNextTable)
        {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        }
    }


    private void fillTable(RtfDocument document, EnrOrgUnit enrOrgUnit, OrgUnit orgUnit, List<OrgUnit> orgUnitList,
                           Map<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>>> requestByCellMap,
                           Map<EnrProgramSetBase, List<EduProgramProf>> programSet2SingleProgramMap)
    {
        List<String[]> rows = new ArrayList<>()
        RtfRowIntercepterBase intercepter = getIntercepter(document)

        if (model.byAllOrgUnit)
        {
            if (isHigh())
            {
                fillVpoTableForOrgUnit(enrOrgUnit, orgUnit, requestByCellMap, programSet2SingleProgramMap, rows)
            }
            else
            {
                fillSpoTableForOrgUnit(enrOrgUnit, orgUnit, requestByCellMap, programSet2SingleProgramMap, rows)
            }
            im.put("orgUnit", orgUnit.getTitle())
        }
        else
        {
            int columnNumber = isHigh() ? 18 : 4
            List<List<Integer>> totalNumbers = new ArrayList<>(columnNumber)
            for (OrgUnit formativeOrgUnit : orgUnitList)
            {
                List<Integer> formativeNumbers
                if (isHigh())
                    formativeNumbers = fillVpoTableForOrgUnit(enrOrgUnit, formativeOrgUnit, requestByCellMap, programSet2SingleProgramMap, rows)
                else
                    formativeNumbers = fillSpoTableForOrgUnit(enrOrgUnit, formativeOrgUnit, requestByCellMap, programSet2SingleProgramMap, rows)
                totalNumbers.add(formativeNumbers)
                im.put("orgUnit", enrOrgUnit.institutionOrgUnit.orgUnit.title)
            }
            List<String> totalCells = new ArrayList<>(columnNumber + 1)

            totalCells.add(total)

            for (int i = 0; i < columnNumber; i++)
            {
                Integer value = null;
                for (List<Integer> numbers : totalNumbers)
                {
                    Integer currentValue = numbers.get(i)
                    if (currentValue != null)
                        value = safeInt(value) + currentValue
                }

                totalCells.add(value == null ? "—" : String.valueOf(value))
            }
            rows.add(totalCells.toArray(new String[totalCells.size()]))
        }
        im.put("date", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.dateTo))
        tm.put(isHigh() ? vpoTable : spoTable, rows.toArray(new String[rows.size()][]))
        tm.put(isHigh() ? vpoTable : spoTable, intercepter)

        String postTitle = ""
        if (model.secretary)
        {
            postTitle = StringUtils.capitalize(model.secretary.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
            postTitle += StringUtils.uncapitalize(model.secretary.orgUnit.accusativeCaseTitle? model.secretary.orgUnit.accusativeCaseTitle : model.secretary.orgUnit.title)
        }
        im.put("delegatePost", postTitle)
        im.put("delegateFio", model.secretary ? model.secretary.fio : "")

        im.modify(document)
        tm.modify(document)
    }

    private List<Integer> fillSpoTableForOrgUnit(EnrOrgUnit enrOrgUnit, OrgUnit orgUnit,
                                                 Map<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>>> requestByCellMap,
                                                 Map<EnrProgramSetBase, List<EduProgramProf>> programSet2SingleProgramMap, List<String[]> rows)
    {

        // получаем Набор ОП для приема на подразделении для текущего подразделения
        List<EnrProgramSetOrgUnit> programSetOrgUnitList = new ArrayList<>(requestByCellMap.get(enrOrgUnit).get(orgUnit).keySet())
        List<List<Integer>> formativeNumbers = new ArrayList<>()
        sortProgramSetOrgUnitList(programSetOrgUnitList)

        for (EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnitList)
        {
            EnrProgramSetBase programSetBase = programSetOrgUnit.programSet
            Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>> levelMap = requestByCellMap.get(enrOrgUnit).get(orgUnit).get(programSetOrgUnit)

            //для каждого набора
            for (EnrEduLevelRequirement levelRequirement : levelMap.keySet())
            {
                //массив с числом заявлений для ОП (по видам заявлений)
                List<Integer> numbers = new ArrayList<>(4);
                //ячейки для вывода в строке
                List<String> cells = new ArrayList<>(5);
                String rowTitle = getRowTitle(programSetBase, model, programSet2SingleProgramMap.get(programSetBase))
                if (!levelRequirement.code.equals(EnrEduLevelRequirementCodes.NO))
                {
                    rowTitle += " (на базе " + levelRequirement.shortTitle + ")"
                }
                cells.add(rowTitle)

                Map<String, Map<String, Integer>> programFormMap = levelMap.get(levelRequirement)
                //бюджет
                ochn:
                {
                    int value = getBudgetCount(programFormMap, EduProgramFormCodes.OCHNAYA)
                    cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getMinisterialPlan() > 0 ? "0" : "—"))
                    numbers.add(value == 0 && programSetOrgUnit.getMinisterialPlan() == 0 ? null : value)

                    value = getContractCount(programFormMap, EduProgramFormCodes.OCHNAYA)
                    cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getContractPlan() > 0 ? "0" : "—"))
                    numbers.add(value == 0 && programSetOrgUnit.getContractPlan() == 0 ? null : value)
                }
                //контракт
                zaochn:
                {
                    int value = getBudgetCount(programFormMap, EduProgramFormCodes.ZAOCHNAYA)
                    cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getMinisterialPlan() > 0 ? "0" : "—"))
                    numbers.add(value == 0 && programSetOrgUnit.getMinisterialPlan() == 0 ? null : value)

                    value = getContractCount(programFormMap, EduProgramFormCodes.ZAOCHNAYA)
                    cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getContractPlan() > 0 ? "0" : "—"))
                    numbers.add(value == 0 && programSetOrgUnit.getContractPlan() == 0 ? null : value)
                }
                rows.add(cells.toArray(new String[cells.size()]))
                formativeNumbers.add(numbers)
            }


        }
        List<String> orgUnitCells = new ArrayList<>(5);
        List<Integer> totalNumbers = new ArrayList<>(4);

        orgUnitCells.add(total + " по " + orgUnit.dativeCaseTitle)
        //считаем нагрузку для формирующего подразделения
        for (int i = 0; i < 4; i++)
        {
            Integer value = null;
            for (List<Integer> numbers : formativeNumbers)
            {
                Integer currentValue = numbers.get(i);
                if (currentValue != null)
                {
                    value = safeInt(value) + currentValue;
                }
            }
            totalNumbers.add(i, value)
            orgUnitCells.add(value == null ? "—" : String.valueOf(value));
        }
        rows.add(orgUnitCells.toArray(new String[orgUnitCells.size()]));
        tm.put(spoTable, rows.toArray(new String[rows.size()][]))
        return totalNumbers
    }

    private List<Integer> fillVpoTableForOrgUnit(EnrOrgUnit enrOrgUnit, OrgUnit orgUnit,
                                                 Map<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>>> requestByCellMap,
                                                 Map<EnrProgramSetBase, List<EduProgramProf>> programSet2SingleProgramMap, List<String[]> rows)
    {
        // получаем Набор ОП для приема на подразделении для текущего подразделения
        List<EnrProgramSetOrgUnit> programSetOrgUnitList = new ArrayList<>(requestByCellMap.get(enrOrgUnit).get(orgUnit).keySet())
        List<List<Integer>> formativeNumbers = new ArrayList<>()
        sortProgramSetOrgUnitList(programSetOrgUnitList)
        //формируем строку для каждого набора
        for (EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnitList)
        {
            //нагрузка сгруппированная по формам обучени и видам заявлений
            Map<String, Map<String, Integer>> loadByFormCompetitionMap = new HashMap<>()
            EnrProgramSetBase programSetBase = programSetOrgUnit.programSet
            Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>> levelMap = requestByCellMap.get(enrOrgUnit).get(orgUnit).get(programSetOrgUnit)

            for (EnrEduLevelRequirement level : levelMap.keySet())
            {
                Map<String, Map<String, Integer>> programFormMap = requestByCellMap.get(enrOrgUnit).get(orgUnit).get(programSetOrgUnit).get(level)
                if (programFormMap != null)
                {
                    for (String programForm : programFormMap.keySet())
                    {
                        Map<String, Integer> competitionTypeMap = programFormMap.get(programForm)
                        if (competitionTypeMap != null)
                        {
                            Map<String, Integer> currentCompMap = new HashMap<>()
                            for (String competitionType : competitionTypeMap.keySet())
                            {
                                int load = competitionTypeMap.get(competitionType)
                                Integer totalLoad = loadByFormCompetitionMap?.get(programForm)?.get(competitionType)
                                totalLoad = totalLoad == null ? load : totalLoad + load
                                currentCompMap.put(competitionType, totalLoad)
                            }
                            if (!currentCompMap.keySet().isEmpty())
                            {
                                loadByFormCompetitionMap.put(programForm, currentCompMap)
                            }
                        }
                    }
                }
            }

            //массив с числом заявлений для ОП (по видам заявлений)
            List<Integer> numbers = new ArrayList<>(18);
            //ячейки для вывода в строке
            List<String> cells = new ArrayList<>(19);
            String rowTitle = getRowTitle(programSetBase, model, programSet2SingleProgramMap.get(programSetBase))

            cells.add(rowTitle)
            fillVpoTableRowByEduForm(loadByFormCompetitionMap, programSetOrgUnit, numbers, cells, EduProgramFormCodes.OCHNAYA)
            fillVpoTableRowByEduForm(loadByFormCompetitionMap, programSetOrgUnit, numbers, cells, EduProgramFormCodes.OCHNO_ZAOCHNAYA)
            fillVpoTableRowByEduForm(loadByFormCompetitionMap, programSetOrgUnit, numbers, cells, EduProgramFormCodes.ZAOCHNAYA)

            rows.add(cells.toArray(new String[cells.size()]))
            formativeNumbers.add(numbers)
        }
        List<String> orgUnitCells = new ArrayList<>(19);
        List<Integer> totalNumbers = new ArrayList<>(18);

        orgUnitCells.add(total + " по " + orgUnit.dativeCaseTitle)
        //считаем нагрузку для формирующего подразделения
        for (int i = 0; i < 18; i++)
        {
            Integer value = null;
            for (List<Integer> numbers : formativeNumbers)
            {
                Integer currentValue = numbers.get(i);
                if (currentValue != null)
                {
                    value = safeInt(value) + currentValue;
                }
            }
            totalNumbers.add(i, value)
            orgUnitCells.add(value == null ? "—" : String.valueOf(value));
        }
        rows.add(orgUnitCells.toArray(new String[orgUnitCells.size()]));
        tm.put(spoTable, rows.toArray(new String[rows.size()][]))
        return totalNumbers
    }

    /* Map содержит  заявлений для каждой ячейки таблицы
       путь до числа заявлений:
       подразделение, ведущее прием - формирующее подразделение - подразделение, ведущее прием по набору ОП - набор ОП - форма освоения(код) - вид приема(код) - требования к уровню образования поступающих - число заявлений
     */

    private static Map<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>>> getRequestByCellMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        Map<EnrOrgUnit, Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>>> requestByCellMap = new HashMap<>()
        Map<EnrOrgUnit, List<EnrRequestedCompetition>> enrOrgUnitMap = new HashMap<>();
        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
        {
            SafeMap.safeGet(enrOrgUnitMap, requestedCompetition.competition.programSetOrgUnit.orgUnit, ArrayList.class).add(requestedCompetition)
        }

        for (EnrOrgUnit enrOrgUnit : enrOrgUnitMap.keySet())
        {
            requestByCellMap.put(enrOrgUnit, getOrgUnitMap(enrOrgUnitMap.get(enrOrgUnit)))
        }
        return requestByCellMap
    }

    //формирующее подразделение - подразделение, ведущее прием по набору ОП - набор ОП - форма освоения(код) - вид приема(код) - требования к уровню образования поступающих - число заявлений
    private static Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>> getOrgUnitMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        Map<OrgUnit, Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>>> orgUnitMap = new HashMap<>()
        Map<OrgUnit, List<EnrRequestedCompetition>> formativeOrgUnitMap = new HashMap<>();
        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
        {
            SafeMap.safeGet(formativeOrgUnitMap, requestedCompetition.competition.programSetOrgUnit.formativeOrgUnit, ArrayList.class).add(requestedCompetition)
        }

        for (OrgUnit enrOrgUnit : formativeOrgUnitMap.keySet())
        {
            orgUnitMap.put(enrOrgUnit, getOrgUnitEnrRequestMap(formativeOrgUnitMap.get(enrOrgUnit)))
        }
        return orgUnitMap
    }

    // Набор ОП для приема на подразделении - набор ОП - форма освоения(код) - вид приема(код) - требования к уровню образования поступающих - число заявлений
    private static Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>> getOrgUnitEnrRequestMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        Map<EnrProgramSetOrgUnit, Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>>> programSetRequestMap = new HashMap<>()
        Map<EnrProgramSetOrgUnit, List<EnrRequestedCompetition>> programSetMap = new HashMap<>();
        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
        {
            SafeMap.safeGet(programSetMap, requestedCompetition.competition.programSetOrgUnit, ArrayList.class).add(requestedCompetition)
        }
        for (EnrProgramSetOrgUnit programSet : programSetMap.keySet())
        {
            programSetRequestMap.put(programSet, getLevelEnrRequestsMap(programSetMap.get(programSet)))
        }
        return programSetRequestMap
    }

    //требования к уровню образования поступающих - форма освоения(код) - вид приема(код) - число заявлений
    private static Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>> getLevelEnrRequestsMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        Map<EnrEduLevelRequirement, Map<String, Map<String, Integer>>> eduLevelMap = new HashMap<>()
        Map<EnrEduLevelRequirement, List<EnrRequestedCompetition>> levelsCompetitionMap = new HashMap<>();

        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
        {
            SafeMap.safeGet(levelsCompetitionMap, requestedCompetition.competition.eduLevelRequirement, ArrayList.class).add(requestedCompetition)
        }

        for (EnrEduLevelRequirement level : levelsCompetitionMap.keySet())
        {
            eduLevelMap.put(level, getProgramFormEnrRequestsMap(levelsCompetitionMap.get(level)))
        }

        return eduLevelMap
    }

    //форма освоения(код) - вид приема(код)- число заявлений
    private static Map<String, Map<String, Integer>> getProgramFormEnrRequestsMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        Map<String, Map<String, Integer>> programFormMap = new HashMap<>()

        Map<String, List<EnrRequestedCompetition>> formReqMap = new HashMap<>();

        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
        {
            SafeMap.safeGet(formReqMap, requestedCompetition.competition.programSetOrgUnit.programSet.programForm.code, ArrayList.class).add(requestedCompetition)
        }

        for (String formCode : formReqMap.keySet())
        {
            programFormMap.put(formCode, getCompTypeEnrRequestsMap(formReqMap.get(formCode)))
        }
        return programFormMap
    }

    //вид приема(код) - число заявлений
    private static Map<String, Integer> getCompTypeEnrRequestsMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        Map<String, Integer> compTypeReqMap = new HashMap<>()
        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
        {
            String competitionCode = requestedCompetition.competition.type.code
            if (compTypeReqMap.get(competitionCode) != null)
            {
                int reqCount = compTypeReqMap.get(competitionCode) + 1
                compTypeReqMap.put(competitionCode, reqCount)
            }
            else
            {
                compTypeReqMap.put(competitionCode, 1)
            }
        }
        return compTypeReqMap
    }


    private static RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);

        if (removeLabel)
        {
            for (RtfRow row : table.getRowList())
            {
                for (RtfCell cell : row.getCellList())
                {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null)
                    {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }
        return table
    }

    private Map<EnrProgramSetBase, List<EduProgramProf>> getProgramSet2SingleProgramMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        Map<EnrProgramSetBase, List<EduProgramProf>> programSet2SingleProgramMap = new HashMap<>()
        List<Long> programSetBaseList = new ArrayList<>();
        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
        {
            programSetBaseList.add(requestedCompetition.competition.programSetOrgUnit.programSet.id)
        }

        List<EnrProgramSetItem> singleProgramList = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetItem.class, "item")
                .column(property("item"))
                .where(DQLExpressions.in(property("item", EnrProgramSetItem.programSet().id()), programSetBaseList))
                .createStatement(getSession()).list()


        for (EnrProgramSetItem item : singleProgramList)
        {
            SafeMap.safeGet(programSet2SingleProgramMap, item.getProgramSet(), ArrayList.class).add(item.program)
        }

        List<EnrProgramSetSecondary> secondaryList = new DQLSelectBuilder()
                .fromEntity(EnrProgramSetSecondary.class, "item")
                .column(property("item"))
                .where(DQLExpressions.in(property("item", EnrProgramSetSecondary.id()), programSetBaseList))
                .createStatement(getSession()).list()


        for (EnrProgramSetSecondary programSet : secondaryList)
        {
            SafeMap.safeGet(programSet2SingleProgramMap, programSet, ArrayList.class).add(programSet.program)
        }
        return programSet2SingleProgramMap;
    }

    private boolean isHigh()
    {
        String code = ((EnrRequestType) model.getCompetitionFilterAddon().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE).getValue()).getCode()
        return code.equals(EnrRequestTypeCodes.BS) || code.equals(EnrRequestTypeCodes.MASTER)
    }


    private static String getRowTitle(EnrProgramSetBase programSetBase, NarfuEnrReportRequestCountAddUI model, List<EduProgramProf> programProfList)
    {
        StringBuilder rowTitle = new StringBuilder()
        rowTitle.append(programSetBase.programSubject.getTitleWithCode())

        if ((!model.isDoNotDisplaySingleProgramTitle() && !model.isReplaceProgramSetBySingleProgramTitle())
                || (programProfList != null && programProfList.size() != 1))
        {
            rowTitle.append(" - ")
                    .append(programSetBase.title)
        }
        else
        {
            if (!model.isDoNotDisplaySingleProgramTitle() && model.isReplaceProgramSetBySingleProgramTitle())
            {
                rowTitle.append(" - ")
                        .append(programProfList.get(0).title)
                        .append(" ")
                        .append(programProfList.get(0).getImplConditionsShort())
            }
            else if (model.isDoNotDisplaySingleProgramTitle())
            {
                rowTitle.append(" ")
                        .append(programProfList.get(0).getImplConditionsShort())
            }
        }


        return rowTitle.toString()
    }

    /** Преобразует null в ноль, остальные значения возвращает без изменений. */
    private static int safeInt(Integer value) { return value == null ? 0 : value; }


    private static RtfRowIntercepterBase getIntercepter(RtfDocument document)
    {

        return new RtfRowIntercepterBase() {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                def lightGray = document.header.colorTable.addColor(192, 192, 191)
                def gray = document.header.colorTable.addColor(128, 128, 128)

                if (StringUtils.contains(value, "#_#"))
                {
                    List<String> lines = Arrays.asList(StringUtils.split(value, "#_#"));
                    RtfString rtfString = new RtfString()
                            .append(lines.get(0))
                            .par()
                            .color(gray)
                            .append(lines.get(1));

                    if (lines.size() > 2)
                        rtfString.par().append(lines.get(2));

                    return rtfString.toList();
                }
                if (StringUtils.contains(value, total + " по "))
                {
                    for (RtfCell cellInRow : row.cellList)
                    {
                        cellInRow.setBackgroundColorIndex(lightGray)
                    }
                    RtfString string = new RtfString();
                    string
                            .boldBegin()
                            .append(value)
                            .boldEnd();
                    return string.toList();
                }
                else if (StringUtils.contains(value, total))
                {
                    for (RtfCell cellInRow : row.cellList)
                    {
                        cellInRow.setBackgroundColorIndex(gray)
                    }
                    RtfString string = new RtfString();
                    string
                            .boldBegin()
                            .append(value)
                            .boldEnd();
                    return string.toList();
                }

                return null;
            }
        }
    }

    private static void sortProgramSetOrgUnitList(List<EnrProgramSetOrgUnit> programSetOrgUnitList)
    {

        programSetOrgUnitList.sort(new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2)
            {
                return o1.programSet.title.compareTo(o2.programSet.title)
            }
        })
        programSetOrgUnitList.sort(false, new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2)
            {
                return o1.programSet.programSubject.title.compareTo(o2.programSet.programSubject.title)
            }
        })
    }

    private static int getBudgetCount(Map<String, Map<String, Integer>> programFormMap, String programFormCode)
    {
        int value = 0
        Map<String, Integer> competitionMap = programFormMap?.get(programFormCode)
        value += safeInt(competitionMap?.get(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL))
        value += safeInt(competitionMap?.get(EnrCompetitionTypeCodes.EXCLUSIVE))
        value += safeInt(competitionMap?.get(EnrCompetitionTypeCodes.TARGET_ADMISSION))
        value += safeInt(competitionMap?.get(EnrCompetitionTypeCodes.MINISTERIAL))
        return value
    }

    private static int getContractCount(Map<String, Map<String, Integer>> programFormMap, String programFormCode)
    {
        int value = 0
        Map<String, Integer> competitionMap = programFormMap?.get(programFormCode)
        value += safeInt(competitionMap?.get(EnrCompetitionTypeCodes.CONTRACT))
        value += safeInt(competitionMap?.get(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT))
        return value
    }

    public static void fillVpoTableRowByEduForm(Map<String, Map<String, Integer>> programFormMap, EnrProgramSetOrgUnit programSetOrgUnit,
                                                List<Integer> numbers, List<String> cells, String programFormCode)
    {

        Map<String, Integer> competitionMap = programFormMap.get(programFormCode)

        //бюджет
        int value = getBudgetCount(programFormMap, programFormCode)
        cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getMinisterialPlan() > 0 ? "0" : "—"))
        numbers.add(value == 0 && programSetOrgUnit.getMinisterialPlan() == 0 ? null : value)

        //по видам бюджета
        value = safeInt(competitionMap?.get(EnrCompetitionTypeCodes.MINISTERIAL))
        cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getMinisterialPlan() > 0 ? "0" : "—"))
        numbers.add(value == 0 && programSetOrgUnit.getMinisterialPlan() == 0 ? null : value)

        value = safeInt(competitionMap?.get(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL))
        cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getMinisterialPlan() > 0 ? "0" : "—"))
        numbers.add(value == 0 && programSetOrgUnit.getMinisterialPlan() == 0 ? null : value)

        value = safeInt(competitionMap?.get(EnrCompetitionTypeCodes.EXCLUSIVE))
        cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getExclusivePlan() > 0 ? "0" : "—"))
        numbers.add(value == 0 && programSetOrgUnit.getExclusivePlan() == 0 ? null : value)

        value = safeInt(competitionMap?.get(EnrCompetitionTypeCodes.TARGET_ADMISSION))
        cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getTargetAdmPlan() > 0 ? "0" : "—"))
        numbers.add(value == 0 && programSetOrgUnit.getTargetAdmPlan() == 0 ? null : value)

        //контракт
        value = getContractCount(programFormMap, programFormCode)
        cells.add(value > 0 ? String.valueOf(value) : (programSetOrgUnit.getContractPlan() > 0 ? "0" : "—"))
        numbers.add(value == 0 && programSetOrgUnit.getContractPlan() == 0 ? null : value)
    }

}

