package uninarfu.scripts.entrant

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.RelationDegree
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.uninarfu.entity.RelationDegreeDeclinable
import ru.tandemservice.uninarfu.util.NarfuEntrantUtil

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Ekaterina Zvereva
 * @since 23.04.2015
 */
return new NarfuEntrantTutorProfile(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

class NarfuEntrantTutorProfile
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        EnrEntrant entrant = entrantRequest.entrant;
        //Получаем список родственников
        List<PersonNextOfKin> kinList = new DQLSelectBuilder().fromEntity(PersonNextOfKin.class, "person")
            .where(eq(property("person", PersonNextOfKin.person()), value(entrant.person)))
            .order(property("person.id"))
            .createStatement(session).list();
        if (kinList.empty)
            throw new ApplicationException("Абитуриент не указал ни одного родственника")
        PersonNextOfKin parent = kinList.get(0);
        im.put('entrantBookNumber', entrant.personalNumber)
        im.put('parentFIO', parent.fullFio)
        im.put('relationDegree', parent.relationDegree.title)

        StringBuilder identityCard = new StringBuilder().append(parent.passportType == null? "" : parent.passportType.title)
        .append(parent.passportSeria == null ? "" : ", серия " + parent.passportSeria)
        .append(parent.passportNumber == null? "" : ", номер " + parent.passportNumber);

        im.put('identityCardNumber', identityCard.toString())
        StringBuilder sb = new StringBuilder()
        if (parent.passportIssuanceDate != null || parent.passportIssuancePlace != null)
        {
            sb.append("выдан")
            if (null != parent.passportIssuanceDate)
                sb.append(' ').append(DateFormatter.DEFAULT_DATE_FORMATTER.format(parent.getPassportIssuanceDate()));

            if (StringUtils.isNotEmpty(parent.getPassportIssuancePlace()))
            {
                sb.append(' ').append(parent.getPassportIssuancePlace());
                if (StringUtils.isNotEmpty(parent.getPassportIssuanceCode()))
                    sb.append(" (").append(parent.getPassportIssuanceCode()).append(')');
            }
        }
        im.put('identityCardPlaceAndDate', sb.toString())

        def personAddress = parent.address
        im.put('addressFact', personAddress != null ? personAddress.titleWithFlat : "")
        im.put('contactPhone', parent.phones == null ? "-" : parent.phones)
        im.put('email', "-")
        im.put('homePhone', "-")

        //данные абитуриента
        IdentityCard entrantCard = entrant.person.identityCard;

        def fio
        def identityCardDeclinability = IUniBaseDao.instance.get().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), entrantCard.id)
        if (identityCardDeclinability != null)
        {
            def lastName = entrantCard.lastName
            if (identityCardDeclinability.lastNameDeclinable)
                lastName = PersonManager.instance().declinationDao().getDeclinationLastName(lastName, GrammaCase.GENITIVE, entrant.person.isMale())

            def firstName = entrantCard.firstName
            if (identityCardDeclinability.firstNameDeclinable)
                firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(firstName, GrammaCase.GENITIVE, entrant.person.isMale())

            def middleName = entrantCard.middleName
            if (identityCardDeclinability.middleNameDeclinable)
                middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(middleName, GrammaCase.GENITIVE, entrant.person.isMale())

            def result = new ArrayList<String>()
            if (!StringUtils.isEmpty(lastName)) result.add(lastName)
            if (!StringUtils.isEmpty(firstName)) result.add(firstName)
            if (!StringUtils.isEmpty(middleName)) result.add(middleName)

            fio = StringUtils.join(result, ' ')

        } else
        {
            fio = PersonManager.instance().declinationDao().getDeclinationFIO(entrantCard, GrammaCase.GENITIVE)
        }

        im.put('FIO_G', fio)
        def entrantPassData = (entrantCard.cardType == null? "" : entrantCard.cardType.title) + " " + (entrantCard.seria != null ? "серия "+entrantCard.seria :"") + ", номер " + entrantCard.number;
        im.put('entrantIdentityCard', StringUtils.uncapitalize(entrantPassData))

        def entrantAddress = entrant.person.address
        im.put('entrantAddress', entrantAddress != null ? entrantAddress.titleWithFlat : "")

        RelationDegree reverseDegree = NarfuEntrantUtil.getReverseRelationDegree(parent.relationDegree.code, entrant.person.male);
        if (reverseDegree != null)
        {
            RelationDegreeDeclinable revDegreeDeclin = DataAccessServices.dao().get(RelationDegreeDeclinable.class, RelationDegreeDeclinable.parent(), reverseDegree);
            def degree = DeclinableManager.instance().dao().getPropertyValue(revDegreeDeclin, "parent.title", IUniBaseDao.instance.get().getByCode(InflectorVariant.class, InflectorVariantCodes.RU_GENITIVE));
            im.put('degreeG', StringUtils.uncapitalize(degree))
        }
        else
            im.put('degreeG', "")



        def regDate = entrantRequest.getRegDate()
        im.put("regDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT.format(regDate) + " года");

        // стандартные выходные параметры скрипта
        return [document: UniRtfUtil.toByteArray(template, im, tm, null),
                fileName: "Анкета родственника абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }
}