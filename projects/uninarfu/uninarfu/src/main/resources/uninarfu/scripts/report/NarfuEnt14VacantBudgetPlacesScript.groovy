/* $Id$ */
package uninarfu.scripts.report

import com.google.common.collect.Multimap
import com.google.common.collect.TreeMultimap
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.CellFormat
import jxl.format.Colour
import jxl.format.VerticalAlignment
import jxl.write.Label
import jxl.write.Number
import jxl.write.NumberFormat
import jxl.write.WritableCellFormat
import jxl.write.WritableFont
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook
import org.hibernate.Session
import org.tandemframework.core.entity.EntityBase
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.entity.catalog.DevelopForm
import ru.tandemservice.uni.entity.catalog.EducationLevels
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection
import ru.tandemservice.uniec.entity.entrant.Entrant
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2009
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013Group
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOkso
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectOksoGroup
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant


/**
 * @author Andrey Andreev
 * @since 28.09.2015
 */
return new NarfuEnt14VacantBudgetPlacesPrint(
        session: session,
        template: template,
        number: number,
        date: date,
        formativeOrgUnitIds: formativeOrgUnitIds,
        studentStatusIds: studentStatusIds,
        campaignsByCourse: campaignsByCourse,
        developFormId: developFormId
).print()

class NarfuEnt14VacantBudgetPlacesPrint
{
    Session session;
    byte[] template;
    Integer number;
    Date date;
    List<Long> formativeOrgUnitIds;
    List<Long> studentStatusIds;
    Multimap<Long, Long> campaignsByCourse;
    Long developFormId;

    final Comparator<EduOrgUnitDatum> eduOrgUnitDatumComparator = new Comparator<EduOrgUnitDatum>() {
        @Override
        int compare(EduOrgUnitDatum o1, EduOrgUnitDatum o2)
        {
            return o1.eduOrgId.compareTo(o2.eduOrgId);
        }
    }
    final Comparator<? extends EntityBase> idComparator = new Comparator<? extends EntityBase>() {
        @Override
        int compare(EntityBase o1, EntityBase o2)
        {
            return o1.id.compareTo(o2.id);
        }
    };
    Multimap<Long, Student> students = TreeMultimap.create(new Comparator<Long>() {
        @Override
        int compare(Long o1, Long o2)
        {
            return o1.compareTo(o2);
        }
    }, (Comparator<Student>) idComparator);


    ByteArrayOutputStream out = new ByteArrayOutputStream();

    def print()
    {
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
        WritableSheet sheet = workbook.createSheet("Информация о бюджетных местах", 0);

        initFormats();

        Set<Long> enrollmentCampaignIds = new TreeSet<>(campaignsByCourse.values());

        Set<EduOrgUnitDatum> eduOrgUnitData = new TreeSet<>(eduOrgUnitDatumComparator);
        eduOrgUnitData.addAll(getEduOrgUnitDataFromNew(session, enrollmentCampaignIds, formativeOrgUnitIds, developFormId));
        eduOrgUnitData.addAll(getEduOrgUnitDataFromOld(session, enrollmentCampaignIds, formativeOrgUnitIds, developFormId));
        if (eduOrgUnitData.isEmpty()) throw new ApplicationException("Нет данных для построения отчета.");

        fillPlans(eduOrgUnitData, campaignsByCourse, developFormId);

        for (Long courseId : campaignsByCourse.keySet()) {
            List<Student> courseStudents = getStudentsOld(session, courseId, campaignsByCourse.get(courseId).toList(), studentStatusIds);
            courseStudents.addAll(getStudentsNew(session, courseId, campaignsByCourse.get(courseId).toList(), studentStatusIds))
            for (Student student : courseStudents) {
                students.put(student.educationOrgUnit.id, student);
            }
        }

        Column<Row> table = prepareColumn();
        table.fillValues(prepareRows(eduOrgUnitData));
        table.print(sheet, 0, 4);

        Column.addTitleCell(sheet, 0, 0, table.fullWidth, 1, "Информация о вакантных бюджетных местах в высших учебных заведениях",
                            STYLE_TABLE_BODY_BOLD);
        StringBuilder subTitle = new StringBuilder()
                .append(((DevelopForm) session.get(DevelopForm.class, developFormId)).title)
                .append(" форма обучения (данные на ")
                .append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(date))
                .append(")");
        Column.addTitleCell(sheet, 0, 1, table.fullWidth, 1, subTitle.toString(), STYLE_TABLE_BODY);

        workbook.write();
        workbook.close();

        String date = RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(date);
        String fileName = "Отчет № " + String.valueOf(number) + " от " + date;

        return [document: out.toByteArray(),
                fileName: fileName]
    }

    /**
     * Заполение плановых мест в коллекции специальностей
     */
    void fillPlans(Set<EduOrgUnitDatum> list, Multimap<Long, Long> campaignsByCourse, Long developFormId)
    {
        TreeMap<Long, EduOrgUnitDatum> eduOrgUnitData = new TreeMap<>();
        for (EduOrgUnitDatum eduOrgUnitDatum : list) {
            eduOrgUnitData.put(eduOrgUnitDatum.eduOrgId, eduOrgUnitDatum);
        }

        for (Long courseId : campaignsByCourse.keySet()) {
            List<Object[]> programSetOrgUnitData = getPlansNew(session, campaignsByCourse.get(courseId), developFormId)
            for (Object[] proSetOUData : programSetOrgUnitData) {
                EnrProgramSetOrgUnit progSetOrgUnit = (EnrProgramSetOrgUnit) proSetOUData[0];
                Long eduOUId = (Long) proSetOUData[1];

                EduOrgUnitDatum eduOrgUnitDatum = eduOrgUnitData.get(eduOUId);
                if (eduOrgUnitDatum != null)
                    incCount(courseId, eduOrgUnitDatum.coursePlan, progSetOrgUnit.ministerialPlan);
            }

            List<Object[]> enrollmentDirectionData = getPlansOld(session, campaignsByCourse.get(courseId), developFormId)
            for (Object[] directionData : enrollmentDirectionData) {
                EnrollmentDirection direction = (EnrollmentDirection) directionData[0];
                Long eduOUId = (Long) directionData[1];

                EduOrgUnitDatum eduOrgUnitDatum = eduOrgUnitData.get(eduOUId);
                if (eduOrgUnitDatum != null)
                    incCount(courseId, eduOrgUnitDatum.coursePlan, direction.ministerialPlan);
            }
        }
    }

    /**
     * Подготовка строк таблицы
     */
    ArrayList<Row> prepareRows(Set<EduOrgUnitDatum> eduOrgUnitData)
    {
        Multimap<EduProgramSubjectOksoGroup, EduOrgUnitDatum> GroupsOKSO =
                TreeMultimap.create((Comparator<EduProgramSubjectOksoGroup>) idComparator, eduOrgUnitDatumComparator);
        Multimap<EduProgramSubject2013Group, EduOrgUnitDatum> Groups2013 =
                TreeMultimap.create((Comparator<EduProgramSubject2013Group>) idComparator, eduOrgUnitDatumComparator);
        List<EduOrgUnitDatum> datasWithoutGroup = new ArrayList<>();

        for (EduOrgUnitDatum data : eduOrgUnitData) {
            if (data.programSubject instanceof EduProgramSubjectOkso) {
                GroupsOKSO.put(((EduProgramSubjectOkso) data.programSubject).item.group, data);
            } else if (data.programSubject instanceof EduProgramSubject2009) {
                GroupsOKSO.put(((EduProgramSubject2009) data.programSubject).group, data);
            } else if (data.programSubject instanceof EduProgramSubject2013) {
                Groups2013.put(((EduProgramSubject2013) data.programSubject).group, data);
            } else {
                datasWithoutGroup.add(data);
            }
        }

        ArrayList<Row> result = new ArrayList<>();

        for (EduProgramSubjectOksoGroup group : GroupsOKSO.keySet()) {
            result.addAll(prepareGroup(group.title, group.code, GroupsOKSO.get(group).toList()))
        }
        for (EduProgramSubject2013Group group : Groups2013.keySet()) {
            result.addAll(prepareGroup(group.title, group.code, Groups2013.get(group).toList()))
        }
        for (EduOrgUnitDatum dataWithoutGroup : datasWithoutGroup) {
            result.addAll(prepareProgramSubjects(Arrays.asList(dataWithoutGroup)))
        }

        return result;
    }

    /**
     * Группа строк таблицы для Укрупненой группы специальностей
     */
    ArrayList<Row> prepareGroup(String groupTitle, String groupCode, List<EduOrgUnitDatum> eduOrgUnitData)
    {
        ArrayList<Row> rows = prepareProgramSubjects(eduOrgUnitData);

        Row headRow = new Row(groupTitle, groupCode, Row.Type.Group);
        for (Row row : rows) {
            if (row.type == Row.Type.ProSubj) {
                headRow.allPlan += row.allPlan;
                headRow.allVacant += row.allVacant;
                headRow.allBusy += row.allBusy;
                headRow.allStudents += row.allStudents;
                for (Long courseId : campaignsByCourse.keySet()) {
                    incCount(courseId, headRow.coursePlan, row.coursePlan.get(courseId));
                    incCount(courseId, headRow.courseVacant, row.courseVacant.get(courseId));
                    incCount(courseId, headRow.courseBusy, row.courseBusy.get(courseId));
                    incCount(courseId, headRow.courseStudents, row.courseStudents.get(courseId));
                }
            }
        }

        rows.add(0, headRow);

        return rows;
    }

    /**
     * Группа строк таблицы для списка специальностей
     */
    ArrayList<Row> prepareProgramSubjects(List<EduOrgUnitDatum> eduOrgUnitData)
    {
        Row bachelorRow = new Row("бакалавриат", "", Row.Type.Bachelor);
        Row specialtyRow = new Row("специалитет", "", Row.Type.Specialty);
        Row masterRow = new Row("магистратура", "", Row.Type.Master);

        List<Row> progRows = new ArrayList<>();
        for (EduOrgUnitDatum eduOrgUnitDatum : eduOrgUnitData) {

            Row programRow = new Row(eduOrgUnitDatum.programSubject.title,
                                     eduOrgUnitDatum.code,
                                     Row.Type.ProSubj);
            programRow.orgUnit = eduOrgUnitDatum.formativeOrgUnit;
            programRow.period = eduOrgUnitDatum.developPeriod;
            programRow.condition = eduOrgUnitDatum.developCondition;


            for (Student student : students.get(eduOrgUnitDatum.eduOrgId)) {
                Long courseId = student.course.id;

                if (student.compensationType.code == CompensationTypeCodes.COMPENSATION_TYPE_BUDGET) {

                    incCount(courseId, programRow.courseBusy, 1);
                    if (eduOrgUnitDatum.levelType.isBachelor())
                        incCount(courseId, bachelorRow.courseBusy, 1);
                    else if (eduOrgUnitDatum.levelType.isSpecialty())
                        incCount(courseId, specialtyRow.courseBusy, 1);
                    else if (eduOrgUnitDatum.levelType.isMaster())
                        incCount(courseId, masterRow.courseBusy, 1);
                }

                incCount(courseId, programRow.courseStudents, 1);
                if (eduOrgUnitDatum.levelType.isBachelor())
                    incCount(courseId, bachelorRow.courseStudents, 1);
                else if (eduOrgUnitDatum.levelType.isSpecialty())
                    incCount(courseId, specialtyRow.courseStudents, 1);
                else if (eduOrgUnitDatum.levelType.isMaster())
                    incCount(courseId, masterRow.courseStudents, 1);
            }

            for (Long courseId : campaignsByCourse.keySet()) {
                Integer plan = eduOrgUnitDatum.coursePlan.get(courseId);
                incCount(courseId, programRow.coursePlan, plan);
                if (eduOrgUnitDatum.levelType.isBachelor())
                    incCount(courseId, bachelorRow.coursePlan, plan);
                else if (eduOrgUnitDatum.levelType.isSpecialty())
                    incCount(courseId, specialtyRow.coursePlan, plan);
                else if (eduOrgUnitDatum.levelType.isMaster())
                    incCount(courseId, masterRow.coursePlan, plan);
            }

            for (Long courseId : campaignsByCourse.keySet()) {
                programRow.courseVacant.put(courseId, calcVacantCount(
                        programRow.coursePlan.get(courseId),
                        programRow.courseBusy.get(courseId)));
                bachelorRow.courseVacant.put(courseId, calcVacantCount(
                        bachelorRow.coursePlan.get(courseId),
                        bachelorRow.courseBusy.get(courseId)));
                specialtyRow.courseVacant.put(courseId, calcVacantCount(
                        specialtyRow.coursePlan.get(courseId),
                        specialtyRow.courseBusy.get(courseId)));
                masterRow.courseVacant.put(courseId, calcVacantCount(
                        masterRow.coursePlan.get(courseId),
                        masterRow.courseBusy.get(courseId)));
            }

            programRow.allPlan = calcSumCount(programRow.coursePlan);
            programRow.allBusy = calcSumCount(programRow.courseBusy);
            programRow.allVacant = programRow.allPlan - programRow.allBusy;
            programRow.allStudents = calcSumCount(programRow.courseStudents);

            bachelorRow.allPlan = calcSumCount(bachelorRow.coursePlan);
            bachelorRow.allBusy = calcSumCount(bachelorRow.courseBusy);
            bachelorRow.allVacant = bachelorRow.allPlan - bachelorRow.allBusy;
            bachelorRow.allStudents = calcSumCount(bachelorRow.courseStudents);

            specialtyRow.allPlan = calcSumCount(specialtyRow.coursePlan);
            specialtyRow.allBusy = calcSumCount(specialtyRow.courseBusy);
            specialtyRow.allVacant = specialtyRow.allPlan - specialtyRow.allBusy;
            specialtyRow.allStudents = calcSumCount(specialtyRow.courseStudents);

            masterRow.allPlan = calcSumCount(masterRow.coursePlan);
            masterRow.allBusy = calcSumCount(masterRow.courseBusy);
            masterRow.allVacant = masterRow.allPlan - masterRow.allBusy;
            masterRow.allStudents = calcSumCount(masterRow.courseStudents);

            progRows.add(programRow);
        }

        Collections.sort(progRows, new Comparator<Row>() {
            @Override
            int compare(Row o1, Row o2)
            {
                return o1.code.compareTo(o2.code);
            }
        })

        ArrayList<Row> result = new ArrayList<>();
        result.addAll(progRows);
        result.add(bachelorRow);
        result.add(specialtyRow);
        result.add(masterRow);

        return result;
    }

    /**
     * Увеличение количества учащихся на курсе
     */
    static void incCount(Long courseId, TreeMap<Long, Integer> courseCount, Integer inc)
    {
        if (inc == null) return;
        Integer count = courseCount.get(courseId);
        if (count == null) count = 0;
        courseCount.put(courseId, count + inc);
    }

    /**
     * Подсчет общего количества учащихся
     */
    static Integer calcSumCount(TreeMap<Long, Integer> courseCount)
    {
        Integer sum = 0;
        for (Integer count : courseCount.values()) {
            sum += count;
        }
        return sum;
    }

    /**
     * Расчет вакантного количества учащихся
     */
    static Integer calcVacantCount(Integer plan, Integer busy)
    {
        if (plan == null) plan = 0;
        if (busy == null) busy = 0;
        return plan - busy;
    }


    //Table
    Column<Row> prepareColumn()
    {
        Collection<Long> courseIds = campaignsByCourse.keySet();
        Map<Long, Course> courses = new TreeMap<>();
        for (Course course : getCourses(session, courseIds)) {
            courses.put(course.id, course);
        }


        Column<Row> table = new Column<>(null, 0, STYLE_TABLE_BODY);

        Column<Row> progTitle_Col = new Column(70, "Перечень укрупненных групп специальностей", 3, 250,
                                               STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY_LEFT);
        progTitle_Col.fillValue = { Row row -> addValueWithLeftBorder2Column(progTitle_Col, row.programSubjectTitle, row.type) }
        table.addColumn(progTitle_Col);

        Column<Row> progCode_Col = new Column(14, "Код спец\u2011ти/ направления", 3, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        progCode_Col.fillValue = { Row row -> addValue2Column(progCode_Col, row.code, row.type) }
        table.addColumn(progCode_Col);

        Column<Row> fOrgUnit_Col = new Column(16, "Формирующие подразделение", 3, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        fOrgUnit_Col.fillValue = { Row row -> addValue2Column(fOrgUnit_Col, row.orgUnit, row.type) }
        table.addColumn(fOrgUnit_Col);

        Column<Row> period_Col = new Column(14, "Срок обучения", 3, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        period_Col.fillValue = { Row row -> addValue2Column(period_Col, row.period, row.type) }
        table.addColumn(period_Col);

        Column<Row> cond_Col = new Column(20, "Условие освоения", 3, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        cond_Col.fillValue = { Row row -> addValue2Column(cond_Col, row.condition, row.type) }
        table.addColumn(cond_Col);

        Column<Row> plan_Col = new Column<>("Кол-во бюджетных мест по плану", 2, STYLE_TABLE_HEAD_NORM);
        table.addColumn(plan_Col);

        Column<Row> allPlan_SubCol = new Column(10, "Всего", 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        allPlan_SubCol.fillValue = { Row row -> addValueWithLeftBorder2Column(allPlan_SubCol, row.allPlan, row.type) }
        plan_Col.addColumn(allPlan_SubCol);

        Column<Row> vacant_Col = new Column<>("Кол-во вакантных бюджетных мест", 2, STYLE_TABLE_HEAD_NORM);
        table.addColumn(vacant_Col);

        Column<Row> allVacant_SubCol = new Column(10, "Всего", 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        allVacant_SubCol.fillValue = { Row row -> addValueWithLeftBorder2Column(allVacant_SubCol, row.allVacant, row.type) }
        vacant_Col.addColumn(allVacant_SubCol);

        Column<Row> busy_Col = new Column<>("Кол-во студентов, обучающихся за счет средств федерального бюджета", 2, STYLE_TABLE_HEAD_NORM);
        table.addColumn(busy_Col);

        Column<Row> allBusy_SubCol = new Column(10, "Всего", 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        allBusy_SubCol.fillValue = { Row row -> addValueWithLeftBorder2Column(allBusy_SubCol, row.allBusy, row.type) }
        busy_Col.addColumn(allBusy_SubCol);

        Column<Row> students_Col = new Column<>("Кол-во всех студентов", 2, STYLE_TABLE_HEAD_NORM);
        table.addColumn(students_Col);

        Column<Row> allStudents_SubCol = new Column(10, "Всего", 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
        allStudents_SubCol.fillValue = { Row row -> addValueWithLeftBorder2Column(allStudents_SubCol, row.allStudents, row.type) }
        students_Col.addColumn(allStudents_SubCol);


        Iterator cIter = courseIds.iterator();
        while (cIter.hasNext()) {
            final Long courseId = cIter.next();
            final boolean hasNextCourse = cIter.hasNext();
            final String courseTitle = "Курс " + courses.get(courseId).title;

            Column<Row> coursePlan_SubCol = new Column(10, courseTitle, 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
            coursePlan_SubCol.fillValue =
                    { Row row -> addCourseValue(coursePlan_SubCol, row.coursePlan.get(courseId), hasNextCourse, row.type); }
            plan_Col.addColumn(coursePlan_SubCol);

            Column<Row> courseVacant_SubCol = new Column(10, courseTitle, 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
            courseVacant_SubCol.fillValue =
                    { Row row -> addCourseValue(courseVacant_SubCol, row.courseVacant.get(courseId), hasNextCourse, row.type); }
            vacant_Col.addColumn(courseVacant_SubCol);

            Column<Row> courseBusy_SubCol = new Column(10, courseTitle, 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
            courseBusy_SubCol.fillValue =
                    { Row row -> addCourseValue(courseBusy_SubCol, row.courseBusy.get(courseId), hasNextCourse, row.type); }
            busy_Col.addColumn(courseBusy_SubCol);

            Column<Row> courseStudents_SubCol = new Column(10, courseTitle, 1, 250, STYLE_TABLE_HEAD_NORM, STYLE_TABLE_BODY);
            courseStudents_SubCol.fillValue =
                    { Row row -> addCourseValue(courseStudents_SubCol, row.courseStudents.get(courseId), hasNextCourse, row.type); }
            students_Col.addColumn(courseStudents_SubCol);
        }

        return table;
    }

    void addValueWithLeftBorder2Column(Column column, Object value, Row.Type type)
    {
        switch (type) {
            case Row.Type.ProSubj:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_LEFT));
                break;
            case Row.Type.Group:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_LEFT_BOLD, 0, 500));
                break;
            case Row.Type.Bachelor:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_LEFT_DARK));
                break;
            case Row.Type.Specialty:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_LEFT_DARK));
                break;
            case Row.Type.Master:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_LEFT_BOTTOM_DARK));
                break;
        }
    }

    void addValue2Column(Column column, Object value, Row.Type type)
    {
        switch (type) {
            case Row.Type.ProSubj:
                column.addValue(value);
                break;
            case Row.Type.Group:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_BOLD, 0, 500));
                break;
            case Row.Type.Bachelor:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_DARK));
                break;
            case Row.Type.Specialty:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_DARK));
                break;
            case Row.Type.Master:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_BOTTOM_DARK));
                break;
        }
    }

    void addValueWithRightBorder2Column(Column column, Object value, Row.Type type)
    {
        switch (type) {
            case Row.Type.ProSubj:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_RIGHT));
                break;
            case Row.Type.Group:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_RIGHT_BOLD, 0, 500));
                break;
            case Row.Type.Bachelor:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_RIGHT_DARK));
                break;
            case Row.Type.Specialty:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_RIGHT_DARK));
                break;
            case Row.Type.Master:
                column.addValue(new Column.Value(value, STYLE_TABLE_BODY_RIGHT_BOTTOM_DARK));
                break;
        }
    }

    void addCourseValue(Column column, Integer value, boolean hasNextCourse, Row.Type type)
    {
        if (value == null) value = 0;
        if (!hasNextCourse) addValueWithRightBorder2Column(column, value, type);
        else addValue2Column(column, value, type);
    }


    //DAO
    static List<EduOrgUnitDatum> getEduOrgUnitDataFromNew(
            Session session, Collection<Long> enrollmentCampaignIds, List<Long> orgUnitIds, Long developFormId)
    {
        String eou_alias = "eou";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, eou_alias);

        dql.where(DQLExpressions.eq(DQLExpressions.property(eou_alias, EducationOrgUnit.developForm().id()),
                                    DQLExpressions.value(developFormId)))
        CommonBaseFilterUtil.applySelectFilter(dql, eou_alias, EducationOrgUnit.formativeOrgUnit().id(), orgUnitIds)


        String psou_alias = "psou";
        dql.fromEntity(EnrProgramSetOrgUnit.class, psou_alias);

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(psou_alias, EnrProgramSetOrgUnit.educationOrgUnit()),
                DQLExpressions.property(eou_alias)))
           .where(DQLExpressions.in(
                DQLExpressions.property(psou_alias, EnrProgramSetOrgUnit.orgUnit().enrollmentCampaign().id()),
                enrollmentCampaignIds))

        //              0
        dql.column(DQLExpressions.property(eou_alias, EducationOrgUnit.id()))
        //              1
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()))
        //              2
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.formativeOrgUnit().shortTitle()))
        //              3
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.developPeriod().title()))
        //              4
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.developCondition().title()))
        //              5
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType()))
        //              6
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.educationLevelHighSchool().educationLevel()))

        List<EduOrgUnitDatum> data = new ArrayList<>();
        for (Object[] raw : dql.createStatement(session).list()) {
            data.add(new EduOrgUnitDatum(raw));
        }

        return data;
    }

    static List<EduOrgUnitDatum> getEduOrgUnitDataFromOld(
            Session session, Collection<Long> enrollmentCampaignIds, List<Long> orgUnitIds, Long developFormId)
    {
        String eou_alias = "eou";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, eou_alias);

        dql.where(DQLExpressions.eq(DQLExpressions.property(eou_alias, EducationOrgUnit.developForm().id()),
                                    DQLExpressions.value(developFormId)))
        CommonBaseFilterUtil.applySelectFilter(dql, eou_alias, EducationOrgUnit.formativeOrgUnit().id(), orgUnitIds)


        String ed_alias = "ed";
        dql.fromEntity(EnrollmentDirection.class, ed_alias);

        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ed_alias, EnrollmentDirection.educationOrgUnit()),
                DQLExpressions.property(eou_alias)))
           .where(DQLExpressions.in(
                DQLExpressions.property(ed_alias, EnrollmentDirection.enrollmentCampaign().id()),
                enrollmentCampaignIds))


        //              0
        dql.column(DQLExpressions.property(eou_alias, EducationOrgUnit.id()))
        //              1
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.educationLevelHighSchool().educationLevel().eduProgramSubject()))
        //              2
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.formativeOrgUnit().shortTitle()))
        //              3
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.developPeriod().title()))
        //              4
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.developCondition().title()))
        //              5
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType()))
        //              6
           .column(DQLExpressions.property(eou_alias, EducationOrgUnit.educationLevelHighSchool().educationLevel()));

        List<EduOrgUnitDatum> data = new ArrayList<>();
        for (Object[] raw : dql.createStatement(session).list()) {
            data.add(new EduOrgUnitDatum(raw));
        }

        return data;
    }

    static List<Object[]> getPlansNew(Session session, Collection<Long> enrollmentCampaignIds, Long developFormId)
    {
        String psou_alias = "psou";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrProgramSetOrgUnit.class, psou_alias);

        dql.where(DQLExpressions.in(
                DQLExpressions.property(psou_alias, EnrProgramSetOrgUnit.orgUnit().enrollmentCampaign().id()),
                enrollmentCampaignIds))
           .where(DQLExpressions.eq(
                DQLExpressions.property(psou_alias, EnrProgramSetOrgUnit.educationOrgUnit().developForm()),
                DQLExpressions.value(developFormId)));

        //              0
        dql.column(DQLExpressions.property(psou_alias))
        //              1
           .column(DQLExpressions.property(psou_alias, EnrProgramSetOrgUnit.educationOrgUnit().id()))

        return dql.createStatement(session).list();
    }

    static List<Object[]> getPlansOld(Session session, Collection<Long> enrollmentCampaignIds, Long developFormId)
    {
        String ed_alias = "ed";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, ed_alias);

        dql.where(DQLExpressions.in(
                DQLExpressions.property(ed_alias, EnrollmentDirection.enrollmentCampaign().id()),
                enrollmentCampaignIds))
           .where(DQLExpressions.eq(
                DQLExpressions.property(ed_alias, EnrollmentDirection.educationOrgUnit().developForm()),
                DQLExpressions.value(developFormId)));

        //              0
        dql.column(DQLExpressions.property(ed_alias))
        //              1
           .column(DQLExpressions.property(ed_alias, EnrollmentDirection.educationOrgUnit().id()))

        return dql.createStatement(session).list();
    }

    static List<Student> getStudentsOld(Session session, Long courseId, List<Long> enrollmentCampaignIds, List<Long> studentStatusIds)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();

        String stud_alias = "stud";
        dql.fromEntity(Student.class, stud_alias)
           .where(DQLExpressions.eq(DQLExpressions.property(stud_alias, Student.course().id()), DQLExpressions.value(courseId)))
           .where(DQLExpressions.in(DQLExpressions.property(stud_alias, Student.status().id()), studentStatusIds));


        String entr_alias = "entr";
        dql.joinEntity(stud_alias, DQLJoinType.inner, Entrant.class, entr_alias,
                       DQLExpressions.eq(DQLExpressions.property(stud_alias, Student.person()),
                                         DQLExpressions.property(entr_alias, Entrant.person())))
           .where(DQLExpressions.in(DQLExpressions.property(entr_alias, Entrant.enrollmentCampaign().id()), enrollmentCampaignIds))

        dql.column(DQLExpressions.property(stud_alias));

        return dql.createStatement(session).list();
    }

    static List<Student> getStudentsNew(Session session, Long courseId, List<Long> enrollmentCampaignIds, List<Long> studentStatusIds)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();

        String stud_alias = "stud";
        dql.fromEntity(Student.class, stud_alias)
           .where(DQLExpressions.eq(DQLExpressions.property(stud_alias, Student.course().id()), DQLExpressions.value(courseId)))
           .where(DQLExpressions.in(DQLExpressions.property(stud_alias, Student.status().id()), studentStatusIds));

        String entr_alias = "entr";
        dql.joinEntity(stud_alias, DQLJoinType.inner, EnrEntrant.class, entr_alias,
                       DQLExpressions.eq(DQLExpressions.property(stud_alias, Student.person()),
                                         DQLExpressions.property(entr_alias, EnrEntrant.person())))
           .where(DQLExpressions.in(DQLExpressions.property(entr_alias, EnrEntrant.enrollmentCampaign().id()), enrollmentCampaignIds))

        dql.column(DQLExpressions.property(stud_alias));

        return dql.createStatement(session).list();
    }

    static List<Course> getCourses(Session session, Collection<Long> ids)
    {
        return (new DQLSelectBuilder())
                .fromEntity(Course.class, "couse")
                .where(DQLExpressions.in(DQLExpressions.property("couse"), ids))
                .column(DQLExpressions.property("couse"))
                .createStatement(session).list();
    }


    //шрифты и форматы
    final WritableFont FONT_NORM = new WritableFont(WritableFont.ARIAL, 10);
    final WritableFont FONT_BOLD = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

    final NumberFormat NUMBER_FORMAT = new NumberFormat("0");

    final WritableCellFormat STYLE_TABLE_HEAD_NORM = new WritableCellFormat(FONT_BOLD);
    final WritableCellFormat STYLE_TABLE_BODY = new WritableCellFormat(FONT_NORM, NUMBER_FORMAT);
    final WritableCellFormat STYLE_TABLE_BODY_BOLD = new WritableCellFormat(FONT_BOLD, NUMBER_FORMAT);
    final WritableCellFormat STYLE_TABLE_BODY_LEFT = new WritableCellFormat(STYLE_TABLE_BODY);
    final WritableCellFormat STYLE_TABLE_BODY_LEFT_BOLD = new WritableCellFormat(STYLE_TABLE_BODY_BOLD);
    final WritableCellFormat STYLE_TABLE_BODY_RIGHT = new WritableCellFormat(STYLE_TABLE_BODY);
    final WritableCellFormat STYLE_TABLE_BODY_RIGHT_BOLD = new WritableCellFormat(STYLE_TABLE_BODY_BOLD);
    final WritableCellFormat STYLE_TABLE_BODY_DARK = new WritableCellFormat(STYLE_TABLE_BODY);
    final WritableCellFormat STYLE_TABLE_BODY_LEFT_DARK = new WritableCellFormat(STYLE_TABLE_BODY);
    final WritableCellFormat STYLE_TABLE_BODY_RIGHT_DARK = new WritableCellFormat(STYLE_TABLE_BODY);
    final WritableCellFormat STYLE_TABLE_BODY_BOTTOM_DARK = new WritableCellFormat(STYLE_TABLE_BODY);
    final WritableCellFormat STYLE_TABLE_BODY_LEFT_BOTTOM_DARK = new WritableCellFormat(STYLE_TABLE_BODY_BOTTOM_DARK);
    final WritableCellFormat STYLE_TABLE_BODY_RIGHT_BOTTOM_DARK = new WritableCellFormat(STYLE_TABLE_BODY_BOTTOM_DARK);

    private void initFormats()
    {
        STYLE_TABLE_HEAD_NORM.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_HEAD_NORM.setAlignment(Alignment.CENTRE);
        STYLE_TABLE_HEAD_NORM.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
        STYLE_TABLE_HEAD_NORM.setWrap(true);

        STYLE_TABLE_BODY.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY.setWrap(true);

        STYLE_TABLE_BODY_BOLD.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_BOLD.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_BOLD.setWrap(true);

        STYLE_TABLE_BODY_LEFT.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_LEFT.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_LEFT.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);

        STYLE_TABLE_BODY_RIGHT.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_RIGHT.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_RIGHT.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);

        STYLE_TABLE_BODY_LEFT_BOLD.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_LEFT_BOLD.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_LEFT_BOLD.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_LEFT_BOLD.setWrap(true);

        STYLE_TABLE_BODY_RIGHT_BOLD.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_RIGHT_BOLD.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_RIGHT_BOLD.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_RIGHT_BOLD.setWrap(true);

        STYLE_TABLE_BODY_DARK.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_DARK.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_DARK.setBackground(Colour.GREY_25_PERCENT);

        STYLE_TABLE_BODY_LEFT_DARK.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_LEFT_DARK.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_LEFT_DARK.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_LEFT_DARK.setBackground(Colour.GREY_25_PERCENT);

        STYLE_TABLE_BODY_RIGHT_DARK.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_RIGHT_DARK.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_RIGHT_DARK.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_RIGHT_DARK.setBackground(Colour.GREY_25_PERCENT);

        STYLE_TABLE_BODY_BOTTOM_DARK.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_BOTTOM_DARK.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_BOTTOM_DARK.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_BOTTOM_DARK.setBackground(Colour.GREY_25_PERCENT);

        STYLE_TABLE_BODY_LEFT_BOTTOM_DARK.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_LEFT_BOTTOM_DARK.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_LEFT_BOTTOM_DARK.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_LEFT_BOTTOM_DARK.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_LEFT_BOTTOM_DARK.setBackground(Colour.GREY_25_PERCENT);

        STYLE_TABLE_BODY_RIGHT_BOTTOM_DARK.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TABLE_BODY_RIGHT_BOTTOM_DARK.setBorder(Border.ALL, BorderLineStyle.DOTTED);
        STYLE_TABLE_BODY_RIGHT_BOTTOM_DARK.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_RIGHT_BOTTOM_DARK.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
        STYLE_TABLE_BODY_RIGHT_BOTTOM_DARK.setBackground(Colour.GREY_25_PERCENT);
    }

    static class EduOrgUnitDatum
    {
        //0
        Long eduOrgId;
        //1
        EduProgramSubject programSubject;
        //2
        String formativeOrgUnit;
        //3
        String developPeriod;
        //4
        String developCondition;
        //5
        StructureEducationLevels levelType;
        //6
        String code;

        TreeMap<Long, Integer> coursePlan = new TreeMap<>();

        EduOrgUnitDatum(Object[] row)
        {
            eduOrgId = (Long) row[0];
            programSubject = (EduProgramSubject) row[1];
            formativeOrgUnit = (String) row[2];
            developPeriod = (String) row[3];
            developCondition = (String) row[4];
            levelType = (StructureEducationLevels) row[5];
            code = ((EducationLevels) row[6]).titleCodePrefix;
        }
    }
}

class Column<T>
{

    int startWidth = 0;
    String title;
    int titleHeight = 0;
    int valueHeight = 0;
    WritableCellFormat titleFormat;
    WritableCellFormat valueFormat;
    List<Column<T>> columns = new ArrayList<>();
    List<Object> values = new ArrayList<>();
    Closure fillValue = { T any ->
        if (columns.isEmpty()) {
            values.add(any);
        } else
            for (Column<T> column : columns) {
                column.fillValue(any);
            }
    };
    int fullWidth = 0;
    int fullHeight = 0;


    Column(String title, int titleHeight, WritableCellFormat titleFormat)
    {
        this.title = title;
        this.titleHeight = titleHeight;
        this.titleFormat = titleFormat;
    }

    Column(int startWidth,
           String title, int titleHeight, int valueHeight,
           WritableCellFormat titleFormat, WritableCellFormat valueFormat
          )
    {
        this.startWidth = startWidth
        this.title = title
        this.titleHeight = titleHeight
        this.valueHeight = valueHeight
        this.titleFormat = titleFormat
        this.valueFormat = valueFormat
    }

    public boolean addColumn(Column<T> c)
    {
        return columns.add(c);
    }

    public boolean addValue(Object o)
    {
        return values.add(o);
    }

    void fillValues(Collection<T> rows)
    {
        for (T row : rows) this.fillValue(row);
    }

    int getTitleHeight()
    {
        int height = 0;
        if (!columns.isEmpty()) {
            for (Column column : columns) {
                height = Math.max(height, column.getTitleHeight())
            };
        }

        return height + titleHeight;
    }

    void print(WritableSheet sheet, int colIndex, int rowIndex)
    {
        fullWidth = 0;

        int subColumnsHeight = 0;
        for (Column column : columns) {
            if (column == null) continue;
            column.print(sheet, colIndex + fullWidth, rowIndex + titleHeight);
            fullWidth += column.fullWidth;
            subColumnsHeight = Math.max(subColumnsHeight, column.fullHeight);
        }

        int valuesHeight = getTitleHeight();
        if (valueHeight > 0 && startWidth > 0) {
            for (Object value : values) {
                printValue(sheet, colIndex + fullWidth, rowIndex + valuesHeight, value)
                valuesHeight++;
            }
            if (!values.isEmpty()) fullWidth++;
        }

        if (fullWidth == 0) fullWidth = 1;

        if (title != null && titleHeight > 0)
            addTitleCell(sheet, colIndex, rowIndex, fullWidth, titleHeight, title, titleFormat);

        fullHeight = Math.max(subColumnsHeight + titleHeight, valuesHeight);
    }

    private void printValue(WritableSheet sheet, int left, int top, Object value)
    {
        if (value != null) {
            if (value instanceof Value) {
                if (value.width <= 0) value.width = startWidth;
                if (value.height <= 0) value.height = valueHeight;
                printValue(sheet, left, top, value.value, value.width, value.height, value.format)
            } else printValue(sheet, left, top, value, startWidth, valueHeight, valueFormat);
        } else addTextCell(sheet, left, top, startWidth, valueHeight, "", valueFormat);
    }

    private static void printValue(WritableSheet sheet, int left, int top, Object value, int width, int height,
                                   WritableCellFormat format)
    {
        if (value != null) {
            if (value instanceof String)
                addTextCell(sheet, left, top, width, height, value, format);
            else if (value instanceof Double)
                addNumberCell(sheet, left, top, width, height, value, format);
            else if (value instanceof Integer)
                addNumberCell(sheet, left, top, width, height, value, format);
            else
                addTextCell(sheet, left, top, width, height, value.toString(), format);
        } else addTextCell(sheet, left, top, width, height, "", format);
    }

    public static void addTextCell(WritableSheet sheet, int left, int top, int width, int height, String text, CellFormat format)
    {
        sheet.addCell(new Label(left, top, text, format));
        if (width != 0) sheet.setColumnView(left, width);
        if (height != 0) sheet.setRowView(top, height);
    }

    public static void addNumberCell(WritableSheet sheet, int left, int top, int width, int height, double number, CellFormat format)
    {
        sheet.addCell(new Number(left, top, number, format));
        if (width != 0) sheet.setColumnView(left, width);
        if (height != 0) sheet.setRowView(top, height);
    }

    public static void addTitleCell(WritableSheet sheet, int left, int top, int width, int height, String text, CellFormat format)
    {
        sheet.addCell(new Label(left, top, text, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }

    public static class Value
    {
        WritableCellFormat format;
        Object value;
        int width = 0;
        int height = 0;

        Value(Object value, WritableCellFormat format)
        {
            this.value = value;
            this.format = format;
        }

        Value(Object value, WritableCellFormat format, int width, int height)
        {
            this.value = value;
            this.format = format;
            this.width = width;
            this.height = height;
        }
    }
}

class Row
{
    String programSubjectTitle;
    String code;
    String orgUnit = "";
    String period = "";
    String condition = "";

    Integer allPlan = 0;
    TreeMap<Long, Integer> coursePlan = new TreeMap<>();
    Integer allVacant = 0;
    TreeMap<Long, Integer> courseVacant = new TreeMap<>();
    Integer allBusy = 0;
    TreeMap<Long, Integer> courseBusy = new TreeMap<>();
    Integer allStudents = 0;
    TreeMap<Long, Integer> courseStudents = new TreeMap<>();

    Type type;

    static enum Type {
        Group,
        ProSubj,
        Bachelor,
        Specialty,
        Master
    }

    Row(String programSubjectTitle, String code, Type type)
    {
        this.programSubjectTitle = programSubjectTitle
        this.code = code
        this.type = type
    }
}






