package uninarfu.scripts.entrant

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.fias.IKladrDefines
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramOrientationCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram

/**
 * @author DMITRY KNYAZEV
 * @since 11.06.2015
 */

return new NarfuEntrantCoverPrint(                                       // стандартные входные параметры скрипта
        session: session,                                           // сессия
        template: template,                                         // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object)   // объект печати
).print()

class NarfuEntrantCoverPrint {

    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    RtfInjectModifier im = new RtfInjectModifier()
    RtfTableModifier tm = new RtfTableModifier()

    def print() {
        // получаем список выбранных направлений приема для заявления
        List<EnrRequestedCompetition> requestedDirections = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, 'red')
                .where(DQLExpressions.eq(DQLExpressions.property("red", EnrRequestedCompetition.request().id()), DQLExpressions.value(entrantRequest.id)))
                .order(DQLExpressions.property("red", EnrRequestedCompetition.priority()))
                .createStatement(session).list()

        EnrRequestedCompetition requestedCompetition = null
        if (!requestedDirections.isEmpty()) {
            requestedCompetition = requestedDirections.find { it.state.code.equals(EnrEntrantStateCodes.ENROLLED) }
            if (requestedCompetition == null)
                requestedCompetition = requestedDirections.get(0)
        }

        EnrCompetition competition = requestedCompetition.competition
        EnrCompetitionType competitionType = competition.type
        EnrEntrant entrant = entrantRequest.entrant
        Person person = entrant.person
        IdentityCard card = person.identityCard

        //квота приема лиц, имеющих особое право;
        EnrCompetitionTypeCodes.EXCLUSIVE.equals(competitionType.code) ? im.put('Q', 'К') : im.put('Q', '')
        //целевой прием
        EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competitionType.code) ? im.put('T', 'Ц') : im.put('T', '')
        //иностранный гражданин
        card.citizenship.code != IKladrDefines.RUSSIA_COUNTRY_CODE ? im.put('Fr', 'Иг') : im.put('Fr', '')

        //Формирующеее подразделение
        im.put('formativeOrgUnit', competition.programSetOrgUnit.formativeOrgUnit.getTitle())
        //Направление/Программа
        im.put('direction', getDirectionString(competition))
        //Профиль/Программа
        im.put('specialisation', getSpecialisationString(requestedCompetition))

        //Форма освоения
        im.put('developForm', getDevelopFormString(competition).capitalize().concat(' форма обучения'))
        //Вид возмещения затрат
        im.put('compensationType', 'Обучение '.concat(competitionType.compensationType.title))

        //Номер дела
        im.put('number', entrant.personalNumber)

        //ФИО
        im.put('firstName', card.firstName)
        im.put('lastName', card.lastName)
        im.put('middleName', card.middleName)

        //Когда начато (дата)
        im.put('startDate', entrantRequest.regDate.format('dd.MM.yyyy'))

        return [document: RtfUtil.toByteArray(template, im, tm), fileName: "Cover.rtf"]
    }

    def private static String getDirectionString(EnrCompetition competition) {
        EnrProgramSetOrgUnit programSetOrgUnit = competition.programSetOrgUnit
        EduProgramSubject programSubject = programSetOrgUnit.programSet.programSubject

        EduProgramKind programKind = programSubject.getEduProgramKind()

        StringBuilder directionTitle = new StringBuilder();
        //'Специальность' для специалитета и для СПО
        if (programKind.isProgramSecondaryProf() || programKind.isProgramSpecialistDegree())
            directionTitle.append('Специальность ')
        else // 'Направление' для всех остальных
            directionTitle.append('Направление ')

        directionTitle.append(programSubject.titleWithCode)

        //выводить текст "прикладной бакалавриат", только если ориентация такая
        if (programSetOrgUnit.program!=null && EduProgramOrientationCodes.APPLIED_BACHELOR_2013 == programSetOrgUnit.program.programOrientation?.code)
            directionTitle.append(' (прикладной бакалавриат)')

        return directionTitle.toString();
    }

    def private String getSpecialisationString(EnrRequestedCompetition requestedCompetition) {
        if (requestedCompetition == null)
            return '';
        StringBuilder specialisationTitle = new StringBuilder();
        EduProgramKind programKind = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject.getEduProgramKind()
        //для магистратуры «программа»
        if (programKind.isProgramMasterDegree())
            specialisationTitle.append('Программа -')
        else // 'Профиль' для всех остальных
            specialisationTitle.append('Профиль -')

        String erpAlias = 'erp'
        EnrProgramSetItem programSetItem = new DQLSelectBuilder()
                .fromEntity(EnrRequestedProgram.class, erpAlias)
                .top(1)
                .where(DQLExpressions.eq(DQLExpressions.property(erpAlias, EnrRequestedProgram.requestedCompetition()), DQLExpressions.value(requestedCompetition)))
                .order(DQLExpressions.property(erpAlias, EnrRequestedProgram.priority()))
                .column(DQLExpressions.property(erpAlias, EnrRequestedProgram.programSetItem()))
                .createStatement(session).uniqueResult()

        if (programSetItem == null)
        {
            String psiAlias = 'psi'
            programSetItem = new DQLSelectBuilder()
                    .fromEntity(EnrProgramSetItem.class, psiAlias)
                    .top(1)
                    .where(DQLExpressions.eq(DQLExpressions.property(psiAlias, EnrProgramSetItem.programSet()),
                                             DQLExpressions.value(requestedCompetition.competition.programSetOrgUnit.programSet)))
                    .column(DQLExpressions.property(psiAlias))
                    .createStatement(session).uniqueResult()
        }

        if (programSetItem != null)
            specialisationTitle.append(' ').append(programSetItem.program.programSpecialization.title)

        return specialisationTitle.toString()
    }

    def private static String getDevelopFormString(EnrCompetition competition) {
        if (competition == null) return '';
        EduProgramForm programForm = competition.programSetOrgUnit.programSet.programForm

        return programForm.getTitle().toLowerCase();
    }
}
