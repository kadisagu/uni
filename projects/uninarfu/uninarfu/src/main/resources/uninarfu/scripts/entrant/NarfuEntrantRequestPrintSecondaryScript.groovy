package uninarfu.scripts.entrant

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.PersonEduDocument
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt
import ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen

/**
 * @author Ekaterina Zvereva
 * @since 27.04.2015
 */
return new NarfuEntrantRequestPrintSecondary(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()


class NarfuEntrantRequestPrintSecondary
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() // метки, строки с которыми необходимо удалить из таблицы

    def print()
    {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()} = ${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
        /).<EnrRequestedCompetition> list()

        fillCompetitionTable(directions)
        fillInjectParameters(directions)
        fillEnrollmentCommissionData()

        // стандартные выходные параметры скрипта
        return [document: UniRtfUtil.toByteArray(template, im, tm, deleteLabels),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }


    def fillCompetitionTable(List<EnrRequestedCompetition> enrRequestedCompetitions) {

        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }
        def dirMap = getDirMap(enrRequestedCompetitions)

        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort{e -> e.priority}


        // для каждого результата ЕГЭ
        for (def requestedCompetition : enrRequestedCompetitions)
        {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = requestedCompetition.priority
            EnrProgramSetBase programSet = requestedCompetition.competition.programSetOrgUnit.programSet

            def subject = programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")

            row[1] = programSubject
            row[2] = programSet.programForm.title.toLowerCase()
            row[3] = getPlaces(requestedCompetition.competition.type)
            row[4] = ((EnrProgramSetSecondary)programSet).program.duration.title
            rows.add(row)
        }

        tm.put('T1', rows as String[][])
    }

    static def getPlaces(EnrCompetitionType type) {
        if(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(type.code) ||
                EnrCompetitionTypeCodes.CONTRACT.equals(type.code))
            return "с полным возмещением затрат на обучение"
        else if(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(type.code) ||
                EnrCompetitionTypeCodes.MINISTERIAL.equals(type.code) )
            return "за счёт средств федерального бюджета"
        else if(EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(type.code))
            return "в пределах квоты целевого приема"
        else if(EnrCompetitionTypeCodes.EXCLUSIVE.equals(type.code))
            return "в пределах квоты приема лиц, имеющих особое право"
        return ""
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id};
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "ch")
        builder.where(DQLExpressions.in(DQLExpressions.property("ch", EnrRequestedProgram.requestedCompetition().id()), ids))

        def programs = builder.createStatement(session).<EnrRequestedProgram>list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for(EnrRequestedProgram program : programs)
        {
            Long id = program.requestedCompetition.id;
            if(!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if(!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear();
            }
        }
        dirMap
    }

    void fillInjectParameters(List<EnrRequestedCompetition> enrRequestedCompetitions)
    {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def personAddress = card.address

        def headers = getHeaderEmployeePostList(TopOrgUnit.instance)
        IdentityCard headCard = (headers != null && !headers.isEmpty()) ? headers.get(0).person.identityCard : null;

//        im.put('regNumber', entrantRequest.stringNumber)
//        im.put('highSchoolTitleShort', TopOrgUnit.instance.shortTitle)

        //ФИО ректора
        StringBuilder headIof = new StringBuilder();
        if (headCard != null)
        {
            if (StringUtils.isNotEmpty(headCard.firstName))
            {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".");
            }
            if (StringUtils.isNotEmpty(headCard.middleName))
            {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".");
            }
            headIof.append(' ').append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }

        im.put('rector_G', headIof.toString())
        im.put('FIO', card.fullFio)

        StringBuilder dateBuilder = new StringBuilder()
        if (card.birthDate != null)
            dateBuilder.append(RussianDateFormatUtils.getDayString(card.birthDate, true)).append(" ").append(RussianDateFormatUtils.getMonthName(card.birthDate, false))
            .append(" ").append(RussianDateFormatUtils.getYearString(card.birthDate, false))

        im.put('birthDate', dateBuilder.toString())

        im.put('citizenship', card.citizenship.fullTitle)
        im.put('identityCardTitle', card.shortTitle)

        StringBuilder cardSb = new StringBuilder()
        if (card.issuancePlace != null || card.issuanceDate != null)
        {
            cardSb.append(" выдан ")
            if(card.issuancePlace != null)
                cardSb.append(card.issuancePlace).append(" ");
            cardSb.append(card.issuanceDate?.format('dd.MM.yyyy'))
        }
        im.put('identityCardPlaceAndDate', cardSb.toString())
        im.put('adressTitleWithFlat', personAddress != null ? personAddress.titleWithFlat : "")
        im.put('adressPhonesTitle', person.contactData.mainPhones == null ? "" : person.contactData.mainPhones)
        im.put('registered', person.male ? 'зарегистрированный' : 'зарегистрированная')

        PersonEduDocument document = entrantRequest.eduDocument;
        im.put("education", document.eduLevel?.title)
        im.put("certificate", document.documentKindTitle)

        //Данные документа об образовании
        StringBuilder certificateBuilder = new StringBuilder();
        if (document.eduOrganizationAddressItem.inheritedRegionCode != null)
            certificateBuilder.append("код региона РФ ").append(document.eduOrganizationAddressItem.inheritedRegionCode)
        if (document.seria != null)
            certificateBuilder.append(", серия ").append(document.seria);
        certificateBuilder.append(" № ").append(document.number).append(", год окончания ").append(document.yearEnd)
        if (document.issuanceDate != null)
            certificateBuilder.append(", выдан ").append(document.issuanceDate.format("dd.MM.yyyy"))

        im.put('dateAndPlaceCertificate', certificateBuilder.toString())
        im.put('entrantBookNumber', entrantRequest.entrant.personalNumber)

        im.put("needHotel", person.needDormitory ? "да" : "нет")

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))

        //выбираем только общий конкурс на бюджет
        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { EnrCompetitionTypeCodes.MINISTERIAL.equals(it.competition.type.code) }

        //образование получает впервые
        if (!enrRequestedCompetitions.isEmpty())
        {
            im.put('firstEducation', new StringBuilder("Среднее профессиональное образование получаю ").append(entrantRequest.receiveEduLevelFirst? "впервые" : "не впервые").toString())
            im.put('signatureFieldFirstEdu', "__________________")
            im.put('signFirstEdu', "(подпись абитуриента)")
        }
        else
        {
            im.put('firstEducation',"")
            im.put('signatureFieldFirstEdu', "")
            im.put('signFirstEdu', "")
        }

    }


    def getHeaderEmployeePostList(OrgUnit orgUnit)
    {
        DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${orgUnit.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
        /).<EmployeePost> list()
    }

    /**
     * Данные отборочной комиссии
     */
    def fillEnrollmentCommissionData()
    {
        EnrEnrollmentCommission enrollmentCommission = entrantRequest.enrollmentCommission;
        if (enrollmentCommission == null)
        {
            im.put('executiveSecretaryPost', "")
            im.put('executiveSecretaryFIO', "")
            im.put('commissionTitle',"")
            return
        }

        //Секретарь отборочной комиссии
        EnrEnrollmentCommissionExt commissionExt = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentCommissionExtGen.NaturalId(enrollmentCommission));
        String postTitle = "";
        if (commissionExt && commissionExt.employee)
        {
            postTitle = StringUtils.capitalize(commissionExt.employee.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
            postTitle += StringUtils.uncapitalize(commissionExt.employee.orgUnit.genitiveCaseTitle? commissionExt.employee.orgUnit.genitiveCaseTitle : commissionExt.employee.orgUnit.title)
        }
        im.put("executiveSecretaryPost", postTitle)
        im.put("executiveSecretaryFIO", commissionExt && commissionExt.employee ? commissionExt.employee.fio : "")

    }
}
