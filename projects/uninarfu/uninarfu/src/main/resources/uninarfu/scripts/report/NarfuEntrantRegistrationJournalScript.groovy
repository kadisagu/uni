package uninarfu.scripts.report

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.collections15.Transformer
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.program.entity.EduProgramProf
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.entity.*
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.OrgUnitPrintTiltleComparator
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.EntrantRegistrationJournalAdd.NarfuEnrReportEntrantRegistrationJournalAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 06.05.2015
 */

return new NarfuEntrantRegistrationJournalPrint(          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

class NarfuEntrantRegistrationJournalPrint
{
    Session session
    byte[] template
    NarfuEnrReportEntrantRegistrationJournalAddUI model
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    private static final HEADER_TABLE = "orgUnit"
    private static final ENTRANTS_TABLE = "T"
    private static final SIGN_TABLE = "delegatePost"
    private static final SECRETARY_POST = "Ответственный секретарь приемной комиссии"


    def print()
    {
        def document = buildReport(new RtfReader().read(template))
        return [document: RtfUtil.toByteArray(document), fileName: 'NarfuEnrReportEntrantRegistrationJournal.rtf']
    }

    private RtfDocument buildReport(RtfDocument document)
    {
        Map<String, RtfTable> contentTables = new HashMap<>()

        IRtfElement reportHeader = document.getElementList().get(0)
        contentTables.put(HEADER_TABLE, copyTable(document, HEADER_TABLE))
        contentTables.put(ENTRANTS_TABLE, copyTable(document, ENTRANTS_TABLE))
        contentTables.put(SIGN_TABLE, copyTable(document, SIGN_TABLE))

        document.elementList.clear()

        document.getElementList().add(reportHeader)
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        fillReport(document, contentTables)


        return document
    }

    private void fillReport(RtfDocument document, Map<String, RtfTable> contentTables)
    {
        //получаем отфильтрованный массив конкурсов
        Collection<EnrCompetition> competitions = model.competitionFilterAddon.getFilteredList()
        Map<OrgUnit, Set<EnrProgramSetOrgUnit>> programSetByOrgUnitMap = new HashMap<>()
        Set<EnrProgramSetOrgUnit> programSetOrgUnitSet = new HashSet<>()
        Map<EnrProgramSetOrgUnit, Set<String>> compTypeByProgramSetOrgUnitMap = new HashMap<>()
        for (EnrCompetition competition : competitions)
        {
            SafeMap.safeGet(programSetByOrgUnitMap, competition.programSetOrgUnit.formativeOrgUnit, HashSet.class).add(competition.programSetOrgUnit)
            SafeMap.safeGet(compTypeByProgramSetOrgUnitMap, competition.programSetOrgUnit, HashSet.class).add(competition.type.compensationType.code)
            programSetOrgUnitSet.add(competition.programSetOrgUnit)
        }

        // Выбираем абитуриентов и подразделения, под параметры отчета:
        Map<EnrProgramSetOrgUnit, Map<String, List<EnrRatingItem>>> entrantMap = getEntrantsByTables(model, model.dateFrom, model.dateTo, model.isIncludeTookAwayDocuments(), true)
        //используется для заполнения количества поданных и отзванных заявлений с начала приема:
        Map<EnrProgramSetOrgUnit, Map<String, List<EnrRatingItem>>> entrantsFromStartCampaignMap = getEntrantsByTables(model, model.enrollmentCampaign.dateFrom, model.dateTo, true, true)
        //используется для заполнения количества поданных и отзванных заявлений:
        Map<EnrProgramSetOrgUnit, Map<String, List<EnrRatingItem>>> entrantsIncludeTakeAwayDocsMap = getEntrantsByTables(model, model.dateTo, model.dateTo, true, true)

        Map<OrgUnit, Set<EnrProgramSetOrgUnit>> nonEmptyProgramSetByOrgUnitMap = new HashMap<>()

        for (EnrProgramSetOrgUnit programSetOrgUnit : entrantMap.keySet())
        {
            OrgUnit orgUnit = programSetOrgUnit.formativeOrgUnit
            SafeMap.safeGet(nonEmptyProgramSetByOrgUnitMap, orgUnit, HashSet.class).add(programSetOrgUnit)
        }

        //убираем путсые таблицы
        if (model.skipEmptyOrgUnit)
        {
            programSetByOrgUnitMap = nonEmptyProgramSetByOrgUnitMap
        }

        // подгрузим образовательные программы наборов
        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = getProgramMap(programSetOrgUnitSet)

        //сортировка списка подразделений
        List<OrgUnit> orgUnitList = new ArrayList<>(programSetByOrgUnitMap.keySet())
        orgUnitList.sort(new OrgUnitPrintTiltleComparator())

        //Отрисовка отчета
        for (OrgUnit orgUnit : orgUnitList)
        {
            if (orgUnitList.indexOf(orgUnit) > 0)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE))
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            fillOrgUnitData(document, contentTables.get(HEADER_TABLE), orgUnit)

            for (EnrProgramSetOrgUnit programSetOrgUnit : programSetByOrgUnitMap.get(orgUnit))
            {
                List<String> compensationTypeList = new ArrayList<>(compTypeByProgramSetOrgUnitMap.get(programSetOrgUnit))
                compensationTypeList.sort()
                //формируем по отдельной таблице для каждого типа возмещения затрат
                for (String compensationType : compensationTypeList)
                {
                    fillEntrantTables(document, contentTables.get(ENTRANTS_TABLE), programSetOrgUnit, programMap, compensationType,
                            entrantMap?.get(programSetOrgUnit)?.get(compensationType),
                            entrantsFromStartCampaignMap?.get(programSetOrgUnit)?.get(compensationType),
                            entrantsIncludeTakeAwayDocsMap?.get(programSetOrgUnit)?.get(compensationType))
                }
            }
            fillSecretaryFio(document, contentTables.get(SIGN_TABLE), orgUnit)
        }

    }

    private void fillOrgUnitData(RtfDocument document, RtfTable table, OrgUnit orgUnit)
    {
        document.getElementList().add(table.clone)
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        im.put("orgUnit", orgUnit.printTitle.toUpperCase())
        im.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.dateTo))
        im.modify(document)
    }

    private void fillEntrantTables(RtfDocument document, RtfTable table, EnrProgramSetOrgUnit programSetOrgUnit, Map<EnrProgramSetBase, List<EduProgramProf>> programMap,
                                   String compensationType, List<EnrRatingItem> entrantList, List<EnrRatingItem> entrantsFromStartCampaign, List<EnrRatingItem> entrantsInDay)
    {
        if (!model.skipEmptyList || !CollectionUtils.isEmpty(entrantList))
        {
            document.getElementList().add(table.clone)
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
            List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet())
            EnrProgramSetBase programSetBase = programSetOrgUnit.programSet
            boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetBase instanceof EnrProgramSetSecondary)

            im.put("programForm", programSetBase.programForm.title)
            im.put("compensationType", getCompensationTypeTitle(compensationType))
            im.put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
            im.put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetBase.title)
            fillEntrantRows(document, entrantList)
            fillEntrantRequestCount(entrantsFromStartCampaign, entrantsInDay)

            tm.modify(document)
            im.modify(document)
        }
    }

    private void fillEntrantRequestCount(List<EnrRatingItem> entrantsFromStartCampaign, List<EnrRatingItem> entrantsInDay)
    {
        int reqByDay = 0
        int takeAwayByDay = 0
        int reqByPeriod = 0
        int takeAwayByPeriod = 0


        for (EnrRatingItem ratingItem : entrantsInDay)
        {
            reqByDay++
            if (ratingItem.requestedCompetition.request.takeAwayDocument)
                takeAwayByDay++
        }

        for (EnrRatingItem ratingItem : entrantsFromStartCampaign)
        {
            reqByPeriod++
            if (ratingItem.requestedCompetition.request.takeAwayDocument)
                takeAwayByPeriod++
        }

        im.put("reqByDay", String.valueOf(reqByDay))
        im.put("takeAwayByDay", String.valueOf(takeAwayByDay))
        im.put("reqByPeriod", String.valueOf(reqByPeriod))
        im.put("takeAwayByPeriod", String.valueOf(takeAwayByPeriod))
    }

    private void fillEntrantRows(RtfDocument document, List<EnrRatingItem> entrantList)
    {
        List<String[]> tableContent = new ArrayList<>();
        if (!CollectionUtils.isEmpty(entrantList))
        {
            entrantList.sort(new Comparator<EnrRatingItem>() {
                @Override
                int compare(EnrRatingItem o1, EnrRatingItem o2)
                {
                    return o1.entrant.fullFio.compareTo(o2.entrant.fullFio)
                }
            })

            int number = 1
            for (EnrRatingItem ratingItem : entrantList)
            {
                List<String> row = new ArrayList<>()
                row.add(String.valueOf(number))
                row.add(ratingItem.entrant.personalNumber)
                row.add(ratingItem.entrant.fullFio)
                row.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(ratingItem.requestedCompetition.request.takeAwayDocumentDate))
                tableContent.add(row as String[])
                number++
            }
        }
        tm.put("T", tableContent as String[][])
        tm.modify(document)
    }

    private void fillSecretaryFio(RtfDocument document, RtfTable table, OrgUnit orgUnit)
    {
        document.getElementList().add(table.clone);
        String postTitle = ""
        if (model.secretary)
        {
            postTitle = StringUtils.capitalize(model.secretary.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
            postTitle += StringUtils.uncapitalize(model.secretary.orgUnit.accusativeCaseTitle? model.secretary.orgUnit.accusativeCaseTitle : model.secretary.orgUnit.title)
        }
        im.put("delegatePost", postTitle)
        im.put("delegateFio", model.secretary ? model.secretary.fio : "")

        im.modify(document)
    }

    private static RtfTable copyTable(RtfDocument document, String label)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);
        return table
    }

    //абитуриенты отсортированые по наборам ОП и типам компенсации
    private Map<EnrProgramSetOrgUnit, Map<String, List<EnrRatingItem>>> getEntrantsByTables(NarfuEnrReportEntrantRegistrationJournalAddUI model,
                                                                                            Date dateFrom, Date dateTo, boolean isIncludeTookAwayDocuments, boolean fetch)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, dateFrom, dateTo, isIncludeTookAwayDocuments, fetch)
        Map<EnrProgramSetOrgUnit, Map<String, List<EnrRatingItem>>> entrantMap = SafeMap.get(new SafeMap.Callback<EnrProgramSetOrgUnit, Map<String, List<EnrRatingItem>>>() {
            @Override
            Map<String, List<EnrRatingItem>> resolve(EnrProgramSetOrgUnit key)
            {
                return SafeMap.get(ArrayList.class);
            }
        })

        for (EnrRatingItem ratingItem : requestedCompDQL.createStatement(getSession()).<EnrRatingItem> list())
        {
            EnrCompetition competition = ratingItem.competition
            entrantMap.get(competition.programSetOrgUnit).get(competition.type.compensationType.code).add(ratingItem)
        }
        return entrantMap
    }

    private static EnrEntrantDQLSelectBuilder prepareEntrantDQL(NarfuEnrReportEntrantRegistrationJournalAddUI model, Date dateFrom,
                                                                Date dateTo, boolean isIncludeTookAwayDocuments, boolean fetch)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(dateFrom, dateTo)
                .filter(model.enrollmentCampaign)

        if (isIncludeTookAwayDocuments)
        {
            requestedCompDQL.where(ne(property(EnrRequestedCompetition.state().code().fromAlias(requestedCompDQL.reqComp())), value(EnrEntrantStateCodes.OUT_OF_COMPETITION)));
        }
        else
        {
            requestedCompDQL.defaultActiveFilter()
        }

        if (model.isEnrollmentCommissionActive())
            requestedCompDQL.where(DQLExpressions.in(property(EnrRequestedCompetition.request().enrollmentCommission().fromAlias(requestedCompDQL.reqComp())), model.enrollmentCommissionList))


        model.getCompetitionFilterAddon().applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelOnly())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)))
        }
        if (model.isSkipParallel())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)))
        }
        return requestedCompDQL;
    }


    private static Map<EnrProgramSetBase, List<EduProgramProf>> getProgramMap(Set<EnrProgramSetOrgUnit> programSetOrgUnitSet)
    {

        Collection<EnrProgramSetBase> programSets = org.apache.commons.collections15.CollectionUtils.collect(programSetOrgUnitSet, new Transformer<EnrProgramSetOrgUnit, EnrProgramSetBase>() {
            @Override
            EnrProgramSetBase transform(EnrProgramSetOrgUnit enrProgramSetOrgUnit)
            {
                return enrProgramSetOrgUnit.programSet;
            }
        })
        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class)
        for (EnrProgramSetItem item : DataAccessServices.dao().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets))
        {
            programMap.get(item.getProgramSet()).add(item.getProgram())
        }
        for (EnrProgramSetBase programSetBase : programSets)
        {
            if (programSetBase instanceof EnrProgramSetSecondary)
            {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary) programSetBase).getProgram())
            }
        }
        return programMap
    }

    private static String getCompensationTypeTitle(String compensationTypeCode)
    {
        if (compensationTypeCode.equals(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET))
        {
            return "За счет средств Федерального бюджета"
        }
        else
        {
            return "Обучение на договорной основе"
        }
    }
}