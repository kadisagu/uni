package uninarfu.scripts.entrant

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.PersonEduDocument
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam
import ru.tandemservice.unienr14.request.entity.*
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt
import ru.tandemservice.uninarfu.entity.EntrantSpecConditionRelation
import ru.tandemservice.uninarfu.entity.catalog.NarfuSpecConditionsEntTests
import ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Ekaterina Zvereva
 * @since 23.04.2015
 */

return new NarfuEntrantRequestPrintBS(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()


class NarfuEntrantRequestPrintBS
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() // метки, строки с которыми необходимо удалить из таблицы

    def print()
    {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()} = ${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
        /).<EnrRequestedCompetition> list()

        fillCompetition(directions)
        fillInternalExams(directions)
        fillEnrEntrantStateExam(directions)
        fillInjectParameters()
        fillCompetitionExclusive(directions)
        fillIndividualResults()
        printFirstEducationText(directions)
        fillEnrollmentCommissionData()

        // стандартные выходные параметры скрипта
        return [document: UniRtfUtil.toByteArray(template, im, tm, deleteLabels),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillEnrEntrantStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions)
    {
        def results = getEnrEntrantMarkSourceStateExam(enrRequestedCompetitions)
        def rows = Lists.newArrayList()

        int count = 0
        if (!results.isEmpty())
        {
            // для каждого результата ЕГЭ
            for (def result : results)
            {
                count++
                // формируем строку из трех столбцов
                rows.add([
                        String.valueOf(count),
                        result.getSubject().getTitle(),
                        result.getMark(),
                        result.getYear()
                ])
            }
        }

        def olimps = getOlympiadResultsList(enrRequestedCompetitions)
        if (!olimps.isEmpty())
        {
            for (def result : olimps)
            {
                count++
                rows.add([
                        String.valueOf(count),
                        result.chosenEntranceExamForm.chosenEntranceExam.discipline.title,
                        "",
                        "",
                        "Да"
                ])
            }
        }


       if (rows.size() > 0)
           tm.put('T3', rows as String[][])
        else
            tm.remove("T3", 0, 1)
    }

    def getOlympiadResultsList(List<EnrRequestedCompetition> enrRequestedCompetitions)
    {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id}

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantMarkSourceBenefit.class, "source")
                .where(DQLExpressions.in(property("source", EnrEntrantMarkSourceBenefit.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()), ids))
                .where(eq(property("source", EnrEntrantMarkSourceBenefit.entrant()), value(entrantRequest.entrant)))

        return builder.createStatement(session).<EnrEntrantMarkSourceBenefit>list()
    }

    // результаты ЕГЭ
    def getEnrEntrantMarkSourceStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id}

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantStateExamResult.class, "ser")
                .where(exists(
                new DQLSelectBuilder()
                        .fromEntity(EnrEntrantMarkSourceStateExam.class, "ms")
                        .where(DQLExpressions.in(property("ms", EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()), ids))
                        .where(eq(property("ms", EnrEntrantMarkSourceStateExam.stateExamResult().id()), property("ser", EnrEntrantStateExamResult.id())))
                        .buildQuery()))
                .order(property("ser", EnrEntrantStateExamResult.subject().title()))

        return builder.createStatement(session).<EnrEntrantStateExamResult>list()
    }

    //Внутренние экзамены
    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def rows = new ArrayList<String[]>()

        final enrChosenEntranceExamForms =
                new DQLSelectBuilder().fromEntity(EnrChosenEntranceExamForm.class, 'ad')
                .distinct()
                .column(property('ad', EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .where(DQLExpressions.in(property('ad', EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition()), enrRequestedCompetitions))
                .where(eq(property('ad', EnrChosenEntranceExamForm.passForm().internal()), value(true)))
                .order(property('ad', EnrChosenEntranceExamForm.chosenEntranceExam().discipline().discipline().title()))
                .createStatement(this.session).<Object[]>list()

        if(!enrChosenEntranceExamForms.isEmpty())
        {
            boolean isProfEdu = entrantRequest.eduDocument.eduLevel.code.equals(EduLevelCodes.PROFESSIONALNOE_OBRAZOVANIE) || entrantRequest.eduDocument.eduLevel.parent.code.equals(EduLevelCodes.PROFESSIONALNOE_OBRAZOVANIE)
            StringBuilder examReasonBuilder = new StringBuilder().append(entrantRequest.internalExamReason? entrantRequest.internalExamReason.title:"")
            if (examReasonBuilder.size() > 0 && isProfEdu)
                examReasonBuilder.append(", ")
            examReasonBuilder.append(isProfEdu? "Лица, имеющие профессиональное образование":"")

            for (String item : enrChosenEntranceExamForms)
            {
                rows.add([
                        String.valueOf(rows.size()+1),
                        String.valueOf(item),
                        examReasonBuilder.toString()
                ] as String[])

            }
            tm.put('T2', rows as String[][])

        } else tm.remove("T2")



    }


    def fillCompetitionExclusive(List<EnrRequestedCompetition> enrRequestedCompetitions)
    {
        def exclusiveCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }.collect { e -> (EnrRequestedCompetitionExclusive) e }
        if (!exclusiveCompetitions.isEmpty())
        {
            im.put('isSpecRights', "Да")
            def benefitCategories = exclusiveCompetitions.collect { e -> e.benefitCategory }.toSet().toList().sort()
            Map<Long, List<IEnrEntrantBenefitProofDocument>> benefiteDocMap = getBenefitProofDocuments(exclusiveCompetitions)

            def rows = Lists.newArrayList()
            for (EnrBenefitCategory item : benefitCategories)
            {
                // формируем строку из трех столбцов
                String[] row = new String[3]
                row[0] = item.title
                row[1] = item.benefitType.title
                row[2] = UniStringUtils.join(benefiteDocMap.get(item.id),IEnrEntrantBenefitProofDocument.DISPLAYABLE_TITLE, ", ")

                rows.add(row)
            }

            tm.put('T4', rows as String[][])
            im.put('signConfirmationSpecialRights', "Подтверждаю подачу заявления о приеме на основании соответствующего особого права только в данную организацию высшего образования")
            im.put('signatureFieldSpecRights', "________________")
            im.put('signSpecRights', "(подпись абитуриента)")


        }
        else
        {
            im.put('isSpecRights', "Нет")
            tm.remove("T4")
            im.put('signConfirmationSpecialRights', "")
            im.put('signatureFieldSpecRights', "")
            im.put('signSpecRights', "")
        }
    }

    Map<Long, List<IEnrEntrantBenefitProofDocument>> getBenefitProofDocuments(List<? extends IEnrEntrantBenefitStatement> reqCompetitions)
    {
        def benefitProofs = new DQLSelectBuilder()
                .fromEntity(EnrEntrantBenefitProof.class, 'bp')
                .column(property('bp'))
                .where(DQLExpressions.in(property('bp', EnrEntrantBenefitProof.benefitStatement().id()), reqCompetitions.collect { e -> e.id }))
                .createStatement(this.getSession()).<EnrEntrantBenefitProof>list()

        Map<Long, List<IEnrEntrantBenefitProofDocument>> result = SafeMap.get(ArrayList.class)
        for (def benefitProof: benefitProofs)
            result.get(benefitProof.benefitStatement.benefitCategory.id).add(benefitProof.document)

        return result
    }


    def fillCompetition(List<EnrRequestedCompetition> enrRequestedCompetitions) {

        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }
        def dirMap = getDirMap(enrRequestedCompetitions)

        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort{e -> e.priority}


        for (def requestedCompetition : enrRequestedCompetitions)
        {
            // формируем строку из 6 столбцов
            String[] row = new String[6]
            row[0] = requestedCompetition.priority


            EduProgramSubject subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
            List<EduProgramHigherProf> programList = new ArrayList<>();
            for (EnrProgramSetItem item : IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), requestedCompetition.competition.programSetOrgUnit.programSet))
                programList.add(item.getProgram());

            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")

            row[1] = programSubject
            row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
            row[3] = getPlaces(requestedCompetition.competition.type)
            row[4] = programList.isEmpty()? "" : programList.get(0).duration.title
            row[5] = requestedCompetition.competition.type.title
            rows.add(row)
        }

        tm.put('T1', rows as String[][])
    }

    static def getPlaces(EnrCompetitionType type) {
        if(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(type.code) ||
                EnrCompetitionTypeCodes.CONTRACT.equals(type.code))
            return "с полным возмещением затрат на обучение"
        else if(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(type.code) ||
                EnrCompetitionTypeCodes.MINISTERIAL.equals(type.code) )
            return "за счёт средств федерального бюджета"
        else if(EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(type.code))
            return "в пределах квоты целевого приема"
        else if(EnrCompetitionTypeCodes.EXCLUSIVE.equals(type.code))
            return "в пределах квоты приема лиц, имеющих особое право"
        return ""
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id};
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "ch")
        builder.where(DQLExpressions.in(DQLExpressions.property("ch", EnrRequestedProgram.requestedCompetition().id()), ids))

        def programs = builder.createStatement(session).<EnrRequestedProgram>list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for(EnrRequestedProgram program : programs)
        {
            Long id = program.requestedCompetition.id;
            if(!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if(!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear();
            }
        }
        dirMap
    }

    void fillInjectParameters()
    {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def personAddress = card.address

        def headers = getHeaderEmployeePostList(TopOrgUnit.instance)
        IdentityCard headCard = (headers != null && !headers.isEmpty() ) ? headers.get(0).person.identityCard : null;

        im.put('regNumber', entrantRequest.stringNumber)
        im.put('highSchoolTitleShort', TopOrgUnit.instance.shortTitle)

        StringBuilder headIof = new StringBuilder();
        if(headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName)) {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".");
            }
            if (StringUtils.isNotEmpty(headCard.middleName)) {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".");
            }
            headIof.append(' ').append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }

        im.put('rector_G', headIof.toString())
        im.put('FIO', card.fullFio)
        im.put('birthDate', card.birthDate?.format('dd.MM.yyyy'))
        im.put('birthPlace', card.birthPlace)
        im.put('citizenship', card.citizenship.fullTitle)
        im.put('identityCardTitle', card.shortTitle)

        StringBuilder cardSb = new StringBuilder()
        if (card.issuancePlace != null || card.issuanceDate != null)
        {
            cardSb.append(" выдан ")
            if(card.issuancePlace != null)
                cardSb.append(card.issuancePlace).append(" ");
            cardSb.append(card.issuanceDate?.format('dd.MM.yyyy'))
        }
        im.put('identityCardPlaceAndDate', cardSb.toString())

        im.put('adressTitleWithFlat', personAddress != null ? personAddress.titleWithFlat : "")
        im.put('adressPhonesTitle', person.contactData.mainPhones == null ? "" : person.contactData.mainPhones)

        im.put('registered', person.male ? 'зарегистрированный' : 'зарегистрированная')

        PersonEduDocument document = entrantRequest.eduDocument;
        im.put("education", document.eduLevel?.title)
        im.put("certificate", document.documentKindTitle)

        //Данные документа об образовании
        StringBuilder certificateBuilder = new StringBuilder();
        if (document.eduOrganizationAddressItem.inheritedRegionCode != null)
            certificateBuilder.append("код региона РФ ").append(document.eduOrganizationAddressItem.inheritedRegionCode)
        if (document.seria != null)
            certificateBuilder.append(", серия ").append(document.seria);
        certificateBuilder.append(" № ").append(document.number).append(", год окончания ").append(document.yearEnd)
        if (document.issuanceDate != null)
            certificateBuilder.append(", выдан ").append(document.issuanceDate.format("dd.MM.yyyy"))

        im.put('dateAndPlaceCertificate', certificateBuilder.toString())
        im.put('entrantBookNumber', entrantRequest.entrant.personalNumber)

        im.put("needHotel", person.needDormitory ? "да" : "нет")
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))


        if (entrant.needSpecialExamConditions)
        {
            //Специальные условия прохождения ВИ
            List<NarfuSpecConditionsEntTests> specConditionsEntTestses = new DQLSelectBuilder().fromEntity(EntrantSpecConditionRelation.class, "rel")
                    .column(property("rel", EntrantSpecConditionRelation.specCondition()))
                    .where(eq(property("rel", EntrantSpecConditionRelation.enrEntrant()), value(entrant))).createStatement(session).list()

            im.put('specialConditions', CommonBaseStringUtil.join(specConditionsEntTestses, NarfuSpecConditionsEntTests.title(), "; "))
            im.put('isSpecCond', "Да, ")
        }
        else
        {
            im.put('isSpecCond', "Нет")
            im.put('specialConditions', "")
        }

    }


    //текст о том, что образование получает впервые
    def printFirstEducationText(List<EnrRequestedCompetition> enrRequestedCompetitions)
    {
        //выбираем только общий конкурс на бюджет
        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { EnrCompetitionTypeCodes.MINISTERIAL.equals(it.competition.type.code) }

        //образование получает впервые
        if (!enrRequestedCompetitions.isEmpty())
        {
            im.put('firstEducation', new StringBuilder("Профессиональное образование указанного уровня получаю ").append(entrantRequest.receiveEduLevelFirst? "впервые" : "не впервые").toString())
            im.put('signatureFieldFirstEdu', "__________________")
            im.put('signFirstEdu', "(подпись абитуриента)")
        }
        else
        {
            im.put('firstEducation',"")
            im.put('signatureFieldFirstEdu', "")
            im.put('signFirstEdu', "")
        }
    }

    def fillIndividualResults()
    {
        List<EnrEntrantAchievement> listAchievement = DataAccessServices.dao().getList(EnrEntrantAchievement.class, EnrEntrantAchievement.entrant(), entrantRequest.entrant);
        if (listAchievement.isEmpty())
        {
            im.put('isIndividResults', "Нет")
            tm.remove("T5")
        }
        else
        {
            def rows = new ArrayList()
            def count=1
            for (EnrEntrantAchievement item : listAchievement)
            {
                String[] row = new String[3]
                row[0] = count++

                row[1] = item.type.achievementKind.title
                row[2] = String.valueOf((int)item.ratingMarkAsLong / 1000)
                rows.add(row)
            }
            im.put('isIndividResults', "Да")
            tm.put('T5', rows as String[][])
        }
    }


    def getHeaderEmployeePostList(OrgUnit orgUnit)
    {
        DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${orgUnit.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
        /).<EmployeePost> list()
    }

    /**
     * Данные отборочной комиссии
     */
    def fillEnrollmentCommissionData()
    {
        EnrEnrollmentCommission enrollmentCommission = entrantRequest.enrollmentCommission;
        if (enrollmentCommission == null)
        {
            im.put('executiveSecretaryPost', "")
            im.put('executiveSecretaryFIO', "")
            im.put('commissionTitle',"")
            return
        }

        //Секретарь отборочной комиссии
        EnrEnrollmentCommissionExt commissionExt = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentCommissionExtGen.NaturalId(enrollmentCommission));
        String postTitle = "";
        if (commissionExt && commissionExt.employee)
        {
            postTitle = StringUtils.capitalize(commissionExt.employee.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
            postTitle += StringUtils.uncapitalize(commissionExt.employee.orgUnit.genitiveCaseTitle? commissionExt.employee.orgUnit.genitiveCaseTitle : commissionExt.employee.orgUnit.title)
        }
        im.put("executiveSecretaryPost", postTitle)
        im.put("executiveSecretaryFIO", commissionExt && commissionExt.employee ? commissionExt.employee.fio : "")

    }

}