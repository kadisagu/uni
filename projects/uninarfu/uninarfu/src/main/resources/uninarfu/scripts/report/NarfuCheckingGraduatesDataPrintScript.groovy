package uninarfu.scripts.report

import com.google.common.collect.ComparisonChain
import com.google.common.collect.Multimap
import com.google.common.collect.TreeMultimap
import org.hibernate.Session
import org.tandemframework.core.CoreCollectionUtils
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.base.entity.DeclinableProperty
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.entity.catalog.EducationLevels
import ru.tandemservice.uni.entity.education.EducationOrgUnit
import ru.tandemservice.uni.entity.employee.Student

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Andreev
 * @since 28.10.2015
 */
return new NarfuCheckingGraduatesDataPrint(
        // стандартные входные параметры скрипта
        session: session,
        template: template,
        orgUnitId: orgUnitId,
        developFormId: developFormId,
        developConditionId: developConditionId,
        developPeriodId: developPeriodId,
        studentsStatusIds: studentsStatusIds,
        programSubjectIds: programSubjectIds
).print()

class NarfuCheckingGraduatesDataPrint
{
    Session session;
    byte[] template;
    Long orgUnitId;
    Long developFormId;
    Long developConditionId;
    Long developPeriodId;
    List<Long> studentsStatusIds;
    List<Long> programSubjectIds;


    String _year;
    Multimap<EducationOrgUnit, Student> _studentsByEduOrgUnit;

    def print()
    {
        _studentsByEduOrgUnit = getStudents();
        if (_studentsByEduOrgUnit.isEmpty())
            throw new ApplicationException("Студентов по выбранным параметрам не найдено");

        _year = Calendar.getInstance().get(Calendar.YEAR) as String;

        String filename = "Ведомость сверки данных (" + _year + ").rtf";

        RtfDocument document = buildReport(template)
        return [document: RtfUtil.toByteArray(document), fileName: filename]
    }

    RtfDocument buildReport(byte[] template)
    {
        RtfDocument fullDocument = RtfDocument.fromScratch();
        RtfDocument temp = RtfDocument.fromByteArray(template);
        fullDocument.setHeader(temp.getHeader());
        fullDocument.setSettings(temp.getSettings());

        for (EducationOrgUnit educationOrgUnit : _studentsByEduOrgUnit.keySet())
        {
            RtfDocument subDoc = printSubDocument(temp.getClone(), educationOrgUnit);
            if (subDoc != null)
                fullDocument.getElementList().addAll(subDoc.getElementList());
        }

        return fullDocument;
    }

    RtfDocument printSubDocument(RtfDocument document, EducationOrgUnit educationOrgUnit)
    {
        Collection<Student> students = _studentsByEduOrgUnit.get(educationOrgUnit);
        if (students == null || students.isEmpty()) return null;

        OrgUnit orgUnit = educationOrgUnit.formativeOrgUnit;

        EducationLevels level = educationOrgUnit.educationLevelHighSchool.educationLevel;

        String qualification = educationOrgUnit.educationLevelHighSchool.assignedQualification?.title ?: ''
        String programSubject = level.getTitleCodePrefix() + " " + level.eduProgramSubject.title;

        String headPost = "Руководитель подразделения";
        String headFIO = orgUnit.head != null ? orgUnit.head.employee.person.identityCard.fio : "";

        for (EmployeePost employeePost : EmployeeManager.instance().dao().getHeaderEmployeePostList(orgUnit))
        {
            if (orgUnit.head != null)
            {
                if (employeePost.person.id.equals(orgUnit.head.employee.person.id))
                {
                    if (employeePost.postRelation.postBoundedWithQGandQL != null)
                    {
                        headPost = employeePost.postRelation.postBoundedWithQGandQL.title;
                        headPost = headPost.substring(0, 1).toUpperCase() + headPost.substring(1);
                        break;
                    }
                }
            } else
            {
                if (employeePost.postRelation.headerPost)
                {
                    if (employeePost.postRelation.postBoundedWithQGandQL != null)
                    {
                        headPost = employeePost.postRelation.postBoundedWithQGandQL.title;
                        headPost = headPost.substring(0, 1).toUpperCase() + headPost.substring(1);
                        headFIO = employeePost.person.identityCard.fio;
                        break;
                    }
                }
            }
        }

        new RtfInjectModifier()
                .put("year", _year)
                .put("orgUnit", orgUnit.title)
                .put("developForm", educationOrgUnit.developForm.title)
                .put("developPeriod", educationOrgUnit.developPeriod.title)
                .put("orgUnit", orgUnit.title)
                .put("specOrProf", educationOrgUnit.educationLevelHighSchool.getEducationLevel().getProgramSpecializationTitleIfChild())
                .put("programSubject", programSubject)
                .put("qualification", qualification)
                .put("delegatePost", headPost)
                .put("delegateFio", headFIO)
                .modify(document);

        ArrayList<String[]> rows = new ArrayList<>();
        int count = 1;
        for (Student student : students)
        {
            String[] row = new String[3];
            row[0] = String.valueOf(count++);
            row[1] = student.fullFio;
            row[2] = getDativeFIO(student.person.identityCard);
            rows.add(row);
        }
        new RtfTableModifier().put("T", (String[][]) rows.toArray()).modify(document);

        return document;
    }


    Multimap<EducationOrgUnit, Student> getStudents()
    {
        String alias = "st";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, alias)
                .column(property(alias, Student.educationOrgUnit()))
                .column(property(alias))
                .where(eq(property(alias, Student.course().intValue()),
                property(alias, Student.educationOrgUnit().developPeriod().lastCourse())))
                .where(eq(property(alias, Student.archival()), value(false)))
//                .where(eq(property(alias, Student.archival()), value(false)))

        CommonBaseFilterUtil.applySelectFilter(dql, alias, Student.educationOrgUnit().formativeOrgUnit().id(), orgUnitId);
        CommonBaseFilterUtil.applySelectFilter(dql, alias, Student.educationOrgUnit().developForm().id(), developFormId);
        CommonBaseFilterUtil.applySelectFilter(dql, alias, Student.educationOrgUnit().developCondition().id(), developConditionId);
        CommonBaseFilterUtil.applySelectFilter(dql, alias, Student.educationOrgUnit().developPeriod().id(), developPeriodId);
        CommonBaseFilterUtil.applySelectFilter(dql, alias, Student.status().id(), studentsStatusIds);
        CommonBaseFilterUtil.applySelectFilter(dql, alias,
                Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().id(),
                programSubjectIds);

        Multimap<EducationOrgUnit, Student> students = TreeMultimap.create(
                new Comparator<EducationOrgUnit>() {
                    @Override
                    int compare(EducationOrgUnit o1, EducationOrgUnit o2)
                    {
                        return Long.compare(o1.id, o2.id);
                    }
                },
                new Comparator<Student>() {
                    @Override
                    int compare(Student o1, Student o2)
                    {
                        return ComparisonChain.start()
                                .compare(o1.fullFio, o2.fullFio)
                                .compare(o1.id, o2.id)
                                .result()
                    }
                }
        )

        List<Object[]> rows = dql.createStatement(session).list();
        for (List<Object> row : rows)
        {
            students.put((EducationOrgUnit) row[0], (Student) row[1]);
        }

        return students;
    }

    String getDativeFIO(IdentityCard card)
    {
        IdentityCardDeclinability fioDeclinability = new DQLSelectBuilder()
                .fromEntity(IdentityCardDeclinability.class, "icd")
                .column(property("icd"))
                .where(eq(property("icd", IdentityCardDeclinability.identityCard()), value(card)))
                .createStatement(session)
                .uniqueResult();

        if (fioDeclinability == null)
            return PersonManager.instance().declinationDao().getDeclinationFIO(card, GrammaCase.DATIVE);

        List<DeclinableProperty> declinablePropertiesList = new DQLSelectBuilder()
                .fromEntity(DeclinableProperty.class, "dp")
                .column(property("dp"))
                .where(eq(property(DeclinableProperty.declinable().fromAlias("dp")), value(fioDeclinability)))
                .createStatement(session)
                .list();

        Map<CoreCollectionUtils.Pair<String, InflectorVariant>, String> savedDeclinations = new HashMap<>();
        for (DeclinableProperty dp : declinablePropertiesList)
        {
            savedDeclinations.put(new CoreCollectionUtils.Pair<String, InflectorVariant>(dp.property, dp.variant), dp.value);
        }

        InflectorVariant variant = DeclinationDao.INFLECTOR_VARIANTS_MAP.get(InflectorVariantCodes.RU_DATIVE);
        boolean sex = card.sex.code.equals(SexCodes.MALE);

        String first = card.firstName;
        if (fioDeclinability.isFirstNameDeclinable())
        {
            first = savedDeclinations.get(new CoreCollectionUtils.Pair<String, InflectorVariant>(IdentityCard.firstName().s(), variant));
            if (first == null)
                first = PersonManager.instance().declinationDao().getDeclinationLastName(card.firstName, GrammaCase.DATIVE, sex);
        }

        String middle = card.middleName;
        if (fioDeclinability.isMiddleNameDeclinable())
        {
            middle = savedDeclinations.get(new CoreCollectionUtils.Pair<String, InflectorVariant>(IdentityCard.middleName().s(), variant));
            if (middle == null)
                middle = PersonManager.instance().declinationDao().getDeclinationLastName(card.middleName, GrammaCase.DATIVE, sex);
        }

        String last = card.lastName;
        if (fioDeclinability.isLastNameDeclinable())
        {
            last = savedDeclinations.get(new CoreCollectionUtils.Pair<String, InflectorVariant>(IdentityCard.lastName().s(), variant));
            if (last == null)
                last = PersonManager.instance().declinationDao().getDeclinationLastName(card.lastName, GrammaCase.DATIVE, sex);
        }

        return last + " " + first + " " + middle;
    }

}