package uninarfu.scripts.order

import org.apache.commons.lang.StringUtils
import org.hibernate.Session

/**
 * @author Ekaterina Zvereva
 * @since 30.07.2015
 */
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramOrientationCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrEnrollmentOrderPrintUtil
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao
import ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt
import ru.tandemservice.uninarfu.order.ext.EnrOrder.logic.EnrOrderFilialPartWrapper
import ru.tandemservice.uninarfu.order.ext.EnrOrder.logic.EnrOrderPartWrapper

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EnrollmentOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов САФУ
 */
class EnrollmentOrderPrint
{
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    List<IAbstractParagraph> paragraphList
    int counter;


    def print()
    {
        def coreDao = IUniBaseDao.instance.get()
        paragraphList = order.paragraphList

        // заполнение меток
         im.put('commitDate', order.commitDate? order.commitDate.format('dd.MM.yyyy'): "            ")
        im.put('orderNumber', order.number? order.number:"              ")
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)

        im.put('reasonText', order.orderBasicText)
        im.put('orderDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))
        im.put('executorFio', order.executor)

        if (paragraphList.size() > 0 && paragraphList[0] instanceof EnrEnrollmentParagraph)
        {
            EnrEnrollmentOrderPrintUtil.initOrgUnit(im,
                    ((EnrEnrollmentParagraph) paragraphList[0]).formativeOrgUnit, 'formativeOrgUnit', '')
        }

        // заполнение виз
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        // заполнение параграфов
        def document = new RtfReader().read(template)
        List<EnrOrderFilialPartWrapper> orderParts = prepareOrderPart();
        List<IRtfElement> partList = []

        counter = 0
        for (EnrOrderFilialPartWrapper orderFilialPart : orderParts)
        {
            Collections.sort(orderFilialPart.orderParts)
            for (EnrOrderPartWrapper wrapper : orderFilialPart.orderParts)
            {

                List<IRtfElement> itemList = fillOrderParts(orderFilialPart, wrapper, document.header)

                def group = RtfBean.elementFactory.createRtfGroup()
                group.elementList = itemList
                partList.add(group)
            }
            partList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
            partList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
            partList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        }

        im.put('PARAGRAPH', partList)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentOrder.rtf']
    }

    /**
     * Необходима особая сортировка и группировка параграфов. Сначала группируется по филиалам, затем по форме освоения, виду конкурса...
     * Поэтому производим разбивку параграфов приказа на группы
     */
    private List<EnrOrderFilialPartWrapper> prepareOrderPart()
    {
        List<EnrOrderFilialPartWrapper> orderFilialParts = new ArrayList<>();
        List<IAbstractParagraph> orderParagraphsList = order.paragraphList;
        for (IAbstractParagraph paragraph : orderParagraphsList)
        {
            if (!paragraph instanceof EnrEnrollmentParagraph)
                throw new ApplicationException("Параграф №${paragraph.number} в приказе «" + paragraph.order.title + "» не может быть напечатан, так как не является параграфом о зачислении.")
            EnrEnrollmentParagraph enrParagraph = (EnrEnrollmentParagraph)paragraph;

            EnrOrderFilialPartWrapper filialPart = new EnrOrderFilialPartWrapper(enrParagraph.formativeOrgUnit)
            int ind = orderFilialParts.indexOf(filialPart)
            if (ind == -1)
                orderFilialParts.add(filialPart)
            else
                filialPart = orderFilialParts.get(ind)

            EnrOrderPartWrapper part = new EnrOrderPartWrapper(enrParagraph.programForm, enrParagraph.competitionType)
            int pos = filialPart.getOrderParts().indexOf(part)
            if (pos == -1)
                filialPart.getOrderParts().add(part)
            else
                part = filialPart.getOrderParts().get(pos)


            part.paragraphList.add(enrParagraph)
        }

        Collections.sort(orderFilialParts)
        return orderFilialParts
    }

    List<IRtfElement> fillOrderParts(EnrOrderFilialPartWrapper orderFilialPart, EnrOrderPartWrapper orderPart, RtfHeader header)
    {
        byte[] paragraphTemplate = EnrEnrollmentOrderPrintUtil.getEnrollmentOrderParagraphTemplate(orderPart.getParagraphList().get(0).getId())
        def paragraphDocument = new RtfReader().read(paragraphTemplate)
        RtfUtil.modifySourceList(header, paragraphDocument.header, paragraphDocument.elementList)
        List<IRtfElement> partList = new ArrayList<>();

        RtfTable topTable = removeTable(paragraphDocument, 'TOP', true)

        int index = orderFilialPart.orderParts.indexOf(orderPart)

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put('filialTitle',index==0? new RtfString().append(orderFilialPart.branch.top?
                "" : StringUtils.capitalize(orderFilialPart.branch.title)).append(IRtfData.PAR): null)

        modifier.put('filialTitle_G', orderFilialPart.branch.top? "" : orderFilialPart.branch.genitiveCaseTitle);


        if (order.requestType.code.equals(EnrRequestTypeCodes.SPO))
        {
            modifier.put('formativeOrgUnitSPO', new StringBuilder(" ").append(orderFilialPart.branch.genitiveCaseTitle ? orderFilialPart.branch.genitiveCaseTitle
                    : orderFilialPart.branch.printTitle).toString())
            modifier.put('filialTitle_G', "");
        }
        else modifier.put('formativeOrgUnitSPO','')

        NarfuEnrOrderExt orderExt = DataAccessServices.dao().get(NarfuEnrOrderExt.class, NarfuEnrOrderExt.enrOrder(), order)

        modifier.put('orderDateStr', order.actionDate? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(order.actionDate): '________________')
        modifier.put('compensationType', order.compensationType.budget ? ", финансируемые из средств федерального бюджета" : " по договорам об оказании платных образовательных услуг")
        modifier.put('developForm_G', getProgramFormString(orderPart.getProgramForm()))
        partList.add(topTable)
        modifier.modify(partList)

        List<IRtfElement> paragraphList = fillParagraphs(orderExt, orderPart.paragraphList, paragraphDocument)

        def group = RtfBean.elementFactory.createRtfGroup()
        group.elementList = paragraphList
        partList.add(group)
        partList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        partList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
        partList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        return partList
    }

    private ArrayList<IRtfElement> fillParagraphs(NarfuEnrOrderExt orderExt, List<EnrEnrollmentParagraph> orderParagraphsList, RtfDocument document)
    {
        RtfTable headerTable = removeTable(document, 'HEADER', true)
        RtfTable studentTable = removeTable(document, 'STUDENT_LIST', false)
        RtfTable bottomTable = removeTable(document, 'BOTTOM', true)

        RtfTableModifier tableModifier = new RtfTableModifier()
        List<IRtfElement> paragraphList = new ArrayList<IRtfElement>()

        Collections.sort(orderParagraphsList, new Comparator<EnrEnrollmentParagraph>() {
            @Override
            int compare(EnrEnrollmentParagraph o1, EnrEnrollmentParagraph o2) {
                return o1.programSubject.code.compareTo(o2.programSubject.code)
            }
        })
        for (EnrEnrollmentParagraph paragraph : orderParagraphsList)
        {
            def enrollmentExtractList = (List<EnrEnrollmentExtract>) paragraph.extractList
            if (enrollmentExtractList.size() == 0)
                throw new ApplicationException("Пустой параграф №${paragraph.number} в приказе «" + paragraph.order.title + "» не может быть напечатан.")

            def firstExtract = enrollmentExtractList.get(0)
            EnrProgramSetBase programSetBase = firstExtract.competition.programSetOrgUnit.programSet;
            List<EduProgramHigherProf> programSetItem = IUniBaseDao.instance.get().<EduProgramHigherProf>getList(new DQLSelectBuilder().fromEntity(EnrProgramSetItem.class, "i")
                    .column(property("i", EnrProgramSetItem.program()))
                    .where(eqValue(property("i", EnrProgramSetItem.L_PROGRAM_SET), programSetBase))
            )

            final durations
            if (programSetBase instanceof EnrProgramSetSecondary) {
                durations = [programSetBase.program.duration.title]
            } else
                 durations = programSetItem.collect {e -> e.duration.title}

            RtfString builder = new RtfString().append(programSetBase.programSubject.getTitleWithCode());
            if (!programSetItem.isEmpty() && programSetItem.get(0) instanceof EduProgramHigherProf)
            {
                EduProgramSpecialization specialization = ((EduProgramHigherProf) programSetItem.get(0)).getProgramSpecialization();
                if (specialization instanceof EduProgramSpecializationChild)
                        builder.append(programSetBase instanceof EnrProgramSetMaster? ", программа " : ", профиль ").append(specialization.getDisplayableTitle())

            }

            String bachelorType = getParagraphBachelorTypeStr(paragraph)
            // заполняем метки в шаблоне параграфа
            def injectModifier = new RtfInjectModifier()
                    .put('parNumber', ++counter as String)
                    .put('developPeriod', durations.empty?"" : ", " + CommonBaseStringUtil.joinNotEmpty(durations, ", "))
                    .put('educationOrgUnit', builder)
                    .put('bachelorType', bachelorType.empty?"": ", " + bachelorType)

            def programKindCode = paragraph.programSubject.eduProgramKind.code
            def types = EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode) ?
                    UniRtfUtil.SPECIALITY_CASES : (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode) ?
                    UniRtfUtil.EDU_PROFESSION_CASES : UniRtfUtil.EDU_DIRECTION_CASES)
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                injectModifier.put('educationType' + UniRtfUtil.CASE_POSTFIX.get(i), types[i])
            }
            RtfTable headerDoc = headerTable.clone
            paragraphList.add(headerDoc)
            paragraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
            injectModifier.modify(paragraphList)

            List<String[]> studentList = new ArrayList<String[]>()
            int count = 1
            for (def extract : enrollmentExtractList)
            {
                ArrayList<String> row = new ArrayList<String>();
                def requestedEnrollmentCompetition = (EnrRequestedCompetition) extract.entity
                def person = requestedEnrollmentCompetition.request.entrant.person
                EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())
                def sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble()

                row.add(String.valueOf(count++))
                row.add(person.fullFio)
                row.add(requestedEnrollmentCompetition.request.entrant.personalNumber)
                row.add(sumMark ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark) : "")

                String code = paragraph.competitionType.code
                if (code.equals(EnrCompetitionTypeCodes.EXCLUSIVE))
                {
                    row.add(((EnrRequestedCompetitionExclusive)requestedEnrollmentCompetition).benefitCategory.title)
                }
                else if (code.equals(EnrCompetitionTypeCodes.TARGET_ADMISSION))
                {
                    row.add(((EnrRequestedCompetitionTA)requestedEnrollmentCompetition).targetAdmissionOrgUnit.titleWithLegalForm)
                }

                studentList.add(row.toArray(new String[row.size()]))
            }

            tableModifier.put('STUDENT_LIST', studentList as String[][])
            RtfTable studentDoc = studentTable.clone
            paragraphList.add(studentDoc)
            paragraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
            tableModifier.modify(paragraphList)

            RtfInjectModifier im = new RtfInjectModifier()
            im.put('numP', orderExt?.protocolNumber ?: '______')
            im.put('dateP', orderExt?.protocolDate?.format("dd.MM.yyyy")?: '______')

            RtfTable bottomDoc = bottomTable.clone
            paragraphList.add(bottomDoc)
            paragraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
            paragraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))
            paragraphList.add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
            im.modify(paragraphList)

        }
        return paragraphList
    }

    String getParagraphBachelorTypeStr(EnrEnrollmentParagraph paragraph) {

        def orientationsTitles = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, 'e')
                .predicate(DQLPredicateType.distinct)
                .column(property('i', EnrProgramSetItem.program().programOrientation().code()))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().competition().programSetOrgUnit().programSet().fromAlias('e'), 's')
                .joinEntity('s', DQLJoinType.inner, EnrProgramSetItem.class, 'i', eq(property('s'), property('i', EnrProgramSetItem.L_PROGRAM_SET)))
                .where(eq(property('e', EnrEnrollmentExtract.paragraph()), value(paragraph)))
                .where(eq(property('i', EnrProgramSetItem.program().programOrientation().subjectIndex().code()), value(EduProgramSubjectIndexCodes.TITLE_2013_03)))
                .createStatement(session).<String>list()

        return (orientationsTitles.size() == 1 && orientationsTitles.get(0) == EduProgramOrientationCodes.APPLIED_BACHELOR_2013) ? 'прикладной бакалавриат' : ''
    }

    private static String getProgramFormString(EduProgramForm programForm)
    {
        switch (programForm.getCode())
        {
            case EduProgramFormCodes.OCHNAYA:
                return "очной";
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                return "очно-заочной";
            case EduProgramFormCodes.ZAOCHNAYA:
                return "заочной";
        }
        return '';
    }

    private static RtfTable removeTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);

        if (removeLabel)
        {
            for (RtfRow row : table.getRowList())
            {
                for (RtfCell cell : row.getCellList())
                {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null)
                    {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }

        document.getElementList().remove(table);

        return table;
    }

}
