/* $Id$ */
package uninarfu.scripts.report

import com.google.common.collect.ComparisonChain
import com.google.common.collect.Multimap
import com.google.common.collect.TreeMultimap
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.CoreCollectionUtils
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.MergeType
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.cell.TextAlignment
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.IRtfRowIntercepter
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.DeclinableManager
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes
import org.tandemframework.shared.employeebase.base.entity.Employee
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability
import org.tandemframework.shared.person.base.entity.gen.IdentityCardDeclinabilityGen
import ru.tandemservice.movestudentrmc.entity.*
import ru.tandemservice.movestudentrmc.entity.catalog.codes.MovestudentOrderStatesCodes
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance
import ru.tandemservice.unidip.base.entity.order.DipStuExcludeExtract
import ru.tandemservice.unidip.bso.entity.blank.DipDiplomaBlank
import ru.tandemservice.unidip.bso.entity.catalog.codes.DipBlankStateCodes
import ru.tandemservice.uninarfu.entity.NarfuBSODisposalActVisaItem
import ru.tandemservice.uninarfu.entity.NarfuBSOVisasTemplate

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new DisposalActPrint(
        session: session,                           // сессия
        template: template,                         // шаблон
        blanks: blanks,                             // бланки дипломов и приложений
        blanksToExtracts: blanksToExtracts,         // выписки из приказов об отчислении, соответствующие бланкам (не используется)
        destructionDate: destructionDate,           // дата уничтожения
        visaTemplateId: visaTemplateId,             // шаблон визирования
        employeeId: employeeId,                     // Материально ответственное лицо
        periodFrom: periodFrom,                     // дата с
        periodTo: periodTo                          // дата по
).print()

/**
 * @author Andrey Andreev
 * @since 26.11.2015
 */

class DisposalActPrint
{
    Session session
    byte[] template
    Collection<DipDiplomaBlank> blanks
    Map<DipDiplomaBlank, DipStuExcludeExtract> blanksToExtracts
    Date destructionDate
    Map<Long, String> _issuedBlanks;

    Long visaTemplateId;
    Long employeeId;

    Date periodFrom;
    Date periodTo;

    static final int TABLE_HEADER_SIZE = 3;

    def print()
    {
        String destructionDateStr = DateFormatter.DEFAULT_DATE_FORMATTER.format(destructionDate)

        _issuedBlanks = new TreeMap<>();
        _issuedBlanks.putAll(getIssuedListOrders());
        _issuedBlanks.putAll(getIssuedDocOrders());
        _issuedBlanks.putAll(getIssuedUniOrders());

        Multimap<String, DipDiplomaBlank> blanksByOrgUnits = TreeMultimap.create(
                new Comparator<String>() {
                    @Override
                    int compare(String o1, String o2)
                    {
                        return o1.compareTo(o2);
                    }
                },
                new Comparator<DipDiplomaBlank>() {
                    @Override
                    int compare(DipDiplomaBlank o1, DipDiplomaBlank o2)
                    {
                        return ComparisonChain
                                .start()
                                .compare(o1.getSeria(), o2.getSeria())
                                .compare(o1.getNumber(), o2.getNumber())
                                .compare(o1.getId(), o2.getId())
                                .result();
                    }
                }
        );
        for (DipDiplomaBlank blank : blanks)
        {
            blanksByOrgUnits.put(blank.storageLocation.title, blank);
        }

        IRtfRowIntercepter inter4Blanks = new IRtfRowIntercepter() {

            List<Integer> mergedRows = new ArrayList<>();

            @Override
            void beforeModify(RtfTable table, int currentRowIndex)
            {}

            @Override
            List<IRtfElement> beforeInject(
                    RtfTable table,
                    RtfRow row,
                    RtfCell cell,
                    int rowIndex,
                    int colIndex,
                    String value)
            {
                return null
            }

            @Override
            void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                int cnt = 0;
                for (RtfRow row : newRowList)
                {
                    if (mergedRows.contains(cnt))
                        RtfUtil.unitAllCell(row, 0);
                    cnt++;
                }
            }
        }

        List<String[]> rows = new ArrayList<>()
        int count = 1;
        for (String orgUnit : blanksByOrgUnits.keySet())
        {
            Set<DipDiplomaBlank> blanksInOrgUnit = blanksByOrgUnits.get(orgUnit);

            String[] row = [orgUnit, "", "", "", ""];
            rows.add(row)
            inter4Blanks.mergedRows.add(rows.size() + TABLE_HEADER_SIZE - 1);

            for (DipDiplomaBlank blank : blanksInOrgUnit)
            {
                String reason;
                if (blank.blankState.code.equals(DipBlankStateCodes.DISPOSAL))
                    reason = blank.disposalReason.shortTitle
                else
                {
                    reason = _issuedBlanks.get(blank.id);
                    if (reason == null) reason = "Приказ отсутствует в системе.";
                }
                row = [count++ as String,
                       blank.seria,
                       blank.number,
                       reason,
                       blank.blankState.code.equals(DipBlankStateCodes.ISSUED) ? "" : destructionDateStr];
                rows.add(row)
            }
        }

        RtfDocument document = new RtfReader().read(template);

        List<NarfuBSODisposalActVisaItem> visasList = getVisas(visaTemplateId);
        String mainVisa = "";
        List<String[]> tVisasList = new ArrayList<>();
        List<String[]> tVisas4Sign = new ArrayList<>();
        if (!visasList.isEmpty())
        {
            visasList.each { it ->
                tVisasList.add(getVisaString(it));
            }
            int index = 0;
            visasList.each { it ->
                tVisas4Sign.add(getVisaRow(it, index++));
            }
            mainVisa = tVisasList.get(0)[0];
            tVisasList.remove(0);
        }

        new RtfTableModifier()
                .put("T", rows as String[][])
                .put("T", inter4Blanks)
                .put("TvisasList", tVisasList as String[][])
                .put("Tvisas4Sign", tVisas4Sign as String[][])
                .put("Tvisas4Sign", getIntercepter4VisasWhithSigns())
                .modify(document);

        String emptyDate = "«__»\u00A0___________\u00A020__\u00A0г.";
        String employeeStr = "";
        if (employeeId != null)
        {
            Employee employee = (Employee) session.get(Employee.class, employeeId);
            employeeStr = employee.person.identityCard.iof;
        }
        final rector = TopOrgUnit.instance.head

        String visasOrder ="";
        if( visaTemplateId != null)
        {
            NarfuBSOVisasTemplate visasTemplate = (NarfuBSOVisasTemplate) session.get(NarfuBSOVisasTemplate.class, visaTemplateId);
            if(visasTemplate!=null)
                visasOrder = visasTemplate.order;
        }

        new RtfInjectModifier()
                .put('fioRector', rector != null ? rector.employee.person.identityCard.fio : '')
                .put('dataForm', 'от ' + RussianDateFormatUtils.getDateFormattedWithMonthName(new Date()) + ' г.')
                .put('dipTypeNNumber', getDipTypesNumbers())
                .put('orderDate', emptyDate)
                .put('orderNumber', "№____")
                .put('periodFrom', periodFrom != null ? RussianDateFormatUtils.getDateFormattedWithMonthName(periodFrom) + '\u00A0г.' : emptyDate)
                .put('periodTo', periodTo != null ? RussianDateFormatUtils.getDateFormattedWithMonthName(periodTo) + '\u00A0г.' : emptyDate)
                .put('mainVisa', mainVisa)
                .put('employee', employeeStr)
                .put('commOrder', visasOrder)
                .modify(document);

        return [document: RtfUtil.toByteArray(document), fileName: 'disposalAct.rtf'];
    }

    String getDipTypesNumbers()
    {
        Map<String, Integer> dipTypesMap = new TreeMap<>();
        for (DipDiplomaBlank blank : blanks)
        {
            String title = blank.blankType.title;
            Integer number = dipTypesMap.get(title);
            if (number == null)
                dipTypesMap.put(title, 1);
            else
                dipTypesMap.put(title, ++number);
        }

        List<String> dipTypesList = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : dipTypesMap.entrySet())
            dipTypesList.add(entry.key + " - " + (entry.value as String));

        return StringUtils.join(dipTypesList, ", ");
    }


    Map<Long, String> getIssuedListOrders()
    {
        String lolr_alias = "lolr";
        String rlrs_alias = "rlrs";
        String di_alias = "di";
        List<Object[]> raws = new DQLSelectBuilder()
                .fromEntity(ListOrdListRepresent.class, lolr_alias)
                .where(eqValue(property(lolr_alias, ListOrdListRepresent.order().state().code()), MovestudentOrderStatesCodes.CODE_5))
                .where(or(eqValue(property(lolr_alias, ListOrdListRepresent.representation().type().code()),
                                  RepresentationTypeCodes.DIPLOM_AND_EXCLUDE),
                          eqValue(property(lolr_alias, ListOrdListRepresent.representation().type().code()),
                                  RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST)))

                .joinEntity(lolr_alias, DQLJoinType.inner, RelListRepresentStudents.class, rlrs_alias,
                            eq(property(lolr_alias, ListOrdListRepresent.representation()),
                               property(rlrs_alias, RelListRepresentStudents.representation())))

                .fromEntity(DiplomaIssuance.class, di_alias)
                .where(eq(property(rlrs_alias, RelListRepresentStudents.student()),
                          property(di_alias, DiplomaIssuance.diplomaObject().student())))

                .column(property(di_alias, DiplomaIssuance.blank().id()))
                .column(property(lolr_alias, ListOrdListRepresent.order()))
                .column(property(rlrs_alias, RelListRepresentStudents.student().person().identityCard()))
                .where(DQLExpressions.in(property(di_alias, DiplomaIssuance.blank()), blanks))

                .createStatement(session).list();

        Map<Long, String> map = new TreeMap();

        for (Object[] raw : raws)
        {
            ListOrder order = (ListOrder) raw[1];
            String reason = getReason((IdentityCard) raw[2], order.number, order.commitDate);
            map.put((Long) raw[0], reason);
        }

        return map;
    }

    Map<Long, String> getIssuedDocOrders()
    {
        String dor_alias = "dor";
        String drsb_alias = "drsb";
        String di_alias = "di";
        List<Object[]> raws = new DQLSelectBuilder()
                .fromEntity(DocOrdRepresent.class, dor_alias)
                .where(eqValue(property(dor_alias, DocOrdRepresent.order().state().code()), MovestudentOrderStatesCodes.CODE_5))
                .where(or(eqValue(property(dor_alias, DocOrdRepresent.representation().type().code()),
                                  RepresentationTypeCodes.DIPLOM_AND_EXCLUDE),
                          eqValue(property(dor_alias, DocOrdRepresent.representation().type().code()),
                                  RepresentationTypeCodes.DIPLOM_AND_EXCLUDE_LIST)))

                .joinEntity(dor_alias, DQLJoinType.inner, DocRepresentStudentBase.class, drsb_alias,
                            eq(property(dor_alias, DocOrdRepresent.representation()),
                               property(drsb_alias, DocRepresentStudentBase.representation())))

                .fromEntity(DiplomaIssuance.class, di_alias)
                .where(eq(property(drsb_alias, DocRepresentStudentBase.student()),
                          property(di_alias, DiplomaIssuance.diplomaObject().student())))

                .column(property(di_alias, DiplomaIssuance.blank().id()))
                .column(property(dor_alias, DocOrdRepresent.order()))
                .column(property(drsb_alias, DocRepresentStudentBase.student().person().identityCard()))
                .where(DQLExpressions.in(property(di_alias, DiplomaIssuance.blank()), blanks))

                .createStatement(session).list();

        Map<Long, String> map = new TreeMap();

        for (Object[] raw : raws)
        {
            DocumentOrder order = (DocumentOrder) raw[1];
            String reason = getReason((IdentityCard) raw[2], order.number, order.commitDate);
            map.put((Long) raw[0], reason);
        }

        return map;
    }

    Map<Long, String> getIssuedUniOrders()
    {
        Map<Long, String> map = new TreeMap();

        if (blanksToExtracts != null)
            for (DipDiplomaBlank blank : blanksToExtracts.keySet())
            {
                DipStuExcludeExtract dipStuExcludeExtract = blanksToExtracts.get(blank)
                if (dipStuExcludeExtract != null)
                {
                    final order = dipStuExcludeExtract.paragraph.order
                    map.put(blank.id, getReason(dipStuExcludeExtract.entity.person.identityCard, order.number, order.commitDate));
                }
            }

        return map;
    }

    static String getReason(IdentityCard student, String number, Date date)
    {
        String lastName;
        IdentityCardDeclinability icardDeclinability = DataAccessServices.dao().getByNaturalId(new IdentityCardDeclinabilityGen.NaturalId(student));

        if (icardDeclinability != null && icardDeclinability.isLastNameNonDeclinable())
            lastName = student.getLastName();
        else
        {
            CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<String, InflectorVariant>(
                    IdentityCard.lastName().s(), IUniBaseDao.instance.get().getCatalogItem(InflectorVariant.class, InflectorVariantCodes.RU_DATIVE));
            if (icardDeclinability != null)
            {
                Map<CoreCollectionUtils.Pair<String, InflectorVariant>, String> savedDeclinations =
                        DeclinableManager.instance().dao().getDeclinablePropertyToValueMap(icardDeclinability);
                lastName = savedDeclinations.get(lastNamePairKey)
            }
            if (lastName == null)
            {
                Map<CoreCollectionUtils.Pair<String, InflectorVariant>, String> fioDeclinatedByGrammarRulesMap =
                        PersonManager.instance().declinationDao().getFioDeclinatedByGrammarRulesMap(student);
                lastName = fioDeclinatedByGrammarRulesMap.get(lastNamePairKey)
            }
        };

        String name = student.middleName == null ?
                      "${lastName} ${student.firstName.charAt(0)}." :
                      "${lastName} ${student.firstName.charAt(0)}. ${student.middleName.charAt(0)}.";
        return "Выдан ${name} (пр.\u00A0№\u00A0${number} от\u00A0${DateFormatter.DEFAULT_DATE_FORMATTER.format(date)})"
    }

    List<NarfuBSODisposalActVisaItem> getVisas(Long visaTemplateId)
    {
        return new DQLSelectBuilder()
                .fromEntity(NarfuBSODisposalActVisaItem.class, "vi")
                .where(eq(property("vi", NarfuBSODisposalActVisaItem.visaTemplate().id()), value(visaTemplateId)))
                .column(property("vi"))
                .order(property("vi", NarfuBSODisposalActVisaItem.priority()))
                .createStatement(session).list();
    }

    static String[] getVisaString(NarfuBSODisposalActVisaItem visa)
    {
        String[] s = new String[1];
        s[0] = new StringBuilder().append(visa.possibleVisa.entity.person.identityCard.iof)
                                  .append(" - ")
                                  .append(visa.possibleVisa.title)
                                  .toString();
        return s;
    }

    static String[] getVisaRow(NarfuBSODisposalActVisaItem visa, int index)
    {
        String[] s = new String[4];
        switch (index)
        {
            case 0:
                s[0] = "Председатель комиссии:";
                break;
            case 1:
                s[0] = "Члены комиссии:";
                break;
            default: s[0] = "";
        }

        s[1] = visa.possibleVisa.title;
        s[2] = "";
        s[3] = visa.possibleVisa.entity.person.identityCard.iof;
        return s;
    }

    static IRtfRowIntercepter getIntercepter4VisasWhithSigns()
    {
        return new IRtfRowIntercepter() {

            @Override
            void beforeModify(RtfTable table, int currentRowIndex)
            {}

            @Override
            List<IRtfElement> beforeInject(
                    RtfTable table,
                    RtfRow row,
                    RtfCell cell,
                    int rowIndex,
                    int colIndex,
                    String value)
            {
                return null
            }

            @Override
            void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                if (newRowList.isEmpty()) return;

                RtfRow emptyRow = getEmptyRow(newRowList.get(0))

                int i = 0;
                while (i < newRowList.size())
                {
                    newRowList.add(i + 1, emptyRow);
                    i += 2;
                }
            }
        }
    }

    static RtfRow getEmptyRow(RtfRow template)
    {
        RtfRow emptyRow = new RtfRow();

        if (template.cellList.size() < emptyRow.cellList.size()) return emptyRow;

        RtfCell cell;
        RtfString value;

        cell = new RtfCell();
        value = new RtfString().append(IRtfData.QC).append("(#)");
        value.fontSize(16);
        cell.setElementList(value.toList());
        cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
        emptyRow.addCell(cell);

        cell = new RtfCell();
        value = new RtfString().append(IRtfData.QC).append("(должность)");
        cell.setElementList(value.toList());
        cell.setTextAlignment(TextAlignment.TOP);
        emptyRow.addCell(cell);

        cell = new RtfCell();
        value = new RtfString().append(IRtfData.QC).append("(подпись)");
        cell.setElementList(value.toList());
        cell.setTextAlignment(TextAlignment.TOP);
        emptyRow.addCell(cell);

        cell = new RtfCell();
        value = new RtfString().append(IRtfData.QC).append("(расшифровка подписи)");
        cell.setElementList(value.toList());
        cell.setTextAlignment(TextAlignment.TOP);
        emptyRow.addCell(cell);

        for (int i = 0; i < emptyRow.cellList.size(); i++)
        {
            RtfCell empty = emptyRow.cellList.get(i);
            RtfCell first = template.cellList.get(i);
            empty.setCellMargin(first.getCellMargin());
            empty.setCellSpacing(first.getCellSpacing());
            empty.setWidth(first.getWidth());
        }

        return emptyRow;
    }
}
