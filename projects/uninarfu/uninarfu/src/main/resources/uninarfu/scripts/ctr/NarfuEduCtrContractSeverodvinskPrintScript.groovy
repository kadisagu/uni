package uninarfu.scripts.ctr

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramCtrTemplateUtils
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData
import ru.tandemservice.uninarfu.ctrTemplate.support.NarfuContractPrintUtils

/**
 * @author DMITRY KNYAZEV
 * @since 07.07.2015
 */
return new NarfuEduCtrContractSeverodvinskPrint(
        session: session,    // сессия
        template: template,  // шаблон
        customer: DataAccessServices.dao().get(CtrContractVersionContractor.class, customerId),
        provider: DataAccessServices.dao().get(CtrContractVersionContractor.class, providerId),
        versionTemplateData: DataAccessServices.dao().get(EduCtrContractVersionTemplateData.class, versionTemplateDataId),
        eduPromise: DataAccessServices.dao().get(EduCtrEducationPromise.class, eduPromiseId)
).print()

class NarfuEduCtrContractSeverodvinskPrint {
    Session session
    byte[] template
    CtrContractVersionContractor customer
    CtrContractVersionContractor provider
    IEducationContractVersionTemplateData versionTemplateData
    EduCtrEducationPromise eduPromise

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    def print() {
        CtrContractVersion contractVersion = eduPromise.getSrc().getOwner();

        //Add your code here
        EduContractPrintUtils.printVersionData(versionTemplateData, contractVersion, im);
        EduContractPrintUtils.printProviderData((EmployeePostContactor) provider.getContactor(), im);
        EduContractPrintUtils.printAcademyData(im);
        EduProgramCtrTemplateUtils.printEduPromiceData(eduPromise, im);

        NarfuContractPrintUtils.printNarfuPaymentData(DataAccessServices.dao().getList(CtrPaymentPromice.class, CtrPaymentPromice.dst(), provider, CtrPaymentPromice.deadlineDate().s()), im);
        NarfuContractPrintUtils.printNarfuCustomerData(customer.getContactor(), im);
        NarfuContractPrintUtils.printNarfuEduPromiceData(eduPromise, im);
        NarfuContractPrintUtils.printNarfuVersionData(versionTemplateData, contractVersion, im);
        //
        CommonBaseRenderer renderer = new CommonBaseRenderer()
                .rtf()
                .fileName("Договор ${contractVersion.getNumber()} (Северодвинск).rtf")
                .document(UniRtfUtil.toByteArray(template, im, tm, deleteLabels));
        return [renderer: renderer]
    }
}
