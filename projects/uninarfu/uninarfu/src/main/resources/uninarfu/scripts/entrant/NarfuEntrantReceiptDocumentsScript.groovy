package uninarfu.scripts.entrant

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCardDeclinability
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unibase.UniBaseUtils
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodItem
import ru.tandemservice.unienr14_fis.base.bo.EnrFisSettings.entity.EnrFisSettingsECPeriodKey
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt
import ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Ekaterina Zvereva
 * @since 13.05.2015
 */
return new NarfuEntrantReceiptDocuments(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати

).print()

class NarfuEntrantReceiptDocuments
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()


    final static String MAGISTER_FIS_CODE = "4"
    final static String BACHELOR_FIS_CODE = "2"
    final static String SPEC_FIS_CODE = "5"
    final static String BACHELOR_PRIC_FIS_CODE = "19"
    final static String BACHELOR_SHORT_FIS_CODE = "3"

    final static String BUDGET_FIS_CODE = "14"
    final static String CONTRACT_FIS_CODE = "15"
    final static String SPECIAL_RIGHTS_FIS = "20"
    final static String TARGET_ADM_FIS = "16"

    final static String CORESP_FORM_FIS = "10"
    final static String FULL_TIME_FORM_FIS = "11"
    final static String PART_TIME_FORM_FIS = "12"



    def print()
    {
        def person = entrantRequest.entrant.person

        im.put('inventoryNumber', entrantRequest.stringNumber)
        im.put('FIO', person.identityCard.fullFio)
        im.put('shortFIO', person.identityCard.iof)
        im.put('receiptNumber', entrantRequest.stringNumber)
        im.put('regDate', entrantRequest.regDate.format("dd.MM.yyyy"))
        im.put('bookNumber', entrantRequest.entrant.personalNumber)

        im.put('commissionFio', "_________________")
        im.put('commissionContactPhone', "______________")

        //название формирующего
        // получаем список выбранных направлений приема
        def direction = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "comp")
                .top(1)
                .where(eq(property("comp", EnrRequestedCompetition.request()), value(entrantRequest)))
                   .order(property("comp", EnrRequestedCompetition.priority()))
                    .createStatement(session).<EnrRequestedCompetition>uniqueResult()
        im.put('highSchoolTitle', direction.competition.programSetOrgUnit.formativeOrgUnit.printTitle)

        //ФИО
        def fio
        def identityCardDeclinability = IUniBaseDao.instance.get().get(IdentityCardDeclinability.class, IdentityCardDeclinability.identityCard().id(), person.identityCard.id)
        if (identityCardDeclinability != null)
        {
            def lastName = person.identityCard.lastName
            if (identityCardDeclinability.lastNameDeclinable)
                lastName = PersonManager.instance().declinationDao().getDeclinationLastName(lastName, GrammaCase.GENITIVE, person.isMale())

            def firstName = person.identityCard.firstName
            if (identityCardDeclinability.firstNameDeclinable)
                firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(firstName, GrammaCase.GENITIVE, person.isMale())

            def middleName = person.identityCard.middleName
            if (identityCardDeclinability.middleNameDeclinable)
                middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(middleName, GrammaCase.GENITIVE, person.isMale())

            def result = new ArrayList<String>()
            if (!StringUtils.isEmpty(lastName)) result.add(lastName)
            if (!StringUtils.isEmpty(firstName)) result.add(firstName)
            if (!StringUtils.isEmpty(middleName)) result.add(middleName)

            fio = StringUtils.join(result, ' ')

        } else
        {
            fio = PersonManager.instance().declinationDao().getDeclinationFIO(person.identityCard, GrammaCase.GENITIVE)
        }

        im.put("FIO_G", fio)


        // получаем названия документов согласно настройке 'Используемые документы для подачи в ОУ и их порядок'
        def titles = getTitles()

        int i = 1
        tm.put('T3', titles.collect { [(i++), it] } as String[][])
        if (!EnrRequestTypeCodes.SPO.equals(entrantRequest.type.code))
        {
            //Заполняем расписание экзаменов
            fillExamsData()

            //Заполняем даты сроков подачи оригиналов документов
            fillDatesAndStageEnrCampaign()
        }

        fillEnrollmentCommissionData()

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Расписка абитуриента ${person.identityCard.fullFio}.rtf"]
    }

    def getTitles()
    {
        def idc = entrantRequest.identityCard   // выбранное в заявлении УЛ
        def edu = entrantRequest.eduDocument // выбранный док. об образовании

        // получаем список документов из заявления в порядке приоритета
        def attachments = DQL.createStatement(session, /
                from ${EnrEntrantRequestAttachment.class.simpleName}
                where ${EnrEntrantRequestAttachment.entrantRequest().id()} = ${entrantRequest.id}
                order by ${EnrEntrantRequestAttachment.id()}
        /).<EnrEntrantRequestAttachment> list()

        attachments.sort() // в объекте реализован корректный compareTo() - сначала по типу по настройке порядка документов, затем по дате создания документа

        def titles = new ArrayList<String>()

        titles.add(idc.getDisplayableTitle() + " (копия)")
        titles.add(edu.getDisplayableTitle() + (entrantRequest.eduInstDocOriginalHandedIn ? " (оригинал)" : " (копия)"))

        for (def attachment: attachments) {
            titles.add(attachment.document.displayableTitle + (attachment.originalHandedIn ? " (оригинал)" : " (копия)"))
        }
        return titles
    }

    def fillExamsData()
    {
        // вычисляем дисциплины для сдачи
        List<EnrExamPassDiscipline> examPassDisciplines = new DQLSelectBuilder()
                .fromEntity(EnrExamPassDiscipline.class, "e").column("e")
                .where(eq(property("e", EnrExamPassDiscipline.entrant().id()), value(entrantRequest.entrant.id)))
                .where(eq(property("e", EnrExamPassDiscipline.retake()), value(Boolean.FALSE)))
                .order(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
                .order(property("e", EnrExamPassDiscipline.passForm().code()))
                .createStatement(session).list();

        //если нет экзаменов
        if (examPassDisciplines.isEmpty())
        {
            tm.remove('T1')
            im.put('scheduleAccept', "")
            im.put('doNotNeedExam', new RtfString().append("Абитуриенту не нужно сдавать внутренние вступительные испытания").par())
            return
        }

        // список строк
        final List<String[]> rows = new ArrayList<String[]>();


        Map<EnrCampaignDiscipline, Integer> discCountMap = new HashMap<>();
        for (EnrExamPassDiscipline exam : examPassDisciplines) {
            discCountMap.put(exam.getDiscipline(), UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline())) + 1);
        }

        int i = 1;
        // для каждой дисциплины для сдачи
        for (def exam : examPassDisciplines)
        {
            int discCount = UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline()));
            boolean printForm = discCount > 1;
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrExamGroupScheduleEvent.class, "ev")
                    .where(eq(property("ev", EnrExamGroupScheduleEvent.examGroup()), value(exam.examGroup)))
            .where(eq(property("ev", EnrExamGroupScheduleEvent.examScheduleEvent().discipline()), value(exam.discipline)))
            .order(property("ev", EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin()))

            List<EnrExamGroupScheduleEvent> events = exam.getExamGroup() == null ? Collections.emptyList() :
                    IUniBaseDao.instance.get().getList(builder);


            // формируем строку
            String[] row = new String[4];
            row[0] = i++;
            row[1] = exam.discipline.title + (printForm ? " (" + exam.passForm.title + ")" : "");
            row[2] = events.isEmpty() ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(events.get(0).examScheduleEvent.scheduleEvent.durationBegin)
            row[3] = events.isEmpty() ? "" : DateFormatter.DATE_FORMATTER_TIME.format(events.get(0).examScheduleEvent.scheduleEvent.durationBegin)

            rows.add(row);
        }

        tm.put('T1', rows as String[][]);
        im.put('doNotNeedExam', "")
        im.put('scheduleAccept', "Расписание составлено верно.")
    }


    def fillDatesAndStageEnrCampaign()
    {
        LinkedHashMap<EnrFisSettingsECPeriodKey, List<EnrFisSettingsECPeriodItem>> dataMap = new LinkedHashMap<EnrFisSettingsECPeriodKey, List<EnrFisSettingsECPeriodItem>>();

        //Магистратура бюджет
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), value(MAGISTER_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(BUDGET_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(FULL_TIME_FORM_FIS)))
        List<Date> magBudgetList = builder.createStatement(session).list()

        im.put("magisterBudget", magBudgetList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(magBudgetList)))

        //Магистратура контракт
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), value(MAGISTER_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(CONTRACT_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(FULL_TIME_FORM_FIS)))
        List<Date> magContractList = builder.createStatement(session).list()

        im.put("magisterContract", magContractList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(magContractList)))

        //Бакалавриат/специалитет целевики
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(DQLExpressions.in(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), Arrays.asList(BACHELOR_PRIC_FIS_CODE, BACHELOR_FIS_CODE, BACHELOR_SHORT_FIS_CODE, SPEC_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(TARGET_ADM_FIS)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(FULL_TIME_FORM_FIS)))
        List<Date> bsTargetList = builder.createStatement(session).list()

        im.put("bsTargetBudget", bsTargetList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(bsTargetList)))

        //Бакалавриат/специалитет контракт
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(DQLExpressions.in(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), Arrays.asList(BACHELOR_PRIC_FIS_CODE, BACHELOR_FIS_CODE, BACHELOR_SHORT_FIS_CODE, SPEC_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(CONTRACT_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(FULL_TIME_FORM_FIS)))
        List<Date> bsContractList = builder.createStatement(session).list()

        im.put("bsContract", bsContractList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(bsContractList)))

        //Бакалавриат/специалитет бюджет первый этап
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(DQLExpressions.in(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), Arrays.asList(BACHELOR_PRIC_FIS_CODE, BACHELOR_FIS_CODE, BACHELOR_SHORT_FIS_CODE, SPEC_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(BUDGET_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(FULL_TIME_FORM_FIS)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.size()), value(2)))
        builder.where(eq(property("i", EnrFisSettingsECPeriodItem.stage()), value(1)))
        List<Date> bsFirstList = builder.createStatement(session).list()

        im.put("bsFirstBudget", bsFirstList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(bsFirstList)))


        //Бакалавриат/специалитет бюджет второй этап
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(DQLExpressions.in(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), Arrays.asList(BACHELOR_PRIC_FIS_CODE, BACHELOR_FIS_CODE, BACHELOR_SHORT_FIS_CODE, SPEC_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(BUDGET_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(FULL_TIME_FORM_FIS)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.size()), value(2)))
        builder.where(eq(property("i", EnrFisSettingsECPeriodItem.stage()), value(2)))
        List<Date> bsSecondList = builder.createStatement(session).list()

        im.put("bsSecondBudget", bsSecondList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(bsSecondList)))


        //Магистратура заочники бюджет
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), value(MAGISTER_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(BUDGET_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(CORESP_FORM_FIS)))
        List<Date> magDistanceBudgetList = builder.createStatement(session).list()

        im.put("magisterDistanceBudget", magDistanceBudgetList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(magDistanceBudgetList)))

        //Магистратура заочники контракт
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), value(MAGISTER_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(CONTRACT_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(CORESP_FORM_FIS)))
        List<Date> magDistanceContractList = builder.createStatement(session).list()

        im.put("magisterDistanceContract", magDistanceContractList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(magDistanceContractList)))

        //Бакалавриат/специалитет бюджет заочники
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(DQLExpressions.in(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), Arrays.asList(BACHELOR_PRIC_FIS_CODE, BACHELOR_FIS_CODE, BACHELOR_SHORT_FIS_CODE, SPEC_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(BUDGET_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(CORESP_FORM_FIS)))
        List<Date> bsDistanceBudgetList = builder.createStatement(session).list()

        im.put("bsDistanceBudget", bsDistanceBudgetList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(bsDistanceBudgetList)))

        //Бакалавриат/специалитет бюджет контракт
        builder = new DQLSelectBuilder().fromEntity(EnrFisSettingsECPeriodItem.class, "i")
        builder.column(property("i", EnrFisSettingsECPeriodItem.dateEnd()))
        builder.joinPath(DQLJoinType.inner, EnrFisSettingsECPeriodItem.key().fromAlias("i"), "key")
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.enrOrgUnit().enrollmentCampaign()), value(entrantRequest.entrant.enrollmentCampaign)))
        builder.where(DQLExpressions.in(property("key", EnrFisSettingsECPeriodKey.educationLevel().fisItemCode()), Arrays.asList(BACHELOR_PRIC_FIS_CODE, BACHELOR_FIS_CODE, BACHELOR_SHORT_FIS_CODE, SPEC_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationSource().fisItemCode()), value(CONTRACT_FIS_CODE)))
        builder.where(eq(property("key", EnrFisSettingsECPeriodKey.educationForm().fisItemCode()), value(CORESP_FORM_FIS)))
        List<Date> bsDistanceContractList = builder.createStatement(session).list()

        im.put("bsDistanceContract", bsDistanceContractList.isEmpty()? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(Collections.max(bsDistanceContractList)))

    }

    /**
     * Заполняем данные отборочной комиссии
     */
    def fillEnrollmentCommissionData()
    {
        EnrEnrollmentCommission enrollmentCommission = entrantRequest.enrollmentCommission;

        if (enrollmentCommission == null)
        {
            im.put("executiveSecretaryPost", "")
            im.put("executiveSecretaryFIO", "")
            im.put("commissionContactPhone", "")
            im.put("commissionSchedule", "")
            return
        }

        //Секретарь отборочной комиссии
        EnrEnrollmentCommissionExt commissionExt = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentCommissionExtGen.NaturalId(enrollmentCommission));
        //Должность
        String postTitle = "";
        if (commissionExt && commissionExt.employee)
        {
            postTitle = StringUtils.capitalize(commissionExt.employee.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
            postTitle += StringUtils.uncapitalize(commissionExt.employee.orgUnit.genitiveCaseTitle? commissionExt.employee.orgUnit.genitiveCaseTitle : commissionExt.employee.orgUnit.title)
        }
        im.put("executiveSecretaryPost", postTitle)
        im.put("executiveSecretaryFIO", commissionExt && commissionExt.employee ? commissionExt.employee.fio : "")
        im.put("commissionContactPhone", commissionExt? commissionExt.phoneNumber:"")
        im.put("commissionSchedule", commissionExt? commissionExt.scheduleEnrollmentCommission:"")

    }

}
