package uninarfu.scripts.report

import org.apache.commons.collections15.CollectionUtils
import org.apache.commons.collections15.Transformer
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.entity.EntityComparator
import org.tandemframework.core.entity.EntityOrder
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.MergeType
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfText
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.program.entity.EduProgramProf
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.entity.*
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.OrgUnitPrintTiltleComparator
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement
import ru.tandemservice.unienr14.settings.entity.IEnrExamSetElementValue
import ru.tandemservice.uninarfu.report.bo.NarfuEnrReport.ui.RequestCompetitionListAdd.NarfuEnrReportRequestCompetitionListAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 27.04.2015
 */
return new NarfuEnrReportRequestCompetitionPrint(                     // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()


class NarfuEnrReportRequestCompetitionPrint
{
    Session session
    byte[] template
    NarfuEnrReportRequestCompetitionListAddUI model
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()


    def print()
    {
        def document = createReport(new RtfReader().read(template))
        return [document: RtfUtil.toByteArray(document), fileName: 'NarfuEnrReportRequestCompetitionList.rtf']
    }

    private RtfDocument createReport(RtfDocument document)
    {
        RtfTable headerTable = copyTable(document, "header", true)
        RtfTable orgUnitTable = copyTable(document, "orgUnit", false)
        RtfTable entrantTable = copyTable(document, "T", false)
        RtfTable signTable = copyTable(document, "delegatePost", false)
        // и чистим шаблон, чтобы не было ненужных переводов строк. Предварительно копируем заголовок
        document.getElementList().clear()
        document.getElementList().add(headerTable)
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD))
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR))

        // Выбираем абитуриентов и подразделения, под параметры отчета
        Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>()
        EnrEntrantDQLSelectBuilder requestedCompDQL = prepareEntrantDQL(model, true)
        requestedCompDQL.order(property(EnrRatingItem.position().fromAlias(requestedCompDQL.rating())))
        requestedCompDQL.column(requestedCompDQL.rating())

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>()
        List<EnrEntrant> entrants = new ArrayList<>();
        List<OrgUnit> orgUnitList = new ArrayList<>()
        for (EnrRatingItem ratingItem : requestedCompDQL.createStatement(getSession()).<EnrRatingItem> list())
        {
            EnrCompetition competition = ratingItem.competition
            OrgUnit orgUnit = competition.programSetOrgUnit.formativeOrgUnit
            SafeMap.safeGet(entrantMap, ratingItem.competition, ArrayList.class).add(ratingItem)
            entrants.add(ratingItem.entrant)
            nonEmptyBlocks.add(competition.programSetOrgUnit)
            if (!orgUnitList.contains(orgUnit))
            {
                orgUnitList.add(orgUnit)
            }
        }

        // После применения фильтров утили к общему массиву конкурсов мы получаем отфильтрованный массив конкурсов
        // (не путать с массивом выбранных конкурсов, о котором уже было выше написано).
        Collection<EnrCompetition> competitions = model.competitionFilterAddon.getFilteredList()

        Map<OrgUnit, List<EnrCompetition>> orgUnitMap = new HashMap<>()
        for (EnrCompetition competition : competitions)
        {
            SafeMap.safeGet(orgUnitMap, competition.programSetOrgUnit.formativeOrgUnit, ArrayList.class).add(competition)
        }
        if (!model.isSkipEmptyOrgUnit())
        {
            orgUnitList = new ArrayList<>(orgUnitMap.keySet())
        }

        // Убираем конкурсы, которые никто не выбрал
        if (model.isSkipEmptyCompetition())
        {
            competitions = entrantMap.keySet();
        }


        orgUnitList.sort(new OrgUnitPrintTiltleComparator())

        // Сортировка конкурсов (таблиц внутри списка) - по коду вида приема (т.е. вначале бюджетные потом внебюджетные: без ВИ КЦП, квота особых прав, и т.д.)
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<EnrCompetition>(new EntityOrder(EnrCompetition.type().code().s())));

        // Полученный массив конкурсов мы группируем по ключу: набор ОП, филиал, формирующее подразделение.
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList)
        {
            // Убираем пустые таблицы
            if (model.isSkipEmptyList() && !nonEmptyBlocks.contains(competition.getProgramSetOrgUnit())) continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        List<EnrProgramSetOrgUnit> programSetOrgUnitList = getSortedProgramSetOrgUnitList(blockMap)

        Map<OrgUnit, List<EnrProgramSetOrgUnit>> programByOrgUnitMap = getProgramByOrgUnitMap(programSetOrgUnitList)

        //результаты ВИ абитуриента:
        Map<EnrRequestedCompetition, Map<String, EnrChosenEntranceExam>> entrantExamMap = getEntrantExamMap()

        // подгрузим наборы ВИ наших конкурсов
        Set<Long> examSetVariantIds = new HashSet<>();
        for (EnrCompetition competition : competitions)
        {
            examSetVariantIds.add(competition.getExamSetVariant().getId());
        }

        Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent = EnrExamSetManager.instance().dao().getExamSetContent(examSetVariantIds)
        // подгрузим образовательные программы наборов
        Collection<EnrProgramSetBase> programSets = CollectionUtils.collect(programSetOrgUnitList, new Transformer<EnrProgramSetOrgUnit, EnrProgramSetBase>() {
            @Override
            EnrProgramSetBase transform(EnrProgramSetOrgUnit enrProgramSetOrgUnit)
            {
                return enrProgramSetOrgUnit.programSet;
            }
        })

        Map<EnrProgramSetBase, List<EduProgramProf>> programMap = SafeMap.get(ArrayList.class)
        for (EnrProgramSetItem item : DataAccessServices.dao().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSets))
        {
            programMap.get(item.getProgramSet()).add(item.getProgram())
        }
        for (EnrProgramSetBase programSetBase : programSets)
        {
            if (programSetBase instanceof EnrProgramSetSecondary)
            {
                programMap.get(programSetBase).add(((EnrProgramSetSecondary) programSetBase).getProgram())
            }
        }

        Map<EnrEntrant, List<EnrEntrantAchievement>> entrantAchievementMap = getEntrantAchievementMap(entrants)

        //отрисовка таблиц по подразделениям
        for (OrgUnit orgUnit : orgUnitList)
        {
            if (orgUnitList.indexOf(orgUnit) > 0)
            {
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            }
            document.getElementList().add(orgUnitTable.clone);
            im.put("orgUnit", orgUnit.printTitle.toUpperCase())
            im.put("date", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.dateTo))

            for (EnrProgramSetOrgUnit programSetOrgUnit : programByOrgUnitMap.get(orgUnit))
            {
                List<EduProgramProf> programs = programMap.get(programSetOrgUnit.getProgramSet());
                boolean printOP = (model.isReplaceProgramSetTitle() && programs.size() == 1) || (programSetOrgUnit.getProgramSet() instanceof EnrProgramSetSecondary);
                fillEntrantTable(document, entrantTable, programSetOrgUnit, examSetContent, blockMap.get(programSetOrgUnit),
                        entrantMap, entrantExamMap, entrantAchievementMap, model.addBenefitsToComment, printOP, programs)
            }


            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(signTable.clone);
            String postTitle = ""
            if (model.secretary)
            {
                postTitle = StringUtils.capitalize(model.secretary.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
                postTitle += StringUtils.uncapitalize(model.secretary.orgUnit.accusativeCaseTitle ? model.secretary.orgUnit.accusativeCaseTitle : model.secretary.orgUnit.title)
            }
            im.put("delegatePost", postTitle)
            im.put("delegateFio", model.secretary ? model.secretary.fio : "")
            im.modify(document)
        }

        return document
    }

    private void fillEntrantTable(RtfDocument document, RtfTable entrantTable, EnrProgramSetOrgUnit programSetOrgUnit, Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent,
                                  List<EnrCompetition> competitionList, Map<EnrCompetition, List<EnrRatingItem>> entrantMap,
                                  Map<EnrRequestedCompetition, Map<String, EnrChosenEntranceExam>> entrantExamMap,
                                  Map<EnrEntrant, List<EnrEntrantAchievement>> entrantAchievementMap, boolean addBenefits, boolean printOP, List<EduProgramProf> programs)
    {
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
        document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        document.getElementList().add(entrantTable.clone)

        Map<String, String> disciplineMap = getDisciplineMap(programSetOrgUnit, examSetContent)
        String convention = getConvention(disciplineMap)

        im.put("programForm", programSetOrgUnit.programSet.programForm.title)
        im.put("programSetOrProgram", printOP ? "Образовательная программа" : "Набор ОП")
        im.put("programSet", printOP ? programs.get(0).getTitleAndConditionsShort() : programSetOrgUnit.getProgramSet().getTitle())
        im.put("budgetCount", programSetOrgUnit.ministerialPlan.toString())
        im.put("exclusiveCount", programSetOrgUnit.exclusivePlan.toString())
        im.put("taCount", programSetOrgUnit.targetAdmPlan.toString())
        im.put("convention", convention)
        im.modify(document)


        List<String[]> tableContent = new ArrayList<>();
        Set<Integer> compHeaderIdx = new HashSet<>()
        int lastIdx = 0

        int entrantNumber = 1
        for (EnrCompetition competition : competitionList)
        {

            compHeaderIdx.add(lastIdx)
            tableContent.add([getFormattedCompetitionType(competition), "", "", "", "", "", "", "", "", ""] as String[])
            for (EnrRatingItem ratingItem : entrantMap.get(competition))
            {
                Map<String, EnrChosenEntranceExam> choosenExamMap = entrantExamMap.get(ratingItem.requestedCompetition)
                tableContent.add(getEntrantRow(ratingItem, entrantNumber, disciplineMap, choosenExamMap, entrantAchievementMap.get(ratingItem.entrant), addBenefits) as String[])
                entrantNumber++
            }
            lastIdx += 1 + (entrantMap.get(competition) == null ? 0 : entrantMap.get(competition).size())
        }
        List<String> disciplineList = new ArrayList<>(disciplineMap.values())
        tm.put("T", tableContent.toArray(new String[tableContent.size()][]))
                .put("T", getBaseIntercepter(compHeaderIdx, disciplineList))
                .modify(document);
    }

    private static List<String> getEntrantRow(EnrRatingItem ratingItem, int entrantNumber, Map<String, String> disciplineMap, Map<String, EnrChosenEntranceExam> choosenExamMap,
                                              List<EnrEntrantAchievement> entrantAchievementList, boolean addBenefits)
    {
        List<String> row = new ArrayList<>()
        EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition()

        row.add(String.valueOf(entrantNumber))
        row.add(ratingItem.entrant.fullFio)
        row.add(reqComp.request.eduInstDocOriginalHandedIn ? "оригинал" : "копия")

        String achMark = ""
        double achMarkForTotal = 0d
        for (EnrEntrantAchievement achievement : entrantAchievementList)
        {
            achMark += achMark.length() > 0 ? ", " : ""
            achMark += achievement.mark > 0 ? DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(achievement.mark) : ""
            achMarkForTotal += achievement.mark
        }

        row.add(ratingItem.getTotalMarkAsString())
        for (String disciplineTitle : disciplineMap.keySet())
        {
            EnrChosenEntranceExam chosenEntranceExam = choosenExamMap.get(disciplineTitle)
            if (chosenEntranceExam != null)
            {
                row.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(chosenEntranceExam.getMarkAsDouble()))
            }
            else
            {
                row.add("-")
            }
        }
        row.add(achMark)
        row.add(reqComp.request.benefitCategory != null ? "да" : "")

        // примечание
        StringBuilder info = new StringBuilder();

        if (!StringUtils.isEmpty(reqComp.request.entrant.additionalInfo))
        {
            info.append(reqComp.request.entrant.additionalInfo)
        }
        if (reqComp instanceof EnrRequestedCompetitionNoExams)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Выбранная ОП: ").append(((EnrRequestedCompetitionNoExams) reqComp).programSetItem.program.shortTitle);
        }
        if (addBenefits && reqComp.getCompetition().getType().getCode().equals(EnrCompetitionTypeCodes.EXCLUSIVE) && reqComp instanceof IEnrEntrantBenefitStatement)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Особое право: ").append(((IEnrEntrantBenefitStatement) reqComp).benefitCategory.shortTitle);
        }
        if (reqComp.getRequest().getBenefitCategory() != null)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Преимущественное право: ").append(reqComp.request.benefitCategory.shortTitle);
        }
        // инд. достижения
        if (ratingItem.achievementMarkAsLong > 0)
        {
            if (info.length() > 0) info.append("\\par ");
            info.append("Индивид. достижения - ").append(ratingItem.achievementMarkAsString);
        }

        row.add(info.toString());

        return row
    }

    private Map<EnrRequestedCompetition, Map<String, EnrChosenEntranceExam>> getEntrantExamMap()
    {
        Map<EnrRequestedCompetition, Map<String, EnrChosenEntranceExam>> entrantExamMap = SafeMap.get(HashMap.class);

        EnrEntrantDQLSelectBuilder examDQL = prepareEntrantDQL(model, false);
        examDQL.fromEntity(EnrChosenEntranceExam.class, "exam");
        examDQL.where(eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("exam")), property(examDQL.reqComp())));
        examDQL.column("exam");
        for (EnrChosenEntranceExam exam : examDQL.createStatement(getSession()).<EnrChosenEntranceExam> list())
        {
            if (exam.getActualExam() != null && exam.getMarkAsDouble() != null)
            {
                entrantExamMap.get(exam.getRequestedCompetition()).put(exam.getActualExam().examSetElement.value.title, exam);
            }
        }
        return entrantExamMap
    }

    private static EnrEntrantDQLSelectBuilder prepareEntrantDQL(NarfuEnrReportRequestCompetitionListAddUI model, boolean fetch)
    {
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(fetch, true)
                .notArchiveOnly()
                .filter(model.dateFrom, model.dateTo)
                .filter(model.enrollmentCampaign)

        //requestedCompDQL.where(eq(property(requestedCompDQL.rating(), EnrRatingItem.statusRatingPositive()), value(Boolean.TRUE)));
        if (model.isIncludeTookAwayDocuments())
        {
            requestedCompDQL.where(ne(property(EnrRequestedCompetition.state().code().fromAlias(requestedCompDQL.reqComp())), value(EnrEntrantStateCodes.OUT_OF_COMPETITION)));
        }
        else
        {
            requestedCompDQL.defaultActiveFilter()
        }

        //Отборочные комиссии
        if (model.isEnrollmentCommissionActive())
            requestedCompDQL.where(DQLExpressions.in(property(EnrRequestedCompetition.request().enrollmentCommission().fromAlias(requestedCompDQL.reqComp())), model.enrollmentCommissionList))

        model.getCompetitionFilterAddon().applyFilters(requestedCompDQL, requestedCompDQL.competition());

        if (model.isParallelOnly())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.TRUE)))
        }
        if (model.isSkipParallel())
        {
            requestedCompDQL.where(eq(property(EnrRequestedCompetition.parallel().fromAlias(requestedCompDQL.reqComp())), value(Boolean.FALSE)))
        }
        return requestedCompDQL;
    }


    private static RtfTable copyTable(RtfDocument document, String label, boolean removeLabel)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), label);
        if (removeLabel)
        {
            for (RtfRow row : table.getRowList())
            {
                for (RtfCell cell : row.getCellList())
                {
                    if (UniRtfUtil.findElement(cell.getElementList(), label) != null)
                    {
                        UniRtfUtil.fillCell(cell, "");
                    }
                }
            }
        }
        document.getElementList().remove(table);
        return table;
    }

    private static Map<OrgUnit, List<EnrProgramSetOrgUnit>> getProgramByOrgUnitMap(List<EnrProgramSetOrgUnit> programSetOrgUnitList)
    {
        Map<OrgUnit, List<EnrProgramSetOrgUnit>> programByOrgUnitMap = new HashMap<>()
        for (EnrProgramSetOrgUnit programSetOrgUnit : programSetOrgUnitList)
        {
            SafeMap.safeGet(programByOrgUnitMap, programSetOrgUnit.formativeOrgUnit, ArrayList.class).add(programSetOrgUnit);
        }
        return programByOrgUnitMap
    }

    // Сортировка списков -
    // вначале по филиалу (вначале головная организация, потом остальные по названию),
    // затем по форме обучения (по коду),
    // затем по виду ОП,
    // затем по направлению подготовки,
    // затем по набору ОП (по названию)
    private static List<EnrProgramSetOrgUnit> getSortedProgramSetOrgUnitList(Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap)
    {
        List<EnrProgramSetOrgUnit> programSetOrgUnitList = new ArrayList<>();
        programSetOrgUnitList.addAll(blockMap.keySet());
        Collections.sort(programSetOrgUnitList, new Comparator<EnrProgramSetOrgUnit>() {
            @Override
            int compare(EnrProgramSetOrgUnit o1, EnrProgramSetOrgUnit o2)
            {
                int result;
                result = -Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
                if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
                if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
                if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
                if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
                return result;
            }
        })
        return programSetOrgUnitList
    }

    private static String getConvention(Map<String, String> disciplineMap)
    {
        String convention = disciplineMap.size() > 0 ? "*Условные обозначения:" : ""
        for (String title : disciplineMap.keySet())
        {
            convention += title + " - " + disciplineMap.get(title) + "; "
        }
        return convention
    }

    //Полное и сокращенное названия дисциплины
    private static Map<String, String> getDisciplineMap(EnrProgramSetOrgUnit programSetOrgUnit, Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent)
    {
        List<EnrExamSetVariant> enrExamSetVariantList = DataAccessServices.dao().getList(EnrExamSetVariant.class, EnrExamSetVariant.owner(), programSetOrgUnit.programSet)

        Map<String, String> disciplineMap = new TreeMap<>()
        for (EnrExamSetVariant examSet : enrExamSetVariantList)
        {
            for (IEnrExamSetDao.IExamSetElementSettings elementSettings : examSetContent.get(examSet.getId()).elementList)
            {
                IEnrExamSetElementValue element = elementSettings.element.value
                if (disciplineMap.get(element.title) == null)
                {
                    disciplineMap.put(element.title, element.shortTitle)
                }
            }
        }
        return disciplineMap
    }


    private static RtfRowIntercepterBase getBaseIntercepter(Set<Integer> compHeaderIdx, List<String> disciplineList)
    {
        return new RtfRowIntercepterBase() {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (compHeaderIdx.contains(rowIndex))
                {
                    cell.setMergeType(colIndex == 0 ? MergeType.HORIZONTAL_MERGED_FIRST : MergeType.HORIZONTAL_MERGED_NEXT)

                    return new RtfString().boldBegin().append(value).boldEnd().toList()
                }

                List<IRtfElement> list = new ArrayList<>();
                IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                text.setRaw(true);
                list.add(text);
                return list;
            }

            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {

                int[] scales = new int[disciplineList.size()];
                Arrays.fill(scales, 1)
                // разбиваем ячейку заголовка таблицы для названий оценок на нужное число элементов
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 4, new IRtfRowSplitInterceptor() {
                    @Override
                    void intercept(RtfCell newCell, int index)
                    {
                        String content = disciplineList.get(index)
                        newCell.getElementList().addAll(new RtfString().append(IRtfData.QC).append(content).toList());
                    }
                }, scales);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 4, null, scales);
            }
        }
    }

    Map<EnrEntrant, List<EnrEntrantAchievement>> getEntrantAchievementMap(List<EnrEntrant> entrants)
    {
        Map<EnrEntrant, List<EnrEntrantAchievement>> entrantAchievementMap = new HashMap<>()
        List<EnrEntrantAchievement> achievementList = new DQLSelectBuilder()
                .fromEntity(EnrEntrantAchievement.class, "a")
                .column(property("a"))
                .where(DQLExpressions.in(property("a", EnrEntrantAchievement.entrant()), entrants)).createStatement(session).list()
        for (EnrEntrantAchievement entrantAchievement : achievementList)
        {
            SafeMap.safeGet(entrantAchievementMap, entrantAchievement.getEntrant(), ArrayList.class).add(entrantAchievement)
        }
        return entrantAchievementMap
    }

    private static String getFormattedCompetitionType(EnrCompetition competition)
    {
        String formattedCompetitionType = ""
        switch (competition.type.code)
        {
            case EnrCompetitionTypeCodes.EXCLUSIVE: formattedCompetitionType = "Абитуриенты, имеющие право на прием в пределах квоты приема лиц, имеющих особое право"; break
            case EnrCompetitionTypeCodes.CONTRACT: formattedCompetitionType = "Абитуриенты, участвующие в конкурсе на основании договора"; break
            case EnrCompetitionTypeCodes.MINISTERIAL: formattedCompetitionType = "Абитуриенты, участвующие в конкурсе на общих основаниях"; break
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT: formattedCompetitionType = "Абитуриенты, имеющие право на прием без вступительных испытаний (по договору)"; break
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL: formattedCompetitionType = "Абитуриенты, имеющие право на прием без вступительных испытаний"; break
            case EnrCompetitionTypeCodes.TARGET_ADMISSION: formattedCompetitionType = "Абитуриенты, имеющие право на прием в рамках целевого приема"; break
        }

        return formattedCompetitionType

    }
}