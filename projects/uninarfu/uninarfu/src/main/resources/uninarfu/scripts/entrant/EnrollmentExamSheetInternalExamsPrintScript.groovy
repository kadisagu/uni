package uninarfu.scripts.entrant

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unibase.UniBaseUtils
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt
import ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Ekaterina Zvereva
 * @since 16.06.2015
 */

return new EnrollmentExamInternalExamsSheetPrint(                              // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrant: session.get(EnrEntrant.class, object), //объект печати
).print()

class EnrollmentExamInternalExamsSheetPrint
{
    Session session
    byte[] template
    EnrEntrant entrant
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def card = entrant.person.identityCard

        im.put('FIO', card.fullFio)

        im.put('number', entrant.getPersonalNumber())
        im.put('date', new Date().format('dd.MM.yyyy'))

        fillEnrollmentResults()
        fillEnrollmentCommission()

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Экзаменационный лист абитуриента ${entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillEnrollmentResults()
    {
        // вычисляем дисциплины для сдачи
        List<EnrExamPassDiscipline> examPassDisciplines = new DQLSelectBuilder()
                .fromEntity(EnrExamPassDiscipline.class, "e").column("e")
                .where(eq(property("e", EnrExamPassDiscipline.entrant().id()), value(entrant.id)))
                .where(eq(property("e", EnrExamPassDiscipline.retake()), value(Boolean.FALSE)))
                .order(property("e", EnrExamPassDiscipline.discipline().discipline().title()))
                .order(property("e", EnrExamPassDiscipline.passForm().code()))
                .createStatement(session).list();

        // список строк
        final List<String[]> rows = new ArrayList<String[]>();

        Map<EnrCampaignDiscipline, Integer> discCountMap = new HashMap<>();
        for (EnrExamPassDiscipline exam : examPassDisciplines) {
            discCountMap.put(exam.getDiscipline(), UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline())) + 1);
        }

        Map<String, String> places = new LinkedHashMap<>();

        int i = 1;
        // для каждой дисциплины для сдачи
        for (def exam : examPassDisciplines)
        {
            int discCount = UniBaseUtils.nullToZero(discCountMap.get(exam.getDiscipline()));
            boolean printForm = discCount > 1;
            Long mark = exam.getMarkAsDouble() == null ? null : Math.round(exam.getMarkAsDouble());
            List<EnrExamGroupScheduleEvent> events = exam.getExamGroup() == null ? Collections.emptyList() :
                    IUniBaseDao.instance.get().getList(EnrExamGroupScheduleEvent.class,
                            EnrExamGroupScheduleEvent.examGroup(), exam.getExamGroup(),
                            EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin().s());

            for (EnrExamGroupScheduleEvent event : events)
                places.put(event.examScheduleEvent.examRoom.place.displayableTitle, event.examScheduleEvent.examRoom.place.fullLocationInfo)

            // сформировать строку
            String[] row = new String[8];
            row[0] = i++;
            row[1] = exam.discipline.title
            row[2] = exam.passForm.title
            row[3] = events.collect { it.timeTitle}.grep().join("; ");

            // итоговый балл цифрами и прописью
            row[4] = mark == null ? "" : mark;
            row[5] = mark == null ? "" : NumberSpellingUtil.spellNumberMasculineGender(mark)

            row[6] = "" // events.collect { it.examScheduleEvent.commission }.grep().join("\n"); - можно подставить, если хочется вывести экзаменаторов

            rows.add(row);
        }
        tm.put('T', rows as String[][]);
    }

    def fillEnrollmentCommission()
    {

        //Ищем отборочную комиссию
        EnrEnrollmentCommission enrollmentCommission = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "comp")
                    .top(1).column(property("comp", EnrRequestedCompetition.request().enrollmentCommission()))
                    .where(eq(property("comp", EnrRequestedCompetition.request().entrant()), value(entrant)))
                    .order(property("comp", EnrRequestedCompetition.priority())).createStatement(session).uniqueResult()

        if (enrollmentCommission == null)
        {
            im.put('executiveSecretaryPost', "")
            im.put('executiveSecretaryFio', "")
            im.put('commissionTitle',"")
            return
        }

        im.put('commissionTitle', enrollmentCommission.title)

        //Секретарь отборочной комиссии
        EnrEnrollmentCommissionExt commissionExt = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentCommissionExtGen.NaturalId(enrollmentCommission));
        String postTitle = "";
        if (commissionExt && commissionExt.employee)
        {
            postTitle = StringUtils.capitalize(commissionExt.employee.postRelation.postBoundedWithQGandQL.post.nominativeCaseTitle) + " "
            postTitle += StringUtils.uncapitalize(commissionExt.employee.orgUnit.genitiveCaseTitle? commissionExt.employee.orgUnit.genitiveCaseTitle : commissionExt.employee.orgUnit.title)
        }
        im.put('executiveSecretaryPost', postTitle)
        im.put('executiveSecretaryFio', commissionExt && commissionExt.employee ? commissionExt.employee.fio : "")

    }

}
