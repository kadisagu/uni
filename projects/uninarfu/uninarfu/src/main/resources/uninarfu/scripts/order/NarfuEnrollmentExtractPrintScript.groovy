package uninarfu.scripts.order

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramOrientationCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecializationChild
import ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes
import ru.tandemservice.uniedu.program.entity.EduProgramHigherProf
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetMaster
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary
import ru.tandemservice.unienr14.order.entity.EnrAbstractParagraph
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.uninarfu.entity.EnrEnrollmentCommissionExt
import ru.tandemservice.uninarfu.entity.NarfuEnrOrderExt
import ru.tandemservice.uninarfu.entity.gen.EnrEnrollmentCommissionExtGen

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Ekaterina Zvereva
 * @since 13.08.2015
 */
return new EnrollmentExtractPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        extract: session.get(EnrEnrollmentExtract.class, object), // объект печати
        parNumber: parNumber
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов САФУ
 */
class EnrollmentExtractPrint
{
    Session session
    byte[] template
    EnrEnrollmentExtract extract
    int parNumber
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def order = extract.paragraph.order as EnrOrder
        def paragraph = extract.paragraph as EnrEnrollmentParagraph
        NarfuEnrOrderExt orderExt = DataAccessServices.dao().get(NarfuEnrOrderExt.class, NarfuEnrOrderExt.enrOrder(), order)

        //
        EnrProgramSetBase programSetBase = extract.competition.programSetOrgUnit.programSet;
        List<EduProgramHigherProf> programSetItem = IUniBaseDao.instance.get().<EduProgramHigherProf>getList(new DQLSelectBuilder().fromEntity(EnrProgramSetItem.class, "i")
                .column(property("i", EnrProgramSetItem.program()))
                .top(1)
                .where(eqValue(property("i", EnrProgramSetItem.L_PROGRAM_SET), programSetBase))
        )

        final durations
        if (programSetBase instanceof EnrProgramSetSecondary) {
            durations = [programSetBase.program.duration.title]
        } else
            durations = programSetItem.collect {e -> e.duration.title}

        RtfString builder = new RtfString().append(programSetBase.programSubject.getTitleWithCode());
        if (!programSetItem.isEmpty() && programSetItem.get(0) instanceof EduProgramHigherProf)
        {
            EduProgramSpecialization specialization = ((EduProgramHigherProf) programSetItem.get(0)).getProgramSpecialization();
            if (specialization instanceof EduProgramSpecializationChild)
                builder.append(programSetBase instanceof EnrProgramSetMaster? ", программа " : ", профиль ").append(specialization.getDisplayableTitle())
        }



        def programKindCode = paragraph.programSubject.eduProgramKind.code
        def types = EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode) ?
                UniRtfUtil.SPECIALITY_CASES : (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode) ?
                UniRtfUtil.EDU_PROFESSION_CASES : UniRtfUtil.EDU_DIRECTION_CASES)
        for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
        {
            im.put('educationType' + UniRtfUtil.CASE_POSTFIX.get(i), types[i])
        }

        OrgUnit filial = paragraph.formativeOrgUnit.orgUnitType.code.equals(OrgUnitTypeCodes.BRANCH)? paragraph.formativeOrgUnit : paragraph.formativeOrgUnit.parent
        im.put('filialTitle_G', filial.top || !filial.orgUnitType.code.equals(OrgUnitTypeCodes.BRANCH)? "" : filial.genitiveCaseTitle);
        if (order.requestType.code.equals(EnrRequestTypeCodes.SPO))
        {
            OrgUnit college = paragraph.formativeOrgUnit
            im.put('formativeOrgUnitSPO', new StringBuilder(" ").append(college.genitiveCaseTitle ?: college.printTitle).toString())
            im.put('filialTitle_G', "");
        }
        else im.put('formativeOrgUnitSPO','')


        // заполнение меток
        im.put('orderDate', order.commitDate? order.commitDate.format('dd.MM.yyyy'): "            ")
        im.put('orderNumber', order.number? order.number:"              ")
        im.put('parNumber', parNumber as String)

        im.put('formativeOrgUnit',  StringUtils.capitalize( ((EnrEnrollmentParagraph) extract.paragraph).formativeOrgUnit.title))
        im.put('compensationType', order.compensationType.budget ? ", финансируемые из средств федерального бюджета" : " по договорам об оказании платных образовательных услуг")
        im.put('developForm_G', getProgramFormString(((EnrEnrollmentParagraph)extract.paragraph).getProgramForm()))
        im.put('orderDateStr', order.actionDate? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(order.actionDate) + " года": '________________')
        im.put('developPeriod', durations.empty?"" : ", " + CommonBaseStringUtil.joinNotEmpty(durations, ", "))

        String bachelorType = getParagraphBachelorTypeStr(extract.paragraph)

        im.put('educationOrgUnit', builder)
        im.put('bachelorType', bachelorType.empty?"": ", " + bachelorType)


        im.put('protocolNumber', orderExt?.protocolNumber ?: '______')
        im.put('protocolDate', orderExt?.protocolDate?.format("dd.MM.yyyy")?: '______')

        im.put('iofSecretary', getEnrollmentCommissionSecretary(extract.entity.request.enrollmentCommission))

        //Данные студента
        ArrayList<String> row = new ArrayList<String>();
        List<String[]> studentList = new ArrayList<>(1);
        def requestedEnrollmentCompetition = (EnrRequestedCompetition) extract.entity
        def person = requestedEnrollmentCompetition.request.entrant.person
        EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())
        def sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble()

        row.add(String.valueOf(extract.number))
        row.add(person.fullFio)
        row.add(requestedEnrollmentCompetition.request.entrant.personalNumber)
        row.add(sumMark ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark) : "")

        String code = ((EnrEnrollmentParagraph)extract.paragraph).competitionType.code
        if (code.equals(EnrCompetitionTypeCodes.EXCLUSIVE))
        {
            row.add(((EnrRequestedCompetitionExclusive)requestedEnrollmentCompetition).benefitCategory.title)
        }
        else if (code.equals(EnrCompetitionTypeCodes.TARGET_ADMISSION))
        {
            row.add(((EnrRequestedCompetitionTA)requestedEnrollmentCompetition).targetAdmissionOrgUnit.titleWithLegalForm)
        }
        studentList.add(row.toArray(new String[row.size()]))
        tm.put('STUDENT_LIST', studentList as String[][])


        def document = new RtfReader().read(template)


        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: document, fileName: 'EnrollmentOrder.rtf']
    }

    String getParagraphBachelorTypeStr(EnrAbstractParagraph paragraph) {

        def orientationsTitles = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, 'e')
                .predicate(DQLPredicateType.distinct)
                .column(property('i', EnrProgramSetItem.program().programOrientation().code()))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().competition().programSetOrgUnit().programSet().fromAlias('e'), 's')
                .joinEntity('s', DQLJoinType.inner, EnrProgramSetItem.class, 'i', eq(property('s'), property('i', EnrProgramSetItem.L_PROGRAM_SET)))
                .where(eq(property('e', EnrEnrollmentExtract.paragraph()), value(paragraph)))
                .where(eq(property('i', EnrProgramSetItem.program().programOrientation().subjectIndex().code()), value(EduProgramSubjectIndexCodes.TITLE_2013_03)))
                .createStatement(session).<String>list()

        return (orientationsTitles.size() == 1 && orientationsTitles.get(0) == EduProgramOrientationCodes.APPLIED_BACHELOR_2013) ? 'прикладной бакалавриат' : ''
    }

    private static String getProgramFormString(EduProgramForm programForm)
    {
        switch (programForm.getCode())
        {
            case EduProgramFormCodes.OCHNAYA:
                return "очной";
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                return "очно-заочной";
            case EduProgramFormCodes.ZAOCHNAYA:
                return "заочной";
        }
        return '';
    }

    /**
     * Данные отборочной комиссии
     */
    static String getEnrollmentCommissionSecretary(EnrEnrollmentCommission commission)
    {
        if (commission == null)
            return ""

        //Секретарь отборочной комиссии
        EnrEnrollmentCommissionExt commissionExt = DataAccessServices.dao().getByNaturalId(new EnrEnrollmentCommissionExtGen.NaturalId(commission));
       return (commissionExt && commissionExt.employee) ? commissionExt.employee.person.identityCard.iof : ""

    }

}
