package ru.tandemservice.unimsu_pilot.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimsu_pilot_2x4x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unienr13 отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unienr13") )
				throw new RuntimeException("Module 'unienr13' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unienr13");

		// удалить персистентный интерфейс ru.tandemservice.unienr13.settings.entity.IEnrExamSetElementValue
		{
			// удалить view
			tool.dropView("ienrexamsetelementvalue_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.unienr13.request.entity.IEnrEntrantRequestAttachable
		{
			// удалить view
			tool.dropView("ienrentrantrequestattachable_v");

		}

		// удалить персистентный интерфейс ru.tandemservice.unienr13.request.entity.IEnrEntrantBenefitProofDocument
		{
			// удалить view
			tool.dropView("enrentrntbnftprfdcmnt_v");

		}

		// удалить сущность enrScriptItem
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_c_script_t", "scriptitem_t");

			// удалить таблицу
			tool.dropTable("enr_c_script_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrScriptItem");  // *

		}

		// удалить сущность enrReportRatingTASep
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_rep_rating_ta_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_rep_rating_ta_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrReportRatingTASep");

		}

		// удалить сущность enrReportRatingByED
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_rep_rating_ed_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_rep_rating_ed_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrReportRatingByED");

		}

		// удалить сущность enrReportRatingByCG
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_rep_rating_cg_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_rep_rating_cg_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrReportRatingByCG");

		}

		// удалить сущность enrReportEntrantListForInternalExam
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_rep_entr_list_int_exam_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_rep_entr_list_int_exam_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrReportEntrantListForInternalExam");

		}

		// удалить сущность enrReportEnrollmentCampaignSummary
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_rep_camp_sum_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_rep_camp_sum_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrReportEnrollmentCampaignSummary");

		}

		// удалить сущность enrReportApplicationSummaryByOU
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_rep_app_sum_ou_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_rep_app_sum_ou_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrReportApplicationSummaryByOU");

		}

		// удалить сущность enrEntrantResidenceDistrib
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_entr_resid_dist_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_entr_resid_dist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrEntrantResidenceDistrib");

		}

		// удалить сущность enrEntrantEduInstDistribReport
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_entr_edu_inst_dist_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_entr_edu_inst_dist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrEntrantEduInstDistribReport");

		}

		// удалить сущность enrEntrantAgeDistribReport
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_entr_age_dist_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr_entr_age_dist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrEntrantAgeDistribReport");

		}

		// удалить сущность enrEntrant
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr_entrant_t", "personrole_t");

			// удалить таблицу
			tool.dropTable("enr_entrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrant");  // *

		}

		// удалить сущность enrVisasTemplate
		{
			// удалить таблицу
			tool.dropTable("enrvisastemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrVisasTemplate");

		}

		// удалить сущность enrTargetAdmissionKind
		{
			// удалить таблицу
			tool.dropTable("enr_c_target_adm_kind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrTargetAdmissionKind"); // *

		}

		// удалить сущность enrStateExamType
		{
			// удалить таблицу
			tool.dropTable("enr_c_state_exam_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrStateExamType");  // *

		}

		// удалить сущность enrStateExamSubjectMark
		{
			// удалить таблицу
			tool.dropTable("enr_state_exam_mark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrStateExamSubjectMark");  // *

		}

		// удалить сущность enrStateExamSubject
		{
			// удалить таблицу
			tool.dropTable("enr_c_st_exam_subj_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrStateExamSubject");  // *

		}

		// удалить сущность enrStateExamImportedFile
		{
			// удалить таблицу
			tool.dropTable("enrstateexamimportedfile_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrStateExamImportedFile");

		}

		// удалить сущность enrStateExamCertificate
		{
			// удалить таблицу
			tool.dropTable("enr_state_exam_cert_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrStateExamCertificate"); // *

		}

		// удалить сущность enrSourceInfoAboutUniversity
		{
			// удалить таблицу
			tool.dropTable("enr_c_source_info_about_univ_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrSourceInfoAboutUniversity");  // *

		}

		// удалить сущность enrRequestedProfile
		{
			// удалить таблицу
			tool.dropTable("enr_requested_profile_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrRequestedProfile");

		}

		// удалить сущность enrRequestedDirection
		{
			// удалить таблицу
			tool.dropTable("enr_requested_direction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrRequestedDirection");

		}

		// удалить сущность enrRequestedCompGroup
		{
			// удалить таблицу
			tool.dropTable("enr_requested_comp_grp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrRequestedCompGroup");

		}

		// удалить сущность enrPreliminaryEnrollmentStudent
		{
			// удалить таблицу
			tool.dropTable("enr_pre_student_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrPreliminaryEnrollmentStudent");

		}

		// удалить сущность enrPartnerEduInstitution
		{
			// удалить таблицу
			tool.dropTable("enrpartnereduinstitution_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrPartnerEduInstitution"); // *

		}

		// удалить сущность enrOrgUnit
		{
			// удалить таблицу
			tool.dropTable("enr_orgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrOrgUnit"); // *

		}

		// удалить сущность enrOrderTypeToGroup
		{
			// удалить таблицу
			tool.dropTable("enr_order_type2grp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrOrderTypeToGroup");

		}

		// удалить сущность enrOrderType
		{
			// удалить таблицу
			tool.dropTable("enr_c_order_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrOrderType");

		}

		// удалить сущность enrOrderReasonToBasicsRel
		{
			// удалить таблицу
			tool.dropTable("enr_order_reason2basic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrOrderReasonToBasicsRel");

		}

		// удалить сущность enrOrderReason
		{
			// удалить таблицу
			tool.dropTable("enr_c_order_reason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrOrderReason");

		}

		// удалить сущность enrOrderPrintText
		{
			// удалить таблицу
			tool.dropTable("enr_order_print_text_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrOrderPrintText");

		}

		// удалить сущность enrOrderBasic
		{
			// удалить таблицу
			tool.dropTable("enr_c_order_basic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrOrderBasic");

		}

		// удалить сущность enrOlympiadType
		{
			// удалить таблицу
			tool.dropTable("enr_c_olymp_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrOlympiadType");  // *

		}

		// удалить сущность enrOlympiadSubject
		{
			// удалить таблицу
			tool.dropTable("enr_c_olymp_subj_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrOlympiadSubject");  // *

		}

		// удалить сущность enrOlympiadHonour
		{
			// удалить таблицу
			tool.dropTable("enr_c_olymp_honour_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrOlympiadHonour");  // *

		}

		// удалить сущность enrOlympiadDiploma
		{
			// удалить таблицу
			tool.dropTable("enr_olymp_diploma_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrOlympiadDiploma");  // *

		}

		// удалить сущность enrOlympiad
		{
			// удалить таблицу
			tool.dropTable("enr_c_olymp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrOlympiad"); // *

		}

		// удалить сущность enrExamType
		{
			// удалить таблицу
			tool.dropTable("enr_c_exam_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamType"); // *

		}

		// удалить сущность enrExamSetElement
		{
			// удалить таблицу
			tool.dropTable("enr_exam_set_el_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamSetElement");  // *

		}

		// удалить сущность enrExamSetDiscipline
		{
			// удалить таблицу
			tool.dropTable("enr_exam_set_discipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamSetDiscipline"); // *

		}

		// удалить сущность enrExamSet
		{
			// удалить таблицу
			tool.dropTable("enr_exam_set_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamSet");  // *

		}

		// удалить сущность enrExamScheduleEvent
		{
			// удалить таблицу
			tool.dropTable("enr_exam_sched_event_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamScheduleEvent");  // *

		}

		// удалить сущность enrExamRoomType
		{
			// удалить таблицу
			tool.dropTable("enr_c_exam_room_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamRoomType");  // *

		}

		// удалить сущность enrExamPassForm
		{
			// удалить таблицу
			tool.dropTable("enr_c_exam_pass_form_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamPassForm");  // *

		}

		// удалить сущность enrExamPassDiscipline
		{
			// удалить таблицу
			tool.dropTable("enr_exam_pass_discipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamPassDiscipline");  // *

		}

		// удалить сущность enrExamPassAppeal
		{
			// удалить таблицу
			tool.dropTable("enr_exam_pass_appeal_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamPassAppeal");  // *

		}

		// удалить сущность enrExamGroupSet
		{
			// удалить таблицу
			tool.dropTable("enr_exam_grp_set_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamGroupSet"); // *

		}

		// удалить сущность enrExamGroupScheduleEvent
		{
			// удалить таблицу
			tool.dropTable("enr_exam_group_sch_event_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamGroupScheduleEvent");  // *

		}

		// удалить сущность enrExamGroup
		{
			// удалить таблицу
			tool.dropTable("enr_exam_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrExamGroup");  // *

		}

		// удалить сущность enrEntrantState
		{
			// удалить таблицу
			tool.dropTable("enr_c_entrant_state_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantState");  // *

		}

		// удалить сущность zAttachment
		{
			// удалить таблицу
			tool.dropTable("enr_entr_req_attachment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantRequestAttachment");  // *

		}

		// удалить сущность enrEntrantRequest
		{
			// удалить таблицу
			tool.dropTable("enr_request_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantRequest");  // *

		}

		// удалить сущность enrEntrantRating
		{
			// удалить таблицу
			tool.dropTable("enr_entrant_rating_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrEntrantRating");

		}

		// удалить сущность enrEntrantMarkSourceStateExam
		{
			// удалить таблицу
			tool.dropTable("enr_mark_src_st_exam_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantMarkSourceStateExam");  // *

		}

		// удалить сущность enrEntrantMarkSourceOlympiad
		{
			// удалить таблицу
			tool.dropTable("enr_mark_src_olymp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantMarkSourceOlympiad");  // *

		}

		// удалить сущность enrEntrantMarkSourceExamPass
		{
			// удалить таблицу
			tool.dropTable("enr_mark_src_exam_pass_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantMarkSourceExamPass");  // *

		}

		// удалить сущность enrEntrantMarkSourceAppeal
		{
			// удалить таблицу
			tool.dropTable("enr_mark_src_appeal_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantMarkSourceAppeal");  // *

		}

		// удалить сущность enrEntrantMarkSource
		{
			// удалить таблицу
			tool.dropTable("enr_mark_src_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantMarkSource");  // *

		}

		// удалить сущность enrEntrantInfoAboutUniversity
		{
			// удалить таблицу
			tool.dropTable("enr_entrant_source_info_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantInfoAboutUniversity");  // *

		}

		// удалить сущность enrEntrantDocumentType
		{
			// удалить таблицу
			tool.dropTable("enr_c_entrant_doc_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantDocumentType");  // *

		}

		// удалить сущность enrEntrantBenefitProof
		{
			// удалить таблицу
			tool.dropTable("enr_entrant_benefit_proof_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantBenefitProof");  // *

		}

		// удалить сущность enrEntrantBenefit
		{
			// удалить таблицу
			tool.dropTable("enr_entrant_benefit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrEntrantBenefit");

		}

		// удалить сущность enrEntrantDisabilityDocument
		{
			// удалить таблицу
			tool.dropTable("enr_entrant_dis_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantDisabilityDocument"); // *

		}

		// удалить сущность enrEntrantBaseDocument
		{
			// удалить таблицу
			tool.dropTable("enr_enrtant_base_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantBaseDocument");  // *

		}

		// удалить сущность enrEntrantAccessDepartment
		{
			// удалить таблицу
			tool.dropTable("enr_entrant_acc_dep_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantAccessDepartment");  // *

		}

		// удалить сущность enrEntrantAccessCourse
		{
			// удалить таблицу
			tool.dropTable("enr_entrant_acc_course_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEntrantAccessCourse");  // *

		}

		// удалить сущность enrEnrollmentCampaignSettings
		{
			// удалить таблицу
			tool.dropTable("enr_campaign_settings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEnrollmentCampaignSettings");  // *

		}

		// удалить сущность enrEnrollmentCampaignRequestWizardSettings
		{
			// удалить таблицу
			tool.dropTable("enr_campaign_rwsettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEnrollmentCampaignRequestWizardSettings");  // *

		}

		// удалить сущность enrEnrollmentCampaign
		{
			// удалить таблицу
			tool.dropTable("enr_campaign_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrEnrollmentCampaign");  // *

		}

		// удалить сущность enrEduYearBenefitCategory
		{
			// удалить таблицу
			tool.dropTable("enr_edu_year_benefit_cat_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrEduYearBenefitCategory");

		}

		// удалить сущность enrDiscipline
		{
			// удалить таблицу
			tool.dropTable("enr_c_discipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrDiscipline");  // *

		}

		// удалить сущность enrDisablementType
		{
			// удалить таблицу
			tool.dropTable("enr_c_disablement_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrDisablementType");  // *

		}

		// удалить сущность enrDirectionTargetAdmissionPlan
		{
			// удалить таблицу
			tool.dropTable("enr_direction_ta_plan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrDirectionTargetAdmissionPlan");

		}

		// удалить сущность enrDirectionProfile
		{
			// удалить таблицу
			tool.dropTable("enr_direction_profile_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrDirectionProfile");

		}

		// удалить сущность enrDirectionCompetitionSecond
		{
			// удалить таблицу
			tool.dropTable("enr_direction_comp_second_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrDirectionCompetitionSecond");

		}

		// удалить сущность enrDirectionCompetitionFirst
		{
			// удалить таблицу
			tool.dropTable("enr_direction_comp_first_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrDirectionCompetitionFirst");

		}

		// удалить сущность enrDirectionCompetition
		{
			// удалить таблицу
			tool.dropTable("enr_direction_comp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrDirectionCompetition");

		}

		// удалить сущность enrDirection
		{
			// удалить таблицу
			tool.dropTable("enr_direction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrDirection");

		}

		// удалить сущность enrContractAdmissionKind
		{
			// удалить таблицу
			tool.dropTable("enr_c_contract_adm_kind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrContractAdmissionKind");

		}

		// удалить сущность enrCompetitionType
		{
			// удалить таблицу
			tool.dropTable("enr_c_comp_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrCompetitionType");  // *

		}

		// удалить сущность enrCompetitionGroupExamPassForm
		{
			// удалить таблицу
			tool.dropTable("enr_cg_exam_pass_form_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrCompetitionGroupExamPassForm");

		}

		// удалить сущность enrCompetitionGroupExam
		{
			// удалить таблицу
			tool.dropTable("enr_cg_exam_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrCompetitionGroupExam");

		}

		// удалить сущность enrCompetitionGroup
		{
			// удалить таблицу
			tool.dropTable("enr_comp_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrCompetitionGroup");

		}

		// удалить сущность enrChosenEntranceExamForm
		{
			// удалить таблицу
			tool.dropTable("enr_chosen_entr_exam_form_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrChosenEntranceExamForm"); // *

		}

		// удалить сущность enrChosenEntranceExam
		{
			// удалить таблицу
			tool.dropTable("enr_chosen_entr_exam_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrChosenEntranceExam");  // *

		}

		// удалить сущность enrCampaignTargetAdmissionKind
		{
			// удалить таблицу
			tool.dropTable("enr_camp_target_adm_kind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrCampaignTargetAdmissionKind");  // *

		}

		// удалить сущность enrCampaignOrderVisaItem
		{
			// удалить таблицу
			tool.dropTable("enrcampaignordervisaitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrCampaignOrderVisaItem");

		}

		// удалить сущность enrCampaignOrderType
		{
			// удалить таблицу
			tool.dropTable("enr_camp_order_extract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrCampaignOrderType");

		}

		// удалить сущность enrCampaignExamRoom
		{
			// удалить таблицу
			tool.dropTable("enr_camp_exam_room_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrCampaignExamRoom");  // *

		}

		// удалить сущность enrCampaignEntrantDocument
		{
			// удалить таблицу
			tool.dropTable("enr_camp_entrant_doc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrCampaignEntrantDocument");  // *

		}

		// удалить сущность enrCampaignDisciplineGroupElement
		{
			// удалить таблицу
			tool.dropTable("enr_camp_disc_group_el_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrCampaignDisciplineGroupElement");  // *

		}

		// удалить сущность enrCampaignDisciplineGroup
		{
			// удалить таблицу
			tool.dropTable("enr_camp_disc_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrCampaignDisciplineGroup");  // *

		}

		// удалить сущность enrCampaignDiscipline
		{
			// удалить таблицу
			tool.dropTable("enr_camp_discipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrCampaignDiscipline"); // *

		}

		// удалить сущность enrBenefitType
		{
			// удалить таблицу
			tool.dropTable("enr_c_benefit_type_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrBenefitType");  // *

		}

		// удалить сущность enrBenefitCategory
		{
			// удалить таблицу
			tool.dropTable("enr_c_benefit_category_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrBenefitCategory");  // *

		}

		// удалить сущность enrAccessDepartment
		{
			// удалить таблицу
			tool.dropTable("enr_c_acc_department_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrAccessDepartment");  // *

		}

		// удалить сущность enrAccessCourse
		{
			// удалить таблицу
			tool.dropTable("enr_c_acc_course_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrAccessCourse");  // *

		}

		// удалить сущность enrParagraph
		{
			// удалить таблицу
			tool.dropTable("enr_order_paragraph_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrParagraph");

		}

		// удалить сущность enrAbstractParagraph
		{
			// удалить таблицу
			tool.dropTable("enr_order_paragraph_abstract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrAbstractParagraph");

		}

		// удалить сущность enrOrder
		{
			// удалить таблицу
			tool.dropTable("enr_order_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrOrder");

		}

		// удалить сущность enrAbstractOrder
		{
			// удалить таблицу
			tool.dropTable("enr_order_abstract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrAbstractOrder");

		}

		// удалить сущность enrTargetExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_ta_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrTargetExtract");

		}

		// удалить сущность enrTargetContractExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_ta_ctr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrTargetContractExtract");

		}

		// удалить сущность enrTargetBudgetExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_ta_bdt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrTargetBudgetExtract");

		}

		// удалить сущность enrStudentShContractExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_student_sh_ctr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrStudentShContractExtract");

		}

		// удалить сущность enrStudentShBugetExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_student_sh_bdt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrStudentShBugetExtract");

		}

		// удалить сущность enrStudentOPPExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_student_opp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrStudentOPPExtract");

		}

		// удалить сущность enrStudentExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_student_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrStudentExtract");

		}

		// удалить сущность enrStudentContractExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_student_ctr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrStudentContractExtract");

		}

		// удалить сущность enrStudentBugetExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_student_bdt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrStudentBugetExtract");

		}

		// удалить сущность enrMasterContractExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_master_ctr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrMasterContractExtract");

		}

		// удалить сущность enrMasterBugetExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_master_bdt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrMasterBugetExtract");

		}

		// удалить сущность enrListenerParExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_listener_par_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrListenerParExtract");

		}

		// удалить сущность enrListenerContractExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_listener_ctr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrListenerContractExtract");

		}

		// удалить сущность enrHighShortExtract
		{
			// удалить таблицу
			tool.dropTable("enr_extract_high_short_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrHighShortExtract");

		}

		// удалить сущность enrExtract
		{
			// удалить таблицу
			tool.dropTable("enr_order_extract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrExtract");

		}

		// удалить сущность enrAbstractExtract
		{
			// удалить таблицу
			tool.dropTable("enr_order_extract_abstract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrAbstractExtract");

		}

		// удалить сущность enrAbsenceNote
		{
			// удалить таблицу
			tool.dropTable("enr_c_abs_note_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			// tool.entityCodes().delete("enrAbsenceNote");  // *

		}

        // * - помечены закомментированные строки, в которых происходит удаление кодов сущностей из unienr13, которые понадобятся в unienr14.
    }
}