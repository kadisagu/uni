/* $Id: NsiBean.java 50555 2016-12-02 11:35:48Z dseleznev $ */
package ru.tandemservice.nsiclient;

import ru.tandemservice.nsiclient.datagram.IXDatagram;
import ru.tandemservice.nsiclient.datagram.XDatagram;
import ru.tandemservice.nsiclient.utils.INsiBean;

/**
 * @author Andrey Nikonov
 * @since 01.09.2015
 */
public class NsiBean implements INsiBean {

    public IXDatagram createXDatagram() {
        return new XDatagram();
    }
}