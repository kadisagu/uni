package ru.tandemservice.product_demo.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

import java.sql.SQLException;

import static org.tandemframework.shared.commonbase.utils.MigrationUtils.removeModuleFromVersion_s;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_demo_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0"),
				 new ScriptDependency("ru.tandemservice.bpms", "1.7.0")
		};
    }

    private void deleteCode(DBTool tool, String code) throws SQLException
    {
        tool.entityCodes().delete(code);
        tool.entityCodes().delete(code.toLowerCase());
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        String sql = "(select id from abstractentrantorder_t union all select id from enrollmentrevertextract_t union all select id from abstractentrantextract_t)";

        tool.executeUpdate("delete from visahistoryitem_t where document_id in " + sql + " or visatask_id in (select vt.id from visatask_t vt join visa_t v on vt.visa_id=v.id where v.document_id in " + sql + ")");
        tool.executeUpdate("delete from visatask_t where visa_id in (select id from visa_t where document_id in " + sql + ")");
        tool.executeUpdate("delete from visa_t where document_id in " + sql);

        ////////////////////////////////////////////////////////////////////////////////
        // модуль uniec_fis отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if( ApplicationRuntime.hasModule("uniec_fis") )
                throw new RuntimeException("Module 'uniec_fis' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "uniec_fis");

        // удалить сущность ecfSettingsECPeriodKey
        {
            // удалить таблицу
            tool.dropTable("ecf_s_period_key_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfSettingsECPeriodKey");

        }

        // удалить сущность ecfSettingsECPeriodItem
        {
            // удалить таблицу
            tool.dropTable("ecf_s_period_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfSettingsECPeriodItem");

        }

        // удалить сущность ecfOrgUnitPackageIOLog
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_log_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackageIOLog");

        }

        // удалить сущность ecfOrgUnitPackage4DelApplications
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_delapplications_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackage4DelApplications");

        }

        // удалить сущность ecfOrgUnitPackage4DelAdmissionOrders
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_deladmissionorders_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackage4DelAdmissionOrders");

        }

        // удалить сущность ecfOrgUnitPackage4CampaignInfo
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_compaign_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackage4CampaignInfo");

        }

        // удалить сущность ecfOrgUnitPackage4ApplicationsInfo
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_applications_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackage4ApplicationsInfo");

        }

        // удалить сущность ecfOrgUnitPackage4AdmissionOrders
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_admissionorders_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackage4AdmissionOrders");

        }

        // удалить сущность ecfOrgUnitPackage4AdmissionInfo
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_admission_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackage4AdmissionInfo");

        }

        // удалить сущность ecfOrgUnitPackage
        {
            // удалить таблицу
            tool.dropTable("ecf_oupkg_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnitPackage");

        }

        // удалить сущность ecfOrgUnit
        {
            // удалить таблицу
            tool.dropTable("ecf_orgunit_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfOrgUnit");

        }

        // удалить сущность ecfConv4SetDiscipline
        {
            // удалить таблицу
            tool.dropTable("ecf_conv_setdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfConv4SetDiscipline");

        }

        // удалить сущность ecfConv4Olympiad
        {
            // удалить таблицу
            tool.dropTable("ecf_conv_olympiad_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfConv4Olympiad");

        }

        // удалить сущность ecfConv4EduOu
        {
            // удалить таблицу
            tool.dropTable("ecf_conv_eduou_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfConv4EduOu");

        }

        // удалить сущность ecfCatalogItem
        {
            // удалить таблицу
            tool.dropTable("ecf_catalog_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecfCatalogItem");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // модуль uniec отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if( ApplicationRuntime.hasModule("uniec") )
                throw new RuntimeException("Module 'uniec' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "uniec");

        // удалить персистентный интерфейс ru.tandemservice.uniec.entity.entrant.IEntrant
        {
            // удалить view
            tool.dropView("ientrant_v");

        }

        // удалить персистентный интерфейс ru.tandemservice.uniec.base.entity.ecg.IPersistentEcgItem
        {
            // удалить view
            tool.dropView("ipersistentecgitem_v");

        }

        // удалить сущность uniecScriptItem
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsByEntityCode(tool.entityCodes().get("uniecscriptitem"), "scriptitem_t");
            tool.deleteRowsByEntityCode(tool.entityCodes().get("uniecScriptItem"), "scriptitem_t");

            // таблицы у сущности нет
            // удалить код сущности
            deleteCode(tool, "uniecScriptItem");
        }

        // удалить сущность technicCommissionReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("techniccommissionreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("techniccommissionreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "technicCommissionReport");

        }

        // удалить сущность summaryStateExamMarkReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("summarystateexammarkreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("summarystateexammarkreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "summaryStateExamMarkReport");

        }

        // удалить сущность summarySheetExamReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("summarysheetexamreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("summarysheetexamreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "summarySheetExamReport");

        }

        // удалить сущность splitEntrantsStuListExtract
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("splitentrantsstulistextract_t", "abstractstudentextract_t");

            // удалить таблицу
            tool.dropTable("splitentrantsstulistextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "splitEntrantsStuListExtract");

        }

        // удалить сущность splitContractMasterEntrantsStuListExtract
        {
            // таблицы у сущности нет
            // удалить код сущности
            deleteCode(tool, "splitContractMasterEntrantsStuListExtract");

        }

        // удалить сущность splitContractBachelorEntrantsStuListExtract
        {
            // таблицы у сущности нет
            // удалить код сущности
            deleteCode(tool, "splitContractBachelorEntrantsStuListExtract");

        }

        // удалить сущность splitBudgetMasterEntrantsStuListExtract
        {
            // таблицы у сущности нет
            // удалить код сущности
            deleteCode(tool, "splitBudgetMasterEntrantsStuListExtract");

        }

        // удалить сущность splitBudgetBachelorEntrantsStuListExtract
        {
            // таблицы у сущности нет
            // удалить код сущности
            deleteCode(tool, "splitBudgetBachelorEntrantsStuListExtract");

        }

        // удалить сущность form76KD2008Report
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("form76kd2008report_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("form76kd2008report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "form76KD2008Report");

        }

        // удалить сущность entrantSummaryBulletinReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("entrantsummarybulletinreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("entrantsummarybulletinreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantSummaryBulletinReport");

        }

        // удалить сущность entrantRequestDailyReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("entrantrequestdailyreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("entrantrequestdailyreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRequestDailyReport");

        }

        // удалить сущность entrantRequestByCGDailyReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("ntrntrqstbycgdlyrprt_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("ntrntrqstbycgdlyrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRequestByCGDailyReport");

        }

        // удалить сущность entrantRequestApplicationReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("requestapplicationreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("requestapplicationreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRequestApplicationReport");

        }

        // удалить сущность entrantRegistryForm1Report
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("entrantregistryform1report_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("entrantregistryform1report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRegistryForm1Report");

        }

        // удалить сущность entrantDailyRatingByTAReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("entrantdailyratingbytareport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("entrantdailyratingbytareport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantDailyRatingByTAReport");

        }

        // удалить сущность entrant
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("entrant_t", "personrole_t");

            // удалить таблицу
            tool.dropTable("entrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrant");

        }

        // удалить сущность entranceExaminationMeetingReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("ntrncexmntnmtngrprt_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("ntrncexmntnmtngrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entranceExaminationMeetingReport");

        }

        // удалить сущность enrollmentResults3NKReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("enrollmentresults3nkreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("enrollmentresults3nkreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResults3NKReport");

        }

        // удалить сущность enrollmentDirProfReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("enrollmentdirprofreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("enrollmentdirprofreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentDirProfReport");

        }

        // удалить сущность enrollmentDataCollectReport
        {
            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("enrollmentdatacollectreport_t", "storablereport_t");

            // удалить таблицу
            tool.dropTable("enrollmentdatacollectreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentDataCollectReport");

        }

        // удалить сущность usedEnrollmentDocument
        {
            // удалить таблицу
            tool.dropTable("usedenrollmentdocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "usedEnrollmentDocument");

        }

        // удалить сущность useExternalOrgUnitForTA
        {
            // удалить таблицу
            tool.dropTable("useexternalorgunitforta_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "useExternalOrgUnitForTA");

        }

        // удалить сущность targetAdmissionPlanRelation
        {
            // удалить таблицу
            tool.dropTable("targetadmissionplanrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "targetAdmissionPlanRelation");

        }

        // удалить сущность targetAdmissionKind
        {
            // удалить таблицу
            tool.dropTable("targetadmissionkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "targetAdmissionKind");

        }

        // удалить сущность summaryQuotasMarksResultsReport
        {
            // удалить таблицу
            tool.dropTable("smmryqtsmrksrsltsrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "summaryQuotasMarksResultsReport");

        }

        // удалить сущность summaryEnrollmentResultsReport
        {
            // удалить таблицу
            tool.dropTable("smmryenrllmntrsltsrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "summaryEnrollmentResultsReport");

        }

        // удалить сущность subjectPassWay
        {
            // удалить таблицу
            tool.dropTable("subjectpassway_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "subjectPassWay");

        }

        // удалить сущность subjectPassForm
        {
            // удалить таблицу
            tool.dropTable("subjectpassform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "subjectPassForm");

        }

        // удалить сущность stateExamType
        {
            // удалить таблицу
            tool.dropTable("stateexamtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "stateExamType");

        }

        // удалить сущность stateExamSubjectPassScore
        {
            // удалить таблицу
            tool.dropTable("stateexamsubjectpassscore_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "stateExamSubjectPassScore");

        }

        // удалить сущность stateExamSubjectMark
        {
            // удалить таблицу
            tool.dropTable("stateexamsubjectmark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "stateExamSubjectMark");

        }

        // удалить сущность stateExamSubject
        {
            // удалить таблицу
            tool.dropTable("stateexamsubject_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "stateExamSubject");

        }

        // удалить сущность stateExamImportedFile
        {
            // удалить таблицу
            tool.dropTable("stateexamimportedfile_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "stateExamImportedFile");

        }

        // удалить сущность stateExamEntrantListReport
        {
            // удалить таблицу
            tool.dropTable("stateexamentrantlistreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "stateExamEntrantListReport");

        }

        // удалить сущность stateExamEnrollmentReport2010
        {
            // удалить таблицу
            tool.dropTable("sttexmenrllmntrprt2010_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "stateExamEnrollmentReport2010");

        }

        // удалить сущность sourceInfoAboutUniversity
        {
            // удалить таблицу
            tool.dropTable("sourceinfoaboutuniversity_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "sourceInfoAboutUniversity");

        }

        // удалить сущность disciplinesGroup
        {
            // удалить таблицу
            tool.dropTable("disciplinesgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "disciplinesGroup");

        }

        // удалить сущность discipline2RealizationWayRelation
        {
            // удалить таблицу
            tool.dropTable("dscpln2rlztnwyrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "discipline2RealizationWayRelation");

        }

        // удалить сущность setDiscipline
        {
            // удалить таблицу
            tool.dropTable("setdiscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "setDiscipline");

        }

        // удалить сущность requestedProfileKnowledge
        {
            // удалить таблицу
            tool.dropTable("requestedprofileknowledge_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "requestedProfileKnowledge");

        }

        // удалить сущность requestedEnrollmentDirection
        {
            // удалить таблицу
            tool.dropTable("requestedenrollmentdirection_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "requestedEnrollmentDirection");

        }

        // удалить сущность qualification2CompetitionKindRelation
        {
            // удалить таблицу
            tool.dropTable("qlfctn2cmpttnkndrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "qualification2CompetitionKindRelation");

        }

        // удалить сущность profileKnowledge
        {
            // удалить таблицу
            tool.dropTable("profileknowledge_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "profileKnowledge");

        }

        // удалить сущность profileExaminationMark
        {
            // удалить таблицу
            tool.dropTable("profileexaminationmark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "profileExaminationMark");

        }

        // удалить сущность profileEducationOrgUnit
        {
            // удалить таблицу
            tool.dropTable("profileeducationorgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "profileEducationOrgUnit");

        }

        // удалить сущность priorityProfileEduOu
        {
            // удалить таблицу
            tool.dropTable("priorityprofileeduou_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "priorityProfileEduOu");

        }

        // удалить сущность preliminaryEnrollmentStudent
        {
            // удалить таблицу
            tool.dropTable("preliminaryenrollmentstudent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "preliminaryEnrollmentStudent");

        }

        // удалить сущность orgUnitSelectionSecretary
        {
            // удалить таблицу
            tool.dropTable("orgunitselectionsecretary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "orgUnitSelectionSecretary");

        }

        // удалить сущность orgUnitExecutiveSecretary
        {
            // удалить таблицу
            tool.dropTable("orgunitexecutivesecretary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "orgUnitExecutiveSecretary");

        }

        // удалить сущность orgUnitCommissionChairman
        {
            // удалить таблицу
            tool.dropTable("orgunitcommissionchairman_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "orgUnitCommissionChairman");

        }

        // удалить сущность onlineRequestedEnrollmentDirection
        {
            // удалить таблицу
            tool.dropTable("onlinerequestenrolldirection_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "onlineRequestedEnrollmentDirection");

        }

        // удалить сущность onlineEntrantInfoAboutUniversity
        {
            // удалить таблицу
            tool.dropTable("onlineentrantuniversityinfo_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "onlineEntrantInfoAboutUniversity");

        }

        // удалить сущность onlineEntrantCertificateMark
        {
            // удалить таблицу
            tool.dropTable("onlineentrantcertificatemark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "onlineEntrantCertificateMark");

        }

        // удалить сущность onlineEntrant
        {
            // удалить таблицу
            tool.dropTable("onlineentrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "onlineEntrant");

        }

        // удалить сущность olympiadDiplomaType
        {
            // удалить таблицу
            tool.dropTable("olympiaddiplomatype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "olympiadDiplomaType");

        }

        // удалить сущность olympiadDiplomaDegree
        {
            // удалить таблицу
            tool.dropTable("olympiaddiplomadegree_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "olympiadDiplomaDegree");

        }

        // удалить сущность olympiadDiploma
        {
            // удалить таблицу
            tool.dropTable("olympiaddiploma_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "olympiadDiploma");

        }

        // удалить сущность notUsedTargetAdmissionKind
        {
            // удалить таблицу
            tool.dropTable("notusedtargetadmissionkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "notUsedTargetAdmissionKind");

        }

        // удалить сущность interviewResult
        {
            // удалить таблицу
            tool.dropTable("interviewresult_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "interviewResult");

        }

        // удалить сущность infoSourcesReport
        {
            // удалить таблицу
            tool.dropTable("infosourcesreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "infoSourcesReport");

        }

        // удалить сущность infoEduProfileReport
        {
            // удалить таблицу
            tool.dropTable("infoeduprofilereport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "infoEduProfileReport");

        }

        // удалить сущность idreCode
        {
            // удалить таблицу
            tool.dropTable("idrecode_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "idreCode");

        }

        // удалить сущность group2DisciplineRelation
        {
            // удалить таблицу
            tool.dropTable("group2disciplinerelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "group2DisciplineRelation");

        }

        // удалить сущность formVPO1in2009Report
        {
            // удалить таблицу
            tool.dropTable("formvpo1in2009report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "formVPO1in2009Report");

        }

        // удалить сущность form76KD2009Report
        {
            // удалить таблицу
            tool.dropTable("form76kd2009report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "form76KD2009Report");

        }

        // удалить сущность form3NKDecription2008Report
        {
            // удалить таблицу
            tool.dropTable("ec_2008rep3nk_dsc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "form3NKDecription2008Report");

        }

        // удалить сущность excludeSubjectPassForm
        {
            // удалить таблицу
            tool.dropTable("excludesubjectpassform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "excludeSubjectPassForm");

        }

        // удалить сущность examPassMarkAppeal
        {
            // удалить таблицу
            tool.dropTable("exampassmarkappeal_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examPassMarkAppeal");

        }

        // удалить сущность examPassMark
        {
            // удалить таблицу
            tool.dropTable("exampassmark_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examPassMark");

        }

        // удалить сущность examPassDiscipline
        {
            // удалить таблицу
            tool.dropTable("exampassdiscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examPassDiscipline");

        }

        // удалить сущность examGroupType
        {
            // удалить таблицу
            tool.dropTable("examgrouptype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examGroupType");

        }

        // удалить сущность examGroupSpecification
        {
            // удалить таблицу
            tool.dropTable("examgroupspecification_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examGroupSpecification");

        }

        // удалить сущность examGroupSetOpened
        {
            // удалить таблицу
            tool.dropTable("examgroupsetopened_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examGroupSetOpened");

        }

        // удалить сущность examGroupSet
        {
            // удалить таблицу
            tool.dropTable("examgroupset_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examGroupSet");

        }

        // удалить сущность examGroupRow
        {
            // удалить таблицу
            tool.dropTable("examgrouprow_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examGroupRow");

        }

        // удалить сущность examGroupLogic
        {
            // удалить таблицу
            tool.dropTable("examgrouplogic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examGroupLogic");

        }

        // удалить сущность examGroup
        {
            // удалить таблицу
            tool.dropTable("examgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examGroup");

        }

        // удалить сущность examAdmissionByCGReport
        {
            // удалить таблицу
            tool.dropTable("examadmissionbycgreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "examAdmissionByCGReport");

        }

        // удалить сущность entrantsRatingReport
        {
            // удалить таблицу
            tool.dropTable("entrantsratingreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantsRatingReport");

        }

        // удалить сущность entrantsForeignLangsReport
        {
            // удалить таблицу
            tool.dropTable("entrantsforeignlangsreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantsForeignLangsReport");

        }

        // удалить сущность entrantStateExamCertificate
        {
            // удалить таблицу
            tool.dropTable("entrantstateexamcertificate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantStateExamCertificate");

        }

        // удалить сущность entrantState
        {
            // удалить таблицу
            tool.dropTable("entrantstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantState");

        }

        // удалить сущность entrantResidenceDistrib
        {
            // удалить таблицу
            tool.dropTable("entrantresidencedistrib_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantResidenceDistrib");

        }

        // удалить сущность entrantRequest
        {
            // удалить таблицу
            tool.dropTable("entrantrequest_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRequest");

        }

        // удалить сущность entrantRegistryInternalStateExamReport
        {
            // удалить таблицу
            tool.dropTable("ntrntrgstryintrnlsttexmrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRegistryInternalStateExamReport");

        }

        // удалить сущность entrantRegistryForm2Report
        {
            // удалить таблицу
            tool.dropTable("entrantregistryform2report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRegistryForm2Report");

        }

        // удалить сущность entrantRegistryForTCReport
        {
            // удалить таблицу
            tool.dropTable("entrantregistryfortcreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRegistryForTCReport");

        }

        // удалить сущность entrantRatingSummaryReport
        {
            // удалить таблицу
            tool.dropTable("entrantratingsummaryreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantRatingSummaryReport");

        }

        // удалить сущность entrantOriginalDocumentRelation
        {
            // удалить таблицу
            tool.dropTable("ntrntorgnldcmntrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantOriginalDocumentRelation");

        }

        // удалить сущность entrantListTakePassEntrance
        {
            // удалить таблицу
            tool.dropTable("entrantlisttakepassentrance_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantListTakePassEntrance");

        }

        // удалить сущность entrantLetterDistributionReport
        {
            // удалить таблицу
            tool.dropTable("ntrntlttrdstrbtnrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantLetterDistributionReport");

        }

        // удалить сущность entrantInfoAboutUniversity
        {
            // удалить таблицу
            tool.dropTable("entrantinfoaboutuniversity_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantInfoAboutUniversity");

        }

        // удалить сущность entrantExamListsFormingFeature
        {
            // удалить таблицу
            tool.dropTable("ntrntexmlstsfrmngftr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantExamListsFormingFeature");

        }

        // удалить сущность entrantExamList
        {
            // удалить таблицу
            tool.dropTable("entrantexamlist_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantExamList");

        }

        // удалить сущность entrantEnrolmentRecommendation
        {
            // удалить таблицу
            tool.dropTable("ntrntenrlmntrcmmndtn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantEnrolmentRecommendation");

        }

        // удалить сущность entrantEnrollmentOrderType
        {
            // удалить таблицу
            tool.dropTable("entrantenrollmentordertype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantEnrollmentOrderType");

        }

        // удалить сущность entrantEnrollmentDocument
        {
            // удалить таблицу
            tool.dropTable("entrantenrollmentdocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantEnrollmentDocument");

        }

        // удалить сущность entrantEnrOrderTypeToGroup
        {
            // удалить таблицу
            tool.dropTable("entrantenrordertypetogroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantEnrOrderTypeToGroup");

        }

        // удалить сущность entrantEduInstDistribReport
        {
            // удалить таблицу
            tool.dropTable("entranteduinstdistribreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantEduInstDistribReport");

        }

        // удалить сущность entrantDailyRatingByEDReport
        {
            // удалить таблицу
            tool.dropTable("entrantdailyratingbyedreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantDailyRatingByEDReport");

        }

        // удалить сущность entrantCustomStateCI
        {
            // удалить таблицу
            tool.dropTable("entrantcustomstateci_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantCustomStateCI");

        }

        // удалить сущность entrantCustomState
        {
            // удалить таблицу
            tool.dropTable("entrantcustomstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantCustomState");

        }

        // удалить сущность entrantBenefitDistribReport
        {
            // удалить таблицу
            tool.dropTable("entrantbenefitdistribreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantBenefitDistribReport");

        }

        // удалить сущность entrantAgeDistribReport
        {
            // удалить таблицу
            tool.dropTable("entrantagedistribreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantAgeDistribReport");

        }

        // удалить сущность entrantAccessDepartment
        {
            // удалить таблицу
            tool.dropTable("entrantaccessdepartment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantAccessDepartment");

        }

        // удалить сущность entrantAccessCourse
        {
            // удалить таблицу
            tool.dropTable("entrantaccesscourse_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantAccessCourse");

        }

        // удалить сущность entrantAbsenceNote
        {
            // удалить таблицу
            tool.dropTable("entrantabsencenote_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrantAbsenceNote");

        }

        // удалить сущность entranceExamMeetingByCKReport2010
        {
            // удалить таблицу
            tool.dropTable("ntrncexmmtngbyckrprt2010_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entranceExamMeetingByCKReport2010");

        }

        // удалить сущность entranceDisciplineType
        {
            // удалить таблицу
            tool.dropTable("entrancedisciplinetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entranceDisciplineType");

        }

        // удалить сущность entranceDisciplineKind
        {
            // удалить таблицу
            tool.dropTable("entrancedisciplinekind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entranceDisciplineKind");

        }

        // удалить сущность entranceDiscipline2SetDisciplineRelation
        {
            // удалить таблицу
            tool.dropTable("ntrncdscpln2stdscplnrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entranceDiscipline2SetDisciplineRelation");

        }

        // удалить сущность entranceDiscipline
        {
            // удалить таблицу
            tool.dropTable("entrancediscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entranceDiscipline");

        }

        // удалить сущность enrollmentRevertExtract
        {
            // удалить таблицу
            tool.dropTable("enrollmentrevertextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentRevertExtract");

        }

        // удалить сущность enrollmentResultMinzdrav2009
        {
            // удалить таблицу
            tool.dropTable("enrollmentresultminzdrav2009_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultMinzdrav2009");

        }

        // удалить сущность enrollmentResultIndexNumbersReport
        {
            // удалить таблицу
            tool.dropTable("enrolresultindexesreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultIndexNumbersReport");

        }

        // удалить сущность enrollmentResultHorizontReport
        {
            // удалить таблицу
            tool.dropTable("nrllmntrslthrzntrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultHorizontReport");

        }

        // удалить сущность enrollmentResultByStageReport
        {
            // удалить таблицу
            tool.dropTable("nrllmntrsltbystgrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultByStageReport");

        }

        // удалить сущность enrollmentResultBySELReport
        {
            // удалить таблицу
            tool.dropTable("enrollmentresultbyselreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultBySELReport");

        }

        // удалить сущность enrollmentResultByOrgUnitsReport
        {
            // удалить таблицу
            tool.dropTable("enrollmentresultbyorgunits_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultByOrgUnitsReport");

        }

        // удалить сущность enrollmentResultByOrgUnitReport
        {
            // удалить таблицу
            tool.dropTable("enrollmentresultbyorgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultByOrgUnitReport");

        }

        // удалить сущность enrollmentResultByEGEReport
        {
            // удалить таблицу
            tool.dropTable("enrollmentresultbyegereport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultByEGEReport");

        }

        // удалить сущность enrollmentResultByCGReport
        {
            // удалить таблицу
            tool.dropTable("enrollmentresultbycgreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentResultByCGReport");

        }

        // удалить сущность enrollmentRecommendation
        {
            // удалить таблицу
            tool.dropTable("enrollmentrecommendation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentRecommendation");

        }

        // удалить сущность enrollmentOrderVisaItem
        {
            // удалить таблицу
            tool.dropTable("enrollmentordervisaitem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrderVisaItem");

        }

        // удалить сущность enrollmentOrderType
        {
            // удалить таблицу
            tool.dropTable("enrollmentordertype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrderType");

        }

        // удалить сущность enrollmentOrderToBasicRel
        {
            // удалить таблицу
            tool.dropTable("enrollmentordertobasicrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrderToBasicRel");

        }

        // удалить сущность enrollmentOrderTextRelation
        {
            // удалить таблицу
            tool.dropTable("enrollmentordertextrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrderTextRelation");

        }

        // удалить сущность enrollmentOrderReasonToBasicsRel
        {
            // удалить таблицу
            tool.dropTable("nrllmntordrrsntbscsrl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrderReasonToBasicsRel");

        }

        // удалить сущность enrollmentOrderReason
        {
            // удалить таблицу
            tool.dropTable("enrollmentorderreason_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrderReason");

        }

        // удалить сущность enrollmentOrderBasic
        {
            // удалить таблицу
            tool.dropTable("enrollmentorderbasic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrderBasic");

        }

        // удалить сущность enrollmentDocument
        {
            // удалить таблицу
            tool.dropTable("enrollmentdocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentDocument");

        }

        // удалить сущность enrollmentDirectionRestriction
        {
            // удалить таблицу
            tool.dropTable("nrllmntdrctnrstrctn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentDirectionRestriction");

        }

        // удалить сущность enrollmentDirection
        {
            // удалить таблицу
            tool.dropTable("enrollmentdirection_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentDirection");

        }

        // удалить сущность enrollmentCompetitionKind
        {
            // удалить таблицу
            tool.dropTable("enrollmentcompetitionkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentCompetitionKind");

        }

        // удалить сущность enrollmentCampaignPeriod
        {
            // удалить таблицу
            tool.dropTable("enrollmentcampaignperiod_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentCampaignPeriod");

        }

        // удалить сущность enrollmentCampaign
        {
            // удалить таблицу
            tool.dropTable("enrollmentcampaign_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentCampaign");

        }

        // удалить сущность enrollmentAccessCoursesReport
        {
            // удалить таблицу
            tool.dropTable("nrllmntaccsscrssrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentAccessCoursesReport");

        }

        // удалить сущность educationSubject
        {
            // удалить таблицу
            tool.dropTable("educationsubject_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "educationSubject");

        }

        // удалить сущность ecgpEntrantRecommended
        {
            // удалить таблицу
            tool.dropTable("ecgpentrantrecommended_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgpEntrantRecommended");

        }

        // удалить сущность ecgpDistributionConfig
        {
            // удалить таблицу
            tool.dropTable("ecgpdistributionconfig_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgpDistributionConfig");

        }

        // удалить сущность ecgpDistribution
        {
            // удалить таблицу
            tool.dropTable("ecgpdistribution_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgpDistribution");

        }

        // удалить сущность ecgReserveRelation
        {
            // удалить таблицу
            tool.dropTable("ec_entrant_reserve_relation", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgReserveRelation");

        }

        // удалить сущность ecgEntrantReserved
        {
            // удалить таблицу
            tool.dropTable("ecgentrantreserved_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgEntrantReserved");

        }

        // удалить сущность ecgEntrantRecommendedState
        {
            // удалить таблицу
            tool.dropTable("ecgentrantrecommendedstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgEntrantRecommendedState");

        }

        // удалить сущность ecgEntrantRecommendedDetail
        {
            // удалить таблицу
            tool.dropTable("ecgentrantrecommendeddetail_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgEntrantRecommendedDetail");

        }

        // удалить сущность ecgEntrantRecommended
        {
            // удалить таблицу
            tool.dropTable("ecgentrantrecommended_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgEntrantRecommended");

        }

        // удалить сущность ecgDistributionTAQuota
        {
            // удалить таблицу
            tool.dropTable("ecgdistributiontaquota_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistributionTAQuota");

        }

        // удалить сущность ecgDistributionState
        {
            // удалить таблицу
            tool.dropTable("ecgdistributionstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistributionState");

        }

        // удалить сущность ecgDistributionQuota
        {
            // удалить таблицу
            tool.dropTable("ecgdistributionquota_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistributionQuota");

        }

        // удалить сущность ecgDistributionDetail
        {
            // удалить таблицу
            tool.dropTable("ecgdistributiondetail_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistributionDetail");

        }

        // удалить сущность ecgDistributionConfig
        {
            // удалить таблицу
            tool.dropTable("ecgdistributionconfig_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistributionConfig");

        }

        // удалить сущность ecgDistribution
        {
            // удалить таблицу
            tool.dropTable("ecgdistribution_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistribution");

        }

        // удалить сущность ecgDistribRelation
        {
            // удалить таблицу
            tool.dropTable("ec_ecg_relation", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistribRelation");

        }

        // удалить сущность ecgDistribQuota
        {
            // удалить таблицу
            tool.dropTable("ec_ecg_quota", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistribQuota");

        }

        // удалить сущность ecgDistribObject
        {
            // удалить таблицу
            tool.dropTable("ec_ecg_distr", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "ecgDistribObject");

        }

        // удалить сущность disciplineDateSetting
        {
            // удалить таблицу
            tool.dropTable("disciplinedatesetting_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "disciplineDateSetting");

        }

        // удалить сущность discipline2RealizationFormRelation
        {
            // удалить таблицу
            tool.dropTable("dscpln2rlztnfrmrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "discipline2RealizationFormRelation");

        }

        // удалить сущность discipline2OlympiadDiplomaRelation
        {
            // удалить таблицу
            tool.dropTable("dscpln2olympddplmrltn_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "discipline2OlympiadDiplomaRelation");

        }

        // удалить сущность dailySummaryWithCMReport
        {
            // удалить таблицу
            tool.dropTable("dailysummarywithcmreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "dailySummaryWithCMReport");

        }

        // удалить сущность currentExamGroupLogic
        {
            // удалить таблицу
            tool.dropTable("currentexamgrouplogic_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "currentExamGroupLogic");

        }

        // удалить сущность conversionScale
        {
            // удалить таблицу
            tool.dropTable("conversionscale_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "conversionScale");

        }

        // удалить сущность contractAdmissionKind
        {
            // удалить таблицу
            tool.dropTable("contractadmissionkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "contractAdmissionKind");

        }

        // удалить сущность contestReport
        {
            // удалить таблицу
            tool.dropTable("contestreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "contestReport");

        }

        // удалить сущность competitionKind
        {
            // удалить таблицу
            tool.dropTable("competitionkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "competitionKind");

        }

        // удалить сущность competitionGroupRestriction
        {
            // удалить таблицу
            tool.dropTable("competitiongrouprestriction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "competitionGroupRestriction");

        }

        // удалить сущность competitionGroupEntrantsByEDReport
        {
            // удалить таблицу
            tool.dropTable("cmpttngrpentrntsbyedrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "competitionGroupEntrantsByEDReport");

        }

        // удалить сущность competitionGroup
        {
            // удалить таблицу
            tool.dropTable("competitiongroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "competitionGroup");

        }

        // удалить сущность chosenEntranceDiscipline
        {
            // удалить таблицу
            tool.dropTable("chosenentrancediscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "chosenEntranceDiscipline");

        }

        // удалить сущность accessDepartment
        {
            // удалить таблицу
            tool.dropTable("accessdepartment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "accessDepartment");

        }

        // удалить сущность accessCoursesReport
        {
            // удалить таблицу
            tool.dropTable("accesscoursesreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "accessCoursesReport");

        }

        // удалить сущность accessCourse
        {
            // удалить таблицу
            tool.dropTable("accesscourse_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "accessCourse");

        }

        // удалить сущность enrollmentRevertParagraph
        {
            // удалить таблицу
            tool.dropTable("enrollmentrevertparagraph_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentRevertParagraph");

        }

        // удалить сущность enrollmentParagraph
        {
            // удалить таблицу
            tool.dropTable("enrollmentparagraph_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentParagraph");

        }

        // удалить сущность abstractEntrantParagraph
        {
            // удалить таблицу
            tool.dropTable("abstractentrantparagraph_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "abstractEntrantParagraph");

        }

        // удалить сущность enrollmentOrder
        {
            // удалить таблицу
            tool.dropTable("enrollmentorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentOrder");

        }

        // удалить сущность abstractEntrantOrder
        {
            // удалить таблицу
            tool.dropTable("abstractentrantorder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "abstractEntrantOrder");

        }

        // удалить сущность entrTargetExtract
        {
            // удалить таблицу
            tool.dropTable("entrtargetextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrTargetExtract");

        }

        // удалить сущность entrTargetContractExtract
        {
            // удалить таблицу
            tool.dropTable("entrtargetcontractextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrTargetContractExtract");

        }

        // удалить сущность entrTargetBudgetExtract
        {
            // удалить таблицу
            tool.dropTable("entrtargetbudgetextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrTargetBudgetExtract");

        }

        // удалить сущность entrStudentShContractExtract
        {
            // удалить таблицу
            tool.dropTable("entrstudentshcontractextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrStudentShContractExtract");

        }

        // удалить сущность entrStudentShBugetExtract
        {
            // удалить таблицу
            tool.dropTable("entrstudentshbugetextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrStudentShBugetExtract");

        }

        // удалить сущность entrStudentOPPExtract
        {
            // удалить таблицу
            tool.dropTable("entrstudentoppextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrStudentOPPExtract");

        }

        // удалить сущность entrStudentExtract
        {
            // удалить таблицу
            tool.dropTable("entrstudentextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrStudentExtract");

        }

        // удалить сущность entrStudentContractExtract
        {
            // удалить таблицу
            tool.dropTable("entrstudentcontractextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrStudentContractExtract");

        }

        // удалить сущность entrStudentBugetExtract
        {
            // удалить таблицу
            tool.dropTable("entrstudentbugetextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrStudentBugetExtract");

        }

        // удалить сущность entrMasterContractExtract
        {
            // удалить таблицу
            tool.dropTable("entrmastercontractextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrMasterContractExtract");

        }

        // удалить сущность entrMasterBugetExtract
        {
            // удалить таблицу
            tool.dropTable("entrmasterbugetextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrMasterBugetExtract");

        }

        // удалить сущность entrListenerParExtract
        {
            // удалить таблицу
            tool.dropTable("entrlistenerparextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrListenerParExtract");

        }

        // удалить сущность entrListenerContractExtract
        {
            // удалить таблицу
            tool.dropTable("entrlistenercontractextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrListenerContractExtract");

        }

        // удалить сущность entrHighShortExtract
        {
            // удалить таблицу
            tool.dropTable("entrhighshortextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "entrHighShortExtract");

        }

        // удалить сущность enrollmentExtract
        {
            // удалить таблицу
            tool.dropTable("enrollmentextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "enrollmentExtract");

        }

        // удалить сущность abstractEntrantExtract
        {
            // удалить таблицу
            tool.dropTable("abstractentrantextract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            deleteCode(tool, "abstractEntrantExtract");

        }

        removeModuleFromVersion_s(tool, "uniec_fis");
        removeModuleFromVersion_s(tool, "uniec");
        removeModuleFromVersion_s(tool, "unienr_fis");
        removeModuleFromVersion_s(tool, "unienr");
        removeModuleFromVersion_s(tool, "unienr13_fis");
        removeModuleFromVersion_s(tool, "unienr13");
        tool.dropTable("tmp_epp_stateedustd_t");
        tool.dropTable("tmp_epp_stdrow_base_t");
        tool.dropColumn("addressstring_t", "title_p");
        tool.dropColumn("reponodekind_t", "createdby_id");
        tool.executeUpdate("delete from globalrole_t where id = (select id from role_t where code_p='defaultRoleEntrant')");
    }
}