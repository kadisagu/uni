package unirsmu.scripts
import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.YesNoFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.data.IRtfData
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.node.IRtfGroup
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.commonbase.catalog.entity.ScriptItem
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes
import ru.tandemservice.unienr14.competition.entity.EnrCompetition
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceBenefit
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new EntrantRequestPrintBS(                                    // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * скрипт печати заявления абитуриента (Бакалавриат, Специалитет)
 * @author Denis Perminov
 * @since 17.06.2014
 */
class EntrantRequestPrintBS {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    def print() {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()}=${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        fillCompetitionParallel(directions)
        fillCompetitionNoParallel(directions)
        fillEnrEntrantStateExam(directions)
        fillInternalStateExams(directions)
        fillInternalExams(directions)
        fillCompetitionExclusive(directions)
        fillCompetitionNoExams(directions)
        fillOlymps(directions)
        fillInjectParameters()
        fillNextOfKin()

        def fillRequestedPrograms = fillRequestedPrograms()
        def fillAcceptedContract = fillAcceptedContract()

        RtfDocument document = new RtfReader().read(template);
        if (fillRequestedPrograms) {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            byte[] template = IUniBaseDao.instance.get().getCatalogItem(ScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_REQUESTED_PROGRAMS).getCurrentTemplate()
            document.getElementList().addAll(new RtfReader().read(template).getElementList())
        }
        if (fillAcceptedContract) {
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

            byte[] template = IUniBaseDao.instance.get().getCatalogItem(ScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_ACCEPTED_CONTRACT).getCurrentTemplate()
            document.getElementList().addAll(new RtfReader().read(template).getElementList())
        }

        if (im != null)
            im.modify(document);
        if (tm != null)
            tm.modify(document);
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillCompetitionParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def parallelCompetitions = enrRequestedCompetitions.findAll() { e -> e.parallel }
        if (!parallelCompetitions.isEmpty()) {
            def dirMap = getDirMap(enrRequestedCompetitions)
            def rows = new ArrayList<String[]>()

            // для каждого выбранного конкурса
            for (EnrRequestedCompetition requestedCompetition: parallelCompetitions) {
                // формируем строку из пяти столбцов
                String[] row = new String[5]
                row[0] = requestedCompetition.priority

                def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")

                row[1] = programSubject
                row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
                row[3] = getPlaces(requestedCompetition.competition.type)
                row[4] = requestedCompetition.competition.type.getShortTitle()
                rows.add(row)
            }
            tm.put("T2", rows as String[][])
        } else
            tm.remove("T2")
    }

    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }
        def dirMap = getDirMap(enrRequestedCompetitions)
        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort { e -> e.priority }

        // для каждого выбранного конкурса
        for (EnrRequestedCompetition requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = requestedCompetition.priority

            def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

            row[1] = programSubject
            row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
            row[3] = getPlaces(requestedCompetition.competition.type)
            row[4] = requestedCompetition.competition.type.getShortTitle()
            rows.add(row)
        }
        tm.put("T1", rows as String[][])
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id };
        def programs = DQL.createStatement(session, /
                from ${EnrRequestedProgram.class.simpleName}
                where ${EnrRequestedProgram.requestedCompetition().id()} in (${ids.join(", ")})
                /).<EnrRequestedProgram> list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for (EnrRequestedProgram program : programs) {
            Long id = program.requestedCompetition.id;
            if (!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if (!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear();
            }
        }
        return dirMap
    }

    static def getPlaces(EnrCompetitionType type) {
        if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(type.code) ||
                EnrCompetitionTypeCodes.CONTRACT.equals(type.code))
            return "по договорам об оказании платных образовательных услуг"
        else if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(type.code) ||
                EnrCompetitionTypeCodes.MINISTERIAL.equals(type.code))
            return "финансируемые из федерального бюджета"
        else if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(type.code))
            return "в пределах квоты целевого приема"
        else if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(type.code))
            return "в пределах квоты приема лиц, имеющих особое право"
        return ""
    }

    def fillEnrEntrantStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def results = DQL.createStatement(session, /
                from ${EnrEntrantMarkSourceStateExam.class.simpleName}
                where ${EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                /).<EnrEntrantMarkSourceStateExam> list()

        Map<Long, EnrEntrantMarkSourceStateExam> disMap = Maps.newHashMap()
        for (EnrEntrantMarkSourceStateExam result : results) {
            Long id = result.stateExamResult.subject.id
            if (!disMap.containsKey(id)) {
                disMap.put(id, result)
            }
        }
        if (null != disMap && !disMap.isEmpty()) {
            def rows = Lists.newArrayList()

            // для каждого результата ЕГЭ
            for (Map.Entry<Long, EnrEntrantMarkSourceStateExam> e : disMap.entrySet()) {
                // формируем строку из трех столбцов
                EnrEntrantMarkSourceStateExam result = e.getValue()
                rows.add([
                        result.stateExamResult.subject.title,
                        result.stateExamResult.mark as String,
                        result.stateExamResult.year as String
                ])
            }
            tm.put("T3", rows as String[][])
        } else {
            tm.remove("T3", 2, 0)
//            tm.remove("T3")
        }
    }

    def fillInternalStateExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().code()}='${EnrExamPassFormCodes.STATE_EXAM}'
                /).<EnrChosenEntranceExamForm> list()

        def subjectIds = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.stateExamSubject.id() }.findAll().toSet().toList()
        def stateExamResultsPending = subjectIds.isEmpty() ? Collections.emptyList() : DQL.createStatement(session, /
                from ${EnrEntrantStateExamResult.class.simpleName}
                where ${EnrEntrantStateExamResult.subject().id()} in (${subjectIds.join(", ")})
                and ${EnrEntrantStateExamResult.entrant().id()}=${entrantRequest.entrant.id}
                and ${EnrEntrantStateExamResult.secondWave()}=${true}
                /).<EnrEntrantStateExamResult> list()

        if (!stateExamResultsPending.isEmpty()) {
            List<String> titles = stateExamResultsPending.collect { e -> e.subject.title }.toSet().toList().sort()
            im.put("internalStateExams", StringUtils.join(titles, ", "))

            def placeEGE = new StringBuilder()
            boolean fl = true
            for (def place : (EnrEntrantStateExamResult) stateExamResultsPending) {
                placeEGE.append((!fl ? ", " : "")).append("(" + place.subject.title + ") " + place.secondWaveExamPlace.title)
                fl = false
            }
            im.put("PlaceEGE", placeEGE.toString())
        } else {
            deleteLabels.add("internalStateExams")
            im.put("PlaceEGE", "")
        }
    }

    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
                /).<EnrChosenEntranceExamForm> list()

        if (!enrChosenEntranceExamForms.isEmpty()) {
            Set<String> titles = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.title }.sort()
            im.put("internalExams", StringUtils.join(titles, ", "))
        } else {
            deleteLabels.add("internalExams")
        }
    }

    def fillCompetitionExclusive(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def exclusiveCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionExclusive }
        if (!exclusiveCompetitions.isEmpty()) {
            EnrRequestedCompetitionExclusive comp = (EnrRequestedCompetitionExclusive) exclusiveCompetitions.get(0)
            def doc = getBenefitProofDocument(comp.benefitCategory, comp.entrant).getDocument()
            im.put("benefitCategory", comp.benefitCategory.shortTitle)
            im.put("benefitStatement", doc instanceof EnrEntrantBaseDocument ? (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code) ? olympDocTitle((EnrOlympiadDiploma)doc.docRelation.document) : baseDocTitle(doc) ): doc.getTitle())
            im.put("usingBenefit", "Использование особого права, указанного в данном заявлении, только при подаче заявления в " + TopOrgUnit.instance.shortTitle + ", подтверждаю")
            im.put("benefitName", comp.benefitCategory.title)
        } else {
            deleteLabels.add("benefitCategory")
            deleteLabels.add("benefitStatement")
            deleteLabels.add("usingBenefit")
            deleteLabels.add("benefitName")
        }
    }

    EnrEntrantBenefitProof getBenefitProofDocument(EnrBenefitCategory benefitCategory, EnrEntrant entrant) {
        def proofs = DQL.createStatement(session, /
                from ${EnrEntrantBenefitProof.class.simpleName}
                where ${EnrEntrantBenefitProof.benefitStatement().benefitCategory().id()}=${benefitCategory.id}
                and ${EnrEntrantBenefitProof.document().entrant().id()}=${entrant.id}
                /).<EnrEntrantBenefitProof> list()
        proofs.isEmpty() ? null : proofs.get(0) as EnrEntrantBenefitProof
    }

    def fillCompetitionNoExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def noExamCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }
        if (!noExamCompetitions.isEmpty()) {
            noExamCompetitions = noExamCompetitions.sort { e -> e.priority }

            def dirMap = getDirMap(noExamCompetitions)

            ScriptItem item = DataAccessServices.dao().get(ScriptItem.class, ScriptItem.code(), EnrScriptItemCodes.COMMON_ENTRANT_REQUEST_BENEFITS);
            byte[] temp = item.getCurrentTemplate();
            RtfDocument document = new RtfReader().read(temp)
            List<IRtfElement> parList = new ArrayList<>();
//            RtfString parList = new RtfString();

            for (EnrRequestedCompetition requestedCompetition : noExamCompetitions) {
                def noExamCompetition = (EnrRequestedCompetitionNoExams) requestedCompetition;
                def tempDoc = document.clone;
                def modifier = new RtfInjectModifier();
                def subject = noExamCompetition.competition.programSetOrgUnit.programSet.programSubject

                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)

                def benefitStatement = getBenefitProofDocument(noExamCompetition.benefitCategory, noExamCompetition.entrant)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

                modifier.put("programSubject", programSubject)
                modifier.put("programForm", noExamCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase())
                modifier.put("places", getPlaces(noExamCompetition.competition.type))

                if (benefitStatement != null && benefitStatement.getDocument() instanceof EnrEntrantBaseDocument) {
                    if (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(benefitStatement.document.documentType.code)) {
                        def doc = (EnrOlympiadDiploma) benefitStatement.document;
                        modifier.put("benefitStatement", olympDocTitle(doc))
                    } else {
                        modifier.put("benefitStatement", baseDocTitle((EnrEntrantBaseDocument) benefitStatement.getDocument()))
                    }
                } else
                    modifier.put("benefitStatement", "")

                modifier.modify(tempDoc)

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(tempDoc.getElementList());
                parList.add(rtfGroup)
//                parList.append(rtfGroup.toString())
            }
            im.put("CompetitionNoExams", parList)
        } else {
            im.put("CompetitionNoExams", "")
        }
    }

    def fillOlymps(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def results = DQL.createStatement(session, /
                from ${EnrEntrantMarkSourceBenefit.class.simpleName}
                where ${EnrEntrantMarkSourceBenefit.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                order by ${EnrEntrantMarkSourceBenefit.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition()}
                /).<EnrEntrantMarkSourceBenefit> list()

        Set<String> proofs = results.collect { e -> e.mainProof.id.toString() }

        if (!proofs.isEmpty()) {
            def diplms = DQL.createStatement(session, /
                    from ${EnrOlympiadDiploma.class.simpleName}
                    where ${EnrOlympiadDiploma.id()} in (${proofs.join(", ")})
                    /).<EnrOlympiadDiploma> list()

            if (!diplms.isEmpty()) {
                Set<String> titles = diplms.collect { e -> e.subject.title }
                im.put("Olymps", StringUtils.join(titles.sort(), ", "))

                def rtfString = new RtfString();
                boolean fl = true

                for (EnrOlympiadDiploma markSourceOlympiad : diplms) {
                    if (fl)
                        rtfString.par()
                    rtfString.append(olympDocTitle(markSourceOlympiad))
                    fl = false
                }
                im.put("OlympStatements", rtfString)
            } else {
                deleteLabels.add("Olymps")
                deleteLabels.add("OlympStatements")
            }
        } else {
            deleteLabels.add("Olymps")
            deleteLabels.add("OlympStatements")
        }
    }

    static def olympDocTitle(EnrOlympiadDiploma doc) {
        return doc.documentType.shortTitle + " " + doc.seriaAndNumber + " (" + doc.olympiad.title + ") по предмету " + doc.subject.title.toLowerCase() + ", " + doc.honour.title.toLowerCase()
    }

    static def baseDocTitle(EnrEntrantBaseDocument doc) {
        return doc.documentType.shortTitle + " " + doc.seria + " " + doc.number + ", выдан: " +
                (StringUtils.isEmpty(doc.issuancePlace) ? "                                                            ": doc.issuancePlace) +
                (doc.issuanceDate ? (" " + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.issuanceDate)) : '') + ", действителен до: " + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.expirationDate)
    }

    boolean fillRequestedPrograms() {
        def requestedPrograms = new DQLSelectBuilder()
                .fromEntity(EnrRequestedProgram.class, "rc")
                .column(property("rc"))
                .fetchPath(DQLJoinType.inner, EnrRequestedProgram.requestedCompetition().competition().fromAlias("rc"), "c")
                .fetchPath(DQLJoinType.inner, EnrCompetition.type().compensationType().fromAlias("c"), "ct")
                .fetchPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().programForm().fromAlias("c"), "pf")
                .fetchPath(DQLJoinType.inner, EnrCompetition.programSetOrgUnit().programSet().programSubject().fromAlias("c"), "ps")
                .where(eq(property("rc", EnrRequestedProgram.requestedCompetition().request()), value(entrantRequest)))
                .order(property("rc", EnrRequestedProgram.requestedCompetition().priority()))
                .order(property("rc", EnrRequestedProgram.priority()))
                .createStatement(session).<EnrRequestedProgram> list()

        if (requestedPrograms.isEmpty()) {
            return false
        } else {
            List<String[]> rows = new ArrayList<>();
            for (EnrRequestedProgram requestedProgram : requestedPrograms) {
                def competition = requestedProgram.getRequestedCompetition().getCompetition()
                def program = requestedProgram.getProgramSetItem().getProgram()
                rows.add([
                        requestedProgram.getRequestedCompetition().getPriority(),
                        competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitle(),
                        competition.getType().getPrintTitle(),
                        competition.getProgramSetOrgUnit().getProgramSet().getProgramForm().getShortTitle(),
                        competition.getType().getCompensationType().getTitle(),
                        program.getProgramSpecialization().isRootSpecialization() ? "" : program.getProgramSpecialization().getTitle(),
                        program.getEduProgramTrait() == null ? "" : program.getEduProgramTrait().getShortTitle(),
                        requestedProgram.getPriority()
                ] as String[])
            }
            tm.put("T5", rows as String[][])
            return true
        }
    }

    boolean fillAcceptedContract() {
        def acceptedContractCompetitions = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "rc")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().type().fromAlias("rc"), "ct")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().eduLevelRequirement().fromAlias("rc"), "elr")
                .fetchPath(DQLJoinType.inner, EnrRequestedCompetition.competition().programSetOrgUnit().programSet().programSubject().fromAlias("rc"), "ps")
                .column("rc")
                .where(eq(property("rc", EnrRequestedCompetition.request()), value(entrantRequest)))
                .where(eq(property("rc", EnrRequestedCompetition.competition().type().compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
                .order(property("rc", EnrRequestedCompetition.priority()))
                .createStatement(session).<EnrRequestedCompetition> list()

        if (acceptedContractCompetitions.isEmpty()) {
            return false
        } else {
            def dirMap = getDirMap(acceptedContractCompetitions)

            List<String[]> rows = new ArrayList<>()
            for (EnrRequestedCompetition requestedCompetition : acceptedContractCompetitions) {
                def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
                def eduLevelReq = requestedCompetition.competition.eduLevelRequirement
                rows.add([
                        requestedCompetition.getPriority(),
                        programSubject,
                        requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase(),
                        getPlaces(requestedCompetition.competition.type),
                        EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle,
                        YesNoFormatter.INSTANCE.format(requestedCompetition.isAcceptedContract())
                ] as String[])
            }
            tm.put("T6", rows as String[][])
            return true
        }
    }

    void fillInjectParameters() {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def sex = card.sex
        def personAddress = person.address

        im.put("regNumber", entrantRequest.stringNumber)
        im.put("entrantNumber", " №" + entrant.personalNumber)
        im.put("highSchoolTitleShort", TopOrgUnit.instance.shortTitle)

        def headers = DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${TopOrgUnit.instance.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
                /).<EmployeePost> list()

        IdentityCard headCard = (headers != null && !headers.isEmpty()) ? headers.get(0).person.identityCard : null;
        StringBuilder headIof = new StringBuilder()
        if (headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName)) {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".");
            }
            if (StringUtils.isNotEmpty(headCard.middleName)) {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".");
            }
            headIof.append(" ").append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }
        im.put("rector_G", headIof.toString())

        im.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(card, GrammaCase.NOMINATIVE))
        im.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(card.birthDate))
        im.put("birthPlace", card.birthPlace)
        im.put("sex", sex.title)
        im.put("citizenship", card.citizenship.fullTitle)
        im.put("identityCardTitle", card.shortTitle)
        im.put("identityCardPlaceAndDate", [card.issuancePlace, DateFormatter.DEFAULT_DATE_FORMATTER.format(card.issuanceDate)].grep().join(", "))
        im.put("adressTitleWithFlat", personAddress != null ? personAddress.titleWithFlat : "")
        im.put("adressPhonesTitle", person.contactData.mainPhones)
        im.put("email", person.contactData.email)
        im.put("age", card.age as String)

        im.put("education", entrantRequest.eduDocument.eduLevel?.title)
        im.put("certificate", entrantRequest.eduDocument.title)
        //
        im.put('eduOrganization', entrantRequest.eduDocument.eduOrganization)
        im.put('eduOrganizationYearEnd', String.valueOf(entrantRequest.eduDocument.yearEnd))
        im.put('receiveEduLevelFirst', entrantRequest.receiveEduLevelFirst ? 'впервые' : 'не впервые')

        List<EnrEntrantAchievement> achievements = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, 'ea')
                .where(eq(property('ea', EnrEntrantAchievement.entrant().id()), value(entrant.id)))
                .order(property("ea", EnrEntrantAchievement.type().achievementKind().title()))
                .createStatement(session).list();

        def rtfString = new RtfString();
        for (EnrEntrantAchievement achievement : achievements) {
            rtfString.append(getAchivmentTitle(achievement))
            if (achievements.indexOf(achievement) != achievements.size() - 1)
                rtfString.par()
        }
        im.put('entrantAchievements', rtfString)
        //
        def fLangs = DQL.createStatement(session, /
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                order by ${PersonForeignLanguage.main()}
                /).<PersonForeignLanguage> list()

        StringBuilder fL = new StringBuilder()
        if (!fLangs.isEmpty()) {
            Set<String> titles = fLangs.collect { e -> e.language.title.concat((null != e.skill ? ", " + e.skill.title : "")) }
            fL.append(StringUtils.join(titles, "; "))
        }
        im.put("foreignLanguages", fL.toString())

        im.put("serviceLength", person.serviceLength)
        im.put("needHotel", person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("additionalInfo", entrant.additionalInfo)

        im.put("averageBall", entrantRequest.eduDocument.avgMarkAsDouble as String)
        im.put("eduInstitution", entrantRequest.eduDocument.eduOrganisationWithAddress + ", " + (entrantRequest.eduDocument.yearEnd as String) + "г.")

        im.put("wayOfProviding", entrantRequest.getOriginalSubmissionWay().getTitle())
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())
        im.put("needSpecialConditions", entrant.isNeedSpecialExamConditions() ? "нуждаюсь" + (entrant.getSpecialExamConditionsDetails() == null ? "" : " (" + entrant.getSpecialExamConditionsDetails() + ")") : "не нуждаюсь")

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))
        im.put("dateReq", DateFormatter.STRING_MONTHS_AND_QUOTES.format(regDate))

        String crimeaEnrCampaignString = '';
        if(entrant.enrollmentCampaign.settings.acceptPeopleResidingInCrimea)
            crimeaEnrCampaignString = "Принадлежу к категории лиц, постоянно проживающих в Крыму";
        im.put('crimeaEnrollmentCampaign', crimeaEnrCampaignString)
    }

    private static String getAchivmentTitle(EnrEntrantAchievement achievement){
        double mark = achievement.getType().isMarked() ? achievement.getMark() : achievement.getType().getAchievementMark()
        return achievement.type.achievementKind.title
                .concat(' ')
                .concat(String.valueOf(mark))
    }

    def fillNextOfKin() {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put("father", father)
        else {
            im.put("father", "")
        }

        if (mother != null)
            im.put("mother", mother)
        else {
            im.put("mother", "")
        }

        if (tutor != null)
            im.put("tutor", tutor)
        else {
            deleteLabels.add("tutor")
        }
    }

    def getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(" "),
                            nextOfKin.phones].grep().join(", ") : null
    }
}
