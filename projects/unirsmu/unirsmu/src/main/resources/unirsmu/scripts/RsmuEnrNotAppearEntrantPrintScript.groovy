package unirsmu.scripts

import org.apache.commons.collections.CollectionUtils
import org.hibernate.Session
import org.tandemframework.caf.logic.wrapper.DataWrapper
import org.tandemframework.core.entity.ViewWrapper
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic.RsmuEnrNotAppearEntrantDSHandler

/**
 * @author Andrey Avetisov
 * @since 11.02.2015
 */

return new RsmuEnrNotAppearEntrantPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        entrantList: entrantList
).print()

class RsmuEnrNotAppearEntrantPrint
{
    Session session
    byte[] template
    List<ViewWrapper> entrantList
    def tm = new RtfTableModifier()

    def print()
    {
        def document = new RtfReader().read(template)

        int rowNumber = 1;
        List entrants = new ArrayList<>();
        for (ViewWrapper wrapper : entrantList)
        {
            EnrEntrant entrant = (EnrEntrant)wrapper.getEntity();
            String requestNumber = !CollectionUtils.isEmpty((List)wrapper.getViewProperty(RsmuEnrNotAppearEntrantDSHandler.VIEW_PROP_REQUEST))?
                    ((DataWrapper)((List)wrapper.getViewProperty(RsmuEnrNotAppearEntrantDSHandler.VIEW_PROP_REQUEST)).get(0)).getTitle():"";
            def row = [rowNumber, entrant.personalNumber, requestNumber, entrant.fullFio, entrant?.state?.title];
            entrants.add(row)
            rowNumber++;

        }
        tm.put("entrant", entrants as String[][])
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'RsmuEnrNotAppearEntrant.rtf']
    }
}