/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package unirsmu.scripts.report

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.RtfComponents
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.IScriptItem
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileTransferAdd.RsmuEnrReportPersonalFileTransferAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq
import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue
import static org.tandemframework.hibsupport.dql.DQLExpressions.exists
import static org.tandemframework.hibsupport.dql.DQLExpressions.property

return new RsmuPersonalFileTransferPrint(                 // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов
 */
class RsmuPersonalFileTransferPrint
{
    Session session
    byte[] template
    RsmuEnrReportPersonalFileTransferAddUI model
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def document = new RtfReader().read(template)
        fillOrders(document.header)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'PersonalFileTransferReport.rtf']
    }

    private void fillOrders(RtfHeader header)
    {
        List<EnrOrder> orderList = model.getEnrOrderList()
        List<IRtfElement> allOrderElementList = []
        int totalEnrCount = 0
        for (EnrOrder enrOrder : orderList)
        {
            List<IRtfElement> orderElementList = []
            String groupOrgUnitTitle = model.getEduGroupOrgUnit().getGroupOrgUnit() != null ? model.getEduGroupOrgUnit().getGroupOrgUnit().getGenitiveCaseTitle() : "";

            //Получаем отфильтрованные параграфы и отфильтрованных абитуриентов
            Map<EnrEnrollmentParagraph, List<EnrEntrant>> filteredParagraphEntrantMap = getFilteredParagraphAndEntrant(enrOrder.getParagraphList())
            Map<EduProgramSubject, List<EnrEntrant>> programSubjectEntrantMap = getProgramSubjectAndEntrantMap(filteredParagraphEntrantMap)

            def injectModifier = new RtfInjectModifier()
                    .put('commitDate', enrOrder.commitDate?.format('dd.MM.yyyy'))
                    .put('orderNumber', enrOrder.number)
                    .put('orgUnit', groupOrgUnitTitle)
                    .put('developForm', getEduProgramFormTitle(filteredParagraphEntrantMap.keySet()))
                    .put('condition', getCompetitionType(filteredParagraphEntrantMap.keySet()))

            final IScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.RSMU_REPORT_PERSONAL_FILE_TRANSFER_ORDER);
            def orderDocument = new RtfReader().read(scriptItem.getTemplate())
            // подготавливаем клон шаблона приказа для вставки в печатную форму отчета
            def orderPart = orderDocument.clone

            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = orderPart.elementList
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(group)
            int enrCount = fillProgramSubjectEntrant(programSubjectEntrantMap, orderElementList)
            if (enrCount > 0)
            {
                allOrderElementList.addAll(orderElementList)
            }
            totalEnrCount += enrCount
            injectModifier.put("enrCount", String.valueOf(enrCount))

            RtfUtil.modifySourceList(header, orderPart.header, orderPart.elementList)
            injectModifier.modify(orderPart)
        }
        im.put('ORDERS', allOrderElementList)
        im.put("totalEnrCount", String.valueOf(totalEnrCount))
    }

    private static Integer fillProgramSubjectEntrant(Map<EduProgramSubject, List<EnrEntrant>> filteredProgramSubjectEntrantMap, List<IRtfElement> orderElementList)
    {
        int enrCount = 0
        for (EduProgramSubject programSubject : filteredProgramSubjectEntrantMap.keySet())
        {

            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.createRtfText("По специальности " + programSubject.code + " «" + programSubject.title + "»"))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.createRtfText("(личных дел: " + filteredProgramSubjectEntrantMap.get(programSubject).size() + " ):"))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))

            List<EnrEntrant> entrantList = filteredProgramSubjectEntrantMap.get(programSubject)

            entrantList.sort(new Comparator<EnrEntrant>() {
                @Override
                int compare(EnrEntrant o1, EnrEntrant o2)
                {
                    return o1.getPersonalNumber().compareTo(o2.getPersonalNumber())
                }
            })
            enrCount += filteredProgramSubjectEntrantMap.get(programSubject).size()
            for (EnrEntrant entrant : entrantList)
            {
                orderElementList.add(RtfBean.elementFactory.createRtfText(entrant.personalNumber))
                orderElementList.add(RtfBean.elementFactory.createRtfText("; "))
            }
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
        }

        return enrCount
    }

    static def getCompetitionType(Set<IAbstractParagraph> paragraphSet)
    {
        def competitionType = ""
        def contractType = "на места, сверх установленных контрольных цифр приема по договорам об оказании платных образовательных услуг"
        def budgetType = "на места, финансируемые из средств федерального бюджета"
        def targetType = "на целевые места, финансируемые из средств федерального бюджета"
        boolean hasContractType = false
        boolean hasSimpleBudgetType = false
        boolean hasTargetType = false
        for (IAbstractParagraph abstractParagraph : paragraphSet)
        {

            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph
            if (((EnrOrder) paragraph.order).compensationType.code.equals(UniDefines.COMPENSATION_TYPE_CONTRACT))
            {
                hasContractType = true;
            }

            if (paragraph.competitionType.code.equals(EnrCompetitionTypeCodes.TARGET_ADMISSION))
            {
                hasTargetType = true;
            }
            else if (((EnrOrder) paragraph.order).compensationType.budget)
            {
                hasSimpleBudgetType = true;
            }

        }

        if (hasContractType)
        {
            competitionType += contractType
        }
        if (hasSimpleBudgetType)
        {
            if (competitionType.length() > 0)
            {
                competitionType += ", "
            }
            competitionType += budgetType
        }
        if (hasTargetType)
        {
            if (competitionType.length() > 0)
            {
                competitionType += ", "
            }
            competitionType += targetType
        }


        return competitionType
    }

    private static def getEduProgramFormTitle(Set<IAbstractParagraph> paragraphSet)
    {
        def formTitle = ""
        ArrayList<String> developFormList = new ArrayList<>()
        for (IAbstractParagraph abstractParagraph : paragraphSet)
        {

            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph

            if (!developFormList.contains(getProgramFormStr_A(paragraph.programForm)))
            {
                developFormList.add(getProgramFormStr_A(paragraph.programForm))
            }
        }
        formTitle = StringUtils.join(developFormList, ",")
        if (developFormList.size() > 1)
        {
            formTitle += " формы обучения"
        }
        else
        {
            formTitle += " форму обучения"
        }

        return formTitle
    }

    private static def getProgramFormStr_A(EduProgramForm programForm)
    {
        switch (programForm.code)
        {
            case EduProgramFormCodes.OCHNAYA: return 'очную (дневную)'
            case EduProgramFormCodes.ZAOCHNAYA: return 'заочную'
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA: return 'очно-заочную (вечернюю)'
        }
        return ''
    }

    private List<EnrEntrant> getFilteredEntrantList(List<Long> paragraphEntrantList)
    {
        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());
        requestedCompDQL.where(eq(property(requestedCompDQL.entrant(), EnrEntrant.P_ID), property("e", EnrEntrant.P_ID)))
        requestedCompDQL.where(eqValue(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.state().code().s()), EnrEntrantStateCodes.ENROLLED))
        requestedCompDQL.where(DQLExpressions.in(property(requestedCompDQL.request(), EnrEntrantRequest.entrant().id()), paragraphEntrantList))
        requestedCompDQL.column(property(requestedCompDQL.entrant()))

        DQLSelectBuilder entrantBuilder = new DQLSelectBuilder().fromEntity(EnrEntrant.class, "e")
                .column(property("e"))
                .where(exists(requestedCompDQL.buildQuery()))
                .order(property("e", EnrEntrant.P_PERSONAL_NUMBER))


        List<EnrEntrant> entrantList = DataAccessServices.dao().getList(entrantBuilder);
        return entrantList
    }

    private Map<EnrEnrollmentParagraph, List<EnrEntrant>> getFilteredParagraphAndEntrant(List<IAbstractParagraph> paragraphList)
    {
        Map<EnrEnrollmentParagraph, List<EnrEntrant>> filteredParagraphEntrantMap = new HashMap<>()

        for (IAbstractParagraph abstractParagraph : paragraphList)
        {
            if (abstractParagraph instanceof EnrEnrollmentParagraph)
            {
                EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph
                List<EnrEnrollmentExtract> enrollmentExtractList = (List<EnrEnrollmentExtract>) paragraph.extractList

                List<Long> entrantInParagraph = new ArrayList();
                for (def extract : enrollmentExtractList)
                {
                    entrantInParagraph.add(extract.entrant.id)
                }

                List<EnrEntrant> entrantList = getFilteredEntrantList(entrantInParagraph);
                if (!entrantList.isEmpty())
                {
                    filteredParagraphEntrantMap.put(paragraph, entrantList)
                }
            }
        }
        return filteredParagraphEntrantMap
    }

    private static Map<EduProgramSubject, List<EnrEntrant>> getProgramSubjectAndEntrantMap(Map<EnrEnrollmentParagraph, List<EnrEntrant>> filteredParagraphEntrantMap)
    {
        Map<EduProgramSubject, List<EnrEntrant>> filteredProgramSubjectEntrantMap = new HashMap<>()

        for (EnrEnrollmentParagraph paragraph : filteredParagraphEntrantMap.keySet())
        {

            List<EnrEntrant> entrantList = filteredParagraphEntrantMap.get(paragraph);

            if (!entrantList.isEmpty())
            {
                if (filteredProgramSubjectEntrantMap.get(paragraph.programSubject) == null)
                {
                    filteredProgramSubjectEntrantMap.put(paragraph.programSubject, entrantList)
                }
                else
                {
                    filteredProgramSubjectEntrantMap.get(paragraph.programSubject).addAll(entrantList)
                }
            }
        }

        return filteredProgramSubjectEntrantMap
    }
}


