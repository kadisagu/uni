package unirsmu.scripts

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.base.entity.PersonNextOfKin
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram

return new EntrantRequestPrintHigher(                                // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * скрипт печати заявления абитуриента (Кадры высшей квалификации)
 * @author Denis Perminov
 * @since 17.06.2014
 */
class EntrantRequestPrintHigher
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList() // метки, строки с которыми необходимо удалить из таблицы

    def print()
    {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()} = ${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
        /).<EnrRequestedCompetition> list()

        fillCompetitionParallel(directions)
        fillCompetitionNoParallel(directions)
        fillInternalExams(directions)
        fillInjectParameters()
        fillNextOfKin()

        RtfDocument document = new RtfReader().read(template);

        if (im != null)
            im.modify(document);
        if (tm != null)
            tm.modify(document);
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillCompetitionParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def parallelCompetitions = enrRequestedCompetitions.findAll() { e -> e.parallel }
        if (!parallelCompetitions.isEmpty()) {
            def dirMap = getDirMap(enrRequestedCompetitions)
            def rows = new ArrayList<String[]>()

            // для каждого выбранного конкурса
            for (EnrRequestedCompetition requestedCompetition: parallelCompetitions) {
                // формируем строку из пяти столбцов
                String[] row = new String[5]
                row[0] = requestedCompetition.priority

                def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")
                def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

                row[1] = programSubject
                row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
                row[3] = getPlaces(requestedCompetition.competition.type)
                row[4] = EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle
                rows.add(row)
            }
            tm.put("T2", rows as String[][])
        } else
            tm.remove("T2")
    }

    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }
        def dirMap = getDirMap(enrRequestedCompetitions)
        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort { e -> e.priority }

        // для каждого выбранного конкурса
        for (EnrRequestedCompetition requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = requestedCompetition.priority

            def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")
            def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

            row[1] = programSubject
            row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
            row[3] = getPlaces(requestedCompetition.competition.type)
            row[4] = EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle
            rows.add(row)
        }
        tm.put("T1", rows as String[][])
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id };
        def programs = DQL.createStatement(session, /
                from ${EnrRequestedProgram.class.simpleName}
                where ${EnrRequestedProgram.requestedCompetition().id()} in (${ids.join(", ")})
                /).<EnrRequestedProgram> list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for (EnrRequestedProgram program : programs) {
            Long id = program.requestedCompetition.id;
            if (!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if (!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear();
            }
        }
        return dirMap
    }

    static def getPlaces(EnrCompetitionType type) {
        if (EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(type.code) ||
                EnrCompetitionTypeCodes.CONTRACT.equals(type.code))
            return "по договорам об оказании платных образовательных услуг"
        else if (EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(type.code) ||
                EnrCompetitionTypeCodes.MINISTERIAL.equals(type.code))
            return "финансируемые из федерального бюджета"
        else if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(type.code))
            return "в пределах квоты целевого приема"
        else if (EnrCompetitionTypeCodes.EXCLUSIVE.equals(type.code))
            return "в пределах квоты приема лиц, имеющих особое право"
        return ""
    }

    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
                /).<EnrChosenEntranceExamForm> list()

        if (!enrChosenEntranceExamForms.isEmpty()) {
            Set<String> titles = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.title }.sort()
            im.put("internalExams", StringUtils.join(titles, ", "))
        } else {
            deleteLabels.add("internalExams")
        }
    }

    void fillInjectParameters() {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def sex = card.sex
        def personAddress = person.address

        im.put("regNumber", entrantRequest.stringNumber)
        im.put("entrantNumber", " №" + entrant.personalNumber)
        im.put("highSchoolTitleShort", TopOrgUnit.instance.shortTitle)

        def headers = DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${TopOrgUnit.instance.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
                /).<EmployeePost> list()

        IdentityCard headCard = (headers != null && !headers.isEmpty()) ? headers.get(0).person.identityCard : null;
        StringBuilder headIof = new StringBuilder()
        if (headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName)) {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".");
            }
            if (StringUtils.isNotEmpty(headCard.middleName)) {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".");
            }
            headIof.append(" ").append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }
        im.put("rector_G", headIof.toString())

        im.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(card, GrammaCase.NOMINATIVE))
        im.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(card.birthDate))
        im.put("birthPlace", card.birthPlace)
        im.put("sex", sex.title)
        im.put("citizenship", card.citizenship.fullTitle)
        im.put("identityCardTitle", card.shortTitle)
        im.put("identityCardPlaceAndDate", [card.issuancePlace, DateFormatter.DEFAULT_DATE_FORMATTER.format(card.issuanceDate)].grep().join(", "))
        im.put("adressTitleWithFlat", personAddress != null ? personAddress.titleWithFlat : "")
        im.put("adressPhonesTitle", person.contactData.mainPhones)
        im.put("email", person.contactData.email)
        im.put("age", card.age as String)

        im.put("education", entrantRequest.eduDocument.eduLevel?.title)
        im.put("certificate", entrantRequest.eduDocument.title)

        def fLangs = DQL.createStatement(session, /
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                order by ${PersonForeignLanguage.main()}
                /).<PersonForeignLanguage> list()

        StringBuilder fL = new StringBuilder()
        if (!fLangs.isEmpty()) {
            Set<String> titles = fLangs.collect { e -> e.language.title.concat((null != e.skill ? ", " + e.skill.title : "")) }
            fL.append(StringUtils.join(titles, "; "))
        }
        im.put("foreignLanguages", fL.toString())

        im.put("serviceLength", person.serviceLength)
        im.put("needHotel", person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("additionalInfo", entrant.additionalInfo)

        im.put("averageBall", entrantRequest.eduDocument.avgMarkAsDouble as String)
        im.put("eduInstitution", entrantRequest.eduDocument.eduOrganisationWithAddress + ", " + (entrantRequest.eduDocument.yearEnd as String) + "г.")

        im.put("wayOfProviding", entrantRequest.getOriginalSubmissionWay().getTitle())
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())
        im.put("needSpecialConditions", entrant.isNeedSpecialExamConditions() ? "нуждаюсь" + (entrant.getSpecialExamConditionsDetails() == null ? "" : " (" + entrant.getSpecialExamConditionsDetails() + ")") : "не нуждаюсь")

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))
        im.put("dateReq", DateFormatter.STRING_MONTHS_AND_QUOTES.format(regDate))

        String crimeaEnrCampaignString = '';
        if(entrant.enrollmentCampaign.settings.acceptPeopleResidingInCrimea)
            crimeaEnrCampaignString = "Категория граждан, попадающая под действие ФЗ-64";
        im.put('crimeaEnrollmentCampaign', crimeaEnrCampaignString)
    }

    def fillNextOfKin() {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put("father", father)
        else {
            im.put("father", "")
        }

        if (mother != null)
            im.put("mother", mother)
        else {
            im.put("mother", "")
        }

        if (tutor != null)
            im.put("tutor", tutor)
        else {
            deleteLabels.add("tutor")
        }
    }

    def getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(" "),
                            nextOfKin.phones].grep().join(", ") : null
    }
}
