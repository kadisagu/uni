package unirsmu.scripts.report

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.RtfComponents
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.IScriptItem
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.ContractTransferAdd.RsmuEnrReportContractTransferAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Avetisov
 * @since 29.01.2015
 */

return new RsmuEnrContractTransferPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        model: model
).print()

class RsmuEnrContractTransferPrint
{
    Session session
    byte[] template
    RsmuEnrReportContractTransferAddUI model
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def document = new RtfReader().read(template)
        fillOrders(document.header)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'RsmuEnrPersonalFileReport.rtf']
    }


    void fillOrders(RtfHeader header)
    {
        List<EnrOrder> orderList = model.getEnrOrderList()
        List<IRtfElement> allOrderElementList = []
        int totalEnrCount = 0
        for (EnrOrder enrOrder : orderList)
        {
            //Получаем отфильтрованные параграфы и отфильтрованных абитуриентов
            Map<EnrEnrollmentParagraph, List<CtrContractObject>> filteredParagraphEntrantMap = getFilteredParagraphAndContract(enrOrder.getParagraphList())
            Map<EduProgramSubject, List<CtrContractObject>> programSubjectContractMap = getProgramSubjectAndContractMap(filteredParagraphEntrantMap)

            def injectModifier = new RtfInjectModifier()
                    .put('commitDate', enrOrder.commitDate?.format('dd.MM.yyyy'))
                    .put('orderNumber', enrOrder.number)
                    .put('developForm', getEduProgramFormTitle(filteredParagraphEntrantMap.keySet()))
            IScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.RSMU_REPORT_ENTRANT_CONTRACT_TRANSFER_ORDER);
            def orderDocument = new RtfReader().read(scriptItem.getTemplate())
            // подготавливаем клон шаблона приказа для вставки в печатную форму отчета
            def orderPart = orderDocument.clone

            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = orderPart.elementList
            List<IRtfElement> orderElementList = []
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(group)
            int enrCount = fillProgramSubjectContract(programSubjectContractMap, orderElementList)
            if (enrCount > 0)
            {
                allOrderElementList.addAll(orderElementList)
            }
            totalEnrCount += enrCount
            injectModifier.put("enrCount", String.valueOf(enrCount))

            RtfUtil.modifySourceList(header, orderPart.header, orderPart.elementList)
            injectModifier.modify(orderPart)
        }
        im.put('ORDERS', allOrderElementList)
        im.put("totalEnrCount", String.valueOf(totalEnrCount))
    }


    Integer fillProgramSubjectContract(Map<EduProgramSubject, List<CtrContractObject>> filteredProgramSubjectContractMap, List<IRtfElement> orderElementList)
    {
        int enrCount = 0
        for (EduProgramSubject programSubject : filteredProgramSubjectContractMap.keySet())
        {

            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.createRtfText("По специальности " + programSubject.code + " «" + programSubject.title + "»"))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.createRtfText("(договоров: " + filteredProgramSubjectContractMap.get(programSubject).size() + " ):"))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))
            orderElementList.add(RtfBean.elementFactory.create(RtfComponents.PARAGRAPH))

            List<CtrContractObject> contractList = filteredProgramSubjectContractMap.get(programSubject)
            contractList.sort(new Comparator<CtrContractObject>() {
                @Override
                int compare(CtrContractObject o1, CtrContractObject o2)
                {
                    return o1.getNumber().compareTo(o2.getNumber())
                }
            })
            enrCount += filteredProgramSubjectContractMap.get(programSubject).size()
            for (CtrContractObject contractObject : contractList)
            {
                orderElementList.add(RtfBean.elementFactory.createRtfText(contractObject.getNumber()))
                orderElementList.add(RtfBean.elementFactory.createRtfText("; "))
            }
        }

        return enrCount
    }

    /**
     * Фильтрует договоры, используя данные аддона из модели.
     * @param paragraphEntrantList список id абитуриентов, договора которых нужно получить
     * @return список договор, удовлетоворяющих условиям фильтрации.
     */
    private List<CtrContractObject> getFilteredEntrantList(List<Long> paragraphEntrantList, EduProgramSubject programSubject)
    {
        // Фильтры
        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder()
                .notArchiveOnly()
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());
        requestedCompDQL.where(eq(property(requestedCompDQL.entrant(), EnrEntrant.P_ID), property("e", EnrEntrantContract.requestedCompetition().request().entrant().id())))
        requestedCompDQL.where(eqValue(property(requestedCompDQL.reqComp(), EnrRequestedCompetition.state().code().s()), EnrEntrantStateCodes.ENROLLED))
        requestedCompDQL.where(DQLExpressions.in(property(requestedCompDQL.request(), EnrEntrantRequest.entrant().id()), paragraphEntrantList))
        requestedCompDQL.column(property(requestedCompDQL.entrant()))

        DQLSelectBuilder entrantContractBuilder = new DQLSelectBuilder().fromEntity(EnrEntrantContract.class, "e")
                .column(property("e", EnrEntrantContract.L_CONTRACT_OBJECT))
                .where(exists(requestedCompDQL.buildQuery()))
                .where(eqValue(property("e", EnrEntrantContract.requestedCompetition().competition().programSetOrgUnit().programSet().programSubject()), programSubject))
                .order(property("e", EnrEntrantContract.contractObject().number()))


        List<CtrContractObject> entrantContractList = DataAccessServices.dao().getList(entrantContractBuilder);
        return entrantContractList
    }

    /**
     * Группирует отфильтрованные договора по параграфам.
     * @param paragraphList список параграфов
     * @return HashMap , содержит переданные параграфы и список отфильтрованных договоров для каждого параграфа
     */
    private Map<EnrEnrollmentParagraph, List<CtrContractObject>> getFilteredParagraphAndContract(List<IAbstractParagraph> paragraphList)
    {
        Map<EnrEnrollmentParagraph, List<CtrContractObject>> filteredParagraphEntrantMap = new HashMap<>()

        for (IAbstractParagraph abstractParagraph : paragraphList)
        {

            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph
            List<EnrEnrollmentExtract> enrollmentExtractList = (List<EnrEnrollmentExtract>) paragraph.extractList

            List<Long> entrantInParagraph = new ArrayList();
            for (def extract : enrollmentExtractList)
            {
                entrantInParagraph.add(extract.entrant.id)
            }

            List<CtrContractObject> contractList = getFilteredEntrantList(entrantInParagraph, paragraph.programSubject);
            if (!contractList.isEmpty())
            {
                filteredParagraphEntrantMap.put(paragraph, contractList)
            }
        }
        return filteredParagraphEntrantMap
    }

    /**
     * Группирует договора по направлениям подготовик.
     * @param filteredParagraphContractMap map, содержащая договора, сгруппированные по параграфам приказа.
     * @return договора , сгруппированные по направлениям подготовки
     */
    private static Map<EduProgramSubject, List<CtrContractObject>> getProgramSubjectAndContractMap(Map<EnrEnrollmentParagraph,
            List<CtrContractObject>> filteredParagraphContractMap)
    {
        Map<EduProgramSubject, List<CtrContractObject>> filteredProgramSubjectContractMap = new HashMap<>()

        for (EnrEnrollmentParagraph paragraph : filteredParagraphContractMap.keySet())
        {

            List<CtrContractObject> contractList = filteredParagraphContractMap.get(paragraph);

            if (!contractList.isEmpty())
            {
                if (filteredProgramSubjectContractMap.get(paragraph.programSubject) == null)
                {
                    filteredProgramSubjectContractMap.put(paragraph.programSubject, contractList)
                }
                else
                {
                    filteredProgramSubjectContractMap.get(paragraph.programSubject).addAll(contractList)
                }
            }
        }

        return filteredProgramSubjectContractMap
    }

    /**
     * В зависимости от форм обучения, указанных в переданных параграфах, возвращает форматированую(мн./ед. число)
     * строку с названиями формам обучения в винительном падеже.
     * @param paragraphSet набор параграфов
     * @return строка с названиям форм обучения в винительном падеже
     */
    private static def getEduProgramFormTitle(Set<IAbstractParagraph> paragraphSet)
    {
        String formTitle = ""
        ArrayList<String> developFormList = new ArrayList<>()
        for (IAbstractParagraph abstractParagraph : paragraphSet)
        {

            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph

            if (!developFormList.contains(getProgramFormStr_A(paragraph.programForm)))
            {
                developFormList.add(getProgramFormStr_A(paragraph.programForm))
            }
        }
        formTitle = StringUtils.join(developFormList, ",")
        if (developFormList.size() > 1)
        {
            formTitle += " формы обучения"
        }
        else
        {
            formTitle += " форму обучения"
        }

        return formTitle
    }

    /**
     * Принимает форму обучения и возвращает ее название в винительном падеже.
     * @param programForm форма обучения
     * @return название формы обучения в винительном падеже
     */
    private static def getProgramFormStr_A(EduProgramForm programForm)
    {
        switch (programForm.code)
        {
            case EduProgramFormCodes.OCHNAYA: return 'очную (дневную)'
            case EduProgramFormCodes.ZAOCHNAYA: return 'заочную'
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA: return 'очно-заочную (вечернюю)'
        }
        return ''
    }

}