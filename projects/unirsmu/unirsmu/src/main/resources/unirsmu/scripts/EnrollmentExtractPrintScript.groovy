/* $Id$ */
package unirsmu.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.unienr14.order.entity.EnrAbstractExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem

return new EnrollmentExtractPrint(
        session: session,
        template: template,
        extract: session.get(EnrAbstractExtract.class, object)
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 31.08.2014
 */
class EnrollmentExtractPrint
{
    Session session
    byte[] template
    EnrAbstractExtract extract

    def print()
    {
        def paragraph = extract.paragraph as EnrEnrollmentParagraph
        def order = paragraph.order
        def document = new RtfReader().read(template)
        def mark = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())?.totalMarkAsDouble

        def im = new RtfInjectModifier()
                .put('fio', extract.requestedCompetition.request.entrant.fullFio)
                .put('number', order.number)
                .put('date', order.commitDate ? order.commitDate.format('dd.MM.yyyy') : '')
                .put('mark', mark ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(mark) : '')
                .put('formativeOrgUnit', paragraph.formativeOrgUnit.printTitle)
                .put('speciality', paragraph.programSubject.title)
                .put('eduForm', getProgramFormStr_A(paragraph.programForm))

        im.modify(document)

        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentExtract.rtf']
    }

    static String getProgramFormStr_A(EduProgramForm programForm)
    {
        switch (programForm.code)
        {
            case EduProgramFormCodes.OCHNAYA: return 'очную'
            case EduProgramFormCodes.ZAOCHNAYA: return 'заочную'
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA: return 'очно-заочную'
        }
        return ''
    }
}
