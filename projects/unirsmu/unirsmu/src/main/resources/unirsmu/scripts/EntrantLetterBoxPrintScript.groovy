package unirsmu.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition

return new EntrantLetterBoxPrint(                                    // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()
/**
 * @author Denis Perminov
 * @since 12.07.2014
 */
class EntrantLetterBoxPrint {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()

    def print() {
        // получаем направление приема
        // со статусом "ЗАЧИСЛЕН"
        def enrolledDirection = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().entrant().id()}=${entrantRequest.entrant.id}
                and ${EnrRequestedCompetition.state().code()}='${EnrEntrantStateCodes.ENROLLED}'
                /).setMaxResults(1).<EnrRequestedCompetition> uniqueResult()

        if (null != enrolledDirection) {
            def competition = enrolledDirection.competition
            def programSetOrgUnit = competition.programSetOrgUnit
            im.put("orgUnit", null != programSetOrgUnit.formativeOrgUnit ? programSetOrgUnit.formativeOrgUnit.printTitle : "")

            def programSet = programSetOrgUnit.programSet
            im.put("programSubject", programSet.title.toLowerCase())    // .programSubject.title.toLowerCase()
            im.put("educationForm", programSet.programForm.title.toLowerCase())

            def eduLevel = entrantRequest.eduDocument.eduLevel.code
            boolean isHigh = EduLevelCodes.VYSSHEE_OBRAZOVANIE_BAKALAVRIAT.equals(eduLevel) ||
                            EduLevelCodes.VYSSHEE_OBRAZOVANIE_SPETSIALITET_MAGISTRATURA.equals(eduLevel) ||
                            EduLevelCodes.VYSSHEE_OBRAZOVANIE_PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII.equals(eduLevel)
            def compensationType = (isHigh ? "2" : "1") + " " + (competition.type.compensationType.budget ? "(ВО) бюджет" : "(ВО) договор")
            im.put("compensationType", compensationType)
        } else {
            im.put("orgUnit", "")
            im.put("programSubject", "")
            im.put("educationForm", "")
            im.put("compensationType", "")
        }

        def entrant = entrantRequest.entrant
        def person = entrant.person
        def card = entrantRequest.identityCard

        im.put("firstName", card.firstName)
        im.put("middleName", card.middleName)
        im.put("lastName", card.lastName)
        im.put("entrantNumber", entrant.personalNumber)

        def foreignLanguageList = DQL.createStatement(session, /
                select distinct ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                /).<String> list()

        im.put("foreignLanguage", foreignLanguageList.join(", "))

        RtfDocument document = new RtfReader().read(template);

        im.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Титульный лист ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }
}