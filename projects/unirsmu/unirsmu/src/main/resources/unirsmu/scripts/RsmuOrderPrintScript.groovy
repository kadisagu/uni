/* $Id: EnrollmentOrderPrintScript.groovy 34426 2014-05-26 09:55:08Z oleyba $ */
// Copyright 2006-2012 Tandem Service Software
package unirsmu.scripts

import org.apache.commons.collections15.CollectionUtils
import org.apache.commons.collections15.Predicate
import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.document.RtfHeader
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unibase.util.SimpleCollectionSelector
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentOrderParagraphPrintFormType
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrEnrollmentOrderPrintUtil
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrOrderPrintFormTypeCodes

import java.text.DateFormatSymbols
import java.text.SimpleDateFormat

import static ru.tandemservice.unirsmu.catalog.entity.codes.EnrEnrollmentOrderParagraphPrintFormTypeCodes.*

return new EnrollmentOrderPrint(                          // стандартные входные параметры скрипта
        session: session,                                 // сессия
        template: template,                               // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

/**
 * Алгоритм печати приказов о зачислении абитуриентов
 */
class EnrollmentOrderPrint
{
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def ruMonths = getDateFormatSymbols()
    def dateFormat = new SimpleDateFormat('"d" MMMMM yyyy г.', ruMonths)
    def dateFormat2 = new SimpleDateFormat('d MMMMM yyyy года', ruMonths)

    static DateFormatSymbols getDateFormatSymbols()
    {
        DateFormatSymbols ret = new DateFormatSymbols(new Locale("ru"))
        ret.setMonths(["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"] as String[])
        return ret
    }

    def print()
    {
        def coreDao = IUniBaseDao.instance.get()
        def academy = TopOrgUnit.instance

        // заполнение меток
        im.put('commitDate', order.commitDate ? dateFormat.format(order.commitDate) : '')
        im.put('enrollmentDate', order.actionDate ? dateFormat2.format(order.actionDate) : '')
        im.put('orderNumber', order.number)
        im.put('highSchoolTitle', academy.title)
        im.put('highSchoolCity', academy.address?.settlement?.titleWithType)
        im.put('orderBasicText', order.orderBasicText)
        im.put('orderText', order.orderText)
        im.put('textParagraph', order.textParagraph)
        im.put('executor', order.executor)
        im.put('orderReason', order.reason?.title)
        im.put('orderBasic', order.basic?.title)

        im.put('reasonText', order.orderBasicText)
        im.put('orderDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))
        im.put('executorFio', order.executor)

//        List<IAbstractParagraph> paragraphList = order.paragraphList
//        if (paragraphList.size() > 0 && paragraphList[0] instanceof EnrEnrollmentParagraph)
//        {
//            EnrEnrollmentOrderPrintUtil.initOrgUnit(im,
//                    ((EnrEnrollmentParagraph) paragraphList[0]).formativeOrgUnit, 'formativeOrgUnit', '')
//        }
//
        // заполнение виз
        def visaGroupList = coreDao.getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order))
        {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('VISAS', printVisaList as String[][])

        // заполнение параграфов
        def document = new RtfReader().read(template)
        fillParagraphs(document.header)
        im.modify(document)
        tm.modify(document)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentOrder.rtf']
    }

    static String getSpecialColumn(String templateCode)
    {
        switch (templateCode)
        {
            case RSMU_BS_EXCLUSIVE: return  'вне конкурса'
            case RSMU_BS_NO_EXAM:
            case RSMU_BS_NO_EXAM_CONTRACT: return 'без экзаменов'
        }
        return ''
    }

    void fillParagraphs(RtfHeader header)
    {
        List<IRtfElement> paragraphList = []
        for (IAbstractParagraph abstractParagraph : order.paragraphList)
        {
            if (!(abstractParagraph instanceof EnrEnrollmentParagraph))
                throw new ApplicationException("Параграф №${abstractParagraph.number} в приказе «" + abstractParagraph.getOrder().getTitle() + "» не может быть напечатан, так как не является параграфом о зачислении.")

            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph


            List<EnrEnrollmentExtract> enrollmentExtractList = (List<EnrEnrollmentExtract>) paragraph.extractList
            if (enrollmentExtractList.size() == 0)
                throw new ApplicationException("Пустой параграф №${paragraph.number} в приказе «" + paragraph.getOrder().getTitle() + "» не может быть напечатан.")

            def firstExtract = enrollmentExtractList.get(0)
            def competition = ((EnrRequestedCompetition) firstExtract.entity).competition
            List<EnrProgramSetItem> programs = IUniBaseDao.instance.get().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), competition.getProgramSetOrgUnit().getProgramSet());
            // заполняем метки в шаблоне параграфа
            EnrRequestedCompetitionTA requestedCompetitionTA = firstExtract.requestedCompetition instanceof EnrRequestedCompetitionTA ? (EnrRequestedCompetitionTA) firstExtract.requestedCompetition : null

            def injectModifier = new RtfInjectModifier()
                    .put('parNumber', paragraph.number.toString())
                    .put('enrollmentDate', order.actionDate ? dateFormat2.format(order.actionDate) : '')
                    .put('developForm', paragraph.programForm.shortTitle)
                    .put('developFormStr_A', getProgramFormStr_A(paragraph.programForm))
                    .put('developPeriod', UniStringUtils.joinUniqueSorted(programs, EnrProgramSetItem.program().duration().title().s(), ", "))
                    .put('educationOrgUnit', paragraph.programSubject.title)
                    .put('foreign_students', EnrOrderPrintFormTypeCodes.RSMU_FOREIGN_ENROLLMENT.equals(order.printFormType.code) ? ' лиц, являющихся гражданами иностранных государств' : '')
                    .put('targetKind_I', requestedCompetitionTA?.targetAdmissionOrgUnit?.title ?: '')
                    .put('targetContractDate', requestedCompetitionTA?.contractDate ? dateFormat2.format(requestedCompetitionTA.contractDate) : '')

            EnrEnrollmentOrderPrintUtil.initOrgUnit(injectModifier, paragraph.formativeOrgUnit, 'formativeOrgUnit', '')

            final String label = "educationType"
            String programKindCode = paragraph.programSubject.getEduProgramKind().getCode()
            final String[] types = EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programKindCode) || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programKindCode) ? UniRtfUtil.SPECIALITY_CASES : (EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_.equals(programKindCode) ? UniRtfUtil.EDU_PROFESSION_CASES : UniRtfUtil.EDU_DIRECTION_CASES);
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                injectModifier.put(label + UniRtfUtil.CASE_POSTFIX.get(i), types[i])
            }

            def tableModifier = new RtfTableModifier()

            def paragraphTemplate = getEnrollmentOrderParagraphTemplate(paragraph.getId())

            def studentList = new ArrayList<String[]>(enrollmentExtractList.size())
            int counter = 1
            for (def extract : enrollmentExtractList)
            {
                def requestedEnrollmentDirection = extract.entity
                def person = requestedEnrollmentDirection.request.entrant.person
                EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())
                def sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble();

                studentList.add([
                        counter++,
                        requestedEnrollmentDirection.request.entrant.personalNumber,
                        person.fullFio.toUpperCase(),
                        getSpecialColumn(paragraphTemplate.code),
                        sumMark ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark) : "",
                ] as String[])
            }
            tableModifier.put('STUDENTS', studentList as String[][])

            def paragraphDocument = new RtfReader().read(paragraphTemplate.content)

            // подготавливаем клон шаблона параграфа для вставки в печатную форму приказа
            def paragraphPart = paragraphDocument.clone
            RtfUtil.modifySourceList(header, paragraphPart.header, paragraphPart.elementList)

            injectModifier.modify(paragraphPart)
            tableModifier.modify(paragraphPart)

            def group = RtfBean.elementFactory.createRtfGroup()
            group.elementList = paragraphPart.elementList
            paragraphList.add(group)
        }

        im.put('PARAGRAPHS', paragraphList)
    }

    static String getProgramFormStr_A(EduProgramForm programForm)
    {
        switch (programForm.code)
        {
            case EduProgramFormCodes.OCHNAYA: return 'очную (дневную) форму обучения'
            case EduProgramFormCodes.ZAOCHNAYA: return 'заочную форму обучения'
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA: return 'очно-заочную (вечернюю) форму обучения'
        }
        return ''
    }

    static EnrEnrollmentOrderParagraphPrintFormType getEnrollmentOrderParagraphTemplate(Long paragraphId)
    {
        EnrEnrollmentParagraph paragraph = IUniBaseDao.instance.get().getNotNull(paragraphId);

        final Long competitionTypeId = paragraph.getCompetitionType().getId();
        final Boolean parallel = paragraph.isParallel();
        final Long requestTypeId = ((EnrOrder) paragraph.getOrder()).getRequestType().getId();

        Collection<EnrEnrollmentOrderParagraphPrintFormType> printForms = IUniBaseDao.instance.get().getList(EnrEnrollmentOrderParagraphPrintFormType.class);
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> initialSortPredicate = new Predicate<EnrEnrollmentOrderParagraphPrintFormType>() {
            @Override
            public boolean evaluate(EnrEnrollmentOrderParagraphPrintFormType type)
            {
                // убираем неподходящие печатные формы: у них заданы вид заявления, вид приема, параллельность, и они отличаются указанных в параграфе
                return (type.getRequestType() == null || type.getRequestType().getId().equals(requestTypeId)) &&
                        (type.getCompetitionType() == null || type.getCompetitionType().getId().equals(competitionTypeId)) &&
                        (type.getParallel() == null || type.getParallel().equals(parallel));
            }
        };

        printForms = CollectionUtils.select(printForms, initialSortPredicate);
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> requestTypePredicate = new Predicate<EnrEnrollmentOrderParagraphPrintFormType>(){
            @Override public boolean evaluate(EnrEnrollmentOrderParagraphPrintFormType type){ return type.getRequestType() != null && type.getRequestType().getId().equals(requestTypeId); }};
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> competitionTypePredicate = new Predicate<EnrEnrollmentOrderParagraphPrintFormType>(){
            @Override public boolean evaluate(EnrEnrollmentOrderParagraphPrintFormType type){ return type.getCompetitionType() != null && type.getCompetitionType().getId().equals(competitionTypeId); }};
        Predicate<EnrEnrollmentOrderParagraphPrintFormType> parallelPredicate = new Predicate<EnrEnrollmentOrderParagraphPrintFormType>(){
            @Override public boolean evaluate(EnrEnrollmentOrderParagraphPrintFormType type){ return type.getParallel() != null && type.getParallel().equals(parallel); }};
        List<Predicate<? super EnrEnrollmentOrderParagraphPrintFormType>> predicates = Arrays.<Predicate<? super EnrEnrollmentOrderParagraphPrintFormType>>asList(requestTypePredicate, competitionTypePredicate, parallelPredicate);
        SimpleCollectionSelector.StopCriterion<EnrEnrollmentOrderParagraphPrintFormType> stopCriterion = new SimpleCollectionSelector.StopCriterion<EnrEnrollmentOrderParagraphPrintFormType>(){
            @Override public boolean isSatisfied(Collection<EnrEnrollmentOrderParagraphPrintFormType> source){ return source.size() == 1; }};

        SimpleCollectionSelector<EnrEnrollmentOrderParagraphPrintFormType> collectionSelector = new SimpleCollectionSelector<>(printForms, stopCriterion, predicates);

        Collection<EnrEnrollmentOrderParagraphPrintFormType> result = collectionSelector.select();

        if (result.size() == 1)
            return result.iterator().next()

        // не нашли подходящий, берем базовый
        result = CollectionUtils.select(printForms, new Predicate<EnrEnrollmentOrderParagraphPrintFormType>(){
            @Override public boolean evaluate(EnrEnrollmentOrderParagraphPrintFormType type){ return type.getRequestType() == null && type.getCompetitionType() == null && type.getParallel() == null; }});

        if (result.size() == 1)
            return result.iterator().next()

        throw new IllegalStateException(); // что-то поменялось, ключ Вид заявления + Вид приема + Параллельность больше не является уникальным (или нет базового)
    }
}
