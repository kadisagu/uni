package unirsmu.scripts

import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.CoreDateUtils
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPersonActivityBase
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.uni.util.UniStringUtils
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind
import ru.tandemservice.unieductr.base.entity.EduCtrContractVersionTemplateData
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData

return new EduCtrContractPrintForeign (
        session: session,   // сессия
        template: template, // шаблон
        customer: DataAccessServices.dao().get(CtrContractVersionContractor.class, customerId), // заказчик
        provider: DataAccessServices.dao().get(CtrContractVersionContractor.class, providerId), // продавец
        versionTemplateData: DataAccessServices.dao().get(EduCtrContractVersionTemplateData.class, versionTemplateDataId), // договора по количеству и типу контрагентов
        eduPromise: DataAccessServices.dao().get(EduCtrEducationPromise.class, eduPromiseId) // жертва
).print()
/**
 * @author Denis Perminov
 * @since 31.07.2014
 */
class EduCtrContractPrintForeign {
    Session session
    byte[] template
    CtrContractVersionContractor customer
    CtrContractVersionContractor provider
    IEducationContractVersionTemplateData versionTemplateData
    EduCtrEducationPromise eduPromise

    RtfInjectModifier im = new RtfInjectModifier()
    RtfTableModifier  tm = new RtfTableModifier()

    def print() {
        // Оплату РНИМУ выбирают за семестр!

        List<CtrPaymentPromice> paymentPromises = DQL.createStatement(session, /
                from ${CtrPaymentPromice.class.simpleName}
                where ${CtrPaymentPromice.src().id()}=${customer.id}
                and ${CtrPaymentPromice.dst().id()}=${provider.id}
                order by ${CtrPaymentPromice.deadlineDate()}
                /).<CtrPaymentPromice> list()

        if (paymentPromises.empty)
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по семестровой оплате.")

        def eduProgram = eduPromise.eduProgram
        short years  = eduProgram.duration.numberOfYears
        short months = eduProgram.duration.numberOfMonths

//        if (2 * years + (months > 0 ? 1 : 0) != paymentPromises.size())
//            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по семестровой оплате.")

        // По договору - "за ОДИН семестр". Не "за первый".
        Double eduPriceFirstYearSem = org.tandemframework.shared.commonbase.catalog.entity.Currency.wrap((Long) paymentPromises.get(0).costAsLong)
        im.put("eduPriceFirstYearSem", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(eduPriceFirstYearSem))
        im.put("eduPriceFirstYearSemStr", NumberSpellingUtil.spellNumberMasculineGender(eduPriceFirstYearSem.intValue()))
        im.put("eduPriceFirstYearSemKop", String.valueOf(Math.round(100.0d * eduPriceFirstYearSem) - 100 * eduPriceFirstYearSem.intValue()))

        // По договору - "за ОДИН год". Не "за первый".
//        Double eduPriceFirstYear = eduPriceFirstYearSem + Currency.wrap((Long) paymentPromises.get(1).costAsLong)
        Double eduPriceFirstYear = 2 * eduPriceFirstYearSem
        im.put("eduPriceFirstYear", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(eduPriceFirstYear))
        im.put("eduPriceFirstYearStr", NumberSpellingUtil.spellNumberMasculineGender(eduPriceFirstYear.intValue()))
        im.put("eduPriceFirstYearKop", String.valueOf(Math.round(100.0d * eduPriceFirstYear) - 100 * eduPriceFirstYear.intValue()))

//        Double eduPriceAll = Currency.wrap((Long) paymentPromises.sum { e -> e.costAsLong })
        Double eduPriceAll = (2 * years + (months > 0 ? 1 : 0)) * eduPriceFirstYearSem
        im.put("eduPriceAll", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(eduPriceAll))
        im.put("eduPriceAllStr", NumberSpellingUtil.spellNumberMasculineGender(eduPriceAll.intValue()))
        im.put("eduPriceAllKop", String.valueOf(Math.round(100.0d * eduPriceAll) - 100 * eduPriceAll.intValue()))

        CtrContractVersion contractVersion = versionTemplateData.owner
        Person student = eduPromise.dst.contactor.person

        // данные договора
        EnrEntrant entrant = ((EnrContractTemplateData) versionTemplateData).requestedCompetition.request.entrant
//        im.put("contractNumber", contractVersion.number)
        im.put("contractNumber", entrant.personalNumber)
        im.put("formingDay", String.valueOf(CoreDateUtils.getDayOfMonth(contractVersion.docStartDate)))
        im.put("formingMonthStr", RussianDateFormatUtils.getMonthName(contractVersion.docStartDate, false))
        im.put("formingYr", RussianDateFormatUtils.getYearString(contractVersion.docStartDate, true))

        im.put("studentFio",  student.fullFio)
        im.put("calledStudent", student.male ? "именуемый" : "именуемая")
        def orgUnit = eduProgram.ownerOrgUnit.orgUnit
        im.put("formativeOrgUnit_G", null != orgUnit.genitiveCaseTitle ? orgUnit.genitiveCaseTitle : orgUnit.printTitle)

        def educationLevel
        EduProgramKind programKind = eduProgram.programSubject.subjectIndex.programKind
        if (programKind.programBasic)
            educationLevel = "начальное профессиональное образование"
        else if (programKind.programSecondaryProf)
            educationLevel = "среднее профессиональное образование"
        else if (programKind.programBachelorDegree)
            educationLevel = "бакалавриат"
        else if (programKind.programSpecialistDegree)
            educationLevel = "специалитет"
        else if (programKind.programAdditionalProf)
            educationLevel = "дополнительное профессиональное образование"
        else
            educationLevel = programKind.title.toLowerCase()
        im.put("educationLevelStr", educationLevel)

        im.put("speciality", eduProgram.programSubject.titleWithCode.toLowerCase())
        im.put("developForm", eduProgram.form.title.toLowerCase())
        im.put("apprenticeshipGOS", eduProgram.duration.title)
        im.put("qualification", eduProgram.programQualification?.title)

        IdentityCard card = student.identityCard
        def country = card.citizenship.fullTitle
        im.put("country_G", "государства " + country)
        im.put("studentPassportSeria", card.seria)
        im.put("studentPassportNumber", card.number)
        im.put("studentPassportDate", null != card.issuanceDate ? DateFormatter.DEFAULT_DATE_FORMATTER.format(card.issuanceDate) : "")
        im.put("studentPassportInfo", StringUtils.trimToEmpty(card.issuancePlace))
        im.put("studentRegAddress", null != card.address ? card.address.titleWithFlat : "")
        im.put("studentFactAddress", null != student.address ? student.address.titleWithFlat : "")
        im.put("phone", student.contactData.mainPhones)

        printCustomerData(customer.contactor, im)

        // стандартные выходные параметры скрипта
        RtfDocument document = new RtfReader().read(template)

        im.modify(document)
        tm.modify(document)

        CommonBaseRenderer renderer = new CommonBaseRenderer()
                .rtf()
                .fileName("Договор ${contractVersion.getNumber()}.rtf")
                .document(RtfUtil.toByteArray(document))
        return [renderer: renderer]
    }

    def printCustomerData(ContactorPerson customer, RtfInjectModifier cim) {
        String foundation
        String phone
        String regAddress
        String factAddress
        Person person = customer.person

        if (customer instanceof JuridicalContactor) {
            JuridicalContactor jCustomer = (JuridicalContactor) customer
            ContactorPersonActivityBase cActivityBase = ContactorManager.instance().dao().getCurrentCotactorPersonActivity(jCustomer)
            ExternalOrgUnit extOrgUnit = jCustomer.externalOrgUnit

            cim.put("customerOrg", extOrgUnit.getLegalFormWithTitle())

            foundation = (null != cActivityBase ? cActivityBase.getDisplayableTitle() : "")

            cim.put("position", StringUtils.isEmpty(jCustomer.post) ? "представитель" : jCustomer.post)

            regAddress = (null != extOrgUnit.legalAddress ? extOrgUnit.legalAddress.titleWithFlat : "")
            factAddress = (null != extOrgUnit.factAddress ? extOrgUnit.factAddress.titleWithFlat : "")
            cim.put("customerINNKPP", UniStringUtils.joinWithSeparator("/", StringUtils.trimToEmpty(extOrgUnit.inn), StringUtils.trimToEmpty(extOrgUnit.kpp)))
            cim.put("customerCurAcc", StringUtils.trimToEmpty(extOrgUnit.curAccount))
            cim.put("customerBankTitle", StringUtils.trimToEmpty(extOrgUnit.bank))
            cim.put("customerCorAcc", StringUtils.trimToEmpty(null != extOrgUnit.corAccount ? "корр.счет " + extOrgUnit.corAccount : ""))
            cim.put("customerBIK", StringUtils.trimToEmpty(extOrgUnit.bic))
            cim.put("customerOKATO", StringUtils.trimToEmpty(extOrgUnit.okato))
            cim.put("customerKBK", "")
            phone = StringUtils.trimToEmpty(StringUtils.trimToEmpty(extOrgUnit.phone) + (null != extOrgUnit.fax ? "(" + extOrgUnit.fax + ")" : ""))
        } else {
            IdentityCard card = person.identityCard
            def country = card.citizenship.fullTitle
            cim.put("customerCountry_G", "государства " + country)

            def personActivity = DQL.createStatement(session, /
                    from ${ContactorPersonActivityBase.class.simpleName}
                    where ${ContactorPersonActivityBase.owner().id()}=${customer.id}
                    /).setMaxResults(1).<ContactorPersonActivityBase> uniqueResult()
            foundation = (null != personActivity ? personActivity.type.title +
                    (personActivity.type.requireNumberAndDate ?
                            (null != personActivity.documentNumber ? " " + personActivity.documentNumber : "") +
                            (null != personActivity.documentIssuanceDate ? " от " + personActivity.documentIssuanceDate : "") +
                            (null != personActivity.documentIssuancePlace ? ", выдан(а) " + personActivity.documentIssuancePlace : "") : "") : "")
            cim.put("calledCustomer", person.male ? "именуемый" : "именуемая")
            cim.put("customerPassportSeria", card.seria)
            cim.put("customerPassportNumber", card.number)
            cim.put("customerPassportDate", null != card.issuanceDate ? DateFormatter.DEFAULT_DATE_FORMATTER.format(card.issuanceDate) : "")
            cim.put("customerPassportInfo",StringUtils.trimToEmpty(card.issuancePlace))
            regAddress = (null != card.address ? card.address.titleWithFlat : "")
            factAddress = (null != person.address ? person.address.titleWithFlat : "")
            phone = StringUtils.trimToEmpty(person.contactData.mainPhones)
        }
        cim.put("customerRegAddress", regAddress)
        cim.put("customerFactAddress", factAddress)
        cim.put("customerFio", person.fullFio)
        cim.put("foundation", foundation)
        cim.put("customerPhone", phone)
    }
}