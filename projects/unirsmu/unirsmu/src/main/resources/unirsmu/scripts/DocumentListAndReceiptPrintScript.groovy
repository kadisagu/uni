package unirsmu.scripts

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unienr14.catalog.entity.EnrBenefitCategory
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new DocumentListAndReceiptPrint(                              // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * скрипт печати описи и расписки абитуриента
 * @author Denis Perminov
 * @since 17.06.2014
 */
class DocumentListAndReceiptPrint {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    def print() {
        Person person = entrantRequest.entrant.person

        im.put("highSchoolTitle", TopOrgUnit.instance.title)
        im.put("inventoryNumber", entrantRequest.stringNumber)
        im.put("entrantNumber", entrantRequest.entrant.personalNumber)
        im.put("FIO", person.fullFio)
        im.put("receiptNumber", entrantRequest.stringNumber)
        im.put("fromFIO", PersonManager.instance().declinationDao().getDeclinationFIO(person.identityCard, GrammaCase.GENITIVE))
        im.put("dateReq", DateFormatter.STRING_MONTHS_AND_QUOTES.format(entrantRequest.regDate))

        def titles = getTitles()

        int i = 1
        tm.put("T1", titles.collect { [i++, it] } as String[][])
        i = 1
        tm.put("T2", titles.collect { [(i++) + ".", it] } as String[][])

        EnrEntrant entrant = entrantRequest.entrant

        def exclusiveCompetitions = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetitionExclusive.class, "erce")
                .column(property("erce", EnrRequestedCompetitionExclusive.benefitCategory()))
                .predicate(DQLPredicateType.distinct)
                .where(eq(property("erce", EnrRequestedCompetitionExclusive.L_ENTRANT), value(entrant)))
                .order(property("erce", EnrRequestedCompetitionExclusive.benefitCategory().title()))
                .createStatement(session).<EnrBenefitCategory> list()

        if (!exclusiveCompetitions.empty)
            im.put("benefitName", exclusiveCompetitions.collect {e -> e.titleWithType}.join(", "))
        else
            deleteLabels.add("benefitName")

        fillInternalExamSchedule()

        RtfDocument document = new RtfReader().read(template)

        im.modify(document)
        tm.modify(document)
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Опись и расписка абитуриента ${person.identityCard.fullFio}.rtf"]
    }

    def getTitles() {
        def idc = entrantRequest.identityCard
        def edu = entrantRequest.eduDocument

        def attachments = DQL.createStatement(session, /
                from ${EnrEntrantRequestAttachment.class.simpleName}
                where ${EnrEntrantRequestAttachment.entrantRequest().id()}=${entrantRequest.id}
                order by ${EnrEntrantRequestAttachment.id()}
                /).<EnrEntrantRequestAttachment> list().sort()

        def titles = new ArrayList<String>()

        titles.add(idc.getDisplayableTitle() + " (копия)")
        titles.add(edu.getDisplayableTitle() + (entrantRequest.eduInstDocOriginalHandedIn ? " (оригинал)" : " (копия)"))

        for (def attachment : attachments) {
            titles.add(attachment.document.displayableTitle + (attachment.originalHandedIn ? " (оригинал)" : " (копия)"))
        }
        return titles
    }

    def fillInternalExamSchedule() {
        EnrEntrant entrant = entrantRequest.entrant
        def eepd = DQL.createStatement(session, /
                from ${EnrExamPassDiscipline.class.simpleName}
                where ${EnrExamPassDiscipline.entrant().id()}=${entrant.id}
                and ${EnrExamPassDiscipline.retake()}=${false}
                order by ${EnrExamPassDiscipline.discipline().discipline().title()}, ${EnrExamPassDiscipline.passForm().code()}
                /).<EnrExamPassDiscipline> list()

        if (!eepd.empty) {
            def rows = Lists.newArrayList()

            // для каждой дисциплины для сдачи
            for (EnrExamPassDiscipline exam : eepd) {
                List<EnrExamGroupScheduleEvent> eegse = (null == exam.examGroup ? Collections.emptyList() :
                        DQL.createStatement(session, /
                                from ${EnrExamGroupScheduleEvent.class.simpleName}
                                where ${EnrExamGroupScheduleEvent.examGroup().id()}=${exam.examGroup.id}
                                order by ${EnrExamGroupScheduleEvent.examScheduleEvent().scheduleEvent().durationBegin()}
                                /).<EnrExamGroupScheduleEvent> list())

                StringBuilder dateTimePlace = new StringBuilder()
                for (EnrExamGroupScheduleEvent event : eegse)
                    dateTimePlace.append(event.timeTitle).append(", ")
                                    .append(event.examScheduleEvent.examRoom.place.displayableTitle).append(" (")
                                    .append(event.examScheduleEvent.examRoom.place.fullLocationInfo).append(")\n")
                // формируем строку из трех столбцов
                String[] row = new String[3]

                row[0] = exam.discipline.title + " (" + exam.passForm.title + ")"
                row[1] = exam.examGroup?.title
                row[2] = dateTimePlace.toString()

                rows.add(row)
            }
            tm.put("T3", rows as String[][])
        } else
            tm.remove("T3")
    }
}