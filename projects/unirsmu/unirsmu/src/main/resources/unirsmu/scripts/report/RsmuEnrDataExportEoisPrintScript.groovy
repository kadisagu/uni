package unirsmu.scripts.report

import com.beust.jcommander.internal.Lists
import com.beust.jcommander.internal.Maps
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.Label
import jxl.write.WritableCellFormat
import org.hibernate.Session
import org.tandemframework.core.CoreStringUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.YesNoFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLFunctions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.shared.fias.IKladrDefines
import org.tandemframework.shared.fias.base.entity.*
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonEduDocument
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.entrant.entity.*
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.request.entity.EnrEntrantBenefitProof
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitProofDocument
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
/**
 * @author Andrey Avetisov
 * @since 05.02.2015
 */

return new RsmuEnrDataEoisExportPrint(
        session: session,
        enrollmentCampaign: enrollmentCampaign,
        dateFromActive: dateFromActive,
        dateToActive: dateToActive,
        dateFrom: dateFrom,
        dateTo: dateTo

).print()


class RsmuEnrDataEoisExportPrint
{
    Session session
    EnrEnrollmentCampaign enrollmentCampaign
    Date dateFrom
    boolean dateFromActive
    Date dateTo
    boolean dateToActive
    ByteArrayOutputStream out = new ByteArrayOutputStream();

    def print()
    {

        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);

        def sheet = workbook.createSheet("Данные зачисленных абитуриентов", 0);
        String[] headerTitles = ["1.1. № дела", "1.2. Фамилия", "1.3. Имя", "1.4. Отчество", "1.5. Пол", "1.6. Дата рождения",
                                 "1.7. Место рождения", "1.8. Гражданство", "1.9. Национальность",
                                 "2.1. тип документа ДУЛ", "2.2. серия ДУЛ", "2.3. номер ДУЛ", "2.4. Дата выдачи ДУЛ", "2.5. кем выдан ДУЛ",
                                 "3.1. индекс", "3.2. субъект РФ", "3.3. район", "3.4. город", "3.5. село", "3.6. улица", "3.7. дом", "3.8. корпус", "3.9. квартира", "3.10. адрес не из КЛАДР",
                                 "4.1. телефон моб", "4.2. телефон дом", "4.3. телефон контактный", "4.4. Email",
                                 "5.1. Иностранный язык", "5.2. Уровень владения",
                                 "6.1. Иностранный язык", "6.2. Оценка",
                                 "7.1. Тип льготы", "7.2. Тип документа, подтверждающего льготу", "7.3. Серия документа, подтверждающего льготу",
                                 "7.4. Номер документа, подтверждающего льготу", "7.5. Дата документа, подтверждающего льготу", "7.6. Кем выдан",
                                 "8.1. Тип документа", "8.2. Тип образовательного учреждения", "8.3. Серия документа", "8.4. Номер документа",
                                 "8.5. Наименование образовательного учреждения", "8.6. Год окончания", "8.7. Тип медали",
                                 "8.8. Базовый уровень (среднее, среднее профессиональное, неоконченное высшее, высшее)",
                                 "9.1. факультет", "9.2. специальность", "9.3. квалификация", "9.4. форма обучения", "9.5. период обучения",
                                 "9.6. форма финансирования (бюджет. договор с физ лицом, договор с юр лицом)", "9.7. целевое направление (да/нет)", "9.8. Регион целевого направления",
                                 "10.1. Номер приказа о зачислении", "10.2. Дата приказа", "11. Доп. статус абитуриента"];

        int colN = 0;
        headerTitles.each { sheet.addCell(new Label(colN++, 0, it)) }

        List<EnrEnrollmentExtract> extractList = getEnrollmentExtractList();

        List<Person> personList = new ArrayList<>(extractList.size())
        Set<EnrProgramSetBase> programSetBaseList = new HashSet<>()
        for (EnrEnrollmentExtract extract : extractList)
        {
            personList.add(extract.entity.request.entrant.person)
            programSetBaseList.add(extract.entity.competition.programSetOrgUnit.programSet)
        }

        Map<Person, List<PersonForeignLanguage>> personLangMap = getPersonLanguageMap(personList)
        Map<EnrEntrant, List<EnrEntrantStateExamResult>> entrantLanguageExamMap = getExamLanguageMap(personList)
        Map<EnrEntrant, List<EnrEntrantBenefitProof>> entrantBenefitMap = getEntrantBenefitMap(personList)
        Map<EnrEntrant, EnrEntrantOriginalDocumentStatus> entrantEduDocMap = getOriginalDocumentMap(personList)
        Map<EnrProgramSetBase, EnrProgramSetItem> eduProgramMap = getEduProgramMap(programSetBaseList)
        Map<EnrEntrant, List<EnrEntrantCustomState>> entrantCustomStateMap = getEntrantCustomStateMap(personList);

        int rowNumber = 1;
        for (EnrEnrollmentExtract extract : extractList)
        {
            EnrRequestedCompetition reqComp = extract.entity
            EnrEntrant entrant = reqComp.request.entrant
            Person person = entrant.person
            IdentityCard identityCard = person.identityCard
            AddressBase address = identityCard.address
            AddressDetailed addressDetailed = address instanceof AddressDetailed ? address : null
            AddressInter addressInter = addressDetailed instanceof AddressInter ? addressDetailed : null
            AddressRu addressRu = addressDetailed instanceof AddressRu ? addressDetailed : null
            AddressString addressString = addressDetailed instanceof AddressString ? addressDetailed : null
            String personForeignLanguageTitle = ""
            String personForeignLanguageMark = ""
            EnrRequestedCompetitionTA reqCompTA = reqComp instanceof EnrRequestedCompetitionTA ? reqComp : null;
            for (PersonForeignLanguage language : personLangMap.get(person))
            {
                personForeignLanguageTitle.length() > 0 ? personForeignLanguageTitle += ", " : " "
                personForeignLanguageTitle += language.language.title

                personForeignLanguageMark.length() > 0 ? personForeignLanguageMark += ", " : " "
                personForeignLanguageMark += language.skill != null ? language.skill.title : " ";
            }


            String entrantForeignExamTitle = ""
            String entrantForeignExamMark = ""
            for (EnrEntrantStateExamResult exam : entrantLanguageExamMap.get(entrant))
            {
                entrantForeignExamTitle.length() > 0 ? entrantForeignExamTitle += ", " : " "
                entrantForeignExamTitle += exam.subject.title

                entrantForeignExamMark.length() > 0 ? entrantForeignExamMark += ", " : " "
                entrantForeignExamMark += exam.mark != null ? exam.mark : " ";
            }

            String benefitType = ""
            String benefitDocument = ""
            String docSeria = ""
            String docNumber = ""
            String issuanceDate = ""
            String issuancePlace = ""
            EnrEntrantOriginalDocumentStatus entrantEduDoc = entrantEduDocMap.get(entrant);
            PersonEduDocument eduDocument = entrantEduDoc?.eduDocument
            Set<IEnrEntrantBenefitProofDocument> usedDocument = new HashSet<>();

            for (EnrEntrantBenefitProof benefit : entrantBenefitMap.get(entrant))
            {
                if (benefit.benefitStatement.benefitCategory != null && !benefitType.contains(benefit.benefitStatement.benefitCategory.titleWithType))
                {
                    benefitType.length() > 0 ? benefitType += ", " : " "
                    benefitType += benefit.benefitStatement.benefitCategory.titleWithType;
                }

                if(!usedDocument.contains(benefit.document))
                {
                    benefitDocument.length() > 0 ? benefitDocument += ", " : " "
                    benefitDocument += benefit.document.documentType != null ? benefit.document.documentType.title : " ";

                    docSeria.length() > 0 ? docSeria += ", " : " "
                    docSeria += benefit.document.seria != null ? benefit.document.seria : " ";

                    docNumber.length() > 0 ? docNumber += ", " : " "
                    docNumber += benefit.document.number != null ? benefit.document.number : " ";

                    issuanceDate.length() > 0 ? issuanceDate += ", " : " "
                    issuanceDate += benefit.document.issuanceDate != null ? benefit.document.issuanceDate.format("dd.MM.yyyy") : " ";



                    issuancePlace.length() > 0 ? issuancePlace += ", " : " "
                    issuancePlace += (benefit.document != null
                            && benefit.document instanceof EnrEntrantBaseDocument
                            && ((EnrEntrantBaseDocument) benefit.document).issuancePlace != null) ? ((EnrEntrantBaseDocument) benefit.document).issuancePlace : "";

                    usedDocument.add(benefit.document)
                }
            }

            //формируем выходную строку доп.статусов + сортируем по алфавиту доп. статусы
            StringBuilder customStateString = new StringBuilder();
            entrantCustomStateMap.getOrDefault(entrant, Lists.newArrayList()).sort { a, b -> a.customState.title.compareTo(b.customState.title) }.each { state ->
                customStateString.append(state.getCustomState().getTitle()).append("\n");
            }

            sheet.addCell(new Label(0, rowNumber, reqComp.request.entrant.personalNumber))
            sheet.addCell(new Label(1, rowNumber, identityCard.lastName))
            sheet.addCell(new Label(2, rowNumber, identityCard.firstName))
            sheet.addCell(new Label(3, rowNumber, identityCard.middleName))
            sheet.addCell(new Label(4, rowNumber, identityCard.sex.title))
            sheet.addCell(new Label(5, rowNumber, identityCard.birthDate?.format("dd.MM.yyyy")))
            sheet.addCell(new Label(6, rowNumber, identityCard.birthPlace))
            sheet.addCell(new Label(7, rowNumber, identityCard.citizenship.shortTitle))
            sheet.addCell(new Label(8, rowNumber, identityCard.nationality?.title))
            sheet.addCell(new Label(9, rowNumber, identityCard.cardType.title))
            sheet.addCell(new Label(10, rowNumber, identityCard.seria))
            sheet.addCell(new Label(11, rowNumber, identityCard.number))
            sheet.addCell(new Label(12, rowNumber, identityCard.issuanceDate?.format("dd.MM.yyyy")))
            sheet.addCell(new Label(13, rowNumber, identityCard.issuancePlace))
            sheet.addCell(new Label(14, rowNumber, addressRu != null? addressRu.inheritedPostCode: addressInter?.postCode))
            sheet.addCell(new Label(15, rowNumber, addressDetailed != null ? getRegion(addressDetailed) : ""))
            sheet.addCell(new Label(16, rowNumber, addressRu != null ? addressRu.district?.title : addressInter?.district))
            sheet.addCell(new Label(17, rowNumber, getCity(addressDetailed)))
            sheet.addCell(new Label(18, rowNumber, getCountry(addressDetailed)))
            sheet.addCell(new Label(19, rowNumber, addressRu != null ? addressRu.getStreet()?.title : addressInter?.getAddressLocation()))
            sheet.addCell(new Label(20, rowNumber, addressRu?.houseNumber))
            sheet.addCell(new Label(21, rowNumber, addressRu?.houseUnitNumber))
            sheet.addCell(new Label(22, rowNumber, addressRu?.flatNumber))
            sheet.addCell(new Label(23, rowNumber, addressString?.title))
            sheet.addCell(new Label(24, rowNumber, person.contactData.phoneMobile))
            sheet.addCell(new Label(25, rowNumber, person.contactData.phoneFact))
            sheet.addCell(new Label(26, rowNumber, person.contactData.phoneDefault))
            sheet.addCell(new Label(27, rowNumber, person.contactData.email))
            sheet.addCell(new Label(28, rowNumber, personForeignLanguageTitle))
            sheet.addCell(new Label(29, rowNumber, personForeignLanguageMark))
            sheet.addCell(new Label(30, rowNumber, entrantForeignExamTitle))
            sheet.addCell(new Label(31, rowNumber, entrantForeignExamMark))
            sheet.addCell(new Label(32, rowNumber, benefitType))
            sheet.addCell(new Label(33, rowNumber, benefitDocument))
            sheet.addCell(new Label(34, rowNumber, docSeria))
            sheet.addCell(new Label(35, rowNumber, docNumber))
            sheet.addCell(new Label(36, rowNumber, issuanceDate))
            sheet.addCell(new Label(37, rowNumber, issuancePlace))
            sheet.addCell(new Label(38, rowNumber, eduDocument?.eduDocumentKind?.title))
            sheet.addCell(new Label(39, rowNumber, eduDocument?.documentEducationLevel))
            sheet.addCell(new Label(40, rowNumber, eduDocument?.seria))
            sheet.addCell(new Label(41, rowNumber, eduDocument?.number))
            sheet.addCell(new Label(42, rowNumber, eduDocument?.eduOrganization))
            sheet.addCell(new Label(43, rowNumber, eduDocument!=null?String.valueOf(eduDocument.yearEnd):""))
            sheet.addCell(new Label(44, rowNumber, eduDocument?.graduationHonour?.title))
            sheet.addCell(new Label(45, rowNumber, eduDocument?.eduLevel?.title))
            sheet.addCell(new Label(46, rowNumber, reqComp.competition.programSetOrgUnit.formativeOrgUnit.title))
            sheet.addCell(new Label(47, rowNumber, reqComp.competition.programSetOrgUnit.programSet.programSubject.titleWithCode))
            sheet.addCell(new Label(48, rowNumber, eduProgramMap.get(reqComp.competition.programSetOrgUnit.programSet).program.programQualification.title))
            sheet.addCell(new Label(49, rowNumber, reqComp.competition.programSetOrgUnit.programSet.programForm.title))
            sheet.addCell(new Label(50, rowNumber, eduProgramMap.get(reqComp.competition.programSetOrgUnit.programSet).program.duration.title))
            sheet.addCell(new Label(51, rowNumber, extract.order.compensationType.shortTitle))
            sheet.addCell(new Label(52, rowNumber, YesNoFormatter.INSTANCE.format(reqComp.competition.targetAdmission)))
            sheet.addCell(new Label(53, rowNumber, reqCompTA?.targetAdmissionKind?.title))
            sheet.addCell(new Label(54, rowNumber, extract.order.number))
            sheet.addCell(new Label(55, rowNumber, extract.order.commitDate.format("dd.MM.yyyy")))

            WritableCellFormat format = new WritableCellFormat();
            format.setWrap(true); // для показа содержимого ячейки в несколько строк
            sheet.addCell(new Label(56, rowNumber, customStateString.toString(), format))

            rowNumber++;
        }

        workbook.write();
        workbook.close();

        // стандартные выходные параметры скрипта
        return [document: out.toByteArray(),
                fileName: "entrant_data_export_eois_" + new Date().format("dd.MM.yyyy") + ".xls"]
    }

    private List<EnrEnrollmentExtract> getEnrollmentExtractList()
    {

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "rc")
                .column(property("rc"))
                .where(eqValue(property("rc", EnrEnrollmentExtract.entity().request().entrant().enrollmentCampaign()), enrollmentCampaign))
                .where(eqValue(property("rc", EnrEnrollmentExtract.entity().state().code()), EnrEntrantStateCodes.ENROLLED))
            if (dateFromActive)
                dql.where(ge(property(EnrEnrollmentExtract.paragraph().order().commitDate().fromAlias("rc")), valueDate(dateFrom)));
            if (dateToActive)
                dql.where(le(property(EnrEnrollmentExtract.paragraph().order().commitDate().fromAlias("rc")), valueDate(dateTo)));


        return dql.createStatement(session).list();
    }

    /**
     * @param address рег. адресс персоны
     * @return регион
     */
    static String getRegion(AddressDetailed addressDetailed)
    {
        def result = ""
        if (addressDetailed?.settlement?.country?.code != IKladrDefines.RUSSIA_COUNTRY_CODE)
        {
            return result;
        }

        def parent = addressDetailed?.settlement
        while (parent != null)
        {
            result = parent.title
            parent = parent.parent
        }

        return result;
    }

    /**
     * @param address рег. адресс персоны
     * @return город
     */
    static String getCity(AddressDetailed addressDetailed)
    {
        def result = ""
        def parent = addressDetailed?.settlement
        while (parent != null)
        {
            if (parent.addressType.code.equals(IKladrDefines.TYPE_CODE_CITY_LVL_1) || parent.addressType.code.equals(IKladrDefines.TYPE_CODE_CITY_LVL_3))
            {
                result = parent.titleWithType
                return result
            }
            parent = parent.parent
        }
        return result;
    }

    /**
     * @param address рег. адресс персоны
     * @return населенный пункт, не являющийся городом (деревня, село, станица и т.п.)
     */
    private static String getCountry(AddressDetailed addressDetailed)
    {
        def settlement = addressDetailed?.settlement

        if (settlement?.addressType?.countryside)
        {
            return settlement?.titleWithType
        }
        else
        {
            return "";
        }
    }

    //Персона и список иностранных языков, которыми она владеет (по собственному утверждению)
    private static Map<Person, List<PersonForeignLanguage>> getPersonLanguageMap(List<Person> personList)
    {
        Map<Person, List<PersonForeignLanguage>> personLanguageMap = new HashMap<>()
        for (PersonForeignLanguage language : DataAccessServices.dao().getList(PersonForeignLanguage.class, PersonForeignLanguage.L_PERSON, personList))
        {
            if (!personLanguageMap.containsKey(language.person))
            {
                List<PersonForeignLanguage> languageList = new ArrayList<>();
                languageList.add(language)
                personLanguageMap.put(language.person, languageList)
            }
            else
            {
                personLanguageMap.get(language.person).add(language)
            }
        }


        return personLanguageMap
    }

    //абитуриент и список ЕГЭ (с иностраннми языками)
    private Map<EnrEntrant, List<EnrEntrantStateExamResult>> getExamLanguageMap(List<Person> personList)
    {
        Map<EnrEntrant, List<EnrEntrantStateExamResult>> examLanguageMap = new HashMap<>()

        List<EnrEntrantStateExamResult> examList = new DQLSelectBuilder().fromEntity(EnrEntrantStateExamResult.class, "r")
                .column(property("r"))
                .where(DQLExpressions.in(property("r", EnrEntrantStateExamResult.entrant().person()), personList))
                .where(
                or(
                        likeUpper(property("r", EnrEntrantStateExamResult.subject().title().s()), value(CoreStringUtils.escapeLike("Французский язык"))),
                        likeUpper(property("r", EnrEntrantStateExamResult.subject().title().s()), value(CoreStringUtils.escapeLike("Немецкий язык"))),
                        likeUpper(property("r", EnrEntrantStateExamResult.subject().title().s()), value(CoreStringUtils.escapeLike("Испанский язык"))),
                        likeUpper(property("r", EnrEntrantStateExamResult.subject().title().s()), value(CoreStringUtils.escapeLike("Английский язык")))
                )

        ).createStatement(session).list()

        for (EnrEntrantStateExamResult exam : examList)
        {
            if (!examLanguageMap.containsKey(exam.entrant))
            {
                List<EnrEntrantStateExamResult> entrantExamList = new ArrayList<>();
                entrantExamList.add(exam)
                examLanguageMap.put(exam.entrant, entrantExamList)
            }
            else
            {
                examLanguageMap.get(exam.entrant).add(exam)
            }
        }


        return examLanguageMap
    }

    private static Map<EnrEntrant, List<EnrEntrantBenefitProof>> getEntrantBenefitMap(List<Person> personList)
    {
        Map<EnrEntrant, List<EnrEntrantBenefitProof>> entrantBenefitMap = new HashMap<>()
        for (EnrEntrantBenefitProof benefit : DataAccessServices.dao().getList(EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement().entrant().person(), personList))
        {
            if (!entrantBenefitMap.containsKey(benefit.benefitStatement.entrant))
            {
                List<EnrEntrantBenefitProof> benefitList = new ArrayList<>();
                benefitList.add(benefit)
                entrantBenefitMap.put(benefit.benefitStatement.entrant, benefitList)
            }
            else
            {
                entrantBenefitMap.get(benefit.benefitStatement.entrant).add(benefit)
            }
        }


        return entrantBenefitMap
    }

    private static Map<EnrEntrant, EnrEntrantOriginalDocumentStatus> getOriginalDocumentMap(List<Person> personList)
    {
        Map<EnrEntrant, EnrEntrantOriginalDocumentStatus> entrantDocMap = new HashMap<>()
        for (EnrEntrantOriginalDocumentStatus doc : DataAccessServices.dao().getList(EnrEntrantOriginalDocumentStatus.class, EnrEntrantOriginalDocumentStatus.entrant().person(), personList))
        {
            entrantDocMap.put(doc.entrant, doc);
        }


        return entrantDocMap
    }

    private static Map<EnrProgramSetBase, EnrProgramSetItem> getEduProgramMap(Set<EnrProgramSetBase> programSetBaseSet)
    {
        Map<EnrProgramSetBase, EnrProgramSetItem> eduProgramMap = new HashMap<>()
        for (EnrProgramSetItem item : DataAccessServices.dao().getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), programSetBaseSet))
        {
            eduProgramMap.put(item.programSet, item);
        }


        return eduProgramMap
    }

    private Map<EnrEntrant, List<EnrEntrantCustomState>> getEntrantCustomStateMap(List<Person> personList) {
        String alias = "customState";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrantCustomState.class, alias)
                .where(DQLExpressions.in(EnrEntrantCustomState.entrant().person().id().fromAlias(alias), personList))
                .where(eq(property(EnrEntrantCustomState.entrant().enrollmentCampaign().fromAlias(alias)), value(enrollmentCampaign)))
                .where(eq(DQLFunctions.substring(property(EnrEntrantCustomState.customState().title().fromAlias("customState")), 1, 1), value("*"))); // ищем знак * вначале тайтла

        //фильтры вхождению по датам приказа
        if (null != dateFrom && null != dateTo) {
            dql.where(and(
                    not(
                            and(
                                    not(isNull(property(alias, EnrEntrantCustomState.beginDate()))),
                                    gt(property(alias, EnrEntrantCustomState.beginDate()), valueDate(dateTo)))
                    ),
                    not(
                            and(
                                    not(isNull(property(alias, EnrEntrantCustomState.endDate()))),
                                    lt(property(alias, EnrEntrantCustomState.endDate()), valueDate(dateFrom)))

                    )));
        } else if (null != dateFrom && null == dateTo) {
            dql.where(not(
                    and(
                            not(isNull(property(alias, EnrEntrantCustomState.endDate()))),
                            lt(property(alias, EnrEntrantCustomState.endDate()), valueDate(dateFrom)))
            ));
        } else if (null == dateFrom && null != dateTo) {
            dql.where(not(
                    and(
                            not(isNull(property(alias, EnrEntrantCustomState.beginDate()))),
                            gt(property(alias, EnrEntrantCustomState.beginDate()), valueDate(dateTo)))
            ));
        }

        List<EnrEntrantCustomState> entrantCustomStateList = dql.createStatement(session).list();
        Map<EnrEntrant, List<EnrEntrantCustomState>> entrantCustomStateListMap = Maps.newHashMap();
        //формируем мапу в которой ключ - абитуриент, а значение - список доп. статусов
        entrantCustomStateList.each { state ->
            EnrEntrant entrant = state.getEntrant();
            SafeMap.safeGet(entrantCustomStateListMap, entrant, ArrayList.class).add(state);
        }
        return entrantCustomStateListMap;
    }


}