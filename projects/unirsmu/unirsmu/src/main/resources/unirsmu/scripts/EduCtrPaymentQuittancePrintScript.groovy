package unirsmu.scripts

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.commonbase.catalog.entity.Currency
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import org.tandemframework.shared.person.base.entity.Person
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract

return new EduCtrPaymentQuittancePrint(
        session: session,                                         // сессия
        template: template,                                       // шаблон
        payPromise: session.get(CtrPaymentPromice.class, object)  // объект печати
).print()
/**
 * @author Denis Perminov
 * @since 22.07.2014
 */
class EduCtrPaymentQuittancePrint {
    Session session
    byte[] template
    CtrPaymentPromice payPromise
    def im = new RtfInjectModifier()

    def print() {
        CtrContractVersion version = payPromise.src.owner
        Person contrAgent = payPromise.src.contactor.person
        IdentityCard agentCard = contrAgent.identityCard

        EnrEntrantContract enrEntrantContract = DataAccessServices.dao().get(EnrEntrantContract.class, EnrEntrantContract.L_CONTRACT_OBJECT, version.contract)
        EnrEntrant entrant = enrEntrantContract.entrant
        IdentityCard entrantCard = entrant.person.identityCard

        TopOrgUnit topOrgUnit = TopOrgUnit.getInstance(true)

        im.put("paymentDstTitle", topOrgUnit.title)
        im.put("paymentStageTitle", payPromise.stage)

        im.put("studentSubdivision", version.contract.orgUnit.title)

        im.put("contrAgentFIO", agentCard.fullFio)

        im.put("contractNumber", entrant.personalNumber)

        im.put("studentSurname", entrantCard.lastName)
        im.put("studentName", entrantCard.firstName)
        im.put("studentPatronymic", entrantCard.middleName)

        im.put("chargeNRate", Long.toString(Currency.getAmount(payPromise.costAsLong)))
        im.put("chargeNRateKopeeks", Long.toString(Currency.getReminder(payPromise.costAsLong)))

        im.put("paymentDstInn", topOrgUnit.inn)
        im.put("paymentDstBank", topOrgUnit.bank)
        im.put("paymentDstAcc", topOrgUnit.curAccount)
        im.put("paymentDstBik", topOrgUnit.bic)

        // стандартные выходные параметры скрипта
        RtfDocument document = new RtfReader().read(template)

        im.modify(document)

        CommonBaseRenderer renderer = new CommonBaseRenderer()
                .rtf()
                .fileName("Квитанция ${version.getContract().getNumber()} от ${DateFormatter.DEFAULT_DATE_FORMATTER.format(payPromise.getDeadlineDate())}.rtf")
                .document(RtfUtil.toByteArray(document))
        return [renderer: renderer]
    }
}
