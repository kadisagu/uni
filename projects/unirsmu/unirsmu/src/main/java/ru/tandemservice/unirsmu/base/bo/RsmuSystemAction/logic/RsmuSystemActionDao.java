/* $Id$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.logic;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.hibernate.NonUniqueResultException;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unirsmu.entity.RsmuVectorIds;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 12.01.2015
 */
public class RsmuSystemActionDao extends UniBaseDao implements IRsmuSystemActionDao
{
    protected static Logger initFileLogger(final Class<?> klass, final String name, final Level threshold)
    {
        final Logger logger = Logger.getLogger(klass);
        try
        {
            final String path = ApplicationRuntime.getAppInstallPath();
            final FileAppender appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} [%t] %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + name + ".log"));
            appender.setName("file-" + name + "-appender");
            appender.setThreshold(threshold);
            logger.setLevel(threshold);
            logger.addAppender(appender);
            return logger;
        } catch (final Throwable t)
        {
            logger.error(t.getMessage(), t);
            return null;
        }
    }

    Logger logger = initFileLogger(this.getClass(), "ImportEnrPhoto", Level.INFO);

    @Override
    public void savePhotos(List<File> photoList)
    {

        int maxFileSize = 50 * 1024;
        String maxFileSizeStr = ApplicationRuntime.getRequiredProperty("person.identityCard.photo.maxFileSize");

        try
        {
            maxFileSize = Integer.valueOf(maxFileSizeStr);
        } catch (NumberFormatException ex)
        {
        }

        if (photoList.isEmpty())
        {
            throw new ApplicationException("В папке «${app.install.path}/data/photo» не содержится ни одного файла.");
        }
        logger.info("Import photo start.");
        for (File photo : photoList)
        {
            String fileTitle = "";
            String fileType = "";

            if (photo.length() > maxFileSize)
            {
                logger.error("Размер файла " + photo.getName() + " не должен превышать " + (maxFileSize / 1024) + "Kb.");
                continue;
            }

            //Проверяем корректность формата файла
            if (photo.getName().endsWith(".jpg"))
            {
                fileTitle = photo.getName().replace(".jpg", "");
                fileType = DatabaseFile.CONTENT_TYPE_IMAGE_JPG;
            } else if (photo.getName().endsWith(".jpeg"))
            {
                fileTitle = photo.getName().replace(".jpeg", "");
                fileType = DatabaseFile.CONTENT_TYPE_IMAGE_JPG;
            } else if (photo.getName().endsWith(".png"))
            {
                fileTitle = photo.getName().replace(".png", "");
                fileType = DatabaseFile.CONTENT_TYPE_IMAGE_PNG;
            } else if (photo.getName().endsWith(".gif"))
            {
                fileTitle = photo.getName().replace(".gif", "");
                fileType = DatabaseFile.CONTENT_TYPE_IMAGE_GIF;
            } else
            {
                logger.error("файл " + photo.getName() + " должен иметь один из следующих форматов: .jpg, .jpeg, .png, .gif");
                continue;
            }


            List<File> photoListForFindDoubles = new ArrayList<>(photoList);
            photoListForFindDoubles.remove(photo);

            if (findFilesWithEqualsName(photoListForFindDoubles, fileTitle))
            {
                logger.error("найдены файлы с одинаковыми названиями " + photo.getName());
                continue;
            }

            EnrEntrant enrEntrant;
            try
            {
                enrEntrant = new DQLSelectBuilder()
                        .fromEntity(EnrEntrant.class, "e").column("e")
                        .where(eqValue(property("e", EnrEntrant.personalNumber()), fileTitle))
                        .createStatement(getSession()).uniqueResult();
            } catch (NonUniqueResultException e)
            {
                logger.error("файл " + photo.getName() + " может быть сопоставлен с несколькими абитуриентами");
                continue;
            }

            if (null == enrEntrant)
            {
                logger.error("не удалось найти подходящего абитуриента для файла  " + photo.getName());
                continue;
            }

            DatabaseFile databaseFile = new DatabaseFile();
            databaseFile.setContent(getFileContent(photo));
            databaseFile.setFilename(fileTitle);
            databaseFile.setContentType(fileType);
            if (null == enrEntrant.getPerson().getIdentityCard().getPhoto() || null == enrEntrant.getPerson().getIdentityCard().getPhoto().getContent())
            {
                enrEntrant.getPerson().getIdentityCard().setPhoto(databaseFile);
                save(databaseFile);
                getSession().flush();
                update(enrEntrant.getPerson().getIdentityCard());
            }

        }
        logger.info("Import photos finished. Done.");
    }

    private byte[] getFileContent(File file)
    {
        byte[] content = null;
        try (InputStream is = new FileInputStream(file))
        {
            content = IOUtils.toByteArray(is);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return content;
    }

    private boolean findFilesWithEqualsName(List<File> photoList, String title)
    {
        boolean hasDoubles = false;

        for (File file : photoList)
        {
            String[] fileName = file.getName().split("\\.");
            if (fileName[0].equals(title))
            {
                hasDoubles = true;
                break;
            }
        }

        return hasDoubles;
    }

    @Override
    public void deleteAllStudents()
    {
        List<Long> studIdList = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property(Student.id().fromAlias("s")))
                .createStatement(getSession()).list();

        List<Long> personIdList = new DQLSelectBuilder().fromEntity(Person.class, "p").column(property(Student.id().fromAlias("p")))
                .joinEntity("p", DQLJoinType.left, PersonRole.class, "pr", eq(property(Person.id().fromAlias("p")), property(PersonRole.person().id().fromAlias("pr"))))
                .joinEntity("pr", DQLJoinType.left, Student.class, "s", eq(property(PersonRole.id().fromAlias("pr")), property(Student.id().fromAlias("s"))))
                .joinEntity("pr", DQLJoinType.left, EnrEntrant.class, "en", eq(property(PersonRole.id().fromAlias("pr")), property(EnrEntrant.id().fromAlias("en"))))
                .where(or(isNull(property(PersonRole.id().fromAlias("pr"))), and(isNotNull(property(Student.id().fromAlias("s"))), isNull(property(EnrEntrant.id().fromAlias("en"))))))
                .createStatement(getSession()).list();

        System.out.println("Person = " + personIdList.size() + ", Student = " + studIdList.size());


        DQLDeleteBuilder deleteOrderDataBuilder = new DQLDeleteBuilder(OrderData.class);
        deleteOrderDataBuilder.where(in(property(OrderData.student().id()), studIdList));
        deleteOrderDataBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteExtractBuilder = new DQLDeleteBuilder(OtherStudentExtract.class);
        deleteExtractBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteParagraphBuilder = new DQLDeleteBuilder(StudentOtherParagraph.class);
        deleteParagraphBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteOrderBuilder = new DQLDeleteBuilder(StudentOtherOrder.class);
        deleteOrderBuilder.createStatement(getSession()).execute();

        DQLUpdateBuilder updBuilder = new DQLUpdateBuilder(IdentityCard.class);
        updBuilder.where(in(property(IdentityCard.person().id()), personIdList));
        updBuilder.setValue(IdentityCard.person().s(), null);
        updBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteStudentBuilder = new DQLDeleteBuilder(Student.class);
        deleteStudentBuilder.where(in(property(Student.id()), studIdList));
        deleteStudentBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder deletePersondBuilder = new DQLDeleteBuilder(Person.class);
        deletePersondBuilder.where(in(property(Person.id()), personIdList));
        deletePersondBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteICardBuilder = new DQLDeleteBuilder(IdentityCard.class);
        deleteICardBuilder.where(isNull(property(IdentityCard.person())));
        deleteICardBuilder.createStatement(getSession()).execute();

        getSession().flush();

        List<Long> photoIdList = new DQLSelectBuilder().fromEntity(DatabaseFile.class, "f").column(property(DatabaseFile.id().fromAlias("f")))
                .joinEntity("f", DQLJoinType.inner, RsmuVectorIds.class, "vi", eq(property(DatabaseFile.id().fromAlias("f")), property(RsmuVectorIds.entityId().fromAlias("vi"))))
                .createStatement(getSession()).list();

        DQLDeleteBuilder deleteDatabaseFileBuilder = new DQLDeleteBuilder(DatabaseFile.class);
        deleteDatabaseFileBuilder.where(in(property(DatabaseFile.id()), photoIdList));
        deleteDatabaseFileBuilder.createStatement(getSession()).execute();

        DQLDeleteBuilder deleteVectorIdsBuilder = new DQLDeleteBuilder(RsmuVectorIds.class);
        deleteVectorIdsBuilder.createStatement(getSession()).execute();

        System.out.println("All the students were deleted successfully.");
    }

    @Override
    public void deleteOtherInvalidExtracts()
    {
        List<OtherStudentExtract> orphanExtracts = new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "e")
                .column(property("e")).where(isNull(property(OtherStudentExtract.paragraph().fromAlias("e"))))
                .createStatement(getSession()).list();

        for (OtherStudentExtract extract : orphanExtracts) delete(extract);

        List<AbstractStudentParagraph> orphanParagraphs = new DQLSelectBuilder().fromEntity(StudentOtherOrder.class, "e").column(property("a"))
                .joinEntity("e", DQLJoinType.left, AbstractStudentParagraph.class, "a", eq(property(StudentOtherOrder.id().fromAlias("e")), property(AbstractStudentParagraph.order().id().fromAlias("a"))))
                .joinEntity("a", DQLJoinType.left, AbstractStudentExtract.class, "ex", eq(property(StudentOtherParagraph.id().fromAlias("a")), property(AbstractStudentExtract.paragraph().id().fromAlias("ex"))))
                .where(isNull(property(OtherStudentExtract.id().fromAlias("ex")))).createStatement(getSession()).list();

        for (AbstractStudentParagraph paragraph : orphanParagraphs) delete(paragraph);

        List<StudentOtherOrder> orphanOrders = new DQLSelectBuilder().fromEntity(StudentOtherOrder.class, "e").column(property("e"))
                .joinEntity("e", DQLJoinType.left, AbstractStudentParagraph.class, "a", eq(property(StudentOtherOrder.id().fromAlias("e")), property(AbstractStudentParagraph.order().id().fromAlias("a"))))
                .joinEntity("a", DQLJoinType.left, AbstractStudentExtract.class, "ex", eq(property(StudentOtherParagraph.id().fromAlias("a")), property(AbstractStudentExtract.paragraph().id().fromAlias("ex"))))
                .where(isNull(property(OtherStudentExtract.id().fromAlias("ex")))).createStatement(getSession()).list();

        for (StudentOtherOrder order : orphanOrders) delete(order);

        List<NsiEntity> nsiEntityList = new DQLSelectBuilder().fromEntity(NsiEntity.class, "e").column(property("e"))
                .where(eq(property(NsiEntity.entityType().fromAlias("e")), value(AbstractStudentOrder.ENTITY_CLASS)))
                .createStatement(getSession()).list();

        for (NsiEntity entity : nsiEntityList) delete(entity);
    }

    @Override
    public int doOptimizeOtherOrders()
    {
        List<Object[]> ordersToOptimize = new DQLSelectBuilder().fromEntity(StudentOtherOrder.class, "o").column(property("o")).column(property("p")).column(property("e")).column(property("n"))
                .joinEntity("o", DQLJoinType.inner, AbstractStudentParagraph.class, "p", eq(property(StudentOtherOrder.id().fromAlias("o")), property(AbstractStudentParagraph.order().id().fromAlias("p"))))
                .joinEntity("p", DQLJoinType.inner, OtherStudentExtract.class, "e", eq(property(StudentOtherParagraph.id().fromAlias("p")), property(AbstractStudentExtract.paragraph().id().fromAlias("e"))))
                .joinEntity("o", DQLJoinType.left, NsiEntity.class, "n", eq(property(StudentOtherOrder.id().fromAlias("o")), property(NsiEntity.entityId().fromAlias("n"))))
                .createStatement(getSession()).list();

        // ключ = ИД студента + код типа приказа + Номер приказа + дата приказа
        Map<String, List<Object[]>> ordersMap = new HashMap<>(); // все приказы, кроме пришедших из НСИ
        Map<String, List<Object[]>> nsiOrdersMap = new HashMap<>(); // приказы из НСИ
        List<NsiEntity> nsiEntityToDeleteList = new ArrayList<>(); // Идентификаторы НСИ, которые надо удалить

        for (Object[] item : ordersToOptimize)
        {
            StudentOtherOrder order = (StudentOtherOrder) item[0];
            OtherStudentExtract extract = (OtherStudentExtract) item[2];
            NsiEntity nsiEntity = (NsiEntity) item[3];

            StringBuilder keyBuilder = new StringBuilder(String.valueOf(extract.getEntity().getId()));
            keyBuilder.append(extract.getType().getCode());
            if (null != order.getNumber()) keyBuilder.append(order.getNumber());
            if (null != order.getCommitDate())
                keyBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
            String key = keyBuilder.toString().toLowerCase();

            if (null != nsiEntity)
            {
                List<Object[]> orders = nsiOrdersMap.get(key);
                if (null == orders) orders = new ArrayList<>();
                else nsiEntityToDeleteList.add(nsiEntity);
                orders.add(item);
                nsiOrdersMap.put(key, orders);
            } else
            {
                List<Object[]> orders = ordersMap.get(key);
                if (null == orders) orders = new ArrayList<>();
                orders.add(item);
                ordersMap.put(key, orders);
            }
        }

        List<Object[]> itemsToDeleteList = new ArrayList<>();
        for (Map.Entry<String, List<Object[]>> entry : nsiOrdersMap.entrySet())
        {
            if (entry.getValue().size() > 0)
            {
                List<Object[]> orders = ordersMap.get(entry.getKey());
                if (null != orders)
                {
                    itemsToDeleteList.addAll(orders);
                    ordersMap.remove(orders);
                }
                if (entry.getValue().size() > 1)
                {
                    entry.getValue().remove(0);
                    itemsToDeleteList.addAll(entry.getValue());
                }
            }
        }

        for (Map.Entry<String, List<Object[]>> entry : ordersMap.entrySet())
        {
            if (entry.getValue().size() > 0)
            {
                entry.getValue().remove(0);
                itemsToDeleteList.addAll(entry.getValue());
            }
        }

        int i = 0;
        for (Object[] item : itemsToDeleteList)
        {
            delete((OtherStudentExtract) item[2]);
            delete((AbstractStudentParagraph) item[1]);
            delete((StudentOtherOrder) item[0]);
            if (i++ > 100) break;
        }

        for (NsiEntity entity : nsiEntityToDeleteList) delete(entity);
        return itemsToDeleteList.size() - i;
    }

    @Override
    public int changeFakeStudentsStatus()
    {
        List<Object[]> items = new DQLSelectBuilder().fromEntity(Student.class, "s").column(property("s")).column(property("v")).column(property("n"))
                .joinEntity("s", DQLJoinType.left, RsmuVectorIds.class, "v", eq(property(Student.id().fromAlias("s")), property(RsmuVectorIds.entityId().fromAlias("v"))))
                .joinEntity("s", DQLJoinType.left, NsiEntity.class, "n", eq(property(Student.id().fromAlias("s")), property(NsiEntity.entityId().fromAlias("n"))))
                .createStatement(getSession()).list();

        Map<String, List<Object[]>> studentsMap = new HashMap<>();
        Map<String, List<Object[]>> vectorMap = new HashMap<>();
        Map<String, List<Object[]>> nsiStudentsMap = new HashMap<>();
        for (Object[] item : items)
        {
            Student student = (Student) item[0];
            RsmuVectorIds vectorId = (RsmuVectorIds) item[1];
            NsiEntity nsiEntity = (NsiEntity) item[2];
            String key = student.getPerson().getIdentityCard().getFullFio().toLowerCase();

            if (null != nsiEntity)
            {
                List<Object[]> students = nsiStudentsMap.get(key);
                if (null == students) students = new ArrayList<>();
                students.add(item);
                nsiStudentsMap.put(key, students);
            } else if (null != vectorId)
            {
                List<Object[]> students = vectorMap.get(key);
                if (null == students) students = new ArrayList<>();
                students.add(item);
                vectorMap.put(key, students);
            } else
            {
                List<Object[]> students = studentsMap.get(key);
                if (null == students) students = new ArrayList<>();
                students.add(item);
                studentsMap.put(key, students);
            }
        }

        List<Object[]> itemsToUpdate = new ArrayList<>();
        Set<Student> occupiedStudents = new HashSet<>();

        for (Map.Entry<String, List<Object[]>> entry : nsiStudentsMap.entrySet())
        {
            List<Object[]> students = entry.getValue();
            if (!students.isEmpty())
            {
                List<Object[]> vectorStudents = vectorMap.get(entry.getKey());
                if (students.size() > 1)
                {
                    EducationOrgUnit eou = null;
                    List<Student> resultList = new ArrayList<>();
                    for (Object[] item : students)
                    {
                        Student s = (Student) item[0];
                        if (s.getStatus().isActive())
                        {
                            if (null == eou)
                            {
                                resultList.add(s);
                                occupiedStudents.add(s);
                                eou = s.getEducationOrgUnit();
                            } else
                            {
                                if (!eou.equals(s.getEducationOrgUnit()))
                                {
                                    resultList.add(s);
                                    occupiedStudents.add(s);
                                } else itemsToUpdate.add(item);
                            }
                        }
                    }

                    if (null != vectorStudents)
                    {
                        for (Object[] vecItem : vectorStudents)
                        {
                            Student vecStudent = (Student) vecItem[0];
                            resultList.remove(vecStudent);
                            if (vecStudent.getStatus().isActive() && !occupiedStudents.contains(vecStudent))
                                itemsToUpdate.add(vecItem);
                        }
                    }
                    /*if (resultList.size() > 1)
                    {
                        System.out.println("------ There are more than on student '" + entry.getKey() + "':");
                        for (Student s : resultList) System.out.println(s.getFullTitle());
                    }*/
                }
            }
        }

        for (Map.Entry<String, List<Object[]>> entry : vectorMap.entrySet())
        {
            List<Object[]> students = entry.getValue();
            for (Object[] item : students)
            {
                Student s = (Student) item[0];
                if (s.getStatus().isActive() && !occupiedStudents.contains(s))
                    itemsToUpdate.add(item);
            }
        }

        for (Map.Entry<String, List<Object[]>> entry : studentsMap.entrySet())
        {
            List<Object[]> students = entry.getValue();
            for (Object[] item : students)
            {
                Student s = (Student) item[0];
                if (s.getStatus().isActive() && !occupiedStudents.contains(s))
                    itemsToUpdate.add(item);
            }
        }

        int i = 0;
        Set<Student> processedStudents = new HashSet<>();
        for(Object[] item : itemsToUpdate)
        {
            Student s = (Student) item[0];
            processedStudents.add(s);
            if(s.getStatus().isActive())
            {
                s.setStatus(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
                update(s);
            }
        }

        return itemsToUpdate.size() - i;
    }
}