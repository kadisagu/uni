/* $Id$ */
package ru.tandemservice.unirsmu.dao.daemon.wrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorDataHolder;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorSyncDaemonDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 24.11.2015
 */
public class PersonWrapper
{
    private String _vectorId;
    private String _lastName;
    private String _firstName;
    private String _middleName;
    private Date _birthDate;
    private String _birthPlace;
    private String _sex;

    private String _icTypeId;
    private String _icTypeStr;
    private String _icSeries;
    private String _icNumber;
    private Date _icIssuanceDate;
    private String _icIssuancePlace;
    private String _icIssuancePlaceCode;

    private String _citizenship;

    public PersonWrapper()
    {
    }

    public PersonWrapper(ResultSet resultSet) throws SQLException
    {
        _vectorId = StringUtils.trimToNull(resultSet.getString(1));
        _lastName = StringUtils.trimToNull(resultSet.getString(2));
        _firstName = StringUtils.trimToNull(resultSet.getString(3));
        _middleName = StringUtils.trimToNull(resultSet.getString(4));
        _birthDate = null != resultSet.getDate(5) ? new Date(resultSet.getDate(5).getTime()) : null;
        _birthPlace = StringUtils.trimToNull(resultSet.getString(6));
        _sex = StringUtils.trimToNull(resultSet.getString(7));

        _icTypeId = StringUtils.trimToNull(resultSet.getString(9));
        _icTypeStr = StringUtils.trimToNull(resultSet.getString(10));
        _icSeries = StringUtils.trimToNull(resultSet.getString(11));
        _icNumber = StringUtils.trimToNull(resultSet.getString(12));
        _icIssuanceDate = null != resultSet.getDate(13) ? new Date(resultSet.getDate(13).getTime()) : null;
        _icIssuancePlace = StringUtils.trimToNull(resultSet.getString(14));
        _icIssuancePlaceCode = StringUtils.trimToNull(resultSet.getString(15));
    }

    public String getKey()
    {
        StringBuilder builder = new StringBuilder();
        if (null != _lastName) builder.append(_lastName);
        if (null != _firstName) builder.append(_firstName);
        if (null != _middleName) builder.append(_middleName);
        if (null != _birthDate) builder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_birthDate));
        return builder.toString().toLowerCase();
    }

    public String getFioWithBirthDate()
    {
        StringBuilder builder = new StringBuilder();
        if (null != _lastName) builder.append(_lastName);
        if (null != _firstName) builder.append(builder.length() > 0 ? " " : "").append(_firstName);
        if (null != _middleName) builder.append(builder.length() > 0 ? " " : "").append(_middleName);
        if (null != _birthDate) builder.append(builder.length() > 0 ? " " : "").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_birthDate));
        return builder.toString();
    }

    public Person generatePerson()
    {
        IdentityCard identityCard = new IdentityCard();
        identityCard.setLastName(null != _lastName ? _lastName : "-");
        identityCard.setFirstName(null != _firstName ? _firstName : "-");
        identityCard.setMiddleName(_middleName);
        identityCard.setBirthDate(_birthDate);
        identityCard.setBirthPlace(_birthPlace);
        identityCard.setSex(RsmuVectorDataHolder.getSex(_sex));
        identityCard.setCardType(RsmuVectorDataHolder.getIdentityCardType(_icTypeId));
        identityCard.setSeria(_icSeries);
        identityCard.setNumber(_icNumber);
        identityCard.setIssuanceDate(_icIssuanceDate);
        identityCard.setIssuancePlace(_icIssuancePlace);
        identityCard.setIssuanceCode(_icIssuancePlaceCode);
        identityCard.setCreationDate(new Date());

        if(null == _citizenship)
            identityCard.setCitizenship(RsmuVectorDataHolder.getRussianCitizenship());
        else
            identityCard.setCitizenship(RsmuVectorDataHolder.getCitizenship(_citizenship));

        if(null == identityCard.getCitizenship()) identityCard.setCitizenship(RsmuVectorDataHolder.getRussianCitizenship());

        if (null == _lastName || null == _firstName)
        {
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Person was automatically corrected" + this.toString());

            if (null == identityCard.getCardType())
                RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Person identity card type was changed to 'without identity card', because it was not filled in Vector or has unknown type.");

            if (null == _lastName || null == _firstName)
                RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Person last and/or first name was changed to '-', because it was not filled in Vector.");
        }

        if (null == identityCard.getCardType())
            identityCard.setCardType(RsmuVectorDataHolder.getIdentityCardType(null));

        Person person = new Person();
        person.setIdentityCard(identityCard);

        return person;
    }

    public void updatePerson(Person person)
    {
        if (null == person) return;

        IdentityCard identityCard = person.getIdentityCard();
        identityCard.setLastName(null != _lastName ? _lastName : "-");
        identityCard.setFirstName(null != _firstName ? _firstName : "-");
        identityCard.setMiddleName(_middleName);
        identityCard.setBirthDate(_birthDate);
        identityCard.setBirthPlace(_birthPlace);
        identityCard.setSex(RsmuVectorDataHolder.getSex(_sex));
        identityCard.setCardType(RsmuVectorDataHolder.getIdentityCardType(_icTypeId));
        identityCard.setSeria(_icSeries);
        identityCard.setNumber(_icNumber);
        identityCard.setIssuanceDate(_icIssuanceDate);
        identityCard.setIssuancePlace(_icIssuancePlace);
        identityCard.setIssuanceCode(_icIssuancePlaceCode);
        person.setIdentityCard(identityCard);

        if (null == _lastName || null == _firstName)
        {
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Person was automatically corrected" + this.toString());

            if (null == identityCard.getCardType())
                RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Person identity card type was changed to 'without identity card', because it was not filled in Vector or has unknown type.");

            if (null == _lastName || null == _firstName)
                RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Person last and/or first name was changed to '-', because it was not filled in Vector.");
        }

        if (null == identityCard.getCardType())
            identityCard.setCardType(RsmuVectorDataHolder.getIdentityCardType(null));
    }

    public static PersonWrapper getTheBest(List<PersonWrapper> wrappers)
    {
        if (null == wrappers || wrappers.isEmpty()) return null;

        Map<Integer, Integer> fullnessMap = new HashMap<>();
        Map<Integer, PersonWrapper> wrapperMap = new HashMap<>();

        int bestFullness = 0;
        PersonWrapper best = wrappers.get(0);
        for (PersonWrapper wrapper : wrappers)
        {
            fullnessMap.put(wrappers.indexOf(wrapper), wrapper.getFullnessCoefficient());
            wrapperMap.put(wrappers.indexOf(wrapper), wrapper);

            if (wrapper.getFullnessCoefficient() > bestFullness)
            {
                bestFullness = wrapper.getFullnessCoefficient();
                best = wrapper;
            }
        }

        Set<Integer> identicalFullness = new HashSet<>();
        for (Map.Entry<Integer, Integer> entry : fullnessMap.entrySet())
        {
            if (entry.getValue() == bestFullness) identicalFullness.add(entry.getKey());
        }

        if (identicalFullness.size() > 1)
        {
            List<PersonWrapper> identicalWrappers = new ArrayList<>();
            for (Integer idx : identicalFullness) identicalWrappers.add(wrappers.get(idx));

            long lastICardDate = 0;
            for (PersonWrapper wrapper : identicalWrappers)
            {
                if (null != wrapper.getIcIssuanceDate() && wrapper.getIcIssuanceDate().getTime() > lastICardDate && "1".equals(wrapper.getIcTypeId()))
                {
                    lastICardDate = wrapper.getIcIssuanceDate().getTime();
                    best = wrapper;
                }
            }
        }

        return best;
    }

    public int getFullnessCoefficient()
    {
        int coeff = 0;
        if (null != _lastName) coeff++;
        if (null != _firstName) coeff++;
        if (null != _middleName) coeff++;
        if (null != _birthDate) coeff++;
        if (null != _birthPlace) coeff++;
        if (null != _sex) coeff++;
        if (null != _icTypeId) coeff++;
        if (null != _icSeries) coeff++;
        if (null != _icNumber) coeff++;
        if (null != _icIssuanceDate) coeff++;
        if (null != _icIssuancePlace) coeff++;
        if (null != _icIssuancePlaceCode) coeff++;
        return coeff;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder(_vectorId).append("\t");
        if (null != _lastName) builder.append(_lastName);
        if (null != _firstName) builder.append(" ").append(_firstName);
        if (null != _middleName) builder.append(" ").append(_middleName);
        if (null != _birthDate) builder.append("\t").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_birthDate));
        else builder.append("\t--.--.----");
        if (null != _birthPlace) builder.append("\t").append(_birthPlace);
        else builder.append("\t--- место рождения не указано ---");
        if (null != _sex) builder.append("\t").append(_sex);
        else builder.append("\t-");

        builder.append(" | ДУЛ: ").append(_icTypeId).append(".").append(_icTypeStr).append("\t").append(": ");
        if (null != _icSeries) builder.append(_icSeries);
        if (null != _icNumber) builder.append(" № ").append(_icNumber);
        else builder.append(" № ------");
        if (null != _icIssuanceDate) builder.append("\t").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_icIssuanceDate));
        else builder.append("\t--.--.----");
        if (null != _icIssuancePlace) builder.append("\t").append(_icIssuancePlace);
        else builder.append("\t--- место выдачи ДУЛ не указано ---");
        if (null != _icIssuancePlaceCode) builder.append(" (").append(_icIssuancePlaceCode).append(")");

        return builder.toString();
    }

    public String getVectorId()
    {
        return _vectorId;
    }

    public void setVectorId(String vectorId)
    {
        _vectorId = vectorId;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public void setLastName(String lastName)
    {
        _lastName = lastName;
    }

    public String getFirstName()
    {
        return _firstName;
    }

    public void setFirstName(String firstName)
    {
        _firstName = firstName;
    }

    public String getMiddleName()
    {
        return _middleName;
    }

    public void setMiddleName(String middleName)
    {
        _middleName = middleName;
    }

    public Date getBirthDate()
    {
        return _birthDate;
    }

    public void setBirthDate(Date birthDate)
    {
        _birthDate = birthDate;
    }

    public String getBirthPlace()
    {
        return _birthPlace;
    }

    public void setBirthPlace(String birthPlace)
    {
        _birthPlace = birthPlace;
    }

    public String getSex()
    {
        return _sex;
    }

    public void setSex(String sex)
    {
        _sex = sex;
    }

    public String getIcTypeId()
    {
        return _icTypeId;
    }

    public void setIcTypeId(String icTypeId)
    {
        _icTypeId = icTypeId;
    }

    public String getIcTypeStr()
    {
        return _icTypeStr;
    }

    public void setIcTypeStr(String icTypeStr)
    {
        _icTypeStr = icTypeStr;
    }

    public String getIcSeries()
    {
        return _icSeries;
    }

    public void setIcSeries(String icSeries)
    {
        _icSeries = icSeries;
    }

    public String getIcNumber()
    {
        return _icNumber;
    }

    public void setIcNumber(String icNumber)
    {
        _icNumber = icNumber;
    }

    public Date getIcIssuanceDate()
    {
        return _icIssuanceDate;
    }

    public void setIcIssuanceDate(Date icIssuanceDate)
    {
        _icIssuanceDate = icIssuanceDate;
    }

    public String getIcIssuancePlace()
    {
        return _icIssuancePlace;
    }

    public void setIcIssuancePlace(String icIssuancePlace)
    {
        _icIssuancePlace = icIssuancePlace;
    }

    public String getIcIssuancePlaceCode()
    {
        return _icIssuancePlaceCode;
    }

    public void setIcIssuancePlaceCode(String icIssuancePlaceCode)
    {
        _icIssuancePlaceCode = icIssuancePlaceCode;
    }

    public String getCitizenship()
    {
        return _citizenship;
    }

    public void setCitizenship(String citizenship)
    {
        _citizenship = citizenship;
    }
}