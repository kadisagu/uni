/* $Id$ */
package ru.tandemservice.unirsmu.order.ext.EnrOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unirsmu.order.ext.EnrOrder.logic.IRsmuEnrOrderDao;
import ru.tandemservice.unirsmu.order.ext.EnrOrder.logic.RsmuEnrOrderDao;

/**
 * @author Nikolay Fedorovskih
 * @since 30.07.2014
 */
@Configuration
public class EnrOrderExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IRsmuEnrOrderDao dao()
    {
        return new RsmuEnrOrderDao();
    }
}