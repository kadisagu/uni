/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.ContractTransferAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

/**
 * @author Andrey Avetisov
 * @since 29.01.2015
 */
@Configuration
public class RsmuEnrReportContractTransferAdd extends BusinessComponentManager
{
    public static final String ENR_ORDER_DS = "enrOrderDS";
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ENR_ORDER_DS, enrOrderDSHandler()))
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrOrderDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrder.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                EnrEnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN);
                dql.where(DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.state().code()), OrderStatesCodes.FINISHED));
                dql.where(DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.L_ENROLLMENT_CAMPAIGN), enrollmentCampaign));
                dql.where(DQLExpressions.or(
                                  DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.type().code()), EnrOrderTypeCodes.ENROLLMENT),
                                  DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.type().code()), EnrOrderTypeCodes.ENROLLMENT_MIN))
                );
                dql.where(DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.compensationType().code()), CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));
            }
        }
                .order(EnrOrder.number())
                .filter(EnrOrder.number())
                .filter(EnrOrder.commitDate());
    }
}
