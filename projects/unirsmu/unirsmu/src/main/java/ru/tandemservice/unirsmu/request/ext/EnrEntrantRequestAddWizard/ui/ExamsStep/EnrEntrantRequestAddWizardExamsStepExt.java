/* $Id$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequestAddWizard.ui.ExamsStep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequestAddWizard.ui.ExamsStep.EnrEntrantRequestAddWizardExamsStep;
import ru.tandemservice.unirsmu.entrant.ext.EnrEntrant.ui.EditChosenExams.EnrEntrantEditChosenExamsExtUI;

/**
 * @author Andrey Avetisov
 * @since 09.06.2015
 */

@Configuration
public class EnrEntrantRequestAddWizardExamsStepExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unirsmu" + EnrEntrantEditChosenExamsExtUI.class.getSimpleName();

    @Autowired
    private EnrEntrantRequestAddWizardExamsStep _entrantRequestAddWizardExamsStep;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_entrantRequestAddWizardExamsStep.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantEditChosenExamsExtUI.class))
                .create();
    }
}
