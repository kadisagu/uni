/* $Id:$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.VectorSync;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 23.11.2015
 */
@Configuration
public class RsmuSystemActionVectorSync extends BusinessComponentManager
{
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().create();
    }
}