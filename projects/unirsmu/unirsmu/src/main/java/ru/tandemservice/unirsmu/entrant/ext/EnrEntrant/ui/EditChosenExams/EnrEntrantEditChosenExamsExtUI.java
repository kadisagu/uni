/* $Id$ */
package ru.tandemservice.unirsmu.entrant.ext.EnrEntrant.ui.EditChosenExams;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import org.tandemframework.shared.person.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditChosenExams.EnrEntrantEditChosenExamsUI;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 31.03.2015
 */
public class EnrEntrantEditChosenExamsExtUI extends UIAddon
{
    private EnrEntrantEditChosenExamsUI _presenter;
    private Map<EnrExamPassForm, Boolean> _selectedPassFormMap = new HashMap<>();


    public EnrEntrantEditChosenExamsExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        _presenter = getPresenter();
        for (EnrExamPassForm passForm : _presenter.getPassFormList())
        {
            _selectedPassFormMap.put(passForm, false);
        }

        // при наличии для ВИ только одной доступной формы сдачи выставлять чекбокс выбора формы сдачи автоматически:
        for (EnrEntrantEditChosenExamsUI.CompetitionRowWrapper rowWrapper : _presenter.getRowList())
        {
            for (EnrEntrantEditChosenExamsUI.DiscWrapper discWrapper : rowWrapper.getDiscRowList())
            {
                Set<EnrExamPassForm> passFormSet = new HashSet<>();
                for (EnrExamPassForm passForm : discWrapper.getAllowedPassForms())
                {
                    if (!isPassFormSelectionDisabled(discWrapper, rowWrapper, passForm) && !EnrExamPassFormCodes.OLYMPIAD.equals(passForm.getCode()))
                    {
                        passFormSet.add(passForm);
                    }

                }
                if (passFormSet.size() == 1)
                {
                    discWrapper.getSelectedPassForms().addAll(passFormSet);
                }
            }
        }
    }

    public int getHeaderRowspan()
    {
        return _presenter.isAllowSelectDiploma() ? 4 : 3;
    }


    public boolean isSelectColumn()
    {
        return _selectedPassFormMap.get(getCurrentPassForm());
    }

    public void setSelectColumn(boolean selectColumn)
    {
        EnrExamPassForm passForm = _presenter.getCurrentPassForm();
        _selectedPassFormMap.put(passForm, selectColumn);
    }

    public void onClickSelectColumn()
    {
        EnrExamPassForm passForm = getListenerParameter();
        for (EnrEntrantEditChosenExamsUI.CompetitionRowWrapper rowWrapper : _presenter.getRowList())
        {
            for (EnrEntrantEditChosenExamsUI.DiscWrapper discWrapper : rowWrapper.getDiscRowList())
            {
                if (!isPassFormSelectionDisabled(discWrapper, rowWrapper, passForm))
                {
                    if (_selectedPassFormMap.get(passForm))
                        discWrapper.getSelectedPassForms().add(passForm);
                    else
                        discWrapper.getSelectedPassForms().remove(passForm);
                }
            }
        }
    }

    public void onClickCurrentPassFormSelect()
    {
        EnrExamPassForm passForm = getListenerParameter();
        _selectedPassFormMap.put(passForm, false);
    }

    public void onChangeInternalExamReason()
    {
        _presenter.onChangeInternalExamReason();
        for (EnrExamPassForm passForm : _selectedPassFormMap.keySet())
        {
            _selectedPassFormMap.put(passForm, false);
        }
    }

    public boolean isColumnCheckBoxDisabled()
    {
        for (EnrEntrantEditChosenExamsUI.CompetitionRowWrapper rowWrapper : _presenter.getRowList())
        {
            for (EnrEntrantEditChosenExamsUI.DiscWrapper discWrapper : rowWrapper.getDiscRowList())
            {
                if (!isPassFormSelectionDisabled(discWrapper, rowWrapper, getCurrentPassForm()))
                {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isPassFormSelectionDisabled(EnrEntrantEditChosenExamsUI.DiscWrapper currentDiscRow,
                                               EnrEntrantEditChosenExamsUI.CompetitionRowWrapper currentRow,
                                               EnrExamPassForm currentPassForm)
    {
        return isPassFormSelectionDisabledByDiploma(currentDiscRow) || isPassFormSelectionDisabledByInternalExamReason(currentDiscRow, currentRow, currentPassForm);
    }

    private boolean isPassFormSelectionDisabledByInternalExamReason(EnrEntrantEditChosenExamsUI.DiscWrapper currentDiscRow,
                                                                    EnrEntrantEditChosenExamsUI.CompetitionRowWrapper currentRow,
                                                                    EnrExamPassForm currentPassForm)
    {
        if (currentRow == null) return false;
        if (currentPassForm == null || !currentPassForm.isInternal()) return false;
        if (_presenter.getInternalExamReason() != null || !isNeedExternalExamReason(currentRow.getCompetition())) return false;

        boolean hasStateExam = false;
        for (EnrExamPassForm passForm : new ArrayList<>(currentDiscRow.getAllowedPassForms()))
            hasStateExam |= passForm.getCode().equals(EnrExamPassFormCodes.STATE_EXAM);
        return hasStateExam;
    }

    private boolean isPassFormSelectionDisabledByDiploma(EnrEntrantEditChosenExamsUI.DiscWrapper currentDiscRow)
    {
        return null != currentDiscRow && (null != currentDiscRow.getDiplomaCategory() || null != currentDiscRow.getDiploma());
    }

    private boolean isNeedExternalExamReason(EnrRequestedCompetition requestedCompetition)
    {
        if (!requestedCompetition.getRequest().getType().getCode().equals(EnrRequestTypeCodes.BS)) return false;
        if (EnrEduLevelRequirementCodes.PO.equals(requestedCompetition.getCompetition().getEduLevelRequirement().getCode())) return false;
        EduLevel eduLevel = requestedCompetition.getRequest().getEduDocument().getEduLevel();
        return eduLevel != null && eduLevel.getCode().equals(EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE);
    }

    public EnrExamPassForm getCurrentPassForm()
    {
        return _presenter.getCurrentPassForm();
    }


}
