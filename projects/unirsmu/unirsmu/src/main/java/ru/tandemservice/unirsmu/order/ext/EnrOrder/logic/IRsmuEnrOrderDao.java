/* $Id$ */
package ru.tandemservice.unirsmu.order.ext.EnrOrder.logic;

import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.IEnrOrderDao;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2014
 */
public interface IRsmuEnrOrderDao extends IEnrOrderDao
{
    byte[] printOrderExtracts(Long enrOrderId);
}