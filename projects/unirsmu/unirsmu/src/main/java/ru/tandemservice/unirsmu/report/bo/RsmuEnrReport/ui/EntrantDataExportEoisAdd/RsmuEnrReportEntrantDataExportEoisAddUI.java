/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantDataExportEoisAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.RsmuEnrReportManager;

import java.util.Date;
import java.util.List;


/**
 * @author Andrey Avetisov
 * @since 05.02.2015
 */
public class RsmuEnrReportEntrantDataExportEoisAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private Date _dateFrom;
    private boolean _dateFromActive;
    private boolean _dateToActive;
    private Date _dateTo;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        // TODO: DEV-9203
//        if(getEnrollmentCampaign()== null && getEnrollmentCampaignList()!=null && !getEnrollmentCampaignList().isEmpty())
//        {
//            setEnrollmentCampaign(getEnrollmentCampaignList().get(0));
//            onChangeEnrollmentCampaign();
//        }
        setDateFromActive(false);
        setDateFrom(null);
        setDateToActive(false);
        setDateTo(null);
    }

    //listeners
    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onClickApply()
    {
        onChangeEnrollmentCampaign();
        ErrorCollector errors = ContextLocal.getErrorCollector();
        checkDates(errors);
        if (errors.hasErrors()) return;
        RsmuEnrReportManager.instance().dao().createEntrantDataExportEoisReport(this);
    }

    //validators
    private void checkDates(ErrorCollector errors) throws ApplicationException
    {
        if (isDateFromActive() && isDateToActive() && getDateFrom().after(getDateTo()))
        {
            errors.add("Дата начала проведения ВИ должна быть не позже даты окончания.", "dateFrom", "dateTo");
        }
    }


    //getters & setters
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public boolean isDateFromActive()
    {
        return _dateFromActive;
    }

    public void setDateFromActive(boolean dateFromActive)
    {
        _dateFromActive = dateFromActive;
    }

    public boolean isDateToActive()
    {
        return _dateToActive;
    }

    public void setDateToActive(boolean dateToActive)
    {
        _dateToActive = dateToActive;
    }
}
