package ru.tandemservice.unirsmu.migration;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unirsmu_2x10x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {

    }
}
