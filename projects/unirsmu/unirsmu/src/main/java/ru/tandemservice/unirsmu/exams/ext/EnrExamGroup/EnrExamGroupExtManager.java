/* $Id$ */
package ru.tandemservice.unirsmu.exams.ext.EnrExamGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.IEnrExamGroupEnrollPassSheetPrintDao;
import ru.tandemservice.unirsmu.exams.ext.EnrExamGroup.logic.EnrExamGroupEnrollPassSheetPrintDao;

/**
 * @author Denis Katkov
 * @since 25.05.2016
 */
@Configuration
public class EnrExamGroupExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public IEnrExamGroupEnrollPassSheetPrintDao enrPassSheetPrintDao()
    {
        return new EnrExamGroupEnrollPassSheetPrintDao();
    }
}
