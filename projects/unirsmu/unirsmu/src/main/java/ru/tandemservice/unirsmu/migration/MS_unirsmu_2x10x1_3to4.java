package ru.tandemservice.unirsmu.migration;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unirsmu_2x10x1_3to4 extends IndependentMigrationScript
{
    // CtrPrintTemplateCodes
    /** Константа кода (code) элемента : С иностранцем (title) */
    private String RSMU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_FOREIGN = "rsmu.edu.ctr.template.vo.2s.foreign";
    /** Константа кода (code) элемента : С иностранцем (title) */
    private String RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_FOREIGN = "rsmu.edu.ctr.template.vo.3sp.foreign";
    /** Константа кода (code) элемента : С иностранцем (title) */
    private String RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_FOREIGN = "rsmu.edu.ctr.template.vo.3so.foreign";

    // CtrContractKindCodes
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
    private String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";


    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        // Получаем уже существующие элементы ctrPrintTemplate
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        SQLSelectQuery selectCtrPrintTemplateQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_prnt_tmpl_t", "pt").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=pt.id"))
                .column("si.id")
                .column("si.code_p");

        List<Object[]> ctrPrintTemplates = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectCtrPrintTemplateQuery));

        for(Object[] obj : ctrPrintTemplates)
        {
            ctrPrintTemplateMap.put((String) obj[1], (Long) obj[0]);
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractVersion

        // заполняем printTemplate и kind

        SQLSelectQuery selectContractVersionQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id");

        List<Object[]> contractVersionList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        for (Object[] contractVersion : contractVersionList)
        {
            fixCtrPrintTemplateAndKind((Long) contractVersion[0], ctrPrintTemplateMap, contractKindMap, tool);
        }

    }

    private void fixCtrPrintTemplateAndKind(Long contractVersionId, Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractver_t")
                .set("kind_id", "?")
                .set("printtemplate_id", "?")
                .where("id=?");

        List<Long> voForeignPrintTemplatesIds = Lists.newArrayList(
                ctrPrintTemplateMap.get(RSMU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_FOREIGN),
                ctrPrintTemplateMap.get(RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_FOREIGN),
                ctrPrintTemplateMap.get(RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_FOREIGN)
        );

        if((CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stduent_c_ctmpldt_t", tool) || CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "enr14_ctmpldt_simple_t", tool))  && checkPrintTemplate(contractVersionId, voForeignPrintTemplatesIds, tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_VO_2_SIDES,
                            EDU_CONTRACT_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_VO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            RSMU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_FOREIGN,
                            RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_FOREIGN,
                            RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_FOREIGN,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
        }
    }

    private Long getKind(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        Long ctrKind;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrKind = contractKindMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrKind = contractKindMap.get(threeSidePerson);
            }
            else
            {
                ctrKind = contractKindMap.get(threeSideOrg);
            }
        }

        return ctrKind;
    }


    private Long getPrintTemplate(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> ctrPrintTemplateMap, DBTool tool) throws SQLException
    {
        Long ctrPrintTemplate;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrPrintTemplate = ctrPrintTemplateMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSidePerson);
            }
            else
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSideOrg);
            }
        }

        return ctrPrintTemplate;
    }

    private boolean checkTwoSides(Long contractVersionId, DBTool tool) throws SQLException
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=c.contactor_id")
        )
                .column("pr.person_id")
                .where("c.owner_id=?")
                .where("r.code_p=?");

        // CtrContractRole
        /** Константа кода (code) элемента : Заказчик (title) */
        String EDU_CONTRACT_CUSTOMER = "01.01.customer";
        /** Константа кода (code) элемента : Исполнитель (title) */
        String EDU_CONTRACT_PROVIDER = "01.01.provider";

        Long providerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_PROVIDER);
        Long customerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_CUSTOMER);

        SQLSelectQuery studentPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv").innerJoin(SQLFrom.table("eductr_student_contract_t", "sc"), "sc.contractobject_id=cv.contract_id") .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=sc.student_id")).column("pr.person_id")
                .where("cv.id=?");

        Long studentPerson = (Long) tool.getUniqueResult(translator.toSql(studentPersonQuery), contractVersionId);

        SQLSelectQuery entrantPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")
                .innerJoin(SQLFrom.table("enr14_entrant_contract_t", "ec"), "ec.contractobject_id=cv.contract_id")
                .innerJoin(SQLFrom.table("enr14_requested_comp_t", "rc"), "rc.id=ec.requestedcompetition_id")
                .innerJoin(SQLFrom.table("enr14_request_t", "r"), "r.id=rc.request_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=r.entrant_id")).column("pr.person_id")
                .where("cv.id=?");

        Long entrantPerson = (Long) tool.getUniqueResult(translator.toSql(entrantPersonQuery), contractVersionId);

        if(entrantPerson == null && studentPerson == null)
        {
            return true;
        }

        if(entrantPerson != null) {
            return Objects.equals(entrantPerson, providerPerson) || Objects.equals(entrantPerson, customerPerson);
        }

        return Objects.equals(studentPerson, providerPerson) || Objects.equals(studentPerson, customerPerson);
    }


    private boolean checkPersonSide(Long contractVersionId, DBTool tool) throws SQLException
    {
        // Проверяем по наличию юр. лица

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("juridicalcontactor_t", "jc"), "jc.id=c.contactor_id")
        )
                .where("c.owner_id=?")
                .column("jc.id");

        List<Object[]> juridicalContator = tool.executeQuery(MigrationUtils.processor(Long.class), translator.toSql(contractorQuery), contractVersionId);

        return juridicalContator.isEmpty();
    }

    private boolean checkPrintTemplate(Long contractVersionId, List<Long> printTemplatesIds, DBTool tool) throws SQLException
    {
        SQLSelectQuery selectQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t")).column("printtemplate_id")
                .where("id=?");

        Long printTemplateId = (Long) tool.getUniqueResult(tool.getDialect().getSQLTranslator().toSql(selectQuery), contractVersionId);

        return printTemplateId != null && printTemplatesIds.contains(printTemplateId);

    }
}
