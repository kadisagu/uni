/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.RatingListAdd.logic;

import com.google.common.collect.Lists;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrRequestTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.EnrReportRatingListAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic.EnrReportRatingListDao;
import ru.tandemservice.unienr14.request.entity.*;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 30.03.2015
 */
public class RsmuEnrReportRatingListDao extends EnrReportRatingListDao
{
    List<String> HIGHER_REQUEST = Lists.newArrayList(EnrRequestTypeCodes.HIGHER, EnrRequestTypeCodes.POSTGRADUATE, EnrRequestTypeCodes.TRAINEESHIP, EnrRequestTypeCodes.INTERNSHIP);

    @Override
    public byte[] buildReport(EnrReportRatingListAddUI model, String templateCatalogCode)
    {
        return buildReport(model, templateCatalogCode, true, false);
    }

    @Override
    protected void fillAdditionalCells(List<String> row, EnrRequestedCompetition reqComp, Set<EnrEntrantAchievement> achievements, String tableName, boolean printPriority, boolean competitionPrintForm)
    {
        if (!competitionPrintForm && !HIGHER_REQUEST.contains(reqComp.getRequest().getType().getCode()))
        {
            // перечень ИД
            NumberFormat formatter = new DecimalFormat("#0.##");
            StringBuilder info = new StringBuilder();
            int n = 0;
            if (achievements != null)
            {
                for (EnrEntrantAchievement achievement : achievements)
                {
                    if (n > 0)
                        info.append("\\par ");
                    info.append(++n).append(". ").append(achievement.getType().getAchievementKind().getShortTitle()).append(" — ")
                            .append(achievement.getType().isMarked() ? formatter.format(achievement.getRatingMarkAsLong() / 1000.0) : formatter.format(achievement.getType().getAchievementMarkAsLong() / 1000.0));
                }
            }
            int column = 3 + (T5.equals(tableName) ? 0 : 1) + (printPriority ? 1 : 0);
            row.add(row.size() - column, info.toString());

            //удаляем колонку с информацией о выбранных ОП, особых правах и пр.
            row.remove(row.size() - 2);
        }

           if (HIGHER_REQUEST.contains(reqComp.getRequest().getType().getCode()))
            {
                super.fillAdditionalCells(row, reqComp, achievements, tableName, printPriority, competitionPrintForm);
                int column = 4 + (T5.equals(tableName) ? 0 : 1) + (printPriority ? 1 : 0);
                row.remove(row.size() - column);
                if (!T5.equals(tableName))
                    row.remove(row.size() - 3);

                row.remove(row.size() - 2);
            }



    }

    @Override
    protected void addCustomLabelForHeader(RtfDocument document, RtfInjectModifier modifier, EnrProgramSetOrgUnit programSetOrgUnit)
    {
        if (EnrRequestTypeCodes.POSTGRADUATE.equals(programSetOrgUnit.getProgramSet().getRequestType().getCode()))
            modifier.put("eduSubjectKind", "Укрупненная группа специальностей");

    }

    @Override
    protected void modifiTableModifier(RtfDocument document, String tableHeader, IEnrExamSetDao.IExamSetSettings examSet, boolean printEntrantNumber, boolean printPriority,
                                       String tableName, List<String[]> tableContent, boolean competitionPrintForm, EnrCompetition competition)
    {
        final String requestCode = competition.getRequestType().getCode();
        new RtfTableModifier()
                .put(tableName, tableContent.toArray(new String[tableContent.size()][]))
                .put(tableName, new RtfRowIntercepterBase() {

                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex)
                    {
                        if (!printPriority)
                        {
                            int column = 4;
                            switch (tableName)
                            {
                                case T2:
                                case T4:
                                    column=5;
                                    break;
                                case T6:
                                case T7:
                                    column=6;
                                    break;
                            }
                            RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex), column); //ячейка в строке с меткой T
                            RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 1), column); //ячейка в шапке
                            if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
                                RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 2), column);
                        }
                        if (HIGHER_REQUEST.contains(requestCode))
                        {

                            if (competitionPrintForm)
                            {
                                int column = 3;
                                switch (tableName)
                                {
                                    case T2:
                                    case T4:
                                        column = 4;
                                        break;
                                    case T6:
                                    case T7:
                                        column = 5;
                                        break;
                                }
                                RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex), column);
                                RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 1), column);
                                if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
                                    RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 2), column);

                                int lenght = table.getRowList().get(currentRowIndex).getCellList().size();
                                RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex), lenght - 3);
                                RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 1), lenght - 3);
                                if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
                                    RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 2), lenght - 3);

                            }
                            else
                            {
                                int column = 4;
                                switch (tableName)
                                {
                                    case T2:
                                    case T4:
                                        column = 5;
                                        break;
                                    case T6:
                                    case T7:
                                        column = 6;
                                        break;
                                }
                                RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex), column + 1);
                                RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 1), column + 1);
                                if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
                                    RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 2), column + 1);

                                RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex), column);
                                RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 1), column);
                                if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
                                    RtfUtil.deleteCellPrevIncrease(table.getRowList().get(currentRowIndex - 2), column);

                            }

                            int lenght = table.getRowList().get(currentRowIndex).getCellList().size();
                            RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex), lenght - 2);
                            RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 1), lenght - 2);
                            if (T2.equals(tableName) || T4.equals(tableName) || T6.equals(tableName) || T7.equals(tableName))
                                RtfUtil.deleteCellNextIncrease(table.getRowList().get(currentRowIndex - 2), lenght - 2);
                        }

                        if (examSet.getElementList().isEmpty()) return;
                        if (!T2.equals(tableName) && !T4.equals(tableName) && !T6.equals(tableName) && !T7.equals(tableName)) return;

                        final int[] scales = new int[examSet.getElementList().size()];
                        Arrays.fill(scales, 1);

                        // разбиваем ячейку заголовка таблицы для названий оценок на нужное число элементов
                        RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), printEntrantNumber ? 4:3, (newCell, index) -> {
                            String content = examSet.getElementList().get(index).getElement().getShortTitle();
                            newCell.getElementList().addAll(new RtfString().append(IRtfData.QC).append(content).toList());
                        }, scales);
                        RtfUtil.splitRow(table.getRowList().get(currentRowIndex), printEntrantNumber ? 4: 3, null, scales);
                    }
                    // todo сделать утильный класс
                    @Override public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                        List<IRtfElement> list = new ArrayList<>();
                        IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                        text.setRaw(true);
                        list.add(text);
                        return list;
                    }
                    @Override public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                        newRowList.get(0).getCellList().get(0).addElements(new RtfString().boldBegin().append(tableHeader).boldEnd().toList());
                    }
                })
                .modify(document);
    }
}
