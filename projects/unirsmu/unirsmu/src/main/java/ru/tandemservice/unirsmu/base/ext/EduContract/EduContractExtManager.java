/* $Id:$ */
package ru.tandemservice.unirsmu.base.ext.EduContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.IEduContractDAO;
import ru.tandemservice.unirsmu.base.ext.EduContract.logic.RsmuEduContractDao;

/**
 * @author Denis Perminov
 * @since 25.07.2014
 */
@Configuration
public class EduContractExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEduContractDAO dao()
    {
        return new RsmuEduContractDao();
    }
}
