/* $Id:$ */
package ru.tandemservice.unirsmu.entrant.ext.EnrEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

/**
 * DEV-5062
 * Изменение логики формирования личного номера Абитуриента (2014)
 *
 * @author Denis Perminov
 * @since 10.06.2014
 */
@Configuration
public class EnrEntrantExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public INumberGenerationRule<EnrEntrant> entrantNumberGenerationRule()
    {
        return new EnrNumberGenerationRule<EnrEntrant>(EnrEntrant.class, EnrEntrant.personalNumber(), EnrNumberGenerationRule.toString(EnrEntrant.enrollmentCampaign().educationYear().intValue()))
        {
            @Override
            public String buildCandidate(EnrEntrant entrant, int currentQueueValue)
            {
                int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
                return String.format("%04d%05d", year, currentQueueValue);
            }
        };
    }
}
