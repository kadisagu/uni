package ru.tandemservice.unirsmu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unirsmu_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2"),
				 new ScriptDependency("ru.tandemservice.nsiclient", "2.9.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность rsmuVectorIds

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("rsmuvectorids_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_rsmuvectorids"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("vectorid_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("entityid_p", DBType.LONG).setNullable(false), 
				new DBColumn("entitytype_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("synctime_p", DBType.TIMESTAMP)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("rsmuVectorIds");

		}


    }
}