/* $Id$ */
package ru.tandemservice.unirsmu.dao.daemon.wrapper;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.fias.base.entity.AddressString;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 24.11.2015
 */
public class AddressWrapper
{
    private String _vectorId;
    private String _personId;
    private String _type;
    private String _postCode;
    private String _region;
    private String _area;
    private String _city;
    private String _streetHouse;

    public AddressWrapper()
    {
    }

    public AddressWrapper(ResultSet resultSet) throws SQLException
    {
        _vectorId = StringUtils.trimToNull(resultSet.getString(1));
        _personId = StringUtils.trimToNull(resultSet.getString(2));
        _type = StringUtils.trimToNull(resultSet.getString(3));
        _postCode = StringUtils.trimToNull(resultSet.getString(4));
        _region = StringUtils.trimToNull(resultSet.getString(5));
        _area = StringUtils.trimToNull(resultSet.getString(6));
        _city = StringUtils.trimToNull(resultSet.getString(7));
        _streetHouse = StringUtils.trimToNull(resultSet.getString(8));
    }

    public AddressString generateAddress()
    {
        StringBuilder addressStr = new StringBuilder();
        if (null != _postCode) addressStr.append(_postCode);
        if (null != _region) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_region);
        if (null != _area) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_area);
        if (null != _city) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_city);
        if (null != _streetHouse) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_streetHouse);

        AddressString address = new AddressString();
        address.setAddress(addressStr.toString());
        return address;
    }

    public void updateAddress(AddressString address)
    {
        if (null == address) return;

        StringBuilder addressStr = new StringBuilder();
        if (null != _postCode) addressStr.append(_postCode);
        if (null != _region) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_region);
        if (null != _area) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_area);
        if (null != _city) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_city);
        if (null != _streetHouse) addressStr.append(addressStr.length() > 0 ? ", " : "").append(_streetHouse);
        address.setAddress(addressStr.toString());
    }

    public static AddressWrapper getTheBest(List<AddressWrapper> wrappers)
    {
        if (null == wrappers || wrappers.isEmpty()) return null;

        Map<Integer, Integer> fullnessMap = new HashMap<>();
        Map<Integer, AddressWrapper> wrapperMap = new HashMap<>();

        int bestFullness = 0;
        AddressWrapper best = wrappers.get(0);
        for (AddressWrapper wrapper : wrappers)
        {
            fullnessMap.put(wrappers.indexOf(wrapper), wrapper.getFullnessCoefficient());
            wrapperMap.put(wrappers.indexOf(wrapper), wrapper);

            if (wrapper.getFullnessCoefficient() > bestFullness)
            {
                bestFullness = wrapper.getFullnessCoefficient();
                best = wrapper;
            }
        }

        Set<Integer> identicalFullness = new HashSet<>();
        for (Map.Entry<Integer, Integer> entry : fullnessMap.entrySet())
        {
            if (entry.getValue() == bestFullness) identicalFullness.add(entry.getKey());
        }

        if (identicalFullness.size() > 1)
        {
            List<AddressWrapper> identicalWrappers = new ArrayList<>();
            for (Integer idx : identicalFullness) identicalWrappers.add(wrappers.get(idx));

            for (AddressWrapper wrapper : identicalWrappers)
            {
                if ("2".equals(wrapper.getType())) best = wrapper;
            }
        }

        return best;
    }

    public int getFullnessCoefficient()
    {
        int coeff = 0;
        if (null != _postCode) coeff++;
        if (null != _region) coeff++;
        if (null != _area) coeff++;
        if (null != _city) coeff++;
        if (null != _streetHouse) coeff++;
        return coeff;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        if (null != _postCode) builder.append(_postCode);
        if (null != _region) builder.append(builder.length() > 0 ? ", " : "").append(_region);
        if (null != _area) builder.append(builder.length() > 0 ? ", " : "").append(_area);
        if (null != _city) builder.append(builder.length() > 0 ? ", " : "").append(_city);
        if (null != _streetHouse) builder.append(builder.length() > 0 ? ", " : "").append(_streetHouse);
        builder.insert(0, _vectorId + "\t" + _personId + "\t");
        return builder.toString();
    }

    public String getVectorId()
    {
        return _vectorId;
    }

    public void setVectorId(String vectorId)
    {
        _vectorId = vectorId;
    }

    public String getPersonId()
    {
        return _personId;
    }

    public void setPersonId(String personId)
    {
        _personId = personId;
    }

    public String getType()
    {
        return _type;
    }

    public void setType(String type)
    {
        _type = type;
    }

    public String getPostCode()
    {
        return _postCode;
    }

    public void setPostCode(String postCode)
    {
        _postCode = postCode;
    }

    public String getRegion()
    {
        return _region;
    }

    public void setRegion(String region)
    {
        _region = region;
    }

    public String getArea()
    {
        return _area;
    }

    public void setArea(String area)
    {
        _area = area;
    }

    public String getCity()
    {
        return _city;
    }

    public void setCity(String city)
    {
        _city = city;
    }

    public String getStreetHouse()
    {
        return _streetHouse;
    }

    public void setStreetHouse(String streetHouse)
    {
        _streetHouse = streetHouse;
    }
}