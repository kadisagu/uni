package ru.tandemservice.unirsmu.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Контрагенты и Договоры»"
 * Имя сущности : ctrTemplateScriptItem
 * Файл data.xml : unirsmu.data.xml
 */
public interface CtrTemplateScriptItemCodes
{
    /** Константа кода (code) элемента : Квитанция для договора на обучение (title) */
    String EDU_CONTRACT_PAYMENT_QUITTANCE = "edu.payment.quittance";
    /** Константа кода (code) элемента : Список должников по оплате (title) */
    String EDU_CTR_DEBITORS_PAY_REPORT = "edu.ctrdebitorspayreport";
    /** Константа кода (code) элемента : Список платежей (title) */
    String EDU_CTR_PAYMENTS_REPORT = "edu.ctrpaymentsreport";
    /** Константа кода (code) элемента : Помесячное поступление средств (title) */
    String EDU_CTR_AGREEMENT_INCOME_REPORT = "edu.ctragreementincomereport";

    Set<String> CODES = ImmutableSet.of(EDU_CONTRACT_PAYMENT_QUITTANCE, EDU_CTR_DEBITORS_PAY_REPORT, EDU_CTR_PAYMENTS_REPORT, EDU_CTR_AGREEMENT_INCOME_REPORT);
}
