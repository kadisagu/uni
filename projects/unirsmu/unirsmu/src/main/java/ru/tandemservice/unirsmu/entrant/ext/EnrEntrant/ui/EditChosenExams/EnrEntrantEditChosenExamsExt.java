/* $Id$ */
package ru.tandemservice.unirsmu.entrant.ext.EnrEntrant.ui.EditChosenExams;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.EditChosenExams.EnrEntrantEditChosenExams;

/**
 * @author Andrey Avetisov
 * @since 31.03.2015
 */

@Configuration
public class EnrEntrantEditChosenExamsExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unirsmu" + EnrEntrantEditChosenExamsExtUI.class.getSimpleName();

    @Autowired
    private EnrEntrantEditChosenExams _entrantEditChosenExams;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_entrantEditChosenExams.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrEntrantEditChosenExamsExtUI.class))
                .create();
    }
}
