/* $Id$ */
package ru.tandemservice.unirsmu.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.RsmuSystemActionManager;
import ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.StudImportFromExcel.RsmuSystemActionStudImportFromExcel;
import ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.VectorSync.RsmuSystemActionVectorSync;

import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 12.01.2015
 */
public class RsmuSystemActionPubAddon extends UIAddon
{
    private static String _photosDir = "data/photo";

    public RsmuSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }


    public void onClickImportEnrPhoto()
    {
        String path = ApplicationRuntime.getAppInstallPath();
        File dir = new File(path, _photosDir);
        if (dir.listFiles() != null)
        {
            List<File> photoList = Arrays.asList(dir.listFiles());
            RsmuSystemActionManager.instance().dao().savePhotos(photoList);
        } else
        {
            throw new ApplicationException("Импорт данных провести не удалось: не найден файлы с исходными данными в каталоге «${app.install.path}/data/photo». Разместите файл для импорта на сервере приложения по указанному пути.");
        }
    }

    public void onClickSyncWithVectorDatabaseForMsr()
    {
        getActivationBuilder().asDesktopRoot(RsmuSystemActionVectorSync.class).activate();
    }

    public void onClickImportStudentsFromExcel()
    {
        getActivationBuilder().asDesktopRoot(RsmuSystemActionStudImportFromExcel.class).activate();
    }

    public void onClickDeleteStudents()
    {
        RsmuSystemActionManager.instance().dao().deleteAllStudents();
    }

    public void onClickDeleteOtherInvalidExtracts()
    {
        RsmuSystemActionManager.instance().dao().deleteOtherInvalidExtracts();
    }

    public void onClickOptimizeOtherOrders()
    {
        int i = 1;
        while (i > 0)
        {
            i = RsmuSystemActionManager.instance().dao().doOptimizeOtherOrders();
            System.out.println("Deleted 100 of " + i + " orders");
        }
    }

    public void onClickDeleteFakeStudents()
    {
        int i = 1;
        while (i > 0)
        {
            i = RsmuSystemActionManager.instance().dao().changeFakeStudentsStatus();
            System.out.println("Updated 100 of " + i + " fake students");
        }
    }
}