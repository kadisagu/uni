/* $Id:$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab.EnrEntrantPubRequestTabUI;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Pub.EnrEntrantRequestPubUI;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.Date;

/**
 * @author Denis Perminov
 * @since 19.06.2014
 */
public class OnClickPrintRequestEqualDocsAction extends NamedUIAction
{
    public OnClickPrintRequestEqualDocsAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        String fio;
        String fio_G;
        Long currentEnttrantRequestId = presenter.getListenerParameterAsLong();
        if (null == currentEnttrantRequestId)
            return;

        EnrEntrantRequest currentEntrantRequest = DataAccessServices.dao().getNotNull(currentEnttrantRequestId);
        if (null == currentEntrantRequest)
            return;
        Date registrationDate = currentEntrantRequest.getRegDate();
        IdentityCard identityCard = currentEntrantRequest.getIdentityCard();
        EnrEntrant enrEntrant = currentEntrantRequest.getEntrant();
        String personalNumber = enrEntrant.getPersonalNumber();

        EnrScriptItem template = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REQUEST_ENTRANT_REQUEST_EQUAL_DOCS);
        if (null != template)
        {
            final RtfDocument document = new RtfReader().read(template.getCurrentTemplate());
            RtfInjectModifier im = new RtfInjectModifier();
            fio = PersonManager.instance().declinationDao().getDeclinationFIO(identityCard, GrammaCase.NOMINATIVE);
            fio_G = PersonManager.instance().declinationDao().getDeclinationFIO(identityCard, GrammaCase.GENITIVE);
            im.put("FIO", fio.toUpperCase());
            im.put("entrantNumber", personalNumber);
            im.put("dateReq", DateFormatter.STRING_MONTHS_AND_QUOTES.format(registrationDate));
            im.modify(document);
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Заявление о приеме иностранного документа " + fio + ".rtf").document(document), false);
        }
    }
}
