/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 11.02.2015
 */
public interface IRsmuEnrNotAppearEntrantDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void updateExamGroupForDiscipline(List<EnrExamPassDiscipline> examPassDisciplineList, EnrExamGroup examGroup);
}
