/* $Id$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.AddAppend;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.util.NumberAsStringComparator;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppendUI;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.util.EnrRequestedCompetitionWrapper;

import java.util.Collections;
import java.util.Comparator;

/**
 * @author Andrey Avetisov
 * @since 07.04.2015
 */
public class EnrEntrantRequestAddAppendClickSelect extends NamedUIAction
{
    public EnrEntrantRequestAddAppendClickSelect(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        EnrEntrantRequestAddAppendUI requestAddAppendUI = (EnrEntrantRequestAddAppendUI) presenter;
        requestAddAppendUI.onClickSelect();

        Collections.sort(requestAddAppendUI.getRequestedCompetitionList(), new Comparator<EnrRequestedCompetitionWrapper>()
        {
            @Override
            public int compare(EnrRequestedCompetitionWrapper o1, EnrRequestedCompetitionWrapper o2)
            {
                return NumberAsStringComparator.get().compare(o1.getCompetition().getType().getCode(), o2.getCompetition().getType().getCode());
            }
        });
    }
}
