package ru.tandemservice.unirsmu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unirsmu.entity.RsmuVectorIds;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Идентификаторы сущностей в Векторе
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RsmuVectorIdsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unirsmu.entity.RsmuVectorIds";
    public static final String ENTITY_NAME = "rsmuVectorIds";
    public static final int VERSION_HASH = 1940220738;
    private static IEntityMeta ENTITY_META;

    public static final String P_VECTOR_ID = "vectorId";
    public static final String P_ENTITY_ID = "entityId";
    public static final String P_ENTITY_TYPE = "entityType";
    public static final String P_SYNC_TIME = "syncTime";

    private String _vectorId;     // Идентификатор объекта в Векторе
    private long _entityId;     // Идентификатор сущности УНИ
    private String _entityType;     // Тип сущности УНИ
    private Date _syncTime;     // Дата и время синхронизации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор объекта в Векторе. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getVectorId()
    {
        return _vectorId;
    }

    /**
     * @param vectorId Идентификатор объекта в Векторе. Свойство не может быть null.
     */
    public void setVectorId(String vectorId)
    {
        dirty(_vectorId, vectorId);
        _vectorId = vectorId;
    }

    /**
     * @return Идентификатор сущности УНИ. Свойство не может быть null.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор сущности УНИ. Свойство не может быть null.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEntityType()
    {
        return _entityType;
    }

    /**
     * @param entityType Тип сущности УНИ. Свойство не может быть null.
     */
    public void setEntityType(String entityType)
    {
        dirty(_entityType, entityType);
        _entityType = entityType;
    }

    /**
     * @return Дата и время синхронизации.
     */
    public Date getSyncTime()
    {
        return _syncTime;
    }

    /**
     * @param syncTime Дата и время синхронизации.
     */
    public void setSyncTime(Date syncTime)
    {
        dirty(_syncTime, syncTime);
        _syncTime = syncTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RsmuVectorIdsGen)
        {
            setVectorId(((RsmuVectorIds)another).getVectorId());
            setEntityId(((RsmuVectorIds)another).getEntityId());
            setEntityType(((RsmuVectorIds)another).getEntityType());
            setSyncTime(((RsmuVectorIds)another).getSyncTime());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RsmuVectorIdsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RsmuVectorIds.class;
        }

        public T newInstance()
        {
            return (T) new RsmuVectorIds();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "vectorId":
                    return obj.getVectorId();
                case "entityId":
                    return obj.getEntityId();
                case "entityType":
                    return obj.getEntityType();
                case "syncTime":
                    return obj.getSyncTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "vectorId":
                    obj.setVectorId((String) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "entityType":
                    obj.setEntityType((String) value);
                    return;
                case "syncTime":
                    obj.setSyncTime((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "vectorId":
                        return true;
                case "entityId":
                        return true;
                case "entityType":
                        return true;
                case "syncTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "vectorId":
                    return true;
                case "entityId":
                    return true;
                case "entityType":
                    return true;
                case "syncTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "vectorId":
                    return String.class;
                case "entityId":
                    return Long.class;
                case "entityType":
                    return String.class;
                case "syncTime":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RsmuVectorIds> _dslPath = new Path<RsmuVectorIds>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RsmuVectorIds");
    }
            

    /**
     * @return Идентификатор объекта в Векторе. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getVectorId()
     */
    public static PropertyPath<String> vectorId()
    {
        return _dslPath.vectorId();
    }

    /**
     * @return Идентификатор сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getEntityType()
     */
    public static PropertyPath<String> entityType()
    {
        return _dslPath.entityType();
    }

    /**
     * @return Дата и время синхронизации.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getSyncTime()
     */
    public static PropertyPath<Date> syncTime()
    {
        return _dslPath.syncTime();
    }

    public static class Path<E extends RsmuVectorIds> extends EntityPath<E>
    {
        private PropertyPath<String> _vectorId;
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _entityType;
        private PropertyPath<Date> _syncTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор объекта в Векторе. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getVectorId()
     */
        public PropertyPath<String> vectorId()
        {
            if(_vectorId == null )
                _vectorId = new PropertyPath<String>(RsmuVectorIdsGen.P_VECTOR_ID, this);
            return _vectorId;
        }

    /**
     * @return Идентификатор сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(RsmuVectorIdsGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Тип сущности УНИ. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getEntityType()
     */
        public PropertyPath<String> entityType()
        {
            if(_entityType == null )
                _entityType = new PropertyPath<String>(RsmuVectorIdsGen.P_ENTITY_TYPE, this);
            return _entityType;
        }

    /**
     * @return Дата и время синхронизации.
     * @see ru.tandemservice.unirsmu.entity.RsmuVectorIds#getSyncTime()
     */
        public PropertyPath<Date> syncTime()
        {
            if(_syncTime == null )
                _syncTime = new PropertyPath<Date>(RsmuVectorIdsGen.P_SYNC_TIME, this);
            return _syncTime;
        }

        public Class getEntityClass()
        {
            return RsmuVectorIds.class;
        }

        public String getEntityName()
        {
            return "rsmuVectorIds";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
