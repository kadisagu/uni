/* $Id$ */
package ru.tandemservice.unirsmu.dao.daemon;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.*;
import org.hibernate.Session;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.movestudent.entity.StudentOtherOrder;
import ru.tandemservice.movestudent.entity.StudentOtherParagraph;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unirsmu.base.bo.RsmuSettings.RsmuSettingsManager;
import ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.StudImportFromExcel.StudentImportFromExcelUtil;
import ru.tandemservice.unirsmu.dao.daemon.wrapper.*;
import ru.tandemservice.unirsmu.entity.RsmuVectorIds;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 23.11.2015
 */
public class RsmuVectorSyncDaemonDao extends UniBaseDao implements IRsmuVectorSyncDaemonDao
{
    public static final int DAEMON_ITERATION_TIME = 1;  //TODO
    private static boolean sync_locked = false;

    public static final String CATALOG_PERSONS = "Person";
    public static final String CATALOG_STUDENTS = "Student";
    public static final String CATALOG_ADDRESS = "Address";
    public static final String CATALOG_CONTACT_DATA = "ContactData";
    public static final String CATALOG_PHOTO = "Photo";

    public static List<String> CATALOGS_TO_SYNC_QUEUE = new ArrayList<>();
    public static final int PORTION_ELEMENTS_AMOUNT = 1000;

    public static String CURRENT_CATALOG;
    public static long CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
    public static long CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
    public static long CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;

    private static InputStream _excelFileInputStream;

    protected static final Logger log4j_logger = Logger.getLogger(RsmuVectorSyncDaemonDao.class);
    protected static final Logger log4j_logger_imp_err = Logger.getLogger(IRsmuVectorSyncDaemonDao.class);

    public static Logger getLog4jLogger()
    {
        return log4j_logger;
    }

    public static void logEvent(Level loggingLevel, String message)
    {
        // проверяем, есть ли уже appender
        Appender appender = log4j_logger.getAppender("VectorSyncAppender");
        Appender appenderImpErr = log4j_logger_imp_err.getAppender("VectorSyncAppenderImpErr");

        if (null != appender) log4j_logger.log(loggingLevel, message);
        if (null != appenderImpErr && Level.ERROR.equals(loggingLevel))
            log4j_logger_imp_err.log(loggingLevel, message);

        if (null == appender)
        {
            try
            {
                // добавляем, если нет
                final String path = ApplicationRuntime.getAppInstallPath();
                Calendar cal = CoreDateUtils.createCalendar(new Date());
                String logFileName = "VectorSync_" + cal.get(Calendar.YEAR) + "_" + (cal.get(Calendar.MONTH) + 1) + "_" + cal.get(Calendar.DAY_OF_MONTH) + ".log";
                try
                {
                    appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + logFileName));
                } catch (IOException ex)
                {

                }

                appender.setName("VectorSyncAppender");
                ((FileAppender) appender).setThreshold(Level.INFO);
                log4j_logger.addAppender(appender);
                log4j_logger.setLevel(Level.INFO);

                if (null != appender)
                {
                    log4j_logger.log(loggingLevel, message);
                }

            } finally
            {
                // и отцепляем, если добавляли
                if (null != appender)
                {
                    log4j_logger.removeAppender(appender);
                }
            }
        }

        if (null == appenderImpErr)
        {
            try
            {
                // добавляем, если нет
                final String path = ApplicationRuntime.getAppInstallPath();
                Calendar cal = CoreDateUtils.createCalendar(new Date());
                String logFileName = "VectorSyncImpErr_" + cal.get(Calendar.YEAR) + "_" + (cal.get(Calendar.MONTH) + 1) + "_" + cal.get(Calendar.DAY_OF_MONTH) + ".log";
                try
                {
                    appenderImpErr = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + logFileName));
                } catch (IOException ex)
                {

                }

                appenderImpErr.setName("VectorSyncAppenderImpErr");
                ((FileAppender) appenderImpErr).setThreshold(Level.ERROR);
                log4j_logger_imp_err.addAppender(appenderImpErr);
                log4j_logger_imp_err.setLevel(Level.ERROR);

                if (null != appenderImpErr)
                {
                    log4j_logger_imp_err.log(loggingLevel, message);
                }

            } finally
            {
                // и отцепляем, если добавляли
                if (null != appenderImpErr)
                {
                    log4j_logger_imp_err.removeAppender(appenderImpErr);
                }
            }
        }
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(RsmuVectorSyncDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IRsmuVectorSyncDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Проверяем, включена ли возможность синхронизации в пропертях.
            // Если хотя бы один из параметров для соединения с сервером БД не задан, то прекращаем синхронизацию.
            if (RsmuSettingsManager.isAnyConnectionParameterIsNotSpecified())
            {
                CATALOGS_TO_SYNC_QUEUE.clear();
                CURRENT_CATALOG = null;
                CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
                CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
                CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;
                return;
            }

            if (!RsmuVectorSyncDaemonDao.isConnectionEstablished())
                logEvent(Level.ERROR, "Could not connect to the Vector's database. Please, check connection settings and database host availability.");

            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked) return;
            sync_locked = true;

            try
            {
                if (null != _excelFileInputStream)
                {
                    StudentImportFromExcelUtil.importStudents(_excelFileInputStream);
                    _excelFileInputStream = null;
                }

                if (!CATALOGS_TO_SYNC_QUEUE.isEmpty()) processCatalogPortion();

            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
                _excelFileInputStream = null;
                t.printStackTrace();
            }
            sync_locked = false;
        }
    };

    public static void registerExcelImport(InputStream excelFileInputStream)
    {
        _excelFileInputStream = excelFileInputStream;
        StudentImportFromExcelUtil.waitForCall();
    }

    protected static Connection createConnection() throws Throwable
    {
        String dbHost = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_HOST));
        String dbPort = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_PORT));
        String dbName = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_NAME));
        String dbLogin = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_LOGIN));
        String dbPassword = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_PASSWORD));
        if (null == dbPort) dbPort = "1433";

        StringBuilder connectionString = new StringBuilder("jdbc:jtds:sqlserver://");
        connectionString.append(dbHost).append(":").append(dbPort).append("/").append(dbName);
        Connection connection = DriverManager.getConnection(connectionString.toString(), dbLogin, dbPassword);
        connection.setAutoCommit(false);

        return connection;
    }

    public static boolean isConnectionEstablished()
    {
        try
        {
            Statement st = createConnection().createStatement();
            st.execute("select count(*) from student");
            ResultSet rs = st.getResultSet();
            rs.next();
            return true;
        } catch (Throwable e)
        {
            return false;
        }
    }

    protected static void processCatalogPortion() throws Throwable
    {
        Connection connection = createConnection();

        switch (CATALOGS_TO_SYNC_QUEUE.get(0))
        {
            case CATALOG_PERSONS:
                processPersonsCatalog(connection);
                break;
            case CATALOG_STUDENTS:
                processStudentsCatalog(connection);
                break;
            case CATALOG_ADDRESS:
                processAddressCatalog(connection);
                break;
            case CATALOG_CONTACT_DATA:
                processContactsCatalog(connection);
                break;
            case CATALOG_PHOTO:
                processPhotoCatalog(connection);
                break;
            default:
                break;
        }

        connection.close();
    }

    protected static void processPersonsCatalog(Connection connection) throws Throwable
    {
        logEvent(Level.INFO, "========= SYNC PERSONS CATALOG ITERATION HAS STARTED =========");

        Statement prePerSt = connection.createStatement();
        prePerSt.execute("select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1 order by p.PersonId");

        ResultSet prePerRs = prePerSt.getResultSet();
        List<String> prePersonUniqIdList = new ArrayList<>();
        while (prePerRs.next()) prePersonUniqIdList.add(prePerRs.getString(1));
        logEvent(Level.INFO, "....... " + prePersonUniqIdList.size() + " active student Persons found at Vector's database");

        Statement preSt = connection.createStatement();
        preSt.execute("select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId left outer join DocumentPerson d on p.PersonId=d.PersonId " +
                "left outer join listTypeDocument ltd on d.TypeDocumentId=ltd.TypeDocumentId " +
                "left outer join listTypeDocumentIdentityCard ltdic on ltd.TypeDocumentId=ltdic.TypeDocumentIdentityCardId " +
                "where a.ActiveAgreement=1 order by p.PersonId"); //and (d.DocumentId is null or ltdic.TypeDocumentIdentityCardId is not null)

        ResultSet preRs = preSt.getResultSet();
        List<String> prePersonIdList = new ArrayList<>();
        while (preRs.next()) prePersonIdList.add(preRs.getString(1));
        logEvent(Level.INFO, "....... " + prePersonIdList.size() + " active student Person identity cards found at Vector's database");

        int uniquePersonsCnt = 0;
        int uniquePersonsFullCnt = 0;
        long stopCount = CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + PORTION_ELEMENTS_AMOUNT;

        Statement st = connection.createStatement();

        List<String> idsToProcess = new ArrayList<>();
        List<String> uniqueIdsFull = new ArrayList<>();
        LinkedHashMap<String, List<PersonWrapper>> persLinesMap = new LinkedHashMap<>();
        List<String> filteredIdList = IRsmuVectorSyncDaemonDao.instance.get().getFilteredIdList(prePersonIdList, Person.class, Person.ENTITY_NAME);

        logEvent(Level.INFO, "....... " + filteredIdList.size() + " active student Person identity cards should be updated (older than 1 day).");

        st.execute("select p.PersonID, p.Family, p.FirstName, p.SecName, p.BirthDate, p.BirthPlace, p.Gender, " +
                "ltdic.TypeDocumentIdentityCardId, ltd.TypeDocumentId, ltd.TypeDocumentFullName,  d.DocumentSeries, " +
                "d.DocumentNumber, d.DocumentDate, d.DocumentOrganization, d.OrganizationCode " +
                "from Person p inner join Student s on p.PersonId=S.StudentId inner join AgreementStudent a on s.StudentId=a.StudentId " +
                "left outer join DocumentPerson d on p.PersonId=d.PersonId left outer join listTypeDocument ltd on d.TypeDocumentId=ltd.TypeDocumentId " +
                "left outer join listTypeDocumentIdentityCard ltdic on ltd.TypeDocumentId=ltdic.TypeDocumentIdentityCardId " +
                "where a.ActiveAgreement=1 order by p.PersonId"); //and (d.DocumentId is null or ltdic.TypeDocumentIdentityCardId is not null)

        ResultSet rs = st.getResultSet();

        while (rs.next())
        {
            String vectorId = rs.getString(1);
            if (!uniqueIdsFull.contains(vectorId))
            {
                uniqueIdsFull.add(vectorId);
                uniquePersonsFullCnt++;
            }

            // Если отфильтрованный список содержит соответствующий идентификатор, то игнорируем (сущность актуальная, с момента обновления прошло меньше суток)
            if (null == vectorId || !filteredIdList.contains(vectorId)) continue;

            PersonWrapper wrapper = new PersonWrapper(rs);
            if (!idsToProcess.contains(wrapper.getVectorId()))
            {
                idsToProcess.add(wrapper.getVectorId());
                uniquePersonsCnt++;
            }

            List<PersonWrapper> persons = persLinesMap.get(wrapper.getVectorId());
            if (null == persons) persons = new ArrayList<>();
            persons.add(wrapper);
            persLinesMap.put(wrapper.getVectorId(), persons);
        }

        st.close();

        CURRENT_CATALOG = CATALOG_PERSONS;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = uniquePersonsFullCnt;
        if (0 == CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = uniquePersonsCnt;

        List<String> idsToProcessPortion = idsToProcess.subList(0, Long.valueOf(Math.min(idsToProcess.size(), PORTION_ELEMENTS_AMOUNT)).intValue());
        Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> idsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsToProcessPortion, Person.class, Person.ENTITY_NAME);

        for (String vectorId : idsToProcessPortion)
        {
            List<PersonWrapper> items = persLinesMap.get(vectorId);

            if (null != items && !items.isEmpty())
            {
                PersonWrapper wrapper = PersonWrapper.getTheBest(items);
                CoreCollectionUtils.Pair<Person, RsmuVectorIds> idsPair = idsMap.get(vectorId);
                RsmuVectorIds ids = null != idsPair ? idsPair.getY() : null;

                if (null == ids)
                {
                    ids = new RsmuVectorIds();
                    ids.setSyncTime(new Date());
                    ids.setVectorId(wrapper.getVectorId());
                    ids.setEntityType(Person.ENTITY_NAME);
                }

                logEvent(Level.INFO, wrapper.toString());
                Person person = null != idsPair && null != idsPair.getX() ? idsPair.getX() : null;
                if (null == person) person = wrapper.generatePerson();
                else wrapper.updatePerson(person);

                idsPair = new CoreCollectionUtils.Pair(person, ids);
                idsMap.put(vectorId, idsPair);
            }
        }

        try
        {
            IRsmuVectorSyncDaemonDao.instance.get().savePersonsList(idsToProcessPortion, idsMap, true);
        } catch (Exception e)
        {
            logEvent(Level.ERROR, "++++++++++ Saving Persons Error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }

        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = Math.min(stopCount, CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED);
        logEvent(Level.INFO, "------ Persons processed = " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + " / " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED + " (" + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL + ")");
        if (CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED >= CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
        {
            CATALOGS_TO_SYNC_QUEUE.remove(CURRENT_CATALOG);
            //CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;
        }

        logEvent(Level.INFO, "========= SYNC PERSONS CATALOG WAS FINISHED =========");
    }

    protected static void processStudentsCatalog(Connection connection) throws Throwable
    {
        logEvent(Level.INFO, "========= SYNC STUDENTS CATALOG HAS STARTED =========");

        Statement preSt = connection.createStatement();
        preSt.execute("select s.StudentID from Person p inner join Student s on p.PersonId=s.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId " +
                "where a.ActiveAgreement=1 and a.Code <> '-' order by p.PersonId");

        ResultSet preRs = preSt.getResultSet();
        List<String> preStudentIdList = new ArrayList<>();
        while (preRs.next()) preStudentIdList.add(preRs.getString(1));

        int uniqueStudentsCnt = 0;
        int uniqueStudentsFullCnt = 0;
        long stopCount = CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + PORTION_ELEMENTS_AMOUNT;

        List<String> idsToProcess = new ArrayList<>();
        List<String> uniqueIdsFull = new ArrayList<>();
        LinkedHashMap<String, StudentWrapper> studentMap = new LinkedHashMap<>();
        List<String> filteredIdList = IRsmuVectorSyncDaemonDao.instance.get().getFilteredIdList(preStudentIdList, Student.class, Student.ENTITY_NAME);

        Statement studSt = connection.createStatement();
        studSt.execute("select s.StudentId, s.StudentComment, f.FacultyName, a.Code, dir.DirectionName, q.QualificationName, ef.FormName, ed.DurationShortName, pd.AgreementId " +
                "from student s inner join AgreementStudent a on s.StudentId=a.StudentId left outer join AgreementStudentPaid pd on a.AgreementId=pd.AgreementId " +
                "inner join listFaculty f on a.FacultyId=f.FacultyId inner join listDirection dir on a.DirectionId=dir.DirectionId " +
                "inner join listQualification q on a.QualificationId=q.QualificationId inner join listFormEducation ef on a.FormId=ef.FormId " +
                "inner join listDurationEducation ed on a.DurationId=ed.DurationId where a.ActiveAgreement=1 and a.Code <> '-' order by s.studentId");

        ResultSet studRs = studSt.getResultSet();

        while (studRs.next())
        {
            String vectorId = studRs.getString(1);
            if (!uniqueIdsFull.contains(vectorId))
            {
                uniqueIdsFull.add(vectorId);
                uniqueStudentsFullCnt++;
            }

            // Если отфильтрованный список содержит соответствующий идентификатор, то игнорируем (сущность актуальная, с момента обновления прошло меньше суток)
            if (null == vectorId || !filteredIdList.contains(vectorId)) continue;

            StudentWrapper wrapper = new StudentWrapper(studRs);
            if (!idsToProcess.contains(wrapper.getVectorId()))
            {
                idsToProcess.add(wrapper.getVectorId());
                uniqueStudentsCnt++;
            }

            studentMap.put(wrapper.getVectorId(), wrapper);
        }

        studSt.close();


        Statement semSt = connection.createStatement();
        semSt.execute("select s.StudentId, sem.Semester, sem.EducationGroup, sem.StatusId, st.StatusName from student s " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId inner join EducationSemester sem on a.AgreementId=sem.EducationAgreementID " +
                "inner join listStatusEducation st on sem.StatusID=st.StatusId where a.ActiveAgreement=1 and a.Code <> '-' " +
                "order by s.studentId asc, sem.WhenAdd desc");

        ResultSet semRs = semSt.getResultSet();
        Set<String> processedStudentIds = new HashSet<>();

        while (semRs.next())
        {
            String vectorId = semRs.getString(1);
            StudentWrapper wrapper = studentMap.get(vectorId);

            if (null != wrapper)
            {
                if (!processedStudentIds.contains(vectorId))
                {
                    wrapper.updateSemesterData(semRs);
                    processedStudentIds.add(vectorId);
                }

                studentMap.put(wrapper.getVectorId(), wrapper);
            }
        }

        semSt.close();


        Statement eventSt = connection.createStatement();
        eventSt.execute("select s.StudentId, ev.OrderNumber, ev.OrderDate, ev.TypeEventId, lte.TypeEventName from student s " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId inner join EventStudent ev on a.AgreementId=ev.AgreementId " +
                "inner join listTypeEvent lte on ev.TypeEventId=lte.TypeEventId where a.ActiveAgreement=1 and a.Code <> '-' " +
                "order by s.studentId, ev.OrderDate desc");

        ResultSet eventRs = eventSt.getResultSet();
        Map<String, EnrOrderWrapper> enrOrderMap = new HashMap<>();
        Map<String, EnrOrderWrapper> restOrderMap = new HashMap<>();

        processedStudentIds.clear();

        while (eventRs.next())
        {
            String vectorId = eventRs.getString(1);
            StudentWrapper wrapper = studentMap.get(vectorId);

            EnrOrderWrapper order = new EnrOrderWrapper(eventRs);
            if (order.isEnrollmentOrder() && !enrOrderMap.containsKey(vectorId)) enrOrderMap.put(vectorId, order);
            if (order.isRestoreOrder() && !restOrderMap.containsKey(vectorId)) restOrderMap.put(vectorId, order);

            if (null != wrapper)
            {
                if (!processedStudentIds.contains(vectorId))
                {
                    wrapper.updateOrderData(eventRs);
                    processedStudentIds.add(vectorId);
                }

                studentMap.put(wrapper.getVectorId(), wrapper);
            }
        }

        eventSt.close();


        for (Map.Entry<String, StudentWrapper> entry : studentMap.entrySet())
        {
            EnrOrderWrapper orderWrapper = enrOrderMap.get(entry.getKey());
            if (null == orderWrapper) orderWrapper = restOrderMap.get(entry.getKey());
            if (null != orderWrapper)
            {
                entry.getValue().updateEnrOrderData(orderWrapper);
            }
        }


        CURRENT_CATALOG = CATALOG_STUDENTS;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = uniqueStudentsFullCnt;
        if (0 == CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = uniqueStudentsCnt;

        List<String> idsToProcessPortion = idsToProcess.subList(0, Long.valueOf(Math.min(idsToProcess.size(), PORTION_ELEMENTS_AMOUNT)).intValue());
        Map<String, CoreCollectionUtils.Pair<Student, RsmuVectorIds>> idsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsToProcessPortion, Student.class, Student.ENTITY_NAME);
        Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> personMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsToProcessPortion, Person.class, Person.ENTITY_NAME);
        List<String> correctIdsToProcessPortion = new ArrayList<>();


        for (String vectorId : idsToProcessPortion)
        {
            StudentWrapper wrapper = studentMap.get(vectorId);

            if (null != wrapper)
            {
                CoreCollectionUtils.Pair<Student, RsmuVectorIds> idsPair = idsMap.get(vectorId);
                RsmuVectorIds ids = null != idsPair ? idsPair.getY() : null;

                if (null == ids)
                {
                    ids = new RsmuVectorIds();
                    ids.setSyncTime(new Date());
                    ids.setVectorId(wrapper.getVectorId());
                    ids.setEntityType(Student.ENTITY_NAME);
                }

                logEvent(Level.INFO, wrapper.toString());
                Student student = null != idsPair && null != idsPair.getX() ? idsPair.getX() : null;
                if (null == student) student = wrapper.generateStudent(personMap);
                else wrapper.updateStudent(student, personMap);

                if (null != student)
                {
                    correctIdsToProcessPortion.add(vectorId);
                    idsPair = new CoreCollectionUtils.Pair(student, ids);
                    idsMap.put(vectorId, idsPair);
                }
            }
        }

        try
        {
            IRsmuVectorSyncDaemonDao.instance.get().saveStudentsList(correctIdsToProcessPortion, idsMap, studentMap);
        } catch (Exception e)
        {
            logEvent(Level.ERROR, "++++++++++ Saving Students Error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }

        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = Math.min(stopCount, CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED);
        logEvent(Level.INFO, "------ Students processed = " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + " / " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED + " (" + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL + ")");
        if (CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED >= CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
        {
            CATALOGS_TO_SYNC_QUEUE.remove(CURRENT_CATALOG);
            //CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;
        }

        logEvent(Level.INFO, "========= SYNC STUDENTS CATALOG WAS FINISHED =========");
    }

    protected static void processAddressCatalog(Connection connection) throws Throwable
    {
        logEvent(Level.INFO, "========= SYNC PERSON ADDRESS CATALOG HAS STARTED =========");

        Statement preSt = connection.createStatement();
        preSt.execute("select AddressId from AddressPerson where personId in (select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1) order by PersonId");

        ResultSet preRs = preSt.getResultSet();
        List<String> preAddressIdList = new ArrayList<>();
        while (preRs.next()) preAddressIdList.add(preRs.getString(1));
        preSt.close();

        int uniqueAddrsCnt = 0;
        int uniqueAddrsFullCnt = 0;
        long stopCount = CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + PORTION_ELEMENTS_AMOUNT;

        Statement st = connection.createStatement();

        List<String> idsToProcess = new ArrayList<>();
        List<String> personIdsList = new ArrayList<>();
        List<String> uniqueIdsFull = new ArrayList<>();
        LinkedHashMap<String, List<AddressWrapper>> addrLinesMap = new LinkedHashMap<>();
        List<String> filteredIdList = IRsmuVectorSyncDaemonDao.instance.get().getFilteredIdList(preAddressIdList, AddressString.class, AddressString.ENTITY_NAME);

        st.execute("select AddressId, PersonId, TypeAddressId, PostIndex, Region, Area, City, StreetHouse " +
                "from AddressPerson where personId in (select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1) order by PersonId");

        ResultSet rs = st.getResultSet();

        while (rs.next())
        {
            String vectorId = rs.getString(1);
            if (!uniqueIdsFull.contains(vectorId))
            {
                uniqueIdsFull.add(vectorId);
                uniqueAddrsFullCnt++;
            }

            // Если отфильтрованный список содержит соответствующий идентификатор, то игнорируем (сущность актуальная, с момента обновления прошло меньше суток)
            if (null == vectorId || !filteredIdList.contains(vectorId)) continue;

            AddressWrapper wrapper = new AddressWrapper(rs);
            personIdsList.add(wrapper.getPersonId());

            if (!idsToProcess.contains(wrapper.getVectorId()))
            {
                idsToProcess.add(wrapper.getVectorId());
                uniqueAddrsCnt++;
            }

            List<AddressWrapper> addresses = addrLinesMap.get(wrapper.getVectorId());
            if (null == addresses) addresses = new ArrayList<>();
            addresses.add(wrapper);
            addrLinesMap.put(wrapper.getVectorId(), addresses);
        }

        st.close();

        CURRENT_CATALOG = CATALOG_ADDRESS;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = uniqueAddrsFullCnt;
        if (0 == CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = uniqueAddrsCnt;

        List<String> idsToProcessPortion = idsToProcess.subList(0, Long.valueOf(Math.min(idsToProcess.size(), PORTION_ELEMENTS_AMOUNT)).intValue());
        Map<String, CoreCollectionUtils.Pair<AddressString, RsmuVectorIds>> idsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsToProcessPortion, AddressString.class, AddressString.ENTITY_NAME);
        Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> personIdsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(personIdsList, Person.class, Person.ENTITY_NAME);
        Map<String, Person> addressIdToPersonMap = new HashMap<>();

        List<String> regAddrIdsPortion = new ArrayList<>();
        List<String> factAddrIdsPortion = new ArrayList<>();

        for (String vectorId : idsToProcessPortion)
        {
            List<AddressWrapper> items = addrLinesMap.get(vectorId);

            if (null != items && !items.isEmpty())
            {
                AddressWrapper wrapper = AddressWrapper.getTheBest(items);
                if ("1".equals(wrapper.getType())) factAddrIdsPortion.add(vectorId);
                else regAddrIdsPortion.add(vectorId);

                CoreCollectionUtils.Pair<AddressString, RsmuVectorIds> idsPair = idsMap.get(vectorId);
                RsmuVectorIds ids = null != idsPair ? idsPair.getY() : null;

                CoreCollectionUtils.Pair<Person, RsmuVectorIds> personPair = personIdsMap.get(wrapper.getPersonId());
                if (null != personPair && null != personPair.getX())
                    addressIdToPersonMap.put(wrapper.getVectorId(), personPair.getX());

                if (null == ids)
                {
                    ids = new RsmuVectorIds();
                    ids.setSyncTime(new Date());
                    ids.setVectorId(wrapper.getVectorId());
                    ids.setEntityType(AddressString.ENTITY_NAME);
                }

                logEvent(Level.INFO, wrapper.toString());
                AddressString address = null != idsPair && null != idsPair.getX() ? idsPair.getX() : null;
                if (null == address) address = wrapper.generateAddress();
                else wrapper.updateAddress(address);

                idsPair = new CoreCollectionUtils.Pair(address, ids);
                idsMap.put(vectorId, idsPair);
            }
        }

        try
        {
            IRsmuVectorSyncDaemonDao.instance.get().saveAddressList(regAddrIdsPortion, idsMap, addressIdToPersonMap, true);
            IRsmuVectorSyncDaemonDao.instance.get().saveAddressList(factAddrIdsPortion, idsMap, addressIdToPersonMap, false);
        } catch (Exception e)
        {
            logEvent(Level.ERROR, "++++++++++ Saving Address Error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }

        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = Math.min(stopCount, CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED);
        logEvent(Level.INFO, "------ Addresses processed = " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + " / " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED + " (" + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL + ")");
        if (CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED >= CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
        {
            CATALOGS_TO_SYNC_QUEUE.remove(CURRENT_CATALOG);
            //CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;
        }

        logEvent(Level.INFO, "========= SYNC PERSON ADDRESS CATALOG WAS FINISHED =========");
    }

    protected static void processContactsCatalog(Connection connection) throws Throwable
    {
        logEvent(Level.INFO, "========= SYNC PERSON CONTACTS CATALOG HAS STARTED =========");

        Statement preSt = connection.createStatement();
        preSt.execute("select PersonId from telephonePerson where PersonId in (" +
                "select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1) order by PersonId");

        ResultSet preRs = preSt.getResultSet();
        List<String> prePersonIdList = new ArrayList<>();
        while (preRs.next()) prePersonIdList.add(preRs.getString(1));
        preSt.close();

        Statement preSt1 = connection.createStatement();
        preSt1.execute("select PersonId from Person_EMail where PersonId in (" +
                "select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1) order by PersonId");

        ResultSet preRs1 = preSt1.getResultSet();
        while (preRs1.next()) prePersonIdList.add(preRs1.getString(1));
        preSt1.close();


        int uniqueAddrsCnt = 0;
        int uniqueAddrsFullCnt = 0;
        long stopCount = CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + PORTION_ELEMENTS_AMOUNT;

        List<String> idsToProcess = new ArrayList<>();
        List<String> uniqueIdsFull = new ArrayList<>();
        LinkedHashMap<String, ContactWrapper> contatsMap = new LinkedHashMap<>();
        List<String> filteredIdList = IRsmuVectorSyncDaemonDao.instance.get().getFilteredIdList(prePersonIdList, PersonContactData.class, PersonContactData.ENTITY_NAME);
        List<String> filteredPersonsList = IRsmuVectorSyncDaemonDao.instance.get().getPersonVectorIdsList(prePersonIdList);

        Statement st = connection.createStatement();
        st.execute("select PersonId, TypeTelephoneID, TelephoneNumber from telephonePerson where PersonId in (" +
                "select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1) order by PersonId");

        ResultSet rs = st.getResultSet();

        while (rs.next())
        {
            String vectorId = rs.getString(1);

            if (!uniqueIdsFull.contains(vectorId))
            {
                uniqueIdsFull.add(vectorId);
                uniqueAddrsFullCnt++;
            }

            // Если отфильтрованный список содержит соответствующий идентификатор, то игнорируем (сущность актуальная, с момента обновления прошло меньше суток)
            if (null == vectorId || !filteredIdList.contains(vectorId) || !filteredPersonsList.contains(vectorId))
                continue;

            ContactWrapper wrapper = contatsMap.get(vectorId);
            if (null == wrapper) wrapper = new ContactWrapper(rs);
            wrapper.parsePhone(rs);
            contatsMap.put(wrapper.getPersonId(), wrapper);

            if (!idsToProcess.contains(wrapper.getPersonId()))
            {
                idsToProcess.add(wrapper.getPersonId());
                uniqueAddrsCnt++;
            }
        }

        Statement stMail = connection.createStatement();
        stMail.execute("select PersonId, EMail from Person_EMail where PersonId in (" +
                "select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1) order by PersonId");
        ResultSet rsMail = stMail.getResultSet();

        while (rsMail.next())
        {
            String vectorId = rsMail.getString(1);

            if (!uniqueIdsFull.contains(vectorId))
            {
                uniqueIdsFull.add(vectorId);
                uniqueAddrsFullCnt++;
            }

            // Если отфильтрованный список содержит соответствующий идентификатор, то игнорируем (сущность актуальная, с момента обновления прошло меньше суток)
            if (null == vectorId || !filteredIdList.contains(vectorId) || !filteredPersonsList.contains(vectorId))
                continue;

            ContactWrapper wrapper = contatsMap.get(vectorId);
            if (null == wrapper) wrapper = new ContactWrapper(rsMail);
            wrapper.parseEmail(rsMail);
            contatsMap.put(wrapper.getPersonId(), wrapper);

            if (!idsToProcess.contains(wrapper.getPersonId()))
            {
                idsToProcess.add(wrapper.getPersonId());
                uniqueAddrsCnt++;
            }
        }

        st.close();
        stMail.close();

        CURRENT_CATALOG = CATALOG_CONTACT_DATA;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = uniqueAddrsFullCnt;
        if (0 == CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = uniqueAddrsCnt;

        List<String> idsToProcessPortion = idsToProcess.subList(0, Long.valueOf(Math.min(idsToProcess.size(), PORTION_ELEMENTS_AMOUNT)).intValue());
        Map<String, CoreCollectionUtils.Pair<PersonContactData, RsmuVectorIds>> idsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsToProcessPortion, PersonContactData.class, PersonContactData.ENTITY_NAME);
        Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> personIdsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsToProcessPortion, Person.class, Person.ENTITY_NAME);

        for (String vectorId : idsToProcessPortion)
        {
            if (null == personIdsMap.get(vectorId)) continue;

            ContactWrapper wrapper = contatsMap.get(vectorId);

            if (null != wrapper)
            {
                CoreCollectionUtils.Pair<PersonContactData, RsmuVectorIds> idsPair = idsMap.get(vectorId);
                RsmuVectorIds ids = null != idsPair ? idsPair.getY() : null;

                if (null == ids)
                {
                    ids = new RsmuVectorIds();
                    ids.setSyncTime(new Date());
                    ids.setVectorId(wrapper.getPersonId());
                    ids.setEntityType(PersonContactData.ENTITY_NAME);
                }

                logEvent(Level.INFO, wrapper.toString());
                Person person = personIdsMap.get(vectorId).getX();
                if (null == person)
                {
                    logEvent(Level.ERROR, "!!!!!!!!!!!! Address error. Person " + vectorId + " not found in database.");
                    continue;
                }
                PersonContactData contact = null != idsPair && null != idsPair.getX() ? idsPair.getX() : null;
                if (null == contact) contact = person.getContactData();
                wrapper.updatePersonContactData(contact);

                idsPair = new CoreCollectionUtils.Pair(contact, ids);
                idsMap.put(vectorId, idsPair);
            }
        }

        try
        {
            IRsmuVectorSyncDaemonDao.instance.get().saveContactsList(idsToProcessPortion, idsMap);
        } catch (Exception e)
        {
            logEvent(Level.ERROR, "++++++++++ Saving Contacts Error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }

        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = Math.min(stopCount, CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED);
        logEvent(Level.INFO, "------ Contacts processed = " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + " / " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED + " (" + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL + ")");
        if (CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED >= CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
        {
            CATALOGS_TO_SYNC_QUEUE.remove(CURRENT_CATALOG);
            //CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;
        }

        logEvent(Level.INFO, "========= SYNC PERSON CONTACTS CATALOG WAS FINISHED =========");
    }

    protected static void processPhotoCatalog(Connection connection) throws Throwable
    {
        logEvent(Level.INFO, "========= SYNC PERSON PHOTO CATALOG HAS STARTED =========");

        Statement st = connection.createStatement();
        st.execute("select PersonId from PhotoPerson where Photo is not null and " +
                "personId in (select p.PersonID from Person p inner join Student s on p.PersonId=S.StudentId " +
                "inner join AgreementStudent a on s.StudentId=a.StudentId where a.ActiveAgreement=1) order by PersonId");

        int uniquePhotCnt = 0;
        int uniquePhotFullCnt = 0;
        long stopCount = CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + 100;

        ResultSet rs = st.getResultSet();
        List<String> idsToProcess = new ArrayList<>();
        while (rs.next())
        {
            idsToProcess.add(rs.getString(1));
        }

        List<String> uniqueIdsFull = new ArrayList<>();
        List<String> filteredIdList = IRsmuVectorSyncDaemonDao.instance.get().getFilteredIdList(idsToProcess, DatabaseFile.class, DatabaseFile.ENTITY_NAME);
        List<String> filteredPersonsList = IRsmuVectorSyncDaemonDao.instance.get().getPersonVectorIdsList(idsToProcess);

        List<String> reFilteredIdList = new ArrayList<>();
        for (String vectorId : idsToProcess)
        {
            if (!uniqueIdsFull.contains(vectorId))
            {
                uniqueIdsFull.add(vectorId);
                uniquePhotFullCnt++;
            }

            // Если отфильтрованный список содержит соответствующий идентификатор, то игнорируем (сущность актуальная, с момента обновления прошло меньше суток)
            if (null != vectorId && filteredIdList.contains(vectorId) && filteredPersonsList.contains(vectorId))
            {
                reFilteredIdList.add(vectorId);
                uniquePhotCnt++;
            }
        }


        List<String> idsPortion = reFilteredIdList.subList(0, Long.valueOf(Math.min(reFilteredIdList.size(), 100)).intValue());

        StringBuilder portionQueryBuilder = new StringBuilder();
        for (String perId : idsPortion)
        {
            portionQueryBuilder.append(portionQueryBuilder.length() > 0 ? ", " : "").append("'").append(perId).append("'");
        }
        portionQueryBuilder.insert(0, "select PersonId, Photo from PhotoPerson where PersonId in (");
        portionQueryBuilder.append(") order by PersonId");

        if (idsPortion.isEmpty())
            portionQueryBuilder = new StringBuilder("select PersonId, Photo from PhotoPerson where PersonId='----'");


        st.execute(portionQueryBuilder.toString());

        ResultSet rs1 = st.getResultSet();
        LinkedHashMap<String, List<PhotoWrapper>> photLinesMap = new LinkedHashMap<>();

        while (rs1.next())
        {
            PhotoWrapper wrapper = new PhotoWrapper(rs1);
            List<PhotoWrapper> photos = photLinesMap.get(wrapper.getPersonId());
            if (null == photos) photos = new ArrayList<>();
            photos.add(wrapper);
            photLinesMap.put(wrapper.getPersonId(), photos);
        }

        st.close();

        CURRENT_CATALOG = CATALOG_PHOTO;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = uniquePhotFullCnt;
        if (0 == CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = uniquePhotCnt;

        Map<String, CoreCollectionUtils.Pair<DatabaseFile, RsmuVectorIds>> idsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsPortion, DatabaseFile.class, DatabaseFile.ENTITY_NAME);
        Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> personIdsMap = IRsmuVectorSyncDaemonDao.instance.get().getIdsMap(idsPortion, Person.class, Person.ENTITY_NAME);
        Map<String, Person> personMap = new HashMap<>();

        for (String personId : idsPortion)
        {
            List<PhotoWrapper> items = photLinesMap.get(personId);

            if (null != items && !items.isEmpty())
            {
                PhotoWrapper wrapper = PhotoWrapper.getTheBest(items);

                CoreCollectionUtils.Pair<DatabaseFile, RsmuVectorIds> idsPair = idsMap.get(personId);
                RsmuVectorIds ids = null != idsPair ? idsPair.getY() : null;

                CoreCollectionUtils.Pair<Person, RsmuVectorIds> personPair = personIdsMap.get(wrapper.getPersonId());
                if (null != personPair && null != personPair.getX())
                    personMap.put(wrapper.getPersonId(), personPair.getX());

                if (null == ids)
                {
                    ids = new RsmuVectorIds();
                    ids.setSyncTime(new Date());
                    ids.setVectorId(wrapper.getPersonId());
                    ids.setEntityType(DatabaseFile.ENTITY_NAME);
                }

                logEvent(Level.INFO, wrapper.toString());
                DatabaseFile photo = null != idsPair && null != idsPair.getX() ? idsPair.getX() : null;
                if (null == photo) photo = wrapper.generatePhoto();
                else wrapper.updatePhoto(photo);

                idsPair = new CoreCollectionUtils.Pair(photo, ids);
                idsMap.put(personId, idsPair);
            }
        }

        try
        {
            IRsmuVectorSyncDaemonDao.instance.get().savePhotosList(idsPortion, idsMap, personMap);
        } catch (Exception e)
        {
            logEvent(Level.ERROR, "++++++++++ Saving Photos Error: " + e.getMessage());
            e.printStackTrace();
            throw e;
        }


        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = Math.min(stopCount, CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED);
        logEvent(Level.INFO, "------ Photos processed = " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + " / " + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED + " (" + CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL + ")");
        if (CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED >= CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED)
        {
            CATALOGS_TO_SYNC_QUEUE.remove(CURRENT_CATALOG);
            //CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
            CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;
        }

        logEvent(Level.INFO, "========= SYNC PERSON PHOTO CATALOG WAS FINISHED =========");
    }

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IRsmuVectorSyncDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    /**
     * Регистрирует синхронизацию справочника и вызывает демона
     *
     * @param catalogName - идентификатор справочника
     */
    public static void registerCatalogToSyncAndWakeUpDaemon(String catalogName)
    {
        if (null == StringUtils.trimToNull(catalogName)) return;
        if (!CATALOGS_TO_SYNC_QUEUE.contains(catalogName))
            CATALOGS_TO_SYNC_QUEUE.add(catalogName);
        DAEMON.wakeUpDaemon();
    }

    /**
     * Пинает демон для продолжения работы, снимая блокировки
     */
    public static void unlockAdnWakeUpDaemon()
    {
        sync_locked = false;
        DAEMON.wakeUpDaemon();
    }

    /**
     * Удаляет справочник из стоящих в очереди на синхронизацию
     *
     * @param catalogName - идентификатор справочника
     */
    public static void removeCatalogToSync(String catalogName)
    {
        CATALOGS_TO_SYNC_QUEUE.remove(catalogName);
    }

    /**
     * Удаляет справочник из стоящих в очереди на синхронизацию
     */
    public static void stopCatalogToSync()
    {
        CATALOGS_TO_SYNC_QUEUE.clear();
        CURRENT_CATALOG = null;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED = 0;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL = 0;
        CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED = 0;
    }

    /**
     * Возвращает true, если справочник синхронизируется в данный момент.
     *
     * @param catalogName - Идентификатор справочника
     * @return - true, если справочник синхронизируется в текущий момент
     */
    public static boolean isEntitySyncInProcess(String catalogName)
    {
        return null != CURRENT_CATALOG && CURRENT_CATALOG.equals(catalogName);
    }

    /**
     * Возвращает true, если справочник синхронизируется в данный момент.
     *
     * @return - true, если справочник синхронизируется в текущий момент
     */
    public static boolean isAnyCatalogSyncInProcess()
    {
        return null != CURRENT_CATALOG && !CATALOGS_TO_SYNC_QUEUE.isEmpty();
    }

    @Override
    public void savePersonsList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> idsMap, boolean takeRealVectorIds)
    {
        int processed = 0;
        for (String vectorId : idsToProcessPortion)
        {
            CoreCollectionUtils.Pair<Person, RsmuVectorIds> pair = idsMap.get(vectorId);
            if (null == pair || (null == pair.getX() && takeRealVectorIds)) // Если пары нет, или отсутствует сам объект - исключительная ситуация
            {
                logEvent(Level.ERROR, "!!!!!! Object Person Id=" + vectorId + " to create was not found");
                continue;
            }

            Person person = pair.getX();
            RsmuVectorIds ids = pair.getY();

            // Если с момента обновления данных прошло меньше суток, то не обновляем
            if (null != ids && null != ids.getId() && System.currentTimeMillis() - ids.getSyncTime().getTime() < 86400000L)
                continue;

            IdentityCard card = person.getIdentityCard();
            getSession().saveOrUpdate(card);

            PersonContactData contactData = person.getContactData();
            if (null == contactData)
            {
                contactData = new PersonContactData();
                getSession().save(contactData);
            }

            person.setIdentityCard(card);
            person.setContactData(contactData);
            getSession().saveOrUpdate(person);

            card.setPerson(person);
            update(card);

            if (null != ids)
            {
                ids.setSyncTime(new Date());
                ids.setEntityId(person.getId());
                getSession().saveOrUpdate(ids);
            }

            processed++;
        }

        getSession().flush();
        getSession().clear();

        if (0 == processed) unlockAdnWakeUpDaemon();
    }

    @Override
    public void saveStudentsList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<Student, RsmuVectorIds>> idsMap, Map<String, StudentWrapper> studentWrappersMap)
    {
        //Попытаемся найти все связанные со студентом объекты, чтобы не созщдавать их с нуля.
        Set<Long> studentIdsSet = new HashSet<>();

        for (String vectorId : idsToProcessPortion)
        {
            CoreCollectionUtils.Pair<Student, RsmuVectorIds> pair = idsMap.get(vectorId);
            if (null != pair && null != pair.getX() && null != pair.getX().getId())
            {
                studentIdsSet.add(pair.getX().getId());
            }
        }

        //Ищем данные о приказах
        Map<Long, OrderData> orderDataMap = new HashMap<>();
        if (!studentIdsSet.isEmpty())
        {
            List<OrderData> orderDataList = new DQLSelectBuilder().fromEntity(OrderData.class, "e").column(property("e"))
                    .where(in(property(OrderData.student().id().fromAlias("e")), studentIdsSet))
                    .createStatement(getSession()).list();

            for (OrderData data : orderDataList) orderDataMap.put(data.getStudent().getId(), data);
        }

        //Ищем последний приказ
        Map<Long, OtherStudentExtract> ordersMap = new HashMap<>();
        if (!studentIdsSet.isEmpty())
        {
            List<OtherStudentExtract> orderList = new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "e").column(property("e"))
                    .fetchPath(DQLJoinType.inner, OtherStudentExtract.paragraph().order().fromAlias("e"))
                    .where(in(property(OtherStudentExtract.entity().id().fromAlias("e")), studentIdsSet))
                    .order(property(OtherStudentExtract.paragraph().order().commitDate().fromAlias("e")), OrderDirection.desc)
                    .createStatement(getSession()).list();

            for (OtherStudentExtract extract : orderList)
            {
                if (!orderDataMap.containsKey(extract.getEntity().getId()))
                    ordersMap.put(extract.getEntity().getId(), extract);
            }
        }

        int processed = 0;
        for (String vectorId : idsToProcessPortion)
        {
            CoreCollectionUtils.Pair<Student, RsmuVectorIds> pair = idsMap.get(vectorId);
            if (null == pair || null == pair.getX()) // Если пары нет, или отсутствует сам объект - исключительная ситуация
            {
                logEvent(Level.ERROR, "!!!!!! Object Student Id=" + vectorId + " to create was not found");
                continue;
            }

            Student student = pair.getX();
            RsmuVectorIds ids = pair.getY();

            // Если с момента обновления данных прошло меньше суток, то не обновляем
            if (null != ids && null != ids.getId() && System.currentTimeMillis() - ids.getSyncTime().getTime() < 86400000L)
                continue;

            if (null == student.getId() && null != student.getPrincipal())
                getSession().saveOrUpdate(student.getPrincipal());

            getSession().saveOrUpdate(student);

            StudentWrapper wrapper = studentWrappersMap.get(vectorId);

            if (null != wrapper)
            {
                OrderData data = orderDataMap.get(student.getId());
                if (null == data)
                {
                    data = new OrderData();
                    data.setStudent(student);
                }

                data.setEduEnrollmentOrderDate(wrapper.getEnrOrderDate());
                data.setEduEnrollmentOrderNumber(wrapper.getEnrOrderNumber());
                if(null != wrapper.getEnrOrderEnrDate())
                    data.setEduEnrollmentOrderEnrDate(wrapper.getEnrOrderEnrDate());
                else
                    data.setEduEnrollmentOrderEnrDate(wrapper.getEnrOrderDate());

                getSession().saveOrUpdate(data);


                OtherStudentExtract extract = ordersMap.get(student.getId());
                if (null == extract && null != wrapper.getLastOrderNumber() && null != wrapper.getLastOrderDate())
                {
                    StudentOtherOrder order = new StudentOtherOrder();
                    order.setCommitDate(wrapper.getLastOrderDate());
                    order.setCommitDateSystem(wrapper.getLastOrderDate());
                    order.setCreateDate(wrapper.getLastOrderDate());
                    order.setNumber(wrapper.getLastOrderNumber());
                    order.setState(RsmuVectorDataHolder.getOrderState());

                    StudentOtherParagraph paragraph = new StudentOtherParagraph();
                    paragraph.setOrder(order);
                    paragraph.setNumber(1);

                    extract = new OtherStudentExtract();
                    extract.setCreateDate(wrapper.getLastOrderDate());
                    extract.setParagraph(paragraph);
                    extract.setEntity(student);
                    extract.setCommitted(true);
                    extract.setStudentTitle(student.getPerson().getFullFio());
                    extract.setStudentTitleStr(student.getPerson().getFullFio());
                    extract.setStudentStatusStr(student.getStatus().getTitle());
                    extract.setCourse(student.getCourse());
                    extract.setPersonalNumberStr(student.getPerNumber());
                    extract.setCourseStr(student.getCourse().getTitle());
                    extract.setCompensationTypeStr(student.getCompensationType().getTitle());
                    extract.setFormativeOrgUnitStr(student.getEducationOrgUnit().getFormativeOrgUnit().getTitle());
                    extract.setTerritorialOrgUnitStr(student.getEducationOrgUnit().getTerritorialOrgUnit().getTitle());
                    extract.setEducationLevelHighSchoolStr(student.getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
                    extract.setDevelopFormStr(student.getEducationOrgUnit().getDevelopForm().getTitle());
                    extract.setDevelopConditionStr(student.getEducationOrgUnit().getDevelopCondition().getTitle());
                    extract.setDevelopTechStr(student.getEducationOrgUnit().getDevelopTech().getTitle());
                    extract.setDevelopPeriodStr(student.getEducationOrgUnit().getDevelopPeriod().getTitle());
                    extract.setState(RsmuVectorDataHolder.getExtractState());
                }

                if (null != extract && null != wrapper.getLastOrderNumber() && null != wrapper.getLastOrderDate())
                {
                    extract.setType(RsmuVectorDataHolder.getExtractType(wrapper.getLastOrderTypeName()));
                    StudentOtherOrder order = (StudentOtherOrder) extract.getParagraph().getOrder();
                    order.setCommitDate(wrapper.getLastOrderDate());
                    order.setCommitDateSystem(wrapper.getLastOrderDate());
                    order.setCreateDate(wrapper.getLastOrderDate());
                    order.setNumber(wrapper.getLastOrderNumber());

                    getSession().saveOrUpdate(order);
                    getSession().saveOrUpdate(extract.getParagraph());
                    getSession().saveOrUpdate(extract);

                    extract.setState(RsmuVectorDataHolder.getExtractState());
                    order.setState(RsmuVectorDataHolder.getOrderState());
                    getSession().update(extract);
                    getSession().update(order);
                }
            }

            if (null != ids)
            {
                ids.setSyncTime(new Date());
                ids.setEntityId(student.getId());
                getSession().saveOrUpdate(ids);
            }

            processed++;
        }

        getSession().flush();
        getSession().clear();

        if (0 == processed) unlockAdnWakeUpDaemon();
    }


    @Override
    public void saveAddressList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<AddressString, RsmuVectorIds>> idsMap, Map<String, Person> addressIdToPersonMap, boolean regAddr)
    {
        int processed = 0;
        for (String vectorId : idsToProcessPortion)
        {
            CoreCollectionUtils.Pair<AddressString, RsmuVectorIds> pair = idsMap.get(vectorId);
            if (null == pair || null == pair.getX())
            {
                logEvent(Level.ERROR, "!!!!!! Object Address Id=" + vectorId + " to create was not found");
                continue;
            }

            AddressString addr = pair.getX();
            RsmuVectorIds ids = pair.getY();

            // Если с момента обновления данных прошло меньше суток, то не обновляем
            if (null != ids && null != ids.getId() && System.currentTimeMillis() - ids.getSyncTime().getTime() < 86400000L)
                continue;

            Person person = addressIdToPersonMap.get(vectorId);
            if (null == person) continue;

            getSession().saveOrUpdate(addr);

            if (regAddr)
            {
                person.getIdentityCard().setAddress(addr);
                getSession().update(person.getIdentityCard());
            } else
            {
                person.setAddress(pair.getX());
                getSession().saveOrUpdate(person);
            }

            ids.setSyncTime(new Date());
            ids.setEntityId(addr.getId());
            getSession().saveOrUpdate(ids);

            processed++;
        }

        getSession().flush();
        getSession().clear();

        if (0 == processed) unlockAdnWakeUpDaemon();
    }

    @Override
    public void saveContactsList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<PersonContactData, RsmuVectorIds>> idsMap)
    {
        int processed = 0;
        for (String vectorId : idsToProcessPortion)
        {
            CoreCollectionUtils.Pair<PersonContactData, RsmuVectorIds> pair = idsMap.get(vectorId);
            if (null == pair || null == pair.getX())
            {
                logEvent(Level.ERROR, "!!!!!! Object Contact Id=" + vectorId + " to create was not found");
                continue;
            }

            PersonContactData contact = pair.getX();
            RsmuVectorIds ids = pair.getY();

            // Если с момента обновления данных прошло меньше суток, то не обновляем
            if (null != ids && null != ids.getId() && System.currentTimeMillis() - ids.getSyncTime().getTime() < 86400000L)
                continue;

            getSession().saveOrUpdate(contact);

            ids.setSyncTime(new Date());
            ids.setEntityId(contact.getId());
            getSession().saveOrUpdate(ids);

            processed++;
        }

        getSession().flush();
        getSession().clear();

        if (0 == processed) unlockAdnWakeUpDaemon();
    }

    @Override
    public void savePhotosList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<DatabaseFile, RsmuVectorIds>> idsMap, Map<String, Person> personMap)
    {
        int processed = 0;
        for (String vectorId : idsToProcessPortion)
        {
            CoreCollectionUtils.Pair<DatabaseFile, RsmuVectorIds> pair = idsMap.get(vectorId);
            if (null == pair || null == pair.getX())
            {
                logEvent(Level.ERROR, "!!!!!! Object Photo Id=" + vectorId + " to create was not found");
                continue;
            }

            DatabaseFile file = pair.getX();
            RsmuVectorIds ids = pair.getY();

            // Если с момента обновления данных прошло меньше суток, то не обновляем
            if (null != ids && null != ids.getId() && System.currentTimeMillis() - ids.getSyncTime().getTime() < 86400000L)
                continue;

            Person person = personMap.get(vectorId);
            if (null == person)
            {
                logEvent(Level.ERROR, "!!!!!!!!!!!! Photo error. Person " + vectorId + " not found in database.");
                continue;
            }

            getSession().saveOrUpdate(file);

            person.getIdentityCard().setPhoto(file);
            getSession().update(person.getIdentityCard());

            ids.setSyncTime(new Date());
            ids.setEntityId(file.getId());
            getSession().saveOrUpdate(ids);

            processed++;
        }

        getSession().flush();
        getSession().clear();

        if (0 == processed) unlockAdnWakeUpDaemon();
    }

    @Override
    public <T extends IEntity> Map<String, CoreCollectionUtils.Pair<T, RsmuVectorIds>> getIdsMap(Collection<String> vectorIdCollection, Class<T> clazz, String entityType)
    {
        if (null == vectorIdCollection || vectorIdCollection.isEmpty()) return new HashMap<>();


        List<Object[]> items = new DQLSelectBuilder().fromEntity(RsmuVectorIds.class, "ids").column(property("ids")).column(property("e"))
                .joinEntity("ids", DQLJoinType.left, clazz, "e", eq(property(RsmuVectorIds.entityId().fromAlias("ids")), property("e", IEntity.P_ID)))
                .where(eq(property(RsmuVectorIds.entityType().fromAlias("ids")), value(entityType)))
                .where(in(property(RsmuVectorIds.vectorId().fromAlias("ids")), vectorIdCollection))
                .createStatement(getSession()).list();

        Map<String, CoreCollectionUtils.Pair<T, RsmuVectorIds>> idsMap = new HashMap<>();
        for (Object[] item : items)
        {
            T entity = (T) item[1];
            RsmuVectorIds ids = (RsmuVectorIds) item[0];
            idsMap.put(ids.getVectorId(), new CoreCollectionUtils.Pair(entity, ids));
            //if(null != entity) idsMap.put(String.valueOf(entity.getId()), new CoreCollectionUtils.Pair(entity, ids));
        }

        return idsMap;
    }

    @Override
    public <T extends IEntity> List<String> getFilteredIdList(List<String> vectorIdList, Class<T> clazz, String entityType)
    {
        if (null == vectorIdList || vectorIdList.isEmpty()) return new ArrayList<>();

        java.sql.Date prevDayDate = new java.sql.Date(CoreDateUtils.getNextDayFirstTimeMoment(new Date(), -1).getTime());

        List<String> items = new DQLSelectBuilder().fromEntity(RsmuVectorIds.class, "ids").column(property(RsmuVectorIds.vectorId().fromAlias("ids")))
                .joinEntity("ids", DQLJoinType.left, clazz, "e", eq(property(RsmuVectorIds.entityId().fromAlias("ids")), property("e", IEntity.P_ID)))
                .where(eq(property(RsmuVectorIds.entityType().fromAlias("ids")), value(entityType)))
                .where(ge(property(RsmuVectorIds.syncTime().fromAlias("ids")), value(prevDayDate)))
                        //.where(in(property(RsmuVectorIds.vectorId().fromAlias("ids")), vectorIdCollection))
                .createStatement(getSession()).list();

        List<String> resultList = new ArrayList<>();

        for (String vectorId : vectorIdList)
        {
            if (!items.contains(vectorId)) resultList.add(vectorId);
        }

        return resultList;
    }

    @Override
    public List<String> getPersonVectorIdsList(List<String> vectorIdList)
    {
        if (null != vectorIdList && vectorIdList.isEmpty()) return new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RsmuVectorIds.class, "ids").column(property(RsmuVectorIds.vectorId().fromAlias("ids")))
                .joinEntity("ids", DQLJoinType.left, Person.class, "e", eq(property(RsmuVectorIds.entityId().fromAlias("ids")), property(Person.id().fromAlias("e"))))
                .where(eq(property(RsmuVectorIds.entityType().fromAlias("ids")), value(Person.ENTITY_NAME)));

        if (null != vectorIdList) builder.where(in(property(RsmuVectorIds.vectorId().fromAlias("ids")), vectorIdList));
        List<String> resultList = builder.createStatement(getSession()).list();

        return resultList;
    }

    @Override
    public void updateIdentityCards(List<String> personList, Map<String, IdentityCard> icardMap)
    {
        for (String personId : personList)
        {
            IdentityCard card = icardMap.get(personId);
            if(null != card) getSession().saveOrUpdate(card);
        }
        getSession().flush();
    }

    @Override
    public void createAddresses(List<AddressBase> addressList)
    {
        for (AddressBase address : addressList) getSession().save(address);
    }

    @Override
    public void deleteAddresses(List<Long> addressList)
    {
        for (Long address : addressList) if(null != address) delete(address);
        getSession().flush();
    }
}