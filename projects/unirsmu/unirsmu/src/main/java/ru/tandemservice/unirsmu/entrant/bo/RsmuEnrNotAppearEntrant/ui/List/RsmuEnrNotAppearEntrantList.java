/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.CollectionFormatter;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic.RsmuEnrExamGroupDSHandler;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic.RsmuEnrNotAppearEntrantDSHandler;

/**
 * @author Andrey Avetisov
 * @since 09.02.2015
 */

@Configuration
public class RsmuEnrNotAppearEntrantList extends BusinessComponentManager
{
    public static final String ENTRANT_DS = "entrantDS";
    public static final String ENR_ORGUNIT_DS = "enrOrgUnitDS";
    public static final String ENR_EXAM_GROUP_DS = "enrExamGroupDS";
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";


    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(searchListDS(ENTRANT_DS, entrantDSColumns(), entrantDSHandler()))
                .addDataSource(selectDS(ENR_ORGUNIT_DS, enrOrgUnitDSHandler()).addColumn("departmentTitle", "departmentTitle"))
                .addDataSource(selectDS(ENR_EXAM_GROUP_DS, enrExamGroupSetDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint entrantDSColumns()
    {
        return columnListExtPointBuilder(ENTRANT_DS)
                .addColumn(checkboxColumn("checkbox"))
                .addColumn(textColumn("personalNumber", EnrEntrant.personalNumber()).order())
                .addColumn(publisherColumn("requestNumber", "title")
                                   .entityListProperty(EnrEntrantDSHandler.VIEW_PROP_REQUEST)
                                   .formatter(CollectionFormatter.COLLECTION_FORMATTER))
                .addColumn(publisherColumn("entrant", PersonRole.person().identityCard().fullFio()).required(Boolean.TRUE).parameters("mvel:['selectedTab':'requestTab']").order())
                .addColumn(textColumn("state", EnrEntrant.state().stateDaemonSafe().s()))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler entrantDSHandler()
    {
        return new RsmuEnrNotAppearEntrantDSHandler(getName());
    }

    @Bean
    IDefaultComboDataSourceHandler enrOrgUnitDSHandler ()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
                .where(EnrOrgUnit.enrollmentCampaign(), ENROLLMENT_CAMPAIGN);
    }

    @Bean
    IDefaultComboDataSourceHandler enrExamGroupSetDSHandler ()
    {
        return new RsmuEnrExamGroupDSHandler(getName());
    }


}
