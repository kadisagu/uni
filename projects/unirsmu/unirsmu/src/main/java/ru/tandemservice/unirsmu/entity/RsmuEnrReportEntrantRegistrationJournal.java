package ru.tandemservice.unirsmu.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unirsmu.entity.gen.RsmuEnrReportEntrantRegistrationJournalGen;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantRegistrationJournalAdd.RsmuEnrReportEntrantRegistrationJournalAdd;

import java.util.Arrays;
import java.util.List;

/**
 * Журнал регистрации абитуриентов (РНИМУ)
 */
public class RsmuEnrReportEntrantRegistrationJournal extends RsmuEnrReportEntrantRegistrationJournalGen implements IEnrReport
{
    public static final String REPORT_KEY = "rsmuEnr14ReportEntrantRegistrationJournal";

    private static List<String> properties = Arrays.asList(
            P_REQUEST_TYPE,
            P_COMPENSATION_TYPE,
            P_PROGRAM_FORM,
            P_COMPETITION_TYPE,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET,
            P_ENROLLMENT_COMMISSION,
            P_GROUP_BY_ENR_COMMISSION
    );

    @Override
    public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override
    public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc()
        {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return RsmuEnrReportEntrantRegistrationJournal.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return RsmuEnrReportEntrantRegistrationJournalAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Журнал регистрации абитуриентов (РНИМУ)»"; }
            @Override public String getListTitle() { return "Список отчетов «Журнал регистрации абитуриентов (РНИМУ)»"; }
        };
    }
}