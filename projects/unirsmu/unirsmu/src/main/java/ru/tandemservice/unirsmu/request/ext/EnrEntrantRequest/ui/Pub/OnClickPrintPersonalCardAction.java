/* $Id:$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.Pub;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.common.logging.entity.LogEvent;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniedu.catalog.entity.subjects.gen.EduProgramSpecializationGen;
import ru.tandemservice.unienr14.catalog.entity.*;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrExamPassFormCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.entity.*;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.*;
import ru.tandemservice.unienr14.request.entity.*;
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 26.06.2014
 */
public class OnClickPrintPersonalCardAction extends NamedUIAction
{
    Map<Long, List<EduProgramSpecialization>> specializationMap;

    public OnClickPrintPersonalCardAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        Long currentEntrantRequestId = presenter.getListenerParameterAsLong();
        if (null == currentEntrantRequestId)
            return;

        EnrEntrantRequest currentEntrantRequest = DataAccessServices.dao().getNotNull(currentEntrantRequestId);
        if (null == currentEntrantRequest)
            return;

        EnrScriptItem template = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REQUEST_ENTRANT_PERSONAL_CARD);
        if (null == template)
            return;

        IdentityCard identityCard = currentEntrantRequest.getIdentityCard();
        EnrEntrant enrEntrant = currentEntrantRequest.getEntrant();
        Person person = enrEntrant.getPerson();

        final RtfDocument document = new RtfReader().read(template.getCurrentTemplate());
        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();
        List<String> deleteLabels = Lists.newArrayList();

        ArrayList<String[]> rowsT1;
        rowsT1 = new ArrayList<>();
        ArrayList<String[]> rowsT2;
        rowsT2 = new ArrayList<>();

        Set<String> competitionExclusiveSet = new TreeSet<>();
        Set<String> benefitDocSet = new TreeSet<>();

        // собираем особые права
        List<EnrBenefitCategory> benefitCategoryExclList = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetitionExclusive.class, "erce")
                        .column(property("erce", EnrRequestedCompetitionExclusive.benefitCategory()))
                        .predicate(DQLPredicateType.distinct)
                        .where(eq(property("erce", EnrRequestedCompetitionExclusive.L_ENTRANT), value(enrEntrant)))
                        .order(property("erce", EnrRequestedCompetitionExclusive.benefitCategory().title()))
        );

        for (EnrBenefitCategory benefitCategory : benefitCategoryExclList)
        {
            List<EnrEntrantBenefitProof> benefitProofList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder()
                            .fromEntity(EnrEntrantBenefitProof.class, "eebp")
                            .column("eebp")
                            .where(eq(property("eebp", EnrEntrantBenefitProof.benefitStatement().benefitCategory()), value(benefitCategory)))
                            .where(eq(property("eebp", EnrEntrantBenefitProof.document().entrant()), value(enrEntrant)))
            );

            if (!benefitProofList.isEmpty())
            {
                competitionExclusiveSet.add("Преим." + benefitCategory.getShortTitle());
                for (EnrEntrantBenefitProof benefitProof : benefitProofList)
                {
                    String title;
                    IEnrEntrantBenefitProofDocument doc = benefitProof.getDocument();
                    if (doc instanceof EnrEntrantBaseDocument)
                    {
                        EnrEntrantBaseDocument customDoc = (EnrEntrantBaseDocument) doc;
                        title = customDoc.getDocumentType().getShortTitle() + (null != customDoc.getSeria() ? " " + customDoc.getSeria() : "") +
                                (null != customDoc.getNumber() ? " " + customDoc.getNumber() : "") + ", выдан: " +
                                (StringUtils.isEmpty(customDoc.getIssuancePlace()) ? "" : customDoc.getIssuancePlace()) +
                                (null != customDoc.getIssuanceDate() ? " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(customDoc.getIssuanceDate()) : "") +
                                (null != customDoc.getDocRelation().getDocument().getExpirationDate() ? ", действителен до: " + DateFormatter.DEFAULT_DATE_FORMATTER.format(customDoc.getDocRelation().getDocument().getExpirationDate()) : "");
                    }
                    else
                        title = doc.getTitle();
                    benefitDocSet.add(title);
                }
            }
        }

        // собираем без ВИ
        List<EnrBenefitCategory> benefitCategoryNoExamList = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrRequestedCompetitionNoExams.class, "ercne")
                        .column(property("ercne", EnrRequestedCompetitionNoExams.benefitCategory()))
                        .predicate(DQLPredicateType.distinct)
                        .where(eq(property("ercne", EnrRequestedCompetitionNoExams.L_ENTRANT), value(enrEntrant)))
                        .order(property("ercne", EnrRequestedCompetitionNoExams.benefitCategory().title()))
        );

        for (EnrBenefitCategory benefitCategory : benefitCategoryNoExamList)
        {
            List<EnrEntrantBenefitProof> benefitProofList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder()
                            .fromEntity(EnrEntrantBenefitProof.class, "eebp")
                            .column("eebp")
                            .where(eq(property("eebp", EnrEntrantBenefitProof.benefitStatement().benefitCategory()), value(benefitCategory)))
                            .where(eq(property("eebp", EnrEntrantBenefitProof.document().entrant()), value(enrEntrant)))
            );

            if (!benefitProofList.isEmpty())
            {
                competitionExclusiveSet.add("Преим.без ВИ " + benefitCategory.getShortTitle());
                for (EnrEntrantBenefitProof benefitProof : benefitProofList)
                {
                    String title;
                    IEnrEntrantBenefitProofDocument doc = benefitProof.getDocument();
                    if (doc instanceof EnrEntrantBaseDocument)
                    {
                        EnrEntrantBaseDocument customDoc = (EnrEntrantBaseDocument) doc;
                        if (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(customDoc.getDocumentType().getCode()))
                        {
                            title = customDoc.getDocumentType().getShortTitle() + " " + (null != customDoc.getSeria() ? " " + customDoc.getSeria() : "") +
                                    (null != customDoc.getNumber() ? " " + customDoc.getNumber() : "");
                        }
                        else
                        {

                            title = customDoc.getDocumentType().getShortTitle() + (null != customDoc.getSeria() ? " " + customDoc.getSeria() : "") +
                                    (null != customDoc.getNumber() ? " " + customDoc.getNumber() : "") + ", выдан: " +
                                    (StringUtils.isEmpty(customDoc.getIssuancePlace()) ? "" : customDoc.getIssuancePlace()) +
                                    (null != customDoc.getIssuanceDate() ? " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(customDoc.getIssuanceDate()) : "")
                                    + (null != customDoc.getDocRelation().getDocument().getExpirationDate() ? ", действителен до: " + DateFormatter.DEFAULT_DATE_FORMATTER.format(customDoc.getDocRelation().getDocument().getExpirationDate()) : "");
                        }
                    }
                    else
                        title = doc.getTitle();
                    benefitDocSet.add(title);
                }
            }
        }

        // собираем ЕГЭ
        List<EnrEntrantStateExamResult> stateExamResultList = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrEntrantStateExamResult.class, "eeser")
                        .column("eeser")
                        .where(eq(property("eeser", EnrEntrantStateExamResult.L_ENTRANT), value(enrEntrant)))
        );

        Set<String> stateExamSet = new TreeSet<>();
        List<String> stateExamSubjectTitleList = new ArrayList<>();
        long sumEGE = 0;

        // собираем все заявления абитуриента
        List<EnrEntrantRequest> entrantRequestList = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrEntrantRequest.class, "er")
                        .column("er")
                        .where(eq(property("er", EnrEntrantRequest.L_ENTRANT), value(enrEntrant)))
                        .order(property("er", EnrEntrantRequest.P_REG_DATE))
        );

        // они обрабатывались этими пользователями
        List<String> logEventList = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(LogEvent.class, "le")
                        .column(property("le", LogEvent.P_PRINCIPAL_CONTEXT_TITLE))
                        .predicate(DQLPredicateType.distinct)
                        .where(in(property("le", LogEvent.P_ENTITY_ID), entrantRequestList))
        );

        Set<String> requestDateSet = new TreeSet<>();

        short i = 1;
        // и по каждому из заявлений

        final DQLSelectBuilder ratingBuilder = new DQLSelectBuilder().fromEntity(EnrRatingItem.class, "eri")
                .top(1)
                .where(DQLExpressions.eq(DQLExpressions.property("eri", EnrRatingItem.requestedCompetition().request().entrant()), DQLExpressions.value(enrEntrant)))
                .order(DQLExpressions.property("eri", EnrRatingItem.requestedCompetition().priority()));
        final List<EnrRatingItem> list = DataAccessServices.dao().getList(ratingBuilder);
        EnrRatingItem ratingItem = list.isEmpty() ? null : list.get(0);
        final long markAsLong = ratingItem != null ? ratingItem.getAchievementMarkAsLong() : 0L;

        for (EnrEntrantRequest entrantRequest : entrantRequestList)
        {
            requestDateSet.add(DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRequest.getRegDate()));
            // по выбранному конкурсу
            List<EnrRequestedCompetition> requestedCompetitionList = DataAccessServices.dao().getList(
                    new DQLSelectBuilder()
                            .fromEntity(EnrRequestedCompetition.class, "erc")
                            .column(property("erc"))
                            .where(eq(property("erc", EnrRequestedCompetition.L_REQUEST), value(entrantRequest)))
                            .order(property("erc", EnrRequestedCompetition.priority()))
            );

            for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList)
            {
                fillCompetitions(requestedCompetitionList, entrantRequest, tm);
                String compCode = requestedCompetition.getCompetition().getType().getCode();
                // целевой прием
                if (EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(compCode) ||
                        EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(compCode))
                {
                    List<EnrRequestedCompetitionTA> requestedCompetitionTAList = DataAccessServices.dao().getList(
                            new DQLSelectBuilder()
                                    .fromEntity(EnrRequestedCompetitionTA.class, "ercta")
                                    .column("ercta")
                                    .where(eq(property("ercta", EnrRequestedCompetitionTA.L_REQUEST), value(entrantRequest)))
                    );
                    for (EnrRequestedCompetitionTA competitionTA : requestedCompetitionTAList)
                        competitionExclusiveSet.add(competitionTA.getParametersTitle());
                }

                // формируем строку из восьми столбцов для T1
                String[] rowT1 = new String[6];

                rowT1[0] = String.valueOf(i++);

                EnrCompetition competition = requestedCompetition.getCompetition();
                rowT1[1] = competition.getProgramSetOrgUnit().getFormativeOrgUnit().getTitle();
                rowT1[2] = competition.getProgramSetOrgUnit().getProgramSet().getProgramSubject().getTitle();
                rowT1[3] = competition.getType().getCompensationType().getShortTitle();

                // все дисциплины внутри конкурса
                // с учетом ЕГЭ и олимпиад
                List<EnrChosenEntranceExam> chosenEntranceExamList = DataAccessServices.dao().getList(
                        new DQLSelectBuilder()
                                .fromEntity(EnrChosenEntranceExam.class, "ecee")
                                .column("ecee")
                                .fetchPath(DQLJoinType.inner, EnrChosenEntranceExam.maxMarkForm().markSource().fromAlias("ecee"), "eemsse")
                                .where(or(instanceOf("eemsse", EnrEntrantMarkSourceStateExam.class), instanceOf("eemsse", EnrEntrantMarkSourceBenefit.class)))
                                .where(eq(property("ecee", EnrChosenEntranceExam.L_REQUESTED_COMPETITION), value(requestedCompetition)))
                                .order(property("ecee", EnrChosenEntranceExam.discipline().id()))
                );

                boolean hasNotMark = false;
                long sum = markAsLong;
                // по всем ВВИ
                for (EnrChosenEntranceExam chosenEntranceExam : chosenEntranceExamList)
                {
                    EnrChosenEntranceExamForm maxMarkForm = chosenEntranceExam.getMaxMarkForm();
                    if (null != maxMarkForm && maxMarkForm.getMarkAsLong() > 0)
                    {
                        // есть балл - учитываем
                        Long mark = maxMarkForm.getMarkAsLong();
                        sum += mark;

                        Long markEGE = null;
                        boolean isOlymp = false;
                        boolean isEGE = true;
                        EnrStateExamSubject stateExamSubject = chosenEntranceExam.getDiscipline().getStateExamSubject();

                        if (chosenEntranceExam.getMarkSource() instanceof EnrEntrantMarkSourceBenefit)
                        {
                            // олимпиада в зачет?
                            EnrEntrantMarkSourceBenefit markSourceBenefit = (EnrEntrantMarkSourceBenefit) chosenEntranceExam.getMarkSource();
                            IEnrEntrantBenefitProofDocument doc = markSourceBenefit.getMainProof();
                            competitionExclusiveSet.add("Преим." + markSourceBenefit.getBenefitCategory().getShortTitle());
                            if (doc instanceof EnrEntrantBaseDocument)
                            {
                                if (PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.getDocumentType().getCode()))
                                {
                                    isOlymp = true;
                                    EnrOlympiadDiploma customDoc = (EnrOlympiadDiploma) doc;
                                    benefitDocSet.add(customDoc.getDocumentType().getShortTitle() + " " + customDoc.getSeriaAndNumber());
                                    // но в табличку выведем значение ЕГЭ, если это возможно...
                                    isEGE = false;
                                    for (EnrEntrantStateExamResult stateExamResult : stateExamResultList)
                                    {
                                        if (stateExamResult.getSubject().equals(stateExamSubject))
                                        {
                                            isEGE = true;
                                            markEGE = stateExamResult.getMarkAsLong();
                                            String documentNumber = stateExamResult.getDocumentNumber();
                                            stateExamSet.add((null != documentNumber ? documentNumber : "н/д") +
                                                    " (" + stateExamSubject.getTitle() + ")");
                                            break;
                                        }
                                    }
                                }
                                else
                                {

                                    EnrEntrantBaseDocument customDoc = (EnrEntrantBaseDocument) doc;
                                    benefitDocSet.add(customDoc.getDocumentType().getShortTitle() + " " +
                                            UniStringUtils.joinWithSeparator(" ", customDoc.getSeria(), customDoc.getNumber()));
                                }
                            }
                            else
                                benefitDocSet.add(doc.getExtendedTitle());
                        }
                        else
                        {
                            // ЕГЭ в зачет
                            for (EnrEntrantStateExamResult stateExamResult : stateExamResultList)
                            {
                                if (stateExamResult.getSubject().equals(stateExamSubject))
                                {
                                    String documentNumber = stateExamResult.getDocumentNumber();
                                    stateExamSet.add((null != documentNumber ? documentNumber : "н/д") +
                                            " (" + stateExamSubject.getTitle() + ")");
                                    break;
                                }
                            }
                        }

                        markEGE = (null == markEGE ? 0 : markEGE);
                        String title = (isOlymp && !isEGE ? chosenEntranceExam.getDiscipline().getDiscipline().getTitle() : stateExamSubject.getTitle());
                        if ((isEGE) && !stateExamSubjectTitleList.contains(title))
                        {
                            stateExamSubjectTitleList.add(title);
                            // формируем строку из пяти столбцов для T2
                            String[] rowT2 = new String[5];

                            rowT2[0] = title;
                            rowT2[1] = "";

                            sumEGE += (isOlymp && isEGE ? markEGE : mark) / 1000;
                            rowT2[2] = String.valueOf((isOlymp && isEGE ? markEGE : mark) / 1000);

                            rowT2[3] = "";
                            rowT2[4] = "";

                            rowsT2.add(rowT2);
                        }
                        else if ((isOlymp && !isEGE) && !stateExamSubjectTitleList.contains(title))
                        {
                            stateExamSubjectTitleList.add(title);
                            // формируем строку из пяти столбцов для T2
                            String[] rowT2 = new String[5];

                            rowT2[0] = title;
                            rowT2[1] = "";
                            rowT2[2] = "0";
                            rowT2[3] = "";
                            rowT2[4] = "+олимп";

                            rowsT2.add(rowT2);
                        }
                    }
                    else
                        hasNotMark = true;
                }

                rowT1[4] = (!hasNotMark && sum > 0 ? String.valueOf(sum / 1000) : "");

                Date regDate = requestedCompetition.getRegDate();
                rowT1[5] = (!regDate.equals(entrantRequest.getRegDate()) ? DateFormatter.DEFAULT_DATE_FORMATTER.format(regDate) : "");

                rowsT1.add(rowT1);
            }
        }

        // все дисциплины для сдачи внутри ОУ
        List<EnrExamPassDiscipline> examPassDisciplineList = DataAccessServices.dao().getList(
                new DQLSelectBuilder()
                        .fromEntity(EnrExamPassDiscipline.class, "eepd")
                        .column("eepd")
                        .where(eq(property("eepd", EnrExamPassDiscipline.L_ENTRANT), value(enrEntrant)))
        );

        boolean isInternalExam = false;
        long sumOU = 0;
        List<String> groupsTitle = new ArrayList<>();

        for (EnrExamPassDiscipline examPassDiscipline : examPassDisciplineList)
        {
            // формируем строку из пяти столбцов для T2
            String[] rowT2 = new String[5];

            EnrExamPassForm passForm = examPassDiscipline.getPassForm();

            rowT2[0] = examPassDiscipline.getDiscipline().getTitle() + " (" + passForm.getTitle() + ")";
            rowT2[1] = "";
            rowT2[2] = "";

            StringBuilder groups = new StringBuilder();
            EnrExamGroup examGroup = examPassDiscipline.getExamGroup();

            if (null != examGroup)
            {
                groupsTitle.add(examGroup.getTitle());
                groups.append(examGroup.getTitle())
                        .append(" (").append(passForm.getTitle()).append(")");
            }
            rowT2[3] = groups.toString();

            Long mark = examPassDiscipline.getMarkAsLong();
            if (null != mark && mark > 0)
            {
                sumOU += (mark / 1000);
                rowT2[4] = String.valueOf(mark / 1000);
            }
            else
                rowT2[4] = "н/д";

            rowsT2.add(rowT2);

            isInternalExam = isInternalExam || passForm.getCode().equals(EnrExamPassFormCodes.EXAM);
        }

        tm.put("T1", rowsT1.toArray(new String[rowsT1.size()][]));
        tm.put("T2", rowsT2.toArray(new String[rowsT2.size()][]));

        im.put("dateReq", StringUtils.join(requestDateSet.iterator(), ", "));
        im.put("printDate", DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()));

        String fio;
        fio = PersonManager.instance().declinationDao().getDeclinationFIO(identityCard, GrammaCase.NOMINATIVE);
        im.put("FIO", fio);
        im.put("entrantNumber", enrEntrant.getPersonalNumber());
        im.put("enrollmentCampaign", enrEntrant.getEnrollmentCampaign().getTitle());
        im.put("group", StringUtils.join(groupsTitle.iterator(), ","));
        im.put("birthDate", null != identityCard.getBirthDate() ? DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()) : "");
        im.put("sex", null != identityCard.getSex() ? identityCard.getSex().getShortTitle() : "");
        im.put("passport", identityCard.getTitle());
        im.put("citizenship", null != identityCard.getCitizenship() ? identityCard.getCitizenship().getFullTitle() : "");
        im.put("addressTitleWithFlat", null != person.getAddress() ? person.getAddress().getTitleWithFlat() : "");
        im.put("addressPhonesTitle", person.getContactData().getAllPhones(", "));
        im.put("certificate", currentEntrantRequest.getEduDocument().getTitleExtended() + (currentEntrantRequest.isEduInstDocOriginalHandedIn() ? "(оригинал)" : "(копия)"));
        if (!stateExamSet.isEmpty())
            im.put("resultEGE", StringUtils.join(stateExamSet.iterator(), ", "));
        else
            deleteLabels.add("resultEGE");
        if (!competitionExclusiveSet.isEmpty())
            im.put("competitionExclusive", StringUtils.join(competitionExclusiveSet.iterator(), "; "));
        else
            deleteLabels.add("competitionExclusive");
        if (!benefitDocSet.isEmpty())
            im.put("benefitDoc", StringUtils.join(benefitDocSet.iterator(), "; "));
        else
            deleteLabels.add("benefitDoc");

        im.put("averageAtt", PersonEduDocument.AVG_MARK_FORMATTER.format(currentEntrantRequest.getEduDocument().getAvgMarkAsLong()));
        im.put("sumEGE", (sumEGE > 0 ? String.valueOf(sumEGE) : ""));
        im.put("sumOU", (sumOU > 0 ? String.valueOf(sumOU) : ""));
        if (isInternalExam)
            im.put("internalExam", "Абитуриенту назначено сдавать экзамены в РНИМУ.");
        else
            deleteLabels.add("internalExam");
        im.put("operator", UniStringUtils.joinNotEmpty(logEventList, ", "));

        //
        putEntrantAchievement(currentEntrantRequest, im);
        //
        tm.modify(document);
        im.modify(document);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, deleteLabels, false, false);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Личная карточка " + fio + ".rtf").document(document), false);
    }


    private void fillCompetitions(List<EnrRequestedCompetition> requestedCompetitionList, EnrEntrantRequest entrantRequest, RtfTableModifier tm)
    {
        List<String[]> simpleRows = new ArrayList<>();
        int i = 0;
        for (EnrRequestedCompetition requestedCompetition : requestedCompetitionList) {
            if (!requestedCompetition.isAccepted()) {
                continue;
            }
            String[] row = {
                    String.valueOf(++i),
                    entrantRequest.getStringNumber(),
                    getProgramSubjectStr(requestedCompetition, requestedCompetitionList),
                    requestedCompetition.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm().getTitle().toLowerCase(),
                    getPlaces(requestedCompetition)
            };
            simpleRows.add(row);
        }
        if (CollectionUtils.isNotEmpty(simpleRows)) {
            String[][] stringArrays = new String[simpleRows.size()][];
            for (int j = 0; j < simpleRows.size(); j++) {
                stringArrays[j] = simpleRows.get(j);
            }
            tm.put("TA1", stringArrays);
        } else {
            tm.remove("TA1", 1, 0);
        }
    }

    private String getProgramSubjectStr(EnrRequestedCompetition requestedCompetition, List<EnrRequestedCompetition> requestedCompetitionList)
    {
        EnrProgramSetBase programSet = requestedCompetition.getCompetition().getProgramSetOrgUnit().getProgramSet();
        List<EduProgramSpecialization> specs = getRequestedProgramMap(requestedCompetitionList).get(requestedCompetition.getId());
        return programSet.getPrintTitle() + ((specs != null && !specs.isEmpty()) ? " (" + specs.stream().map(EduProgramSpecializationGen::getTitle).collect(Collectors.joining(", ")) + ")" : "");
    }

    private Map<Long, List<EduProgramSpecialization>> getRequestedProgramMap(List<EnrRequestedCompetition> requestedCompetitionList)
    {
        if (specializationMap == null) {
            specializationMap = SafeMap.get(ArrayList.class);
            DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "p")
                    .column(property("p"))
                    .where(DQLExpressions.in(property("p", EnrRequestedProgram.requestedCompetition()), requestedCompetitionList)))
                    .stream().forEach(o -> specializationMap.get(((EnrRequestedProgram) o).getRequestedCompetition().getId()).add(((EnrRequestedProgram) o).getProgramSetItem().getProgram().getProgramSpecialization()));

            specializationMap.entrySet().stream()
                    .filter(longListEntry -> longListEntry.getValue().size() == 1 && longListEntry.getValue().get(0).isRootSpecialization())
                    .forEach(longListEntry -> longListEntry.getValue().clear());
        }
        return specializationMap;
    }

    private String getPlaces(EnrRequestedCompetition requestedCompetition)
    {
        StringBuilder str = new StringBuilder();
        switch (requestedCompetition.getCompetition().getType().getCode()) {
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
            case EnrCompetitionTypeCodes.CONTRACT:
                str.append("по договорам об оказании платных образовательных услуг");
                break;
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
            case EnrCompetitionTypeCodes.MINISTERIAL:
                str.append("финансируемые из федерального бюджета");
                break;
            case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                str.append("в пределах квоты целевого приема");
                break;
            case EnrCompetitionTypeCodes.EXCLUSIVE:
                str.append("в пределах квоты приема лиц, имеющих особое право");
                break;
            default:
                return "";
        }
        if (requestedCompetition instanceof EnrRequestedCompetitionTA) {
            // Если конкурс по ЦП, дописываем название вида ЦП в скобках
            str.append(" (").append(((EnrRequestedCompetitionTA) requestedCompetition).getTargetAdmissionKind().getTargetAdmissionKind().getTitle()).append(")");
        }
        return str.toString();
    }


    private void putEntrantAchievement(EnrEntrantRequest entrantRequest, RtfInjectModifier im){
        EnrEntrant entrant = entrantRequest.getEntrant();
        EnrRequestType requestType = entrantRequest.getType();
        final ICommonDAO dao = DataAccessServices.dao();
        final String alias = "ea";

        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, alias)
                .where(eq(property(alias, EnrEntrantAchievement.entrant()), value(entrant)))
                .where(eq(property(alias, EnrEntrantAchievement.type().achievementKind().requestType()), value(requestType)));

        List<EnrEntrantAchievement> achievementList  = dao.getList(builder);
        if(achievementList.isEmpty())
        {
            im.put("entrantAchievementList", "нет");
            return;
        }

        RtfString rtfString = new RtfString();
        final Iterator<EnrEntrantAchievement> iterator = achievementList.iterator();
        while (iterator.hasNext()){
            final EnrEntrantAchievement achievement = iterator.next();
            final double mark = achievement.getRatingMarkAsLong() / 1000D;
            rtfString.append(achievement.getType().getTitle()).append("-").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(mark));
            if(iterator.hasNext())
                rtfString.par();
        }
        im.put("entrantAchievementList", rtfString);
    }
}