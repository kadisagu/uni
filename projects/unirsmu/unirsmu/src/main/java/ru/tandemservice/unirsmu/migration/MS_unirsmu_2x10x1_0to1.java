package ru.tandemservice.unirsmu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLDeleteQuery;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.unienr14_ctr.migration.MS_unienr14_ctr_2x10x1_0to1;

import java.sql.Timestamp;
import java.util.List;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unirsmu_2x10x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.nsiclient", "2.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrContractSpoTemplateDataSimple

        // создана новая сущность
        {
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("enrContractSpoTemplateDataSimple");

            SQLSelectQuery selectQuery = new SQLSelectQuery().from(
                    SQLFrom.table("enr14_ctmpldt_simple_t", "ectds")
                            // enr14_ctmpldt_t requestedcompetition_id
                            // eductr_ctmpldt_t educationyear_id cipher_p cost_id
                            // ctr_version_template_data_t owner_id docstartdate_p
                            .innerJoin(SQLFrom.table("enr14_ctmpldt_t", "enrtd"), "enrtd.id=ectds.id")
                            .innerJoin(SQLFrom.table("eductr_ctmpldt_t", "etd"), "etd.id=ectds.id")
                            .innerJoin(SQLFrom.table("ctr_version_template_data_t", "td"), "td.id=ectds.id")

                            .innerJoin(SQLFrom.table("ctr_contractver_t", "v"), "v.id=td.owner_id")
                            .innerJoin(SQLFrom.table("ctr_contractobj_t", "co"), "co.id=v.contract_id")
                            .innerJoin(SQLFrom.table("ctrcontracttype_t", "ct"), "ct.id=co.type_id")
            )
                    .column("ectds.id")
                    .column("enrtd.requestedcompetition_id")
                    .column("etd.educationyear_id")
                    .column("etd.cipher_p")
                    .column("etd.cost_id")
                    .column("td.owner_id")
                    .column("td.docstartdate_p")

                    .where("ct.code_p = ?");

            ISQLTranslator translator = tool.getDialect().getSQLTranslator();

            List<Object[]> items = tool.executeQuery(
                    MigrationUtils.processor(Long.class, Long.class, Long.class, String.class, Long.class, Long.class, Timestamp.class),
                    translator.toSql(selectQuery), "01.01.spf"); // Договор с иностранцем на обучение СПО (title) */ String DOGOVOR_S_INOSTRANTSEM_NA_OBUCHENIE_S_P_O = "01.01.spf";

            if(!items.isEmpty())
                tool.dropConstraint("ctr_version_template_data_t", "chk_class_d53992e9");

            for(Object[] item : items)
            {
                Long id = (Long) item[0];
                Long newId = EntityIDGenerator.generateNewId(entityCode);
                Long requestedcompetitionId = (Long) item[1];
                Long educationyearId = (Long) item[2];
                String cipher = (String) item[3];
                Long cost = (Long) item[4];
                Long owner = (Long) item[5];
                Timestamp docStartDate = (Timestamp) item[6];

                // delete
                // enr14_ctmpldt_simple_t
                SQLDeleteQuery deleteSimpleQuery = new SQLDeleteQuery("enr14_ctmpldt_simple_t").where("id=?");
                tool.executeUpdate(translator.toSql(deleteSimpleQuery), id);

                // enr14_ctmpldt_t
                SQLDeleteQuery deleteEnrQuery = new SQLDeleteQuery("enr14_ctmpldt_t").where("id=?");
                tool.executeUpdate(translator.toSql(deleteEnrQuery), id);

                // eductr_ctmpldt_t
                SQLDeleteQuery deleteEduQuery = new SQLDeleteQuery("eductr_ctmpldt_t").where("id=?");
                tool.executeUpdate(translator.toSql(deleteEduQuery), id);

                // ctr_version_template_data_t
                SQLDeleteQuery deleteCtrQuery = new SQLDeleteQuery("ctr_version_template_data_t").where("id=?");
                tool.executeUpdate(translator.toSql(deleteCtrQuery), id);

                // insert
                // ctr_version_template_data_t
                tool.executeUpdate("insert into ctr_version_template_data_t (id, discriminator, owner_id, docstartdate_p) values (?,?,?,?)", newId, entityCode, owner, docStartDate);

                // eductr_ctmpldt_t
                tool.executeUpdate("insert into eductr_ctmpldt_t (id, educationyear_id, cipher_p, cost_id) values (?,?,?,?)", newId, educationyearId, cipher, cost);

                // enr14_ctmpldt_t
                tool.executeUpdate("insert into enr14_ctmpldt_t (id, requestedcompetition_id) values (?,?)", newId, requestedcompetitionId);

                // enr14_ctmpldt_spo_t
                tool.executeUpdate("insert into enr14_ctmpldt_spo_t (id) values (?)", newId);
            }
        }

    }
}