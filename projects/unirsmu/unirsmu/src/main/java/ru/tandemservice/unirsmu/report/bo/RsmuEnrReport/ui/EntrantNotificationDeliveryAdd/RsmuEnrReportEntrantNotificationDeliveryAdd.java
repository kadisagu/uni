/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantNotificationDeliveryAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;

/**
 * @author Andrey Avetisov
 * @since 15.01.2015
 */
@Configuration
public class RsmuEnrReportEntrantNotificationDeliveryAdd extends BusinessComponentManager
{
    public static final String REPORT_KEY = "rsmuEnr14ReportPersonalNotificationDelivery";
    public static final String COMPETITION_FILTER_ADDON = CommonFilterAddon.class.getSimpleName();
    public static final String ENTRANT_STATE_DS = "entrantStateDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(COMPETITION_FILTER_ADDON, EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(ENTRANT_STATE_DS, entrantStateDSHandler()))
                .addDataSource(CommonManager.instance().yesNoDSConfig())
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantState.class)
                .order(EnrEntrantState.title())
                .filter(EnrEntrantState.title());
    }
}
