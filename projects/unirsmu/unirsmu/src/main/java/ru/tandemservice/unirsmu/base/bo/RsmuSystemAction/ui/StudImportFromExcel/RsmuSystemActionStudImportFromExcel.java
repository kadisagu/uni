/* $Id:$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.StudImportFromExcel;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Dmitry Seleznev
 * @since 17.12.2015
 */
@Configuration
public class RsmuSystemActionStudImportFromExcel extends BusinessComponentManager
{
}