/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantNotificationDeliveryAdd;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.*;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.program.entity.EduProgram;
import ru.tandemservice.uniedu.program.entity.EduProgramProf;
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantOriginalDocumentStatus;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 16.01.2015
 */
public class ReportEntrantNotificationDeliveryExcelBuilder
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private List<EnrEntrantState> _entrantStateList;
    private boolean _originalDocActive;
    private boolean _originalDoc;
    private boolean _acceptedActive;
    private boolean _accepted;
    private boolean _enrollmentAvailableActive;
    private boolean _enrollmentAvailable;
    private boolean _lowLimitPointsRatingActive;
    private boolean _upLimitPointsRatingActive;
    private Long _lowLimitPointsRating;
    private Long _upLimitPointsRating;
    private boolean _xlsFormat;
    private EnrCompetitionFilterAddon _util;
    private String[] _columnTitles = {"Личный номер абитуриента", "Фамилия Имя Отчество", "Номер мобильного телефона"};


    public ReportEntrantNotificationDeliveryExcelBuilder(EnrCompetitionFilterAddon util, EnrEnrollmentCampaign enrollmentCampaign,
                                                         boolean enrollmentStateActive, List<EnrEntrantState> entrantStateList,
                                                         boolean originalDocActive, boolean originalDoc,
                                                         boolean acceptedActive, boolean accepted,
                                                         boolean enrollmentAvailableActive, boolean enrollmentAvailable,
                                                         boolean lowLimitPointsRatingActive, long lowLimitPointsRating,
                                                         boolean upLimitPointsRatingActive, long upLimitPointsRating,
                                                         boolean xlsFormat)
    {
        setEnrollmentCampaign(enrollmentCampaign);
        if (enrollmentStateActive)
        {
            setEntrantStateList(entrantStateList);
        }
        if (originalDocActive)
        {
            setOriginalDoc(originalDoc);
            setOriginalDocActive(true);
        }
        if (lowLimitPointsRatingActive)
        {
            setLowLimitPointsRating(lowLimitPointsRating);
            setLowLimitPointsRatingActive(true);
        }
        if (upLimitPointsRatingActive)
        {
            setUpLimitPointsRating(upLimitPointsRating);
            setUpLimitPointsRatingActive(true);
        }

        if (acceptedActive)
        {
            setAccepted(accepted);
            setAcceptedActive(true);
        }
        if (enrollmentAvailableActive)
        {
            setEnrollmentAvailable(enrollmentAvailable);
            setEnrollmentAvailableActive(true);
        }

        setXlsFormat(xlsFormat);
        setUtil(util);
    }

    public ByteArrayOutputStream buildReport() throws Exception
    {
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        final WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем шрифты
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);
        WritableFont arial12bold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
        WritableFont arial10bold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);

        // шрифт для шапки отчета
        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        headerFormat.setBackground(jxl.format.Colour.GRAY_25);
        headerFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        // шрифт для строк отчета
        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        rowFormat.setWrap(true);

        // шрифт для параметров генерации
        WritableCellFormat paramFormatHeader = new WritableCellFormat(arial12bold);
        paramFormatHeader.setAlignment(jxl.format.Alignment.RIGHT);
        paramFormatHeader.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        paramFormatHeader.setWrap(true);

        // шрифт для параметров генерации
        WritableCellFormat paramFormat = new WritableCellFormat(arial10bold);
        paramFormat.setAlignment(jxl.format.Alignment.RIGHT);
        paramFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        paramFormat.setWrap(true);

        // шрифт для параметров генерации
        WritableCellFormat paramValueFormat = new WritableCellFormat(arial10);
        paramValueFormat.setAlignment(jxl.format.Alignment.RIGHT);
        paramValueFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        paramValueFormat.setWrap(true);


        try
        {
            WritableSheet sheet = workbook.createSheet("абитуриенты", workbook.getSheets().length);
            sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
            sheet.getSettings().setCopies(1);
            int excelRow = 0;
            if (isXlsFormat())
            {
                //sheet.getSettings().setVerticalFreeze(1);
                sheet.addCell(new Label(0, excelRow, "Параметры отчета:", paramFormatHeader));
                excelRow++;
                sheet.addCell(new Label(0, excelRow, "Приемная кампания:", paramFormat));
                sheet.addCell(new Label(1, excelRow, getEnrollmentCampaign().getTitle(), paramValueFormat));
                excelRow++;
                if (getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT).isEnableCheckboxChecked())
                {
                    sheet.addCell(new Label(0, excelRow, EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT.getDisplayName() + ":", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            getOrgUnitsTitle((List<OrgUnit>) getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT).getValue()),
                                            paramValueFormat));
                    excelRow++;
                }
                if (getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT).isEnableCheckboxChecked())
                {
                    sheet.addCell(new Label(0, excelRow, EnrCompetitionFilterAddon.PROGRAM_SUBJECT.getDisplayName() + ":", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            getProgramSubjectsTitle((List<EduProgramSubject>) getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT).getValue()),
                                            paramValueFormat));
                    excelRow++;
                }
                if (getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM).isEnableCheckboxChecked())
                {
                    sheet.addCell(new Label(0, excelRow, EnrCompetitionFilterAddon.PROGRAM_FORM.getDisplayName() + ":", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            getProgramFormsTitle((List<EduProgramProf>) getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM).getValue()),
                                            paramValueFormat));
                    excelRow++;
                }
                if (getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET).isEnableCheckboxChecked())
                {
                    sheet.addCell(new Label(0, excelRow, EnrCompetitionFilterAddon.PROGRAM_SET.getDisplayName() + ":", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            getProgramSetTitle((List<EnrProgramSetBase>) getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET).getValue()),
                                            paramValueFormat));
                    excelRow++;
                }
                if (getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE).isEnableCheckboxChecked())
                {
                    sheet.addCell(new Label(0, excelRow, EnrCompetitionFilterAddon.COMPETITION_TYPE.getDisplayName() + ":", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            getCompetitionTypeTitle((List<EnrCompetitionType>) getUtil().getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE).getValue()),
                                            paramValueFormat));
                    excelRow++;
                }

                if (_entrantStateList != null && !_entrantStateList.isEmpty())
                {
                    sheet.addCell(new Label(0, excelRow, "Состояние абитуриента:", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            getEntrantStateTitle(_entrantStateList),
                                            paramValueFormat));
                    excelRow++;
                }

                if (isOriginalDocActive())
                {
                    sheet.addCell(new Label(0, excelRow, "Сдан оригинал документа об образовании:", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            YesNoFormatter.INSTANCE.format(isOriginalDoc()),
                                            paramValueFormat));
                    excelRow++;
                }

                if (isAcceptedActive())
                {
                    sheet.addCell(new Label(0, excelRow, "Согласие на зачисление:", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            YesNoFormatter.INSTANCE.format(isAccepted()),
                                            paramValueFormat));
                    excelRow++;
                }

                if (isEnrollmentAvailableActive())
                {
                    sheet.addCell(new Label(0, excelRow, "Итоговое согласие:", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            YesNoFormatter.INSTANCE.format(isEnrollmentAvailable()),
                                            paramValueFormat));
                    excelRow++;
                }

                if (isLowLimitPointsRatingActive())
                {
                    sheet.addCell(new Label(0, excelRow, "Нижняя граница набранных баллов по всем ВИ:", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            String.valueOf(getLowLimitPointsRating()),
                                            paramValueFormat));
                    excelRow++;
                }

                if (isUpLimitPointsRatingActive())
                {
                    sheet.addCell(new Label(0, excelRow, "Верхняя граница набранных баллов по всем ВИ:", paramFormat));
                    sheet.addCell(new Label(1, excelRow,
                                            String.valueOf(getUpLimitPointsRating()),
                                            paramValueFormat));
                    excelRow++;
                }


            }
            for (int i = 0; i < _columnTitles.length; i++)
            {
                // задаем ширину колонки
                sheet.setColumnView(i, 35);

                // задаем название колонки
                sheet.addCell(new Label(i, excelRow, _columnTitles[i], headerFormat));
            }
            excelRow++;

            List<EnrEntrant> enrEntrantList = DataAccessServices.dao().getList(getEnrEntrantDql());

            for (EnrEntrant enrEntrant : enrEntrantList)
            {
                String personalNumber = enrEntrant.getPersonalNumber();
                String fio = enrEntrant.getFio();
                String mobilePhone = enrEntrant.getPerson().getContactData().getPhoneMobile();

                sheet.addCell(new Label(0, excelRow, personalNumber, rowFormat));
                sheet.addCell(new Label(1, excelRow, fio, rowFormat));
                sheet.addCell(new Label(2, excelRow, mobilePhone, rowFormat));
                excelRow++;

            }


        }
        catch (RowsExceededException e)
        {
            throw new ApplicationException("Невозможно сформировать отчет, так как объем итоговых данных превышает допустимый предел в 65536 строк.");
        }
        workbook.write();
        workbook.close();

        return out;
    }

    private DQLSelectBuilder getEnrEntrantDql()
    {
        DQLSelectBuilder filterDql = new DQLSelectBuilder().fromEntity(EnrRatingItem.class, "r").column(property("r"))
                .joinEntity("r", DQLJoinType.inner, EnrCompetition.class, "c", eq(property("r", EnrRatingItem.competition().id()), property("c", EnrCompetition.id())))
                .where(eq(property("e", EnrEntrant.P_ID), property("r", EnrRatingItem.entrant().id())));
        //Нижняя граница набранных баллов по всем ВИ
        if (getLowLimitPointsRating() != null)
        {
            filterDql.where(ge(property("r", EnrRatingItem.P_TOTAL_MARK_AS_LONG), value(getLowLimitPointsRating() * 1000)));
        }
        //Верхняя граница набранных баллов по всем ВИ
        if (getUpLimitPointsRating() != null)
        {
            filterDql.where(le(property("r", EnrRatingItem.P_TOTAL_MARK_AS_LONG), value(getUpLimitPointsRating() * 1000)));
        }

        //Формирующее подр., Направление, спец., профессия, Образовательная программа, Набор образовательных программ
        getUtil().applyFilters(filterDql, "c");


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrEntrant.class, "e").column(DQLExpressions.property("e"));
            //Применяем фильтры
            dql.where(exists(filterDql.buildQuery()));
            //Приемная кампания
            dql.where(eqValue(property("e", EnrEntrant.L_ENROLLMENT_CAMPAIGN), getEnrollmentCampaign()));

        //Состояние абитуриента
        if (getEntrantStateList() != null && !getEntrantStateList().isEmpty())
        {
            dql.where(in(property("e", EnrEntrant.L_STATE), getEntrantStateList()));
        }

        //Сдан оригинал документа об образовании
        if (isOriginalDocActive())
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rq").column(property("rq"))
                    .where(eq(property("e", EnrEntrant.P_ID), property("rq", EnrRequestedCompetition.request().entrant().id())))
                    .where(eqValue(property("rq", EnrRequestedCompetition.originalDocumentHandedIn()), Boolean.TRUE));

            dql.where(isOriginalDoc()? exists(subBuilder.buildQuery()) : notExists(subBuilder.buildQuery()));
        }

        if (isAcceptedActive())
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rq").column(property("rq"))
                    .where(eq(property("e", EnrEntrant.P_ID), property("rq", EnrRequestedCompetition.request().entrant().id())))
                    .where(eq(property("rq", EnrRequestedCompetition.accepted()), value(Boolean.TRUE)));

            dql.where(isAccepted()? exists(subBuilder.buildQuery()) : notExists(subBuilder.buildQuery()));
        }

        if (isEnrollmentAvailableActive())
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "rq").column(property("rq"))
                    .where(eq(property("e", EnrEntrant.P_ID), property("rq", EnrRequestedCompetition.request().entrant().id())))
                    .where(eq(property("rq", EnrRequestedCompetition.enrollmentAvailable()), value(Boolean.TRUE)));

            dql.where(isEnrollmentAvailable()? exists(subBuilder.buildQuery()) : notExists(subBuilder.buildQuery()));
        }

        dql.order(property("e", EnrEntrant.P_PERSONAL_NUMBER))
                .order(property("e", EnrEntrant.person().identityCard().lastName()))
                .order(property("e", EnrEntrant.person().identityCard().middleName()))
                .order(property("e", EnrEntrant.person().identityCard().firstName()));

        return dql;
    }

    //getters & setters
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public List<EnrEntrantState> getEntrantStateList()
    {
        return _entrantStateList;
    }

    public void setEntrantStateList(List<EnrEntrantState> entrantStateList)
    {
        _entrantStateList = entrantStateList;
    }

    public boolean isOriginalDoc()
    {
        return _originalDoc;
    }

    public void setOriginalDoc(boolean originalDoc)
    {
        _originalDoc = originalDoc;
    }

    public Long getLowLimitPointsRating()
    {
        return _lowLimitPointsRating;
    }

    public void setLowLimitPointsRating(Long lowLimitPointsRating)
    {
        _lowLimitPointsRating = lowLimitPointsRating;
    }

    public Long getUpLimitPointsRating()
    {
        return _upLimitPointsRating;
    }

    public void setUpLimitPointsRating(Long upLimitPointsRating)
    {
        _upLimitPointsRating = upLimitPointsRating;
    }

    public boolean isXlsFormat()
    {
        return _xlsFormat;
    }

    public void setXlsFormat(boolean xlsFormat)
    {
        _xlsFormat = xlsFormat;
    }

    public EnrCompetitionFilterAddon getUtil()
    {
        return _util;
    }

    public void setUtil(EnrCompetitionFilterAddon util)
    {
        _util = util;
    }

    public boolean isOriginalDocActive()
    {
        return _originalDocActive;
    }

    public void setOriginalDocActive(boolean originalDocActive)
    {
        _originalDocActive = originalDocActive;
    }

    public boolean isLowLimitPointsRatingActive()
    {
        return _lowLimitPointsRatingActive;
    }

    public void setLowLimitPointsRatingActive(boolean lowLimitPointsRatingActive)
    {
        _lowLimitPointsRatingActive = lowLimitPointsRatingActive;
    }

    public boolean isUpLimitPointsRatingActive()
    {
        return _upLimitPointsRatingActive;
    }

    public void setUpLimitPointsRatingActive(boolean upLimitPointsRatingActive)
    {
        _upLimitPointsRatingActive = upLimitPointsRatingActive;
    }

    private String getOrgUnitsTitle(List<OrgUnit> orgUnitList)
    {
        String title = "";

        for (OrgUnit orgUnit : orgUnitList)
        {
            title += orgUnit.getFullTitle() + "; ";
        }

        return title;
    }

    private String getProgramSubjectsTitle(List<EduProgramSubject> programSubjectList)
    {
        String title = "";

        for (EduProgramSubject programSubject : programSubjectList)
        {
            title += programSubject.getTitleWithCode() + "; ";
        }
        return title;
    }

    private String getProgramFormsTitle(List<EduProgramProf> programFormList)
    {
        String title = "";

        for (EduProgramProf programForm : programFormList)
        {
                title +=  programForm.getTitleWithCodeAndConditionsShortWithForm() + "; ";
        }
        return title;
    }

    private String getProgramSetTitle(List<EnrProgramSetBase> programSetList)
    {
        String title = "";

        for (EnrProgramSetBase programSetBase : programSetList)
        {
            title += programSetBase.getTitle() + "; ";
        }
        return title;
    }

    private String getCompetitionTypeTitle(List<EnrCompetitionType> competitionTypeList)
    {
        String title = "";

        for (EnrCompetitionType competitionType : competitionTypeList)
        {
            title += competitionType.getPrintTitle() + "; ";
        }
        return title;
    }

    private String getEntrantStateTitle(List<EnrEntrantState> entrantStateList)
    {
        String title = "";

        for (EnrEntrantState entrantState : entrantStateList)
        {
            title += entrantState.getTitle() + "; ";
        }
        return title;
    }

    public boolean isAcceptedActive() {
        return _acceptedActive;
    }

    public void setAcceptedActive(boolean _acceptedActive) {
        this._acceptedActive = _acceptedActive;
    }

    public boolean isAccepted() {
        return _accepted;
    }

    public void setAccepted(boolean _accepted) {
        this._accepted = _accepted;
    }

    public boolean isEnrollmentAvailableActive() {
        return _enrollmentAvailableActive;
    }

    public void setEnrollmentAvailableActive(boolean _enrollmentAvailableActive) {
        this._enrollmentAvailableActive = _enrollmentAvailableActive;
    }

    public boolean isEnrollmentAvailable() {
        return _enrollmentAvailable;
    }

    public void setEnrollmentAvailable(boolean _enrollmentAvailable) {
        this._enrollmentAvailable = _enrollmentAvailable;
    }
}
