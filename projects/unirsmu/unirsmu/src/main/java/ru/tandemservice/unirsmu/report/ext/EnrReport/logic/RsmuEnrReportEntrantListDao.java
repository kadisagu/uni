/* $Id$ */
package ru.tandemservice.unirsmu.report.ext.EnrReport.logic;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.logic.EnrReportEntrantListDao;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionExclusive;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionNoExams;
import ru.tandemservice.unienr14.request.entity.IEnrEntrantBenefitStatement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 08.07.2015
 */
public class RsmuEnrReportEntrantListDao extends EnrReportEntrantListDao
{
    @Override
    protected void addCustomColumn(List<String> row, EnrRequestedCompetition entrant, String tableName)
    {
        row.add(2, entrant.getRequest().getEntrant().getPersonalNumber()); // добавляем личный номер
        row.remove(row.size()-3); //удаляем номер заявления
    }
}