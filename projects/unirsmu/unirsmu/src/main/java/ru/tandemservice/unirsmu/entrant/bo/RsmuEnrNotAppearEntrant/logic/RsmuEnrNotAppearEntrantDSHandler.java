/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrAbsenceNoteCodes;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.ui.List.RsmuEnrNotAppearEntrantListUI;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 10.02.2015
 */
public class RsmuEnrNotAppearEntrantDSHandler extends EnrEntrantDSHandler
{
    public static String SETTINGS = "settings";
    public static String DISCIPLINES = "disciplines";
    public static String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";


    public RsmuEnrNotAppearEntrantDSHandler(String ownerId)
    {
        super(ownerId);
    }

    protected DQLSelectBuilder getEntrantDQL(ExecutionContext context)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EnrEntrant.class, ENTRANT_ALIAS)
                .column(ENTRANT_ALIAS)
                .joinPath(DQLJoinType.inner, EnrEntrant.person().fromAlias(ENTRANT_ALIAS), "person")
                .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("person"), "idc");

        IDataSettings settings = context.get(SETTINGS);

        FilterUtils.applySelectFilter(dql, ENTRANT_ALIAS, EnrEntrant.L_ENROLLMENT_CAMPAIGN, context.get(ENROLLMENT_CAMPAIGN));

        EnrOrgUnit enrOrgUnit = settings.get("enrOrgUnit");
        if (null != enrOrgUnit)
        {
            DQLSelectBuilder competitionBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrRequestedCompetition.class, "rc")
                    .column(property("rc"))
                    .where(eqValue(property("rc", EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit()), enrOrgUnit))
                    .where(eq(property("rc", EnrRequestedCompetition.request().entrant()), property(ENTRANT_ALIAS)));

            dql.where(exists(competitionBuilder.buildQuery()));
        }

        EnrExamGroup enrExamGroup = settings.get("enrExamGroup");
        DataWrapper examPassDisciplineWrapper = context.get(DISCIPLINES);
        DQLSelectBuilder examDql = new DQLSelectBuilder()
                .fromEntity(EnrExamPassDiscipline.class, "ed")
                .column(property("ed"))
                .where(eq(property("ed", EnrExamPassDiscipline.entrant()), property(ENTRANT_ALIAS)))
                .where(eqValueNullSafe(property("ed", EnrExamPassDiscipline.examGroup()), enrExamGroup))
                .where(eqValue(property("ed", EnrExamPassDiscipline.discipline()), examPassDisciplineWrapper.get(RsmuEnrNotAppearEntrantListUI.PASS_DISCIPLINE)))
                .where(eqValue(property("ed", EnrExamPassDiscipline.passForm()), examPassDisciplineWrapper.get(RsmuEnrNotAppearEntrantListUI.PASS_FORM)))
                .where(eqValue(property("ed", EnrExamPassDiscipline.absenceNote().code()), EnrAbsenceNoteCodes.VALID));

        dql.where(exists(examDql.buildQuery()));
        return dql;
    }
}
