/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantExaminationResultAdd.RsmuEnrReportEntrantExaminationResultAddUI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 03.02.2015
 */
public class RsmuExamGroupDSHandler extends DefaultComboDataSourceHandler
{
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String EXAM_PASS = "examPass";
    public static final String EXAM_PASS_ACTIVE = "examPassActive";


    public RsmuExamGroupDSHandler(String ownerId)
    {
        super(ownerId, EnrExamGroup.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {

        List<DataWrapper> examPass = context.get(EXAM_PASS);
        boolean examPassActive = context.getBoolean(EXAM_PASS_ACTIVE, false);
        List<EnrCampaignDiscipline> examPassDisciplineList = new ArrayList<>();
        List<EnrExamPassForm> examPassFormList = new ArrayList<>();
        if (examPass!=null && examPassActive)
        {
            for (DataWrapper wrapper : examPass)
            {
                examPassDisciplineList.add((EnrCampaignDiscipline)wrapper.get(RsmuEnrReportEntrantExaminationResultAddUI.PASS_DISCIPLINE));
                examPassFormList.add((EnrExamPassForm)wrapper.get(RsmuEnrReportEntrantExaminationResultAddUI.PASS_FORM));
            }
        }

        EnrEnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN);
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "g").column("g")
                .where(eqValue(property("g", EnrExamGroup.examGroupSet().enrollmentCampaign()), enrollmentCampaign));
        if (examPassActive)
        {
            dql.where(in(property("g", EnrExamGroup.discipline()), examPassDisciplineList));
        }
        if (examPassActive)
        {
            dql.where(in(property("g", EnrExamGroup.passForm()), examPassFormList));
        }

        String filter = input.getComboFilterByValue();
        if (StringUtils.isNotEmpty(filter))
        {
            dql.where(likeUpper(property("g", EnrExamGroup.title()), value(CoreStringUtils.escapeLike(filter, true))));
        }

        Set keys = input.getPrimaryKeys();
        if (keys != null && !keys.isEmpty())
        {
            if (keys.size() == 1)
                dql.where(eq(property("g", EnrExamGroup.id()), commonValue(keys.iterator().next())));
            else
                dql.where(in(property("g", EnrExamGroup.id()), keys));
        }
        List<EnrExamGroup> examGroupList = dql.createStatement(context.getSession()).list();
        Collections.sort(examGroupList, (o1, o2) -> NumberAsStringComparator.INSTANCE.compare(o1.getTitle(), o2.getTitle()));

        return ListOutputBuilder.get(input, examGroupList).build();
    }


}
