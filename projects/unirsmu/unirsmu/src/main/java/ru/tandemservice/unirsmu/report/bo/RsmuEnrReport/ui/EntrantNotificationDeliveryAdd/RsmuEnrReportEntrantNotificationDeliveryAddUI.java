/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantNotificationDeliveryAdd;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.logic.EnrEntrantDSHandler;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.GlobalList.EnrReportBaseGlobalList;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 15.01.2015
 */
public class RsmuEnrReportEntrantNotificationDeliveryAddUI extends UIPresenter
{
    private static String XLS_FORMAT = "xls";
    private static String CSV_FORMAT = "csv";
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private boolean _enrollmentStateActive;
    private List<EnrEntrantState> _entrantStateList;
    private boolean _originalDocActive;
    private DataWrapper _originalDoc;
    private boolean _acceptedActive;
    private DataWrapper _accepted;
    private boolean _enrollmentAvailableActive;
    private DataWrapper _enrollmentAvailable;
    private boolean _lowLimitPointsRatingActive;
    private long _lowLimitPointsRating;
    private boolean _upLimitPointsRatingActive;
    private long _upLimitPointsRating;
    private String[] _formatList = new String[]{XLS_FORMAT, CSV_FORMAT};
    private String _currentFormatValue;
    private byte[] _report = null;

    @Override
    public void onComponentPrepareRender()
    {
        if (_report != null)
        {
            // предлагаем пользователю скачать файл отчета
            if (_currentFormatValue.equals(XLS_FORMAT))
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Сведения для рассылки уведомлений абитуриентам.xls").document(_report), true);
            }
            else
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Сведения для рассылки уведомлений абитуриентам.csv").document(_report), true);
            }
            _report = null;
        }
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        setEnrollmentStateActive(false);
        setEntrantStateList(null);
        setOriginalDocActive(false);
        setOriginalDoc(null);
        setCurrentFormatValue(XLS_FORMAT);
        setLowLimitPointsRatingActive(false);
        setLowLimitPointsRating(0);
        setUpLimitPointsRatingActive(false);
        setUpLimitPointsRating(0);
        configUtil(getCompetitionFilterAddon());
        getCompetitionFilterAddon().clearSettings();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEntrantDSHandler.PARAM_ENR_COMPETITION_UTIL, getConfig().getAddon(CommonFilterAddon.class.getSimpleName()));
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    // Utils
    private void configUtil(EnrCompetitionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(RsmuEnrReportEntrantNotificationDeliveryAdd.COMPETITION_FILTER_ADDON);
    }

    //getters & setters

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }
    public boolean isEnrollmentStateActive()
    {
        return _enrollmentStateActive;
    }

    public void setEnrollmentStateActive(boolean enrollmentStateActive)
    {
        _enrollmentStateActive = enrollmentStateActive;
    }

    public boolean isOriginalDocActive()
    {
        return _originalDocActive;
    }

    public void setOriginalDocActive(boolean originalDocActive)
    {
        _originalDocActive = originalDocActive;
    }

    public DataWrapper getOriginalDoc()
    {
        return _originalDoc;
    }

    public void setOriginalDoc(DataWrapper originalDoc)
    {
        _originalDoc = originalDoc;
    }

    public List<EnrEntrantState> getEntrantStateList()
    {
        return _entrantStateList;
    }

    public void setEntrantStateList(List<EnrEntrantState> entrantStateList)
    {
        _entrantStateList = entrantStateList;
    }

    public boolean isLowLimitPointsRatingActive()
    {
        return _lowLimitPointsRatingActive;
    }

    public void setLowLimitPointsRatingActive(boolean lowLimitPointsRatingActive)
    {
        _lowLimitPointsRatingActive = lowLimitPointsRatingActive;
    }

    public boolean isUpLimitPointsRatingActive()
    {
        return _upLimitPointsRatingActive;
    }

    public void setUpLimitPointsRatingActive(boolean upLimitPointsRatingActive)
    {
        _upLimitPointsRatingActive = upLimitPointsRatingActive;
    }

    public long getLowLimitPointsRating()
    {
        return _lowLimitPointsRating;
    }

    public void setLowLimitPointsRating(long lowLimitPointsRating)
    {
        _lowLimitPointsRating = lowLimitPointsRating;
    }

    public long getUpLimitPointsRating()
    {
        return _upLimitPointsRating;
    }

    public void setUpLimitPointsRating(long upLimitPointsRating)
    {
        _upLimitPointsRating = upLimitPointsRating;
    }

    public String[] getFormatList()
    {
        return _formatList;
    }

    public void setFormatList(String[] formatList)
    {
        _formatList = formatList;
    }

    public String getCurrentFormatValue()
    {
        return _currentFormatValue;
    }

    public void setCurrentFormatValue(String currentFormatValue)
    {
        _currentFormatValue = currentFormatValue;
    }

    public boolean isXlsFormat()
    {
        return getCurrentFormatValue().equals(XLS_FORMAT);
    }

    //listeners
    public void onClickCancel()
    {
        _uiActivation.asDesktopRoot(EnrReportBaseGlobalList.class).activate();
    }

    public void onClickApply()
    {
        getCompetitionFilterAddon().saveSettings();


        try
        {
            ReportEntrantNotificationDeliveryExcelBuilder builder = new ReportEntrantNotificationDeliveryExcelBuilder(getCompetitionFilterAddon(), getEnrollmentCampaign(),
                    isEnrollmentStateActive(), getEntrantStateList(),
                    isOriginalDocActive(), getBooleanFromWrapper(getOriginalDoc()),
                    isAcceptedActive(), getBooleanFromWrapper(getAccepted()),
                    isEnrollmentAvailableActive(), getBooleanFromWrapper(getEnrollmentAvailable()),
                    isLowLimitPointsRatingActive(), getLowLimitPointsRating(),
                    isUpLimitPointsRatingActive(), getUpLimitPointsRating(),
                    isXlsFormat());
            try
            {
                _report = builder.buildReport().toByteArray();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }


    }

    //Utils
    private boolean getBooleanFromWrapper(DataWrapper o)
    {
        return TwinComboDataSourceHandler.getSelectedValueNotNull(o);
    }

    public boolean isAcceptedActive() {
        return _acceptedActive;
    }

    public void setAcceptedActive(boolean _acceptedActive) {
        this._acceptedActive = _acceptedActive;
    }

    public DataWrapper getAccepted() {
        return _accepted;
    }

    public void setAccepted(DataWrapper _accepted) {
        this._accepted = _accepted;
    }

    public boolean isEnrollmentAvailableActive() {
        return _enrollmentAvailableActive;
    }

    public void setEnrollmentAvailableActive(boolean _enrollmentAvailableActive) {
        this._enrollmentAvailableActive = _enrollmentAvailableActive;
    }

    public DataWrapper getEnrollmentAvailable() {
        return _enrollmentAvailable;
    }

    public void setEnrollmentAvailable(DataWrapper _enrollmentAvailable) {
        this._enrollmentAvailable = _enrollmentAvailable;
    }
}
