package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic.IEnrReportRatingListDao;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic.IRsmuEnrReportDao;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic.RsmuEnrReportDao;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.RatingListAdd.logic.RsmuEnrReportRatingListDao;

/**
 * @author Alexander Shaburov
 * @since 15.11.13
 */
@Configuration
public class RsmuEnrReportManager extends BusinessObjectManager
{
    public static RsmuEnrReportManager instance()
    {
        return instance(RsmuEnrReportManager.class);
    }

    @Bean
    public IRsmuEnrReportDao dao()
    {
        return new RsmuEnrReportDao();
    }

    @Bean
    public IEnrReportRatingListDao ratingListDao()
    {
        return new RsmuEnrReportRatingListDao();
    }
}
