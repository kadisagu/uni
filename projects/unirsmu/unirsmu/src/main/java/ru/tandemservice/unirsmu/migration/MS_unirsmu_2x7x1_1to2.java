package ru.tandemservice.unirsmu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unirsmu_2x7x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность rsmuEnrReportContractTransfer

		// создана новая сущность
		{
			// создать таблицу
            if (!tool.tableExists("rsmu_enr14_ctr_transfer_t"))
            {
                DBTable dbt = new DBTable("rsmu_enr14_ctr_transfer_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("enrollmentcampaign_id", DBType.LONG).setNullable(false),
                                          new DBColumn("enrorder_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("formativeorgunit_p", DBType.TEXT),
                                          new DBColumn("programsubject_p", DBType.TEXT),
                                          new DBColumn("eduprogram_p", DBType.TEXT),
                                          new DBColumn("programset_p", DBType.TEXT)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("rsmuEnrReportContractTransfer");
            }

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность rsmuEnrReportPersonalFileTransferList

		//  свойство enrOrgUnit стало обязательным
		{

			// задать значение по умолчанию
			java.lang.String defaultEnrOrgUnit = "";
			tool.executeUpdate("update rsmu_enr14_rep_file_tr_list_t set enrorgunit_p=? where enrorgunit_p is null", defaultEnrOrgUnit);
			// сделать колонку NOT NULL
			tool.setColumnNullable("rsmu_enr14_rep_file_tr_list_t", "enrorgunit_p", false);

		}


    }
}