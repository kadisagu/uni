/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileTransferAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;

/**
 * @author Andrey Avetisov
 * @since 20.01.2015
 */
@Configuration
public class RsmuEnrReportPersonalFileTransferAdd extends BusinessComponentManager
{
    public static final String ENR_ORDER_DS = "enrOrderDS";
    public static final String EDU_GROUP_ORGUNIT_DS = "eduGroupOrgUnitDS";
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addDataSource(selectDS(ENR_ORDER_DS, enrOrderDSHandler()))
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(EDU_GROUP_ORGUNIT_DS, eduGroupOrgUnitHandler()).addColumn(EducationOrgUnit.L_GROUP_ORG_UNIT, EducationOrgUnit.L_GROUP_ORG_UNIT, new IFormatter()
                {
                    @Override
                    public String format(Object source)
                    {
                        return ((OrgUnit) source).getFullTitle();
                    }
                }))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler enrOrderDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrOrder.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                EnrEnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN);
                dql.where(DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.state().code()), OrderStatesCodes.FINISHED));
                dql.where(DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.L_ENROLLMENT_CAMPAIGN), enrollmentCampaign));
                dql.where(DQLExpressions.or(
                        DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.type().code()), EnrOrderTypeCodes.ENROLLMENT),
                        DQLExpressions.eqValue(DQLExpressions.property(alias, EnrOrder.type().code()), EnrOrderTypeCodes.ENROLLMENT_MIN))
                );
            }
        }
                .order(EnrOrder.number())
                .filter(EnrOrder.number())
                .filter(EnrOrder.commitDate());
    }

    @Bean
    public IDefaultComboDataSourceHandler eduGroupOrgUnitHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationOrgUnit.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(DQLExpressions.isNotNull(DQLExpressions.property(alias, EducationOrgUnit.L_GROUP_ORG_UNIT)));
            }
        }
                .order(EducationOrgUnit.groupOrgUnit().title())
                .filter(EducationOrgUnit.groupOrgUnit().title());
    }
}
