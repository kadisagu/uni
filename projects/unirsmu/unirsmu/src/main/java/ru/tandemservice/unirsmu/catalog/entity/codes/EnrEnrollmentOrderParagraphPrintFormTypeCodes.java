package ru.tandemservice.unirsmu.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные формы параграфов приказов о зачислении"
 * Имя сущности : enrEnrollmentOrderParagraphPrintFormType
 * Файл data.xml : unirsmu.data.xml
 */
public interface EnrEnrollmentOrderParagraphPrintFormTypeCodes
{
    /** Константа кода (code) элемента : Базовый шаблон параграфа приказа о зачислении (title) */
    String BASE = "base";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении (целевой прием) (title) */
    String TARGET_ADMISSION = "target_admission";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении без ВИ, по договору (title) */
    String RSMU_BS_NO_EXAM_CONTRACT = "rsmu_bs_no_exam_contract";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении без ВИ (title) */
    String RSMU_BS_NO_EXAM = "rsmu_bs_no_exam";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении в рамках квоты особых прав (title) */
    String RSMU_BS_EXCLUSIVE = "rsmu_bs_exclusive";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении КЦП (title) */
    String RSMU_BS_MINISTERIAL = "rsmu_bs_ministerial";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении по договору (title) */
    String RSMU_BS_CONTRACT = "rsmu_bs_contract";

    Set<String> CODES = ImmutableSet.of(BASE, TARGET_ADMISSION, RSMU_BS_NO_EXAM_CONTRACT, RSMU_BS_NO_EXAM, RSMU_BS_EXCLUSIVE, RSMU_BS_MINISTERIAL, RSMU_BS_CONTRACT);
}
