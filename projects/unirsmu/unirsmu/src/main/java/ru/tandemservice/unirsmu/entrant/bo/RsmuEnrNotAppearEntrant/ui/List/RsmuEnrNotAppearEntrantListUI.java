/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.ui.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.*;
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic.RsmuEnrNotAppearEntrantDSHandler;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic.RsmuEnrNotAppearEntrantExcelBuilder;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.ui.ChangeExamGroup.RsmuEnrNotAppearEntrantChangeExamGroup;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.ui.ChangeExamGroup.RsmuEnrNotAppearEntrantChangeExamGroupUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic.RsmuExamGroupDSHandler;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 09.02.2015
 */
public class RsmuEnrNotAppearEntrantListUI extends UIPresenter
{
    public static String PASS_FORM = "passForm";
    public static String PASS_DISCIPLINE = "passDiscipline";

    private EnrEnrollmentCampaign _enrollmentCampaign;
    private ISingleSelectModel _disciplineModel;
    private Map<Long, DataWrapper> _disciplineModelWrapperMap;
    private DataWrapper _examPassDiscipline;
    private byte[] _report = null;
    String _reportType = "";

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());
        prepareDisciplineSelectModel();
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (_report!=null)
        {
            if (_reportType.equals("xls"))
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Неявившиеся абитуриенты." + _reportType).document(_report), true);
            }
            else if (_reportType.equals("rtf"))
            {
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Неявившиеся абитуриенты." + _reportType).document(_report), true);
            }
            _report = null;
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(dataSource.getName().equals(RsmuEnrNotAppearEntrantList.ENTRANT_DS))
        {
            dataSource.put(RsmuEnrNotAppearEntrantDSHandler.SETTINGS, getSettings());
            dataSource.put(RsmuEnrNotAppearEntrantDSHandler.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
            dataSource.put(RsmuEnrNotAppearEntrantDSHandler.DISCIPLINES, getExamPassDiscipline());
        }
        if (dataSource.getName().equals(RsmuEnrNotAppearEntrantList.ENR_ORGUNIT_DS))
        {
            dataSource.put(RsmuEnrNotAppearEntrantList.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
        }
        if (dataSource.getName().equals(RsmuEnrNotAppearEntrantList.ENR_EXAM_GROUP_DS))
        {
            dataSource.put(RsmuExamGroupDSHandler.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
        }
    }
    public void onChangeExamGroup ()
    {
        saveSettings();
    }

    //listeners
    public void onClickSubmit()
    {
        saveSettings();
    }
    public void onClickClear()
    {
        getSettings().remove("enrOrgUnit");
    }

    public void onClickPrintRtf()
    {
        _reportType = "rtf";
        final IScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.RSMU_NOT_APPEAR_ENTRANT);
        List<ViewWrapper> enrEntrantList = getConfig().getDataSource(RsmuEnrNotAppearEntrantList.ENTRANT_DS).getRecords();
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "entrantList", enrEntrantList);
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        _report = RtfUtil.toByteArray(mainDoc);

    }

    public void onClickPrintXls()
    {
        _reportType = "xls";
        List<ViewWrapper> enrEntrantList = getConfig().getDataSource(RsmuEnrNotAppearEntrantList.ENTRANT_DS).getRecords();
        RsmuEnrNotAppearEntrantExcelBuilder xlsBuilder = new RsmuEnrNotAppearEntrantExcelBuilder(enrEntrantList);
        try
        {
            _report = xlsBuilder.buildReport().toByteArray();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        prepareDisciplineSelectModel();
    }

    public void onClickSetExamGroup ()
    {
        CheckboxColumn checkboxColumn = (CheckboxColumn) ((PageableSearchListDataSource) _uiConfig.getDataSource(RsmuEnrNotAppearEntrantList.ENTRANT_DS)).getLegacyDataSource().getColumn("checkbox");
        Collection<IEntity> selectedRows = checkboxColumn.getSelectedObjects();
        if (CollectionUtils.isEmpty(selectedRows))
        {
            throw new ApplicationException("Выберите абитуриентов.");
        }
        List<Long> entrantIds = new ArrayList<>(selectedRows.size());
        for (IEntity entity : selectedRows)
        {
            entrantIds.add(entity.getId());
        }

        Long enrOrgUnitId = getSettings().get("enrOrgUnit")!=null?((EnrOrgUnit)getSettings().get("enrOrgUnit")).getId():null;
        _uiActivation
                .asDialog(RsmuEnrNotAppearEntrantChangeExamGroup.class)
                .parameter(RsmuEnrNotAppearEntrantChangeExamGroupUI.ENTRANT_ID_LIST, entrantIds)
                .parameter(RsmuEnrNotAppearEntrantChangeExamGroupUI.CAMPAIGN_DISCIPLINE, ((EnrCampaignDiscipline) getExamPassDiscipline().get(PASS_DISCIPLINE)).getId())
                .parameter(RsmuEnrNotAppearEntrantChangeExamGroupUI.EXAM_PASS_FORM, ((EnrExamPassForm) getExamPassDiscipline().get(PASS_FORM)).getId())
                .parameter(RsmuEnrNotAppearEntrantChangeExamGroupUI.ENR_ORG_UNIT, enrOrgUnitId)
                .parameter(RsmuEnrNotAppearEntrantChangeExamGroupUI.EXAM_GROUP, ((EnrExamGroup)getSettings().get("enrExamGroup")).getId())
                .activate();
    }

    public void prepareDisciplineSelectModel()
    {
        {
            _disciplineModelWrapperMap = new HashMap<>();

            final DQLSelectBuilder discResultDQL = new DQLSelectBuilder()
                    .fromEntity(EnrExamVariant.class, "ge")
                    .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                    .joinEntity("ge", DQLJoinType.inner, EnrCampaignDiscipline.class, "cd", eq(property(EnrCampaignDiscipline.id().fromAlias("cd")), property("cdValue.id")))
                    .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                    .where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("cd")), value(getEnrollmentCampaign())))
                    .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)));

            final DQLSelectBuilder discGroupResultDQL = new DQLSelectBuilder()
                    .fromEntity(EnrExamVariant.class, "ge")
                    .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                    .joinEntity("ge", DQLJoinType.inner, EnrCampaignDisciplineGroup.class, "dg", eq(property(EnrCampaignDisciplineGroup.id().fromAlias("dg")), property("cdValue.id")))
                    .joinEntity("dg", DQLJoinType.inner, EnrCampaignDisciplineGroupElement.class, "dge", eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("dge")), property("dg")))
                    .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                    .where(eq(property(EnrCampaignDisciplineGroup.enrollmentCampaign().fromAlias("dg")), value(getEnrollmentCampaign())))
                    .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)));

            discResultDQL
                    .column(property("cd"))
                    .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            discGroupResultDQL
                    .column(property(EnrCampaignDisciplineGroupElement.discipline().fromAlias("dge")))
                    .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            final List<Object[]> resultDiscList = DataAccessServices.dao().getList(discResultDQL);
            final List<Object[]> resultDiscGroupList = DataAccessServices.dao().getList(discGroupResultDQL);

            long id = 1L;
            final Set<CoreCollectionUtils.Pair<Long, Long>> uniqPair = new LinkedHashSet<>();
            for (Object[] objects : resultDiscList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty(PASS_DISCIPLINE, campDisc);
                    wrapper.setProperty(PASS_FORM, passForm);
                    _disciplineModelWrapperMap.put(id++, wrapper);
                }
            }

            for (Object[] objects : resultDiscGroupList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty(PASS_DISCIPLINE, campDisc);
                    wrapper.setProperty(PASS_FORM, passForm);
                    _disciplineModelWrapperMap.put(id++, wrapper);
                }
            }
        }

        _disciplineModel = new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings({"unchecked", "ConstantConditions"})
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (o != null)
                {
                    final Long key = (Long) o;
                    final DataWrapper wrapper = _disciplineModelWrapperMap.get(key);

                    return new SimpleListResultBuilder<>(Collections.singleton(wrapper));
                }

                final Collection<DataWrapper> values = new ArrayList<>(_disciplineModelWrapperMap.values());

                if (StringUtils.isNotEmpty(filter))
                {
                    for (Iterator<DataWrapper> iterator = values.iterator();iterator.hasNext();)
                    {
                        if (!StringUtils.containsIgnoreCase(iterator.next().getTitle(), filter))
                            iterator.remove();
                    }
                }

                return new SimpleListResultBuilder(values)
                {
                    @Override
                    public ListResult findOptions()
                    {
                        final List objects = super.findOptions().getObjects();
                        List list = objects.size() >= 50 ? objects.subList(0, 50) : objects;
                        int count = list==null ? 0 : list.size();
                        if( count==50)
                            count = objects.size();

                        Collections.sort(list, new Comparator<DataWrapper>()
                        {
                            @Override
                            public int compare(DataWrapper o1, DataWrapper o2)
                            {
                                return o1.getTitle().compareTo(o2.getTitle());
                            }
                        });

                        return new ListResult(list, count);
                    }
                };
            }
        };
    }

    //getters & setters
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public ISingleSelectModel getDisciplineModel()
    {
        return _disciplineModel;
    }

    public void setDisciplineModel(ISingleSelectModel disciplineModel)
    {
        _disciplineModel = disciplineModel;
    }

    public DataWrapper getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    public void setExamPassDiscipline(DataWrapper examPassDiscipline)
    {
        _examPassDiscipline = examPassDiscipline;
    }

    public boolean isNothingSelected(){ return null == getExamPassDiscipline() || null == getSettings().get("enrExamGroup"); }
}
