/* $Id$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.VectorSync;

import org.apache.log4j.Level;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unirsmu.base.bo.RsmuSettings.RsmuSettingsManager;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorSyncDaemonDao;

/**
 * @author Dmitry Seleznev
 * @since 23.11.2015
 */
public class RsmuSystemActionVectorSyncUI extends UIPresenter
{

    private boolean _autoRefreshEnabled = false;
    private int _timerInterval = 2000;

    @Override
    public void onComponentRefresh()
    {
        RsmuVectorSyncDaemonDao.unlockAdnWakeUpDaemon();
    }

    public void onClickApply()
    {
        if(RsmuSettingsManager.isAnyConnectionParameterIsNotSpecified())
            throw new ApplicationException("Не заданы параметры подключения к БД ИС \"Вектор\".");

        if(!RsmuVectorSyncDaemonDao.isConnectionEstablished())
            throw new ApplicationException("Не возможно установить соединение с БД ИС \"Вектор\".");

        RsmuVectorSyncDaemonDao.logEvent(Level.INFO, "************* Vector sync was initiated manually ");
        RsmuVectorSyncDaemonDao.registerCatalogToSyncAndWakeUpDaemon(RsmuVectorSyncDaemonDao.CATALOG_PERSONS);
        RsmuVectorSyncDaemonDao.registerCatalogToSyncAndWakeUpDaemon(RsmuVectorSyncDaemonDao.CATALOG_STUDENTS);
        RsmuVectorSyncDaemonDao.registerCatalogToSyncAndWakeUpDaemon(RsmuVectorSyncDaemonDao.CATALOG_ADDRESS);
        RsmuVectorSyncDaemonDao.registerCatalogToSyncAndWakeUpDaemon(RsmuVectorSyncDaemonDao.CATALOG_CONTACT_DATA);
        RsmuVectorSyncDaemonDao.registerCatalogToSyncAndWakeUpDaemon(RsmuVectorSyncDaemonDao.CATALOG_PHOTO);

        /*RsmuVectorSyncHolder.addCatalogToSync(RsmuVectorSyncHolder.CATALOG_PERSONS);

        if (!RsmuSettingsManager.isVectorDBUse())
            throw new ApplicationException("Связь с базой данных ИС \"Вектор\" отключена. См. меню \"Система\"/\"Настройки\"/\"Общие настройки\"/\"Служебные настройки РНИМУ\".");

        String dbHost = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_HOST));
        String dbPort = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_PORT));
        String dbName = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_NAME));
        String dbLogin = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_LOGIN));
        String dbPassword = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_PASSWORD));

        if (null == dbPort) dbPort = "1433";

        if (null == dbHost)
            throw new ApplicationException("Не задан адрес сервера БД ИС \"Вектор\". См. меню \"Система\"/\"Настройки\"/\"Общие настройки\"/\"Служебные настройки РНИМУ\".");
        if (null == dbName)
            throw new ApplicationException("Не задано имя БД ИС \"Вектор\". См. меню \"Система\"/\"Настройки\"/\"Общие настройки\"/\"Служебные настройки РНИМУ\".");
        if (null == dbLogin)
            throw new ApplicationException("Не задан логин для подключения к БД ИС \"Вектор\". См. меню \"Система\"/\"Настройки\"/\"Общие настройки\"/\"Служебные настройки РНИМУ\".");
        if (null == dbPassword)
            throw new ApplicationException("Не задан пароль для подключения к БД ИС \"Вектор\". См. меню \"Система\"/\"Настройки\"/\"Общие настройки\"/\"Служебные настройки РНИМУ\".");


        try
        {
            Connection connection = DriverManager.getConnection(connectionString.toString(), dbLogin, dbPassword);
            connection.setAutoCommit(false);

            Statement st = connection.createStatement();
            st.execute("select p.PersonID, p.Family, p.FirstName, p.SecName, p.BirthDate, p.BirthPlace, p.Gender, " +
                    "ltdic.TypeDocumentIdentityCardId, ltd.TypeDocumentId, ltd.TypeDocumentFullName,  d.DocumentSeries, " +
                    "d.DocumentNumber, d.DocumentDate, d.DocumentOrganization, d.OrganizationCode\n" +
                    "from Person p \n" +
                    "inner join Student s on p.PersonId=S.StudentId\n" +
                    "inner join AgreementStudent a on s.StudentId=a.StudentId\n" +
                    "left outer join DocumentPerson d on p.PersonId=d.PersonId\n" +
                    "left outer join listTypeDocument ltd on d.TypeDocumentId=ltd.TypeDocumentId\n" +
                    "left outer join listTypeDocumentIdentityCard ltdic on ltd.TypeDocumentId=ltdic.TypeDocumentIdentityCardId\n" +
                    "where a.ActiveAgreement=1 and (d.DocumentId is null or ltdic.TypeDocumentIdentityCardId is not null) \n" +
                    "order by p.PersonId");

            int i = 0;
            ResultSet rs = st.getResultSet();

            while (rs.next())
            {
                System.out.println(rs.getObject(1) + "\t" + rs.getObject(2) + " " + rs.getObject(3) + " " + rs.getObject(4) + "\t" + rs.getObject(5) + "\t" + rs.getObject(6) + "\t" + rs.getObject(7));
                i++;
            }

            System.out.println("------ Total persons count = " + i);*/

            /*st.execute("select top(10) PersonId, Photo from PhotoPerson where Photo is not null order by PersonId");
            ResultSet rs2 = st.getResultSet();

            while (rs2.next())
            {
                String id = rs2.getString(1);
                InputStream stream = rs2.getBinaryStream(2);

                try
                {
                    byte[] data = new byte[stream.available()];
                    stream.read(data);
                    stream.close();

                    int c;
                    StringBuffer strBuff = new StringBuffer();
                    while ((c = stream.read()) != -1)
                    {
                        strBuff.append((char) c);
                    }

                    FileOutputStream fos = new FileOutputStream("D:/1/" + id + ".jpeg");
                    fos.write(data);
                    fos.close();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

            }*/

            /*st.close();
            connection.close();

        } catch (SQLException e)
        {
            e.printStackTrace();
        }     */
    }

    public void onClickBreak()
    {
        RsmuVectorSyncDaemonDao.stopCatalogToSync();
        RsmuVectorSyncDaemonDao.logEvent(Level.INFO, "************* Vector sync was stopped manually ");
    }

    public void onChangeAutoRefresh()
    {
        _autoRefreshEnabled = !_autoRefreshEnabled;
        if (_autoRefreshEnabled) _timerInterval = 2000;
        else _timerInterval = 86400000;
    }

    public String getSyncStatus()
    {
        if (RsmuVectorSyncDaemonDao.isAnyCatalogSyncInProcess())
            return "Идёт синхронизация справочника \"" + RsmuVectorSyncDaemonDao.CURRENT_CATALOG + "\": " + RsmuVectorSyncDaemonDao.CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_PROCESSED + " из " + RsmuVectorSyncDaemonDao.CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL_FILTERED + " (всего " + RsmuVectorSyncDaemonDao.CURRENT_SYNC_CATALOG_ITEMS_AMOUNT_TOTAL + " элементов) завершено.";

        return "Готов к синхронизации";
    }

    public boolean isSyncEnabled()
    {
        return !RsmuVectorSyncDaemonDao.isAnyCatalogSyncInProcess();
    }

    public boolean isAutoRefreshEnabled()
    {
        return _autoRefreshEnabled;
    }

    public void setAutoRefreshEnabled(boolean autoRefreshEnabled)
    {
        _autoRefreshEnabled = autoRefreshEnabled;
    }

    public int getTimerInterval()
    {
        return _timerInterval;
    }

    public void setTimerInterval(int timerInterval)
    {
        _timerInterval = timerInterval;
    }
}