/* $Id:$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSettings;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.core.runtime.ApplicationRuntime;

/**
 * @author Dmitry Seleznev
 * @since 18.11.2015
 */
@Configuration
public class RsmuSettingsManager extends BusinessObjectManager
{
    public static final String VECTOR_DB_USE = "vector.db.use";
    public static final String VECTOR_DB_HOST = "vector.db.host";
    public static final String VECTOR_DB_PORT = "vector.db.port";
    public static final String VECTOR_DB_NAME = "vector.db.name";
    public static final String VECTOR_DB_LOGIN = "vector.db.login";
    public static final String VECTOR_DB_PASSWORD = "vector.db.password";

    public static RsmuSettingsManager instance()
    {
        return instance(RsmuSettingsManager.class);
    }

    /*@Bean
    public IFefuSettingsDao dao()
    {
        return new FefuSettingsDao();
    } */

    public static boolean isVectorDBUse()
    {
        String vectorDbUseStr = ApplicationRuntime.getProperty(VECTOR_DB_USE);
        return null != vectorDbUseStr && Boolean.parseBoolean(vectorDbUseStr);
    }

    public static String getVectorDBParameter(String paramName)
    {
        return ApplicationRuntime.getProperty(paramName);
    }

    public static boolean isAnyConnectionParameterIsNotSpecified()
    {
        if(!isVectorDBUse()) return true;
        String dbHost = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_HOST));
        String dbName = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_NAME));
        String dbLogin = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_LOGIN));
        String dbPassword = StringUtils.trimToNull(RsmuSettingsManager.getVectorDBParameter(RsmuSettingsManager.VECTOR_DB_PASSWORD));
        if (null == dbHost || null == dbName || null == dbLogin || null == dbPassword) return true;
        return false;
    }
}