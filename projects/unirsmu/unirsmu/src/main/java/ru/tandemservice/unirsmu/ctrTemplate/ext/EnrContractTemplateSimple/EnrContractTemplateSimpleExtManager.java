/* $Id:$ */
package ru.tandemservice.unirsmu.ctrTemplate.ext.EnrContractTemplateSimple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleDao;
import ru.tandemservice.unirsmu.ctrTemplate.ext.EnrContractTemplateSimple.logic.EnrContractTemplateSimpleExtDao;

/**
 * @author Denis Perminov
 * @since 14.07.2014
 */
@Configuration
public class EnrContractTemplateSimpleExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEnrContractTemplateSimpleDao dao()
    {
        return new EnrContractTemplateSimpleExtDao();
    }
}