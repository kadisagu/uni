/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 11.02.2015
 */
public class RsmuEnrNotAppearEntrantDao extends UniBaseDao implements IRsmuEnrNotAppearEntrantDao
{
    @Override
    public void updateExamGroupForDiscipline(List<EnrExamPassDiscipline> examPassDisciplineList, EnrExamGroup examGroup)
    {
        for (EnrExamPassDiscipline discipline : examPassDisciplineList)
        {
            discipline.setAbsenceNote(null);
            discipline.syncAbsenceNoteAndMarkState();
            discipline.setExamGroup(examGroup);
            update(discipline);
        }
    }
}
