/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.ContractTransferAdd.RsmuEnrReportContractTransferAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantDataExportEoisAdd.RsmuEnrReportEntrantDataExportEoisAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileCoverAdd.RsmuEnrReportPersonalFileCoverAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileTransferAdd.RsmuEnrReportPersonalFileTransferAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.RatingListAdd.RsmuEnrReportRatingListAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantRegistrationJournalAdd.RsmuEnrReportEntrantRegistrationJournalAddUI;

/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public interface IRsmuEnrReportDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createEntrantRegistrationJournalReport(RsmuEnrReportEntrantRegistrationJournalAddUI enrReportEntrantRegistrationJournalAddUI);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createEntrantRatingListReport(RsmuEnrReportRatingListAddUI enrReportRatingListAddUI);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] createMassPersonalFileReport(RsmuEnrReportPersonalFileCoverAddUI reportParams);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createPersonalFileTransferReport(RsmuEnrReportPersonalFileTransferAddUI reportParams);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createContractTransferReport(RsmuEnrReportContractTransferAddUI reportParams);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void createEntrantDataExportEoisReport(RsmuEnrReportEntrantDataExportEoisAddUI reportParams);
}
