/* $Id$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.StudImportFromExcel;

import jxl.Cell;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.OtherStudentExtract;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unirsmu.dao.daemon.IRsmuVectorSyncDaemonDao;
import ru.tandemservice.unirsmu.dao.daemon.wrapper.AddressWrapper;
import ru.tandemservice.unirsmu.dao.daemon.wrapper.PersonWrapper;
import ru.tandemservice.unirsmu.dao.daemon.wrapper.StudentWrapper;
import ru.tandemservice.unirsmu.entity.RsmuVectorIds;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Dmitry Seleznev
 * @since 18.12.2015
 */
public class StudentImportFromExcelUtil
{
    public static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("dd.MM.yyyy");

    private static String _currentStatus;
    private static String _warning;
    private static String _error;

    private static final int IDX_COLUMN_FACULTY = 0;
    private static final int IDX_COLUMN_LAST_NAME = 1;
    private static final int IDX_COLUMN_FIRST_NAME = 2;
    private static final int IDX_COLUMN_MIDDLE_NAME = 3;
    private static final int IDX_COLUMN_BIRTH_DATE = 4;
    private static final int IDX_COLUMN_SEX = 5;
    private static final int IDX_COLUMN_CITIZENSHIP = 6;
    private static final int IDX_COLUMN_ICARD_TYPE = 7;
    private static final int IDX_COLUMN_ICARD_SERIES = 8;
    private static final int IDX_COLUMN_ICARD_NUMBER = 9;
    private static final int IDX_COLUMN_ICARD_ISS_DATE = 10;
    private static final int IDX_COLUMN_ICARD_ISS_PLACE = 11;
    private static final int IDX_COLUMN_ICARD_ISS_CODE = 12;
    private static final int IDX_COLUMN_ADDR_FACT = 13;
    private static final int IDX_COLUMN_ADDR_REG = 14;
    private static final int IDX_COLUMN_STATUS = 15;
    private static final int IDX_COLUMN_STATUS_TYPE = 16;
    private static final int IDX_COLUMN_STATUS_CHANGE_DATE = 17;
    private static final int IDX_COLUMN_ENR_DATE = 18;
    private static final int IDX_COLUMN_COURSE = 19;
    private static final int IDX_COLUMN_EDU_FORM = 20;
    private static final int IDX_COLUMN_LAST_ORDER_NUMBER = 21;
    private static final int IDX_COLUMN_LAST_ORDER_DATE = 22;
    private static final int IDX_COLUMN_LAST_ORDER_TYPE = 23;
    private static final int IDX_COLUMN_EDU_LEVEL = 24;
    private static final int IDX_COLUMN_EDU_PERIOD = 25;

    private static Map<String, String> sexMap = new HashMap<>();
    //private static Map<String, String> eduFormMap = new HashMap<>();

    public static void waitForCall()
    {
        _currentStatus = "Ожидание загрузки Excel-файла.";
        _warning = null;
        _error = null;
    }

    public static String getCurrentStatus()
    {
        return _currentStatus;
    }

    public static String getError()
    {
        return _error;
    }

    public static String getWarning()
    {
        return _warning;
    }

    public static void clearStatus()
    {
        _currentStatus = null;
    }

    public static void clearError()
    {
        _error = null;
        _warning = null;
    }

    static
    {
        sexMap.put("женский", "Ж");
        sexMap.put("мужской", "М");
        sexMap.put("муж.", "М");
    }


    public static void main(String[] args)
    {
        /*String[] dates = new String[] { "3.11.1981", "03 . 10/ 2010 fdg", "1 2/ 03. 980-" };
        for(String dateStr : dates) System.out.println(DateFormatter.DEFAULT_DATE_FORMATTER.format(getCorrectedDate(dateStr)));*/

        /*String[] series = new String[]{"", " 45 08", " 45 14", "/0309", "/0710", "0", "1207", "1208", "2609�", "29 13�", "29006", "2901", "2902", "45  06", "45 07�", "45 08", "4508_mbf", "4509.",
                "4513�", "46  02", "82-10", "9211", "9511", "96 10", "9911", "AZE", "H", "HB", "P KAZ", "kaz", "В", "КН", "О713", "РР AZE", "С", "мр", "р", "рр", "�2913", "#4514", "№7014"};

        for (String ser : series) System.out.println(ser + "\t-\t" + getPassportSeriesNumberCorrected(ser));*/

        String[] codes = new String[]{"", "  110-004", " 020-022", " 060-004", " 080-001", " 500-179", " 770-042", " 770-074", " 770-085", " 770-117", " 772-059", " 772-078", "000-000", "000000", "010-001", "010-005", "020-004", "020-005", "020-007", "020-008", "020-010", "020-011", "020-012", "020-018", "020-020", "020-022", "020-023", "020-024", "020-025", "020-026", "020-028", "020-043", "020-053", "020-063", "030-002", "030-003", "030-004", "030-005", "030-007", "030-015", "030-016", "030-019", "030-025", "030-030", "030-038", "030-040", "050-002", "050-003", "050-004", "050-005", "050-006", "050-007", "050-008", "050-009", "050-010", "050-011", "050-012", "050-013", "050-014", "050-015", "050-016", "050-017", "050-023", "050-025", "050-026", "050-028", "050-029", "050-031", "050-033", "050-034", "050-036", "050-037", "050-039", "050-040", "050-041", "050-043", "050-046", "050-047", "050-048", "050-049", "050-050", "050-051", "050-052", "050-053", "050-054", "050-058", "050-060", "052-002", "052-010", "052-032", "060-002", "060-003", "060-004", "060-005", "060-006", "060-007", "070-001", "070-002", "070-003", "070-004", "070-005", "070-006", "070-007", "070-008", "070-009", "070-010", "070-011", "070-012", "077-031", "080 - 001", "080-001", "080-003", "080-004", "080-005", "080-006", "080-010", "080-012", "080-014", "082-012", "090-001", "090-002", "090-003", "090-004", "090-005", "090-007", "090-008", "090-009", "090-010", "090-012", "090-013", "100-002", "110-003", "110-013", "120-001", "120-020", "12101", "130-001", "130-002", "130-003", "130-011", "130-013", "130-014", "130-019", "130-022", "130-025", "132-002", "14,29,1", "14.38.1", "140-002", "140-003", "140-004", "140-006", "140-016", "140-019", "140-026", "140-029", "140-036", "150-002", "150-003", "150-004", "150-005", "150-006", "150-007", "150-008", "150-009", "150-013", "150006", "160-003", "160-008", "160-012", "160-014", "160-015", "160-017", "160-018", "160-020", "160-021", "160-025", "160-027", "160-030", "160-032", "160-038", "160-049", "160-115", "162-060", "170- 117", "170-001", "170-002", "170-003", "170-005", "170-010", "170-011", "172-001", "172-018", "180--003", "180-002", "180-003", "180-005", "180-006", "180-008", "180-009", "180-010", "180-016", "182-008", "190-002", "200-005", "200-006", "200-007", "200-008", "200-010", "200-011", "200-014", "200-015", "200-017", "210-003", "210-004", "210-007", "210-008", "210-010", "210-011", "210-012", "210-014", "210-016", "210-017", "210-018", "210-019", "210-020", "210-022", "210-024", "210-025", "210-026", "212-026", "220-044", "220-069", "220-071", "220-073", "230-004", "230-006", "230-007", "230-008", "230-009", "230-010", "230-011", "230-012", "230-013", "230-014", "230-015", "230-016", "230-018", "230-019", "230-020", "230-025", "230-027", "230-029", "230-032", "230-033", "230-036", "230-040", "230-042", "230-047", "230-049", "230-050", "230-052", "230-054", "230-060", "232-029", "240-003", "240-013", "250-036", "250-059", "260-004", "260-006", "260-007", "260-010", "260-013", "260-014", "260-019", "260-021", "260-022", "260-023", "260-026", "260-027", "260-028", "260-029", "260-030", "260-033", "260-034", "260-035", "260-036", "262-016", "270-001", "270-003", "270-006", "270-007", "270-009", "280-002", "280-003", "290-002", "290-020", "290-025", "300-003", "300-005", "310-002", "310-003", "310-004", "310-006", "310-007", "310-014", "310-015", "310-020", "310-021", "310-022", "310-024", "310-029", "320 - 008", "320-001", "320-002", "320-003", "320-004", "320-006", "320-008", "320-009", "320-010", "320-012", "320-016", "320-017", "320-019", "320-022", "320-023", "320-028", "320-030", "320-031", "320-032", "322-019", "330-001", "330-003", "330-005", "330-008", "330-011", "330-012", "330-013", "330-014", "330-017", "330-018", "330-020", "330-024", "330-027", "330-028", "330-029", "330-030", "330-040", "330-070", "332-001", "340--027", "340-001", "340-003", "340-004", "340-005", "340-007", "340-011", "340-016", "340-018", "340-019", "340-024", "340-027", "340-028", "340-030", "340-038", "340-041", "340-045", "340-099", "350-002", "350-003", "350-010", "353-004", "360-004", "360-005", "360-006", "360-007", "360-010", "360-013", "360-015", "360-020", "360-024", "360-025", "360-033", "360-036", "360-037", "360-041", "362-011", "362-025", "362-037", "370-002", "370-005", "370-013", "370-026", "370-027", "372-014", "380-013", "390-003", "390-004", "390-005", "390-008", "390-013", "390-014", "390-023", "400-001", "400-002", "400-003", "400-004", "400-005", "400-006", "400-007", "400-008", "400-009", "400-011", "400-013", "400-014", "400-016", "400-017", "400-018", "400-019", "400-020", "400-023", "400-024", "400-025", "400-026", "400-027", "400-028", "400-031", "400-032", "400-033", "400-034", "400-035", "400-037", "400-040", "402-004", "410-001", "410-005", "410-015", "420-003", "420-020", "420-022", "420-025", "420-028", "420-040", "43-030", "430-007", "430-030", "430-040", "430-041", "440-001", "440-002", "440-003", "440-015", "440-016", "440-017", "450-002", "460-002", "460-003", "460-004", "460-007", "460-013", "460-015", "460-021", "460-022", "460-032", "462-014", "470-062", "480-002", "480-003", "480-003 ", "480-004", "480-008", "480-010", "480-012", "480-013", "480-014", "480-015", "480-017", "480-019", "480-020", "480-022", "480-024", "490-001", "500-006", "500-007", "500-008", "500-009", "500-010", "500-010�", "500-011", "500-012", "500-014", "500-015", "500-016", "500-017", "500-018", "500-019", "500-020", "500-021", "500-022", "500-023", "500-024", "500-025", "500-027", "500-028", "500-029", "500-030", "500-034", "500-035", "500-036", "500-037", "500-038", "500-039", "500-040", "500-041", "500-042", "500-043", "500-044", "500-046", "500-050", "500-051", "500-052", "500-053", "500-054", "500-055", "500-056", "500-057", "500-059", "500-060", "500-061", "500-063", "500-064", "500-065", "500-066", "500-067", "500-068", "500-070", "500-073", "500-074", "500-075", "500-076", "500-077", "500-078", "500-079", "500-080", "500-081", "500-082", "500-084", "500-085", "500-086", "500-087", "500-089", "500-091", "500-092", "500-093", "500-096", "500-097", "500-097 ", "500-098", "500-099", "500-100", "500-102", "500-103", "500-104", "500-105", "500-106", "500-107", "500-108", "500-109", "500-111", "500-112", "500-113", "500-114", "500-115", "500-116", "500-117", "500-118", "500-120", "500-121", "500-122", "500-123", "500-124", "500-125", "500-126", "500-127", "500-128", "500-129", "500-130", "500-132", "500-133", "500-134", "500-135", "500-136", "500-138", "500-139", "500-140", "500-141", "500-142", "500-143", "500-144", "500-147", "500-148", "500-149", "500-150", "500-151", "500-152", "500-153", "500-155", "500-156", "500-157", "500-158", "500-159", "500-160", "500-161", "500-162", "500-164", "500-166", "500-168", "500-170", "500-171", "500-172", "500-173", "500-174", "500-175", "500-178", "500-179", "500-180", "500-181", "500-182", "500-185", "500-186", "500-187", "500-188", "500-190", "500-191", "500-192", "500-195", "500-199", "500-208", "500-212", "500-219", "500-221", "500-222", "500-224", "500-225", "500-226", "500-227", "500-228", "500-229", "500-230", "500-232", "500-233", "500-234", "50015", "500173", "502-006", "502-014", "502-026", "502-034", "502-035", "502-049", "503-002", "503-004", "503-005", "503-008", "503-010", "503-013", "503-023", "503-039", "503-050", "503-053", "503-057", "503-062", "503-067", "503-097", "503-100", "503-105", "503-112", "503-114", "503-124", "503-141", "503-179", "503112", "505-011", "510-001", "510-002", "510-003", "510-018", "520-003", "520-005", "520-006", "520-007", "520-009", "520-010", "520-014", "520-019", "520-030", "520-053", "522-020", "530-017", "550-001", "550-006", "550-038", "550-074", "550-168", "550-186", "560-004", "560-006", "560-007", "560-009", "560-034", "560-039", "562-042", "570-002", "570-005", "570-014", "570-015", "570-017", "570-030", "570-032", "570-034", "570-035", "570-036", "580-001", "580-002", "580-003", "580-004", "580-005", "580-006", "580-007", "580-013", "580-020", "580-024", "580-032", "590-006", "590-042", "590-048", "600-002", "600-003", "600-009", "600-011", "602-002", "610-001", "610-002", "610-005", "610-007", "610-008", "610-009", "610-012", "610-013", "610-016", "610-018", "610-024 ", "610-026", "610-028", "610-039", "610-053", "610-054", "610-056", "610-058", "610-062", "612-002", "620-008", "620-013", "620-015", "620-019", "620-020", "620-023", "620-025", "620-032", "620-033", "620-034", "622-008", "630-001", "630-002", "630-003", "630-005", "630-006", "630-007", "630-009", "630-010", "630-021", "630-033", "630-035", "630-040", "640-001", "640-004", "640-005", "640-006", "640-011", "640-019", "640-036", "640-044", "642-046", "650-002", "650-019", "652-001", "660-009", "660-010", "660-011", "660-022", "670-001", "670-002", "670-006", "670-009", "670-010", "670-025", "670-027", "672-006", "672-009", "680-001", "680-002", "680-003", "680-004", "680-005", "680-006", "680-007", "680-008", "680-012", "680-013", "680-015", "680-016", "680-017", "680-019", "680-020", "680-023", "680-024", "680-025", "680-026", "680-027", "680-050", "680027", "682-012", "682-016", "690-002", "690-004", "690-005", "690-007", "690-010", "690-013", "690-015", "690-017", "690-018", "690-030", "690-034", "690-036", "690-040", "690-042", "700-005", "700-009", "700-014", "700-036", "700-097", "700-106", "710-002", "710-004", "710-006", "710-008", "710-009", "710-010", "710-012", "710-013", "710-015", "710-018", "710-020", "710-021", "710-022", "710-024", "710-025", "710-026", "710-027", "710-030", "710-031", "710-031 ", "710-034", "710-035", "710-037", "710-038", "710-040", "710-041", "710-043", "710-044", "710-104", "710041", "712-002", "720-002", "720-004", "720-020", "720-122", "730-001", "730-003", "730-005", "730-008", "730-009", "730-015", "730-028", "732-024", "7330-028", "740-021", "740-026", "740-027", "740-047", "740-049", "740-052", "740-055", "740-056", "742-042", "750-002", "750-046", "750-048", "760-002", "760-003", "760-005", "760-006", "760-007", "760-008", "760-009", "760-013", "77-011", "77-074", "770 059", "770- 093 ", "770-001", "770-003", "770-004", "770-005", "770-006", "770-006�", "770-007", "770-008", "770-009", "770-010", "770-011", "770-012", "770-013", "770-014", "770-015", "770-016", "770-017", "770-018", "770-019", "770-020", "770-021", "770-022", "770-023", "770-024", "770-025", "770-026", "770-027", "770-028", "770-029", "770-029                                                       ", "770-029�", "770-030", "770-031", "770-032", "770-033", "770-034", "770-035", "770-036", "770-037", "770-038", "770-039", "770-040", "770-041", "770-042", "770-043", "770-044", "770-045", "770-046", "770-047", "770-048", "770-049", "770-050", "770-051", "770-052", "770-053", "770-054", "770-055", "770-056", "770-057", "770-058", "770-059", "770-060", "770-061", "770-062", "770-063", "770-064", "770-066", "770-067", "770-068", "770-069", "770-070", "770-071", "770-072", "770-073", "770-074", "770-075", "770-076", "770-077", "770-078", "770-079", "770-080", "770-081", "770-082", "770-083", "770-084", "770-085", "770-086", "770-087", "770-088", "770-089", "770-090", "770-091", "770-092", "770-093", "770-094", "770-095", "770-096", "770-097", "770-098", "770-099", "770-100", "770-101", "770-102", "770-103", "770-104", "770-105", "770-106", "770-107", "770-108", "770-109", "770-110", "770-110 ", "770-111", "770-112", "770-113", "770-114", "770-115", "770-116", "770-116�", "770-117", "770-117                                                                      ", "770-118", "770-119", "770-120", "770-121", "770-122", "770-123", "770-124", "770-124�", "770-125", "770-126", "770-127", "770-128", "770-129", "770-135", "770-140", "770-141", "770-142", "770-143", "770-144", "770-146", "770-161", "770-346", "770/041", "770/070", "770004", "770038", "770069", "770117", "770118", "770122", "770125", "770–106", "770—028", "770�117", "771-001", "772-004", "772-005", "772-009", "772-011", "772-013", "772-014", "772-015", "772-016", "772-017", "772-018", "772-022", "772-023", "772-025", "772-026", "772-029", "772-031", "772-034", "772-036", "772-037", "772-038", "772-040", "772-041", "772-042", "772-043", "772-045", "772-047", "772-051", "772-052", "772-054", "772-055", "772-058", "772-059", "772-060", "772-061", "772-065", "772-067", "772-069", "772-070", "772-071", "772-074", "772-076", "772-077", "772-078", "772-079", "772-080", "772-083", "772-084", "772-085", "772-086", "772-087", "772-089", "772-091", "772-092", "772-093", "772-094", "772-095", "772-096", "772-097", "772-099", "772-100", "772-101", "772-102", "772-103", "772-104", "772-108", "772-112", "772-118", "772-120", "772-124", "772-128", "772-132", "772-149", "772-156", "773-027", "773-035", "773-053", "780-019", "780-022", "780-023", "780-075", "780-091", "782-041", "790-002", "860-004", "860-011", "860-015", "860-020", "860-026", "860-028", "860-040", "890-002", "890-003", "890-006", "890-008", "890-012", "890-027", "890-028", "900-002", "900-003", "900-004", "910-013", "910-023", "920-001", "920-004", "KAZ", "ак", "белорус", "замена", "отч", "�500-010", "�500-091", "�500-208", "�710-006", "�770 082", "�770-064", "�770-104",};
        for (String code : codes) System.out.println(code + "\t-\t" + getPassportIssuanceCodeCorrected(code));
    }

    public static void importStudents(InputStream excelStream)
    {
        Workbook book = null;
        try
        {
            book = Workbook.getWorkbook(excelStream);
        } catch (IOException io)
        {
            _error = "Невозможно прочитать файл. Вероятно файл имеет формат, отличный от Excel 97-2003, либо поврежден.";
            throw new ApplicationException("Невозможно прочитать файл. Вероятно файл имеет формат, отличный от Excel 97-2003, либо поврежден.");
        } catch (BiffException biff)
        {
            _error = "Невозможно прочитать файл. Вероятно файл имеет формат, отличный от Excel 97-2003, либо поврежден.";
            throw new ApplicationException("Невозможно прочитать файл. Вероятно файл имеет формат, отличный от Excel 97-2003, либо поврежден.");
        }

        Set<String> occVectorId = new HashSet<>();
        List<PersonWrapper> personWrappersList = new ArrayList<>();
        Map<String, AddressWrapper> factAddrMap = new HashMap<>();
        Map<String, AddressWrapper> regAddrMap = new HashMap<>();
        Map<String, String> personToFacultyNameMap = new HashMap<>();
        Map<String, StudentWrapper> studentWrappersMap = new HashMap<>();

        int count = 0;
        int doubles = 0;
        Set<String> keysSet = new HashSet<>();
        //Map<String, Integer> countMap = new HashMap<>();

        _currentStatus = "Чтение Excel-файла.";
        for (Sheet sheet : book.getSheets())
        {
            if (sheet.isHidden()) continue;

            for (int i = 1; i < sheet.getRows(); i++)
            {
                Cell[] cells = sheet.getRow(i);
                if(checkLineIsEmpty(cells)) continue;

                count++;
                PersonWrapper wrapper = new PersonWrapper();

                if(null == _warning && cells.length < 25)
                {
                    _warning = "Одна, или несколько записей содержат незаполненные поля. Все недозаполненные записи будут проигнорированы при импорте данных.";
                }

                wrapper.setVectorId(String.valueOf(System.currentTimeMillis()));
                while (occVectorId.contains(wrapper.getVectorId()))
                    wrapper.setVectorId(String.valueOf(System.currentTimeMillis()));

                wrapper.setLastName(StringUtils.trimToNull(cells[IDX_COLUMN_LAST_NAME].getContents()));
                wrapper.setFirstName(StringUtils.trimToNull(cells[IDX_COLUMN_FIRST_NAME].getContents()));
                wrapper.setMiddleName(StringUtils.trimToNull(cells[IDX_COLUMN_MIDDLE_NAME].getContents()));
                wrapper.setBirthDate(getCorrectedDate(cells[IDX_COLUMN_BIRTH_DATE]));

                if (wrapper.getKey().isEmpty()) continue;

                if (keysSet.contains(wrapper.getKey()))
                {
                    doubles++;
                    System.out.println(wrapper.getFioWithBirthDate());
                }
                keysSet.add(wrapper.getKey());

                wrapper.setSex(getSexCorrected(cells[IDX_COLUMN_SEX].getContents()));
                wrapper.setCitizenship(StringUtils.trimToNull(cells[IDX_COLUMN_CITIZENSHIP].getContents()));
                String icTypeStr = StringUtils.trimToNull(cells[IDX_COLUMN_ICARD_TYPE].getContents());
                wrapper.setIcTypeId(null != icTypeStr ? icTypeStr.toLowerCase() : null);
                wrapper.setIcTypeStr(cells[IDX_COLUMN_ICARD_TYPE].getContents());
                wrapper.setIcSeries(getPassportSeriesNumberCorrected(cells[IDX_COLUMN_ICARD_SERIES].getContents()));
                wrapper.setIcNumber(getPassportSeriesNumberCorrected(cells[IDX_COLUMN_ICARD_NUMBER].getContents()));
                wrapper.setIcIssuanceDate(getCorrectedDate(cells[IDX_COLUMN_ICARD_ISS_DATE]));
                wrapper.setIcIssuancePlace(StringUtils.trimToNull(cells[IDX_COLUMN_ICARD_ISS_PLACE].getContents()));
                wrapper.setIcIssuancePlaceCode(getPassportIssuanceCodeCorrected(cells[IDX_COLUMN_ICARD_ISS_CODE].getContents()));

                personToFacultyNameMap.put(wrapper.getVectorId(), sheet.getName());
                occVectorId.add(wrapper.getVectorId());
                personWrappersList.add(wrapper);

                StudentWrapper studWrapper = new StudentWrapper();
                studWrapper.setVectorId(wrapper.getVectorId());
                studWrapper.setFacultyName(StringUtils.trimToNull(sheet.getName()));
                studWrapper.setPersonalNumber(StringUtils.trimToNull(studWrapper.getVectorId()));
                studWrapper.setEduFormName(StringUtils.trimToNull(cells[IDX_COLUMN_EDU_FORM].getContents()));
                studWrapper.setLastSemesterStatusId(StringUtils.trimToNull(cells[IDX_COLUMN_STATUS_TYPE].getContents().toLowerCase()));
                studWrapper.setLastSemesterStatusName(StringUtils.trimToNull(cells[IDX_COLUMN_STATUS_TYPE].getContents()));
                studWrapper.setEnrOrderEnrDate(getCorrectedDate(cells[IDX_COLUMN_ENR_DATE]));
                studWrapper.setLastSemesterNumber("1");

                if(cells.length > IDX_COLUMN_EDU_PERIOD)
                {
                    studWrapper.setDirectionName(StringUtils.trimToNull(cells[IDX_COLUMN_EDU_LEVEL].getContents()));
                    studWrapper.setEduDuration(StringUtils.trimToNull(cells[IDX_COLUMN_EDU_PERIOD].getContents()));
                }

                try
                {
                    String courseNumStr = StringUtils.trimToNull(cells[IDX_COLUMN_COURSE].getContents());
                    if (null != courseNumStr)
                        studWrapper.setLastSemesterNumber(String.valueOf(Integer.parseInt(courseNumStr) * 2));
                } catch (NumberFormatException ex)
                {
                }

                Date enrDate = getCorrectedDate(cells[IDX_COLUMN_ENR_DATE]);
                if (null != enrDate)
                {
                    GregorianCalendar calendar = new GregorianCalendar();
                    calendar.setTime(enrDate);
                    studWrapper.setEnrYear(calendar.get(Calendar.YEAR));
                }

                String orderTypeStr = StringUtils.trimToNull(cells[IDX_COLUMN_LAST_ORDER_TYPE].getContents());
                if (null != orderTypeStr)
                {
                    Date orderDate = getCorrectedDate(cells[IDX_COLUMN_LAST_ORDER_DATE]);
                    if (orderTypeStr.toLowerCase().contains("зачисле"))
                    {
                        studWrapper.setEnrOrderDate(orderDate);
                        studWrapper.setEnrOrderNumber(StringUtils.trimToNull(cells[IDX_COLUMN_LAST_ORDER_NUMBER].getContents()));
                    } else
                    {
                        studWrapper.setLastOrderDate(orderDate);
                        studWrapper.setLastOrderNumber(StringUtils.trimToNull(cells[IDX_COLUMN_LAST_ORDER_NUMBER].getContents()));
                        studWrapper.setLastOrderTypeName(orderTypeStr);
                    }
                }

                studentWrappersMap.put(studWrapper.getVectorId(), studWrapper);

                String factAddrStr = StringUtils.trimToNull(cells[IDX_COLUMN_ADDR_FACT].getContents());
                if (null != factAddrStr)
                {
                    AddressWrapper factAddr = new AddressWrapper();
                    factAddr.setPersonId(wrapper.getVectorId());
                    factAddr.setCity(factAddrStr);
                    factAddrMap.put(wrapper.getVectorId(), factAddr);
                }

                String regAddrStr = StringUtils.trimToNull(cells[IDX_COLUMN_ADDR_REG].getContents());
                if (null != regAddrStr)
                {
                    AddressWrapper regAddr = new AddressWrapper();
                    regAddr.setPersonId(wrapper.getVectorId());
                    regAddr.setCity(regAddrStr);
                    regAddrMap.put(wrapper.getVectorId(), regAddr);
                }
            }
        }

        System.out.println("Students = " + count + ", Doubles = " + doubles);
        _currentStatus = "Найдено " + count + " физ. лиц. Количество дублей " + doubles + " шт. Идёт обработка данных перед записью в базу данных.";

        List<String> vectorIdsList = new ArrayList<>();
        List<String> studentVectorIdsList = new ArrayList<>();
        Map<String, Person> personMap = getPersonsMap();
        Map<String, Person> studPersonMap = getStudentPersonsMap();
        Map<Long, Student> studentsMap = getPersonIdToStudentsMap();

        List<String> updatedPersons = new ArrayList<>();
        List<String> updatedIcards = new ArrayList<>();
        List<Long> addressToDelList = new ArrayList<>();
        List<AddressBase> addressToCreateList = new ArrayList<>();
        Map<String, AddressBase> regAddressMap = new HashMap<>();
        Map<String, AddressBase> factAddressMap = new HashMap<>();
        final Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> newPersonsMap = new HashMap<>();
        Map<String, Person> updatedPersonsMap = new HashMap<>();
        Map<String, IdentityCard> updatedIcardsMap = new HashMap<>();
        Set<String> processedKeys = new HashSet<>();

        List<Student> updatedStudentsList = new ArrayList<>();
        Map<String, CoreCollectionUtils.Pair<Student, RsmuVectorIds>> studentVectorIdsMap = new HashMap<>();
        Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> personDummyMap = new HashMap<>();

        for (PersonWrapper wrapper : personWrappersList)
        {
            String key = wrapper.getKey();
            if (processedKeys.contains(key)) continue;
            processedKeys.add(key);

            AddressWrapper regAddressWrapper = regAddrMap.get(wrapper.getVectorId());
            AddressWrapper factAddressWrapper = factAddrMap.get(wrapper.getVectorId());
            AddressBase regAddress = null != regAddressWrapper ? regAddressWrapper.generateAddress() : null;
            AddressBase factAddress = null != factAddressWrapper ? factAddressWrapper.generateAddress() : null;

            if (null != regAddress)
            {
                regAddressMap.put(wrapper.getVectorId(), regAddress);
                addressToCreateList.add(regAddress);
            }
            if (null != factAddress)
            {
                factAddressMap.put(wrapper.getVectorId(), factAddress);
                addressToCreateList.add(factAddress);
            }

            Person newPerson = wrapper.generatePerson();
            Person oldPerson = studPersonMap.get(key);

            if (null == oldPerson)
            {
                String facultyName = personToFacultyNameMap.get(wrapper.getVectorId());
                if (null != facultyName) oldPerson = personMap.get(facultyName.toLowerCase() + " - " + key);
            }
            if (null == oldPerson) oldPerson = personMap.get(key);

            if (null != oldPerson)
            {
                IdentityCard newIc = newPerson.getIdentityCard();
                IdentityCard oldIc = oldPerson.getIdentityCard();
                if (!"-".equals(newIc.getLastName())) oldIc.setLastName(newIc.getLastName());
                if (!"-".equals(newIc.getFirstName())) oldIc.setFirstName(newIc.getFirstName());
                if (null != newIc.getMiddleName()) oldIc.setMiddleName(newIc.getMiddleName());
                if (null != newIc.getBirthDate()) oldIc.setBirthDate(newIc.getBirthDate());
                if (null != newIc.getCitizenship()) oldIc.setCitizenship(newIc.getCitizenship());
                if (null != newIc.getSex()) oldIc.setSex(newIc.getSex());
                if (null != newIc.getSeria()) oldIc.setSeria(newIc.getSeria());
                if (null != newIc.getNumber()) oldIc.setNumber(newIc.getNumber());
                if (null != newIc.getIssuanceDate()) oldIc.setIssuanceDate(newIc.getIssuanceDate());
                if (null != newIc.getIssuancePlace()) oldIc.setIssuancePlace(newIc.getIssuancePlace());
                if (null != newIc.getIssuanceCode()) oldIc.setIssuanceCode(newIc.getIssuanceCode());
                if (null != newIc.getCardType()) oldIc.setCardType(newIc.getCardType());

                if (null != regAddress)
                {
                    if (null == oldIc.getAddress() || (oldIc.getAddress() instanceof AddressString && !oldIc.getAddress().getTitle().equals(regAddress.getTitle())))
                    {
                        if (null != oldIc.getAddress()) addressToDelList.add(oldIc.getAddress().getId());
                        updatedIcardsMap.put(wrapper.getVectorId(), oldIc);
                        updatedIcards.add(wrapper.getVectorId());
                    }
                }

                if (null != factAddress)
                {
                    if (null == oldPerson.getAddress() || (oldPerson.getAddress() instanceof AddressString && !oldPerson.getAddress().getTitle().equals(factAddress.getTitle())))
                    {
                        if (null != oldPerson.getAddress()) addressToDelList.add(oldPerson.getAddress().getId());
                        newPersonsMap.put(wrapper.getVectorId(), new CoreCollectionUtils.Pair(oldPerson, null));
                        updatedPersonsMap.put(wrapper.getVectorId(), oldPerson);
                        updatedPersons.add(wrapper.getVectorId());
                    }
                }

                updatedIcards.add(wrapper.getVectorId());
                updatedIcardsMap.put(wrapper.getVectorId(), oldIc);
                personDummyMap.put(wrapper.getVectorId(), new CoreCollectionUtils.Pair(oldPerson, null));
            } else
            {
                newPersonsMap.put(wrapper.getVectorId(), new CoreCollectionUtils.Pair(newPerson, null));
                personDummyMap.put(wrapper.getVectorId(), new CoreCollectionUtils.Pair(newPerson, null));
                vectorIdsList.add(wrapper.getVectorId());
            }
        }


        final Map<String, Integer> cnt = new HashMap<>();

        cnt.put("cnt", 0);
        System.out.println("Creating " + addressToCreateList.size() + " addresses.");
        _currentStatus = "Создание " + addressToCreateList.size() + " адресов физ. лиц.";
        BatchUtils.execute(addressToCreateList, 200, new BatchUtils.Action<AddressBase>()
        {
            @Override
            public void execute(Collection<AddressBase> elements)
            {
                IRsmuVectorSyncDaemonDao.instance.get().createAddresses(new ArrayList(elements));
                cnt.put("cnt", Math.min(cnt.get("cnt") + 200, addressToCreateList.size()));
                _currentStatus = "Создание " + addressToCreateList.size() + " адресов физ. лиц. Создано " + cnt.get("cnt");
            }
        });
        System.out.println("FINISHED Creating " + addressToCreateList.size() + " addresses.");


        for (Map.Entry<String, Person> entry : updatedPersonsMap.entrySet())
        {
            AddressBase factAddress = factAddressMap.get(entry.getKey());
            if (null != factAddress) entry.getValue().setAddress(factAddress);
        }

        cnt.put("cnt", 0);
        System.out.println("Updating " + updatedPersons.size() + " persons (address changed).");
        _currentStatus = "Обновление " + updatedPersons.size() + " физ. лиц в части новых адресов.";
        BatchUtils.execute(updatedPersons, 200, new BatchUtils.Action<String>()
        {
            @Override
            public void execute(Collection<String> elements)
            {
                IRsmuVectorSyncDaemonDao.instance.get().savePersonsList(new ArrayList(elements), newPersonsMap, false);
                cnt.put("cnt", Math.min(cnt.get("cnt") + 200, updatedPersons.size()));
                _currentStatus = "Обновление " + updatedPersons.size() + " физ. лиц в части новых адресов. Обновлено " + cnt.get("cnt");
            }
        });
        System.out.println("FINISHED Updating " + updatedPersons.size() + " persons (address changed).");


        for (Map.Entry<String, IdentityCard> entry : updatedIcardsMap.entrySet())
        {
            AddressBase regAddress = regAddressMap.get(entry.getKey());
            if (null != regAddress) entry.getValue().setAddress(regAddress);
        }

        cnt.put("cnt", 0);
        System.out.println("Updating " + updatedIcards.size() + " identity cards.");
        _currentStatus = "Обновление " + updatedIcards.size() + " документов, удостоверяющих личность.";
        BatchUtils.execute(updatedIcards, 200, new BatchUtils.Action<String>()
        {
            @Override
            public void execute(Collection<String> elements)
            {
                IRsmuVectorSyncDaemonDao.instance.get().updateIdentityCards(new ArrayList(elements), updatedIcardsMap);
                cnt.put("cnt", Math.min(cnt.get("cnt") + 200, updatedIcards.size()));
                _currentStatus = "Обновление " + updatedIcards.size() + " документов, удостоверяющих личность. Обновлено " + cnt.get("cnt");
            }
        });
        System.out.println("FINISHED Updating " + updatedIcards.size() + " identity cards.");


        for (Map.Entry<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> entry : newPersonsMap.entrySet())
        {
            AddressBase regAddress = regAddressMap.get(entry.getKey());
            AddressBase factAddress = factAddressMap.get(entry.getKey());

            if (null != regAddress) entry.getValue().getX().getIdentityCard().setAddress(regAddress);
            if (null != factAddress) entry.getValue().getX().setAddress(factAddress);
        }

        cnt.put("cnt", 0);
        System.out.println("Creating " + vectorIdsList.size() + " new persons.");
        _currentStatus = "Создание " + vectorIdsList.size() + " новых физ. лиц.";
        BatchUtils.execute(vectorIdsList, 200, new BatchUtils.Action<String>()
        {
            @Override
            public void execute(Collection<String> elements)
            {
                IRsmuVectorSyncDaemonDao.instance.get().savePersonsList(new ArrayList(elements), newPersonsMap, false);
                cnt.put("cnt", Math.min(cnt.get("cnt") + 200, vectorIdsList.size()));
                _currentStatus = "Создание " + vectorIdsList.size() + " новых физ. лиц. Создано " + cnt.get("cnt");
            }
        });
        System.out.println("FINISHED Creating " + vectorIdsList.size() + " new persons.");


        for (PersonWrapper wrapper : personWrappersList)
        {
            StudentWrapper studWrapper = studentWrappersMap.get(wrapper.getVectorId());
            CoreCollectionUtils.Pair<Person, RsmuVectorIds> personPair = personDummyMap.get(wrapper.getVectorId());

            if(null != studWrapper && null != personPair)
            {
                studentVectorIdsList.add(wrapper.getVectorId());
                Student newStudent = studWrapper.generateStudent(personDummyMap);

                if (null != newStudent)
                {
                    Student oldStudent = studentsMap.get(personPair.getX().getId());
                    if (null != oldStudent)
                    {
                        if (null != newStudent.getCourse()) oldStudent.setCourse(newStudent.getCourse());
                        if (null != newStudent.getStatus()) oldStudent.setStatus(newStudent.getStatus());
                        if (null != newStudent.getEducationOrgUnit()) oldStudent.setEducationOrgUnit(newStudent.getEducationOrgUnit());
                        if (newStudent.getEntranceYear() > 1999) oldStudent.setEntranceYear(newStudent.getEntranceYear());
                        studentVectorIdsMap.put(studWrapper.getVectorId(), new CoreCollectionUtils.Pair(oldStudent, null));
                        updatedStudentsList.add(oldStudent);
                    } else
                    {
                        studentVectorIdsMap.put(studWrapper.getVectorId(), new CoreCollectionUtils.Pair(newStudent, null));
                        updatedStudentsList.add(newStudent);
                    }
                }
            }
        }

        cnt.put("cnt", 0);
        System.out.println("Creating/updating " + studentVectorIdsList.size() + " students.");
        _currentStatus = "Создание/обновление " + studentVectorIdsList.size() + " студентов.";
        BatchUtils.execute(studentVectorIdsList, 200, new BatchUtils.Action<String>()
        {
            @Override
            public void execute(Collection<String> elements)
            {
                IRsmuVectorSyncDaemonDao.instance.get().saveStudentsList(new ArrayList(elements), studentVectorIdsMap, studentWrappersMap);
                cnt.put("cnt", Math.min(cnt.get("cnt") + 200, studentVectorIdsList.size()));
                _currentStatus = "Создание/обновление " + studentVectorIdsList.size() + " студентов. Создано/обновлено " + cnt.get("cnt");
            }
        });
        System.out.println("FINISHED Creating/updating " + studentVectorIdsList.size() + " students.");


        cnt.put("cnt", 0);
        System.out.println("Deleting " + addressToDelList.size() + " old addresses.");
        _currentStatus = "Удаление " + addressToDelList.size() + " адресов, утративших актуальность.";
        BatchUtils.execute(addressToDelList, 200, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                IRsmuVectorSyncDaemonDao.instance.get().deleteAddresses(new ArrayList(elements));
                cnt.put("cnt", Math.min(cnt.get("cnt") + 200, addressToDelList.size()));
                _currentStatus = "Удаление " + addressToDelList.size() + " адресов, утративших актуальность. Удалено " + cnt.get("cnt");
            }
        });
        System.out.println("FINISHED Deleting " + addressToDelList.size() + " old addresses.");
        _currentStatus = null;
    }

    private static Map<String, Person> getStudentPersonsMap()
    {
        List<Person> personList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Student.class, "s").column(property(Student.person().fromAlias("s"))).order(property(Student.person().id().fromAlias("s"))));

        Map<String, Person> personMap = new HashMap<>();
        for (Person person : personList)
        {
            StringBuilder key = new StringBuilder();
            key.append(person.getIdentityCard().getLastName()).append(person.getIdentityCard().getFirstName());
            if (null != person.getIdentityCard().getMiddleName()) key.append(person.getIdentityCard().getMiddleName());
            if (null != person.getIdentityCard().getBirthDate())
                key.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(person.getIdentityCard().getBirthDate()));
            String keyStr = key.toString().toLowerCase();
            if (null == personMap.get(keyStr)) personMap.put(keyStr, person);
        }

        return personMap;
    }

    private static Map<Long, OrderData> getPersonIdToOrderDataMap()
    {
        List<OrderData> orderDataList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(OrderData.class, "s").column(property("s")).order(property(OrderData.student().id().fromAlias("s"))));

        Map<Long, OrderData> studentMap = new HashMap<>();
        for (OrderData orderData : orderDataList)
        {
            if (null == studentMap.get(orderData.getStudent().getPerson().getId()))
                studentMap.put(orderData.getStudent().getPerson().getId(), orderData);
        }

        return studentMap;
    }

    private static Set<String> getStudentOrderKeysSet()
    {
        List<Object[]> orderDataList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(OtherStudentExtract.class, "e")
                .column(property(OtherStudentExtract.entity().id().fromAlias("e")))
                .column(property(OtherStudentExtract.paragraph().order().number().fromAlias("e")))
                .column(property(OtherStudentExtract.paragraph().order().commitDate().fromAlias("e")))
                .column(property(OtherStudentExtract.type().title().fromAlias("e")))
                .order(property(OtherStudentExtract.entity().id().fromAlias("e"))));

        Set<String> studentOrdersKeysSet = new HashSet<>();
        for (Object[] orderData : orderDataList)
        {
            if(null != orderData[1] && null != orderData[2])
            {
                StringBuilder key = new StringBuilder();
                key.append(orderData[0]);
                key.append(orderData[3]);
                key.append(((String)orderData[1]).toLowerCase());
                key.append(DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) orderData[2]));
                studentOrdersKeysSet.add(key.toString());
            }
        }

        return studentOrdersKeysSet;
    }

    private static Map<Long, Student> getPersonIdToStudentsMap()
    {
        List<Student> studentList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Student.class, "s").column(property("s")).order(property(Student.id().fromAlias("s"))));

        Map<Long, Student> studentMap = new HashMap<>();
        for (Student student : studentList)
        {
            if (null == studentMap.get(student.getPerson().getId()))
                studentMap.put(student.getPerson().getId(), student);
        }

        return studentMap;
    }

    private static Map<String, Person> getPersonsMap()
    {
        List<Person> personList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Person.class, "p").column(property("p")));

        Map<String, Person> personMap = new HashMap<>();
        for (Person person : personList)
        {
            StringBuilder key = new StringBuilder();
            key.append(person.getIdentityCard().getLastName()).append(person.getIdentityCard().getFirstName());
            if (null != person.getIdentityCard().getMiddleName()) key.append(person.getIdentityCard().getMiddleName());
            if (null != person.getIdentityCard().getBirthDate())
                key.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(person.getIdentityCard().getBirthDate()));
            String keyStr = key.toString().toLowerCase();
            if (null == personMap.get(keyStr)) personMap.put(keyStr, person);
        }

        return personMap;
    }

    private static Map<String, AddressBase> getPersonsRegAddressMap()
    {
        List<Object[]> addrObjList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property(Student.person().identityCard().fromAlias("s")))
                .column(property(Student.person().identityCard().address().fromAlias("s"))));

        Map<String, AddressBase> addressMap = new HashMap<>();
        for (Object[] addressObj : addrObjList)
        {
            IdentityCard card = (IdentityCard) addressObj[0];
            AddressBase address = (AddressBase) addressObj[1];

            if (null != address)
            {
                StringBuilder key = new StringBuilder();
                key.append(card.getLastName()).append(card.getFirstName());
                if (null != card.getMiddleName()) key.append(card.getMiddleName());
                if (null != card.getBirthDate())
                    key.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getBirthDate()));
                String keyStr = key.toString().toLowerCase();
                if (null == addressMap.get(keyStr)) addressMap.put(keyStr, address);
            }
        }

        return addressMap;
    }

    private static Map<String, AddressBase> getPersonsFactAddressMap()
    {
        List<Object[]> addrObjList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property(Student.person().identityCard().fromAlias("s")))
                .column(property(Student.person().address().fromAlias("s"))));

        Map<String, AddressBase> addressMap = new HashMap<>();
        for (Object[] addressObj : addrObjList)
        {
            IdentityCard card = (IdentityCard) addressObj[0];
            AddressBase address = (AddressBase) addressObj[1];

            if (null != address)
            {
                StringBuilder key = new StringBuilder();
                key.append(card.getLastName()).append(card.getFirstName());
                if (null != card.getMiddleName()) key.append(card.getMiddleName());
                if (null != card.getBirthDate())
                    key.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getBirthDate()));
                String keyStr = key.toString().toLowerCase();
                if (null == addressMap.get(keyStr)) addressMap.put(keyStr, address);
            }
        }

        return addressMap;
    }

    private static Date getCorrectedDate(Cell cell)
    {
        String dateStr = null;
        if (cell instanceof DateCell)
        {
            DateCell dateCell = (DateCell) cell;
            dateStr = DateFormatter.DEFAULT_DATE_FORMATTER.format(dateCell.getDate());
        } else dateStr = cell.getContents();

        dateStr = dateStr.replaceAll("/", ".").replaceAll(",", ".");
        if (!dateStr.contains(".")) return null; //TODO some warning

        StringBuilder builder = new StringBuilder();
        for (char symbol : dateStr.toCharArray())
        {
            if (Character.isDigit(symbol) || '.' == symbol) builder.append(symbol);
        }

        int dotIdx = builder.indexOf(".");
        if (1 == dotIdx) builder.insert(0, "0");

        try
        {
            Date date = DATE_PARSER.parse(builder.toString());
            Calendar cal = new GregorianCalendar();
            cal.setTime(date);

            if (cal.get(Calendar.YEAR) < 1900)
            {
                cal.set(1900, 1, 1);
                return cal.getTime();
            }

            return date;
        } catch (ParseException ex)
        {

        }

        return null;
    }

    private static String getPassportSeriesNumberCorrected(String str)
    {
        String preStr = StringUtils.trimToNull(str);
        if (null != preStr)
        {
            StringBuilder result = new StringBuilder();
            for (char symbol : preStr.toCharArray())
            {
                if (Character.isDigit(symbol) || Character.isAlphabetic(symbol)) result.append(symbol);
            }
            return result.toString();
        }

        return null;
    }

    private static String getPassportIssuanceCodeCorrected(String str)
    {
        String preStr = StringUtils.trimToNull(str);
        if (null != preStr)
        {
            StringBuilder result = new StringBuilder();
            for (char symbol : preStr.toCharArray())
            {
                if (Character.isDigit(symbol) || '-' == symbol) result.append(symbol);
            }

            if (result.length() == 6 && !result.toString().contains("-"))
                return result.substring(0, 3) + "-" + result.substring(3, 6);

            if (result.length() == 7 && result.toString().contains("-"))
                return result.toString();

            return preStr;
        }

        return null;
    }

    private static String getSexCorrected(String str)
    {
        String preStr = StringUtils.trimToNull(str);
        if (null != sexMap.get(preStr)) return sexMap.get(preStr);
        return preStr;
    }

    private static boolean checkLineIsEmpty(Cell[] cells)
    {
        return cells.length < 15 || null == StringUtils.trimToNull(cells[0].getContents());
    }
}