/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.ui.ChangeExamGroup;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrAbsenceNoteCodes;
import ru.tandemservice.unienr14.exams.bo.EnrExamGroup.ui.IncludeEntrant.EnrExamGroupIncludeEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.RsmuEnrNotAppearEntrantManager;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 11.02.2015
 */
@State({
               @Bind(key = RsmuEnrNotAppearEntrantChangeExamGroupUI.ENTRANT_ID_LIST, binding = "entrantIds"),
               @Bind(key = RsmuEnrNotAppearEntrantChangeExamGroupUI.CAMPAIGN_DISCIPLINE, binding = "campaignDiscipline.id"),
               @Bind(key = RsmuEnrNotAppearEntrantChangeExamGroupUI.EXAM_PASS_FORM, binding = "examPassForm.id"),
               @Bind(key = RsmuEnrNotAppearEntrantChangeExamGroupUI.ENR_ORG_UNIT, binding = "orgUnit.id"),
               @Bind(key = RsmuEnrNotAppearEntrantChangeExamGroupUI.EXAM_GROUP, binding = "examGroup.id")
       })
public class RsmuEnrNotAppearEntrantChangeExamGroupUI extends UIPresenter
{
    public static final String ENTRANT_ID_LIST = "entrantIds";
    public static final String CAMPAIGN_DISCIPLINE = "campaignDiscipline";
    public static final String EXAM_PASS_FORM = "examPassForm";
    public static final String EXAM_GROUP = "examGroup";
    public static final String ENR_ORG_UNIT = "orgUnit";

    private List<Long> _entrantIds = new ArrayList<>();
    private List<EnrExamPassDiscipline> _examPassDisciplineList;
    private EnrCampaignDiscipline _campaignDiscipline = new EnrCampaignDiscipline();
    private EnrExamPassForm _examPassForm = new EnrExamPassForm();
    private EnrExamGroup _examGroup = new EnrExamGroup();
    private EnrOrgUnit _orgUnit = new EnrOrgUnit();


    private EnrExamGroupSet _examGroupSet;
    private EnrExamGroup _previousExamGroup;

    @Override
    public void onComponentActivate()
    {
        setCampaignDiscipline(DataAccessServices.dao().get(EnrCampaignDiscipline.class, EnrExamPassDiscipline.P_ID, getCampaignDiscipline().getId()));
        setExamPassForm(DataAccessServices.dao().get(EnrExamPassForm.class, EnrExamPassForm.P_ID, getExamPassForm().getId()));
        setExamGroup(DataAccessServices.dao().get(EnrExamGroup.class, EnrExamGroup.P_ID, getExamGroup().getId()));
        setExamGroupSet(getExamGroup().getExamGroupSet());

        DQLSelectBuilder examPassDisciplineDql = new DQLSelectBuilder()
                .fromEntity(EnrExamPassDiscipline.class, "e")
                .column(property("e"))
                .where(eqValue(property("e", EnrExamPassDiscipline.L_DISCIPLINE), getCampaignDiscipline()))
                .where(eqValue(property("e", EnrExamPassDiscipline.L_PASS_FORM), getExamPassForm()))
                .where(in(property("e", EnrExamPassDiscipline.L_ENTRANT), getEntrantIds()))
                .where(eqValue(property("e", EnrExamPassDiscipline.absenceNote().code()), EnrAbsenceNoteCodes.VALID));

        setExamPassDisciplineList(DataAccessServices.dao().<EnrExamPassDiscipline>getList(examPassDisciplineDql));
        setPreviousExamGroup(getExamGroup());


        if (getOrgUnit().getId() != null)
        {
            setOrgUnit(DataAccessServices.dao().get(EnrOrgUnit.class, EnrOrgUnit.P_ID, getOrgUnit().getId()));
        }
    }

    public void onClickApply()
    {

        RsmuEnrNotAppearEntrantManager.instance().dao().updateExamGroupForDiscipline(getExamPassDisciplineList(), getExamGroup());
        deactivate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EnrEnrollmentCampaignManager.PARAM_ENR_CAMPAIGN, getCampaignDiscipline().getEnrollmentCampaign());
        if (getOrgUnit() != null && getOrgUnit().getInstitutionOrgUnit() != null)
        {
            dataSource.put(EnrExamGroupIncludeEntrant.BIND_ORG_UNIT, getOrgUnit().getInstitutionOrgUnit().getOrgUnit());
        }
        dataSource.put(EnrExamGroupIncludeEntrant.BIND_EXAM_GROUP_SET, getExamGroupSet());
        dataSource.put(EnrExamGroupIncludeEntrant.BIND_DISCIPLINE, getCampaignDiscipline());
        dataSource.put(EnrExamGroupIncludeEntrant.BIND_PASS_FORM, getExamPassForm());
        dataSource.put(EnrExamGroupIncludeEntrant.BIND_PREVIOUS_EXAM_GROUP, getPreviousExamGroup());
    }

    // getters and setters


    public List<EnrExamPassDiscipline> getExamPassDisciplineList()
    {
        return _examPassDisciplineList;
    }

    public void setExamPassDisciplineList(List<EnrExamPassDiscipline> examPassDisciplineList)
    {
        _examPassDisciplineList = examPassDisciplineList;
    }

    public EnrExamGroup getExamGroup()
    {
        return _examGroup;
    }

    public void setExamGroup(EnrExamGroup examGroup)
    {
        _examGroup = examGroup;
    }

    public EnrExamGroupSet getExamGroupSet()
    {
        return _examGroupSet;
    }

    public void setExamGroupSet(EnrExamGroupSet examGroupSet)
    {
        _examGroupSet = examGroupSet;
    }

    public EnrOrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    public void setOrgUnit(EnrOrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }

    public EnrExamGroup getPreviousExamGroup()
    {
        return _previousExamGroup;
    }

    public void setPreviousExamGroup(EnrExamGroup previousExamGroup)
    {
        _previousExamGroup = previousExamGroup;
    }

    public List<Long> getEntrantIds()
    {
        return _entrantIds;
    }

    public void setEntrantIds(List<Long> entrantIds)
    {
        _entrantIds = entrantIds;
    }

    public EnrCampaignDiscipline getCampaignDiscipline()
    {
        return _campaignDiscipline;
    }

    public void setCampaignDiscipline(EnrCampaignDiscipline campaignDiscipline)
    {
        _campaignDiscipline = campaignDiscipline;
    }

    public EnrExamPassForm getExamPassForm()
    {
        return _examPassForm;
    }

    public void setExamPassForm(EnrExamPassForm examPassForm)
    {
        _examPassForm = examPassForm;
    }

    public String getDisciplineTitle()
    {
        return getCampaignDiscipline().getTitle() + " (" + getExamPassForm().getTitle() + ")";
    }

}
