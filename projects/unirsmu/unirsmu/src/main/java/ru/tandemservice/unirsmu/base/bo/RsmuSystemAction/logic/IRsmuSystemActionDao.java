/* $Id$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

import java.io.File;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 12.01.2015
 */
public interface IRsmuSystemActionDao extends INeedPersistenceSupport
{
    /**
     * Метод закрепляет файлы с фотогрфиями абитуриентов, за конкертными абитуриентами.
     * Соответсивие фото-абитуриент устанавливается по названию файла (770088_1992_12_24.jpg - личный номер_дата рождения)
     *
     * @param photoList
     */
    void savePhotos(List<File> photoList);

    void deleteAllStudents();

    void deleteOtherInvalidExtracts();

    int doOptimizeOtherOrders();

    int changeFakeStudentsStatus();
}