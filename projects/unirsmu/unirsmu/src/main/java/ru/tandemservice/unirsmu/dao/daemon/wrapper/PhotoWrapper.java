/* $Id:$ */
package ru.tandemservice.unirsmu.dao.daemon.wrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorSyncDaemonDao;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 24.11.2015
 */
public class PhotoWrapper
{
    private String _personId;
    private byte[] _photo;

    public PhotoWrapper(ResultSet resultSet) throws SQLException
    {
        _personId = StringUtils.trimToNull(resultSet.getString(1));

        InputStream stream = resultSet.getBinaryStream(2);

        try
        {
            byte[] data = new byte[stream.available()];
            stream.read(data);
            stream.close();
            _photo = data;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public DatabaseFile generatePhoto()
    {
        DatabaseFile file = new DatabaseFile();
        file.setContent(_photo);
        return file;
    }

    public void updatePhoto(DatabaseFile file)
    {
        if (null == file)
        {
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Photo was not uploaded: " + this.toString());
            return;
        }
        file.setContent(_photo);
    }

    public static PhotoWrapper getTheBest(List<PhotoWrapper> wrappers)
    {
        if (null == wrappers || wrappers.isEmpty()) return null;
        return wrappers.get(0);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder(_personId);
        return builder.toString();
    }

    public String getPersonId()
    {
        return _personId;
    }

    public void setPersonId(String personId)
    {
        _personId = personId;
    }

    public byte[] getPhoto()
    {
        return _photo;
    }

    public void setPhoto(byte[] photo)
    {
        _photo = photo;
    }
}