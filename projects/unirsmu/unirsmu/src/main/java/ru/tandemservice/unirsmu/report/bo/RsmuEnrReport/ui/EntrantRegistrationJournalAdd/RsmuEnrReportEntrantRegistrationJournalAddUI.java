/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantRegistrationJournalAdd;

import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantRegistrationJournalAdd.EnrReportEntrantRegistrationJournalAddUI;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.RsmuEnrReportManager;

/**
 * @author Nikolay Fedorovskih
 * @since 02.07.2014
 */
public class RsmuEnrReportEntrantRegistrationJournalAddUI extends EnrReportEntrantRegistrationJournalAddUI
{
    @Override
    public boolean isPrintEntrantNumberColumn()
    {
        return true;
    }

    @Override
    public void onClickApply()
    {
        Long reportId = RsmuEnrReportManager.instance().dao().createEntrantRegistrationJournalReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }
}