/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.tapsupport.component.selection.*;

import java.util.*;

/**
 * @author Andrey Avetisov
 * @since 02.02.2015
 */
public class RsmuEnrReportEntrantExaminationDataModel extends CommonMultiSelectModel
{
    Map<Long, DataWrapper> _dataModelWrapperMap;
    List <DataWrapper> values;

    public RsmuEnrReportEntrantExaminationDataModel(Map<Long, DataWrapper> dataModelWrapperMap)
    {
        _dataModelWrapperMap = dataModelWrapperMap;
    }



    @Override
    @SuppressWarnings({"unchecked", "ConstantConditions"})
    protected IListResultBuilder createBuilder(String filter, Set set)
    {
        values = new ArrayList<>(_dataModelWrapperMap.values());

        if (StringUtils.isNotEmpty(filter))
        {
            for (Iterator<DataWrapper> iterator = values.iterator(); iterator.hasNext(); )
            {
                if (!StringUtils.containsIgnoreCase(iterator.next().getTitle().replace("\n", " "), filter))
                    iterator.remove();
            }
        }

        return new SimpleListResultBuilder(values)
        {
            @Override
            public ListResult findOptions()
            {
                final List objects = super.findOptions().getObjects();
                List list = objects.size() >= 50 ? objects.subList(0, 50) : objects;
                int count = list == null ? 0 : list.size();
                if (count == 50)
                    count = objects.size();

                Collections.sort(list, new Comparator<DataWrapper>()
                {
                    @Override
                    public int compare(DataWrapper o1, DataWrapper o2)
                    {
                        return o1.getTitle().compareTo(o2.getTitle());
                    }
                });

                return new ListResult(list, count);
            }
        };
    }

    public List<DataWrapper> getValues()
    {
        return values;
    }
}
