/**
 *$Id$
 */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.ui.StudImportFromExcel;

import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.tapsupport.validator.FileExtensionsValidator;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorDataHolder;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorSyncDaemonDao;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 18.11.2013
 */
public class RsmuSystemActionStudImportFromExcelUI extends UIPresenter
{
    private IUploadFile _source;
    private int _timerInterval = 2000;

    public void onClickApply()
    {
        if (null == StudentImportFromExcelUtil.getCurrentStatus())
        {
            Workbook book = null;
            try
            {
                book = Workbook.getWorkbook(_source.getStream());
            } catch (IOException io)
            {
                throw new ApplicationException("Невозможно прочитать файл. Вероятно файл имеет формат, отличный от Excel 87-2003, либо поврежден.");
            } catch (BiffException biff)
            {
                throw new ApplicationException("Невозможно прочитать файл. Вероятно файл имеет формат, отличный от Excel 87-2003, либо поврежден.");
            }

            RsmuVectorDataHolder.clearCacheMaps();
            RsmuVectorSyncDaemonDao.registerExcelImport(_source.getStream());
            RsmuVectorSyncDaemonDao.DAEMON.wakeUpDaemon();
        }

    }

    @Override
    public void deactivate()
    {
        StudentImportFromExcelUtil.clearStatus();
        StudentImportFromExcelUtil.clearError();
        super.deactivate();
    }

    public String getSyncStatus()
    {
        if(null != StudentImportFromExcelUtil.getError()) StudentImportFromExcelUtil.clearStatus();
        return StudentImportFromExcelUtil.getCurrentStatus();
    }

    public String getError()
    {
        return StudentImportFromExcelUtil.getError();
    }

    public String getWarning()
    {
        return StudentImportFromExcelUtil.getWarning();
    }

    public boolean isAutoRefreshEnabled()
    {
        return null != StudentImportFromExcelUtil.getCurrentStatus();
    }

    public List<Validator> getValidators()
    {
        return Arrays.<Validator>asList(new Required(), new FileExtensionsValidator("fileExtensions=xls;xlsx"));
    }

    public IUploadFile getSource()
    {
        return _source;
    }

    public void setSource(IUploadFile source)
    {
        _source = source;
    }

    public int getTimerInterval()
    {
        return _timerInterval;
    }
}