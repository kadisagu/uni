/* $Id$ */
package ru.tandemservice.unirsmu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressString;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unirsmu.dao.daemon.wrapper.StudentWrapper;
import ru.tandemservice.unirsmu.entity.RsmuVectorIds;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 23.11.2015
 */
public interface IRsmuVectorSyncDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IRsmuVectorSyncDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IRsmuVectorSyncDaemonDao> instance = new SpringBeanCache<>(IRsmuVectorSyncDaemonDao.class.getName());

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void savePersonsList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> idsMap, boolean takeRealVectorIds);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void saveStudentsList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<Student, RsmuVectorIds>> idsMap, Map<String, StudentWrapper> studentWrappersMap);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void saveAddressList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<AddressString, RsmuVectorIds>> idsMap, Map<String, Person> addressIdToPersonMap, boolean regAddr);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void saveContactsList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<PersonContactData, RsmuVectorIds>> idsMap);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void savePhotosList(List<String> idsToProcessPortion, Map<String, CoreCollectionUtils.Pair<DatabaseFile, RsmuVectorIds>> idsMap, Map<String, Person> personMap);

    <T extends IEntity> Map<String, CoreCollectionUtils.Pair<T, RsmuVectorIds>> getIdsMap(Collection<String> vectorIdCollection, Class<T> clazz, String entityType);

    <T extends IEntity> List<String> getFilteredIdList(List<String> vectorIdList, Class<T> clazz, String entityType);

    List<String> getPersonVectorIdsList(List<String> vectorIdList);

    void updateIdentityCards(List<String> personList, Map<String, IdentityCard> icardMap);

    void createAddresses(List<AddressBase> addressList);

    void deleteAddresses(List<Long> addressList);
}