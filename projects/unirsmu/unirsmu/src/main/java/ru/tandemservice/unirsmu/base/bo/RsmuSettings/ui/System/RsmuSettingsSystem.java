/* $Id:$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSettings.ui.System;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 18.11.2015
 */
@Configuration
public class RsmuSettingsSystem extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().create();
    }
}