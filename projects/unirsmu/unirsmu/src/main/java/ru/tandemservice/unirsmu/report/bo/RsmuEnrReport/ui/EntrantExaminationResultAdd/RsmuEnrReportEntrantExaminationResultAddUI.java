/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantExaminationResultAdd;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariantPassForm;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.GlobalList.EnrReportBaseGlobalList;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroup;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDisciplineGroupElement;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic.RsmuEnrReportEntrantExaminationDataModel;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic.RsmuExamGroupDSHandler;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 30.01.2015
 */
public class RsmuEnrReportEntrantExaminationResultAddUI extends UIPresenter
{
    public static String EXAM_SET = "examSet";
    public static String PASS_FORM = "passForm";
    public static String PASS_DISCIPLINE = "passDiscipline";

    private boolean _entrantStateActive;
    private boolean _dateFromActive;
    private boolean _dateToActive;
    private boolean _examPassDisciplineActive;
    private boolean _examGroupActive;
    private boolean _absenceNoteActive;
    private boolean _upLimitPointsRatingActive;
    private boolean _lowLimitPointsRatingActive;

    private List<EnrEnrollmentCampaign> _enrollmentCampaignList;
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private List<EnrEntrantState> _entrantStateList;
    private Date _dateFrom;
    private Date _dateTo;
    private List<DataWrapper> _examSetList;
    private List<EnrExamPassForm> _examPassFormList;
    private List<DataWrapper> _examPassDisciplineList;
    private List<EnrExamGroup> _examGroupList;
    private List<EnrAbsenceNote> _absenceNoteList;
    private long _lowLimitPointsRating;
    private long _upLimitPointsRating;
    private IMultiSelectModel _examPassDisciplineModel;
    private IMultiSelectModel _examSetModel;
    private byte[] _report = null;


    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setEntrantStateActive(false);
        setEntrantStateList(null);
        setDateFromActive(false);
        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateToActive(false);
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());
        setExamPassFormList(null);
        setExamPassDisciplineActive(false);
        setExamPassDisciplineList(null);
        setExamSetList(new ArrayList<DataWrapper>());
        setExamGroupActive(false);
        setExamGroupList(null);
        setAbsenceNoteActive(false);
        setAbsenceNoteList(null);
        setLowLimitPointsRatingActive(false);
        setUpLimitPointsRatingActive(false);
        prepareDisciplineSelectModel();
        prepareExamSetModel();
        configUtil(getCompetitionFilterAddon());
        getCompetitionFilterAddon().clearSettings();
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (_report != null)
        {
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Результаты ВИ.xls").document(_report), true);
            _report = null;
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(RsmuEnrReportEntrantExaminationResultAdd.EXAM_GROUP_DS))
        {
            dataSource.put(RsmuExamGroupDSHandler.ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
            dataSource.put(RsmuExamGroupDSHandler.EXAM_PASS, getExamPassDisciplineList());
            dataSource.put(RsmuExamGroupDSHandler.EXAM_PASS_ACTIVE, isExamPassDisciplineActive());
        }
    }

    //Listeners
    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        prepareDisciplineSelectModel();
        prepareExamSetModel();
        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());
    }

    public void onClickCancel()
    {
        _uiActivation.asDesktopRoot(EnrReportBaseGlobalList.class).activate();
    }

    public void onClickApply()
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        checkDates(errors);
        checkLimitPointsRating(errors);
        if (errors.hasErrors()) return;
        getCompetitionFilterAddon().saveSettings();
        try
        {

            ReportEntrantExaminationResultExcelBuilder builder = new ReportEntrantExaminationResultExcelBuilder(isEntrantStateActive(), isDateFromActive(), isDateToActive(),
                                                                                                                isExamPassDisciplineActive(), isExamGroupActive(), isAbsenceNoteActive(),
                                                                                                                isUpLimitPointsRatingActive(), isLowLimitPointsRatingActive(),
                                                                                                                getEnrollmentCampaign(), getEntrantStateList(),
                                                                                                                getDateFrom(), getDateTo(), getExamSetList(),
                                                                                                                getExamPassFormList(), getExamPassDisciplineList(),
                                                                                                                getExamGroupList(), getAbsenceNoteList(),
                                                                                                                getLowLimitPointsRating(), getUpLimitPointsRating(), getCompetitionFilterAddon());
            try
            {
                _report = builder.buildReport().toByteArray();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    public void onClickActiveAbsenceNoteActive()
    {
        if (isAbsenceNoteActive())
        {
            setUpLimitPointsRatingActive(false);
            setLowLimitPointsRatingActive(false);
        }
    }

    public void onClickLimitRatingActive()
    {
        if (isLowLimitPointsRatingActive() || isUpLimitPointsRatingActive())
        {
            setAbsenceNoteActive(false);
        }
    }

    // utils
    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util
                .clearWhereFilter()
                .configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {

        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);


        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }


    //models
    public void prepareExamSetModel()
    {
        Map<Long, DataWrapper> examSetModelWrapperMap = new HashMap<>();

        List<EnrExamSet> resultExamSetList = DataAccessServices.dao().getList(EnrExamSet.class, EnrExamSet.L_ENROLLMENT_CAMPAIGN, getEnrollmentCampaign());
        long id = 1L;
        for (EnrExamSet examSet : resultExamSetList)
        {
            final DataWrapper wrapper = new DataWrapper(id, examSet.getElementsTitle());
            wrapper.setProperty(EXAM_SET, examSet);
            examSetModelWrapperMap.put(id++, wrapper);
        }
        _examSetModel = new RsmuEnrReportEntrantExaminationDataModel(examSetModelWrapperMap);
    }


    public void prepareDisciplineSelectModel()
    {
        Map<Long, DataWrapper> disciplineModelWrapperMap;
        {
            disciplineModelWrapperMap = new HashMap<>();
            List<EnrExamSet> unWrappedExamSetList = new ArrayList<>();
            for (DataWrapper wrapper : getExamSetList())
            {
                EnrExamSet examSet = wrapper.get(EXAM_SET);
                unWrappedExamSetList.add(examSet);
            }

            final DQLSelectBuilder discResultDQL = new DQLSelectBuilder()
                    .fromEntity(EnrExamVariant.class, "ge")
                    .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                    .joinEntity("ge", DQLJoinType.inner, EnrCampaignDiscipline.class, "cd", eq(property(EnrCampaignDiscipline.id().fromAlias("cd")), property("cdValue.id")))
                    .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                    .where(eq(property(EnrCampaignDiscipline.enrollmentCampaign().fromAlias("cd")), value(getEnrollmentCampaign())))
                    .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)))
                    .where(in(property(EnrExamVariant.examSetVariant().examSet().fromAlias("ge")), unWrappedExamSetList))
                    .where(in(property(EnrExamVariantPassForm.passForm().fromAlias("ef")), getExamPassFormList()));

            final DQLSelectBuilder discGroupResultDQL = new DQLSelectBuilder()
                    .fromEntity(EnrExamVariant.class, "ge")
                    .joinPath(DQLJoinType.inner, EnrExamVariant.examSetElement().value().fromAlias("ge"), "cdValue")
                    .joinEntity("ge", DQLJoinType.inner, EnrCampaignDisciplineGroup.class, "dg", eq(property(EnrCampaignDisciplineGroup.id().fromAlias("dg")), property("cdValue.id")))
                    .joinEntity("dg", DQLJoinType.inner, EnrCampaignDisciplineGroupElement.class, "dge", eq(property(EnrCampaignDisciplineGroupElement.group().fromAlias("dge")), property("dg")))
                    .joinEntity("ge", DQLJoinType.inner, EnrExamVariantPassForm.class, "ef", eq(property(EnrExamVariantPassForm.examVariant().fromAlias("ef")), property("ge")))
                    .where(eq(property(EnrCampaignDisciplineGroup.enrollmentCampaign().fromAlias("dg")), value(getEnrollmentCampaign())))
                    .where(eq(property(EnrExamVariantPassForm.passForm().internal().fromAlias("ef")), value(true)))
                    .where(in(property(EnrExamVariant.examSetVariant().examSet().fromAlias("ge")), unWrappedExamSetList))
                    .where(in(property(EnrExamVariantPassForm.passForm().fromAlias("ef")), getExamPassFormList()));

            discResultDQL
                    .column(property("cd"))
                    .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            discGroupResultDQL
                    .column(property(EnrCampaignDisciplineGroupElement.discipline().fromAlias("dge")))
                    .column(property(EnrExamVariantPassForm.passForm().fromAlias("ef")));

            final List<Object[]> resultDiscList = DataAccessServices.dao().getList(discResultDQL);
            final List<Object[]> resultDiscGroupList = DataAccessServices.dao().getList(discGroupResultDQL);

            long id = 1L;
            final Set<CoreCollectionUtils.Pair<Long, Long>> uniqPair = new LinkedHashSet<>();
            for (Object[] objects : resultDiscList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty(PASS_DISCIPLINE, campDisc);
                    wrapper.setProperty(PASS_FORM, passForm);
                    disciplineModelWrapperMap.put(id++, wrapper);
                }
            }

            for (Object[] objects : resultDiscGroupList)
            {
                final EnrCampaignDiscipline campDisc = (EnrCampaignDiscipline) objects[0];
                final EnrExamPassForm passForm = (EnrExamPassForm) objects[1];

                if (uniqPair.add(new CoreCollectionUtils.Pair<>(campDisc.getId(), passForm.getId())))
                {
                    final DataWrapper wrapper = new DataWrapper(id, campDisc.getTitle() + " (" + passForm.getTitle() + ")");
                    wrapper.setProperty(PASS_DISCIPLINE, campDisc);
                    wrapper.setProperty(PASS_FORM, passForm);
                    disciplineModelWrapperMap.put(id++, wrapper);
                }
            }
        }

        setExamPassDisciplineModel(new RsmuEnrReportEntrantExaminationDataModel(disciplineModelWrapperMap));
    }


    //validators
    private void checkDates(ErrorCollector errors) throws ApplicationException
    {
        if (isDateFromActive() && isDateToActive() && getDateFrom().after(getDateTo()))
        {
            errors.add("Дата начала проведения ВИ должна быть не позже даты окончания.", "dateFrom", "dateTo");
        }
    }

    private void checkLimitPointsRating(ErrorCollector errors) throws ApplicationException
    {
        if (isLowLimitPointsRatingActive() && isUpLimitPointsRatingActive() && (getLowLimitPointsRating() > getUpLimitPointsRating()))
        {
            errors.add("Нижняя граница набранных баллов должна быть не больше верхней.", "lowLimitPointsRating", "upLimitPointsRating");
        }
    }

    //getters & setters
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public List<DataWrapper> getExamSetList()
    {
        return _examSetList;
    }

    public void setExamSetList(List<DataWrapper> examSetList)
    {
        _examSetList = examSetList;
    }

    public List<EnrEntrantState> getEntrantStateList()
    {
        return _entrantStateList;
    }

    public void setEntrantStateList(List<EnrEntrantState> entrantStateList)
    {
        _entrantStateList = entrantStateList;
    }

    public List<EnrExamPassForm> getExamPassFormList()
    {
        return _examPassFormList;
    }

    public void setExamPassFormList(List<EnrExamPassForm> examPassFormList)
    {
        _examPassFormList = examPassFormList;
    }

    public List<DataWrapper> getExamPassDisciplineList()
    {
        return _examPassDisciplineList;
    }

    public void setExamPassDisciplineList(List<DataWrapper> examPassDisciplineList)
    {
        _examPassDisciplineList = examPassDisciplineList;
    }

    public List<EnrExamGroup> getExamGroupList()
    {
        return _examGroupList;
    }

    public void setExamGroupList(List<EnrExamGroup> examGroupList)
    {
        _examGroupList = examGroupList;
    }

    public List<EnrAbsenceNote> getAbsenceNoteList()
    {
        return _absenceNoteList;
    }

    public void setAbsenceNoteList(List<EnrAbsenceNote> absenceNoteList)
    {
        _absenceNoteList = absenceNoteList;
    }

    public long getLowLimitPointsRating()
    {
        return _lowLimitPointsRating;
    }

    public void setLowLimitPointsRating(long lowLimitPointsRating)
    {
        _lowLimitPointsRating = lowLimitPointsRating;
    }

    public long getUpLimitPointsRating()
    {
        return _upLimitPointsRating;
    }

    public void setUpLimitPointsRating(long upLimitPointsRating)
    {
        _upLimitPointsRating = upLimitPointsRating;
    }

    public boolean isEntrantStateActive()
    {
        return _entrantStateActive;
    }

    public void setEntrantStateActive(boolean entrantStateActive)
    {
        _entrantStateActive = entrantStateActive;
    }

    public boolean isDateFromActive()
    {
        return _dateFromActive;
    }

    public void setDateFromActive(boolean dateFromActive)
    {
        _dateFromActive = dateFromActive;
    }

    public boolean isDateToActive()
    {
        return _dateToActive;
    }

    public void setDateToActive(boolean dateToActive)
    {
        _dateToActive = dateToActive;
    }

    public boolean isExamPassDisciplineActive()
    {
        return _examPassDisciplineActive;
    }

    public void setExamPassDisciplineActive(boolean examPassDisciplineActive)
    {
        _examPassDisciplineActive = examPassDisciplineActive;
    }

    public boolean isAbsenceNoteActive()
    {
        return _absenceNoteActive;
    }

    public void setAbsenceNoteActive(boolean absenceNoteActive)
    {
        _absenceNoteActive = absenceNoteActive;
    }

    public boolean isUpLimitPointsRatingActive()
    {
        return _upLimitPointsRatingActive;
    }

    public void setUpLimitPointsRatingActive(boolean upLimitPointsRatingActive)
    {
        _upLimitPointsRatingActive = upLimitPointsRatingActive;
    }

    public boolean isLowLimitPointsRatingActive()
    {
        return _lowLimitPointsRatingActive;
    }

    public void setLowLimitPointsRatingActive(boolean lowLimitPointsRatingActive)
    {
        _lowLimitPointsRatingActive = lowLimitPointsRatingActive;
    }

    public IMultiSelectModel getExamPassDisciplineModel()
    {
        return _examPassDisciplineModel;
    }

    public void setExamPassDisciplineModel(IMultiSelectModel examPassDisciplineModel)
    {
        _examPassDisciplineModel = examPassDisciplineModel;
    }

    public IMultiSelectModel getExamSetModel()
    {
        return _examSetModel;
    }

    public void setExamSetModel(IMultiSelectModel examSetModel)
    {
        _examSetModel = examSetModel;
    }

    public boolean isExamGroupActive()
    {
        return _examGroupActive;
    }

    public void setExamGroupActive(boolean examGroupActive)
    {
        _examGroupActive = examGroupActive;
    }
}
