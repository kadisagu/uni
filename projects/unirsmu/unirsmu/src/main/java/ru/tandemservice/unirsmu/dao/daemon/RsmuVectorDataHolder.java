/* $Id$ */
package ru.tandemservice.unirsmu.dao.daemon;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.fias.base.entity.ICitizenship;
import org.tandemframework.shared.fias.base.entity.NoCitizenship;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonRole;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.IdentityCardTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.IUniDao;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unirsmu.dao.daemon.wrapper.StudentWrapper;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Dmitry Seleznev
 * @since 24.11.2015
 */
public class RsmuVectorDataHolder
{
    private static Map<String, Sex> _sexMap;
    private static Map<String, IdentityCardType> _identityCardTypeMap;
    private static Map<String, ICitizenship> _citizenshipMap;
    private static AddressCountry _russia;

    private static Set<String> _studentPerNumbersSet;
    private static Map<Long, String> _studentIdToPerNumbersMap;

    private static StudentCategory _studentCategory;
    private static CompensationType _compensationTypeBudget;
    private static CompensationType _compensationTypeContract;

    private static Map<String, Course> _courseMap;
    private static Map<String, StudentStatus> _studentStatusMap;
    private static Map<String, EducationOrgUnit> _eduOrgUnitMap;

    private static OrderStates _orderStateCommitted;
    private static ExtractStates _extractStateCommitted;
    private static Map<String, StudentExtractType> _extractTypesMap;

    private static Set<String> _loginSet;
    private static Map<Long, String> _loginMap;
    private static AuthenticationType _authTypeSimple;

    public static void clearCacheMaps()
    {
        _citizenshipMap = null;
        _studentPerNumbersSet = null;
        _studentIdToPerNumbersMap = null;
        _eduOrgUnitMap = null;
        _extractTypesMap = null;
        _loginSet = null;
        _loginMap = null;
    }

    public static Sex getSex(String sexLetter)
    {
        if (null == _sexMap)
        {
            _sexMap = new HashMap<>();
            Sex maleSex = IUniDao.instance.get().getCatalogItem(Sex.class, SexCodes.MALE);
            Sex femaleSex = IUniDao.instance.get().getCatalogItem(Sex.class, SexCodes.FEMALE);
            _sexMap.put("м", maleSex);
            _sexMap.put("М", maleSex);
            _sexMap.put("ж", femaleSex);
            _sexMap.put("Ж", femaleSex);
            _sexMap.put(null, maleSex);
        }
        return _sexMap.get(sexLetter);
    }

    public static IdentityCardType getIdentityCardType(String identityCardTypeId)
    {
        if (null == _identityCardTypeMap)
        {
            _identityCardTypeMap = new HashMap<>();
            IdentityCardType otherDocType = IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.DRUGOY_DOKUMENT);
            _identityCardTypeMap.put(null, IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.BEZ_UDOSTOVERENIYA));
            _identityCardTypeMap.put("1", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII));
            _identityCardTypeMap.put("20", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA));
            _identityCardTypeMap.put("21", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT));
            _identityCardTypeMap.put("17", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.SVIDETELSTVO_O_ROJDENII));
            _identityCardTypeMap.put("22", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.VREMENNOE_UDOSTOVERENIE));
            _identityCardTypeMap.put("19", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.VID_NA_JITELSTVO));
            _identityCardTypeMap.put("2", otherDocType);
            _identityCardTypeMap.put("3", otherDocType);
            _identityCardTypeMap.put("4", otherDocType);
            _identityCardTypeMap.put("23", otherDocType);

            //TODO грязный хак для импорта из экселя
            _identityCardTypeMap.put("вид на жительство", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.VID_NA_JITELSTVO));
            _identityCardTypeMap.put("временное удостоверение", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.VREMENNOE_UDOSTOVERENIE));
            _identityCardTypeMap.put("заграничный паспорт", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT));
            _identityCardTypeMap.put("заграничный паспрорт", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT));
            _identityCardTypeMap.put("иностранный паспорт", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA));
            _identityCardTypeMap.put("паспорт", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII));
            _identityCardTypeMap.put("паспорт рф", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII));
            _identityCardTypeMap.put("паспорт гражданина рф", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII));
            _identityCardTypeMap.put("паспорт гражданина российской федерации", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_ROSSIYSKOY_FEDERATSII));
            _identityCardTypeMap.put("паспорт гражданина иностранного государства", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.PASPORT_GRAJDANINA_INOSTRANNOGO_GOSUDARSTVA));
            _identityCardTypeMap.put("российский заграничный паспорт", IUniDao.instance.get().getCatalogItem(IdentityCardType.class, IdentityCardTypeCodes.ROSSIYSKIY_ZAGRANICHNYY_PASPORT));
        }
        return _identityCardTypeMap.get(identityCardTypeId);
    }

    public static AddressCountry getRussianCitizenship()
    {
        if (null == _russia)
        {
            _russia = DataAccessServices.dao().getNotNull(AddressCountry.class, AddressCountry.code(), 0);
        }
        return _russia;
    }

    public static ICitizenship getCitizenship(String citizenship)
    {
        if (null == _citizenshipMap)
        {
            _citizenshipMap = new HashMap<>();
            _citizenshipMap.put("рф", getRussianCitizenship());

            for(ICitizenship country : DataAccessServices.dao().getList(ICitizenship.class))
            {
                _citizenshipMap.put(country.getTitle().toLowerCase(), country);
                if(country instanceof NoCitizenship) _citizenshipMap.put(null, country);
            }
        }
        return _citizenshipMap.get(null != citizenship ? StringUtils.trimToNull(citizenship) : citizenship);
    }

    public static String getUniquePersonalNumber(Student student)
    {
        if (null == student) return null;

        if (null == _studentIdToPerNumbersMap)
        {
            _studentPerNumbersSet = new HashSet<>();
            _studentIdToPerNumbersMap = new HashMap<>();

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                    .column(property(Student.id().fromAlias("s"))).column(property(Student.personalNumber().fromAlias("s")));

            List<Object[]> perNumList = DataAccessServices.dao().getList(builder);
            for (Object[] loginItem : perNumList)
            {
                String perNumber = (String) loginItem[1];
                _studentPerNumbersSet.add(perNumber);
                _studentIdToPerNumbersMap.put((Long) loginItem[0], perNumber);
            }
        }


        String currNumber = null != student.getId() ? _studentIdToPerNumbersMap.get(student.getId()) : null;

        if (null == currNumber)
        {
            int postfix = 1;
            String result = student.getPersonalNumber();
            while (_studentPerNumbersSet.contains(result))
            {
                result = student.getPersonalNumber() + "/" + postfix++;
            }

            _studentPerNumbersSet.add(result);
            _studentIdToPerNumbersMap.put(student.getId(), result);
            return result;
        }

        return currNumber;
    }

    public static StudentCategory getStudentCategory()
    {
        if (null == _studentCategory)
        {
            _studentCategory = IUniDao.instance.get().getCatalogItem(StudentCategory.class, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);
        }
        return _studentCategory;
    }

    public static CompensationType getCompensationType(boolean contract)
    {
        if (null == _compensationTypeBudget || null == _compensationTypeContract)
        {
            _compensationTypeBudget = IUniDao.instance.get().getCatalogItem(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_BUDGET);
            _compensationTypeContract = IUniDao.instance.get().getCatalogItem(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT);
        }
        return contract ? _compensationTypeContract : _compensationTypeBudget;
    }

    public static StudentStatus getStudentStatus(String statusId)
    {
        if (null == _studentStatusMap)
        {
            _studentStatusMap = new HashMap<>();
            _studentStatusMap.put("1", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
            _studentStatusMap.put("2", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
            _studentStatusMap.put("3", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
            _studentStatusMap.put("4", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
            _studentStatusMap.put("5", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED));
            _studentStatusMap.put("6", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
            _studentStatusMap.put("7", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACADEM));

            //TODO грязный хак для импорта из экселя
            _studentStatusMap.put("академический отпуск", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACADEM));
            _studentStatusMap.put("восстановление", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
            _studentStatusMap.put("зачисление", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
            _studentStatusMap.put("окончание обучения", IUniDao.instance.get().getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
        }
        return _studentStatusMap.get(statusId);
    }

    public static Course getCourse(String lastSemesterNumber)
    {
        if (null == _courseMap)
        {
            _courseMap = new HashMap<>();
            List<Course> courseList = DataAccessServices.dao().getList(Course.class, Course.intValue().s());

            int semester = 1;
            for (Course course : courseList)
            {
                _courseMap.put(String.valueOf(semester++), course);
                _courseMap.put(String.valueOf(semester++), course);
            }
        }
        return _courseMap.get(lastSemesterNumber);
    }

    public static EducationOrgUnit getEducationOrgUnit(StudentWrapper studentWrapper, boolean takeQualificationIntoAccount)
    {
        if (null == _eduOrgUnitMap)
        {
            _eduOrgUnitMap = new HashMap<>();

            List<EducationOrgUnit> eduOrgUnitList = DataAccessServices.dao().getList(EducationOrgUnit.class);
            for (EducationOrgUnit eduOu : eduOrgUnitList)
            {
                StringBuilder builder = new StringBuilder();
                builder.append(eduOu.getFormativeOrgUnit().getTitle().toLowerCase()).append(" ");

                if (eduOu.getEducationLevelHighSchool().getEducationLevel().getLevelType().isHighGos2() || !takeQualificationIntoAccount)
                {
                    builder.append(eduOu.getEducationLevelHighSchool().getTitle().toLowerCase()).append(" ");
                } else
                {
                    builder.append(eduOu.getEducationLevelHighSchool().getEducationLevel().getTitle().toLowerCase()).append(" ");
                    if (eduOu.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramSpecialistDegree())
                        builder.append("специалист ");
                    else if (eduOu.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramBachelorDegree())
                        builder.append("бакалавр ");
                    else if (eduOu.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramMasterDegree())
                        builder.append("магистр ");
                }

                builder.append(eduOu.getDevelopForm().getTitle().toLowerCase()).append(" ");

                EduProgramDuration duration = eduOu.getDevelopPeriod().getEduProgramDuration();
                if (null != duration)
                {
                    builder.append(duration.getNumberOfYears());
                    if (duration.getNumberOfMonths() == 6) builder.append(",5");
                } else
                {
                    String durStr = eduOu.getDevelopPeriod().getTitle();
                    for (int i = 0; i < durStr.length(); i++)
                    {
                        if (StringUtils.isNumeric(durStr.substring(i, i + 1)))
                            builder.append(durStr.substring(i, i + 1));
                    }
                }
                builder.append(" ");

                builder.append(eduOu.getDevelopCondition().getTitle().toLowerCase()).append(" ");
                builder.append(eduOu.getDevelopTech().getTitle().toLowerCase());

                _eduOrgUnitMap.put(builder.toString(), eduOu);
            }
        }

        if(null == studentWrapper.getFacultyName() || null == studentWrapper.getDirectionName()
                || null == studentWrapper.getEduFormName() || null == studentWrapper.getEduDuration())
            return  null;

        StringBuilder keyBuilder = new StringBuilder();
        keyBuilder.append(studentWrapper.getFacultyName().toLowerCase()).append(" ");
        keyBuilder.append(studentWrapper.getDirectionName().toLowerCase()).append(" ");
        if(null != studentWrapper.getQualificationName())
            keyBuilder.append(studentWrapper.getQualificationName().toLowerCase()).append(" ");
        keyBuilder.append(studentWrapper.getEduFormName().toLowerCase()).append(" ");
        keyBuilder.append(studentWrapper.getEduDuration().toLowerCase()).append(" ");
        keyBuilder.append("полный срок обычная");

        return _eduOrgUnitMap.get(keyBuilder.toString());
    }

    public static OrderStates getOrderState()
    {
        if (null == _orderStateCommitted)
        {
            _orderStateCommitted = IUniDao.instance.get().getCatalogItem(OrderStates.class, OrderStatesCodes.FINISHED);
        }
        return _orderStateCommitted;
    }

    public static ExtractStates getExtractState()
    {
        if (null == _extractStateCommitted)
        {
            _extractStateCommitted = IUniDao.instance.get().getCatalogItem(ExtractStates.class, ExtractStatesCodes.FINISHED);
        }
        return _extractStateCommitted;
    }

    public static StudentExtractType getExtractType(String typeName)
    {
        if (null == _extractTypesMap)
        {
            _extractTypesMap = new HashMap<>();
            List<StudentExtractType> extractTypeList = DataAccessServices.dao().getList(StudentExtractType.class);
            for (StudentExtractType type : extractTypeList)
            {
                if (null != type.getParent() && StudentExtractTypeCodes.OTHER_ORDER.equals(type.getParent().getCode()))
                    _extractTypesMap.put(type.getTitle().toLowerCase(), type);
            }
        }
        return _extractTypesMap.get(typeName.toLowerCase());
    }

    public static String getUniqueLogin(Person person)
    {
        if (null == person) return null;

        if (null == _loginMap)
        {
            _loginSet = new HashSet<>();
            _loginMap = new HashMap<>();

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Principal.class, "p")
                    .column(property(PersonRole.person().id().fromAlias("pr"))).column(property(Principal.login().fromAlias("p")))
                    .joinEntity("p", DQLJoinType.left, PersonRole.class, "pr", eq(property(Principal.id().fromAlias("p")), property(PersonRole.principal().fromAlias("pr"))));

            List<Object[]> loginList = DataAccessServices.dao().getList(builder);
            for (Object[] loginItem : loginList)
            {
                String login = (String) loginItem[1];
                _loginSet.add(login);
                _loginMap.put((Long) loginItem[0], login);
            }
        }


        String currLogin = _loginMap.get(person.getId());

        if (null == currLogin)
        {
            final String firstName = person.getIdentityCard().getFirstName();
            final String middleName = person.getIdentityCard().getMiddleName();
            final String lastName = person.getIdentityCard().getLastName();

            int postfix = 1;
            final String login = PersonSecurityUtil.generateLogin(firstName, lastName, middleName);
            String result = login;
            while (_loginSet.contains(result))
            {
                result = login + postfix++;
            }

            _loginSet.add(result);
            _loginMap.put(person.getId(), result);
            return result;
        }

        return currLogin;
    }

    public static AuthenticationType getDefaultAuthType()
    {
        if (null == _authTypeSimple)
        {
            _authTypeSimple = CatalogManager.instance().dao().getCatalogItem(AuthenticationType.class, "simple");
        }

        return _authTypeSimple;
    }
}