/* $Id$ */
package ru.tandemservice.unirsmu.report.ext.EnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.EntrantListAdd.logic.IEnrReportEntrantListDao;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.logic.IEnrReportRatingListDao;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.RatingListAdd.logic.RsmuEnrReportRatingListDao;
import ru.tandemservice.unirsmu.report.ext.EnrReport.logic.RsmuEnrReportEntrantListDao;

/**
 * @author Ekaterina Zvereva
 * @since 08.07.2015
 */
@Configuration
public class EnrReportManagerExt extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEnrReportEntrantListDao entrantListDao()
    {
        return new RsmuEnrReportEntrantListDao();
    }

    @Bean
    @BeanOverride
    public IEnrReportRatingListDao ratingListDao()
    {
        return new RsmuEnrReportRatingListDao();
    }
}
