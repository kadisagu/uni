/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Avetisov
 * @since 10.02.2015
 */
public class RsmuEnrExamGroupDSHandler extends DefaultComboDataSourceHandler
{
    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";

    public RsmuEnrExamGroupDSHandler(String ownerId)
    {
        super(ownerId, EnrExamGroup.class);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrEnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN);
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrExamGroup.class, "g").column("g")
                .where(eqValueNullSafe(property("g", EnrExamGroup.examGroupSet().enrollmentCampaign()), enrollmentCampaign));


        String filter = input.getComboFilterByValue();
        if (StringUtils.isNotEmpty(filter))
        {
            dql.where(likeUpper(property("g", EnrExamGroup.title()), value(CoreStringUtils.escapeLike(filter, true))));
        }

        Set keys = input.getPrimaryKeys();
        if (keys != null && !keys.isEmpty())
        {
            if (keys.size() == 1)
                dql.where(eq(property("g", EnrExamGroup.id()), commonValue(keys.iterator().next())));
            else
                dql.where(in(property("g", EnrExamGroup.id()), keys));
        }
        List<EnrExamGroup> examGroupList = dql.createStatement(context.getSession()).list();
        Collections.sort(examGroupList, (o1, o2) -> NumberAsStringComparator.INSTANCE.compare(o1.getTitle(), o2.getTitle()));

        return ListOutputBuilder.get(input, examGroupList).build();
    }
}
