/* $Id$ */
package ru.tandemservice.unirsmu.exams.ext.EnrExamGroup.logic;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Katkov
 * @since 25.05.2016
 */
public class EnrExamGroupEnrollPassSheetPrintDao extends ru.tandemservice.unienr14.exams.bo.EnrExamGroup.logic.EnrExamGroupEnrollPassSheetPrintDao
{
    @Override
    protected RtfTableModifier injectTableModifier(final EnrExamGroup examGroup, String sheetType)
    {
        final RtfTableModifier tableModifier = new RtfTableModifier();

        /* подготавливаем данные БД */
        final List<EnrExamPassDiscipline> discList = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "d").column(property("d"))
                .where(eq(property(EnrExamPassDiscipline.examGroup().fromAlias("d")), value(examGroup)))
                .order(property(EnrExamPassDiscipline.entrant().person().identityCard().fullFio().fromAlias("d")))
                .createStatement(getSession()).list();

        int number = 1;
        final List<String[]> table = new ArrayList<>();
        for (EnrExamPassDiscipline disc : discList) {
            String[] line;
            if (EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET.equalsIgnoreCase(sheetType))
                line = writeEnrPassSheetLine(disc);
            else if (EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED.equalsIgnoreCase(sheetType))
                line = writeEnrPassSheetCodedLine(disc);
            else if (EnrScriptItemCodes.EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS.equalsIgnoreCase(sheetType))
                line = writeEnrPassSheetMarksLine(disc);
            else
                throw new IllegalStateException();

            line[0] = String.valueOf(number++);

            table.add(line);
        }

        /* метки */
        tableModifier.put("T", table.toArray(new String[table.size()][]));

        return tableModifier;
    }

    @Override
    protected String[] writeEnrPassSheetLine(EnrExamPassDiscipline disc)
    {
        return new String[]{"", disc.getEntrant().getPerson().getFullFio()};
    }

    @Override
    protected String[] writeEnrPassSheetMarksLine(EnrExamPassDiscipline disc)
    {
        return new String[]{"", disc.getEntrant().getPerson().getFullFio(), disc.getAbsenceNote() != null ? disc.getAbsenceNote().getShortTitle() : disc.getMarkAsString()};
    }
}