/* $Id:$ */
package ru.tandemservice.unirsmu.base.ext.EduContract.logic;

import org.apache.commons.lang.mutable.MutableObject;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractDAO;

import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 25.07.2014
 */
public class RsmuEduContractDao extends EduContractDAO
{
    @Override
    public String doGetNextNumber(final String prefix)
    {
        // Номер генерировать по следующей схеме:
        // личный номер абитуриента-XX порядковый номер договора у абитуриента (без лидирующих нулей),
        // для первого договора "-XX" не выводить

        final String key = "eductr_contract_" + prefix;
        final MutableObject holder = new MutableObject();
        UniDaoFacade.getNumberDao().execute(key, new INumberQueueDAO.Action()
            {
                @Override
                public int execute(int postfix)
                {
                    final Set<String> usedNumbers = new HashSet<String>(new DQLSelectBuilder()
                            .fromEntity(CtrContractObject.class, "c")
                            .column(property(CtrContractObject.number().fromAlias("c")))
                            .where(like(property(CtrContractObject.number().fromAlias("c")), value(CoreStringUtils.escapeLike(prefix, true))))
                            .createStatement(getSession()).<String>list());

                    if (usedNumbers.isEmpty())
                    {
                        // договоров еще не было, постфикс не нужен
                        holder.setValue(prefix);
                        return (1);
                    }
                    else if (usedNumbers.size() > 99)
                        // лимит исчерпан
                        throw new ApplicationException("У абитуриента нет свободных номеров для договоров.");

                    postfix = (postfix <= 0 ? 1 : postfix);

                    while (usedNumbers.contains(prefix + "-" + String.valueOf(postfix)))
                    {
                        if (++postfix > 99)
                            // лимит исчерпан
                            throw new ApplicationException("У абитуриента нет свободных номеров для договоров.");
                    }
                    holder.setValue(prefix + "-" + String.valueOf(postfix));
                    return (1 + postfix);
                }
            });

        return (String) holder.getValue();
    }
}
