/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileCoverAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.order.entity.EnrAbstractOrder;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.GlobalList.EnrReportBaseGlobalList;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.RsmuEnrReportManager;

import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 01.08.2014
 */
public class RsmuEnrReportPersonalFileCoverAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;
    private Date _dateFrom;
    private Date _dateTo;
    private boolean _personalNumberActive;
    private String _personalNumber;
    private boolean _enrollmentOrderActive;
    private List<EnrOrder> _enrollmentOrderList;

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
    }

    public void onClickApply()
    {
        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(RsmuEnrReportManager.instance().dao().createMassPersonalFileReport(this))
                        .fileName("EntrantPersonalFileReport.rtf")
                        .rtf(),
                false);

        _uiActivation.asDesktopRoot(EnrReportBaseGlobalList.class).activate();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (RsmuEnrReportPersonalFileCoverAdd.ENROLLMENT_ORDER_DS.equals(dataSource.getName()))
        {
            dataSource.put(RsmuEnrReportPersonalFileCoverAdd.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
        }
    }

    public void onClickCancel()
    {
        _uiActivation.asDesktopRoot(EnrReportBaseGlobalList.class).activate();
    }

    public boolean isPersonalNumberActive()
    {
        return _personalNumberActive;
    }

    public void setPersonalNumberActive(boolean personalNumberActive)
    {
        _personalNumberActive = personalNumberActive;
    }

    public String getPersonalNumber()
    {
        return _personalNumber;
    }

    public void setPersonalNumber(String personalNumber)
    {
        _personalNumber = personalNumber;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return _dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        _dateTo = dateTo;
    }

    public boolean isEnrollmentOrderActive()
    {
        return _enrollmentOrderActive;
    }

    public void setEnrollmentOrderActive(boolean enrollmentOrderActive)
    {
        _enrollmentOrderActive = enrollmentOrderActive;
    }

    public List<EnrOrder> getEnrollmentOrderList()
    {
        return _enrollmentOrderList;
    }

    public void setEnrollmentOrderList(List<EnrOrder> enrollmentOrderList)
    {
        _enrollmentOrderList = enrollmentOrderList;
    }
}