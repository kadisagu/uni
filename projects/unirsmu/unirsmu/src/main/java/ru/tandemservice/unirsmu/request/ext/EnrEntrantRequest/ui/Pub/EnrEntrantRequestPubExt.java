/* $Id:$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.Pub.EnrEntrantRequestPub;
import ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.ActionsAddon.RsmuEntrantRequestActionsAddon;

/**
 * @author Denis Perminov
 * @since 17.06.2014
 */
@Configuration
public class EnrEntrantRequestPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantRequestPub _enrEntrantRequestPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantRequestPub.presenterExtPoint())
            .replaceAddon(uiAddon(EnrEntrantRequestActionsAddon.NAME, RsmuEntrantRequestActionsAddon.class))
            .create();
    }
}
