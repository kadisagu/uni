/* $Id$ */
package ru.tandemservice.unirsmu.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

/**
 * @author Andrey Avetisov
 * @since 12.01.2015
 */

@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager
{
    public static final String RSMU_SYSTEM_ACTION_PUB_ADDON_NAME = "rsmuSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(RSMU_SYSTEM_ACTION_PUB_ADDON_NAME, RsmuSystemActionPubAddon.class))
                .create();
    }
}
