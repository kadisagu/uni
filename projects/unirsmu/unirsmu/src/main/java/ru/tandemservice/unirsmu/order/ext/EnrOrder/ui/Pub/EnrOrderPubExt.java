/* $Id$ */
package ru.tandemservice.unirsmu.order.ext.EnrOrder.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2014
 */
@Configuration
public class EnrOrderPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrOrderPub _enrOrderPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrOrderPub.presenterExtPoint())
                .addAction(new PrintExtractsAction("onClickPrintExtracts"))
                .create();
    }
}