/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantExaminationResultAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic.RsmuExamGroupDSHandler;

import java.util.List;
import java.util.Set;

/**
 * @author Andrey Avetisov
 * @since 30.01.2015
 */

@Configuration
public class RsmuEnrReportEntrantExaminationResultAdd extends BusinessComponentManager
{
    public static final String REPORT_KEY = "rsmuEnr14ReportEntrantExaminationResultAdd";
    public static final String ENTRANT_STATE_DS = "entrantStateDS";
    public static final String EXAM_SET_DS = "examSetDS";
    public static final String EXAM_PASS_FORM_DS = "examPassFormDS";
    public static final String EXAM_GROUP_DS = "examGroupDS";
    public static final String ABSENCE_NOTE_DS = "absenceNoteDS";

    public static final String ENROLLMENT_CAMPAIGN = "enrollmentCampaign";



    public static final String COMPETITION_FILTER_ADDON = CommonFilterAddon.class.getSimpleName();

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EnrEnrollmentCampaignManager.instance().enrCampaignDSConfig())
                .addAddon(uiAddon(COMPETITION_FILTER_ADDON, EnrCompetitionFilterAddon.class))
                .addDataSource(selectDS(ENTRANT_STATE_DS, entrantStateDSHandler()))
                .addDataSource(selectDS(EXAM_PASS_FORM_DS, examPassFormDSHandler()))
                .addDataSource(selectDS(EXAM_GROUP_DS, examGroupDSHandler()))
                .addDataSource(selectDS(ABSENCE_NOTE_DS, absenceNoteDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler entrantStateDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrEntrantState.class)
                .order(EnrEntrantState.title())
                .filter(EnrEntrantState.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler examPassFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamPassForm.class)
                .filter(EnrExamPassForm.title())
                .where(EnrExamPassForm.internal(), true)
                .order(EnrExamPassForm.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler examGroupDSHandler()
    {
        return new RsmuExamGroupDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler absenceNoteDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrAbsenceNote.class)
                .filter(EnrAbsenceNote.title())
                .order(EnrAbsenceNote.title());

    }
}
