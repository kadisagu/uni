/* $Id$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.AddAppend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.AddAppend.EnrEntrantRequestAddAppend;

/**
 * @author Andrey Avetisov
 * @since 07.04.2015
 */
@Configuration
public class EnrEntrantRequestAddAppendExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantRequestAddAppend _enrEntrantRequestAddAppend;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantRequestAddAppend.presenterExtPoint())
                .addAction(new EnrEntrantRequestAddAppendClickSelect("onClickSelect"))
                .create();
    }
}
