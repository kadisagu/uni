/* $Id:$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.ActionsAddon;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;

/**
 * @author Denis Perminov
 * @since 13.07.2014
 */
public class RsmuEntrantRequestActionsAddon extends EnrEntrantRequestActionsAddon
{
    public RsmuEntrantRequestActionsAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onClickPrintEntrantLetterbox() throws Exception
    {
        // документ печатается только когда состояние Абитуриента "ЗАЧИСЛЕН"!
        Long listenerParameter = getListenerParameter();
        EnrEntrantRequest entrantRequest = DataAccessServices.dao().get(EnrEntrantRequest.class, listenerParameter);
        String code = entrantRequest.getEntrant().getState().getCode();

        if (!EnrEntrantStateCodes.ENROLLED.equals(code)) {
            throw new ApplicationException("Документ выводится на печать после проведения приказа о зачислении абитуриента.");
        }

        EnrScriptItem scriptItem =  DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_LETTER_BOX);
        if(scriptItem.isPrintPdf())
        {
            UniRtfUtil.downloadRtfScriptResultAsPdf(CommonManager.instance().scriptDao().getScriptResult(
                    scriptItem, listenerParameter));
        }
        else
        {
            CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(
                    scriptItem, listenerParameter);
        }
    }
}
