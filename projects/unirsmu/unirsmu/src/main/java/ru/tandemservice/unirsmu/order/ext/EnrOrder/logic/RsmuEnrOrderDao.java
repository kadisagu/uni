/* $Id$ */
package ru.tandemservice.unirsmu.order.ext.EnrOrder.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderDao;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Nikolay Fedorovskih
 * @since 30.07.2014
 */
public class RsmuEnrOrderDao extends EnrOrderDao implements IRsmuEnrOrderDao
{
    @Override
    public String getDefaultOrderBasicText(String enrOrderTypeCode)
    {
        if (EnrOrderTypeCodes.ENROLLMENT.equals(enrOrderTypeCode))
        {
            return "В соответствии с приказом Министерства образования и науки Российской Федерации N3 от 09.01.2014 г., " +
                    "правилами приёма на обучение по образовательным программам высшего образования в Государственное " +
                    "бюджетное образовательное учреждение высшего профессионального образования " +
                    "\"Российский национальный исследовательский медицинский университет имени Н.И. Пирогова\" " +
                    "Министерства здравоохранения Российской Федерации в 2014 году и на основании решения приёмной комиссии ";
        }
        return super.getDefaultOrderBasicText(enrOrderTypeCode);
    }

    @Override
    public byte[] printOrderExtracts(Long enrOrderId)
    {
        // Список выписок приказа. Сортируем по ФИО
        List<Long> extractIds = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e")
                .column(property("e", EnrEnrollmentExtract.id()))
                .where(eqValue(property("e", EnrEnrollmentExtract.paragraph().order()), enrOrderId))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fromAlias("e"), "card")
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME))
                .createStatement(getSession()).list();

        if (extractIds.isEmpty())
            throw new ApplicationException("Нет выписок для печати.");

        final Iterator<Long> iterator = extractIds.iterator();
        // Скрипт печати выписки и шаблон
        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.RSMU_ENROLLMENT_EXTRACT);
        final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        // Разрыв страницы
        final IRtfControl pageBreak = elementFactory.createRtfControl(IRtfData.PAGE);
        // Первая выписка будет изначальным документов, в который будут вставляться все остальные
        final RtfReader reader = new RtfReader();
        List<IRtfElement> mainElementList = null;
        RtfDocument mainDoc = null;
        RtfDocument extractDoc = null;
        // На странице будет по выписке но в двух экземплярах (одна выдается студенту, друга остается в личном деле)
        int counter = 0;
        do
        {
            if (counter % 2 == 0)
            {
                Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
                extractDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));

                if (mainDoc == null)
                {
                    mainDoc = extractDoc.getClone();
                    mainElementList = mainDoc.getElementList();
                }

                if (counter > 0)
                {
                    mainElementList.add(pageBreak);
                }
            }
            else
            {
                if (extractDoc == null)
                    throw new NullPointerException();
                extractDoc = extractDoc.getClone();
            }

            if (counter > 0)
            {
                mainElementList.addAll(extractDoc.getElementList());
            }
            counter++;
        }
        while (iterator.hasNext() || counter % 2 != 0);

        return RtfUtil.toByteArray(mainDoc);
    }
}