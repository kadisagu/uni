/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.PageOrientation;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * @author Andrey Avetisov
 * @since 10.02.2015
 */
public class RsmuEnrNotAppearEntrantExcelBuilder
{
    List<ViewWrapper> _enrEntrantList;
    public RsmuEnrNotAppearEntrantExcelBuilder(List<ViewWrapper> enrEntrantList)
    {
        _enrEntrantList=enrEntrantList;
    }

    public ByteArrayOutputStream buildReport() throws Exception
    {
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        final WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем шрифты
        WritableFont arial12 = new WritableFont(WritableFont.ARIAL, 12);
        WritableFont arial12bold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);

        // шрифт для шапки отчета
        WritableCellFormat headerFormat = new WritableCellFormat(arial12bold);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        headerFormat.setBackground(jxl.format.Colour.GRAY_25);
        headerFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        // шрифт для строк отчета
        WritableCellFormat rowFormat = new WritableCellFormat(arial12);
        rowFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        rowFormat.setWrap(true);
        try
        {
            WritableSheet sheet = workbook.createSheet("абитуриенты", workbook.getSheets().length);
            sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
            sheet.getSettings().setCopies(1);
            int excelRowNumber = 0;
            int excelColNumber = 0;

            String[] headerTitles = {"№", "Личный номер", "№ заявления", "Абитуриент", "Состояние"};

            for (String title : headerTitles)
            {
                sheet.addCell(new Label(excelColNumber, 0, title, headerFormat));
                sheet.setColumnView(excelColNumber, 30);
                excelColNumber++;
            }

            excelRowNumber++;



            for (ViewWrapper wrapper : _enrEntrantList)
            {
                EnrEntrant entrant = (EnrEntrant)wrapper.getEntity();
                String requestNumber = !CollectionUtils.isEmpty((List)wrapper.getViewProperty(RsmuEnrNotAppearEntrantDSHandler.VIEW_PROP_REQUEST))?
                        ((DataWrapper)((List)wrapper.getViewProperty(RsmuEnrNotAppearEntrantDSHandler.VIEW_PROP_REQUEST)).get(0)).getTitle():"";
                sheet.addCell(new Label(0, excelRowNumber, String.valueOf(excelRowNumber), rowFormat));
                sheet.addCell(new Label(1, excelRowNumber, (String)wrapper.getProperty("personalNumber"), rowFormat));
                sheet.addCell(new Label(2, excelRowNumber, requestNumber, rowFormat));
                sheet.addCell(new Label(3, excelRowNumber, entrant.getFullFio(), rowFormat));
                sheet.addCell(new Label(4, excelRowNumber, entrant.getState()!=null? entrant.getState().getTitle():"", rowFormat));
                excelRowNumber++;
            }

        }
        catch (RowsExceededException e)
        {
            throw new ApplicationException("Невозможно сформировать отчет, так как объем итоговых данных превышает допустимый предел в 65536 строк.");
        }
        workbook.write();
        workbook.close();

        return out;
    }
}
