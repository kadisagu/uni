package ru.tandemservice.unirsmu.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unirsmu.entity.gen.*;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.RatingListAdd.RsmuEnrReportRatingListAdd;

import java.util.Arrays;
import java.util.List;

/**
 * Рейтинговые (конкурсные) списки, списки поступающих (РНИМУ)
 */
public class RsmuEnrReportRatingList extends RsmuEnrReportRatingListGen implements IEnrReport
{
    public static final String REPORT_KEY = "rsmuEnr14ReportRatingList";

    private static List<String> properties = Arrays.asList(
            P_REQUEST_TYPE,
            P_COMPENSATION_TYPE,
            P_PROGRAM_FORM,
            P_COMPETITION_TYPE,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET,
            P_PARALLEL
    );

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return RsmuEnrReportRatingList.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return RsmuEnrReportRatingListAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Рейтинговые (конкурсные) списки, списки поступающих (РНИМУ)»"; }
            @Override public String getListTitle() { return "Список отчетов «Рейтинговые (конкурсные) списки, списки поступающих (РНИМУ)»"; }
        };
    }

    @Override public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}