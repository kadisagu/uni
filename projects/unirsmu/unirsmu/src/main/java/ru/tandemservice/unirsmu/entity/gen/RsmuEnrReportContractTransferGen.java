package ru.tandemservice.unirsmu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Акт передачи договоров зачисленных абитуриентов (РНИМУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RsmuEnrReportContractTransferGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer";
    public static final String ENTITY_NAME = "rsmuEnrReportContractTransfer";
    public static final int VERSION_HASH = -1803677205;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_ENR_ORDER = "enrOrder";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_PROGRAM_SUBJECT = "programSubject";
    public static final String P_EDU_PROGRAM = "eduProgram";
    public static final String P_PROGRAM_SET = "programSet";

    private EnrEnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private String _enrOrder;     // Приказ о зачислении
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _programSubject;     // Направление, спец., профессия
    private String _eduProgram;     // Образовательная программа
    private String _programSet;     // Набор образовательных программ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Приказ о зачислении. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrOrder()
    {
        return _enrOrder;
    }

    /**
     * @param enrOrder Приказ о зачислении. Свойство не может быть null.
     */
    public void setEnrOrder(String enrOrder)
    {
        dirty(_enrOrder, enrOrder);
        _enrOrder = enrOrder;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Направление, спец., профессия.
     */
    public String getProgramSubject()
    {
        return _programSubject;
    }

    /**
     * @param programSubject Направление, спец., профессия.
     */
    public void setProgramSubject(String programSubject)
    {
        dirty(_programSubject, programSubject);
        _programSubject = programSubject;
    }

    /**
     * @return Образовательная программа.
     */
    public String getEduProgram()
    {
        return _eduProgram;
    }

    /**
     * @param eduProgram Образовательная программа.
     */
    public void setEduProgram(String eduProgram)
    {
        dirty(_eduProgram, eduProgram);
        _eduProgram = eduProgram;
    }

    /**
     * @return Набор образовательных программ.
     */
    public String getProgramSet()
    {
        return _programSet;
    }

    /**
     * @param programSet Набор образовательных программ.
     */
    public void setProgramSet(String programSet)
    {
        dirty(_programSet, programSet);
        _programSet = programSet;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof RsmuEnrReportContractTransferGen)
        {
            setEnrollmentCampaign(((RsmuEnrReportContractTransfer)another).getEnrollmentCampaign());
            setEnrOrder(((RsmuEnrReportContractTransfer)another).getEnrOrder());
            setFormativeOrgUnit(((RsmuEnrReportContractTransfer)another).getFormativeOrgUnit());
            setProgramSubject(((RsmuEnrReportContractTransfer)another).getProgramSubject());
            setEduProgram(((RsmuEnrReportContractTransfer)another).getEduProgram());
            setProgramSet(((RsmuEnrReportContractTransfer)another).getProgramSet());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RsmuEnrReportContractTransferGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RsmuEnrReportContractTransfer.class;
        }

        public T newInstance()
        {
            return (T) new RsmuEnrReportContractTransfer();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "enrOrder":
                    return obj.getEnrOrder();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "programSubject":
                    return obj.getProgramSubject();
                case "eduProgram":
                    return obj.getEduProgram();
                case "programSet":
                    return obj.getProgramSet();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrEnrollmentCampaign) value);
                    return;
                case "enrOrder":
                    obj.setEnrOrder((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "programSubject":
                    obj.setProgramSubject((String) value);
                    return;
                case "eduProgram":
                    obj.setEduProgram((String) value);
                    return;
                case "programSet":
                    obj.setProgramSet((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "enrOrder":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "programSubject":
                        return true;
                case "eduProgram":
                        return true;
                case "programSet":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "enrOrder":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "programSubject":
                    return true;
                case "eduProgram":
                    return true;
                case "programSet":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return EnrEnrollmentCampaign.class;
                case "enrOrder":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "programSubject":
                    return String.class;
                case "eduProgram":
                    return String.class;
                case "programSet":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RsmuEnrReportContractTransfer> _dslPath = new Path<RsmuEnrReportContractTransfer>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RsmuEnrReportContractTransfer");
    }
            

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getEnrollmentCampaign()
     */
    public static EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Приказ о зачислении. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getEnrOrder()
     */
    public static PropertyPath<String> enrOrder()
    {
        return _dslPath.enrOrder();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getProgramSubject()
     */
    public static PropertyPath<String> programSubject()
    {
        return _dslPath.programSubject();
    }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getEduProgram()
     */
    public static PropertyPath<String> eduProgram()
    {
        return _dslPath.eduProgram();
    }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getProgramSet()
     */
    public static PropertyPath<String> programSet()
    {
        return _dslPath.programSet();
    }

    public static class Path<E extends RsmuEnrReportContractTransfer> extends StorableReport.Path<E>
    {
        private EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<String> _enrOrder;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _programSubject;
        private PropertyPath<String> _eduProgram;
        private PropertyPath<String> _programSet;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getEnrollmentCampaign()
     */
        public EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrEnrollmentCampaign.Path<EnrEnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Приказ о зачислении. Свойство не может быть null.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getEnrOrder()
     */
        public PropertyPath<String> enrOrder()
        {
            if(_enrOrder == null )
                _enrOrder = new PropertyPath<String>(RsmuEnrReportContractTransferGen.P_ENR_ORDER, this);
            return _enrOrder;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(RsmuEnrReportContractTransferGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Направление, спец., профессия.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getProgramSubject()
     */
        public PropertyPath<String> programSubject()
        {
            if(_programSubject == null )
                _programSubject = new PropertyPath<String>(RsmuEnrReportContractTransferGen.P_PROGRAM_SUBJECT, this);
            return _programSubject;
        }

    /**
     * @return Образовательная программа.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getEduProgram()
     */
        public PropertyPath<String> eduProgram()
        {
            if(_eduProgram == null )
                _eduProgram = new PropertyPath<String>(RsmuEnrReportContractTransferGen.P_EDU_PROGRAM, this);
            return _eduProgram;
        }

    /**
     * @return Набор образовательных программ.
     * @see ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer#getProgramSet()
     */
        public PropertyPath<String> programSet()
        {
            if(_programSet == null )
                _programSet = new PropertyPath<String>(RsmuEnrReportContractTransferGen.P_PROGRAM_SET, this);
            return _programSet;
        }

        public Class getEntityClass()
        {
            return RsmuEnrReportContractTransfer.class;
        }

        public String getEntityName()
        {
            return "rsmuEnrReportContractTransfer";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
