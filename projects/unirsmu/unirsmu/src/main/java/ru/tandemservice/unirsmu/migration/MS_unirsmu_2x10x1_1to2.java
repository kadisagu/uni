package ru.tandemservice.unirsmu.migration;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unirsmu_2x10x1_1to2 extends IndependentMigrationScript
{
    // CtrPrintTemplateCodes
    /** Константа кода (code) элемента : С иностранцем (title) */
    private String RSMU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_FOREIGN = "rsmu.edu.ctr.template.vo.2s.foreign";
    /** Константа кода (code) элемента : С иностранцем (title) */
    private String RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_FOREIGN = "rsmu.edu.ctr.template.vo.3sp.foreign";
    /** Константа кода (code) элемента : С иностранцем (title) */
    private String RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_FOREIGN = "rsmu.edu.ctr.template.vo.3so.foreign";

    // CtrContractKindCodes
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
    private String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
    /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
    private String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";


    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        // Получаем уже существующие элементы ctrPrintTemplate
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        SQLSelectQuery selectCtrPrintTemplateQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_prnt_tmpl_t", "pt").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=pt.id"))
                .column("si.id")
                .column("si.code_p");

        List<Object[]> ctrPrintTemplates = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectCtrPrintTemplateQuery));

        for(Object[] obj : ctrPrintTemplates)
        {
            ctrPrintTemplateMap.put((String) obj[1], (Long) obj[0]);
        }

        fillCatalogs(ctrPrintTemplateMap, contractKindMap, tool);

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractVersion

        // заполняем printTemplate и kind

        SQLSelectQuery selectContractVersionQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null or printtemplate_id is null");

        List<Object[]> contractVersionList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        for (Object[] contractVersion : contractVersionList)
        {
            setCtrPrintTemplate((Long) contractVersion[0], ctrPrintTemplateMap, contractKindMap, tool);
        }

        // Проверяем, что нет версий без вида, если нет, то делаем колонку обязательной
        SQLSelectQuery selectCtrVerQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null");

        List<Object[]> ctrVerList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        if (ctrVerList.isEmpty())
        {
            tool.setColumnNullable("ctr_contractver_t", "kind_id", false);
        }
        updateCtrType(tool);
        clearCatalogs(tool);
    }

    private void updateCtrType(DBTool tool) throws SQLException
    {
        // сменить тип договора c "01.01.vpf" на "01.01.vpo"

        Map<String, Long> ctrTypeMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontracttype_t");
        Long vpoId = ctrTypeMap.get("01.01.vpo");
        Long vpoForeignId = ctrTypeMap.get("01.01.vpf");
        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractobj_t").set("type_id", "?").where("type_id=?");

        tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery), vpoId, vpoForeignId);
    }

    private void setCtrPrintTemplate(Long contractVersionId, Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractver_t")
                .set("kind_id", "?")
                .set("printtemplate_id", "?")
                .where("id=?");

        // пропускаем если не "Договор с иностранцем на обучение ВПО" ("01.01.vpf")
        if((CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "eductr_stduent_c_ctmpldt_t", tool) || CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "enr14_ctmpldt_simple_t", tool)) && CtrMigrationUtil.checkContractType(contractVersionId, "01.01.vpf", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_VO_2_SIDES,
                            EDU_CONTRACT_VO_3_SIDES_PERSON,
                            EDU_CONTRACT_VO_3_SIDES_ORG,
                            contractKindMap, tool),
                    getPrintTemplate(contractVersionId,
                            RSMU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_FOREIGN,
                            RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_FOREIGN,
                            RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_FOREIGN,
                            ctrPrintTemplateMap, tool),
                    contractVersionId);
        }
    }

    private Long getKind(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        Long ctrKind;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrKind = contractKindMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrKind = contractKindMap.get(threeSidePerson);
            }
            else
            {
                ctrKind = contractKindMap.get(threeSideOrg);
            }
        }

        return ctrKind;
    }


    private Long getPrintTemplate(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> ctrPrintTemplateMap, DBTool tool) throws SQLException
    {
        Long ctrPrintTemplate;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrPrintTemplate = ctrPrintTemplateMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSidePerson);
            }
            else
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSideOrg);
            }
        }

        return ctrPrintTemplate;
    }

    private boolean checkTwoSides(Long contractVersionId, DBTool tool) throws SQLException
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=c.contactor_id")
        )
                .column("pr.person_id")
                .where("c.owner_id=?")
                .where("r.code_p=?");

        // CtrContractRole
        /** Константа кода (code) элемента : Заказчик (title) */
        String EDU_CONTRACT_CUSTOMER = "01.01.customer";
        /** Константа кода (code) элемента : Исполнитель (title) */
        String EDU_CONTRACT_PROVIDER = "01.01.provider";

        Long providerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_PROVIDER);
        Long customerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_CUSTOMER);

        SQLSelectQuery studentPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv").innerJoin(SQLFrom.table("eductr_student_contract_t", "sc"), "sc.contractobject_id=cv.contract_id") .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=sc.student_id")).column("pr.person_id")
                .where("cv.id=?");

        Long studentPerson = (Long) tool.getUniqueResult(translator.toSql(studentPersonQuery), contractVersionId);

        SQLSelectQuery entrantPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")
                .innerJoin(SQLFrom.table("enr14_entrant_contract_t", "ec"), "ec.contractobject_id=cv.contract_id")
                .innerJoin(SQLFrom.table("enr14_requested_comp_t", "rc"), "rc.id=ec.requestedcompetition_id")
                .innerJoin(SQLFrom.table("enr14_request_t", "r"), "r.id=rc.request_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=r.entrant_id")).column("pr.person_id")
                .where("cv.id=?");

        Long entrantPerson = (Long) tool.getUniqueResult(translator.toSql(entrantPersonQuery), contractVersionId);

        if(entrantPerson == null && studentPerson == null)
        {
            return true;
        }

        if(entrantPerson != null) {
            return Objects.equals(entrantPerson, providerPerson) || Objects.equals(entrantPerson, customerPerson);
        }

        return Objects.equals(studentPerson, providerPerson) || Objects.equals(studentPerson, customerPerson);
    }

    private boolean checkPersonSide(Long contractVersionId, DBTool tool) throws SQLException
    {
        // Проверяем по наличию юр. лица

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("juridicalcontactor_t", "jc"), "jc.id=c.contactor_id")
        )
                .where("c.owner_id=?")
                .column("jc.id");

        List<Object[]> juridicalContator = tool.executeQuery(MigrationUtils.processor(Long.class), translator.toSql(contractorQuery), contractVersionId);

        return juridicalContator.isEmpty();
    }

    private void addCtrPrintTemplate(Long id, String code, String templatePath, String scriptPath, Object[] templateScriptItem, short ctrPrintTemplateCode, Long kindId, String title, String shortTitle, DBTool tool, ISQLTranslator translator) throws SQLException
    {
        SQLInsertQuery insertScriptItemQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertScriptItemNoBlobQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
//                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertCtrPrintTemplateQuery = new SQLInsertQuery("ctr_c_prnt_tmpl_t")
                .set("id", "?")
                .set("shorttitle_p", "?")
                .set("kind_id", "?");

        String catalogCode = "ctrPrintTemplate";

        Blob userTemplate;
        Timestamp userTemplateEditDate;
        String userTemplateComment;
        String userScript;
        String defaultScript;
        Timestamp userScriptEditDate;
        Boolean useScript;

        if(templateScriptItem != null)
        {
            userTemplate = (Blob) templateScriptItem[2];
            userTemplateEditDate = (Timestamp) templateScriptItem[3];
            userTemplateComment = (String) templateScriptItem[4];
            userScript = (String) templateScriptItem[5];
            defaultScript = (String) templateScriptItem[6];
            userScriptEditDate = (Timestamp) templateScriptItem[7];
            useScript = (Boolean) templateScriptItem[8];
            if(useScript == null) useScript = false;
        }
        else
        {
            userTemplate = null;
            userTemplateEditDate = null;
            userTemplateComment = null;
            userScript = null;
            defaultScript = null;
            userScriptEditDate = null;
            useScript = false;
        }
        if(userTemplate != null)
        {
            tool.executeUpdate(translator.toSql(insertScriptItemQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplate, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        else
        {
            tool.executeUpdate(translator.toSql(insertScriptItemNoBlobQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        tool.executeUpdate(translator.toSql(insertCtrPrintTemplateQuery), id, shortTitle, kindId);
    }

    private void fillCatalogs(Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrPrintTemplate

        // гарантировать наличие кода сущности
        short ctrPrintTemplateCode = tool.entityCodes().ensure("ctrPrintTemplate");


        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        ////////////////////////////////////////////////////////////////////////////////
        // заполняем справочники ctrPrintTemplate ctr_c_prnt_tmpl_t

        // Получаем скрипты ctrTemplateScriptItem которые мигрируем
        SQLSelectQuery selectСtrTemplateScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_tmpl_script_t", "ts").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=ts.id"))
                .column("si.id")
                .column("si.code_p")
                .column("si.usertemplate_p")
                .column("si.usertemplateeditdate_p")
                .column("si.usertemplatecomment_p")
                .column("si.userscript_p")
                .column("si.defaultscript_p")
                .column("si.userscripteditdate_p")
                .column("si.usescript_p");

        List<Object[]> ctrTemplateScriptItems = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class, Blob.class, Timestamp.class, String.class, String.class, String.class, Timestamp.class, Boolean.class),
                translator.toSql(selectСtrTemplateScriptItemQuery));

        final Map<String, Object[]> ctrTemplateScriptItemMap = Maps.newHashMap();
        for(Object[] obj : ctrTemplateScriptItems)
        {
            ctrTemplateScriptItemMap.put((String) obj[1], obj);
        }

        tool.dropConstraint("scriptitem_t", "chk_class_scriptitem");

        String ctrPrintTemplateTitle = "С иностранцем";
        String ctrPrintTemplateShortTitle = "с иностр.";

        // code - "rsmu.edu.ctr.template.vo.2s.foreign",
        // templatePath - "unirsmu/templates/ctr/EduCtrContractForeign2Side.rtf"
        // scriptPath - "unirsmu/scripts/EduCtrContractPrintScriptForeign.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(RSMU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_FOREIGN))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = RSMU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_FOREIGN;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unirsmu/templates/ctr/EduCtrContractForeign2Side.rtf";
            String scriptPath = "unirsmu/scripts/EduCtrContractPrintScriptForeign.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.ctrtemplate.2sf");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_2_SIDES), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "rsmu.edu.ctr.template.vo.3sp.foreign",
        // templatePath - "unirsmu/templates/ctr/EduCtrContractForeign3SidePerson.rtf"
        // scriptPath - "unirsmu/scripts/EduCtrContractPrintScriptForeign.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_FOREIGN))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_FOREIGN;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unirsmu/templates/ctr/EduCtrContractForeign3SidePerson.rtf";
            String scriptPath = "unirsmu/scripts/EduCtrContractPrintScriptForeign.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.ctrtemplate.3spf");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "rsmu.edu.ctr.template.vo.3so.foreign",
        // templatePath - "unirsmu/templates/ctr/EduCtrContractForeign3SideOrg.rtf"
        // scriptPath - "unirsmu/scripts/EduCtrContractPrintScriptForeign.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_FOREIGN))
        {
            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = RSMU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_FOREIGN;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "unirsmu/templates/ctr/EduCtrContractForeign3SideOrg.rtf";
            String scriptPath = "unirsmu/scripts/EduCtrContractPrintScriptForeign.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("edu.ctrtemplate.3sof");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }
    }

    private void clearCatalogs(DBTool tool) throws SQLException
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        // удаляем скрипты
        // "edu.ctrtemplate.2sf" Договор с иностранцем на обучение по ОП (двухсторонний)
        // "edu.ctrtemplate.3spf" Договор с иностранцем на обучение по ОП (трехсторонний с физ. лицом)
        // "edu.ctrtemplate.3sof" Договор с иностранцем на обучение по ОП (трехсторонний с юр. лицом)

        SQLSelectQuery selectScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("scriptitem_t"))
                .where("code_p in ('edu.ctrtemplate.2sf', 'edu.ctrtemplate.3spf', 'edu.ctrtemplate.3sof')")
                .column("id");

        List<Object[]> scriptItemIdObjList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                translator.toSql(selectScriptItemQuery));

        if(!scriptItemIdObjList.isEmpty())
        {
            List<Long> scriptItemIdList = scriptItemIdObjList.stream().map(a -> (Long) a[0]).collect(Collectors.toList());

            SQLDeleteQuery deleteCtrTemplateScriptQuery = new SQLDeleteQuery("ctr_c_tmpl_script_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(translator.toSql(deleteCtrTemplateScriptQuery));

            SQLDeleteQuery deleteScriptItemQuery = new SQLDeleteQuery("scriptitem_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(translator.toSql(deleteScriptItemQuery));
        }

        // Удаляем типы договоров с иностранцем "01.01.vpf" и "01.01.spf" "ctrcontracttype_t"
        SQLDeleteQuery deleteCtrTypeBuilder = new SQLDeleteQuery("ctrcontracttype_t").where("code_p in ('01.01.vpf', '01.01.spf')");
        tool.executeUpdate(translator.toSql(deleteCtrTypeBuilder));
    }
}
