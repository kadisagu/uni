/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.ui.ChangeExamGroup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroupScheduleEvent;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.logic.EnrOrgUnitBaseDSHandler;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.unienr14.settings.entity.EnrExamGroupSet;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 11.02.2015
 */

@Configuration
public class RsmuEnrNotAppearEntrantChangeExamGroup extends BusinessComponentManager
{
    public static final String ORG_UNIT_DS = "orgUnitDS";
    public static final String EXAM_GROUP_SET_DS = "examGroupSetDS";
    public static final String EXAM_GROUP_DS = "examGroupDS";

    public static final String BIND_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String BIND_ORG_UNIT = "orgUnit";
    public static final String BIND_EXAM_GROUP_SET = "examGroupSet";
    public static final String BIND_DISCIPLINE = "discipline";
    public static final String BIND_PASS_FORM = "passForm";
    public static final String BIND_PREVIOUS_EXAM_GROUP = "prevExamGroup";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(ORG_UNIT_DS, orgUnitDSHandler()).addColumn(EnrOrgUnit.institutionOrgUnit().orgUnit().shortTitleWithTopEmphasized().s()))
                .addDataSource(selectDS(EXAM_GROUP_SET_DS, examGroupSetSelectDSHandler()).addColumn(EnrExamGroupSet.periodTitle().s()))
                .addDataSource(selectDS(EXAM_GROUP_DS, examGroupSelectDSHandler()).addColumn(EnrExamGroup.title().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSetSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroupSet.class)
                .where(EnrExamGroupSet.enrollmentCampaign(), BIND_ENROLLMENT_CAMPAIGN)
                .order(EnrExamGroupSet.beginDate())
                .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> examGroupSelectDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EnrExamGroup.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);
                EnrEnrollmentCampaign campaign = context.get(BIND_ENROLLMENT_CAMPAIGN);

                if (campaign == null) {
                    dql.where(isNull(alias + ".id"));
                    return;
                }

                EnrExamGroup prevExamGroup = context.get(BIND_PREVIOUS_EXAM_GROUP);

                // только открытые или текущая
                dql.where(or(
                        eq(property(EnrExamGroup.closed().fromAlias(alias)), value(Boolean.FALSE)),
                        eq(property(alias + ".id"), value(prevExamGroup == null ? null : prevExamGroup.getId()))
                ));

                // только поставленные в расписание
                DQLSelectBuilder innerDql = new DQLSelectBuilder()
                        .fromEntity(EnrExamGroupScheduleEvent.class, "e")
                        .where(eq(property(EnrExamGroupScheduleEvent.examGroup().examGroupSet().enrollmentCampaign().fromAlias("e")), value(campaign)))
                        .column(property(EnrExamGroupScheduleEvent.examGroup().id().fromAlias("e")));
                dql.where(in(alias + ".id", innerDql.buildQuery()));
            }
        }
                .where(EnrExamGroup.examGroupSet(), BIND_EXAM_GROUP_SET)
                .where(EnrExamGroup.discipline(), BIND_DISCIPLINE)
                .where(EnrExamGroup.passForm(), BIND_PASS_FORM)
                .where(EnrExamGroup.territorialOrgUnit(), BIND_ORG_UNIT)
                .order(EnrExamGroup.title())
                .filter(EnrExamGroup.title())
                .pageable(true);
    }

    @Bean
    public IDefaultComboDataSourceHandler orgUnitDSHandler()
    {
        return new EnrOrgUnitBaseDSHandler(getName())
                .where(EnrOrgUnit.enrollmentCampaign(), BIND_ENROLLMENT_CAMPAIGN);
    }
}
