/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.RatingListAdd;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.process.*;
import ru.tandemservice.unienr14.report.bo.EnrReport.ui.RatingListAdd.EnrReportRatingListAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.RsmuEnrReportManager;

/**
 * @author Nikolay Fedorovskih
 * @since 04.07.2014
 */
public class RsmuEnrReportRatingListAddUI extends EnrReportRatingListAddUI
{
    @Override
    public boolean isPrintEntrantNumberColumn()
    {
        return true;
    }

    @Override
    public void onClickApply()
    {
        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase() {
            @Override
            public ProcessResult run(final ProcessState state) {
                setReportId(RsmuEnrReportManager.instance().dao().createEntrantRatingListReport(RsmuEnrReportRatingListAddUI.this));
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }


}