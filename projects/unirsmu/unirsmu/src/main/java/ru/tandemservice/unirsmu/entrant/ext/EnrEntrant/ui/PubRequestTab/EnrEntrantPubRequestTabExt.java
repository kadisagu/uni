/* $Id:$ */
package ru.tandemservice.unirsmu.entrant.ext.EnrEntrant.ui.PubRequestTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.PubRequestTab.EnrEntrantPubRequestTab;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.ui.ActionsAddon.EnrEntrantRequestActionsAddon;
import ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.ActionsAddon.RsmuEntrantRequestActionsAddon;
import ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.Pub.*;

/**
 * @author Denis Perminov
 * @since 18.06.2014
 */
@Configuration
public class EnrEntrantPubRequestTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantPubRequestTab _enrEntrantPubRequestTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantPubRequestTab.presenterExtPoint())
                .replaceAddon(uiAddon(EnrEntrantRequestActionsAddon.NAME, RsmuEntrantRequestActionsAddon.class))
                .addAction(new OnClickPrintExtractRulesAction("onClickPrintExtractRules"))
                .addAction(new OnClickPrintRequestInternalExamsAction("onClickPrintRequestInternalExams"))
                .addAction(new OnClickPrintExtractHouseRulesAction("onClickPrintExtractHouseRules"))
                .addAction(new OnClickPrintObligationProvideDocsAction("onClickPrintObligationProvideDocs"))
                .addAction(new OnClickPrintExtractHostalRulesAction("onClickPrintExtractHostalRules"))
                .addAction(new OnClickPrintRequestEqualDocsAction("onClickPrintRequestEqualDocs"))
                .addAction(new OnClickPrintAcceptPayingAction("onClickPrintAcceptPaying"))
                .addAction(new OnClickPrintPersonalCardAction("onClickPrintPersonalCard"))
                .create();
    }
}
