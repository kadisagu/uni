/* $Id:$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Denis Perminov
 * @since 17.06.2014
 */
@Configuration
public class EnrEntrantRequestExtManager extends BusinessObjectExtensionManager
{
}
