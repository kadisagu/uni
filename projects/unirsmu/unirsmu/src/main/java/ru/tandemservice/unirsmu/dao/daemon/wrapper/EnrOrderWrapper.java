/* $Id:$ */
package ru.tandemservice.unirsmu.dao.daemon.wrapper;

import org.apache.commons.lang.StringUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 25.11.2015
 */
public class EnrOrderWrapper
{
    private String _vectorId;
    private String _orderNumber;
    private Date _orderDate;
    private String _eventTypeId;
    private String _eventTypeName;

    public EnrOrderWrapper(ResultSet resultSet) throws SQLException
    {
        _vectorId = StringUtils.trimToNull(resultSet.getString(1));
        _orderNumber = StringUtils.trimToNull(resultSet.getString(2));
        _orderDate = null != resultSet.getDate(3) ? new Date(resultSet.getDate(3).getTime()) : null;
        _eventTypeId = StringUtils.trimToNull(resultSet.getString(4));
        _eventTypeName = StringUtils.trimToNull(resultSet.getString(5));
    }

    public boolean isEnrollmentOrder()
    {
        return null != _eventTypeId && "3".equals(_eventTypeId);
    }

    public boolean isRestoreOrder()
    {
        return null != _eventTypeId && "2".equals(_eventTypeId);
    }

    public String getVectorId()
    {
        return _vectorId;
    }

    public void setVectorId(String vectorId)
    {
        _vectorId = vectorId;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        _orderNumber = orderNumber;
    }

    public Date getOrderDate()
    {
        return _orderDate;
    }

    public void setOrderDate(Date orderDate)
    {
        _orderDate = orderDate;
    }

    public String getEventTypeId()
    {
        return _eventTypeId;
    }

    public void setEventTypeId(String eventTypeId)
    {
        _eventTypeId = eventTypeId;
    }

    public String getEventTypeName()
    {
        return _eventTypeName;
    }

    public void setEventTypeName(String eventTypeName)
    {
        _eventTypeName = eventTypeName;
    }
}