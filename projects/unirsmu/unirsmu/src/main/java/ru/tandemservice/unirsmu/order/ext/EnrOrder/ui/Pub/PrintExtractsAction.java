/* $Id$ */
package ru.tandemservice.unirsmu.order.ext.EnrOrder.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPubUI;
import ru.tandemservice.unirsmu.order.ext.EnrOrder.EnrOrderExtManager;
import ru.tandemservice.unirsmu.order.ext.EnrOrder.logic.IRsmuEnrOrderDao;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2014
 */
public class PrintExtractsAction extends NamedUIAction
{
    public PrintExtractsAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter iuiPresenter)
    {
        final EnrOrderPubUI presenter = (EnrOrderPubUI) iuiPresenter;
        final IRsmuEnrOrderDao dao = (IRsmuEnrOrderDao) EnrOrderManager.instance().dao();
        final byte[] document = dao.printOrderExtracts(presenter.getOrder().getId());

        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(document)
                        .fileName("EnrollmentExtractList.rtf")
                        .rtf(),
                false);
    }
}