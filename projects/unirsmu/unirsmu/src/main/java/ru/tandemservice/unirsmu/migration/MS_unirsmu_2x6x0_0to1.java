/* $Id:$ */
package ru.tandemservice.unirsmu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Denis Perminov
 * @since 09.06.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unirsmu_2x6x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // старая логика формирования подразумевает последние две цифры года в начале личного номера, при этом общая длина личного номера равна 7. Т.е. 1400001
        tool.getStatement().executeUpdate("update ENR14_ENTRANT_T set PERSONALNUMBER_P="
                + tool.createStringConcatenationSQL("'20'", "PERSONALNUMBER_P")
                + " where len(PERSONALNUMBER_P)=7 and PERSONALNUMBER_P like '14%'");
        // то, что наизменяли операторы - не трогаем
    }
}
