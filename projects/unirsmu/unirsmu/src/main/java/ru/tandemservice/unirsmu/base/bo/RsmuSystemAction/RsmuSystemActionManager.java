/* $Id$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.logic.IRsmuSystemActionDao;
import ru.tandemservice.unirsmu.base.bo.RsmuSystemAction.logic.RsmuSystemActionDao;

/**
 * @author Andrey Avetisov
 * @since 12.01.2015
 */
@Configuration
public class RsmuSystemActionManager extends BusinessObjectManager
{
    public static RsmuSystemActionManager instance()
    {
        return instance(RsmuSystemActionManager.class);
    }

    @Bean
    public IRsmuSystemActionDao dao()
    {
        return new RsmuSystemActionDao();
    }
}
