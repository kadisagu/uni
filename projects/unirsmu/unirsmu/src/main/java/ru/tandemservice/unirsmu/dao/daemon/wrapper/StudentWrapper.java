/* $Id$ */
package ru.tandemservice.unirsmu.dao.daemon.wrapper;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorDataHolder;
import ru.tandemservice.unirsmu.dao.daemon.RsmuVectorSyncDaemonDao;
import ru.tandemservice.unirsmu.entity.RsmuVectorIds;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 24.11.2015
 */
public class StudentWrapper
{
    private String _vectorId;
    private String _personalNumber;
    private String _facultyName;
    private String _directionOkso;
    private String _directionName;
    private String _qualificationName;
    private String _eduFormName;
    private String _eduDuration;
    private boolean _compensationTypeContract;

    private String _lastSemesterNumber;
    private String _lastSemesterStatusId;
    private String _lastSemesterStatusName;

    private String _groupName;

    private String _enrOrderNumber;
    private Date _enrOrderDate;
    private Date _enrOrderEnrDate;
    private Integer _enrYear;

    private String _lastOrderNumber;
    private Date _lastOrderDate;
    private String _lastOrderTypeId;
    private String _lastOrderTypeName;

    public StudentWrapper()
    {
    }

    public StudentWrapper(ResultSet resultSet) throws SQLException
    {
        _vectorId = StringUtils.trimToNull(resultSet.getString(1));
        _facultyName = StringUtils.trimToNull(resultSet.getString(3));
        _directionOkso = StringUtils.trimToNull(resultSet.getString(4));
        _directionName = StringUtils.trimToNull(resultSet.getString(5));
        _qualificationName = StringUtils.trimToNull(resultSet.getString(6));
        _eduFormName = StringUtils.trimToNull(resultSet.getString(7));
        _eduDuration = StringUtils.trimToNull(resultSet.getString(8));
        _compensationTypeContract = null != StringUtils.trimToNull(resultSet.getString(9));

        String perNumber = StringUtils.trimToNull(resultSet.getString(2));
        if (null != perNumber && perNumber.contains("}"))
        {
            int idx = perNumber.lastIndexOf(",\"");
            if (idx > 0)
            {
                int idx1 = perNumber.lastIndexOf("\"");
                String result = perNumber.substring(idx, idx1 - 1).trim();
                _personalNumber = StringUtils.trimToNull(result);
            }
        } else _personalNumber = perNumber;

        if (null == _personalNumber)
        {
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Student personal Number was auto generated for Student " + _vectorId + " because it was not filled in Vector.");
            _personalNumber = "---" + System.currentTimeMillis();
        }
    }

    public void updateSemesterData(ResultSet resultSet) throws SQLException
    {
        _lastSemesterNumber = StringUtils.trimToNull(resultSet.getString(2));
        _lastSemesterStatusId = StringUtils.trimToNull(resultSet.getString(4));
        _lastSemesterStatusName = StringUtils.trimToNull(resultSet.getString(5));
        _groupName = StringUtils.trimToNull(resultSet.getString(3));
    }

    public void updateOrderData(ResultSet resultSet) throws SQLException
    {
        _lastOrderNumber = StringUtils.trimToNull(resultSet.getString(2));
        _lastOrderDate = null != resultSet.getDate(3) ? new Date(resultSet.getDate(3).getTime()) : null;
        _lastOrderTypeId = StringUtils.trimToNull(resultSet.getString(4));
        _lastOrderTypeName = StringUtils.trimToNull(resultSet.getString(5));
    }

    public void updateEnrOrderData(EnrOrderWrapper wrapper)
    {
        _enrOrderDate = wrapper.getOrderDate();
        _enrOrderNumber = wrapper.getOrderNumber();
        if (null != _enrOrderDate) _enrYear = CoreDateUtils.getYear(_enrOrderDate);
    }

    public Student generateStudent(Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> personMap)
    {
        Person person = null != personMap.get(_vectorId) ? personMap.get(_vectorId).getX() : null;
        if (null == person)
        {
            RsmuVectorSyncDaemonDao.logEvent(Level.ERROR, "Student was not created, Person " + _vectorId + " was not found: " + this.toString());
            return null; // TODO если не нашли персону, то дальше делать нечего
        }

        Student student = new Student();
        student.setPerson(person);
        student.setPersonalNumber(_personalNumber);
        student.setPersonalNumber(RsmuVectorDataHolder.getUniquePersonalNumber(student));
        if (!_personalNumber.equals(student.getPersonalNumber()))
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Student personal number was changed to " + student.getPerNumber() + ": " + this.toString());

        student.setStudentCategory(RsmuVectorDataHolder.getStudentCategory());
        student.setCompensationType(RsmuVectorDataHolder.getCompensationType(_compensationTypeContract));

        student.setEducationOrgUnit(RsmuVectorDataHolder.getEducationOrgUnit(this, null != getQualificationName()));
        if (null == student.getEducationOrgUnit())
        {
            RsmuVectorSyncDaemonDao.logEvent(Level.ERROR, "Student was not created, EducationOrgUnit was not found: " + this.toString());
            return null; // TODO если не нашли НПП, то дальше делать нечего
        }
        else
            student.setDevelopPeriodAuto(student.getEducationOrgUnit().getDevelopPeriod());

        if (null == _lastSemesterStatusId)
        {
            _lastSemesterStatusId = "1";
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Student status is not specified, so it was changed to 'active': " + this.toString());
        }
        student.setStatus(RsmuVectorDataHolder.getStudentStatus(_lastSemesterStatusId));

        if (null == _lastSemesterNumber)
        {
            _lastSemesterNumber = "1";
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Student course is not specified, so it was changed to 1st course: " + this.toString());
        }
        student.setCourse(RsmuVectorDataHolder.getCourse(_lastSemesterNumber));

        if (null == student.getCourse())
        {
            student.setCourse(RsmuVectorDataHolder.getCourse("14"));
            RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Student course could not be calculated for the term number " + _lastSemesterNumber + ", so it was changed to 7th course: " + this.toString());
        }

        if (null == _enrYear)
        {
            if (null != _enrOrderDate) _enrYear = CoreDateUtils.getYear(_enrOrderDate);
            if (null == _enrYear)
            {
                RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Student entrance year could not be calculated, so it was changed to 1900: " + this.toString());
                _enrYear = 1900;
            }
        }
        student.setEntranceYear(_enrYear);

        if (null == student.getPrincipal())
        {
            Principal principal = new Principal();
            principal.setActive(true);
            principal.setLogin(RsmuVectorDataHolder.getUniqueLogin(person));
            principal.assignNewPassword(PersonSecurityUtil.generatePassword(8));
            principal.setAuthenticationType(RsmuVectorDataHolder.getDefaultAuthType());
            student.setPrincipal(principal);
        }

        return student;
    }

    public void updateStudent(Student student, Map<String, CoreCollectionUtils.Pair<Person, RsmuVectorIds>> personMap)
    {
        if (null == student) return;

        Person person = null != personMap.get(_vectorId) ? personMap.get(_vectorId).getX() : null;
        if (null != person) student.setPerson(person);
        if (null != _personalNumber)
        {
            student.setPersonalNumber(_personalNumber);
            student.setPersonalNumber(RsmuVectorDataHolder.getUniquePersonalNumber(student));
            if (!_personalNumber.equals(student.getPersonalNumber()))
                RsmuVectorSyncDaemonDao.logEvent(Level.WARN, "Student personal number was changed to " + student.getPerNumber() + ": " + this.toString());
        }
        student.setStudentCategory(RsmuVectorDataHolder.getStudentCategory());
        student.setCompensationType(RsmuVectorDataHolder.getCompensationType(_compensationTypeContract));

        EducationOrgUnit eduou = RsmuVectorDataHolder.getEducationOrgUnit(this, null != getQualificationName());
        if (null != eduou) student.setEducationOrgUnit(eduou);

        if (null != _lastSemesterStatusId)
            student.setStatus(RsmuVectorDataHolder.getStudentStatus(_lastSemesterStatusId));
        if (null != _lastSemesterNumber) student.setCourse(RsmuVectorDataHolder.getCourse(_lastSemesterNumber));
        if (null != _enrOrderDate) _enrYear = CoreDateUtils.getYear(_enrOrderDate);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder(_vectorId);
        if (null != _personalNumber) builder.append("\t").append(_personalNumber);
        if (null != _facultyName) builder.append("\t").append(_facultyName);
        if (null != _directionOkso) builder.append("\t").append(_directionOkso);
        if (null != _directionName) builder.append("\t").append(_directionName);
        if (null != _qualificationName) builder.append("\t").append(_qualificationName);
        if (null != _eduFormName) builder.append("\t").append(_eduFormName);
        if (null != _eduDuration) builder.append("\t").append(_eduDuration);
        if (null != _lastSemesterNumber) builder.append("\t").append(_lastSemesterNumber);
        if (null != _lastSemesterStatusId) builder.append("\t").append(_lastSemesterStatusId);
        if (null != _lastSemesterStatusName) builder.append("\t").append(_lastSemesterStatusName);
        if (null != _groupName) builder.append("\t").append(_groupName);
        if (null != _enrOrderNumber) builder.append("\t").append(_enrOrderNumber);
        if (null != _enrOrderDate)
            builder.append("\t").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_enrOrderDate));
        if (null != _lastOrderNumber) builder.append("\t").append(_lastOrderNumber);
        if (null != _lastOrderDate)
            builder.append("\t").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_lastOrderDate));
        if (null != _lastOrderTypeId) builder.append("\t").append(_lastOrderTypeId);
        if (null != _lastOrderTypeName) builder.append("\t").append(_lastOrderTypeName);
        return builder.toString();
    }

    public String getVectorId()
    {
        return _vectorId;
    }

    public void setVectorId(String vectorId)
    {
        _vectorId = vectorId;
    }

    public String getPersonalNumber()
    {
        return _personalNumber;
    }

    public void setPersonalNumber(String personalNumber)
    {
        _personalNumber = personalNumber;
    }

    public String getFacultyName()
    {
        return _facultyName;
    }

    public void setFacultyName(String facultyName)
    {
        _facultyName = facultyName;
    }

    public String getDirectionOkso()
    {
        return _directionOkso;
    }

    public void setDirectionOkso(String directionOkso)
    {
        _directionOkso = directionOkso;
    }

    public String getDirectionName()
    {
        return _directionName;
    }

    public void setDirectionName(String directionName)
    {
        _directionName = directionName;
    }

    public String getQualificationName()
    {
        return _qualificationName;
    }

    public void setQualificationName(String qualificationName)
    {
        _qualificationName = qualificationName;
    }

    public String getEduFormName()
    {
        return _eduFormName;
    }

    public void setEduFormName(String eduFormName)
    {
        _eduFormName = eduFormName;
    }

    public String getEduDuration()
    {
        return _eduDuration;
    }

    public void setEduDuration(String eduDuration)
    {
        _eduDuration = eduDuration;
    }

    public boolean isCompensationTypeContract()
    {
        return _compensationTypeContract;
    }

    public void setCompensationTypeContract(boolean compensationTypeContract)
    {
        _compensationTypeContract = compensationTypeContract;
    }

    public String getLastSemesterNumber()
    {
        return _lastSemesterNumber;
    }

    public void setLastSemesterNumber(String lastSemesterNumber)
    {
        _lastSemesterNumber = lastSemesterNumber;
    }

    public String getLastSemesterStatusId()
    {
        return _lastSemesterStatusId;
    }

    public void setLastSemesterStatusId(String lastSemesterStatusId)
    {
        _lastSemesterStatusId = lastSemesterStatusId;
    }

    public String getLastSemesterStatusName()
    {
        return _lastSemesterStatusName;
    }

    public void setLastSemesterStatusName(String lastSemesterStatusName)
    {
        _lastSemesterStatusName = lastSemesterStatusName;
    }

    public String getGroupName()
    {
        return _groupName;
    }

    public void setGroupName(String groupName)
    {
        _groupName = groupName;
    }

    public String getEnrOrderNumber()
    {
        return _enrOrderNumber;
    }

    public void setEnrOrderNumber(String enrOrderNumber)
    {
        _enrOrderNumber = enrOrderNumber;
    }

    public Date getEnrOrderDate()
    {
        return _enrOrderDate;
    }

    public void setEnrOrderDate(Date enrOrderDate)
    {
        _enrOrderDate = enrOrderDate;
    }

    public Date getEnrOrderEnrDate()
    {
        return _enrOrderEnrDate;
    }

    public void setEnrOrderEnrDate(Date enrOrderEnrDate)
    {
        _enrOrderEnrDate = enrOrderEnrDate;
    }

    public Integer getEnrYear()
    {
        return _enrYear;
    }

    public void setEnrYear(Integer enrYear)
    {
        _enrYear = enrYear;
    }

    public String getLastOrderNumber()
    {
        return _lastOrderNumber;
    }

    public void setLastOrderNumber(String lastOrderNumber)
    {
        _lastOrderNumber = lastOrderNumber;
    }

    public Date getLastOrderDate()
    {
        return _lastOrderDate;
    }

    public void setLastOrderDate(Date lastOrderDate)
    {
        _lastOrderDate = lastOrderDate;
    }

    public String getLastOrderTypeId()
    {
        return _lastOrderTypeId;
    }

    public void setLastOrderTypeId(String lastOrderTypeId)
    {
        _lastOrderTypeId = lastOrderTypeId;
    }

    public String getLastOrderTypeName()
    {
        return _lastOrderTypeName;
    }

    public void setLastOrderTypeName(String lastOrderTypeName)
    {
        _lastOrderTypeName = lastOrderTypeName;
    }
}