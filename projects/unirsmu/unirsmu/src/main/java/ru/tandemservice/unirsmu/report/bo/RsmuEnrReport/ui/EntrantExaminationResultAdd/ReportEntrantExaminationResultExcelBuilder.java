/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantExaminationResultAdd;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.PageOrientation;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.NumberAsStringComparator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unienr14.catalog.entity.EnrAbsenceNote;
import ru.tandemservice.unienr14.catalog.entity.EnrEntrantState;
import ru.tandemservice.unienr14.catalog.entity.EnrExamPassForm;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.entity.EnrExamGroup;
import ru.tandemservice.unienr14.exams.entity.EnrExamSet;
import ru.tandemservice.unienr14.exams.entity.EnrExamSetDiscipline;
import ru.tandemservice.unienr14.rating.entity.EnrExamPassDiscipline;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;

import java.io.ByteArrayOutputStream;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Avetisov
 * @since 03.02.2015
 */
public class ReportEntrantExaminationResultExcelBuilder
{
    private boolean _entrantStateActive;
    private boolean _dateFromActive;
    private boolean _dateToActive;
    private boolean _examPassDisciplineActive;
    private boolean _examGroupActive;
    private boolean _absenceNoteActive;
    private boolean _upLimitPointsRatingActive;
    private boolean _lowLimitPointsRatingActive;

    private EnrEnrollmentCampaign _enrollmentCampaign;
    private List<EnrEntrantState> _entrantStateList;
    private Date _dateFrom;
    private Date _dateTo;
    private List<DataWrapper> _examSetList;
    private List<EnrExamPassForm> _examPassFormList;
    private List<DataWrapper> _examPassDisciplineList;
    private List<EnrExamGroup> _examGroupList;
    private List<EnrAbsenceNote> _absenceNoteList;
    private long _lowLimitPointsRating;
    private long _upLimitPointsRating;
    EnrCompetitionFilterAddon _util;

    public ReportEntrantExaminationResultExcelBuilder(boolean entrantStateActive, boolean dateFromActive, boolean dateToActive,
                                                      boolean examPassDisciplineActive, boolean examGroupActive, boolean absenceNoteActive,
                                                      boolean upLimitPointsRatingActive, boolean lowLimitPointsRatingActive,
                                                      EnrEnrollmentCampaign enrollmentCampaign, List<EnrEntrantState> entrantStateList,
                                                      Date dateFrom, Date dateTo, List<DataWrapper> examSetList,
                                                      List<EnrExamPassForm> examPassFormList, List<DataWrapper> examPassDisciplineList,
                                                      List<EnrExamGroup> examGroupList, List<EnrAbsenceNote> absenceNoteList,
                                                      long lowLimitPointsRating, long upLimitPointsRating, EnrCompetitionFilterAddon util)
    {
        _entrantStateActive = entrantStateActive;
        _dateFromActive = dateFromActive;
        _dateToActive = dateToActive;
        _examPassDisciplineActive = examPassDisciplineActive;
        _examGroupActive = examGroupActive;
        _absenceNoteActive = absenceNoteActive;
        _upLimitPointsRatingActive = upLimitPointsRatingActive;
        _lowLimitPointsRatingActive = lowLimitPointsRatingActive;
        _enrollmentCampaign = enrollmentCampaign;
        _entrantStateList = entrantStateList;
        _dateFrom = dateFrom;
        _dateTo = dateTo;
        _examSetList = examSetList;
        _examPassFormList = examPassFormList;
        _examPassDisciplineList = examPassDisciplineList;
        _examGroupList = examGroupList;
        _absenceNoteList = absenceNoteList;
        _lowLimitPointsRating = lowLimitPointsRating;
        _upLimitPointsRating = upLimitPointsRating;
        _util = util;
    }

    public ByteArrayOutputStream buildReport() throws Exception
    {
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WorkbookSettings ws = new WorkbookSettings();
        ws.setEncoding("UTF-8");
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        final WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        // создаем шрифты
        WritableFont arial8 = new WritableFont(WritableFont.ARIAL, 8);
        WritableFont arial8bold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);

        // шрифт для шапки отчета
        WritableCellFormat headerFormat = new WritableCellFormat(arial8bold);
        headerFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        headerFormat.setBackground(jxl.format.Colour.GRAY_25);
        headerFormat.setAlignment(jxl.format.Alignment.CENTRE);
        headerFormat.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
        headerFormat.setWrap(true);

        // шрифт для строк отчета
        WritableCellFormat rowFormat = new WritableCellFormat(arial8);
        rowFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        rowFormat.setVerticalAlignment(jxl.format.VerticalAlignment.TOP);
        rowFormat.setWrap(true);
        try
        {
            WritableSheet sheet = workbook.createSheet("абитуриенты", workbook.getSheets().length);
            sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
            sheet.getSettings().setCopies(1);
            int excelRowNumber = 0;
            sheet.addCell(new Label(0, excelRowNumber, "Личный номер абитуриента", headerFormat));
            sheet.addCell(new Label(1, excelRowNumber, "ФИО", headerFormat));
            excelRowNumber++;


            Map<EnrEntrant, List<EnrExamPassDiscipline>> entrantDisciplineMap = getEntrantPassDisciplineList(getExamPassDisciplineList());
            List<EnrEntrant> entrantList = new ArrayList<>(entrantDisciplineMap.keySet());
            Collections.sort(entrantList, new Comparator<EnrEntrant>()
            {
                @Override
                public int compare(EnrEntrant o1, EnrEntrant o2)
                {
                    return NumberAsStringComparator.get().compare(o1.getPersonalNumber(), o2.getPersonalNumber());
                }
            });

            for (EnrEntrant entrant : entrantList)
            {
                sheet.addCell(new Label(0, excelRowNumber, entrant.getPersonalNumber(), rowFormat));
                sheet.addCell(new Label(1, excelRowNumber, entrant.getFullFio(), rowFormat));

                int examPassDisciplineColNumber = 2;
                int examPassDisciplineNumber = 1;
                for (EnrExamPassDiscipline examPassDiscipline : entrantDisciplineMap.get(entrant))
                {
                    sheet.addCell(new Label(examPassDisciplineColNumber, 0, "ВИ " + examPassDisciplineNumber, headerFormat));
                    sheet.setColumnView(examPassDisciplineColNumber, 15);
                    sheet.addCell(new Label(examPassDisciplineColNumber++, excelRowNumber, examPassDiscipline.getTitle(), rowFormat));
                    sheet.addCell(new Label(examPassDisciplineColNumber, 0, "Оценка (ВИ " + examPassDisciplineNumber + ")", headerFormat));
                    sheet.setColumnView(examPassDisciplineColNumber, 13);
                    sheet.addCell(new Label(examPassDisciplineColNumber++, excelRowNumber,
                                            examPassDiscipline.getAbsenceNote() != null ? examPassDiscipline.getAbsenceNote().getTitle() : examPassDiscipline.getMarkAsString(), rowFormat));
                    sheet.addCell(new Label(examPassDisciplineColNumber, 0, "Наименование ЭГ (ВИ" + examPassDisciplineNumber + ")", headerFormat));
                    sheet.setColumnView(examPassDisciplineColNumber, 13);
                    sheet.addCell(new Label(examPassDisciplineColNumber++, excelRowNumber, examPassDiscipline.getExamGroup() != null ? examPassDiscipline.getExamGroup().getTitle() : "", rowFormat));

                    examPassDisciplineNumber++;
                }


                excelRowNumber++;
            }

            for (int i = 0; i < 2; i++)
            {
                // задаем ширину колонки
                sheet.setColumnView(i, 20);
            }
        }
        catch (RowsExceededException e)
        {
            throw new ApplicationException("Невозможно сформировать отчет, так как объем итоговых данных превышает допустимый предел в 65536 строк.");
        }
        workbook.write();
        workbook.close();

        return out;
    }

    private Map<EnrEntrant, List<EnrExamPassDiscipline>> getEntrantPassDisciplineList(List<EnrExamPassDiscipline> examPassDisciplineList)
    {

        Map<EnrEntrant, List<EnrExamPassDiscipline>> entrantDisciplineMap = new HashMap<>();

        for (EnrExamPassDiscipline discipline : examPassDisciplineList)
        {
            if (null == entrantDisciplineMap.get(discipline.getEntrant()))
            {
                List<EnrExamPassDiscipline> entrantDisciplineList = new ArrayList<>();
                entrantDisciplineList.add(discipline);
                entrantDisciplineMap.put(discipline.getEntrant(), entrantDisciplineList);
            }
            else if (!entrantDisciplineMap.get(discipline.getEntrant()).contains(discipline))
            {
                entrantDisciplineMap.get(discipline.getEntrant()).add(discipline);
            }
        }
        return entrantDisciplineMap;
    }


    private List<EnrExamPassDiscipline> getExamPassDisciplineList()
    {
        DQLSelectBuilder examPassDql = new DQLSelectBuilder().fromEntity(EnrExamPassDiscipline.class, "d").column(property("d"))
                .joinEntity("d", DQLJoinType.inner, EnrRatingItem.class, "r", eq(property("r", EnrRatingItem.entrant().id()), property("d", EnrExamPassDiscipline.entrant().id())))
                .joinPath(DQLJoinType.inner, EnrRatingItem.competition().fromAlias("r"), "c")
                .joinPath(DQLJoinType.inner, EnrCompetition.examSetVariant().examSet().fromAlias("c"), "s")
                .joinEntity("s", DQLJoinType.inner, EnrExamSetDiscipline.class, "sd",
                            and(
                                    eq(property("sd", EnrExamSetDiscipline.examSet().id()), property("s", EnrExamSet.P_ID)),
                                    eq(property("sd", EnrExamSetDiscipline.L_DISCIPLINE), property("d", EnrExamPassDiscipline.L_DISCIPLINE))
                            )


                )


                //Приемная кампания
                .where(eqValue(property("d", EnrExamPassDiscipline.entrant().enrollmentCampaign()), _enrollmentCampaign));

        //Формирующее подр., вид приема
        _util.applyFilters(examPassDql, "c");

        //Состояние абитуриента
        if (_entrantStateActive)
        {
            examPassDql.where(in(property("d", EnrExamPassDiscipline.entrant().state()), _entrantStateList));
        }
        //Дата с
        if (_dateFromActive)
        {
            examPassDql.where(ge(property("d", EnrExamPassDiscipline.examGroup().examGroupSet().beginDate()), valueDate(_dateFrom)));
        }
        //Дата по
        if (_dateToActive)
        {
            examPassDql.where(le(property("d", EnrExamPassDiscipline.examGroup().examGroupSet().endDate()), valueDate(_dateTo)));
        }
        //Набор ВИ, форма сдачи
        examPassDql.where(in(property("s", EnrExamSet.id()), getExamSet(_examSetList)))
                .where(in(property("d", EnrExamPassDiscipline.passForm()), _examPassFormList));
        //Вступительное испытание
        if (_examPassDisciplineActive)
        {
            examPassDql.where(in(property("d", EnrExamPassDiscipline.discipline()), getCampaignDiscipline(_examPassDisciplineList)));
            examPassDql.where(in(property("d", EnrExamPassDiscipline.passForm()), getExamPassForm(_examPassDisciplineList)));
        }
        //Экзаменационная группа
        if (_examGroupActive)
        {
            examPassDql.where(in(property("d", EnrExamPassDiscipline.examGroup()), _examGroupList));
        }
        //Отметка
        if (_absenceNoteActive)
        {
            examPassDql.where(in(property("d", EnrExamPassDiscipline.absenceNote()), _absenceNoteList));
        }
        //Нижняя граница набранных баллов по всем ВИ
        if (_lowLimitPointsRatingActive)
        {
            examPassDql.where(ge(property("d", EnrExamPassDiscipline.P_MARK_AS_LONG), value(_lowLimitPointsRating * 1000)));
        }
        //Верхняя граница набранных баллов по всем ВИ
        if (_upLimitPointsRatingActive)
        {
            examPassDql.where(le(property("d", EnrExamPassDiscipline.P_MARK_AS_LONG), value(_upLimitPointsRating * 1000)));
        }
        examPassDql.order(property("s", EnrExamSet.key()));
        examPassDql.order(property("sd", EnrExamSetDiscipline.source().number()));
        examPassDql.order(property("d", EnrExamPassDiscipline.discipline().discipline().title()));
        return DataAccessServices.dao().getList(examPassDql);
    }

    private List<EnrCampaignDiscipline> getCampaignDiscipline(List<DataWrapper> wrappedExamPassDisciplineList)
    {
        List<EnrCampaignDiscipline> passDiscipline = new ArrayList<>(_examPassDisciplineList.size());
        for (DataWrapper wrapper : wrappedExamPassDisciplineList)
        {
            passDiscipline.add((EnrCampaignDiscipline) wrapper.get(RsmuEnrReportEntrantExaminationResultAddUI.PASS_DISCIPLINE));
        }
        return passDiscipline;
    }

    private List<EnrExamPassForm> getExamPassForm(List<DataWrapper> wrappedExamPassDisciplineList)
    {
        List<EnrExamPassForm> passFormList = new ArrayList<>(_examPassDisciplineList.size());
        for (DataWrapper wrapper : wrappedExamPassDisciplineList)
        {
            passFormList.add((EnrExamPassForm) wrapper.get(RsmuEnrReportEntrantExaminationResultAddUI.PASS_FORM));
        }
        return passFormList;
    }

    private List<EnrExamSet> getExamSet(List<DataWrapper> wrappedExamSetList)
    {
        List<EnrExamSet> examSetList = new ArrayList<>(_examSetList.size());
        for (DataWrapper wrapper : wrappedExamSetList)
        {
            EnrExamSet examSet = wrapper.get(RsmuEnrReportEntrantExaminationResultAddUI.EXAM_SET);
            examSetList.add(examSet);
        }
        return examSetList;
    }
}
