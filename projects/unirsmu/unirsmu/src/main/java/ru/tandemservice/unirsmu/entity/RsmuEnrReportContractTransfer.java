package ru.tandemservice.unirsmu.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unirsmu.entity.gen.*;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.ContractTransferAdd.RsmuEnrReportContractTransferAdd;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/** @see ru.tandemservice.unirsmu.entity.gen.RsmuEnrReportContractTransferGen */
public class RsmuEnrReportContractTransfer extends RsmuEnrReportContractTransferGen implements IEnrReport
{
    public static final String REPORT_KEY = "rsmuEnr14ReportContractTransfer";

    private static List<String> properties = Arrays.asList(
            P_ENR_ORDER,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET
    );



    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc()
        {
            @Override
            public String getReportKey()
            {
                return REPORT_KEY;
            }

            @Override
            public Class<? extends IEnrReport> getReportClass()
            {
                return RsmuEnrReportContractTransfer.class;
            }

            @Override
            public List<String> getPropertyList()
            {
                return properties;
            }

            @Override
            public Class<? extends BusinessComponentManager> getAddFormComponent()
            {
                return RsmuEnrReportContractTransferAdd.class;
            }

            @Override
            public String getPubTitle()
            {
                return "Отчет «Акт передачи договоров зачисленных абитуриентов (РНИМУ)»";
            }

            @Override
            public String getListTitle()
            {
                return "Список отчетов «Акт передачи договоров зачисленных абитуриентов (РНИМУ)»";
            }
        };
    }

    @Override
    public IEnrStorableReportDesc getDesc()
    {
        return getDescription();
    }

    @Override
    public String getPeriodTitle()
    {
        return "";
    }

    public Date getDateFrom()
    {
        return null;
    }

    public Date getDateTo()
    {
        return null;
    }
}