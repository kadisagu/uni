/* $Id$ */
package ru.tandemservice.unirsmu.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportEntrantRegistrationJournal;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportPersonalFileTransferList;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportRatingList;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantDataExportEoisAdd.RsmuEnrReportEntrantDataExportEoisAdd;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantExaminationResultAdd.RsmuEnrReportEntrantExaminationResultAdd;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantNotificationDeliveryAdd.RsmuEnrReportEntrantNotificationDeliveryAdd;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileCoverAdd.RsmuEnrReportPersonalFileCoverAdd;

/**
 * @author Nikolay Fedorovskih
 * @since 02.07.2014
 */
@Configuration
public class EnrReportBaseExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
                .add(RsmuEnrReportEntrantRegistrationJournal.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RsmuEnrReportEntrantRegistrationJournal.REPORT_KEY))
                .add(RsmuEnrReportRatingList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RsmuEnrReportRatingList.REPORT_KEY))
                .add(RsmuEnrReportPersonalFileCoverAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(RsmuEnrReportPersonalFileCoverAdd.REPORT_KEY, RsmuEnrReportPersonalFileCoverAdd.class))
                .add(RsmuEnrReportEntrantNotificationDeliveryAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(RsmuEnrReportEntrantNotificationDeliveryAdd.REPORT_KEY, RsmuEnrReportEntrantNotificationDeliveryAdd.class))
                .add(RsmuEnrReportPersonalFileTransferList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RsmuEnrReportPersonalFileTransferList.REPORT_KEY))
                .add(RsmuEnrReportContractTransfer.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(RsmuEnrReportContractTransfer.REPORT_KEY))
                .add(RsmuEnrReportEntrantExaminationResultAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(RsmuEnrReportEntrantExaminationResultAdd.REPORT_KEY, RsmuEnrReportEntrantExaminationResultAdd.class))
                .add(RsmuEnrReportEntrantDataExportEoisAdd.REPORT_KEY, EnrReportBaseManager.getReportDefinition(RsmuEnrReportEntrantDataExportEoisAdd.REPORT_KEY, RsmuEnrReportEntrantDataExportEoisAdd.class))
                .create();
    }

    @Bean
    public ItemListExtension<IEnrStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(_enrReportBaseManager.storableReportDescExtPoint())
                .add(RsmuEnrReportEntrantRegistrationJournal.REPORT_KEY, RsmuEnrReportEntrantRegistrationJournal.getDescription())
                .add(RsmuEnrReportRatingList.REPORT_KEY, RsmuEnrReportRatingList.getDescription())
                .add(RsmuEnrReportPersonalFileTransferList.REPORT_KEY, RsmuEnrReportPersonalFileTransferList.getDescription())
                .add(RsmuEnrReportContractTransfer.REPORT_KEY, RsmuEnrReportContractTransfer.getDescription())
                .create();
    }
}