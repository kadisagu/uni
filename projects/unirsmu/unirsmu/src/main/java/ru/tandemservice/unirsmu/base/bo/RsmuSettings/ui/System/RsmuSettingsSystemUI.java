/* $Id:$ */
package ru.tandemservice.unirsmu.base.bo.RsmuSettings.ui.System;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unirsmu.base.bo.RsmuSettings.RsmuSettingsManager;

import java.io.File;

/**
 * @author Dmitry Seleznev
 * @since 18.11.2015
 */
public class RsmuSettingsSystemUI extends UIPresenter
{
    private boolean _vectorDbUse;
    private String _vectorDbHost;
    private String _vectorDbPort;
    private String _vectorDbName;
    private String _vectorDbLogin;
    private String _vectorDbPassword1;
    private String _vectorDbPassword2;

    public boolean isVectorDbSettingsDisabled()
    {
        return !_vectorDbUse;
    }

    @Override
    public void onComponentRefresh()
    {
        String vectorDbUseStr = ApplicationRuntime.getProperty(RsmuSettingsManager.VECTOR_DB_USE);
        _vectorDbUse = (null != vectorDbUseStr && Boolean.parseBoolean(vectorDbUseStr));
        _vectorDbHost = ApplicationRuntime.getProperty(RsmuSettingsManager.VECTOR_DB_HOST);
        _vectorDbPort = ApplicationRuntime.getProperty(RsmuSettingsManager.VECTOR_DB_PORT);
        _vectorDbName = ApplicationRuntime.getProperty(RsmuSettingsManager.VECTOR_DB_NAME);
        _vectorDbLogin = ApplicationRuntime.getProperty(RsmuSettingsManager.VECTOR_DB_LOGIN);
        _vectorDbPassword1 = ApplicationRuntime.getProperty(RsmuSettingsManager.VECTOR_DB_PASSWORD);
        _vectorDbPassword2 = _vectorDbPassword1;
    }

    // Listeners

    public void onClickApply() throws Exception
    {
        PropertiesConfiguration config = new PropertiesConfiguration(new File(ApplicationRuntime.getAppConfigPath(), "app.properties"));

        _vectorDbHost = StringUtils.trimToEmpty(_vectorDbHost);
        _vectorDbPort = StringUtils.trimToEmpty(_vectorDbPort);
        _vectorDbName = StringUtils.trimToEmpty(_vectorDbName);
        _vectorDbLogin = StringUtils.trimToEmpty(_vectorDbLogin);
        _vectorDbPassword1 = StringUtils.trimToEmpty(_vectorDbPassword1);
        _vectorDbPassword2 = StringUtils.trimToEmpty(_vectorDbPassword2);

        if (null != _vectorDbPassword1 && null != _vectorDbPassword2 && !_vectorDbPassword1.equals(_vectorDbPassword2))
        {
            ContextLocal.getErrorCollector().add("Пароли, указанные для подключения к БД ИС \"Вектор\", не совпадают.", "vectorDbPassword1", "vectorDbPassword2");
            return;
        }

        config.setProperty(RsmuSettingsManager.VECTOR_DB_USE, _vectorDbUse ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
        config.setProperty(RsmuSettingsManager.VECTOR_DB_HOST, _vectorDbHost);
        config.setProperty(RsmuSettingsManager.VECTOR_DB_PORT, _vectorDbPort);
        config.setProperty(RsmuSettingsManager.VECTOR_DB_NAME, _vectorDbName);
        config.setProperty(RsmuSettingsManager.VECTOR_DB_LOGIN, _vectorDbLogin);
        config.setProperty(RsmuSettingsManager.VECTOR_DB_PASSWORD, _vectorDbPassword1);

        ApplicationRuntime.getProperties().setProperty(RsmuSettingsManager.VECTOR_DB_USE, _vectorDbUse ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
        ApplicationRuntime.getProperties().setProperty(RsmuSettingsManager.VECTOR_DB_HOST, _vectorDbHost);
        ApplicationRuntime.getProperties().setProperty(RsmuSettingsManager.VECTOR_DB_PORT, _vectorDbPort);
        ApplicationRuntime.getProperties().setProperty(RsmuSettingsManager.VECTOR_DB_NAME, _vectorDbName);
        ApplicationRuntime.getProperties().setProperty(RsmuSettingsManager.VECTOR_DB_LOGIN, _vectorDbLogin);
        ApplicationRuntime.getProperties().setProperty(RsmuSettingsManager.VECTOR_DB_PASSWORD, _vectorDbPassword1);

        config.getLayout().setBlancLinesBefore(RsmuSettingsManager.VECTOR_DB_USE, 1);
        config.getLayout().setComment(RsmuSettingsManager.VECTOR_DB_USE, "vector db connection settings");

        config.save();
    }

    // Getters & setters

    public boolean isVectorDbUse()
    {
        return _vectorDbUse;
    }

    public void setVectorDbUse(boolean vectorDbUse)
    {
        _vectorDbUse = vectorDbUse;
    }

    public String getVectorDbHost()
    {
        return _vectorDbHost;
    }

    public void setVectorDbHost(String vectorDbHost)
    {
        _vectorDbHost = vectorDbHost;
    }

    public String getVectorDbPort()
    {
        return _vectorDbPort;
    }

    public void setVectorDbPort(String vectorDbPort)
    {
        _vectorDbPort = vectorDbPort;
    }

    public String getVectorDbName()
    {
        return _vectorDbName;
    }

    public void setVectorDbName(String vectorDbName)
    {
        _vectorDbName = vectorDbName;
    }

    public String getVectorDbLogin()
    {
        return _vectorDbLogin;
    }

    public void setVectorDbLogin(String vectorDbLogin)
    {
        _vectorDbLogin = vectorDbLogin;
    }

    public String getVectorDbPassword1()
    {
        return _vectorDbPassword1;
    }

    public void setVectorDbPassword1(String vectorDbPassword1)
    {
        _vectorDbPassword1 = vectorDbPassword1;
    }

    public String getVectorDbPassword2()
    {
        return _vectorDbPassword2;
    }

    public void setVectorDbPassword2(String vectorDbPassword2)
    {
        _vectorDbPassword2 = vectorDbPassword2;
    }
}