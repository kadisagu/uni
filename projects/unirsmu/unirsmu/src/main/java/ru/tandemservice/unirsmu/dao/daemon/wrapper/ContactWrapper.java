/* $Id:$ */
package ru.tandemservice.unirsmu.dao.daemon.wrapper;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.person.base.entity.PersonContactData;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmitry Seleznev
 * @since 24.11.2015
 */
public class ContactWrapper
{
    private String _personId;
    private String _homePhone;
    private String _workPhone;
    private String _mobilePhone;
    private String _contactPhone;
    private String _email;

    public ContactWrapper(String personId)
    {
        _personId = personId;
    }

    public ContactWrapper(ResultSet resultSet) throws SQLException
    {
        _personId = StringUtils.trimToNull(resultSet.getString(1));
    }

    public void parsePhone(ResultSet resultSet) throws SQLException
    {
        String phoneType = StringUtils.trimToNull(resultSet.getString(2));
        String phoneNumber = StringUtils.trimToNull(resultSet.getString(3));
        if (null == phoneType || null == phoneNumber) return;

        StringBuilder filteredPhoneNumber = new StringBuilder();
        for (int i = 0; i < phoneNumber.length(); i++)
        {
            if (StringUtils.isNumeric(phoneNumber.substring(i, i + 1)))
                filteredPhoneNumber.append(phoneNumber.substring(i, i + 1));
        }

        if (phoneNumber.startsWith("+")) filteredPhoneNumber.insert(0, "+");

        switch (phoneType)
        {
            case "1":
                _homePhone = filteredPhoneNumber.toString();
                break;
            case "2":
                _workPhone = filteredPhoneNumber.toString();
                break;
            case "3":
                _mobilePhone = filteredPhoneNumber.toString();
                break;
            case "4":
                _contactPhone = filteredPhoneNumber.toString();
                break;
            default:
                return;
        }
    }

    public void parseEmail(ResultSet resultSet) throws SQLException
    {
        _email = StringUtils.trimToNull(resultSet.getString(2));
    }

    public void updatePersonContactData(PersonContactData contact)
    {
        if (null == contact) return;
        contact.setPhoneFact(_homePhone);
        contact.setPhoneDefault(_contactPhone);
        contact.setPhoneWork(_workPhone);
        contact.setPhoneMobile(_mobilePhone);
        contact.setEmail(_email);
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder(_personId);
        return builder.toString();
    }

    public String getPersonId()
    {
        return _personId;
    }

    public void setPersonId(String personId)
    {
        _personId = personId;
    }

    public String getHomePhone()
    {
        return _homePhone;
    }

    public void setHomePhone(String homePhone)
    {
        _homePhone = homePhone;
    }

    public String getWorkPhone()
    {
        return _workPhone;
    }

    public void setWorkPhone(String workPhone)
    {
        _workPhone = workPhone;
    }

    public String getMobilePhone()
    {
        return _mobilePhone;
    }

    public void setMobilePhone(String mobilePhone)
    {
        _mobilePhone = mobilePhone;
    }

    public String getContactPhone()
    {
        return _contactPhone;
    }

    public void setContactPhone(String contactPhone)
    {
        _contactPhone = contactPhone;
    }

    public String getEmail()
    {
        return _email;
    }

    public void setEmail(String email)
    {
        _email = email;
    }
}