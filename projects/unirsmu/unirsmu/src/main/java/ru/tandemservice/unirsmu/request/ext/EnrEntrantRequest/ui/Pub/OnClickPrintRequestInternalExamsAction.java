/* $Id:$ */
package ru.tandemservice.unirsmu.request.ext.EnrEntrantRequest.ui.Pub;

import org.hibernate.Session;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrCampaignDiscipline;
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes;

import java.util.Date;
import java.util.List;

/**
 * @author Denis Perminov
 * @since 18.06.2014
 */
public class OnClickPrintRequestInternalExamsAction extends NamedUIAction
{

    public OnClickPrintRequestInternalExamsAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        String fio;
        String fio_G;
        Long currentEnttrantRequestId = presenter.getListenerParameterAsLong();
        if (null == currentEnttrantRequestId)
            return;

        EnrEntrantRequest currentEntrantRequest = DataAccessServices.dao().getNotNull(currentEnttrantRequestId);
        if (null == currentEntrantRequest)
            return;
        Date registrationDate = currentEntrantRequest.getRegDate();
        IdentityCard identityCard = currentEntrantRequest.getIdentityCard();
        EnrEntrant enrEntrant = currentEntrantRequest.getEntrant();
        String personalNumber = enrEntrant.getPersonalNumber();
        Session session = presenter.getSupport().getSession();
        StringBuilder internalExams = new StringBuilder();

        List<EnrRequestedCompetition> erc = new DQLSelectBuilder()
                .fromEntity(EnrRequestedCompetition.class, "erc")
                .column(DQLExpressions.property("erc"))
                .where(DQLExpressions.eq(DQLExpressions.property("erc", EnrRequestedCompetition.request()), DQLExpressions.value(currentEntrantRequest)))
                .order(DQLExpressions.property("erc", EnrRequestedCompetition.priority()))
                .createStatement(session).list();
        List<EnrCampaignDiscipline> eceef = new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExamForm.class, "eceef")
                .column(DQLExpressions.property("eceef", EnrChosenEntranceExamForm.chosenEntranceExam().discipline()))
                .where(DQLExpressions.in(DQLExpressions.property("eceef", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition()), erc))
                .where(DQLExpressions.eq(DQLExpressions.property("eceef", EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().request()), DQLExpressions.value(currentEntrantRequest)))
                .where(DQLExpressions.eq(DQLExpressions.property("eceef", EnrChosenEntranceExamForm.passForm().internal()), DQLExpressions.value(Boolean.TRUE)))
                .createStatement(session).list();

        if (!eceef.isEmpty()) {
            for(EnrCampaignDiscipline discipline : eceef)
                internalExams.append(internalExams.indexOf(discipline.getTitle())<0 ? (internalExams.length()>internalExams.lastIndexOf(",")+1 ? ", " : "") + discipline.getTitle() : "");

            EnrScriptItem template = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.REQUEST_ENTRANT_REQUEST_INTERNAL_EXAMS);
            if (null != template)
            {
                final RtfDocument document = new RtfReader().read(template.getCurrentTemplate());
                RtfInjectModifier im = new RtfInjectModifier();
                fio = PersonManager.instance().declinationDao().getDeclinationFIO(identityCard, GrammaCase.NOMINATIVE);
                fio_G = PersonManager.instance().declinationDao().getDeclinationFIO(identityCard, GrammaCase.GENITIVE);
                im.put("FIO", fio.toUpperCase());
                im.put("entrantNumber", personalNumber);
                im.put("disciplinasInternal", internalExams.toString().toLowerCase());
                im.put("dateReq", DateFormatter.STRING_MONTHS_AND_QUOTES.format(registrationDate));
                im.modify(document);
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Заявление о допуске к ВВИ " + fio + ".rtf").document(document), false);
            }
        }
    }
}