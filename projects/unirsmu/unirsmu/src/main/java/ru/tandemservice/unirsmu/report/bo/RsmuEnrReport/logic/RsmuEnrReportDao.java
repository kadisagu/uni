/* $Id$ */
package ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentCommission;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEntrantStateCodes;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.report.bo.EnrReport.EnrReportManager;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unirsmu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportContractTransfer;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportEntrantRegistrationJournal;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportPersonalFileTransferList;
import ru.tandemservice.unirsmu.entity.RsmuEnrReportRatingList;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.RsmuEnrReportManager;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.ContractTransferAdd.RsmuEnrReportContractTransferAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantDataExportEoisAdd.RsmuEnrReportEntrantDataExportEoisAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.EntrantRegistrationJournalAdd.RsmuEnrReportEntrantRegistrationJournalAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileCoverAdd.RsmuEnrReportPersonalFileCoverAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.PersonalFileTransferAdd.RsmuEnrReportPersonalFileTransferAddUI;
import ru.tandemservice.unirsmu.report.bo.RsmuEnrReport.ui.RatingListAdd.RsmuEnrReportRatingListAddUI;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author rsizonenko
 * @since 09.06.2014
 */
public class RsmuEnrReportDao extends UniBaseDao implements IRsmuEnrReportDao
{
    @Override
    public Long createEntrantRegistrationJournalReport(RsmuEnrReportEntrantRegistrationJournalAddUI model)
    {
        RsmuEnrReportEntrantRegistrationJournal report = new RsmuEnrReportEntrantRegistrationJournal();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, RsmuEnrReportEntrantRegistrationJournal.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, RsmuEnrReportEntrantRegistrationJournal.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, RsmuEnrReportEntrantRegistrationJournal.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, RsmuEnrReportEntrantRegistrationJournal.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, RsmuEnrReportEntrantRegistrationJournal.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RsmuEnrReportEntrantRegistrationJournal.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RsmuEnrReportEntrantRegistrationJournal.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RsmuEnrReportEntrantRegistrationJournal.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RsmuEnrReportEntrantRegistrationJournal.P_PROGRAM_SET, "title");

        if (model.isFilterByEnrCommission())
            report.setEnrollmentCommission(UniStringUtils.join(model.getEnrollmentCommissionList(), EnrEnrollmentCommission.title().s(), "; "));

        report.setGroupByEnrCommission(model.isGroupByEnrCommission() ? "Да" : "Нет");

        DatabaseFile content = new DatabaseFile();
        content.setContent(EnrReportManager.instance().entrantRegistrationJournalDao().buildReport(model));
        content.setFilename("RsmuEnrReportEntrantRegistrationJournal.xls");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }

    @Override
    public Long createEntrantRatingListReport(RsmuEnrReportRatingListAddUI model)
    {
        RsmuEnrReportRatingList report = new RsmuEnrReportRatingList();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateFrom());
        report.setDateTo(model.getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, RsmuEnrReportRatingList.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, RsmuEnrReportRatingList.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, RsmuEnrReportRatingList.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, RsmuEnrReportRatingList.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, RsmuEnrReportRatingList.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RsmuEnrReportRatingList.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RsmuEnrReportRatingList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RsmuEnrReportRatingList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RsmuEnrReportRatingList.P_PROGRAM_SET, "title");
        if (model.isParallelActive())
        {
            report.setParallel(model.getParallel().getTitle());
        }

        DatabaseFile content = new DatabaseFile();
        content.setContent(RsmuEnrReportManager.instance().ratingListDao().buildReport(model, EnrScriptItemCodes.RSMU_REPORT_RATING_LIST));
        content.setFilename("RsmuEnrReportRatingList.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    @Override
    public byte[] createMassPersonalFileReport(RsmuEnrReportPersonalFileCoverAddUI reportParams)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "e")
                .column("e.id")
                .where(eqValue(property("e", EnrEntrantRequest.entrant().enrollmentCampaign()), reportParams.getEnrollmentCampaign()))
                .where(betweenDays(EnrEntrantRequest.regDate().fromAlias("e"), reportParams.getDateFrom(), reportParams.getDateTo()))
                .where(eqValue(property("e", EnrEntrantRequest.entrant().state().code()), EnrEntrantStateCodes.ENROLLED))
                .joinPath(DQLJoinType.inner, EnrEntrantRequest.entrant().person().identityCard().fromAlias("e"), "card")
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME));

        if (reportParams.isPersonalNumberActive() && StringUtils.isNotEmpty(reportParams.getPersonalNumber()))
        {
            dql.where(likeUpper(property("e", EnrEntrantRequest.entrant().personalNumber()), value(CoreStringUtils.escapeLike(reportParams.getPersonalNumber()))));
        }

        if (reportParams.isEnrollmentOrderActive() && reportParams.getEnrollmentOrderList() != null && !reportParams.getEnrollmentOrderList().isEmpty())
        {
            dql.where(exists(EnrEnrollmentExtract.class,
                             EnrEnrollmentExtract.entity().request().s(), property("e"),
                             EnrEnrollmentExtract.paragraph().order().s(), reportParams.getEnrollmentOrderList()));
        }

        Iterator<Long> iterator = createStatement(dql).<Long>list().iterator();

        if (!iterator.hasNext())
            throw new ApplicationException("Данные для построения отчета отсутствуют.");

        final IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        final IRtfControl pageBreak = elementFactory.createRtfControl(IRtfData.PAGE);
        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.COMMON_ENTRANT_LETTER_BOX);
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
        final List<IRtfElement> mainElementList = mainDoc.getElementList();
        while (iterator.hasNext())
        {
            mainElementList.add(pageBreak);
            scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
            RtfDocument nextDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
            mainElementList.addAll(nextDoc.getElementList());
        }

        return RtfUtil.toByteArray(mainDoc);
    }

    @Override
    public Long createPersonalFileTransferReport(RsmuEnrReportPersonalFileTransferAddUI model)
    {
        RsmuEnrReportPersonalFileTransferList report = new RsmuEnrReportPersonalFileTransferList();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setEnrOrgUnit(model.getEduGroupOrgUnit().getGroupOrgUnit().getTitle());
        List<String> orderTitles = new ArrayList<>();
        for (EnrOrder order : model.getEnrOrderList())
        {
            orderTitles.add(order.getTitle());
        }
        report.setEnrOrder(CommonBaseStringUtil.joinWithSeparator("; ", orderTitles));

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();


        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RsmuEnrReportPersonalFileTransferList.P_FORMATIVE_ORG_UNIT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RsmuEnrReportPersonalFileTransferList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RsmuEnrReportPersonalFileTransferList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RsmuEnrReportPersonalFileTransferList.P_PROGRAM_SET, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, RsmuEnrReportPersonalFileTransferList.P_COMPETITION_TYPE, "title");


        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.RSMU_REPORT_PERSONAL_FILE_TRANSFER_LIST);
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));


        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(mainDoc));
        content.setFilename("RsmuEnrPersonalFileReport.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }

    @Override
    public Long createContractTransferReport(RsmuEnrReportContractTransferAddUI model)
    {
        RsmuEnrReportContractTransfer report = new RsmuEnrReportContractTransfer();
        report.setFormingDate(new Date());
        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        List<String> orderTitles = new ArrayList<>();
        for (EnrOrder order : model.getEnrOrderList())
        {
            orderTitles.add(order.getTitle());
        }
        report.setEnrOrder(CommonBaseStringUtil.joinWithSeparator("; ", orderTitles));

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();


        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, RsmuEnrReportPersonalFileTransferList.P_FORMATIVE_ORG_UNIT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, RsmuEnrReportPersonalFileTransferList.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, RsmuEnrReportPersonalFileTransferList.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, RsmuEnrReportPersonalFileTransferList.P_PROGRAM_SET, "title");


        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.RSMU_REPORT_ENTRANT_CONTRACT_TRANSFER);
        final RtfReader reader = new RtfReader();
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                                                                                                IScriptExecutor.TEMPLATE_VARIABLE, scriptItem.getCurrentTemplate(),
                                                                                                "model", model);
        final RtfDocument mainDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));


        DatabaseFile content = new DatabaseFile();
        content.setContent(RtfUtil.toByteArray(mainDoc));
        content.setFilename("RsmuEnrContractTransferReport.rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);
        return report.getId();
    }


    @Override
    public void createEntrantDataExportEoisReport(RsmuEnrReportEntrantDataExportEoisAddUI model)
    {
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao()
                .getScriptResult(DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.RSMU_REPORT_ENTRANT_DATA_EOIS_EXPORT),
                                 "template", new byte[]{}, // Шаблон не используем, делаем заглушку
                                 "enrollmentCampaign", model.getEnrollmentCampaign(),
                                 "dateTo", model.getDateTo(),
                                 "dateFrom", model.getDateFrom(),
                                 "dateToActive", model.isDateToActive(),
                                 "dateFromActive", model.isDateFromActive()
                );

        byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

        if (null == content)
        {
            throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
        }
        if (null == filename || !filename.contains("."))
        {
            throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
        }
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).document(content), false);
    }
}
