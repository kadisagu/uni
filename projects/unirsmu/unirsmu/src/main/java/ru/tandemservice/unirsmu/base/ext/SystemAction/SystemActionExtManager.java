/* $Id$ */
package ru.tandemservice.unirsmu.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unirsmu.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Andrey Avetisov
 * @since 12.01.2015
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("unienr14_importEnrPhoto", new SystemActionDefinition("unienr14", "importEnrPhoto", "onClickImportEnrPhoto", SystemActionPubExt.RSMU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unirsmu_syncWithVectorDatabaseForMsr", new SystemActionDefinition("unirsmu", "syncWithVectorDatabaseForMsr", "onClickSyncWithVectorDatabaseForMsr", SystemActionPubExt.RSMU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unirsmu_importStudentsFromExcel", new SystemActionDefinition("unirsmu", "importStudentsFromExcel", "onClickImportStudentsFromExcel", SystemActionPubExt.RSMU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unirsmu_deleteStudents", new SystemActionDefinition("unirsmu", "deleteStudents", "onClickDeleteStudents", SystemActionPubExt.RSMU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unirsmu_deleteOtherInvalidExtracts", new SystemActionDefinition("unirsmu", "deleteOtherInvalidExtracts", "onClickDeleteOtherInvalidExtracts", SystemActionPubExt.RSMU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unirsmu_optimizeOtherOrders", new SystemActionDefinition("unirsmu", "optimizeOtherOrders", "onClickOptimizeOtherOrders", SystemActionPubExt.RSMU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("unirsmu_deleteFakeStudents", new SystemActionDefinition("unirsmu", "deleteFakeStudents", "onClickDeleteFakeStudents", SystemActionPubExt.RSMU_SYSTEM_ACTION_PUB_ADDON_NAME))
        .create();
    }
}