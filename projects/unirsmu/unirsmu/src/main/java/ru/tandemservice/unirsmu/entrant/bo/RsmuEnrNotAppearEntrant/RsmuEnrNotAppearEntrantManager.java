/* $Id$ */
package ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic.IRsmuEnrNotAppearEntrantDao;
import ru.tandemservice.unirsmu.entrant.bo.RsmuEnrNotAppearEntrant.logic.RsmuEnrNotAppearEntrantDao;

/**
 * @author Andrey Avetisov
 * @since 09.02.2015
 */
@Configuration
public class RsmuEnrNotAppearEntrantManager extends BusinessObjectManager
{
    public static RsmuEnrNotAppearEntrantManager instance() { return instance(RsmuEnrNotAppearEntrantManager.class); }
    @Bean
    public IRsmuEnrNotAppearEntrantDao dao()
    {
        return new RsmuEnrNotAppearEntrantDao();
    }
}
