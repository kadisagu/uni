/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.employee_box;

import org.tandemframework.core.runtime.IRuntimeExtension;

import ru.tandemservice.employee_box.dao.IEmployeeBoxOrgstructDao;

/**
 * @author dseleznev
 * Created on: 24.11.2009
 */
public class EmployeeBoxOrgstructRuntimeExtension implements IRuntimeExtension
{
    private IEmployeeBoxOrgstructDao _dao;

    public void setDao(IEmployeeBoxOrgstructDao dao)
    {
        this._dao = dao;
    }

    public void init(Object object)
    {
        _dao.createOrgstructHierarhy();
        _dao.createOrgUnitToKindRelations();
        _dao.createOrgUnitsStructure();
        _dao.customizeOrgUnitTypes();
    }

    public void destroy()
    {
    }
}