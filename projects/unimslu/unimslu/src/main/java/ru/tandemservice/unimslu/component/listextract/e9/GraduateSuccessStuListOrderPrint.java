/* $Id$ */
package ru.tandemservice.unimslu.component.listextract.e9;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.employee.Student;


/**
 * @author Ekaterina Zvereva
 * @since 13.01.2017
 */
public class GraduateSuccessStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    private static final IStudentListParagraphPrintFormatter STUDENT_LIST_FORMATTER = new IStudentListParagraphPrintFormatter()
    {
        @Override
        public String formatSingleStudent(Student student, int extractNumber)
        {
            return "\\par " + extractNumber + ".  " + student.getPerson().getFullFio()
                    + " (" + (student.getCompensationType().isBudget()? "за счет средств бюджета" : "на основе договоров с оплатой обучения") + ")";
        }
    };

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        ListStudentExtract firstExtract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId());
        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, firstExtract);
        IListParagraphPrintFormCreator<AbstractStudentExtract> printForm = CommonListOrderPrint.getListParagraphPrintForm(EntityRuntime.getMeta(firstExtract).getName() + "_extractPrint");
        printForm.modifyOrderTemplate(injectModifier, order, firstExtract);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        CommonListOrderPrint.injectParagraphs(document, order, STUDENT_LIST_FORMATTER, firstExtract);

        return document;
    }

}