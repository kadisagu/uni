/* $Id$ */
package ru.tandemservice.univgma.order.ext.EnrOrder.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderDao;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.univgma.catalog.entity.codes.EnrScriptItemCodes;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 04.08.2015
 */
public class VgmaEnrOrderDao extends EnrOrderDao implements IVgmaEnrOrderDao
{
    @Override
    public byte[] printEnrOrderExtracts(Long enrOrderId)
    {
        // Список выписок приказа. Сортируем по ФИО
        List<Long> extractIds = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e")
                .column(property("e", EnrEnrollmentExtract.id()))
                .where(eqValue(property("e", EnrEnrollmentExtract.paragraph().order()), enrOrderId))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fromAlias("e"), "card")
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME))
                .createStatement(getSession()).list();

        if (extractIds.isEmpty())
            throw new ApplicationException("Нет выписок для печати.");

        final Iterator<Long> iterator = extractIds.iterator();
        // Скрипт печати выписки и шаблон
        final IScriptItem scriptItem = getByCode(EnrScriptItem.class, EnrScriptItemCodes.VGMA_ENROLLMENT_EXTRACT);

        final IRtfControl pageBreak = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        // Первая выписка будет изначальным документов, в который будут вставляться все остальные
        final RtfReader reader = new RtfReader();
        List<IRtfElement> mainElementList = null;
        RtfDocument mainDoc = null;
        RtfDocument extractDoc = null;
        do
        {
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
            extractDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
            if (extractDoc == null)
                throw new NullPointerException();

            if (mainDoc == null)
            {
                mainDoc = extractDoc.getClone();
                mainElementList = mainDoc.getElementList();
            }
            else
                mainElementList.addAll(extractDoc.getElementList());
            if (iterator.hasNext())
                mainElementList.add(pageBreak);


        }
        while (iterator.hasNext());

        return RtfUtil.toByteArray(mainDoc);

    }
}
