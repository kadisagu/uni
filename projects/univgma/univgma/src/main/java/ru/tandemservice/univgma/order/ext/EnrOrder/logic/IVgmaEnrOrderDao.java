/* $Id$ */
package ru.tandemservice.univgma.order.ext.EnrOrder.logic;

import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.IEnrOrderDao;


/**
 * @author Ekaterina Zvereva
 * @since 04.08.2015
 */
public interface IVgmaEnrOrderDao extends IEnrOrderDao
{
    byte[] printEnrOrderExtracts(Long enrOrderId);
}
