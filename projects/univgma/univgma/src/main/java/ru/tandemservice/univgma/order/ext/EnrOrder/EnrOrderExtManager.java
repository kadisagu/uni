/* $Id$ */
package ru.tandemservice.univgma.order.ext.EnrOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.univgma.order.ext.EnrOrder.logic.IVgmaEnrOrderDao;
import ru.tandemservice.univgma.order.ext.EnrOrder.logic.VgmaEnrOrderDao;

/**
 * @author Ekaterina Zvereva
 * @since 04.08.2015
 */
@Configuration
public class EnrOrderExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public IVgmaEnrOrderDao dao()
    {
        return new VgmaEnrOrderDao();
    }
}
