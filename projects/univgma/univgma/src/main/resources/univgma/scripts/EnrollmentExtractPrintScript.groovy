package univgma.scripts

/**
 * @author Ekaterina Zvereva
 * @since 04.08.2015
 */

import org.hibernate.Session
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA


return new EnrollmentExtractPrint(
        session: session,
        template: template,
        extract: session.get(EnrEnrollmentExtract.class, object)
).print()

class EnrollmentExtractPrint
{
    Session session
    byte[] template
    EnrEnrollmentExtract extract

    def print()
    {
        def paragraph = extract.paragraph as EnrEnrollmentParagraph
        def order = paragraph.order as EnrOrder
        def document = new RtfReader().read(template)
        def mark = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())?.totalMarkAsDouble

        def im = new RtfInjectModifier()
                .put('FIO', extract.requestedCompetition.request.entrant.fullFio)
                .put('orderNumber', order.number? order.number : '              ')
                .put('orderDate', order.commitDate ? order.commitDate.format('dd.MM.yyyy') : '              ')
                .put('orderBase', order.orderBasicText? order.orderBasicText : '')
                .put('mark', mark ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(mark) : '')
                .put('programSubject', paragraph.programSubject.titleWithCode)
                .put('enrCompetitionType', paragraph.competitionType.title)
                .put('paragraphNumber', paragraph.number as String)
                .put('speciality', extract.requestedCompetition.competition.programSetOrgUnit.programSet.title)

        if (extract.entity instanceof EnrRequestedCompetitionTA)
        {
            im.put('targetAdmissionType', " (" + ((EnrRequestedCompetitionTA)extract.entity).targetAdmissionKind.title + ")")
        }
        else im.put('targetAdmissionType', '')

        im.modify(document)

        return [document: RtfUtil.toByteArray(document), fileName: 'EnrollmentExtract.rtf']

    }

}
