/* $Id$ */
package ru.tandemservice.unipgups.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;
import ru.tandemservice.unipgups.entity.PgupsEmployeePostExt;

/**
 * @author Ekaterina Zvereva
 * @since 28.01.2015
 */
public class EmployeePostAddEditExtUI extends UIAddon
{
    public EmployeePostAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    final EmployeePostAddEditUI _presenter = (EmployeePostAddEditUI) getPresenterConfig().getPresenter();
    private PgupsEmployeePostExt _pgupsEmployeePostExt;

    public PgupsEmployeePostExt getPgupsEmployeePostExt()
    {
        return _pgupsEmployeePostExt;
    }

    public void setPgupsEmployeePostExt(PgupsEmployeePostExt pgupsEmployeePostExt)
    {
        _pgupsEmployeePostExt = pgupsEmployeePostExt;
    }

    public boolean isCanEdit()
    {
        if (_presenter.isAddForm())
            return true;

      return CoreServices.securityService().check(SecurityRuntime.getInstance().getCommonSecurityObject(),
                                                         ContextLocal.getUserContext().getPrincipalContext(), "editPgupsPostEmployee_employeePost");
    }

    @Override
    public void onComponentRefresh()
    {
        _pgupsEmployeePostExt = DataAccessServices.dao().get(PgupsEmployeePostExt.class, PgupsEmployeePostExt.employeePost(), _presenter.getEmployeePost());
        if (_pgupsEmployeePostExt == null)
        {
            _pgupsEmployeePostExt = new PgupsEmployeePostExt(_presenter.getEmployeePost());
        }
    }

    public boolean isDisabledEditOrgUnit()
    {
        return !isCanEdit();
    }
}