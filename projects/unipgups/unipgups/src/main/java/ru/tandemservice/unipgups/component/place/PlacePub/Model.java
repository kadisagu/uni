/* $Id$ */
package ru.tandemservice.unipgups.component.place.PlacePub;

import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;


/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.PlacePub.Model
{
    private UniPlacesPlaceExt _placeExt;

    public UniPlacesPlaceExt getPlaceExt()
    {
        return _placeExt;
    }

    public void setPlaceExt(UniPlacesPlaceExt placeExt)
    {
        _placeExt = placeExt;
    }
}