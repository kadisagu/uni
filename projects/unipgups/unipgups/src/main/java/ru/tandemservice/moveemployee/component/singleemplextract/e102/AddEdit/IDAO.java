/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102.AddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.ICommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public interface IDAO extends ICommonSingleEmployeeExtractAddEditDAO<ProlongationContractExtract, Model>
{
}