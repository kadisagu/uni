/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubModel;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public class Model extends SingleEmployeeExtractPubModel<AdditionalPaymentExtract>
{
    EmployeeBonus _employeeBonus;

    public EmployeeBonus getEmployeeBonus()
    {
        return _employeeBonus;
    }

    public void setEmployeeBonus(EmployeeBonus employeeBonus)
    {
        _employeeBonus = employeeBonus;
    }
}