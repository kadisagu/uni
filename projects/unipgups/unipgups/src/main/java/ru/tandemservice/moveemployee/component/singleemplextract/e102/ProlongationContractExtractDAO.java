/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

import java.util.Date;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public class ProlongationContractExtractDAO extends UniBaseDao implements IExtractComponentDao<ProlongationContractExtract>
{

    @Override
    public void doCommit(ProlongationContractExtract extract, Map parameters)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        EmployeeLabourContract contract = extract.getLabourContract();
        extract.setOldEndDate(contract.getEndDate());
        contract.setEndDate(extract.getNewEndDate());
        update(contract);

        ContractCollateralAgreement agreement = null;

        for (ContractCollateralAgreement collateralAgreement : UniempDaoFacade.getUniempDAO().getContractCollateralAgreementsList(contract))
            if (collateralAgreement.getNumber().equals(extract.getContractAddAgreementNumber()) && collateralAgreement.getDate().equals(extract.getContractAddAgreementDate()))
            {
                agreement = collateralAgreement;
                break;
            }

        if (agreement == null)
        {
            agreement = new ContractCollateralAgreement();

            agreement.setContract(contract);
            agreement.setDate(extract.getContractAddAgreementDate());
            agreement.setNumber(extract.getContractAddAgreementNumber());
            agreement.setDescription("Изменение срока действия трудового договора");

            extract.setCreateCollateralAgreement(agreement);

            save(agreement);
        }

        // изменяем дату увольнения у сотрудника согластно приказу
        Date newDismissalDate = extract.getNewEndDate();
        EmployeePost employeePost = extract.getEntity();
        //запоминаем старую дату увольнения на случай отката приказа
        extract.setOldDismissalDate(employeePost.getDismissalDate());
        //и обновляем дату увольенния на новую
        employeePost.setDismissalDate(newDismissalDate);

        update(employeePost);
        update(extract);
    }


    @Override
    public void doRollback(ProlongationContractExtract extract, Map parameters)
    {
        if (extract.getCreateCollateralAgreement() != null)
        {
            delete(extract.getCreateCollateralAgreement());
            extract.setCreateCollateralAgreement(null);
        }

        extract.getLabourContract().setEndDate(extract.getOldEndDate());
        extract.setOldEndDate(null);

        //откатываем назад предыдущую дату увольнения сотрудника
        Date oldDismissalDate = extract.getOldDismissalDate();
        if(oldDismissalDate != null){
            EmployeePost employeePost = extract.getEntity();
            employeePost.setDismissalDate(oldDismissalDate);
            update(employeePost);
        }

        update(extract.getLabourContract());
        update(extract);

    }
}