/* $Id$ */
package ru.tandemservice.unipgups.component.place.PlaceList;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.component.place.PlaceList.Model;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlaceCondition;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesPlacePurpose;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.PlaceList.DAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        DynamicListDataSource<PlaceListWrapper> dataSource = ((ru.tandemservice.unipgups.component.place.PlaceList.Model)model).getPlaceDataSource();
        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniplacesPlace.class, "place")
                .addAdditionalAlias(UniPlacesPlaceExt.class, "placeExt")
                .addAdditionalAlias(UniplacesPlace.class, "place");

        final DQLSelectBuilder builder = orderRegistry.buildDQLSelectBuilder()
        .joinPath(DQLJoinType.inner, UniplacesPlace.floor().fromAlias("place"), "floor")
        .joinPath(DQLJoinType.inner, UniplacesFloor.unit().fromAlias("floor"), "unit")
        .joinPath(DQLJoinType.inner, UniplacesUnit.building().fromAlias("unit"), "building")
        .joinEntity("place", DQLJoinType.left, UniPlacesPlaceExt.class, "placeExt", eq(property("place.id"), property("placeExt", UniPlacesPlaceExt.place().id())))
                .column(property("place"))
                .column(property("placeExt"));

        List<UniplacesPlacePurpose> purposes = model.getPurposes();
        if (purposes != null && !purposes.isEmpty())
        {
            builder.where(in(property("place", UniplacesPlace.purpose()), purposes));
        }

        List<UniplacesClassroomType> classroomTypes = model.getClassroomTypes();
        if(classroomTypes != null && !classroomTypes.isEmpty())
        {
            builder.where(in(property("place", UniplacesPlace.classroomType()), classroomTypes));
        }

        final UniplacesPlaceCondition condition = model.getCondition();
        if (condition != null)
        {
            builder.where(eq(property("place", UniplacesPlace.condition()), value(condition)));
        }

        if (model.getPlace() != null)
            builder.where(eq(property("place.id"), value(model.getPlace())));
        else if (model.getSettings().get("pgupsPlace") != null)
            builder.where(eq(property("placeExt", UniPlacesPlaceExt.pgupsPlace()), commonValue(model.getSettings().get("pgupsPlace"))));
        else if (model.getFloor() != null)
            builder.where(eq(property("place", UniplacesPlace.floor()), value(model.getFloor())));
        else if (model.getUnit() != null)
            builder.where(eq(property("floor", UniplacesFloor.unit()), value(model.getUnit())));
        else if (model.getBuilding() != null)
            builder.where(eq(property("unit", UniplacesUnit.building()), value(model.getBuilding())));

        if (model.getOrgUnit() != null)
            builder.where(eq(property("place", UniplacesPlace.responsibleOrgUnit()), value(model.getOrgUnit())));
        if (model.getAreaMax() != null)
            builder.where(le(property("place", UniplacesPlace.area()), value(model.getAreaMax())));
        if (model.getAreaMin() != null)
            builder.where(ge(property("place", UniplacesPlace.area()), value(model.getAreaMin())));
        if (model.getLengthMax()!= null)
            builder.where(le(property("place", UniplacesPlace.length()), value(model.getLengthMax())));
        if (model.getLengthMin()!= null)
            builder.where(ge(property("place", UniplacesPlace.length()), value(model.getLengthMin())));
        if (model.getWidthMax()!= null)
            builder.where(le(property("place", UniplacesPlace.width()), value(model.getWidthMax())));
        if (model.getWidthMin()!= null)
            builder.where(ge(property("place", UniplacesPlace.width()), value(model.getWidthMin())));
        if (model.getHeightMax()!= null)
            builder.where(le(property("place", UniplacesPlace.height()), value(model.getHeightMax())));
        if (model.getHeightMin()!= null)
            builder.where(ge(property("place", UniplacesPlace.height()), value(model.getHeightMin())));



        //Ответственный сотрудник
        Object responsiblePerson = model.getSettings().get("responsiblePerson");
        if (responsiblePerson != null)
            builder.where(eq(property("ext", UniPlacesPlaceExt.responsiblePerson()), commonValue(responsiblePerson)));

        //Возможность аренды
        Object avaliableRent = model.getSettings().get("availableRent");
        if (avaliableRent != null)
            builder.where(eq(property("placeExt", UniPlacesPlaceExt.mayRented()), value(((IEntity)avaliableRent).getId().equals(ru.tandemservice.unipgups.component.place.PlaceList.Model.AVALIABLE_RENT_TRUE))));

        orderRegistry.applyOrderWithLeftJoins(builder, dataSource.getEntityOrder());



        final Number count = getCount(builder);
        dataSource.setTotalSize((null == count ? 0 : count.longValue()));

        long startRow = dataSource.isPrintEnabled() ? dataSource.getPrintStartRow() : dataSource.getStartRow();
        long countRow = dataSource.isPrintEnabled() ? dataSource.getPrintCountRow() : dataSource.getCountRow();
        List<Object[]> list = getList(builder, (int) startRow, (int) countRow);
        List<PlaceListWrapper> resultList= new ArrayList<>(list.size());
        List<Long> idsList = new ArrayList<>(list.size());

        for(Object[] row : list)
        {
            UniplacesPlace place = (UniplacesPlace) row[0];
            UniPlacesPlaceExt placeExt = (UniPlacesPlaceExt) row[1];
            PlaceListWrapper wrapper = new PlaceListWrapper(place.getId(), place.getTitle());
            wrapper.setPlace(place);
            wrapper.setPlaceExt(placeExt);
            idsList.add(place.getId());
            resultList.add(wrapper);
        }
        dataSource.createPage(resultList);

//        flushClearAndRefresh();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        //модель выбора ответственного лица
        ((ru.tandemservice.unipgups.component.place.PlaceList.Model) model).setResponsiblePersonModel(new BaseSingleSelectModel(EmployeePost.P_FULL_TITLE)
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return get(EmployeePost.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e");
                builder.column("e");

                // фильтр по фио + должность + подр. + тип подр.
                if (StringUtils.isNotEmpty(filter))
                {
                    IDQLExpression expression = DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()),
                                                                    DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.postRelation().postBoundedWithQGandQL().post().title().s()),
                                                                                        DQLFunctions.concat(DQLExpressions.property("e", EmployeePost.orgUnit().shortTitle().s()),
                                                                                                            DQLExpressions.property("e", EmployeePost.orgUnit().orgUnitType().title().s())
                                                                                        )));

                    builder.where(DQLExpressions.like(DQLFunctions.upper(expression), DQLExpressions.value(CoreStringUtils.escapeLike(filter).replace(" ", "%"))));
                }
                if (model.getSettings().get("orgUnit") != null)
                    builder.where(eq(property("e", EmployeePost.orgUnit()), commonValue(model.getSettings().get("orgUnit"))));

                // только активные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.postStatus().active().s()), DQLExpressions.value(Boolean.TRUE)));

                // не архивные сотрудники
                builder.where(DQLExpressions.eq(DQLExpressions.property("e", EmployeePost.employee().archival().s()), DQLExpressions.value(Boolean.FALSE)));

                // сортируем по фио
                builder.order(DQLExpressions.property("e", EmployeePost.employee().person().identityCard().fullFio().s()));

                // показываем не больше 50
//                Number count = builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
//                IDQLStatement statement = builder.createStatement(new DQLExecutionContext(getSession()));
//                statement.setMaxResults(50);

                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });
        ((ru.tandemservice.unipgups.component.place.PlaceList.Model) model).setPgupsPlaceList(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniPgupsPlace> findValues(String filter)
            {
                    if (null == model.getFloor())
                        return ListResult.getEmpty();
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniPgupsPlace.class, "pgupsPlace")
                            .column(property("pgupsPlace"));
                    builder.where(eq(property("pgupsPlace", UniPgupsPlace.floor()), value(model.getFloor())));
                    if (StringUtils.isNotBlank(filter))
                        builder.where(likeUpper(property("pgupsPlace", UniPgupsPlace.title()), value(CoreStringUtils.escapeLike(filter))));
                    builder.order(property("pgupsPlace", UniPgupsPlace.title()));
                    
//                    int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                    return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });

        //аудитории
        model.setPlaceList(new FullCheckSelectModel()
        {
            @Override
            public ListResult<UniplacesPlace> findValues(String filter)
            {
                if (null == model.getSettings().get("pgupsPlace"))
                    return ListResult.getEmpty();
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "pl")
                        .column(property("pl"))
                        .joinEntity("pl", DQLJoinType.inner, UniPlacesPlaceExt.class, "ext", eq(property("pl.id"), property("ext", UniPlacesPlaceExt.place())));
                builder.where(eq(property("ext", UniPlacesPlaceExt.pgupsPlace()), commonValue(model.getSettings().get("pgupsPlace"))));
                if (StringUtils.isNotBlank(filter))
                    builder.where(likeUpper(property("pl", UniplacesPlace.title()), value(CoreStringUtils.escapeLike(filter))));
                builder.order(property("pl", UniplacesPlace.title()));

//                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                return new ListResult<>(builder.createStatement(getSession()).list());
            }

        });

    }
}