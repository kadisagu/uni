/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unipgups.entity.report.PgupsEntrantsTADataReport;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class EntrantsTADataReportListDSHandler extends DefaultSearchDataSourceHandler
{

    public EntrantsTADataReportListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long enrollmentCampaignId = context.get("enrollmentCampaign");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PgupsEntrantsTADataReport.class, "r");
        if(null != enrollmentCampaignId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("r", PgupsEntrantsTADataReport.enrollmentCampaign().id()), DQLExpressions.value(enrollmentCampaignId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
//        return ListOutputBuilder.get(input, Collections.emptyList()).build();
    }
}
