/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.EntrantsTADataReportVO;
import ru.tandemservice.unipgups.entity.report.PgupsEntrantsTADataReport;

/**
 * @author nvankov
 * @since 7/17/13
 */
public interface IEntrantsTADataReportDAO
{
    PgupsEntrantsTADataReport createReport(EntrantsTADataReportVO reportVO);

    EnrollmentDirection getSelectedEnrollmentDirection(EntrantsTADataReportVO reportVO);
}
