/* $Id:$ */
package ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.PubForeignTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditAccomodation.PgupsEnrEntrantEditAccomodation;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditContractOrg.PgupsEnrEntrantEditContractOrg;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditMigrationCard.PgupsEnrEntrantEditMigrationCard;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditResidencePermit.PgupsEnrEntrantEditResidencePermit;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditTranscription.PgupsEnrEntrantEditTranscription;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditVisa.PgupsEnrEntrantEditVisa;

/**
 * @author Denis Perminov
 * @since 24.07.2014
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id"),
        @Bind(key = "selectedDataTab")
})
public class PgupsEnrEntrantPubForeignTabUI extends UIPresenter
{
    private EnrEntrant _enrEntrant = new EnrEntrant();
    private String _selectedDataTab;
    private PgupsEnrForeignEntrant _foreignEntrant;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(CommonDAO.DAO_CACHE.get().getNotNull(EnrEntrant.class, _enrEntrant.getId()));
        _foreignEntrant = CommonDAO.DAO_CACHE.get().get(PgupsEnrForeignEntrant.class, PgupsEnrForeignEntrant.L_ENTRANT, _enrEntrant);
        if (null == _foreignEntrant)
        {
            _foreignEntrant = new PgupsEnrForeignEntrant();
            _foreignEntrant.setEntrant(_enrEntrant);
        }
    }

    public void onClickEditForeignEntrantContractOrg()
    {
        _uiActivation.asRegionDialog(PgupsEnrEntrantEditContractOrg.class)
                .parameter(IUIPresenter.PUBLISHER_ID, _enrEntrant.getId())
                .activate();
    }

    public void onClickEditForeignEntrantTranscription()
    {
        _uiActivation.asRegionDialog(PgupsEnrEntrantEditTranscription.class)
                .parameter(IUIPresenter.PUBLISHER_ID, _enrEntrant.getId())
                .activate();
    }

    public void onClickEditForeignEntrantVisa()
    {
        _uiActivation.asRegionDialog(PgupsEnrEntrantEditVisa.class)
                .parameter(IUIPresenter.PUBLISHER_ID, _enrEntrant.getId())
                .activate();
    }

    public void onClickEditForeignEntrantAccommodation()
    {
        _uiActivation.asRegionDialog(PgupsEnrEntrantEditAccomodation.class)
                .parameter(IUIPresenter.PUBLISHER_ID, _enrEntrant.getId())
                .activate();
    }

    public void onClickEditForeignEntrantMigrationCard()
    {
        _uiActivation.asRegionDialog(PgupsEnrEntrantEditMigrationCard.class)
                .parameter(IUIPresenter.PUBLISHER_ID, _enrEntrant.getId())
                .activate();
    }

    public void onClickEditForeignEntrantResidencePermit()
    {
        _uiActivation.asRegionDialog(PgupsEnrEntrantEditResidencePermit.class)
                .parameter(IUIPresenter.PUBLISHER_ID, _enrEntrant.getId())
                .activate();
    }

    public boolean isAccessible() { return _enrEntrant.isArchival(); }

    public EnrEntrant getEntrant()
    {
        return _enrEntrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this._enrEntrant = entrant;
    }

    public String getSelectedDataTab()
    {
        return _selectedDataTab;
    }

    public void setSelectedDataTab(String selectedDataTab)
    {
        this._selectedDataTab = selectedDataTab;
    }

    public PgupsEnrForeignEntrant getForeignEntrant()
    {
        return _foreignEntrant;
    }

    public void setForeignEntrant(PgupsEnrForeignEntrant foreignEntrant)
    {
        this._foreignEntrant = foreignEntrant;
    }
}
