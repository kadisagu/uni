package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности помещения (ПГУПС)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniPlacesPlaceExtGen extends EntityBase
 implements INaturalIdentifiable<UniPlacesPlaceExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.UniPlacesPlaceExt";
    public static final String ENTITY_NAME = "uniPlacesPlaceExt";
    public static final int VERSION_HASH = 516761123;
    private static IEntityMeta ENTITY_META;

    public static final String L_PLACE = "place";
    public static final String L_RESPONSIBLE_PERSON = "responsiblePerson";
    public static final String L_PGUPS_PLACE = "pgupsPlace";
    public static final String P_MAY_RENTED = "mayRented";

    private UniplacesPlace _place;     // Помещение
    private EmployeePost _responsiblePerson;     // Ответственное лицо
    private UniPgupsPlace _pgupsPlace;     // Ссылка на помещение (ПГУПС)
    private boolean _mayRented = false;     // Признак возможности сдачи в аренду

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniplacesPlace getPlace()
    {
        return _place;
    }

    /**
     * @param place Помещение. Свойство не может быть null и должно быть уникальным.
     */
    public void setPlace(UniplacesPlace place)
    {
        dirty(_place, place);
        _place = place;
    }

    /**
     * @return Ответственное лицо.
     */
    public EmployeePost getResponsiblePerson()
    {
        return _responsiblePerson;
    }

    /**
     * @param responsiblePerson Ответственное лицо.
     */
    public void setResponsiblePerson(EmployeePost responsiblePerson)
    {
        dirty(_responsiblePerson, responsiblePerson);
        _responsiblePerson = responsiblePerson;
    }

    /**
     * @return Ссылка на помещение (ПГУПС). Свойство не может быть null.
     */
    @NotNull
    public UniPgupsPlace getPgupsPlace()
    {
        return _pgupsPlace;
    }

    /**
     * @param pgupsPlace Ссылка на помещение (ПГУПС). Свойство не может быть null.
     */
    public void setPgupsPlace(UniPgupsPlace pgupsPlace)
    {
        dirty(_pgupsPlace, pgupsPlace);
        _pgupsPlace = pgupsPlace;
    }

    /**
     * @return Признак возможности сдачи в аренду. Свойство не может быть null.
     */
    @NotNull
    public boolean isMayRented()
    {
        return _mayRented;
    }

    /**
     * @param mayRented Признак возможности сдачи в аренду. Свойство не может быть null.
     */
    public void setMayRented(boolean mayRented)
    {
        dirty(_mayRented, mayRented);
        _mayRented = mayRented;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniPlacesPlaceExtGen)
        {
            if (withNaturalIdProperties)
            {
                setPlace(((UniPlacesPlaceExt)another).getPlace());
            }
            setResponsiblePerson(((UniPlacesPlaceExt)another).getResponsiblePerson());
            setPgupsPlace(((UniPlacesPlaceExt)another).getPgupsPlace());
            setMayRented(((UniPlacesPlaceExt)another).isMayRented());
        }
    }

    public INaturalId<UniPlacesPlaceExtGen> getNaturalId()
    {
        return new NaturalId(getPlace());
    }

    public static class NaturalId extends NaturalIdBase<UniPlacesPlaceExtGen>
    {
        private static final String PROXY_NAME = "UniPlacesPlaceExtNaturalProxy";

        private Long _place;

        public NaturalId()
        {}

        public NaturalId(UniplacesPlace place)
        {
            _place = ((IEntity) place).getId();
        }

        public Long getPlace()
        {
            return _place;
        }

        public void setPlace(Long place)
        {
            _place = place;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UniPlacesPlaceExtGen.NaturalId) ) return false;

            UniPlacesPlaceExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getPlace(), that.getPlace()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getPlace());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getPlace());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniPlacesPlaceExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniPlacesPlaceExt.class;
        }

        public T newInstance()
        {
            return (T) new UniPlacesPlaceExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "place":
                    return obj.getPlace();
                case "responsiblePerson":
                    return obj.getResponsiblePerson();
                case "pgupsPlace":
                    return obj.getPgupsPlace();
                case "mayRented":
                    return obj.isMayRented();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "place":
                    obj.setPlace((UniplacesPlace) value);
                    return;
                case "responsiblePerson":
                    obj.setResponsiblePerson((EmployeePost) value);
                    return;
                case "pgupsPlace":
                    obj.setPgupsPlace((UniPgupsPlace) value);
                    return;
                case "mayRented":
                    obj.setMayRented((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "place":
                        return true;
                case "responsiblePerson":
                        return true;
                case "pgupsPlace":
                        return true;
                case "mayRented":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "place":
                    return true;
                case "responsiblePerson":
                    return true;
                case "pgupsPlace":
                    return true;
                case "mayRented":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "place":
                    return UniplacesPlace.class;
                case "responsiblePerson":
                    return EmployeePost.class;
                case "pgupsPlace":
                    return UniPgupsPlace.class;
                case "mayRented":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniPlacesPlaceExt> _dslPath = new Path<UniPlacesPlaceExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniPlacesPlaceExt");
    }
            

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#getPlace()
     */
    public static UniplacesPlace.Path<UniplacesPlace> place()
    {
        return _dslPath.place();
    }

    /**
     * @return Ответственное лицо.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#getResponsiblePerson()
     */
    public static EmployeePost.Path<EmployeePost> responsiblePerson()
    {
        return _dslPath.responsiblePerson();
    }

    /**
     * @return Ссылка на помещение (ПГУПС). Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#getPgupsPlace()
     */
    public static UniPgupsPlace.Path<UniPgupsPlace> pgupsPlace()
    {
        return _dslPath.pgupsPlace();
    }

    /**
     * @return Признак возможности сдачи в аренду. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#isMayRented()
     */
    public static PropertyPath<Boolean> mayRented()
    {
        return _dslPath.mayRented();
    }

    public static class Path<E extends UniPlacesPlaceExt> extends EntityPath<E>
    {
        private UniplacesPlace.Path<UniplacesPlace> _place;
        private EmployeePost.Path<EmployeePost> _responsiblePerson;
        private UniPgupsPlace.Path<UniPgupsPlace> _pgupsPlace;
        private PropertyPath<Boolean> _mayRented;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Помещение. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#getPlace()
     */
        public UniplacesPlace.Path<UniplacesPlace> place()
        {
            if(_place == null )
                _place = new UniplacesPlace.Path<UniplacesPlace>(L_PLACE, this);
            return _place;
        }

    /**
     * @return Ответственное лицо.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#getResponsiblePerson()
     */
        public EmployeePost.Path<EmployeePost> responsiblePerson()
        {
            if(_responsiblePerson == null )
                _responsiblePerson = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_PERSON, this);
            return _responsiblePerson;
        }

    /**
     * @return Ссылка на помещение (ПГУПС). Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#getPgupsPlace()
     */
        public UniPgupsPlace.Path<UniPgupsPlace> pgupsPlace()
        {
            if(_pgupsPlace == null )
                _pgupsPlace = new UniPgupsPlace.Path<UniPgupsPlace>(L_PGUPS_PLACE, this);
            return _pgupsPlace;
        }

    /**
     * @return Признак возможности сдачи в аренду. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPlacesPlaceExt#isMayRented()
     */
        public PropertyPath<Boolean> mayRented()
        {
            if(_mayRented == null )
                _mayRented = new PropertyPath<Boolean>(UniPlacesPlaceExtGen.P_MAY_RENTED, this);
            return _mayRented;
        }

        public Class getEntityClass()
        {
            return UniPlacesPlaceExt.class;
        }

        public String getEntityName()
        {
            return "uniPlacesPlaceExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
