/* $Id:$ */
package ru.tandemservice.unipgups.entrant.ext.EnrEntrant.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.PgupsEnrEntrantManager;
import ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.PubForeignTab.PgupsEnrEntrantPubForeignTab;
import ru.tandemservice.unienr14.entrant.bo.EnrEntrant.ui.Pub.EnrEntrantPub;

/**
 * @author Denis Perminov
 * @since 24.07.2014
 */
@Configuration
public class EnrEntrantPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EnrEntrantPub _enrEntrantPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_enrEntrantPub.presenterExtPoint())
                .create();
    }

    @Bean
    public TabPanelExtension entrantPubTabPanelExtension()
    {
        return tabPanelExtensionBuilder(_enrEntrantPub.entrantPubTabPanelExtPoint())
                .addTab(componentTab("foreignTab", PgupsEnrEntrantPubForeignTab.class).permissionKey("pgupsEnr14EntrantForeignPubTabView"))
                .create();
    }
}
