/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.*;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
@Configuration
public class PgupsEcReportManager extends BusinessObjectManager
{
    public static PgupsEcReportManager instance()
    {
        return instance(PgupsEcReportManager.class);
    }

    @Bean
    public IPgupsEcReportDAO dao()
    {
        return new PgupsEcReportDAO();
    }

    @Bean
    public IEntrantsTADataReportDAO entrantsTADataReportDAO()
    {
        return new EntrantsTADataReportDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler entrantsTADataReportListDSHandler()
    {
        return new EntrantsTADataReportListDSHandler(getName());
    }

}