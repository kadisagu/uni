/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubController;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public class Controller extends SingleEmployeeExtractPubController<AdditionalPaymentExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(component.getModel());
    }
}