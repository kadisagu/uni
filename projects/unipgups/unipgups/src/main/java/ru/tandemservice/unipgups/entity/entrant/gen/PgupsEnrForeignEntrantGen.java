package ru.tandemservice.unipgups.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unipgups.entity.catalog.PgupsViewVisa;
import ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные иностранного абитуриента (2014)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsEnrForeignEntrantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant";
    public static final String ENTITY_NAME = "pgupsEnrForeignEntrant";
    public static final int VERSION_HASH = 1620588389;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";
    public static final String P_TRANSCRIPTION_LAST_NAME = "transcriptionLastName";
    public static final String P_TRANSCRIPTION_FIRST_NAME = "transcriptionFirstName";
    public static final String P_TRANSCRIPTION_MIDDLE_NAME = "transcriptionMiddleName";
    public static final String L_VIEW_VISA = "viewVisa";
    public static final String P_PURPOSE_VISIT = "purposeVisit";
    public static final String P_VISA_NUMBER = "visaNumber";
    public static final String P_AUTHORITY_ISSUANCE_VISA = "authorityIssuanceVisa";
    public static final String P_VISA_DATE_FROM = "visaDateFrom";
    public static final String P_VISA_DATE_TO = "visaDateTo";
    public static final String P_RESIDENCE_COUNTRY_ADDRESS = "residenceCountryAddress";
    public static final String P_MIGRATION_REG_ADDRESS = "migrationRegAddress";
    public static final String P_MIGRATION_REG_DATE_FROM = "migrationRegDateFrom";
    public static final String P_MIGRATION_REG_DATE_TO = "migrationRegDateTo";
    public static final String P_ACTUAL_ADDRESS = "actualAddress";
    public static final String P_CONTACT_PHONE_NUMBER = "contactPhoneNumber";
    public static final String P_MIGRATION_CARD_NUMBER = "migrationCardNumber";
    public static final String P_MIGRATION_CARD_DATE = "migrationCardDate";
    public static final String P_MIGRATION_CARD_BORDER_POINT = "migrationCardBorderPoint";
    public static final String P_CERTIFICATE_NUMBER = "certificateNumber";
    public static final String P_CERTIFICATE_DATE = "certificateDate";
    public static final String P_AUTHORITY_ISSUANCE_CERTIFICATE = "authorityIssuanceCertificate";
    public static final String P_CERTIFICATE_DATE_FROM = "certificateDateFrom";
    public static final String P_CERTIFICATE_DATE_TO = "certificateDateTo";

    private EnrEntrant _entrant;     // Абитуриент
    private String _contractNumber;     // Номер договора
    private Date _contractDate;     // Дата заключения
    private ExternalOrgUnit _externalOrgUnit;     // Организация
    private String _transcriptionLastName;     // Транскрипция фамилии
    private String _transcriptionFirstName;     // Транскрипция имени
    private String _transcriptionMiddleName;     // Транскрипция отчества
    private PgupsViewVisa _viewVisa;     // Вид визы
    private String _purposeVisit;     // Цель въезда
    private String _visaNumber;     // Номер визы
    private String _authorityIssuanceVisa;     // Орган, выдавший визу
    private Date _visaDateFrom;     // Действие визы с
    private Date _visaDateTo;     // Действие визы по
    private String _residenceCountryAddress;     // Адрес в стране постоянного проживания
    private String _migrationRegAddress;     // Адрес миграционного учета
    private Date _migrationRegDateFrom;     // Срок учета с
    private Date _migrationRegDateTo;     // Срок учета по
    private String _actualAddress;     // Фактический адрес
    private String _contactPhoneNumber;     // Контактный телефон
    private String _migrationCardNumber;     // Номер карты
    private Date _migrationCardDate;     // Дата въезда
    private String _migrationCardBorderPoint;     // Пункт пересечения границы
    private String _certificateNumber;     // Номер свидетельства
    private Date _certificateDate;     // Дата выдачи свидетельства
    private String _authorityIssuanceCertificate;     // Орган, выдавший свидетельство
    private Date _certificateDateFrom;     // Срок действия свидетельства с
    private Date _certificateDateTo;     // Срок действия свидетельства по

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Абитуриент. Свойство не может быть null.
     */
    @NotNull
    public EnrEntrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент. Свойство не может быть null.
     */
    public void setEntrant(EnrEntrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Номер договора.
     */
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер договора.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата заключения.
     */
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата заключения.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Организация.
     */
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Организация.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    /**
     * @return Транскрипция фамилии.
     */
    @Length(max=255)
    public String getTranscriptionLastName()
    {
        return _transcriptionLastName;
    }

    /**
     * @param transcriptionLastName Транскрипция фамилии.
     */
    public void setTranscriptionLastName(String transcriptionLastName)
    {
        dirty(_transcriptionLastName, transcriptionLastName);
        _transcriptionLastName = transcriptionLastName;
    }

    /**
     * @return Транскрипция имени.
     */
    @Length(max=255)
    public String getTranscriptionFirstName()
    {
        return _transcriptionFirstName;
    }

    /**
     * @param transcriptionFirstName Транскрипция имени.
     */
    public void setTranscriptionFirstName(String transcriptionFirstName)
    {
        dirty(_transcriptionFirstName, transcriptionFirstName);
        _transcriptionFirstName = transcriptionFirstName;
    }

    /**
     * @return Транскрипция отчества.
     */
    @Length(max=255)
    public String getTranscriptionMiddleName()
    {
        return _transcriptionMiddleName;
    }

    /**
     * @param transcriptionMiddleName Транскрипция отчества.
     */
    public void setTranscriptionMiddleName(String transcriptionMiddleName)
    {
        dirty(_transcriptionMiddleName, transcriptionMiddleName);
        _transcriptionMiddleName = transcriptionMiddleName;
    }

    /**
     * @return Вид визы.
     */
    public PgupsViewVisa getViewVisa()
    {
        return _viewVisa;
    }

    /**
     * @param viewVisa Вид визы.
     */
    public void setViewVisa(PgupsViewVisa viewVisa)
    {
        dirty(_viewVisa, viewVisa);
        _viewVisa = viewVisa;
    }

    /**
     * @return Цель въезда.
     */
    @Length(max=1024)
    public String getPurposeVisit()
    {
        return _purposeVisit;
    }

    /**
     * @param purposeVisit Цель въезда.
     */
    public void setPurposeVisit(String purposeVisit)
    {
        dirty(_purposeVisit, purposeVisit);
        _purposeVisit = purposeVisit;
    }

    /**
     * @return Номер визы.
     */
    @Length(max=255)
    public String getVisaNumber()
    {
        return _visaNumber;
    }

    /**
     * @param visaNumber Номер визы.
     */
    public void setVisaNumber(String visaNumber)
    {
        dirty(_visaNumber, visaNumber);
        _visaNumber = visaNumber;
    }

    /**
     * @return Орган, выдавший визу.
     */
    @Length(max=255)
    public String getAuthorityIssuanceVisa()
    {
        return _authorityIssuanceVisa;
    }

    /**
     * @param authorityIssuanceVisa Орган, выдавший визу.
     */
    public void setAuthorityIssuanceVisa(String authorityIssuanceVisa)
    {
        dirty(_authorityIssuanceVisa, authorityIssuanceVisa);
        _authorityIssuanceVisa = authorityIssuanceVisa;
    }

    /**
     * @return Действие визы с.
     */
    public Date getVisaDateFrom()
    {
        return _visaDateFrom;
    }

    /**
     * @param visaDateFrom Действие визы с.
     */
    public void setVisaDateFrom(Date visaDateFrom)
    {
        dirty(_visaDateFrom, visaDateFrom);
        _visaDateFrom = visaDateFrom;
    }

    /**
     * @return Действие визы по.
     */
    public Date getVisaDateTo()
    {
        return _visaDateTo;
    }

    /**
     * @param visaDateTo Действие визы по.
     */
    public void setVisaDateTo(Date visaDateTo)
    {
        dirty(_visaDateTo, visaDateTo);
        _visaDateTo = visaDateTo;
    }

    /**
     * @return Адрес в стране постоянного проживания.
     */
    @Length(max=1024)
    public String getResidenceCountryAddress()
    {
        return _residenceCountryAddress;
    }

    /**
     * @param residenceCountryAddress Адрес в стране постоянного проживания.
     */
    public void setResidenceCountryAddress(String residenceCountryAddress)
    {
        dirty(_residenceCountryAddress, residenceCountryAddress);
        _residenceCountryAddress = residenceCountryAddress;
    }

    /**
     * @return Адрес миграционного учета.
     */
    @Length(max=1024)
    public String getMigrationRegAddress()
    {
        return _migrationRegAddress;
    }

    /**
     * @param migrationRegAddress Адрес миграционного учета.
     */
    public void setMigrationRegAddress(String migrationRegAddress)
    {
        dirty(_migrationRegAddress, migrationRegAddress);
        _migrationRegAddress = migrationRegAddress;
    }

    /**
     * @return Срок учета с.
     */
    public Date getMigrationRegDateFrom()
    {
        return _migrationRegDateFrom;
    }

    /**
     * @param migrationRegDateFrom Срок учета с.
     */
    public void setMigrationRegDateFrom(Date migrationRegDateFrom)
    {
        dirty(_migrationRegDateFrom, migrationRegDateFrom);
        _migrationRegDateFrom = migrationRegDateFrom;
    }

    /**
     * @return Срок учета по.
     */
    public Date getMigrationRegDateTo()
    {
        return _migrationRegDateTo;
    }

    /**
     * @param migrationRegDateTo Срок учета по.
     */
    public void setMigrationRegDateTo(Date migrationRegDateTo)
    {
        dirty(_migrationRegDateTo, migrationRegDateTo);
        _migrationRegDateTo = migrationRegDateTo;
    }

    /**
     * @return Фактический адрес.
     */
    @Length(max=1024)
    public String getActualAddress()
    {
        return _actualAddress;
    }

    /**
     * @param actualAddress Фактический адрес.
     */
    public void setActualAddress(String actualAddress)
    {
        dirty(_actualAddress, actualAddress);
        _actualAddress = actualAddress;
    }

    /**
     * @return Контактный телефон.
     */
    @Length(max=255)
    public String getContactPhoneNumber()
    {
        return _contactPhoneNumber;
    }

    /**
     * @param contactPhoneNumber Контактный телефон.
     */
    public void setContactPhoneNumber(String contactPhoneNumber)
    {
        dirty(_contactPhoneNumber, contactPhoneNumber);
        _contactPhoneNumber = contactPhoneNumber;
    }

    /**
     * @return Номер карты.
     */
    @Length(max=255)
    public String getMigrationCardNumber()
    {
        return _migrationCardNumber;
    }

    /**
     * @param migrationCardNumber Номер карты.
     */
    public void setMigrationCardNumber(String migrationCardNumber)
    {
        dirty(_migrationCardNumber, migrationCardNumber);
        _migrationCardNumber = migrationCardNumber;
    }

    /**
     * @return Дата въезда.
     */
    public Date getMigrationCardDate()
    {
        return _migrationCardDate;
    }

    /**
     * @param migrationCardDate Дата въезда.
     */
    public void setMigrationCardDate(Date migrationCardDate)
    {
        dirty(_migrationCardDate, migrationCardDate);
        _migrationCardDate = migrationCardDate;
    }

    /**
     * @return Пункт пересечения границы.
     */
    @Length(max=255)
    public String getMigrationCardBorderPoint()
    {
        return _migrationCardBorderPoint;
    }

    /**
     * @param migrationCardBorderPoint Пункт пересечения границы.
     */
    public void setMigrationCardBorderPoint(String migrationCardBorderPoint)
    {
        dirty(_migrationCardBorderPoint, migrationCardBorderPoint);
        _migrationCardBorderPoint = migrationCardBorderPoint;
    }

    /**
     * @return Номер свидетельства.
     */
    @Length(max=255)
    public String getCertificateNumber()
    {
        return _certificateNumber;
    }

    /**
     * @param certificateNumber Номер свидетельства.
     */
    public void setCertificateNumber(String certificateNumber)
    {
        dirty(_certificateNumber, certificateNumber);
        _certificateNumber = certificateNumber;
    }

    /**
     * @return Дата выдачи свидетельства.
     */
    public Date getCertificateDate()
    {
        return _certificateDate;
    }

    /**
     * @param certificateDate Дата выдачи свидетельства.
     */
    public void setCertificateDate(Date certificateDate)
    {
        dirty(_certificateDate, certificateDate);
        _certificateDate = certificateDate;
    }

    /**
     * @return Орган, выдавший свидетельство.
     */
    @Length(max=255)
    public String getAuthorityIssuanceCertificate()
    {
        return _authorityIssuanceCertificate;
    }

    /**
     * @param authorityIssuanceCertificate Орган, выдавший свидетельство.
     */
    public void setAuthorityIssuanceCertificate(String authorityIssuanceCertificate)
    {
        dirty(_authorityIssuanceCertificate, authorityIssuanceCertificate);
        _authorityIssuanceCertificate = authorityIssuanceCertificate;
    }

    /**
     * @return Срок действия свидетельства с.
     */
    public Date getCertificateDateFrom()
    {
        return _certificateDateFrom;
    }

    /**
     * @param certificateDateFrom Срок действия свидетельства с.
     */
    public void setCertificateDateFrom(Date certificateDateFrom)
    {
        dirty(_certificateDateFrom, certificateDateFrom);
        _certificateDateFrom = certificateDateFrom;
    }

    /**
     * @return Срок действия свидетельства по.
     */
    public Date getCertificateDateTo()
    {
        return _certificateDateTo;
    }

    /**
     * @param certificateDateTo Срок действия свидетельства по.
     */
    public void setCertificateDateTo(Date certificateDateTo)
    {
        dirty(_certificateDateTo, certificateDateTo);
        _certificateDateTo = certificateDateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PgupsEnrForeignEntrantGen)
        {
            setEntrant(((PgupsEnrForeignEntrant)another).getEntrant());
            setContractNumber(((PgupsEnrForeignEntrant)another).getContractNumber());
            setContractDate(((PgupsEnrForeignEntrant)another).getContractDate());
            setExternalOrgUnit(((PgupsEnrForeignEntrant)another).getExternalOrgUnit());
            setTranscriptionLastName(((PgupsEnrForeignEntrant)another).getTranscriptionLastName());
            setTranscriptionFirstName(((PgupsEnrForeignEntrant)another).getTranscriptionFirstName());
            setTranscriptionMiddleName(((PgupsEnrForeignEntrant)another).getTranscriptionMiddleName());
            setViewVisa(((PgupsEnrForeignEntrant)another).getViewVisa());
            setPurposeVisit(((PgupsEnrForeignEntrant)another).getPurposeVisit());
            setVisaNumber(((PgupsEnrForeignEntrant)another).getVisaNumber());
            setAuthorityIssuanceVisa(((PgupsEnrForeignEntrant)another).getAuthorityIssuanceVisa());
            setVisaDateFrom(((PgupsEnrForeignEntrant)another).getVisaDateFrom());
            setVisaDateTo(((PgupsEnrForeignEntrant)another).getVisaDateTo());
            setResidenceCountryAddress(((PgupsEnrForeignEntrant)another).getResidenceCountryAddress());
            setMigrationRegAddress(((PgupsEnrForeignEntrant)another).getMigrationRegAddress());
            setMigrationRegDateFrom(((PgupsEnrForeignEntrant)another).getMigrationRegDateFrom());
            setMigrationRegDateTo(((PgupsEnrForeignEntrant)another).getMigrationRegDateTo());
            setActualAddress(((PgupsEnrForeignEntrant)another).getActualAddress());
            setContactPhoneNumber(((PgupsEnrForeignEntrant)another).getContactPhoneNumber());
            setMigrationCardNumber(((PgupsEnrForeignEntrant)another).getMigrationCardNumber());
            setMigrationCardDate(((PgupsEnrForeignEntrant)another).getMigrationCardDate());
            setMigrationCardBorderPoint(((PgupsEnrForeignEntrant)another).getMigrationCardBorderPoint());
            setCertificateNumber(((PgupsEnrForeignEntrant)another).getCertificateNumber());
            setCertificateDate(((PgupsEnrForeignEntrant)another).getCertificateDate());
            setAuthorityIssuanceCertificate(((PgupsEnrForeignEntrant)another).getAuthorityIssuanceCertificate());
            setCertificateDateFrom(((PgupsEnrForeignEntrant)another).getCertificateDateFrom());
            setCertificateDateTo(((PgupsEnrForeignEntrant)another).getCertificateDateTo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsEnrForeignEntrantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsEnrForeignEntrant.class;
        }

        public T newInstance()
        {
            return (T) new PgupsEnrForeignEntrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
                case "transcriptionLastName":
                    return obj.getTranscriptionLastName();
                case "transcriptionFirstName":
                    return obj.getTranscriptionFirstName();
                case "transcriptionMiddleName":
                    return obj.getTranscriptionMiddleName();
                case "viewVisa":
                    return obj.getViewVisa();
                case "purposeVisit":
                    return obj.getPurposeVisit();
                case "visaNumber":
                    return obj.getVisaNumber();
                case "authorityIssuanceVisa":
                    return obj.getAuthorityIssuanceVisa();
                case "visaDateFrom":
                    return obj.getVisaDateFrom();
                case "visaDateTo":
                    return obj.getVisaDateTo();
                case "residenceCountryAddress":
                    return obj.getResidenceCountryAddress();
                case "migrationRegAddress":
                    return obj.getMigrationRegAddress();
                case "migrationRegDateFrom":
                    return obj.getMigrationRegDateFrom();
                case "migrationRegDateTo":
                    return obj.getMigrationRegDateTo();
                case "actualAddress":
                    return obj.getActualAddress();
                case "contactPhoneNumber":
                    return obj.getContactPhoneNumber();
                case "migrationCardNumber":
                    return obj.getMigrationCardNumber();
                case "migrationCardDate":
                    return obj.getMigrationCardDate();
                case "migrationCardBorderPoint":
                    return obj.getMigrationCardBorderPoint();
                case "certificateNumber":
                    return obj.getCertificateNumber();
                case "certificateDate":
                    return obj.getCertificateDate();
                case "authorityIssuanceCertificate":
                    return obj.getAuthorityIssuanceCertificate();
                case "certificateDateFrom":
                    return obj.getCertificateDateFrom();
                case "certificateDateTo":
                    return obj.getCertificateDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((EnrEntrant) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
                case "transcriptionLastName":
                    obj.setTranscriptionLastName((String) value);
                    return;
                case "transcriptionFirstName":
                    obj.setTranscriptionFirstName((String) value);
                    return;
                case "transcriptionMiddleName":
                    obj.setTranscriptionMiddleName((String) value);
                    return;
                case "viewVisa":
                    obj.setViewVisa((PgupsViewVisa) value);
                    return;
                case "purposeVisit":
                    obj.setPurposeVisit((String) value);
                    return;
                case "visaNumber":
                    obj.setVisaNumber((String) value);
                    return;
                case "authorityIssuanceVisa":
                    obj.setAuthorityIssuanceVisa((String) value);
                    return;
                case "visaDateFrom":
                    obj.setVisaDateFrom((Date) value);
                    return;
                case "visaDateTo":
                    obj.setVisaDateTo((Date) value);
                    return;
                case "residenceCountryAddress":
                    obj.setResidenceCountryAddress((String) value);
                    return;
                case "migrationRegAddress":
                    obj.setMigrationRegAddress((String) value);
                    return;
                case "migrationRegDateFrom":
                    obj.setMigrationRegDateFrom((Date) value);
                    return;
                case "migrationRegDateTo":
                    obj.setMigrationRegDateTo((Date) value);
                    return;
                case "actualAddress":
                    obj.setActualAddress((String) value);
                    return;
                case "contactPhoneNumber":
                    obj.setContactPhoneNumber((String) value);
                    return;
                case "migrationCardNumber":
                    obj.setMigrationCardNumber((String) value);
                    return;
                case "migrationCardDate":
                    obj.setMigrationCardDate((Date) value);
                    return;
                case "migrationCardBorderPoint":
                    obj.setMigrationCardBorderPoint((String) value);
                    return;
                case "certificateNumber":
                    obj.setCertificateNumber((String) value);
                    return;
                case "certificateDate":
                    obj.setCertificateDate((Date) value);
                    return;
                case "authorityIssuanceCertificate":
                    obj.setAuthorityIssuanceCertificate((String) value);
                    return;
                case "certificateDateFrom":
                    obj.setCertificateDateFrom((Date) value);
                    return;
                case "certificateDateTo":
                    obj.setCertificateDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
                case "externalOrgUnit":
                        return true;
                case "transcriptionLastName":
                        return true;
                case "transcriptionFirstName":
                        return true;
                case "transcriptionMiddleName":
                        return true;
                case "viewVisa":
                        return true;
                case "purposeVisit":
                        return true;
                case "visaNumber":
                        return true;
                case "authorityIssuanceVisa":
                        return true;
                case "visaDateFrom":
                        return true;
                case "visaDateTo":
                        return true;
                case "residenceCountryAddress":
                        return true;
                case "migrationRegAddress":
                        return true;
                case "migrationRegDateFrom":
                        return true;
                case "migrationRegDateTo":
                        return true;
                case "actualAddress":
                        return true;
                case "contactPhoneNumber":
                        return true;
                case "migrationCardNumber":
                        return true;
                case "migrationCardDate":
                        return true;
                case "migrationCardBorderPoint":
                        return true;
                case "certificateNumber":
                        return true;
                case "certificateDate":
                        return true;
                case "authorityIssuanceCertificate":
                        return true;
                case "certificateDateFrom":
                        return true;
                case "certificateDateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
                case "externalOrgUnit":
                    return true;
                case "transcriptionLastName":
                    return true;
                case "transcriptionFirstName":
                    return true;
                case "transcriptionMiddleName":
                    return true;
                case "viewVisa":
                    return true;
                case "purposeVisit":
                    return true;
                case "visaNumber":
                    return true;
                case "authorityIssuanceVisa":
                    return true;
                case "visaDateFrom":
                    return true;
                case "visaDateTo":
                    return true;
                case "residenceCountryAddress":
                    return true;
                case "migrationRegAddress":
                    return true;
                case "migrationRegDateFrom":
                    return true;
                case "migrationRegDateTo":
                    return true;
                case "actualAddress":
                    return true;
                case "contactPhoneNumber":
                    return true;
                case "migrationCardNumber":
                    return true;
                case "migrationCardDate":
                    return true;
                case "migrationCardBorderPoint":
                    return true;
                case "certificateNumber":
                    return true;
                case "certificateDate":
                    return true;
                case "authorityIssuanceCertificate":
                    return true;
                case "certificateDateFrom":
                    return true;
                case "certificateDateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return EnrEntrant.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
                case "transcriptionLastName":
                    return String.class;
                case "transcriptionFirstName":
                    return String.class;
                case "transcriptionMiddleName":
                    return String.class;
                case "viewVisa":
                    return PgupsViewVisa.class;
                case "purposeVisit":
                    return String.class;
                case "visaNumber":
                    return String.class;
                case "authorityIssuanceVisa":
                    return String.class;
                case "visaDateFrom":
                    return Date.class;
                case "visaDateTo":
                    return Date.class;
                case "residenceCountryAddress":
                    return String.class;
                case "migrationRegAddress":
                    return String.class;
                case "migrationRegDateFrom":
                    return Date.class;
                case "migrationRegDateTo":
                    return Date.class;
                case "actualAddress":
                    return String.class;
                case "contactPhoneNumber":
                    return String.class;
                case "migrationCardNumber":
                    return String.class;
                case "migrationCardDate":
                    return Date.class;
                case "migrationCardBorderPoint":
                    return String.class;
                case "certificateNumber":
                    return String.class;
                case "certificateDate":
                    return Date.class;
                case "authorityIssuanceCertificate":
                    return String.class;
                case "certificateDateFrom":
                    return Date.class;
                case "certificateDateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsEnrForeignEntrant> _dslPath = new Path<PgupsEnrForeignEntrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsEnrForeignEntrant");
    }
            

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getEntrant()
     */
    public static EnrEntrant.Path<EnrEntrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата заключения.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Организация.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    /**
     * @return Транскрипция фамилии.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getTranscriptionLastName()
     */
    public static PropertyPath<String> transcriptionLastName()
    {
        return _dslPath.transcriptionLastName();
    }

    /**
     * @return Транскрипция имени.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getTranscriptionFirstName()
     */
    public static PropertyPath<String> transcriptionFirstName()
    {
        return _dslPath.transcriptionFirstName();
    }

    /**
     * @return Транскрипция отчества.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getTranscriptionMiddleName()
     */
    public static PropertyPath<String> transcriptionMiddleName()
    {
        return _dslPath.transcriptionMiddleName();
    }

    /**
     * @return Вид визы.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getViewVisa()
     */
    public static PgupsViewVisa.Path<PgupsViewVisa> viewVisa()
    {
        return _dslPath.viewVisa();
    }

    /**
     * @return Цель въезда.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getPurposeVisit()
     */
    public static PropertyPath<String> purposeVisit()
    {
        return _dslPath.purposeVisit();
    }

    /**
     * @return Номер визы.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getVisaNumber()
     */
    public static PropertyPath<String> visaNumber()
    {
        return _dslPath.visaNumber();
    }

    /**
     * @return Орган, выдавший визу.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getAuthorityIssuanceVisa()
     */
    public static PropertyPath<String> authorityIssuanceVisa()
    {
        return _dslPath.authorityIssuanceVisa();
    }

    /**
     * @return Действие визы с.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getVisaDateFrom()
     */
    public static PropertyPath<Date> visaDateFrom()
    {
        return _dslPath.visaDateFrom();
    }

    /**
     * @return Действие визы по.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getVisaDateTo()
     */
    public static PropertyPath<Date> visaDateTo()
    {
        return _dslPath.visaDateTo();
    }

    /**
     * @return Адрес в стране постоянного проживания.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getResidenceCountryAddress()
     */
    public static PropertyPath<String> residenceCountryAddress()
    {
        return _dslPath.residenceCountryAddress();
    }

    /**
     * @return Адрес миграционного учета.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationRegAddress()
     */
    public static PropertyPath<String> migrationRegAddress()
    {
        return _dslPath.migrationRegAddress();
    }

    /**
     * @return Срок учета с.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationRegDateFrom()
     */
    public static PropertyPath<Date> migrationRegDateFrom()
    {
        return _dslPath.migrationRegDateFrom();
    }

    /**
     * @return Срок учета по.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationRegDateTo()
     */
    public static PropertyPath<Date> migrationRegDateTo()
    {
        return _dslPath.migrationRegDateTo();
    }

    /**
     * @return Фактический адрес.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getActualAddress()
     */
    public static PropertyPath<String> actualAddress()
    {
        return _dslPath.actualAddress();
    }

    /**
     * @return Контактный телефон.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getContactPhoneNumber()
     */
    public static PropertyPath<String> contactPhoneNumber()
    {
        return _dslPath.contactPhoneNumber();
    }

    /**
     * @return Номер карты.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationCardNumber()
     */
    public static PropertyPath<String> migrationCardNumber()
    {
        return _dslPath.migrationCardNumber();
    }

    /**
     * @return Дата въезда.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationCardDate()
     */
    public static PropertyPath<Date> migrationCardDate()
    {
        return _dslPath.migrationCardDate();
    }

    /**
     * @return Пункт пересечения границы.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationCardBorderPoint()
     */
    public static PropertyPath<String> migrationCardBorderPoint()
    {
        return _dslPath.migrationCardBorderPoint();
    }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateNumber()
     */
    public static PropertyPath<String> certificateNumber()
    {
        return _dslPath.certificateNumber();
    }

    /**
     * @return Дата выдачи свидетельства.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateDate()
     */
    public static PropertyPath<Date> certificateDate()
    {
        return _dslPath.certificateDate();
    }

    /**
     * @return Орган, выдавший свидетельство.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getAuthorityIssuanceCertificate()
     */
    public static PropertyPath<String> authorityIssuanceCertificate()
    {
        return _dslPath.authorityIssuanceCertificate();
    }

    /**
     * @return Срок действия свидетельства с.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateDateFrom()
     */
    public static PropertyPath<Date> certificateDateFrom()
    {
        return _dslPath.certificateDateFrom();
    }

    /**
     * @return Срок действия свидетельства по.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateDateTo()
     */
    public static PropertyPath<Date> certificateDateTo()
    {
        return _dslPath.certificateDateTo();
    }

    public static class Path<E extends PgupsEnrForeignEntrant> extends EntityPath<E>
    {
        private EnrEntrant.Path<EnrEntrant> _entrant;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;
        private PropertyPath<String> _transcriptionLastName;
        private PropertyPath<String> _transcriptionFirstName;
        private PropertyPath<String> _transcriptionMiddleName;
        private PgupsViewVisa.Path<PgupsViewVisa> _viewVisa;
        private PropertyPath<String> _purposeVisit;
        private PropertyPath<String> _visaNumber;
        private PropertyPath<String> _authorityIssuanceVisa;
        private PropertyPath<Date> _visaDateFrom;
        private PropertyPath<Date> _visaDateTo;
        private PropertyPath<String> _residenceCountryAddress;
        private PropertyPath<String> _migrationRegAddress;
        private PropertyPath<Date> _migrationRegDateFrom;
        private PropertyPath<Date> _migrationRegDateTo;
        private PropertyPath<String> _actualAddress;
        private PropertyPath<String> _contactPhoneNumber;
        private PropertyPath<String> _migrationCardNumber;
        private PropertyPath<Date> _migrationCardDate;
        private PropertyPath<String> _migrationCardBorderPoint;
        private PropertyPath<String> _certificateNumber;
        private PropertyPath<Date> _certificateDate;
        private PropertyPath<String> _authorityIssuanceCertificate;
        private PropertyPath<Date> _certificateDateFrom;
        private PropertyPath<Date> _certificateDateTo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Абитуриент. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getEntrant()
     */
        public EnrEntrant.Path<EnrEntrant> entrant()
        {
            if(_entrant == null )
                _entrant = new EnrEntrant.Path<EnrEntrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Номер договора.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата заключения.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Организация.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

    /**
     * @return Транскрипция фамилии.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getTranscriptionLastName()
     */
        public PropertyPath<String> transcriptionLastName()
        {
            if(_transcriptionLastName == null )
                _transcriptionLastName = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_TRANSCRIPTION_LAST_NAME, this);
            return _transcriptionLastName;
        }

    /**
     * @return Транскрипция имени.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getTranscriptionFirstName()
     */
        public PropertyPath<String> transcriptionFirstName()
        {
            if(_transcriptionFirstName == null )
                _transcriptionFirstName = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_TRANSCRIPTION_FIRST_NAME, this);
            return _transcriptionFirstName;
        }

    /**
     * @return Транскрипция отчества.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getTranscriptionMiddleName()
     */
        public PropertyPath<String> transcriptionMiddleName()
        {
            if(_transcriptionMiddleName == null )
                _transcriptionMiddleName = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_TRANSCRIPTION_MIDDLE_NAME, this);
            return _transcriptionMiddleName;
        }

    /**
     * @return Вид визы.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getViewVisa()
     */
        public PgupsViewVisa.Path<PgupsViewVisa> viewVisa()
        {
            if(_viewVisa == null )
                _viewVisa = new PgupsViewVisa.Path<PgupsViewVisa>(L_VIEW_VISA, this);
            return _viewVisa;
        }

    /**
     * @return Цель въезда.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getPurposeVisit()
     */
        public PropertyPath<String> purposeVisit()
        {
            if(_purposeVisit == null )
                _purposeVisit = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_PURPOSE_VISIT, this);
            return _purposeVisit;
        }

    /**
     * @return Номер визы.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getVisaNumber()
     */
        public PropertyPath<String> visaNumber()
        {
            if(_visaNumber == null )
                _visaNumber = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_VISA_NUMBER, this);
            return _visaNumber;
        }

    /**
     * @return Орган, выдавший визу.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getAuthorityIssuanceVisa()
     */
        public PropertyPath<String> authorityIssuanceVisa()
        {
            if(_authorityIssuanceVisa == null )
                _authorityIssuanceVisa = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_AUTHORITY_ISSUANCE_VISA, this);
            return _authorityIssuanceVisa;
        }

    /**
     * @return Действие визы с.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getVisaDateFrom()
     */
        public PropertyPath<Date> visaDateFrom()
        {
            if(_visaDateFrom == null )
                _visaDateFrom = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_VISA_DATE_FROM, this);
            return _visaDateFrom;
        }

    /**
     * @return Действие визы по.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getVisaDateTo()
     */
        public PropertyPath<Date> visaDateTo()
        {
            if(_visaDateTo == null )
                _visaDateTo = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_VISA_DATE_TO, this);
            return _visaDateTo;
        }

    /**
     * @return Адрес в стране постоянного проживания.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getResidenceCountryAddress()
     */
        public PropertyPath<String> residenceCountryAddress()
        {
            if(_residenceCountryAddress == null )
                _residenceCountryAddress = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_RESIDENCE_COUNTRY_ADDRESS, this);
            return _residenceCountryAddress;
        }

    /**
     * @return Адрес миграционного учета.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationRegAddress()
     */
        public PropertyPath<String> migrationRegAddress()
        {
            if(_migrationRegAddress == null )
                _migrationRegAddress = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_MIGRATION_REG_ADDRESS, this);
            return _migrationRegAddress;
        }

    /**
     * @return Срок учета с.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationRegDateFrom()
     */
        public PropertyPath<Date> migrationRegDateFrom()
        {
            if(_migrationRegDateFrom == null )
                _migrationRegDateFrom = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_MIGRATION_REG_DATE_FROM, this);
            return _migrationRegDateFrom;
        }

    /**
     * @return Срок учета по.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationRegDateTo()
     */
        public PropertyPath<Date> migrationRegDateTo()
        {
            if(_migrationRegDateTo == null )
                _migrationRegDateTo = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_MIGRATION_REG_DATE_TO, this);
            return _migrationRegDateTo;
        }

    /**
     * @return Фактический адрес.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getActualAddress()
     */
        public PropertyPath<String> actualAddress()
        {
            if(_actualAddress == null )
                _actualAddress = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_ACTUAL_ADDRESS, this);
            return _actualAddress;
        }

    /**
     * @return Контактный телефон.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getContactPhoneNumber()
     */
        public PropertyPath<String> contactPhoneNumber()
        {
            if(_contactPhoneNumber == null )
                _contactPhoneNumber = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_CONTACT_PHONE_NUMBER, this);
            return _contactPhoneNumber;
        }

    /**
     * @return Номер карты.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationCardNumber()
     */
        public PropertyPath<String> migrationCardNumber()
        {
            if(_migrationCardNumber == null )
                _migrationCardNumber = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_MIGRATION_CARD_NUMBER, this);
            return _migrationCardNumber;
        }

    /**
     * @return Дата въезда.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationCardDate()
     */
        public PropertyPath<Date> migrationCardDate()
        {
            if(_migrationCardDate == null )
                _migrationCardDate = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_MIGRATION_CARD_DATE, this);
            return _migrationCardDate;
        }

    /**
     * @return Пункт пересечения границы.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getMigrationCardBorderPoint()
     */
        public PropertyPath<String> migrationCardBorderPoint()
        {
            if(_migrationCardBorderPoint == null )
                _migrationCardBorderPoint = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_MIGRATION_CARD_BORDER_POINT, this);
            return _migrationCardBorderPoint;
        }

    /**
     * @return Номер свидетельства.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateNumber()
     */
        public PropertyPath<String> certificateNumber()
        {
            if(_certificateNumber == null )
                _certificateNumber = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_CERTIFICATE_NUMBER, this);
            return _certificateNumber;
        }

    /**
     * @return Дата выдачи свидетельства.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateDate()
     */
        public PropertyPath<Date> certificateDate()
        {
            if(_certificateDate == null )
                _certificateDate = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_CERTIFICATE_DATE, this);
            return _certificateDate;
        }

    /**
     * @return Орган, выдавший свидетельство.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getAuthorityIssuanceCertificate()
     */
        public PropertyPath<String> authorityIssuanceCertificate()
        {
            if(_authorityIssuanceCertificate == null )
                _authorityIssuanceCertificate = new PropertyPath<String>(PgupsEnrForeignEntrantGen.P_AUTHORITY_ISSUANCE_CERTIFICATE, this);
            return _authorityIssuanceCertificate;
        }

    /**
     * @return Срок действия свидетельства с.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateDateFrom()
     */
        public PropertyPath<Date> certificateDateFrom()
        {
            if(_certificateDateFrom == null )
                _certificateDateFrom = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_CERTIFICATE_DATE_FROM, this);
            return _certificateDateFrom;
        }

    /**
     * @return Срок действия свидетельства по.
     * @see ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant#getCertificateDateTo()
     */
        public PropertyPath<Date> certificateDateTo()
        {
            if(_certificateDateTo == null )
                _certificateDateTo = new PropertyPath<Date>(PgupsEnrForeignEntrantGen.P_CERTIFICATE_DATE_TO, this);
            return _certificateDateTo;
        }

        public Class getEntityClass()
        {
            return PgupsEnrForeignEntrant.class;
        }

        public String getEntityName()
        {
            return "pgupsEnrForeignEntrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
