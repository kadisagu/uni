/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104.AddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.ICommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public interface IDAO extends ICommonSingleEmployeeExtractAddEditDAO<AdditionalPaymentExtract, Model>
{
}