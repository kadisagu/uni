package ru.tandemservice.unipgups.reactors.daemon;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics;

public interface INsiUniEduProgramDisciplineStatisticsDaemon
{
    final String GLOBAL_DAEMON_LOCK = INsiUniEduProgramDisciplineStatisticsDaemon.class.getName() + ".global-lock";
    final SpringBeanCache<INsiUniEduProgramDisciplineStatisticsDaemon> instance = new SpringBeanCache<>(INsiUniEduProgramDisciplineStatisticsDaemon.class.getName());

    void initStatistics();

    void updateStatistics(Student student);

    NsiUniEduProgramDisciplineStatistics findStatistics(DevelopForm developForm, Course course, Term term, EduProgramQualification qualification, EducationLevels levels, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit);
}