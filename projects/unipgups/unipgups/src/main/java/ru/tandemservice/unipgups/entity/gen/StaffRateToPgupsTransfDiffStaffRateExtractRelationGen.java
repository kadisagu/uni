package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;
import ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь ставки должности и выписки индивидуального приказа «О переводе сотрудника на иную долю ставки»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StaffRateToPgupsTransfDiffStaffRateExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation";
    public static final String ENTITY_NAME = "staffRateToPgupsTransfDiffStaffRateExtractRelation";
    public static final int VERSION_HASH = 281095685;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_EMPLOYEE_POST_STAFF_RATE_ITEM = "employeePostStaffRateItem";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_FINANCING_SOURCE_ITEM = "financingSourceItem";
    public static final String L_STAFF_LIST_ALLOCATION_ITEM = "staffListAllocationItem";
    public static final String P_STAFF_RATE = "staffRate";
    public static final String L_FINANCING_SOURCE_OLD = "financingSourceOld";
    public static final String L_FINANCING_SOURCE_ITEM_OLD = "financingSourceItemOld";
    public static final String P_STAFF_RATE_OLD = "staffRateOld";

    private PgupsTransferToDiffStaffRatesExtract _extract;     // Выписка из индивидуального приказа по кадровому составу. О переводе сотрудника на иную долю ставки
    private EmployeePostStaffRateItem _employeePostStaffRateItem;     // Ставка сотрудника (связь ставки и сотрудника)
    private FinancingSource _financingSource;     // Источник финансирования (настройка названий)
    private FinancingSourceItem _financingSourceItem;     // Источник финансирования (настройка названий, детально)
    private StaffListAllocationItem _staffListAllocationItem;     // Созданная должность штатной расстановки
    private double _staffRate;     // Ставка
    private FinancingSource _financingSourceOld;     // Предыдущий источник финансирования
    private FinancingSourceItem _financingSourceItemOld;     // Предыдущий источник финансирования(детально)
    private Double _staffRateOld;     // Предыдушая ставка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О переводе сотрудника на иную долю ставки. Свойство не может быть null.
     */
    @NotNull
    public PgupsTransferToDiffStaffRatesExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка из индивидуального приказа по кадровому составу. О переводе сотрудника на иную долю ставки. Свойство не может быть null.
     */
    public void setExtract(PgupsTransferToDiffStaffRatesExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Ставка сотрудника (связь ставки и сотрудника).
     */
    public EmployeePostStaffRateItem getEmployeePostStaffRateItem()
    {
        return _employeePostStaffRateItem;
    }

    /**
     * @param employeePostStaffRateItem Ставка сотрудника (связь ставки и сотрудника).
     */
    public void setEmployeePostStaffRateItem(EmployeePostStaffRateItem employeePostStaffRateItem)
    {
        dirty(_employeePostStaffRateItem, employeePostStaffRateItem);
        _employeePostStaffRateItem = employeePostStaffRateItem;
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     */
    @NotNull
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования (настройка названий). Свойство не может быть null.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     */
    public FinancingSourceItem getFinancingSourceItem()
    {
        return _financingSourceItem;
    }

    /**
     * @param financingSourceItem Источник финансирования (настройка названий, детально).
     */
    public void setFinancingSourceItem(FinancingSourceItem financingSourceItem)
    {
        dirty(_financingSourceItem, financingSourceItem);
        _financingSourceItem = financingSourceItem;
    }

    /**
     * @return Созданная должность штатной расстановки.
     */
    public StaffListAllocationItem getStaffListAllocationItem()
    {
        return _staffListAllocationItem;
    }

    /**
     * @param staffListAllocationItem Созданная должность штатной расстановки.
     */
    public void setStaffListAllocationItem(StaffListAllocationItem staffListAllocationItem)
    {
        dirty(_staffListAllocationItem, staffListAllocationItem);
        _staffListAllocationItem = staffListAllocationItem;
    }

    /**
     * @return Ставка. Свойство не может быть null.
     */
    @NotNull
    public double getStaffRate()
    {
        return _staffRate;
    }

    /**
     * @param staffRate Ставка. Свойство не может быть null.
     */
    public void setStaffRate(double staffRate)
    {
        dirty(_staffRate, staffRate);
        _staffRate = staffRate;
    }

    /**
     * @return Предыдущий источник финансирования.
     */
    public FinancingSource getFinancingSourceOld()
    {
        return _financingSourceOld;
    }

    /**
     * @param financingSourceOld Предыдущий источник финансирования.
     */
    public void setFinancingSourceOld(FinancingSource financingSourceOld)
    {
        dirty(_financingSourceOld, financingSourceOld);
        _financingSourceOld = financingSourceOld;
    }

    /**
     * @return Предыдущий источник финансирования(детально).
     */
    public FinancingSourceItem getFinancingSourceItemOld()
    {
        return _financingSourceItemOld;
    }

    /**
     * @param financingSourceItemOld Предыдущий источник финансирования(детально).
     */
    public void setFinancingSourceItemOld(FinancingSourceItem financingSourceItemOld)
    {
        dirty(_financingSourceItemOld, financingSourceItemOld);
        _financingSourceItemOld = financingSourceItemOld;
    }

    /**
     * @return Предыдушая ставка.
     */
    public Double getStaffRateOld()
    {
        return _staffRateOld;
    }

    /**
     * @param staffRateOld Предыдушая ставка.
     */
    public void setStaffRateOld(Double staffRateOld)
    {
        dirty(_staffRateOld, staffRateOld);
        _staffRateOld = staffRateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StaffRateToPgupsTransfDiffStaffRateExtractRelationGen)
        {
            setExtract(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getExtract());
            setEmployeePostStaffRateItem(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getEmployeePostStaffRateItem());
            setFinancingSource(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getFinancingSource());
            setFinancingSourceItem(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getFinancingSourceItem());
            setStaffListAllocationItem(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getStaffListAllocationItem());
            setStaffRate(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getStaffRate());
            setFinancingSourceOld(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getFinancingSourceOld());
            setFinancingSourceItemOld(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getFinancingSourceItemOld());
            setStaffRateOld(((StaffRateToPgupsTransfDiffStaffRateExtractRelation)another).getStaffRateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StaffRateToPgupsTransfDiffStaffRateExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StaffRateToPgupsTransfDiffStaffRateExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new StaffRateToPgupsTransfDiffStaffRateExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "employeePostStaffRateItem":
                    return obj.getEmployeePostStaffRateItem();
                case "financingSource":
                    return obj.getFinancingSource();
                case "financingSourceItem":
                    return obj.getFinancingSourceItem();
                case "staffListAllocationItem":
                    return obj.getStaffListAllocationItem();
                case "staffRate":
                    return obj.getStaffRate();
                case "financingSourceOld":
                    return obj.getFinancingSourceOld();
                case "financingSourceItemOld":
                    return obj.getFinancingSourceItemOld();
                case "staffRateOld":
                    return obj.getStaffRateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((PgupsTransferToDiffStaffRatesExtract) value);
                    return;
                case "employeePostStaffRateItem":
                    obj.setEmployeePostStaffRateItem((EmployeePostStaffRateItem) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "financingSourceItem":
                    obj.setFinancingSourceItem((FinancingSourceItem) value);
                    return;
                case "staffListAllocationItem":
                    obj.setStaffListAllocationItem((StaffListAllocationItem) value);
                    return;
                case "staffRate":
                    obj.setStaffRate((Double) value);
                    return;
                case "financingSourceOld":
                    obj.setFinancingSourceOld((FinancingSource) value);
                    return;
                case "financingSourceItemOld":
                    obj.setFinancingSourceItemOld((FinancingSourceItem) value);
                    return;
                case "staffRateOld":
                    obj.setStaffRateOld((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "employeePostStaffRateItem":
                        return true;
                case "financingSource":
                        return true;
                case "financingSourceItem":
                        return true;
                case "staffListAllocationItem":
                        return true;
                case "staffRate":
                        return true;
                case "financingSourceOld":
                        return true;
                case "financingSourceItemOld":
                        return true;
                case "staffRateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "employeePostStaffRateItem":
                    return true;
                case "financingSource":
                    return true;
                case "financingSourceItem":
                    return true;
                case "staffListAllocationItem":
                    return true;
                case "staffRate":
                    return true;
                case "financingSourceOld":
                    return true;
                case "financingSourceItemOld":
                    return true;
                case "staffRateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return PgupsTransferToDiffStaffRatesExtract.class;
                case "employeePostStaffRateItem":
                    return EmployeePostStaffRateItem.class;
                case "financingSource":
                    return FinancingSource.class;
                case "financingSourceItem":
                    return FinancingSourceItem.class;
                case "staffListAllocationItem":
                    return StaffListAllocationItem.class;
                case "staffRate":
                    return Double.class;
                case "financingSourceOld":
                    return FinancingSource.class;
                case "financingSourceItemOld":
                    return FinancingSourceItem.class;
                case "staffRateOld":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StaffRateToPgupsTransfDiffStaffRateExtractRelation> _dslPath = new Path<StaffRateToPgupsTransfDiffStaffRateExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StaffRateToPgupsTransfDiffStaffRateExtractRelation");
    }
            

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О переводе сотрудника на иную долю ставки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getExtract()
     */
    public static PgupsTransferToDiffStaffRatesExtract.Path<PgupsTransferToDiffStaffRatesExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Ставка сотрудника (связь ставки и сотрудника).
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getEmployeePostStaffRateItem()
     */
    public static EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> employeePostStaffRateItem()
    {
        return _dslPath.employeePostStaffRateItem();
    }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSourceItem()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
    {
        return _dslPath.financingSourceItem();
    }

    /**
     * @return Созданная должность штатной расстановки.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getStaffListAllocationItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
    {
        return _dslPath.staffListAllocationItem();
    }

    /**
     * @return Ставка. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getStaffRate()
     */
    public static PropertyPath<Double> staffRate()
    {
        return _dslPath.staffRate();
    }

    /**
     * @return Предыдущий источник финансирования.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSourceOld()
     */
    public static FinancingSource.Path<FinancingSource> financingSourceOld()
    {
        return _dslPath.financingSourceOld();
    }

    /**
     * @return Предыдущий источник финансирования(детально).
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSourceItemOld()
     */
    public static FinancingSourceItem.Path<FinancingSourceItem> financingSourceItemOld()
    {
        return _dslPath.financingSourceItemOld();
    }

    /**
     * @return Предыдушая ставка.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getStaffRateOld()
     */
    public static PropertyPath<Double> staffRateOld()
    {
        return _dslPath.staffRateOld();
    }

    public static class Path<E extends StaffRateToPgupsTransfDiffStaffRateExtractRelation> extends EntityPath<E>
    {
        private PgupsTransferToDiffStaffRatesExtract.Path<PgupsTransferToDiffStaffRatesExtract> _extract;
        private EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> _employeePostStaffRateItem;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItem;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _staffListAllocationItem;
        private PropertyPath<Double> _staffRate;
        private FinancingSource.Path<FinancingSource> _financingSourceOld;
        private FinancingSourceItem.Path<FinancingSourceItem> _financingSourceItemOld;
        private PropertyPath<Double> _staffRateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из индивидуального приказа по кадровому составу. О переводе сотрудника на иную долю ставки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getExtract()
     */
        public PgupsTransferToDiffStaffRatesExtract.Path<PgupsTransferToDiffStaffRatesExtract> extract()
        {
            if(_extract == null )
                _extract = new PgupsTransferToDiffStaffRatesExtract.Path<PgupsTransferToDiffStaffRatesExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Ставка сотрудника (связь ставки и сотрудника).
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getEmployeePostStaffRateItem()
     */
        public EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem> employeePostStaffRateItem()
        {
            if(_employeePostStaffRateItem == null )
                _employeePostStaffRateItem = new EmployeePostStaffRateItem.Path<EmployeePostStaffRateItem>(L_EMPLOYEE_POST_STAFF_RATE_ITEM, this);
            return _employeePostStaffRateItem;
        }

    /**
     * @return Источник финансирования (настройка названий). Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Источник финансирования (настройка названий, детально).
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSourceItem()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItem()
        {
            if(_financingSourceItem == null )
                _financingSourceItem = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM, this);
            return _financingSourceItem;
        }

    /**
     * @return Созданная должность штатной расстановки.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getStaffListAllocationItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> staffListAllocationItem()
        {
            if(_staffListAllocationItem == null )
                _staffListAllocationItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_STAFF_LIST_ALLOCATION_ITEM, this);
            return _staffListAllocationItem;
        }

    /**
     * @return Ставка. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getStaffRate()
     */
        public PropertyPath<Double> staffRate()
        {
            if(_staffRate == null )
                _staffRate = new PropertyPath<Double>(StaffRateToPgupsTransfDiffStaffRateExtractRelationGen.P_STAFF_RATE, this);
            return _staffRate;
        }

    /**
     * @return Предыдущий источник финансирования.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSourceOld()
     */
        public FinancingSource.Path<FinancingSource> financingSourceOld()
        {
            if(_financingSourceOld == null )
                _financingSourceOld = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE_OLD, this);
            return _financingSourceOld;
        }

    /**
     * @return Предыдущий источник финансирования(детально).
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getFinancingSourceItemOld()
     */
        public FinancingSourceItem.Path<FinancingSourceItem> financingSourceItemOld()
        {
            if(_financingSourceItemOld == null )
                _financingSourceItemOld = new FinancingSourceItem.Path<FinancingSourceItem>(L_FINANCING_SOURCE_ITEM_OLD, this);
            return _financingSourceItemOld;
        }

    /**
     * @return Предыдушая ставка.
     * @see ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation#getStaffRateOld()
     */
        public PropertyPath<Double> staffRateOld()
        {
            if(_staffRateOld == null )
                _staffRateOld = new PropertyPath<Double>(StaffRateToPgupsTransfDiffStaffRateExtractRelationGen.P_STAFF_RATE_OLD, this);
            return _staffRateOld;
        }

        public Class getEntityClass()
        {
            return StaffRateToPgupsTransfDiffStaffRateExtractRelation.class;
        }

        public String getEntityName()
        {
            return "staffRateToPgupsTransfDiffStaffRateExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
