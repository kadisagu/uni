package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. О продлении срока действия трудового договора
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ProlongationContractExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.ProlongationContractExtract";
    public static final String ENTITY_NAME = "prolongationContractExtract";
    public static final int VERSION_HASH = 896410001;
    private static IEntityMeta ENTITY_META;

    public static final String L_LABOUR_CONTRACT = "labourContract";
    public static final String P_NEW_END_DATE = "newEndDate";
    public static final String P_OLD_END_DATE = "oldEndDate";
    public static final String P_OLD_DISMISSAL_DATE = "oldDismissalDate";
    public static final String P_CONTRACT_ADD_AGREEMENT_NUMBER = "contractAddAgreementNumber";
    public static final String P_CONTRACT_ADD_AGREEMENT_DATE = "contractAddAgreementDate";
    public static final String L_CREATE_COLLATERAL_AGREEMENT = "createCollateralAgreement";

    private EmployeeLabourContract _labourContract;     // Трудовой договор
    private Date _newEndDate;     // Новая дата окончания трудового договора
    private Date _oldEndDate;     // Дата прекращения договора до проведения приказа
    private Date _oldDismissalDate;     // Дата увольнения сотрудника до проведения приказа
    private String _contractAddAgreementNumber;     // Номер доп. соглашения
    private Date _contractAddAgreementDate;     // Дата доп. соглашения
    private ContractCollateralAgreement _createCollateralAgreement;     // Доп. соглашение созданное при проведении выписки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     */
    @NotNull
    public EmployeeLabourContract getLabourContract()
    {
        return _labourContract;
    }

    /**
     * @param labourContract Трудовой договор. Свойство не может быть null.
     */
    public void setLabourContract(EmployeeLabourContract labourContract)
    {
        dirty(_labourContract, labourContract);
        _labourContract = labourContract;
    }

    /**
     * @return Новая дата окончания трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getNewEndDate()
    {
        return _newEndDate;
    }

    /**
     * @param newEndDate Новая дата окончания трудового договора. Свойство не может быть null.
     */
    public void setNewEndDate(Date newEndDate)
    {
        dirty(_newEndDate, newEndDate);
        _newEndDate = newEndDate;
    }

    /**
     * @return Дата прекращения договора до проведения приказа.
     */
    public Date getOldEndDate()
    {
        return _oldEndDate;
    }

    /**
     * @param oldEndDate Дата прекращения договора до проведения приказа.
     */
    public void setOldEndDate(Date oldEndDate)
    {
        dirty(_oldEndDate, oldEndDate);
        _oldEndDate = oldEndDate;
    }

    /**
     * @return Дата увольнения сотрудника до проведения приказа.
     */
    public Date getOldDismissalDate()
    {
        return _oldDismissalDate;
    }

    /**
     * @param oldDismissalDate Дата увольнения сотрудника до проведения приказа.
     */
    public void setOldDismissalDate(Date oldDismissalDate)
    {
        dirty(_oldDismissalDate, oldDismissalDate);
        _oldDismissalDate = oldDismissalDate;
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractAddAgreementNumber()
    {
        return _contractAddAgreementNumber;
    }

    /**
     * @param contractAddAgreementNumber Номер доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementNumber(String contractAddAgreementNumber)
    {
        dirty(_contractAddAgreementNumber, contractAddAgreementNumber);
        _contractAddAgreementNumber = contractAddAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    public Date getContractAddAgreementDate()
    {
        return _contractAddAgreementDate;
    }

    /**
     * @param contractAddAgreementDate Дата доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementDate(Date contractAddAgreementDate)
    {
        dirty(_contractAddAgreementDate, contractAddAgreementDate);
        _contractAddAgreementDate = contractAddAgreementDate;
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     */
    public ContractCollateralAgreement getCreateCollateralAgreement()
    {
        return _createCollateralAgreement;
    }

    /**
     * @param createCollateralAgreement Доп. соглашение созданное при проведении выписки.
     */
    public void setCreateCollateralAgreement(ContractCollateralAgreement createCollateralAgreement)
    {
        dirty(_createCollateralAgreement, createCollateralAgreement);
        _createCollateralAgreement = createCollateralAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ProlongationContractExtractGen)
        {
            setLabourContract(((ProlongationContractExtract)another).getLabourContract());
            setNewEndDate(((ProlongationContractExtract)another).getNewEndDate());
            setOldEndDate(((ProlongationContractExtract)another).getOldEndDate());
            setOldDismissalDate(((ProlongationContractExtract)another).getOldDismissalDate());
            setContractAddAgreementNumber(((ProlongationContractExtract)another).getContractAddAgreementNumber());
            setContractAddAgreementDate(((ProlongationContractExtract)another).getContractAddAgreementDate());
            setCreateCollateralAgreement(((ProlongationContractExtract)another).getCreateCollateralAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ProlongationContractExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ProlongationContractExtract.class;
        }

        public T newInstance()
        {
            return (T) new ProlongationContractExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "labourContract":
                    return obj.getLabourContract();
                case "newEndDate":
                    return obj.getNewEndDate();
                case "oldEndDate":
                    return obj.getOldEndDate();
                case "oldDismissalDate":
                    return obj.getOldDismissalDate();
                case "contractAddAgreementNumber":
                    return obj.getContractAddAgreementNumber();
                case "contractAddAgreementDate":
                    return obj.getContractAddAgreementDate();
                case "createCollateralAgreement":
                    return obj.getCreateCollateralAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "labourContract":
                    obj.setLabourContract((EmployeeLabourContract) value);
                    return;
                case "newEndDate":
                    obj.setNewEndDate((Date) value);
                    return;
                case "oldEndDate":
                    obj.setOldEndDate((Date) value);
                    return;
                case "oldDismissalDate":
                    obj.setOldDismissalDate((Date) value);
                    return;
                case "contractAddAgreementNumber":
                    obj.setContractAddAgreementNumber((String) value);
                    return;
                case "contractAddAgreementDate":
                    obj.setContractAddAgreementDate((Date) value);
                    return;
                case "createCollateralAgreement":
                    obj.setCreateCollateralAgreement((ContractCollateralAgreement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "labourContract":
                        return true;
                case "newEndDate":
                        return true;
                case "oldEndDate":
                        return true;
                case "oldDismissalDate":
                        return true;
                case "contractAddAgreementNumber":
                        return true;
                case "contractAddAgreementDate":
                        return true;
                case "createCollateralAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "labourContract":
                    return true;
                case "newEndDate":
                    return true;
                case "oldEndDate":
                    return true;
                case "oldDismissalDate":
                    return true;
                case "contractAddAgreementNumber":
                    return true;
                case "contractAddAgreementDate":
                    return true;
                case "createCollateralAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "labourContract":
                    return EmployeeLabourContract.class;
                case "newEndDate":
                    return Date.class;
                case "oldEndDate":
                    return Date.class;
                case "oldDismissalDate":
                    return Date.class;
                case "contractAddAgreementNumber":
                    return String.class;
                case "contractAddAgreementDate":
                    return Date.class;
                case "createCollateralAgreement":
                    return ContractCollateralAgreement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ProlongationContractExtract> _dslPath = new Path<ProlongationContractExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ProlongationContractExtract");
    }
            

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getLabourContract()
     */
    public static EmployeeLabourContract.Path<EmployeeLabourContract> labourContract()
    {
        return _dslPath.labourContract();
    }

    /**
     * @return Новая дата окончания трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getNewEndDate()
     */
    public static PropertyPath<Date> newEndDate()
    {
        return _dslPath.newEndDate();
    }

    /**
     * @return Дата прекращения договора до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getOldEndDate()
     */
    public static PropertyPath<Date> oldEndDate()
    {
        return _dslPath.oldEndDate();
    }

    /**
     * @return Дата увольнения сотрудника до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getOldDismissalDate()
     */
    public static PropertyPath<Date> oldDismissalDate()
    {
        return _dslPath.oldDismissalDate();
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getContractAddAgreementNumber()
     */
    public static PropertyPath<String> contractAddAgreementNumber()
    {
        return _dslPath.contractAddAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getContractAddAgreementDate()
     */
    public static PropertyPath<Date> contractAddAgreementDate()
    {
        return _dslPath.contractAddAgreementDate();
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getCreateCollateralAgreement()
     */
    public static ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
    {
        return _dslPath.createCollateralAgreement();
    }

    public static class Path<E extends ProlongationContractExtract> extends SingleEmployeeExtract.Path<E>
    {
        private EmployeeLabourContract.Path<EmployeeLabourContract> _labourContract;
        private PropertyPath<Date> _newEndDate;
        private PropertyPath<Date> _oldEndDate;
        private PropertyPath<Date> _oldDismissalDate;
        private PropertyPath<String> _contractAddAgreementNumber;
        private PropertyPath<Date> _contractAddAgreementDate;
        private ContractCollateralAgreement.Path<ContractCollateralAgreement> _createCollateralAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getLabourContract()
     */
        public EmployeeLabourContract.Path<EmployeeLabourContract> labourContract()
        {
            if(_labourContract == null )
                _labourContract = new EmployeeLabourContract.Path<EmployeeLabourContract>(L_LABOUR_CONTRACT, this);
            return _labourContract;
        }

    /**
     * @return Новая дата окончания трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getNewEndDate()
     */
        public PropertyPath<Date> newEndDate()
        {
            if(_newEndDate == null )
                _newEndDate = new PropertyPath<Date>(ProlongationContractExtractGen.P_NEW_END_DATE, this);
            return _newEndDate;
        }

    /**
     * @return Дата прекращения договора до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getOldEndDate()
     */
        public PropertyPath<Date> oldEndDate()
        {
            if(_oldEndDate == null )
                _oldEndDate = new PropertyPath<Date>(ProlongationContractExtractGen.P_OLD_END_DATE, this);
            return _oldEndDate;
        }

    /**
     * @return Дата увольнения сотрудника до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getOldDismissalDate()
     */
        public PropertyPath<Date> oldDismissalDate()
        {
            if(_oldDismissalDate == null )
                _oldDismissalDate = new PropertyPath<Date>(ProlongationContractExtractGen.P_OLD_DISMISSAL_DATE, this);
            return _oldDismissalDate;
        }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getContractAddAgreementNumber()
     */
        public PropertyPath<String> contractAddAgreementNumber()
        {
            if(_contractAddAgreementNumber == null )
                _contractAddAgreementNumber = new PropertyPath<String>(ProlongationContractExtractGen.P_CONTRACT_ADD_AGREEMENT_NUMBER, this);
            return _contractAddAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getContractAddAgreementDate()
     */
        public PropertyPath<Date> contractAddAgreementDate()
        {
            if(_contractAddAgreementDate == null )
                _contractAddAgreementDate = new PropertyPath<Date>(ProlongationContractExtractGen.P_CONTRACT_ADD_AGREEMENT_DATE, this);
            return _contractAddAgreementDate;
        }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.ProlongationContractExtract#getCreateCollateralAgreement()
     */
        public ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
        {
            if(_createCollateralAgreement == null )
                _createCollateralAgreement = new ContractCollateralAgreement.Path<ContractCollateralAgreement>(L_CREATE_COLLATERAL_AGREEMENT, this);
            return _createCollateralAgreement;
        }

        public Class getEntityClass()
        {
            return ProlongationContractExtract.class;
        }

        public String getEntityName()
        {
            return "prolongationContractExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
