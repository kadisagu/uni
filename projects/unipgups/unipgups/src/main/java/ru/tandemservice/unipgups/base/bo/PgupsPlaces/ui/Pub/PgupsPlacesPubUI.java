/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.AddEdit.PgupsPlacesAddEdit;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.uniplaces.UniplacesComponents;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "placeId", required = true)
})
public class PgupsPlacesPubUI extends UIPresenter
{
    private Long _placeId;
    private UniPgupsPlace _place;

    public Long getPlaceId()
    {
        return _placeId;
    }

    public void setPlaceId(Long placeId)
    {
        _placeId = placeId;
    }

    public UniPgupsPlace getPlace()
    {
        return _place;
    }

    public void setPlace(UniPgupsPlace place)
    {
        _place = place;
    }

    @Override
    public void onComponentRefresh()
    {
        _place = DataAccessServices.dao().getNotNull(UniPgupsPlace.class, _placeId);
    }

    public void onClickEditPgupsPlace()
    {
        _uiActivation.asRegionDialog(PgupsPlacesAddEdit.class).parameter("placeId", _placeId).activate();
    }

    public void onClickDeletePgupsPlace()
    {
        DataAccessServices.dao().delete(_placeId);
        deactivate();
    }

    public void onClickAddPlace()
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(UniplacesComponents.PLACE_ADD_EDIT, new ParametersMap().add("placePgupsId", _placeId)));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(PgupsPlacesPub.PLACES_DS))
            dataSource.put(PgupsPlacesPub.PLACE_ID, _placeId);
    }

    public void onEditPlaceClick()
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(UniplacesComponents.PLACE_ADD_EDIT, new ParametersMap().add("placeId", getListenerParameterAsLong())));
    }

    public void onDeletePlaceClick()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}