/* $Id:$ */
package ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditVisa;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unipgups.entity.catalog.PgupsViewVisa;
import ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant;

import java.util.Date;

/**
 * @author Denis Perminov
 * @since 29.07.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required = true))
public class PgupsEnrEntrantEditVisaUI extends UIPresenter
{
    private EnrEntrant _enrEntrant = new EnrEntrant();
    private PgupsEnrForeignEntrant _foreignEntrant = new PgupsEnrForeignEntrant();
    private ISelectModel viewVisaListModel;

    @Override
    public void onComponentRefresh()
    {
        setEntrant(CommonDAO.DAO_CACHE.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        _foreignEntrant = CommonDAO.DAO_CACHE.get().get(PgupsEnrForeignEntrant.class, PgupsEnrForeignEntrant.L_ENTRANT, _enrEntrant);
        if (null == _foreignEntrant)
        {
            _foreignEntrant = new PgupsEnrForeignEntrant();
            _foreignEntrant.setEntrant(_enrEntrant);
        }

        setViewVisaListModel(new LazySimpleSelectModel<>(PgupsViewVisa.class));
    }

    public void onClickApply()
    {
        CommonDAO.DAO_CACHE.get().saveOrUpdate(_foreignEntrant);
        deactivate();
    }

        public Validator getVisaDateToValidator()
    {
        return new BaseValidator()
        {
            public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
            {
                if ((_foreignEntrant.getVisaDateFrom() != null) && (object != null))
                    if (((Date) object).before(_foreignEntrant.getVisaDateFrom()))
                        throw new ValidatorException("Дата окончания периода должна быть после даты начала");
            }
        };
    }

    public EnrEntrant getEntrant()
    {
        return _enrEntrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this._enrEntrant = entrant;
    }

    public PgupsEnrForeignEntrant getForeignEntrant()
    {
        return _foreignEntrant;
    }

    public void setForeignEntrant(PgupsEnrForeignEntrant foreignEntrant)
    {
        this._foreignEntrant = foreignEntrant;
    }

    public ISelectModel getViewVisaListModel()
    {
        return viewVisaListModel;
    }

    public void setViewVisaListModel(ISelectModel viewVisaListModel)
    {
        this.viewVisaListModel = viewVisaListModel;
    }
}
