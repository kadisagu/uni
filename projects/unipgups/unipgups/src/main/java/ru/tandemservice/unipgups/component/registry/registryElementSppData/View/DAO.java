/* $Id$ */
package ru.tandemservice.unipgups.component.registry.registryElementSppData.View;

import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.component.registry.registryElementSppData.View.Model;

/**
 * @author Igor Belanov
 * @since 07.02.2017
 */
public class DAO extends ru.tandemservice.unispp.component.registry.registryElementSppData.View.DAO
{
    @Override
    public SppRegElementExt createSppRegElementExt(Model model)
    {
        SppRegElementExt sppRegElementExt = super.createSppRegElementExt(model);
        // не нужно это ПГУПСу, пусть у них по дефолту будет false
        sppRegElementExt.setCheckDuplicates(false);
        return sppRegElementExt;
    }
}
