/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104.Pub;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public class DAO extends SingleEmployeeExtractPubDAO<AdditionalPaymentExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        EmployeeBonus bonus = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "emp").where(eq(property("emp", EmployeeBonus.extract()), value(model.getExtract())))
                .order(property("emp", EmployeeBonus.priority())).top(1).createStatement(getSession()).uniqueResult();
        model.setEmployeeBonus(bonus);
    }
}