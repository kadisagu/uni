/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e100;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 20.08.2015
 */
public class CombinationPostExtractPrint implements IPrintFormCreator<CombinationPostExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, CombinationPostExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder) extract.getParagraph().getOrder());

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        modifier.put("FIO", extract.getEmployee().getFullFio());
        modifier.put("orgUnit", getOrgUnitTitleWithParent(extract.getEntity().getOrgUnit()));
        modifier.put("combinationReason", StringUtils.isBlank(extract.getTextParagraph())? "":extract.getTextParagraph());

        StringBuilder combinationPeriod = new StringBuilder();
        combinationPeriod.append("c ");
        combinationPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        if (extract.getEndDate() != null)
        {
            combinationPeriod.append(" по ");
            combinationPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        }
        modifier.put("combinationPeriod", combinationPeriod.toString());

        String postTitle = extract.getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        modifier.put("combinationPost", null != postTitle ? StringUtils.uncapitalize(postTitle) : StringUtils.uncapitalize(extract.getPostBoundedWithQGandQL().getPost().getTitle()));
        modifier.put("post", StringUtils.uncapitalize(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPostStaffRateExtractRelation.class, "b")
                .distinct()
                .column(property("b", CombinationPostStaffRateExtractRelation.financingSource().title()))
                .where(eq(property("b", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract()), value(extract)));

        modifier.put("financingSource", CommonBaseStringUtil.joinNotEmpty(DataAccessServices.dao().getList(builder), ", "));

        if (extract.getMissingEmployeePost() != null)
        {
            String fioDeclinated = CommonExtractUtil.getModifiedFioInitials(extract.getMissingEmployeePost().getPerson().getIdentityCard(), GrammaCase.GENITIVE);
            modifier.put("missingEmployeePost_G", fioDeclinated);
            modifier.put("combinationOrgUnit", getOrgUnitTitleWithParent(extract.getMissingEmployeePost().getOrgUnit()));
        }
        else
        {
            modifier.put("missingEmployeePost_G", "");
            modifier.put("combinationOrgUnit", "");
        }


        Double staffRate = 0d;
        for (CombinationPostStaffRateExtractRelation relation : MoveEmployeeDaoFacade.getMoveEmployeeDao().getCombinationPostStaffRateExtractRelationList(extract))
            staffRate += relation.getStaffRate();

        modifier.put("procStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate) + " ставки");
        modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getSalary()));
        modifier.put("contractAddAgreementDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getContractAddAgreementDate()));
        modifier.put("contractAddAgreementNumber", extract.getContractAddAgreementNumber());
        modifier.put("contractDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getContractDate()));
        modifier.put("contractNumber", extract.getContractNumber());

        modifier.modify(document);

        return document;
    }

    private String getOrgUnitTitleWithParent(OrgUnit orgUnit)
    {
        StringBuilder builder = new StringBuilder();
        if (orgUnit.getParent() != null)
        {
            builder.append(StringUtils.capitalize(orgUnit.getParent().getOrgUnitType().getTitle())).append(" «").append(orgUnit.getParent().getPrintTitle()).append("», ");
        }
        builder.append(StringUtils.capitalize(orgUnit.getOrgUnitType().getTitle())).append(" «").append(orgUnit.getPrintTitle()).append("»");
        return builder.toString();
    }
}