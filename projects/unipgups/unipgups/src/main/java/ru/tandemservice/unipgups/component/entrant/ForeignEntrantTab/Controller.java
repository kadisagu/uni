/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.ForeignEntrantTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author Alexey Lopatin
 * @since 03.08.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickEditForeignEntrantContractOrg(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit", new ParametersMap().add("foreignEntrantButton", "contractOrg")));
    }

    public void onClickEditForeignEntrantTranscription(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit", new ParametersMap().add("foreignEntrantButton", "transcription")));
    }

    public void onClickEditForeignEntrantVisa(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit", new ParametersMap().add("foreignEntrantButton", "visa")));
    }

    public void onClickEditForeignEntrantAccommodation(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit", new ParametersMap().add("foreignEntrantButton", "accommodation")));
    }

    public void onClickEditForeignEntrantMigrationCard(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit", new ParametersMap().add("foreignEntrantButton", "migrationCard")));
    }

    public void onClickEditForeignEntrantResidencePermit(IBusinessComponent component)
    {
        component.createDefaultChildRegion(new ComponentActivator("ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit", new ParametersMap().add("foreignEntrantButton", "residencePermit")));
    }
}
