/* $Id$ */
package ru.tandemservice.unipgups.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uniedu.migration.MS_uniedu_2x8x2_0to1;

/**
 * @author Nikolay Fedorovskih
 * @since 25.06.2015
 */
public class MS_unipgups_2x8x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.17"),
                new ScriptDependency("org.tandemframework.shared", "1.8.1"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        MS_uniedu_2x8x2_0to1.migrate(tool);
    }
}