/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditController;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public class Controller extends CommonSingleEmployeeExtractAddEditController<ProlongationContractExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(component.getModel());
    }
}