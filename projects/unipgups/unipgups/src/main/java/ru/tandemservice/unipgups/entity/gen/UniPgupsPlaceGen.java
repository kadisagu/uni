package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesRegistryRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Помещение (ПГУПС)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniPgupsPlaceGen extends UniplacesRegistryRecord
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.UniPgupsPlace";
    public static final String ENTITY_NAME = "uniPgupsPlace";
    public static final int VERSION_HASH = 472658897;
    private static IEntityMeta ENTITY_META;

    public static final String P_TITLE = "title";
    public static final String P_NUMBER = "number";
    public static final String L_FLOOR = "floor";

    private String _title;     // Название
    private String _number;     // Номер
    private UniplacesFloor _floor;     // Этаж

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Название. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство не может быть null.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Этаж. Свойство не может быть null.
     */
    @NotNull
    public UniplacesFloor getFloor()
    {
        return _floor;
    }

    /**
     * @param floor Этаж. Свойство не может быть null.
     */
    public void setFloor(UniplacesFloor floor)
    {
        dirty(_floor, floor);
        _floor = floor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UniPgupsPlaceGen)
        {
            setTitle(((UniPgupsPlace)another).getTitle());
            setNumber(((UniPgupsPlace)another).getNumber());
            setFloor(((UniPgupsPlace)another).getFloor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniPgupsPlaceGen> extends UniplacesRegistryRecord.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniPgupsPlace.class;
        }

        public T newInstance()
        {
            return (T) new UniPgupsPlace();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return obj.getTitle();
                case "number":
                    return obj.getNumber();
                case "floor":
                    return obj.getFloor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "floor":
                    obj.setFloor((UniplacesFloor) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                        return true;
                case "number":
                        return true;
                case "floor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return true;
                case "number":
                    return true;
                case "floor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "title":
                    return String.class;
                case "number":
                    return String.class;
                case "floor":
                    return UniplacesFloor.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniPgupsPlace> _dslPath = new Path<UniPgupsPlace>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniPgupsPlace");
    }
            

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPgupsPlace#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Номер.
     * @see ru.tandemservice.unipgups.entity.UniPgupsPlace#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Этаж. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPgupsPlace#getFloor()
     */
    public static UniplacesFloor.Path<UniplacesFloor> floor()
    {
        return _dslPath.floor();
    }

    public static class Path<E extends UniPgupsPlace> extends UniplacesRegistryRecord.Path<E>
    {
        private PropertyPath<String> _title;
        private PropertyPath<String> _number;
        private UniplacesFloor.Path<UniplacesFloor> _floor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Название. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPgupsPlace#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UniPgupsPlaceGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unipgups.entity.UniPgupsPlace#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UniPgupsPlaceGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Этаж. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.UniPgupsPlace#getFloor()
     */
        public UniplacesFloor.Path<UniplacesFloor> floor()
        {
            if(_floor == null )
                _floor = new UniplacesFloor.Path<UniplacesFloor>(L_FLOOR, this);
            return _floor;
        }

        public Class getEntityClass()
        {
            return UniPgupsPlace.class;
        }

        public String getEntityName()
        {
            return "uniPgupsPlace";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
