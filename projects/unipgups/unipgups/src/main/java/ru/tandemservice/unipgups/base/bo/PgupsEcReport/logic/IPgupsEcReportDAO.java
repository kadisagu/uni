/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.IPgupsEcReportVO;

/**
 * @author Nikolay Fedorovskih
 * @since 18.07.2013
 */
public interface IPgupsEcReportDAO extends INeedPersistenceSupport, ICommonDAO
{
    void saveNewReport(IPgupsEcReport report, IPgupsEcReportVO data, IPgupsEcReportBuilder reportBuilder);
}