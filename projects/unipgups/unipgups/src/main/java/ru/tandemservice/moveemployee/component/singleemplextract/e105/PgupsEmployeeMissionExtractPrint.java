/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105;

import org.springframework.util.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class PgupsEmployeeMissionExtractPrint implements IPrintFormCreator<PgupsEmployeeMissionExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, PgupsEmployeeMissionExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder) extract.getParagraph().getOrder());

        EmployeePost employeePost = extract.getEntity();

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        modifier.put("FIO", extract.getEmployee().getFullFio());
        modifier.put("orgUnit", getOrgUnitTitleWithParent(employeePost.getOrgUnit()));

        modifier.put("post", StringUtils.uncapitalize(employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));

        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("endDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        modifier.put("targetText", extract.getPurposeText());
        AddressDetailed factAddress = extract.getExternalOrgUnit().getFactAddress();

        StringBuilder builder = new StringBuilder().append(factAddress!= null? factAddress.getCountry().getTitle() + ", ":"")
                .append(factAddress != null && factAddress.getSettlement() != null? factAddress.getSettlement().getTitle() + ", ": "")
                .append(extract.getExternalOrgUnit().getTitleWithLegalForm());
        modifier.put("port", builder.toString());

        modifier.put("financingSource", extract.isInternalFinancingSource()? "финансируется отправляющей стороной": extract.getFinancingSourceOrgUnit().getTitleWithLegalForm());
        modifier.put("commentText", extract.getCommentText() != null? extract.getCommentText():"");
        modifier.put("Days", String.valueOf(CommonBaseDateUtil.getDifferenceBetweenDatesInDays(extract.getBeginDate(), extract.getEndDate())));

        modifier.modify(document);

        return document;
    }


    private String getOrgUnitTitleWithParent(OrgUnit orgUnit)
    {
        StringBuilder builder = new StringBuilder();
        if (orgUnit.getParent() != null)
        {
            builder.append(StringUtils.capitalize(orgUnit.getParent().getOrgUnitType().getTitle())).append(" «").append(orgUnit.getParent().getPrintTitle()).append("», ");
        }
        builder.append(StringUtils.capitalize(orgUnit.getOrgUnitType().getTitle())).append(" «").append(orgUnit.getPrintTitle()).append("»");
        return builder.toString();
    }
}