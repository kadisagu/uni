/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106.AddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;
import ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 07.09.2015
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<PgupsTransferToDiffStaffRatesExtract>
{
    private ISingleSelectModel _raisingCoefficientListModel; //повышающий коэффициент
    private ISingleSelectModel _etksLevelModel; //
    private ISingleSelectModel _financingSourceItemModel;
    private ISingleSelectModel _financingSourceModel;
    private ISingleSelectModel _contractTypeModel;

    private DynamicListDataSource<EmployeePostStaffRateItem> _employeeStaffRateDataSource; //Ставки сотрудника
    private DynamicListDataSource<StaffRateToPgupsTransfDiffStaffRateExtractRelation> _staffRateDataSource; //Новые ставки сотрудника

    private List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> _staffRateItemList = new ArrayList<>(); //список новых ставок сотрудника
    private List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> _staffRateListForDelete = new ArrayList<>(); //список ставок на удаление

    private boolean _needUpdateDataSource = true;
    private boolean _thereAnyActiveStaffList = false; //признак наличия активного штатного расписания

    private boolean _disabledContractFields = true; //

    private Double _summStaffRateBefore;

    private Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, String> _emptyStaffRateAllocItemMap = new HashMap<>();
    private Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> _emptyStaffRateAllocLongMap = new HashMap<>();

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;
        }

        @Override
        public Location getLocation()
        {
            return null;
        }
    };

    //Getters & Setters

//    public boolean  isDisabledFinSource()
//    {
//        if (getStaffRateDataSource()!= null && getStaffRateDataSource().getCurrentEntity()!= null)
//        {
//            StaffRateToPgupsTransfDiffStaffRateExtractRelation relation = getStaffRateDataSource().getCurrentEntity();
//            EmployeePostStaffRateItem staffRateItem = relation.getEmployeePostStaffRateItem();
//            return staffRateItem!= null && relation.getFinancingSource().equals(staffRateItem.getFinancingSource());
//        }
//        return false;
//    }
//
//    public boolean isDisabledFinSourceItem()
//    {
//        if (!isDisabledFinSource())
//        {
//            StaffRateToPgupsTransfDiffStaffRateExtractRelation relation = getStaffRateDataSource().getCurrentEntity();
//            EmployeePostStaffRateItem staffRateItem = relation.getEmployeePostStaffRateItem();
//            return relation.getFinancingSourceItem() != null && relation.getFinancingSourceItem().equals(staffRateItem.getFinancingSourceItem());
//        }
//        return false;
//    }

    public String getStaffRateColumnId()
    {
        return "staffRate_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcColumnId()
    {
        return "finSrc_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItmColumnId()
    {
        return "finSrcItm_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getEmployeeHRId()
    {
        return "employeeHR_Id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public boolean isDisabledContractFields()
    {
        return _disabledContractFields;
    }

    public void setDisabledContractFields(boolean disabledContractFields)
    {
        _disabledContractFields = disabledContractFields;
    }

    public Double getSummStaffRateBefore()
    {
        return _summStaffRateBefore;
    }

    public void setSummStaffRateBefore(Double summStaffRateBefore)
    {
        _summStaffRateBefore = summStaffRateBefore;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getEmployeeStaffRateDataSource()
    {
        return _employeeStaffRateDataSource;
    }

    public void setEmployeeStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> employeeStaffRateDataSource)
    {
        _employeeStaffRateDataSource = employeeStaffRateDataSource;
    }

    public ISingleSelectModel getContractTypeModel()
    {
        return _contractTypeModel;
    }

    public void setContractTypeModel(ISingleSelectModel contractTypeModel)
    {
        _contractTypeModel = contractTypeModel;
    }

    public boolean isNeedUpdateDataSource()
    {
        return _needUpdateDataSource;
    }

    public void setNeedUpdateDataSource(boolean needUpdateDataSource)
    {
        _needUpdateDataSource = needUpdateDataSource;
    }

    public boolean isThereAnyActiveStaffList()
    {
        return _thereAnyActiveStaffList;
    }

    public void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList)
    {
        _thereAnyActiveStaffList = thereAnyActiveStaffList;
    }

    public ISingleSelectModel getRaisingCoefficientListModel()
    {
        return _raisingCoefficientListModel;
    }

    public void setRaisingCoefficientListModel(ISingleSelectModel raisingCoefficientListModel)
    {
        _raisingCoefficientListModel = raisingCoefficientListModel;
    }

    public ISingleSelectModel getEtksLevelModel()
    {
        return _etksLevelModel;
    }

    public void setEtksLevelModel(ISingleSelectModel etksLevelModel)
    {
        _etksLevelModel = etksLevelModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

    public DynamicListDataSource<StaffRateToPgupsTransfDiffStaffRateExtractRelation> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<StaffRateToPgupsTransfDiffStaffRateExtractRelation> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> getStaffRateItemList()
    {
        return _staffRateItemList;
    }

    public void setStaffRateItemList(List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> staffRateItemList)
    {
        _staffRateItemList = staffRateItemList;
    }

    public List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> getStaffRateListForDelete()
    {
        return _staffRateListForDelete;
    }

    public void setStaffRateListForDelete(List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> staffRateListForDelete)
    {
        _staffRateListForDelete = staffRateListForDelete;
    }

    public Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, String> getEmptyStaffRateAllocItemMap()
    {
        return _emptyStaffRateAllocItemMap;
    }

    public void setEmptyStaffRateAllocItemMap(Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, String> emptyStaffRateAllocItemMap)
    {
        _emptyStaffRateAllocItemMap = emptyStaffRateAllocItemMap;
    }

    public Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> getEmptyStaffRateAllocDoubleMap()
    {
        return _emptyStaffRateAllocLongMap;
    }

    public void setEmptyStaffRateAllocDoubleMap(Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> emptyStaffRateAllocLongMap)
    {
        _emptyStaffRateAllocLongMap = emptyStaffRateAllocLongMap;
    }
}