/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;
import ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation;

import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 07.09.2015
 */
public class PgupsTransferToDiffStaffRatesExtractDAO extends UniBaseDao implements IExtractComponentDao<PgupsTransferToDiffStaffRatesExtract>
{

    @Override
    public void doCommit(PgupsTransferToDiffStaffRatesExtract extract, Map parameters)
    {
        //Должность ПКГиКУ
        PostBoundedWithQGandQL postBoundedWithQGandQL = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL();

        //проверка ставок ШР в момент проведения приказа
        CommonExtractCommitUtil.validateAllocationItem(extract, extract.getEntity().getOrgUnit(), postBoundedWithQGandQL);

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors())
            return;

        //Сохранение текста выписки
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        //Список связей выписки и ставки должности
        DQLSelectBuilder staffRateBuilder = new DQLSelectBuilder().fromEntity(StaffRateToPgupsTransfDiffStaffRateExtractRelation.class, "sr")
                .where(eq(property("sr", StaffRateToPgupsTransfDiffStaffRateExtractRelation.extract()), value(extract)));

        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation relation : this.<StaffRateToPgupsTransfDiffStaffRateExtractRelation>getList(staffRateBuilder))
        {
            EmployeePostStaffRateItem employeePostStaffRateItem = relation.getEmployeePostStaffRateItem();
            relation.setStaffListAllocationItem(null);
            relation.setEmployeePostStaffRateItem(null);
            update(relation);
//            getSession().flush();
            // удаляем старые связи ставки и сотрудника
            if (employeePostStaffRateItem != null)
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getEntity().getOrgUnit(), postBoundedWithQGandQL, employeePostStaffRateItem.getFinancingSource(), employeePostStaffRateItem.getFinancingSourceItem());
                relation.setFinancingSourceItemOld(employeePostStaffRateItem.getFinancingSourceItem());
                relation.setStaffRateOld(employeePostStaffRateItem.getStaffRate());
                relation.setFinancingSourceOld(employeePostStaffRateItem.getFinancingSource());
                update(relation);
                delete(employeePostStaffRateItem);
                new DQLDeleteBuilder(StaffListAllocationItem.class)
                        .where(eq(property(StaffListAllocationItem.employeePost()), value(extract.getEntity())))
                        .where(eq(property(StaffListAllocationItem.staffListItem()), value(staffListItem)))
                        .where(eq(property(StaffListAllocationItem.combination()), value(Boolean.FALSE)))
                        .where(isNull(property(StaffListAllocationItem.combinationPost())))
                        .createStatement(getSession()).execute();
            }

        }

        getSession().flush();

        //создаем новые связи ставки и сотрудника
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation relation : this.<StaffRateToPgupsTransfDiffStaffRateExtractRelation>getList(staffRateBuilder))
        {
            EmployeePostStaffRateItem rateItem = new EmployeePostStaffRateItem();
            rateItem.setEmployeePost(extract.getEntity());
            rateItem.setFinancingSource(relation.getFinancingSource());
            rateItem.setFinancingSourceItem(relation.getFinancingSourceItem());
            rateItem.setStaffRate(relation.getStaffRate());

            save(rateItem);

            relation.setEmployeePostStaffRateItem(rateItem);
            update(relation);
            getSession().flush();

            //Если есть активное штатное расписание
            if (UniempDaoFacade.getStaffListDAO().getActiveStaffList(extract.getEntity().getOrgUnit()) != null)
            {
                // получаем должность штатного расписания
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getEntity().getOrgUnit(), postBoundedWithQGandQL, relation.getFinancingSource(), relation.getFinancingSourceItem());
                //создаем должность штатной расстановки
                StaffListAllocationItem allocItem = new StaffListAllocationItem();
                allocItem.setCombination(false);
                allocItem.setStaffRate(relation.getStaffRate());
                allocItem.setFinancingSource(relation.getFinancingSource());
                allocItem.setFinancingSourceItem(relation.getFinancingSourceItem());
                allocItem.setStaffListItem(staffListItem);
                allocItem.setEmployeePost(extract.getEntity());
                allocItem.setRaisingCoefficient(extract.getSalaryRaisingCoefficient());
                if (allocItem.getRaisingCoefficient() != null)
                    allocItem.setMonthBaseSalaryFund(allocItem.getRaisingCoefficient().getRecommendedSalary() * allocItem.getStaffRate());
                else if (allocItem.getStaffListItem() != null)
                    allocItem.setMonthBaseSalaryFund(allocItem.getStaffListItem().getSalary() * allocItem.getStaffRate());

                save(allocItem);
                relation.setStaffListAllocationItem(allocItem);
                update(relation);
                getSession().flush();
                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocItem, getSession());
            }

        }

        // Трудовой договор и дополнительное соглашение
        MoveEmployeeDaoFacade.getMoveEmployeeDao().createOrUpdateContractAndAgreement(extract, "Перевод сотрудника на иную долю ставки");
        update(extract);

    }

    public void doRollback(PgupsTransferToDiffStaffRatesExtract extract, Map parameters)
    {
        //Если бы создано соглашение к трудовому договору - удаляем его
        if (extract.getCreateCollateralAgreement() != null)
        {
            delete(extract.getCreateCollateralAgreement());
            extract.setCreateCollateralAgreement(null);
        }

        //Если был создан трудовой договор - удаляем
        if (extract.getCreateEmployeeContract() != null)
        {
            delete(extract.getCreateEmployeeContract());
            extract.setCreateEmployeeContract(null);
        }

        update(extract);

        //Должность ПКГиКУ
        PostBoundedWithQGandQL postBoundedWithQGandQL = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL();

        DQLSelectBuilder staffRateBuilder = new DQLSelectBuilder().fromEntity(StaffRateToPgupsTransfDiffStaffRateExtractRelation.class, "sr");
        staffRateBuilder.where(eq(property("sr", StaffRateToPgupsTransfDiffStaffRateExtractRelation.extract()), value(extract)));

        //Для связей выписки со ставкой должности
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation relation : this.<StaffRateToPgupsTransfDiffStaffRateExtractRelation>getList(staffRateBuilder))
        {
            StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getEntity().getOrgUnit(), postBoundedWithQGandQL, relation.getFinancingSource(), relation.getFinancingSourceItem());

            //удаляем должность штатной расстановки, созданную проведенной выпиской
            if (relation.getStaffListAllocationItem() != null)
                delete(relation.getStaffListAllocationItem());
            relation.setStaffListAllocationItem(null);
            update(relation);

            if (UniempDaoFacade.getStaffListDAO().getActiveStaffList(extract.getEntity().getOrgUnit()) != null && relation.getStaffRateOld()!= null &&  relation.getStaffRateOld() > 0)
            {
                    //восстанавливаем старую должность штатной расстановки
                    StaffListAllocationItem allocationItem = new StaffListAllocationItem();

                    allocationItem.setStaffRate(relation.getStaffRateOld());
                    allocationItem.setFinancingSource(relation.getFinancingSourceOld());
                    allocationItem.setFinancingSourceItem(relation.getFinancingSourceItem());
                    allocationItem.setStaffListItem(staffListItem);
                    allocationItem.setEmployeePost(extract.getEntity());

                    if (allocationItem.getEmployeePost() != null)
                        allocationItem.setRaisingCoefficient(relation.getExtract().getEntity().getRaisingCoefficient());
                    if (allocationItem.getRaisingCoefficient() != null)
                        allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
                    else if (allocationItem.getStaffListItem() != null)
                        allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());

                    save(allocationItem);
                    StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());
            }

            //удаляем связь ставки и сотрудника
            if (relation.getEmployeePostStaffRateItem() != null)
            {
                delete(relation.getEmployeePostStaffRateItem());
                relation.setEmployeePostStaffRateItem(null);
                update(relation);
            }
        }

        getSession().flush();

        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation relation : this.<StaffRateToPgupsTransfDiffStaffRateExtractRelation>getList(staffRateBuilder))
        {
            // восстанавливаем старые связи ставки и сотрудника
            if (relation.getEmployeePostStaffRateItem() == null && relation.getFinancingSourceOld() != null)
            {
                EmployeePostStaffRateItem staffItem = new EmployeePostStaffRateItem(extract.getEntity(), relation.getFinancingSourceOld());
                staffItem.setStaffRate(relation.getStaffRateOld());
                staffItem.setFinancingSourceItem(relation.getFinancingSourceItemOld());
                save(staffItem);
                relation.setEmployeePostStaffRateItem(staffItem);
            }

            update(relation);
        }

    }
}