package ru.tandemservice.unipgups.entity;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unipgups.entity.gen.*;

/** @see ru.tandemservice.unipgups.entity.gen.PgupsEmployeePostExtGen */
public class PgupsEmployeePostExt extends PgupsEmployeePostExtGen
{
    public PgupsEmployeePostExt(EmployeePost employeePost)
    {
        setEmployeePost(employeePost);
    }

    public PgupsEmployeePostExt()
    {
    }
}