/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e100.Pub;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 20.08.2015
 */
public class DAO extends SingleEmployeeExtractPubDAO<CombinationPostExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        StringBuilder staffRateBuilder = new StringBuilder();
        List<CombinationPostStaffRateExtractRelation> relationList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getCombinationPostStaffRateExtractRelationList(model.getExtract());
        if (relationList.size() > 0)
        {
            Double value = 0d;
            for (CombinationPostStaffRateExtractRelation relation : relationList)
                value += relation.getStaffRate();

            staffRateBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value));
            staffRateBuilder.append("<p/>");

            for (CombinationPostStaffRateExtractRelation relation : relationList)
            {
                staffRateBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(relation.getStaffRate()));
                staffRateBuilder.append(" - ");
                staffRateBuilder.append(relation.getFinancingSource().getTitle());
                if (relation.getFinancingSourceItem() != null)
                    staffRateBuilder.append(" (").append(relation.getFinancingSourceItem().getTitle()).append(")");

                staffRateBuilder.append("<p/>");
            }
        }

        model.setStaffRateStr(staffRateBuilder.toString());
    }
}