/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.AddEdit;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<CancelCombinationPostExtract>
{
    DynamicListDataSource _employeeStaffRateDataSource;
    DynamicListDataSource _combinationStaffRateDataSource;

    ISingleSelectModel _combinationPostModel;

    List<CombinationPostStaffRateItem> _combinationPostStaffRateItemList = new ArrayList<>();

    //Getters & Setters

    public List<CombinationPostStaffRateItem> getCombinationPostStaffRateItemList()
    {
        return _combinationPostStaffRateItemList;
    }

    public void setCombinationPostStaffRateItemList(List<CombinationPostStaffRateItem> combinationPostStaffRateItemList)
    {
        _combinationPostStaffRateItemList = combinationPostStaffRateItemList;
    }

    public DynamicListDataSource getEmployeeStaffRateDataSource()
    {
        return _employeeStaffRateDataSource;
    }

    public void setEmployeeStaffRateDataSource(DynamicListDataSource employeeStaffRateDataSource)
    {
        _employeeStaffRateDataSource = employeeStaffRateDataSource;
    }

    public DynamicListDataSource getCombinationStaffRateDataSource()
    {
        return _combinationStaffRateDataSource;
    }

    public void setCombinationStaffRateDataSource(DynamicListDataSource combinationStaffRateDataSource)
    {
        _combinationStaffRateDataSource = combinationStaffRateDataSource;
    }

    public ISingleSelectModel getCombinationPostModel()
    {
        return _combinationPostModel;
    }

    public void setCombinationPostModel(ISingleSelectModel combinationPostModel)
    {
        _combinationPostModel = combinationPostModel;
    }
}