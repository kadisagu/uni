/* $Id$ */
package ru.tandemservice.unipgups.component.catalog.pgupsViewVisa.PgupsViewVisaAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.IDefaultCatalogAddEditDAO;
import ru.tandemservice.unipgups.entity.catalog.PgupsViewVisa;

/**
 * @author Lopatin
 * @since 06.08.2013
 */
public interface IDAO extends IDefaultCatalogAddEditDAO<PgupsViewVisa, Model>
{
}