/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 18.07.2013
 */
public interface IPgupsEcReport extends IStorableReport
{
    public static final String SUMMARY_ENROLLMENT_REPORT_CODE = "pgupsSummaryEnrollmentReport";

    void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign);

    void setDateFrom(Date dateFrom);

    void setDateTo(Date dateTo);

    void setFormingDate(Date formingDate);

    void setContent(DatabaseFile content);
}