/* $Id$ */
package ru.tandemservice.unipgups.component.place.PlaceAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
@Input({@Bind(key = "placeId", binding = "place.id"),
        @Bind(key = "floorId"),
        @Bind(key = "placePgupsId", binding = "placePgupsId")
})
public class Model extends ru.tandemservice.uniplaces.component.place.PlaceAddEdit.Model
{
    private UniPlacesPlaceExt _placeExt;
    private ISelectModel _personModel;
    private ISelectModel _placesModel;
    private UniPgupsPlace _placePups;
    private EmployeePost _responsiblePerson;
    private ISelectModel _buildingModel;
    private ISelectModel _unitModel;
    private ISelectModel _floorModel;

    private UniplacesFloor _floor;
    private UniplacesUnit _unit;
    private UniplacesBuilding _building;
    private Long _placePgupsId;


    public EmployeePost getResponsiblePerson()
    {
        return _responsiblePerson;
    }

    public void setResponsiblePerson(EmployeePost responsiblePerson)
    {
        _responsiblePerson = responsiblePerson;
    }

    public UniPgupsPlace getPlacePups()
    {
        return _placePups;
    }

    public void setPlacePups(UniPgupsPlace placePups)
    {
        _placePups = placePups;
    }

    public ISelectModel getPlacesModel()
    {
        return _placesModel;
    }

    public void setPlacesModel(ISelectModel placesModel)
    {
        _placesModel = placesModel;
    }

    public ISelectModel getPersonModel()
    {
        return _personModel;
    }

    public void setPersonModel(ISelectModel personModel)
    {
        _personModel = personModel;
    }

    public UniPlacesPlaceExt getPlaceExt()
    {
        return _placeExt;
    }

    public void setPlaceExt(UniPlacesPlaceExt placeExt)
    {
        _placeExt = placeExt;
    }

    public ISelectModel getBuildingModel()
    {
        return _buildingModel;
    }

    public void setBuildingModel(ISelectModel buildingModel)
    {
        _buildingModel = buildingModel;
    }

    public ISelectModel getUnitModel()
    {
        return _unitModel;
    }

    public void setUnitModel(ISelectModel unitModel)
    {
        _unitModel = unitModel;
    }

    public ISelectModel getFloorModel()
    {
        return _floorModel;
    }

    public void setFloorModel(ISelectModel floorModel)
    {
        _floorModel = floorModel;
    }

    public UniplacesFloor getFloor()
    {
        return _floor;
    }

    public void setFloor(UniplacesFloor floor)
    {
        _floor = floor;
    }

    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        _unit = unit;
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public Long getPlacePgupsId()
    {
        return _placePgupsId;
    }

    public void setPlacePgupsId(Long placePgupsId)
    {
        _placePgupsId = placePgupsId;
    }

    public boolean isDisabledChange()
    {
        return getFloorId() != null || _placePgupsId != null || isEditForm();
    }
}