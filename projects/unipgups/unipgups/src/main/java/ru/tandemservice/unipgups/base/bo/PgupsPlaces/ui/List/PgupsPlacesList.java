/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.PgupsPlacesManager;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.Pub.PgupsPlacesPub;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
@Configuration
public class PgupsPlacesList extends BusinessComponentManager
{
    public static final String PLACES_DS = "placesSearchListDS";
    public static final String BUILDINGS_DS ="buildingDS";
    public static final String UNITS_DS ="unitDS";
    public static final String FLOORS_DS ="floorDS";



    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PLACES_DS, placesDSColumn(), placesDSHandler()))
                .addDataSource(selectDS(BUILDINGS_DS, buildingDSHandler()))
                .addDataSource(selectDS(UNITS_DS, unitsDSHandler()))
                .addDataSource(selectDS(FLOORS_DS, floorDSHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint placesDSColumn()
    {
        return columnListExtPointBuilder(PLACES_DS)
                .addColumn(publisherColumn("name", UniPgupsPlace.title()).businessComponent(PgupsPlacesPub.class).order())
                .addColumn(textColumn("number", UniPgupsPlace.number()))
                .addColumn(textColumn("floor", UniPgupsPlace.floor().title()))
                .addColumn(textColumn("unit", UniPgupsPlace.floor().unit().title()))
                .addColumn(textColumn("building", UniPgupsPlace.floor().unit().building().title()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onEditPlaceClick").permissionKey("pgupsPlaceEdit"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onDeletePlaceClick")
                                   .alert("message:placesSearchListDS.delete.alert")
                                   .permissionKey("pgupsPlaceDelete"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> placesDSHandler() {
        return PgupsPlacesManager.instance().createPlacesDataSource();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> buildingDSHandler()
    {
        return PgupsPlacesManager.instance().createBuildingHandler();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> floorDSHandler()
    {
        return PgupsPlacesManager.instance().createFloorHandler();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> unitsDSHandler()
    {
        return PgupsPlacesManager.instance().createUnitHandler();
    }
}