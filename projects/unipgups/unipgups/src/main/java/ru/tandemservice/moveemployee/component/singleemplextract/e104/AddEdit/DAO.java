/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.SimpleListResultBuilder;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

import java.util.ArrayList;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<AdditionalPaymentExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    @Override
    protected AdditionalPaymentExtract createNewInstance()
    {
        return new AdditionalPaymentExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());
        model.setLabourContract(UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost()));

        model.setFinancingSourceModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSource.class, "fs")
                        .where(like(property("fs", FinancingSource.title()), value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(eq(property("fs", FinancingSource.id()), value((Long) o)));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                FinancingSource financingSource = model.getEmployeeBonus().getFinancingSource();

                if (financingSource == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceItem.class, "fsi");
                builder.where(eq(property("fsi", FinancingSourceItem.financingSource().id()), value(financingSource.getId())));
                builder.where(like(property("fsi", FinancingSourceItem.title()), value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(eq(property("fsi", FinancingSourceItem.id()), value((Long) o)));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setPaymentModel(CommonEmployeeExtractUtil.getPaymentModelForPaymentBlock());


        createOrAssignEmployeeBonus(model);
    }

    private void createOrAssignEmployeeBonus(Model model)
    {
        if (model.isEditForm())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "emp")
                    .top(1)
                    .where(eq(property("emp", EmployeeBonus.extract()), value(model.getExtract())))
                    .order(property("emp", EmployeeBonus.priority()));
            model.setEmployeeBonus(builder.createStatement(getSession()).uniqueResult());
        }

        if (model.getEmployeeBonus() == null)
        {
            EmployeeBonus item = new EmployeeBonus();
            short discriminator = EntityRuntime.getMeta(EmployeeBonus.class).getEntityCode();
            long id = EntityIDGenerator.generateNewId(discriminator);

            item.setId(id);
            item.setPriority(0);
            model.setEmployeeBonus(item);
        }
    }


    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (model.getEmployeeBonus().getEndDate() != null && !model.getEmployeeBonus().getBeginDate().before(model.getEmployeeBonus().getEndDate()))
            errors.add("Дата окончания действия выплаты должна быть позже даты начала.", "startDate");
    }

    @Override
    public void update(Model model)
    {
        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());
         super.update(model);

        if (model.isEditForm())
        {
            for (EmployeeBonus bonus : getList(EmployeeBonus.class, EmployeeBonus.extract(), model.getExtract()))
                delete(bonus);
        }

        model.getEmployeeBonus().setExtract(model.getExtract());
        save(model.getEmployeeBonus());
    }
}