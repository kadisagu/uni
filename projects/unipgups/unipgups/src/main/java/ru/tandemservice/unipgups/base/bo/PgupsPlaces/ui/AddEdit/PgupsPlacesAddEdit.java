/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.PgupsPlacesManager;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
@Configuration
public class PgupsPlacesAddEdit extends BusinessComponentManager
{
    public static final String BUILDINGS_DS ="buildingDS";
    public static final String UNITS_DS ="unitDS";
    public static final String FLOORS_DS ="floorDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(BUILDINGS_DS, PgupsPlacesManager.instance().createBuildingHandler()))
                .addDataSource(selectDS(UNITS_DS, PgupsPlacesManager.instance().createUnitHandler()))
                .addDataSource(selectDS(FLOORS_DS, PgupsPlacesManager.instance().createFloorHandler()))
                .create();
    }

}