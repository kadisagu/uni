package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.AbstractEmployeeExtract;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMission;
import ru.tandemservice.unipgups.entity.PgupsMissionToExtractRelation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь выписки с командировкой сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsMissionToExtractRelationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.PgupsMissionToExtractRelation";
    public static final String ENTITY_NAME = "pgupsMissionToExtractRelation";
    public static final int VERSION_HASH = 1662611315;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT = "extract";
    public static final String L_MISSION = "mission";

    private AbstractEmployeeExtract _extract;     // Выписка
    private PgupsEmployeeMission _mission;     // Командировка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractEmployeeExtract getExtract()
    {
        return _extract;
    }

    /**
     * @param extract Выписка. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtract(AbstractEmployeeExtract extract)
    {
        dirty(_extract, extract);
        _extract = extract;
    }

    /**
     * @return Командировка. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PgupsEmployeeMission getMission()
    {
        return _mission;
    }

    /**
     * @param mission Командировка. Свойство не может быть null и должно быть уникальным.
     */
    public void setMission(PgupsEmployeeMission mission)
    {
        dirty(_mission, mission);
        _mission = mission;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PgupsMissionToExtractRelationGen)
        {
            setExtract(((PgupsMissionToExtractRelation)another).getExtract());
            setMission(((PgupsMissionToExtractRelation)another).getMission());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsMissionToExtractRelationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsMissionToExtractRelation.class;
        }

        public T newInstance()
        {
            return (T) new PgupsMissionToExtractRelation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extract":
                    return obj.getExtract();
                case "mission":
                    return obj.getMission();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extract":
                    obj.setExtract((AbstractEmployeeExtract) value);
                    return;
                case "mission":
                    obj.setMission((PgupsEmployeeMission) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extract":
                        return true;
                case "mission":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extract":
                    return true;
                case "mission":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extract":
                    return AbstractEmployeeExtract.class;
                case "mission":
                    return PgupsEmployeeMission.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsMissionToExtractRelation> _dslPath = new Path<PgupsMissionToExtractRelation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsMissionToExtractRelation");
    }
            

    /**
     * @return Выписка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.PgupsMissionToExtractRelation#getExtract()
     */
    public static AbstractEmployeeExtract.Path<AbstractEmployeeExtract> extract()
    {
        return _dslPath.extract();
    }

    /**
     * @return Командировка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.PgupsMissionToExtractRelation#getMission()
     */
    public static PgupsEmployeeMission.Path<PgupsEmployeeMission> mission()
    {
        return _dslPath.mission();
    }

    public static class Path<E extends PgupsMissionToExtractRelation> extends EntityPath<E>
    {
        private AbstractEmployeeExtract.Path<AbstractEmployeeExtract> _extract;
        private PgupsEmployeeMission.Path<PgupsEmployeeMission> _mission;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.PgupsMissionToExtractRelation#getExtract()
     */
        public AbstractEmployeeExtract.Path<AbstractEmployeeExtract> extract()
        {
            if(_extract == null )
                _extract = new AbstractEmployeeExtract.Path<AbstractEmployeeExtract>(L_EXTRACT, this);
            return _extract;
        }

    /**
     * @return Командировка. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.PgupsMissionToExtractRelation#getMission()
     */
        public PgupsEmployeeMission.Path<PgupsEmployeeMission> mission()
        {
            if(_mission == null )
                _mission = new PgupsEmployeeMission.Path<PgupsEmployeeMission>(L_MISSION, this);
            return _mission;
        }

        public Class getEntityClass()
        {
            return PgupsMissionToExtractRelation.class;
        }

        public String getEntityName()
        {
            return "pgupsMissionToExtractRelation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
