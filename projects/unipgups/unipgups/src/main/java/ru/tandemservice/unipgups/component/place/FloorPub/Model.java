/* $Id$ */
package ru.tandemservice.unipgups.component.place.FloorPub;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public class Model extends ru.tandemservice.uniplaces.component.place.FloorPub.Model
{
    private DynamicListDataSource<UniPgupsPlace> _dataSourcePlace;

    public DynamicListDataSource<UniPgupsPlace> getDataSourcePlace()
    {
        return _dataSourcePlace;
    }

    public void setDataSourcePlace(DynamicListDataSource<UniPgupsPlace> dataSourcePlace)
    {
        _dataSourcePlace = dataSourcePlace;
    }
}