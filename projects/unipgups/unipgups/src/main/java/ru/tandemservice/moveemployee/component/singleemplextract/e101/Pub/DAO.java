/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.Pub;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class DAO extends SingleEmployeeExtractPubDAO<CancelCombinationPostExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        StringBuilder staffRateBuilder = new StringBuilder();
        List<CombinationPostStaffRateItem> relationList = UniempDaoFacade.getUniempDAO().getCombinationPostStaffRateItemList(model.getExtract().getCombinationPost());
        if (relationList.size() > 0)
        {
            Double value = 0d;
            for (CombinationPostStaffRateItem relation : relationList)
                value += relation.getStaffRate();

            staffRateBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value));
            staffRateBuilder.append("<p/>");

            for (CombinationPostStaffRateItem relation : relationList)
            {
                staffRateBuilder.append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(relation.getStaffRate()));
                staffRateBuilder.append(" - ");
                staffRateBuilder.append(relation.getFinancingSource().getTitle());
                if (relation.getFinancingSourceItem() != null)
                    staffRateBuilder.append(" (" + relation.getFinancingSourceItem().getTitle() + ")");

                staffRateBuilder.append("<p/>");
            }
        }

        model.setStaffRateStr(staffRateBuilder.toString());
    }
}