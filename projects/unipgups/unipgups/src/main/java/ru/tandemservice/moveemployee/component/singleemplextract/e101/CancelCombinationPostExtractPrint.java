/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.commons.CommonExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class CancelCombinationPostExtractPrint implements IPrintFormCreator<CancelCombinationPostExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, CancelCombinationPostExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder) extract.getParagraph().getOrder());

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        modifier.put("FIO", extract.getEmployee().getFullFio());
        modifier.put("orgUnit", getOrgUnitTitleWithParent(extract.getEntity().getOrgUnit()));
        modifier.put("combinationReason", StringUtils.isBlank(extract.getTextParagraph()) ? "" : extract.getTextParagraph());

        modifier.put("removalDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getRemovalDate()));

        String postTitle = extract.getCombinationPost().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        modifier.put("combinationPost", null != postTitle ? StringUtils.uncapitalize(postTitle) : StringUtils.uncapitalize(extract.getCombinationPost().getPostBoundedWithQGandQL().getPost().getTitle()));
        modifier.put("post", StringUtils.uncapitalize(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPostStaffRateExtractRelation.class, "b")
                .distinct()
                .column(property("b", CombinationPostStaffRateExtractRelation.financingSource().title()))
                .where(eq(property("b", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract()), value(extract)));

        modifier.put("financingSource", CommonBaseStringUtil.joinNotEmpty(DataAccessServices.dao().getList(builder), ", "));

        if (extract.getCombinationPost().getMissingEmployeePost() != null)
        {
            String fioDeclinated = CommonExtractUtil.getModifiedFioInitials(extract.getCombinationPost().getMissingEmployeePost().getPerson().getIdentityCard(), GrammaCase.GENITIVE);
            modifier.put("missingEmployeePost_G", fioDeclinated);
            modifier.put("combinationOrgUnit", getOrgUnitTitleWithParent(extract.getCombinationPost().getMissingEmployeePost().getOrgUnit()));
        }
        else
        {
            modifier.put("missingEmployeePost_G", "");
            modifier.put("combinationOrgUnit", "");
        }


        List<CombinationPostStaffRateItem> combinationPostStaffRateItemList = UniempDaoFacade.getUniempDAO().getCombinationPostStaffRateItemList(extract.getCombinationPost());
        Double staffRate = 0d;
        for (CombinationPostStaffRateItem item :combinationPostStaffRateItemList)
            staffRate += item.getStaffRate();
        modifier.put("procStaffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRate) + " ставки");

        if (extract.getCombinationPost().getSalaryRaisingCoefficient() != null)
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCombinationPost().getSalaryRaisingCoefficient().getRecommendedSalary()));
        else
            modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCombinationPost().getPostBoundedWithQGandQL().getSalary()));

        modifier.modify(document);

        return document;
    }

    private String getOrgUnitTitleWithParent(OrgUnit orgUnit)
    {
        StringBuilder builder = new StringBuilder();
        if (orgUnit.getParent() != null)
        {
            builder.append(StringUtils.capitalize(orgUnit.getParent().getOrgUnitType().getTitle())).append(" «").append(orgUnit.getParent().getPrintTitle()).append("», ");
        }
        builder.append(StringUtils.capitalize(orgUnit.getOrgUnitType().getTitle())).append(" «").append(orgUnit.getPrintTitle()).append("»");
        return builder.toString();
    }
}