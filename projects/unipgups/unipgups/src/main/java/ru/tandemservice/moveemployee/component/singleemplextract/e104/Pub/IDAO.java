/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.ISingleEmployeeExtractPubDAO;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public interface IDAO extends ISingleEmployeeExtractPubDAO<AdditionalPaymentExtract, Model>
{
}