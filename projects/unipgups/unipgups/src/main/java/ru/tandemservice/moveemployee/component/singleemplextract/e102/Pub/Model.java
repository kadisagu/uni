/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubModel;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public class Model extends SingleEmployeeExtractPubModel<ProlongationContractExtract>
{
    String labourContractTitle;

    public String getLabourContractTitle()
    {
        return labourContractTitle;
    }

    public void setLabourContractTitle(String labourContractTitle)
    {
        this.labourContractTitle = labourContractTitle;
    }
}