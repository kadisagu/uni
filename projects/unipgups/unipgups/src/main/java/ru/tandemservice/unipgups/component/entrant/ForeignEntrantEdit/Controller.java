/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

/**
 * @author Alexey Lopatin
 * @since 03.08.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent component)
    {
        getDao().update(getModel(component));
        deactivate(component);
    }
}
