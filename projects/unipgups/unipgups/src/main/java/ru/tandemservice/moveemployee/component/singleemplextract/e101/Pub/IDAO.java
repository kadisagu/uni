/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.ISingleEmployeeExtractPubDAO;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public interface IDAO extends ISingleEmployeeExtractPubDAO<CancelCombinationPostExtract, Model>
{
}