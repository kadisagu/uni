package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipgups_2x9x0_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность pgupsEmployeePostExt

		// создана новая сущность
		if (!tool.tableExists("pgupsemployeepostext_t")){
			// создать таблицу
			DBTable dbt = new DBTable("pgupsemployeepostext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_pgupsemployeepostext"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("employeepost_id", DBType.LONG).setNullable(false), 
				new DBColumn("naturework_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("pgupsEmployeePostExt");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность pgupsTransferToDiffStaffRatesExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("pgpstrnsfrtdffstffrtsextrct_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_1a215ea2"),
									  new DBColumn("staffratebefore_p", DBType.DOUBLE),
									  new DBColumn("staffrateafter_p", DBType.DOUBLE).setNullable(false),
									  new DBColumn("salaryraisingcoefficient_id", DBType.LONG),
									  new DBColumn("etkslevels_id", DBType.LONG),
									  new DBColumn("salary_p", DBType.DOUBLE).setNullable(false),
									  new DBColumn("contracttype_id", DBType.LONG).setNullable(false),
									  new DBColumn("contractnumber_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("contractdate_p", DBType.DATE).setNullable(false),
									  new DBColumn("contractbegindate_p", DBType.DATE).setNullable(false),
									  new DBColumn("contractenddate_p", DBType.DATE),
									  new DBColumn("contractaddagreementnumber_p", DBType.createVarchar(255)).setNullable(false),
									  new DBColumn("contractaddagreementdate_p", DBType.DATE).setNullable(false),
									  new DBColumn("begindate_p", DBType.DATE).setNullable(false),
									  new DBColumn("createemployeecontract_id", DBType.LONG),
									  new DBColumn("createcollateralagreement_id", DBType.LONG),
									  new DBColumn("characterwork_p", DBType.createVarchar(255)),
									  new DBColumn("freelance_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("pgupsTransferToDiffStaffRatesExtract");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность staffRateToPgupsTransfDiffStaffRateExtractRelation

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("stffrttpgpstrnsfdffstffrtext_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_74cde166"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("extract_id", DBType.LONG).setNullable(false),
									  new DBColumn("employeepoststaffrateitem_id", DBType.LONG),
									  new DBColumn("financingsource_id", DBType.LONG).setNullable(false),
									  new DBColumn("financingsourceitem_id", DBType.LONG),
									  new DBColumn("stafflistallocationitem_id", DBType.LONG),
									  new DBColumn("staffrate_p", DBType.DOUBLE).setNullable(false),
									  new DBColumn("financingsourceold_id", DBType.LONG),
									  new DBColumn("financingsourceitemold_id", DBType.LONG),
									  new DBColumn("staffrateold_p", DBType.DOUBLE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("staffRateToPgupsTransfDiffStaffRateExtractRelation");

		}




	}
}