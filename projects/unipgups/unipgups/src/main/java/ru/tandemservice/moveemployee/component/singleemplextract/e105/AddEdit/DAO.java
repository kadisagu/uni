/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;


/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<PgupsEmployeeMissionExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    @Override
    protected PgupsEmployeeMissionExtract createNewInstance()
    {
        return new PgupsEmployeeMissionExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());
        model.setExternalOrgUnitModel(new LazySimpleSelectModel<>(ExternalOrgUnit.class, ExternalOrgUnit.P_TITLE_WITH_LEGAL_FORM));
        if (model.isEditForm() && model.getExtract().getExternalOrgUnit().getFactAddress() != null && model.getExtract().getExternalOrgUnit().getFactAddress().getSettlement() != null)
            model.setCityOrgUnit(model.getExtract().getExternalOrgUnit().getFactAddress().getSettlement().getTitle());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (model.getExtract().getEndDate() != null && !model.getExtract().getBeginDate().before(model.getExtract().getEndDate()))
            errors.add("Дата окончания командировки должна быть позже даты начала.", "missionBeginDate");
    }

    @Override
    public void update(Model model)
    {
        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        if (model.getExtract().isInternalFinancingSource())
            model.getExtract().setFinancingSourceOrgUnit(null);
        super.update(model);

    }
}