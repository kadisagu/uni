/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class CancelCombinationPostExtractDAO extends UniBaseDao implements IExtractComponentDao<CancelCombinationPostExtract>
{
    public void doCommit(CancelCombinationPostExtract extract, Map parameters)
    {
        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        if (extract.getCombinationPost().getEndDate() != null)
        {
            if (extract.getRemovalDate().after(extract.getCombinationPost().getEndDate()))
            {
                extract.setEndDateOld(null);
            }
            else
            {
                extract.setEndDateOld(extract.getCombinationPost().getEndDate());
                extract.getCombinationPost().setEndDate(extract.getRemovalDate());
            }
        }
        else
        {
            extract.setEndDateOld(null);
            extract.getCombinationPost().setEndDate(extract.getRemovalDate());
        }

        update(extract.getCombinationPost());

        StaffList activaStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(extract.getCombinationPost().getOrgUnit());

        if (activaStaffList != null)
        {
            extract.setChangeStaffList(true);
            extract.setChangedStaffList(activaStaffList);

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListAllocationItem.class, "b")
                .where(eq(property("b", StaffListAllocationItem.staffListItem().staffList()), value(activaStaffList)))
                .where(eq(property("b", StaffListAllocationItem.combination()), value(true)))
                .where(eq(property("b", StaffListAllocationItem.combinationPost()), value(extract.getCombinationPost())));

            for (StaffListAllocationItem item : this.<StaffListAllocationItem>getList(builder))
            {
                if (item.getEmployeePost() != null && item.getEmployeePost().equals(extract.getEntity()))
                {
                    item.setEmployeePost(null);
                    update(item);
                }
            }
        }
        else
        {
            extract.setChangeStaffList(false);
            extract.setChangedStaffList(null);
        }

        update(extract);
    }

    public void doRollback(CancelCombinationPostExtract extract, Map parameters)
    {
            extract.getCombinationPost().setEndDate(extract.getEndDateOld());
            extract.setEndDateOld(null);
            update(extract.getCombinationPost());

        if (extract.getChangeStaffList())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListAllocationItem.class, "b")
                .where(eq(property("b", StaffListAllocationItem.staffListItem().staffList()), value(extract.getChangedStaffList())))
                .where(eq(property("b", StaffListAllocationItem.combination()), value(true)))
                .where(eq(property("b", StaffListAllocationItem.combinationPost()), value(extract.getCombinationPost())));

            for (StaffListAllocationItem item : this.<StaffListAllocationItem>getList(builder))
            {
                if (item.getEmployeePost() == null)
                {
                    item.setEmployeePost(extract.getEntity());
                    update(item);
                }
            }
        }
    }
}
