package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipgups_2x9x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность prolongationContractExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("prolongationcontractextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_prolongationcontractextract"), 
				new DBColumn("labourcontract_id", DBType.LONG).setNullable(false), 
				new DBColumn("newenddate_p", DBType.DATE).setNullable(false), 
				new DBColumn("oldenddate_p", DBType.DATE), 
				new DBColumn("contractaddagreementnumber_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("contractaddagreementdate_p", DBType.DATE).setNullable(false), 
				new DBColumn("createcollateralagreement_id", DBType.LONG)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("prolongationContractExtract");

		}


    }
}