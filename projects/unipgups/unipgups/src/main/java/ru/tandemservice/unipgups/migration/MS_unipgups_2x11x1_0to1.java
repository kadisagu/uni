/* $Id$ */
package ru.tandemservice.unipgups.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLInsertQuery;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.unispp.migration.MS_unispp_2x11x1_6to7;

/**
 * @author Igor Belanov
 * @since 09.02.2017
 */
@SuppressWarnings({"unused", "deprecation", "Duplicates"})
public class MS_unipgups_2x11x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.11.1")
                };
    }

    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[]
                {
                        MigrationUtils.createScriptDependency(MS_unispp_2x11x1_6to7.class)
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        final String funcPrefix = tool.getDataSource().getSqlFunctionPrefix();
        final ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        ////////////////////////////////////////////////////////////////////////////////
        // сущность sppRegElementExt

        // гарантировать наличие кода сущности
        short sppRegElementExtCode = tool.entityCodes().ensure("sppRegElementExt");

        // свойство checkDuplicates в этом проекте должно быть false у всех элементов реестра
        // но не у всех элементов реестра есть сущность, в которой это поле хранится
        {
            // сбросим у тех, которые в базе есть
            tool.executeUpdate("update spp_reg_element_ext set checkduplicates_p=?", false);

            // выберем элементы реестра, к которым не прицеплена таблица с данными по расписаниям
            SQLSelectQuery select = new SQLSelectQuery();
            select.column("el.id id");
            select.column("? + row_number() over (order by el.id) tm_rn"); // это для генератора id (время + номер строки (чтобы не было дублей))
            select.from(SQLFrom.table("epp_reg_base_t", "el")
                    .leftJoin(SQLFrom.table("spp_reg_element_ext", "elExt"), "elExt.registryelement_id=el.id"));
            select.where("elExt.id is null");

            // вставка элементов сущности "Доп.характеристика элемента реестра"
            SQLInsertQuery ins = new SQLInsertQuery("spp_reg_element_ext")
                    .set("id", funcPrefix + "createIdFromNumber(el.tm_rn, " + String.valueOf(sppRegElementExtCode) + ")")
                    .set("discriminator", String.valueOf(sppRegElementExtCode))
                    .set("registryelement_id", "el.id")
                    .set("checkplacedates_p", "false")
                    .set("checkduplicates_p", "false")
                    .from(SQLFrom.select(translator.toSql(select), "el"));

            tool.executeUpdate(translator.toSql(ins), System.currentTimeMillis());
        }
    }
}
