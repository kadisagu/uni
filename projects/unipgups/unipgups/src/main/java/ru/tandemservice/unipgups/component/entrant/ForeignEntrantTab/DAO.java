/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.ForeignEntrantTab;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unipgups.entity.entrant.PgupsForeignEntrant;

/**
 * @author Alexey Lopatin
 * @since 03.08.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    public void prepare(Model model)
    {
        model.setEntrant((Entrant) getNotNull(Entrant.class, model.getEntrant().getId()));
        PgupsForeignEntrant foreignEntrant = get(PgupsForeignEntrant.class, PgupsForeignEntrant.entrant().s(), model.getEntrant());

        if (foreignEntrant != null)
            model.setForeignEntrant(foreignEntrant);
    }
}
