/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104;

import org.springframework.util.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public class AdditionalPaymentExtractPrint implements IPrintFormCreator<AdditionalPaymentExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AdditionalPaymentExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder) extract.getParagraph().getOrder());

        EmployeePost employeePost = extract.getEntity();

        List<EmployeeBonus> bonusList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(EmployeeBonus.class, "emp")
                                                                                 .where(eq(property(EmployeeBonus.extract().fromAlias("emp")), value(extract)))
                                                                                 .top(1).order(property(EmployeeBonus.priority().fromAlias("emp"))));
        EmployeeBonus employeeBonus = bonusList.isEmpty()? null : bonusList.get(0);

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        modifier.put("FIO", extract.getEmployee().getFullFio());
        modifier.put("orgUnit", getOrgUnitTitleWithParent(employeePost.getOrgUnit()));

        modifier.put("post", StringUtils.uncapitalize(employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));

        StringBuilder newSalaryPeriod = new StringBuilder();
        newSalaryPeriod.append("c ");
        newSalaryPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeBonus != null ? employeeBonus.getBeginDate() : null));
        if (employeeBonus != null && employeeBonus.getEndDate() != null)
        {
            newSalaryPeriod.append(" по ");
            newSalaryPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(employeeBonus.getEndDate()));
        }
        modifier.put("paymentPeriod", newSalaryPeriod.toString());

        StringBuilder bonusBuilder = new StringBuilder();
        StringBuilder bonusValue = new StringBuilder();
        if (employeeBonus != null)
        {
            bonusBuilder.append(employeeBonus.getPayment().getTitle()).append(" (").append(employeeBonus.getFinancingSourceTitle())
                    .append(employeeBonus.getFinancingSourceItem() != null? " (" + employeeBonus.getFinancingSourceItem().getTitle() + ")" : "").append(")");
            bonusValue.append(employeeBonus.getValue());
            switch(employeeBonus.getPayment().getPaymentUnit().getCode())
            {
                case UniempDefines.PAYMENT_UNIT_RUBLES:
                    bonusValue.append(CommonBaseStringUtil.numberPostfixCase((long)employeeBonus.getValue(), "рубль", "рубля", "рублей"));
                    break;
                case  UniempDefines.PAYMENT_UNIT_BASE_PERCENT:
                    bonusValue.append("% от оклада");
                    break;
                case  UniempDefines.PAYMENT_UNIT_COEFFICIENT:
                    bonusValue.append(" части оклада");
                    break;

            }
        }
        modifier.put("paymentString", bonusBuilder.toString());
        modifier.put("paymentValue", bonusValue.toString());


        Double staffValue = 0D;
        List<EmployeePostStaffRateItem> staffItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(employeePost);
        for (EmployeePostStaffRateItem item : staffItemList)
        {
            staffValue += item.getStaffRate();
        }

        modifier.put("staffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffValue) + " ставки");

        modifier.modify(document);

        return document;
    }

    private String getOrgUnitTitleWithParent(OrgUnit orgUnit)
    {
        StringBuilder builder = new StringBuilder();
        if (orgUnit.getParent() != null)
        {
            builder.append(StringUtils.capitalize(orgUnit.getParent().getOrgUnitType().getTitle())).append(" «").append(orgUnit.getParent().getPrintTitle()).append("», ");
        }
        builder.append(StringUtils.capitalize(orgUnit.getOrgUnitType().getTitle())).append(" «").append(orgUnit.getPrintTitle()).append("»");
        return builder.toString();
    }
}