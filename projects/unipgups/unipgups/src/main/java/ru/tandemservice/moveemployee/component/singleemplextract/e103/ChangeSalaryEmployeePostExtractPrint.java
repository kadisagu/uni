/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103;

import org.springframework.util.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.PostTypeCodes;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 28.08.2015
 */
public class ChangeSalaryEmployeePostExtractPrint implements IPrintFormCreator<ChangeSalaryEmployeePostExtract>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeSalaryEmployeePostExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder) extract.getParagraph().getOrder());

        EmployeePost employeePost = extract.getEntity();

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        modifier.put("FIO", extract.getEmployee().getFullFio());
        modifier.put("orgUnit", getOrgUnitTitleWithParent(employeePost.getOrgUnit()));

        modifier.put("post", StringUtils.uncapitalize(employeePost.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));

        StringBuilder newSalaryPeriod = new StringBuilder();
        newSalaryPeriod.append("c ");
        newSalaryPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getStartDate()));
        if (extract.getEndDate() != null)
        {
            newSalaryPeriod.append(" по ");
            newSalaryPeriod.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEndDate()));
        }
        modifier.put("newSalaryPeriod", newSalaryPeriod.toString());
        modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getNewSalary()));

        if (!employeePost.getPostType().getCode().equals(PostTypeCodes.MAIN_JOB))
            modifier.put("postType", ", " + employeePost.getPostType().getTitle());
        else modifier.put("postType", "");

        Double staffValue = 0D;
        List<EmployeePostStaffRateItem> staffItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(employeePost);
        for (EmployeePostStaffRateItem item : staffItemList)
        {
            staffValue += item.getStaffRate();
        }

        modifier.put("financingSource", CommonBaseStringUtil.joinUnique(staffItemList, EmployeePostStaffRateItem.financingSource().title(), ", "));
        modifier.put("staffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffValue) + " ставки");

        modifier.modify(document);

        return document;
    }



    private String getOrgUnitTitleWithParent(OrgUnit orgUnit)
    {
        StringBuilder builder = new StringBuilder();
        if (orgUnit.getParent() != null)
        {
            builder.append(StringUtils.capitalize(orgUnit.getParent().getOrgUnitType().getTitle())).append(" «").append(orgUnit.getParent().getPrintTitle()).append("», ");
        }
        builder.append(StringUtils.capitalize(orgUnit.getOrgUnitType().getTitle())).append(" «").append(orgUnit.getPrintTitle()).append("»");
        return builder.toString();
    }
}