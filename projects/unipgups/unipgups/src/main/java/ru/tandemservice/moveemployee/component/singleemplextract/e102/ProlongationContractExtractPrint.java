/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public class ProlongationContractExtractPrint implements IPrintFormCreator<ProlongationContractExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ProlongationContractExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder) extract.getParagraph().getOrder());

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        modifier.put("FIO", extract.getEmployee().getFullFio());
        modifier.put("orgUnit", getOrgUnitTitleWithParent(extract.getEntity().getOrgUnit()));

        modifier.put("post", StringUtils.uncapitalize(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));
        modifier.put("oldEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getOldEndDate()));
        modifier.put("newEndDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getNewEndDate()));

        Double staffValue = 0D;
        for (EmployeePostStaffRateItem item : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity()))
        {
            staffValue += item.getStaffRate();
        }

        modifier.put("staffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffValue) + " ставки");
        modifier.put("contractAddAgreementDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getContractAddAgreementDate()));
        modifier.put("contractAddAgreementNumber", extract.getContractAddAgreementNumber());
        modifier.put("contractDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLabourContract().getDate()));
        modifier.put("contractNumber", extract.getLabourContract().getNumber());

        modifier.modify(document);

        return document;
    }

    private String getOrgUnitTitleWithParent(OrgUnit orgUnit)
    {
        StringBuilder builder = new StringBuilder();
        if (orgUnit.getParent() != null)
        {
            builder.append(StringUtils.capitalize(orgUnit.getParent().getOrgUnitType().getTitle())).append(" «").append(orgUnit.getParent().getPrintTitle()).append("», ");
        }
        builder.append(StringUtils.capitalize(orgUnit.getOrgUnitType().getTitle())).append(" «").append(orgUnit.getPrintTitle()).append("»");
        return builder.toString();
    }
}