/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.ISingleEmployeeExtractPubDAO;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 31.08.2015
 */
public interface IDAO extends ISingleEmployeeExtractPubDAO<ChangeSalaryEmployeePostExtract, Model>
{
}