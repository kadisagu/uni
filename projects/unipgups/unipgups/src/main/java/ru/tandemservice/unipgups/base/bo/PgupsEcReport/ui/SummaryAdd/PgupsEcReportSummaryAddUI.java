/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.PgupsEcReportManager;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.IPgupsEcReport;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.IPgupsEcReportDAO;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.SummaryReportBuilder;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryPub.PgupsEcReportSummaryPub;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.IPgupsEcReportVO;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.SummaryReportVO;
import ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
public class PgupsEcReportSummaryAddUI extends UIPresenter
{
    private IPgupsEcReport report = new PgupsSummaryEnrollmentReport();
    private IPgupsEcReportVO reportData = new SummaryReportVO();

    @Override
    public void onComponentRefresh()
    {
        EnrollmentCampaign ec = UniecDAOFacade.getEntrantDAO().getLastEnrollmentCampaign();
        report.setEnrollmentCampaign(ec);
        report.setDateFrom(ec != null ? ec.getStartDate() : CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())));
        report.setDateTo(new Date());
    }

    public void onClickApply()
    {
        IPgupsEcReportDAO dao = PgupsEcReportManager.instance().dao();
        dao.saveNewReport(report, reportData, new SummaryReportBuilder(report, reportData, dao.getComponentSession()));

        deactivate();

        _uiActivation.asDesktopRoot(PgupsEcReportSummaryPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }

    // Getters & Setters

    public IPgupsEcReport getReport()
    {
        return report;
    }

    public IPgupsEcReportVO getReportData()
    {
        return reportData;
    }
}