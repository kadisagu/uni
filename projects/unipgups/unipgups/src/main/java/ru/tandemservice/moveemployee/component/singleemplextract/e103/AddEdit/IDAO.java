/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103.AddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.ICommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 30.08.2015
 */
public interface IDAO extends ICommonSingleEmployeeExtractAddEditDAO<ChangeSalaryEmployeePostExtract, Model>
{
}