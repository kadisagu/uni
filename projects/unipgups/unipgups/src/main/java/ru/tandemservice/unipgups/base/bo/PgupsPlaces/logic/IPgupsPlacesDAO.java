/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public interface IPgupsPlacesDAO
{
    String BUILDING ="building";
    String UNIT ="unit";
    String FLOOR ="floor";
    String TITLE ="title";

    boolean existRelatedObjects(Long placeId);
}