/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic.IPgupsPlacesDAO;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.AddEdit.PgupsPlacesAddEdit;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public class PgupsPlacesListUI extends UIPresenter
{

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(PgupsPlacesList.PLACES_DS))
        {
            dataSource.put(IPgupsPlacesDAO.BUILDING, getSettings().get(IPgupsPlacesDAO.BUILDING));
            dataSource.put(IPgupsPlacesDAO.UNIT, getSettings().get(IPgupsPlacesDAO.UNIT));
            dataSource.put(IPgupsPlacesDAO.FLOOR, getSettings().get(IPgupsPlacesDAO.FLOOR));
            dataSource.put(IPgupsPlacesDAO.TITLE, getSettings().get(IPgupsPlacesDAO.TITLE));
        }
        else if (dataSource.getName().equals(PgupsPlacesList.UNITS_DS))
        {
            dataSource.put(IPgupsPlacesDAO.BUILDING, getSettings().get(IPgupsPlacesDAO.BUILDING));
        }
        else if (dataSource.getName().equals(PgupsPlacesList.FLOORS_DS))
        {
            dataSource.put(IPgupsPlacesDAO.BUILDING, getSettings().get(IPgupsPlacesDAO.BUILDING));
            dataSource.put(IPgupsPlacesDAO.UNIT, getSettings().get(IPgupsPlacesDAO.UNIT));
        }

    }

    @Override
    public String getSettingsKey()
    {
        return "PgupsPlaceList.filter";
    }

    public void onEditPlaceClick()
    {
        _uiActivation.asRegionDialog(PgupsPlacesAddEdit.class).parameter("placeId", getListenerParameterAsLong()).activate();
    }

    public void onClickAddPgupsPlace()
    {
        _uiActivation.asRegionDialog(PgupsPlacesAddEdit.class).activate();
    }

    public void onDeletePlaceClick()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickSearch()
    {
        getSettings().clear();
    }
}