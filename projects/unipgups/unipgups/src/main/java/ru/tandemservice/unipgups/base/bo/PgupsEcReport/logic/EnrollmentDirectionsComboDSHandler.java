/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.EntrantsTADataReportVO;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class EnrollmentDirectionsComboDSHandler extends SimpleTitledComboDataSourceHandler
{

    public EnrollmentDirectionsComboDSHandler(String ownerId)
    {
        super(ownerId);
        _filtered = true;
    }

    public static enum Columns
    {
        FORMATIVE_ORG_UNIT(EnrollmentDirection.educationOrgUnit().formativeOrgUnit(), EnrollmentDirection.educationOrgUnit().formativeOrgUnit().title(), OrgUnit.title()),
        TERRITORIAL_ORG_UNIT(EnrollmentDirection.educationOrgUnit().territorialOrgUnit(), EnrollmentDirection.educationOrgUnit().territorialOrgUnit().territorialFullTitle(), OrgUnit.territorialFullTitle()),
        EDUCATION_LEVELS_HIGH_SCHOOL(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool(), EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().displayableTitle(), EducationLevelsHighSchool.displayableTitle()),
        DEVELOP_FORM(EnrollmentDirection.educationOrgUnit().developForm(), EnrollmentDirection.educationOrgUnit().developForm().title(), DevelopForm.title()),
        DEVELOP_CONDITION(EnrollmentDirection.educationOrgUnit().developCondition(), EnrollmentDirection.educationOrgUnit().developCondition().title(), DevelopCondition.title()),
        DEVELOP_TECH(EnrollmentDirection.educationOrgUnit().developTech(), EnrollmentDirection.educationOrgUnit().developTech().title(), DevelopTech.title()),
        DEVELOP_PERIOD(EnrollmentDirection.educationOrgUnit().developPeriod(), EnrollmentDirection.educationOrgUnit().developPeriod().title(), DevelopPeriod.title());

        private Columns(MetaDSLPath column, MetaDSLPath orderPath, MetaDSLPath filterPath)
        {
            _column = column;
            _orderPath = orderPath;
            _filterPath = filterPath;
        }

        private MetaDSLPath _column;
        private MetaDSLPath _orderPath;
        private MetaDSLPath _filterPath;

        public MetaDSLPath getColumn()
        {
            return _column;
        }

        public MetaDSLPath getOrderPath()
        {
            return _orderPath;
        }

        public void setOrderPath(MetaDSLPath orderPath)
        {
            _orderPath = orderPath;
        }

        public void setColumn(MetaDSLPath column)
        {
            _column = column;
        }

        public MetaDSLPath getFilterPath()
        {
            return _filterPath;
        }

        public void setFilterPath(MetaDSLPath filterPath)
        {
            _filterPath = filterPath;
        }
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Columns column =  context.get("column");
        EntrantsTADataReportVO reportVO = context.get("reportVO");

        if(null == reportVO.getEnrollmentCampaign())
            return ListOutputBuilder.get(input, Collections.emptyList()).build();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed");
        if(null != column.getColumn())
            builder.column(property("ed", column.getColumn()));
        builder.where(eq(property("ed", EnrollmentDirection.enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        if(!Columns.FORMATIVE_ORG_UNIT.equals(column))
        {
            if(null == reportVO.getFormativeOrgUnit())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            else
                builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().id()), value(reportVO.getFormativeOrgUnit().getId())));
        }
        if(!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column))
        {
            if(null == reportVO.getFormativeOrgUnit() || null == reportVO.getTerritorialOrgUnit())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            else
                builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().id()), value(reportVO.getTerritorialOrgUnit().getId())));
        }
        if(!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column) && !Columns.EDUCATION_LEVELS_HIGH_SCHOOL.equals(column))
        {
            if(null == reportVO.getFormativeOrgUnit() || null == reportVO.getTerritorialOrgUnit() || null == reportVO.getEducationLevelsHighSchool())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            else
                builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().id()), value(reportVO.getEducationLevelsHighSchool().getId())));
        }

        if(!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column) &&
                !Columns.EDUCATION_LEVELS_HIGH_SCHOOL.equals(column) && !Columns.DEVELOP_FORM.equals(column))
        {
            if(null == reportVO.getFormativeOrgUnit() || null == reportVO.getTerritorialOrgUnit() ||
                    null == reportVO.getEducationLevelsHighSchool() || null == reportVO.getDevelopForm())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            else
                builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developForm().id()), value(reportVO.getDevelopForm().getId())));
        }

        if(!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column) &&
                !Columns.EDUCATION_LEVELS_HIGH_SCHOOL.equals(column) && !Columns.DEVELOP_FORM.equals(column) &&
                !Columns.DEVELOP_CONDITION.equals(column))
        {
            if(null == reportVO.getFormativeOrgUnit() || null == reportVO.getTerritorialOrgUnit() ||
                    null == reportVO.getEducationLevelsHighSchool() || null == reportVO.getDevelopForm() ||
                    null == reportVO.getDevelopCondition())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            else
                builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developCondition().id()), value(reportVO.getDevelopCondition().getId())));
        }

        if(!Columns.FORMATIVE_ORG_UNIT.equals(column) && !Columns.TERRITORIAL_ORG_UNIT.equals(column) &&
                !Columns.EDUCATION_LEVELS_HIGH_SCHOOL.equals(column) && !Columns.DEVELOP_FORM.equals(column) &&
                !Columns.DEVELOP_CONDITION.equals(column) && !Columns.DEVELOP_TECH.equals(column))
        {
            if(null == reportVO.getFormativeOrgUnit() || null == reportVO.getTerritorialOrgUnit() ||
                    null == reportVO.getEducationLevelsHighSchool() || null == reportVO.getDevelopForm() ||
                    null == reportVO.getDevelopCondition() || null == reportVO.getDevelopTech())
                return ListOutputBuilder.get(input, Collections.emptyList()).build();
            else
                builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developTech().id()), value(reportVO.getDevelopTech().getId())));
        }

//        builder.order(property("ed", column.getOrderPath().s()));
//        if(!StringUtils.isEmpty(input.getComboFilterByValue()))
//            builder.where(like(DQLFunctions.upper(property("ed", column.getFilterPath())), value(CoreStringUtils.escapeLike(input.getComboFilterByValue()))));
        setOrderByProperty(column.getOrderPath().s());
        setFilterByProperty(column.getFilterPath().s());
        context.put(UIDefines.COMBO_OBJECT_LIST, createStatement(builder).list());
        return super.execute(input, context);
    }
}
