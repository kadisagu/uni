/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexey Lopatin
 * @since 03.08.2013
 */
public abstract interface IDAO extends IUniDao<Model>
{
}
