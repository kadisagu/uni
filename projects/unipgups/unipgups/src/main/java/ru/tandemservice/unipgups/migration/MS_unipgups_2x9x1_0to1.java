package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipgups_2x9x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1"),
				 new ScriptDependency("ru.tandemservice.nsiclient", "2.9.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность uniPlacesPlaceExt

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("uniplacesplaceext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_uniplacesplaceext"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("place_id", DBType.LONG).setNullable(false), 
				new DBColumn("responsibleperson_id", DBType.LONG), 
				new DBColumn("pgupsplace_id", DBType.LONG).setNullable(false), 
				new DBColumn("mayrented_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("uniPlacesPlaceExt");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность uniPgupsPlace

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("unipgupsplace_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_unipgupsplace"), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("number_p", DBType.createVarchar(255)), 
				new DBColumn("floor_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("uniPgupsPlace");

		}


    }
}