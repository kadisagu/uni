package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMission;
import ru.tandemservice.unipgups.entity.PgupsServiceTask;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Командировка сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsEmployeeMissionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.PgupsEmployeeMission";
    public static final String ENTITY_NAME = "pgupsEmployeeMission";
    public static final int VERSION_HASH = 61027600;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";
    public static final String L_FINANCING_SOURCE_O_U = "financingSourceOU";
    public static final String P_INTERNAL_FINANCING_SOURCE = "internalFinancingSource";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_PGUPS_SERVICE_TASK = "pgupsServiceTask";
    public static final String P_WARRANT_NUMBER = "warrantNumber";
    public static final String P_WARRANT_DATE = "warrantDate";

    private EmployeePost _employeePost;     // Сотрудник
    private ExternalOrgUnit _externalOrgUnit;     // Организация, в которую направляется сотрудник
    private ExternalOrgUnit _financingSourceOU;     // Источник финансирования
    private boolean _internalFinancingSource = false;     // Финансируется отправляющей стороной
    private Date _beginDate;     // Дата начала командировки
    private Date _endDate;     // Дата окончания командировки
    private PgupsServiceTask _pgupsServiceTask;     // Служебное задание
    private String _warrantNumber;     // Номер командировочного удостоверения
    private Date _warrantDate;     // Дата командировочного удостоверения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Организация, в которую направляется сотрудник. Свойство не может быть null.
     */
    @NotNull
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Организация, в которую направляется сотрудник. Свойство не может быть null.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    /**
     * @return Источник финансирования.
     */
    public ExternalOrgUnit getFinancingSourceOU()
    {
        return _financingSourceOU;
    }

    /**
     * @param financingSourceOU Источник финансирования.
     */
    public void setFinancingSourceOU(ExternalOrgUnit financingSourceOU)
    {
        dirty(_financingSourceOU, financingSourceOU);
        _financingSourceOU = financingSourceOU;
    }

    /**
     * @return Финансируется отправляющей стороной. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternalFinancingSource()
    {
        return _internalFinancingSource;
    }

    /**
     * @param internalFinancingSource Финансируется отправляющей стороной. Свойство не может быть null.
     */
    public void setInternalFinancingSource(boolean internalFinancingSource)
    {
        dirty(_internalFinancingSource, internalFinancingSource);
        _internalFinancingSource = internalFinancingSource;
    }

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала командировки. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания командировки. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания командировки. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Служебное задание. Свойство не может быть null.
     */
    @NotNull
    public PgupsServiceTask getPgupsServiceTask()
    {
        return _pgupsServiceTask;
    }

    /**
     * @param pgupsServiceTask Служебное задание. Свойство не может быть null.
     */
    public void setPgupsServiceTask(PgupsServiceTask pgupsServiceTask)
    {
        dirty(_pgupsServiceTask, pgupsServiceTask);
        _pgupsServiceTask = pgupsServiceTask;
    }

    /**
     * @return Номер командировочного удостоверения.
     */
    @Length(max=255)
    public String getWarrantNumber()
    {
        return _warrantNumber;
    }

    /**
     * @param warrantNumber Номер командировочного удостоверения.
     */
    public void setWarrantNumber(String warrantNumber)
    {
        dirty(_warrantNumber, warrantNumber);
        _warrantNumber = warrantNumber;
    }

    /**
     * @return Дата командировочного удостоверения.
     */
    public Date getWarrantDate()
    {
        return _warrantDate;
    }

    /**
     * @param warrantDate Дата командировочного удостоверения.
     */
    public void setWarrantDate(Date warrantDate)
    {
        dirty(_warrantDate, warrantDate);
        _warrantDate = warrantDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PgupsEmployeeMissionGen)
        {
            setEmployeePost(((PgupsEmployeeMission)another).getEmployeePost());
            setExternalOrgUnit(((PgupsEmployeeMission)another).getExternalOrgUnit());
            setFinancingSourceOU(((PgupsEmployeeMission)another).getFinancingSourceOU());
            setInternalFinancingSource(((PgupsEmployeeMission)another).isInternalFinancingSource());
            setBeginDate(((PgupsEmployeeMission)another).getBeginDate());
            setEndDate(((PgupsEmployeeMission)another).getEndDate());
            setPgupsServiceTask(((PgupsEmployeeMission)another).getPgupsServiceTask());
            setWarrantNumber(((PgupsEmployeeMission)another).getWarrantNumber());
            setWarrantDate(((PgupsEmployeeMission)another).getWarrantDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsEmployeeMissionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsEmployeeMission.class;
        }

        public T newInstance()
        {
            return (T) new PgupsEmployeeMission();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
                case "financingSourceOU":
                    return obj.getFinancingSourceOU();
                case "internalFinancingSource":
                    return obj.isInternalFinancingSource();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "pgupsServiceTask":
                    return obj.getPgupsServiceTask();
                case "warrantNumber":
                    return obj.getWarrantNumber();
                case "warrantDate":
                    return obj.getWarrantDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
                case "financingSourceOU":
                    obj.setFinancingSourceOU((ExternalOrgUnit) value);
                    return;
                case "internalFinancingSource":
                    obj.setInternalFinancingSource((Boolean) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "pgupsServiceTask":
                    obj.setPgupsServiceTask((PgupsServiceTask) value);
                    return;
                case "warrantNumber":
                    obj.setWarrantNumber((String) value);
                    return;
                case "warrantDate":
                    obj.setWarrantDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "externalOrgUnit":
                        return true;
                case "financingSourceOU":
                        return true;
                case "internalFinancingSource":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "pgupsServiceTask":
                        return true;
                case "warrantNumber":
                        return true;
                case "warrantDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "externalOrgUnit":
                    return true;
                case "financingSourceOU":
                    return true;
                case "internalFinancingSource":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "pgupsServiceTask":
                    return true;
                case "warrantNumber":
                    return true;
                case "warrantDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
                case "financingSourceOU":
                    return ExternalOrgUnit.class;
                case "internalFinancingSource":
                    return Boolean.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "pgupsServiceTask":
                    return PgupsServiceTask.class;
                case "warrantNumber":
                    return String.class;
                case "warrantDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsEmployeeMission> _dslPath = new Path<PgupsEmployeeMission>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsEmployeeMission");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Организация, в которую направляется сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getFinancingSourceOU()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> financingSourceOU()
    {
        return _dslPath.financingSourceOU();
    }

    /**
     * @return Финансируется отправляющей стороной. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#isInternalFinancingSource()
     */
    public static PropertyPath<Boolean> internalFinancingSource()
    {
        return _dslPath.internalFinancingSource();
    }

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Служебное задание. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getPgupsServiceTask()
     */
    public static PgupsServiceTask.Path<PgupsServiceTask> pgupsServiceTask()
    {
        return _dslPath.pgupsServiceTask();
    }

    /**
     * @return Номер командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getWarrantNumber()
     */
    public static PropertyPath<String> warrantNumber()
    {
        return _dslPath.warrantNumber();
    }

    /**
     * @return Дата командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getWarrantDate()
     */
    public static PropertyPath<Date> warrantDate()
    {
        return _dslPath.warrantDate();
    }

    public static class Path<E extends PgupsEmployeeMission> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _financingSourceOU;
        private PropertyPath<Boolean> _internalFinancingSource;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PgupsServiceTask.Path<PgupsServiceTask> _pgupsServiceTask;
        private PropertyPath<String> _warrantNumber;
        private PropertyPath<Date> _warrantDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Организация, в которую направляется сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getFinancingSourceOU()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> financingSourceOU()
        {
            if(_financingSourceOU == null )
                _financingSourceOU = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_FINANCING_SOURCE_O_U, this);
            return _financingSourceOU;
        }

    /**
     * @return Финансируется отправляющей стороной. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#isInternalFinancingSource()
     */
        public PropertyPath<Boolean> internalFinancingSource()
        {
            if(_internalFinancingSource == null )
                _internalFinancingSource = new PropertyPath<Boolean>(PgupsEmployeeMissionGen.P_INTERNAL_FINANCING_SOURCE, this);
            return _internalFinancingSource;
        }

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(PgupsEmployeeMissionGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(PgupsEmployeeMissionGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Служебное задание. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getPgupsServiceTask()
     */
        public PgupsServiceTask.Path<PgupsServiceTask> pgupsServiceTask()
        {
            if(_pgupsServiceTask == null )
                _pgupsServiceTask = new PgupsServiceTask.Path<PgupsServiceTask>(L_PGUPS_SERVICE_TASK, this);
            return _pgupsServiceTask;
        }

    /**
     * @return Номер командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getWarrantNumber()
     */
        public PropertyPath<String> warrantNumber()
        {
            if(_warrantNumber == null )
                _warrantNumber = new PropertyPath<String>(PgupsEmployeeMissionGen.P_WARRANT_NUMBER, this);
            return _warrantNumber;
        }

    /**
     * @return Дата командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMission#getWarrantDate()
     */
        public PropertyPath<Date> warrantDate()
        {
            if(_warrantDate == null )
                _warrantDate = new PropertyPath<Date>(PgupsEmployeeMissionGen.P_WARRANT_DATE, this);
            return _warrantDate;
        }

        public Class getEntityClass()
        {
            return PgupsEmployeeMission.class;
        }

        public String getEntityName()
        {
            return "pgupsEmployeeMission";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
