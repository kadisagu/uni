/* $Id:$ */
package ru.tandemservice.unipgups.reactors;

import ru.tandemservice.nsiclient.base.bo.NsiSync.logic.IUnknownDatagramsCollector;
import ru.tandemservice.nsiclient.datagram.*;
import ru.tandemservice.nsiclient.entity.NsiEntity;
import ru.tandemservice.nsiclient.reactor.BaseEntityReactor;
import ru.tandemservice.nsiclient.reactor.support.*;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Nikonov
 * @since 07.07.2015
 */
public class EduProgramDisciplineStatisticsTypeReactor extends BaseEntityReactor<NsiUniEduProgramDisciplineStatistics, EduProgramDisciplineStatisticsType>
{
    @Override
    public Class<NsiUniEduProgramDisciplineStatistics> getEntityClass()
    {
        return NsiUniEduProgramDisciplineStatistics.class;
    }

    @Override
    public void collectUnknownDatagrams(EduProgramDisciplineStatisticsType datagramObject, IUnknownDatagramsCollector collector)
    {
        EduProgramDisciplineStatisticsType.EducationalProgramID educationalProgramID = datagramObject.getEducationalProgramID();
        EducationalProgramType educationalProgramType = educationalProgramID == null ? null : educationalProgramID.getEducationalProgram();
        if(educationalProgramType != null)
            collector.check(educationalProgramType.getID(), EducationalProgramType.class);

        EduProgramDisciplineStatisticsType.DepartmentID departmentID = datagramObject.getDepartmentID();
        DepartmentType departmentType = departmentID == null ? null : departmentID.getDepartment();
        if(departmentType != null)
            collector.check(departmentType.getID(), DepartmentType.class);

        EduProgramDisciplineStatisticsType.CourseID courseID = datagramObject.getCourseID();
        CourseType courseType = courseID == null ? null : courseID.getCourse();
        if(courseType != null)
            collector.check(courseType.getID(), CourseType.class);

        EduProgramDisciplineStatisticsType.ProgramQualificationID qualificationID = datagramObject.getProgramQualificationID();
        EduProgramQualificationType qualificationType = qualificationID == null ? null : qualificationID.getProgramQualification();
        if(qualificationType != null)
            collector.check(qualificationType.getID(), EduProgramQualificationType.class);
    }

    @Override
    public EduProgramDisciplineStatisticsType createDatagramObject(String guid)
    {
        EduProgramDisciplineStatisticsType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineStatisticsType();
        if (null != guid)
            datagramObject.setID(guid);
        return datagramObject;
    }

    private static String getShortTitle(DevelopForm developForm)
    {
        switch (developForm.getCode()) {
            case DevelopFormCodes.FULL_TIME_FORM:
                return "о";
            case DevelopFormCodes.CORESP_FORM:
                return "зо";
            case DevelopFormCodes.PART_TIME_FORM:
                return "озо";
            case DevelopFormCodes.EXTERNAL_FORM:
                return "э";
            case DevelopFormCodes.APPLICANT_FORM:
                return "со";
        }
        return "";
    }

    @Override
    public ResultListWrapper createDatagramObject(NsiUniEduProgramDisciplineStatistics entity, NsiEntity nsiEntity, Map<String, Object> commonCache)
    {
        EduProgramDisciplineStatisticsType datagramObject = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineStatisticsType();
        List<IDatagramObject> result = new ArrayList<>(4);
        result.add(datagramObject);

        datagramObject.setID(nsiEntity.getGuid());
        datagramObject.setEduProgramDisciplineStatisticsDevelopFormName(getShortTitle(entity.getDevelopForm()));
        datagramObject.setEduProgramDisciplineStatisticsCourseNumber(Integer.toString(entity.getCourse().getIntValue()));
        datagramObject.setEduProgramDisciplineStatisticsTermNumber(entity.getTerm()!=null?Integer.toString(entity.getTerm().getIntValue()):null);
        EduProgramQualification qualification = entity.getQualification();
        if(qualification != null)
            datagramObject.setEduProgramDisciplineStatisticsQualificationName(entity.getQualification().getTitle());
        EducationLevels level = entity.getEducationLevels();
        datagramObject.setEduProgramDisciplineStatisticsOKSO(level.getInheritedOkso());
        datagramObject.setEduProgramDisciplineStatisticsProfileOKSO(level.getInheritedOkso() + "(" + level.getShortTitle() + ")");
        datagramObject.setEduProgramDisciplineStatisticDepartmentName(entity.getFormativeOrgUnit().getTypeTitle() + " (" + entity.getTerritorialOrgUnit().getTerritorialTitle() + ")");
        datagramObject.setEduProgramDisciplineStatisticPlanStudentsAmount(Integer.toString(entity.getAmount()));

        StringBuilder warning = new StringBuilder();

//        ProcessedDatagramObject<EducationalProgramType> educationalProgramType = createDatagramObject(EducationalProgramType.class, entity.getEducationOrgUnit(), commonCache, warning);
//        EduProgramDisciplineStatisticsType.EducationalProgramID educationalProgramID = NsiUtils.OBJECT_FACTORY.createEduProgramDisciplineStatisticsTypeEducationalProgramID();
//        educationalProgramID.setEducationalProgram(educationalProgramType.getObject());
//        datagramObject.setEducationalProgramID(educationalProgramID);
//        if (educationalProgramType.getList() != null)
//            result.addAll(educationalProgramType.getList());

        ProcessedDatagramObject<DepartmentType> departmentType = createDatagramObject(DepartmentType.class, entity.getFormativeOrgUnit(), commonCache, warning);
        EduProgramDisciplineStatisticsType.DepartmentID departmentID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineStatisticsTypeDepartmentID();
        departmentID.setDepartment(departmentType.getObject());
        datagramObject.setDepartmentID(departmentID);
        if (departmentType.getList() != null)
            result.addAll(departmentType.getList());

        ProcessedDatagramObject<CourseType> courseType = createDatagramObject(CourseType.class, entity.getCourse(), commonCache, warning);
        EduProgramDisciplineStatisticsType.CourseID courseID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineStatisticsTypeCourseID();
        courseID.setCourse(courseType.getObject());
        datagramObject.setCourseID(courseID);
        if (courseType.getList() != null)
            result.addAll(courseType.getList());

        if(entity.getQualification() != null)
        {
            ProcessedDatagramObject<EduProgramQualificationType> qualificationType = createDatagramObject(EduProgramQualificationType.class, entity.getQualification(), commonCache, warning);
            EduProgramDisciplineStatisticsType.ProgramQualificationID qualificationID = DatagramObjectFactoryHolder.OBJECT_FACTORY.createEduProgramDisciplineStatisticsTypeQualificationID();
            qualificationID.setProgramQualification(qualificationType.getObject());
            datagramObject.setProgramQualificationID(qualificationID);
            if (qualificationType.getList() != null)
                result.addAll(qualificationType.getList());
        }
        return new ResultListWrapper(warning.length() > 0 ? warning.toString() : null, result);
    }

    @Override
    public ChangedWrapper<NsiUniEduProgramDisciplineStatistics> processDatagramObject(EduProgramDisciplineStatisticsType datagramObject, Map<String, Object> commonCache, Map<String, Object> reactorCache, Map<String, Object> additionalParams) throws ProcessingDatagramObjectException {
        return null;
    }
}
