/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditController;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class Controller extends CommonSingleEmployeeExtractAddEditController<PgupsEmployeeMissionExtract, IDAO, Model>
{

    public void onChangeExternalOrgUnit(IBusinessComponent component)
    {
        Model model = component.getModel();
        if (model.getExtract().getExternalOrgUnit().getFactAddress() != null && model.getExtract().getExternalOrgUnit().getFactAddress().getSettlement()!= null)
            model.setCityOrgUnit(model.getExtract().getExternalOrgUnit().getFactAddress().getSettlement().getTitle());
    }
}