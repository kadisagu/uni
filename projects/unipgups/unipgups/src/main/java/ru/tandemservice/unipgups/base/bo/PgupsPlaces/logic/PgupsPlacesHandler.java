/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public class PgupsPlacesHandler extends DefaultSearchDataSourceHandler
{
    public PgupsPlacesHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Object building = context.get(IPgupsPlacesDAO.BUILDING);
        Object unit = context.get(IPgupsPlacesDAO.UNIT);
        Object floor = context.get(IPgupsPlacesDAO.FLOOR);
        String title = context.get(IPgupsPlacesDAO.TITLE);
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniPgupsPlace.class, "pl")
                .column(property("pl"));
        if (building != null)
            builder.where(eq(property(UniPgupsPlace.floor().unit().building().fromAlias("pl")), commonValue(building)));

        if (unit != null)
            builder.where(eq(property(UniPgupsPlace.floor().unit().fromAlias("pl")), commonValue(unit)));

        if (floor != null)
            builder.where(eq(property(UniPgupsPlace.floor().fromAlias("pl")), commonValue(floor)));
        if(!StringUtils.isEmpty(title))
            builder.where(likeUpper(property(UniPgupsPlace.title().fromAlias("pl")), value(CoreStringUtils.escapeLike((String) title))));

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniPgupsPlace.class, "pl");

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order(orderRegistry).pageable(true).build();
    }
}
