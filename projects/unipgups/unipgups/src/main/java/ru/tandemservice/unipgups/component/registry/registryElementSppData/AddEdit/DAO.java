/* $Id$ */
package ru.tandemservice.unipgups.component.registry.registryElementSppData.AddEdit;

import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.component.registry.registryElementSppData.AddEdit.Model;

/**
 * @author Igor Belanov
 * @since 07.02.2017
 */
public class DAO extends ru.tandemservice.unispp.component.registry.registryElementSppData.AddEdit.DAO
{
    @Override
    public SppRegElementExt createSppRegElementExt(Model model)
    {
        SppRegElementExt sppRegElementExt = super.createSppRegElementExt(model);
        // не нужно это ПГУПСу, пусть у них по дефолту будет false
        sppRegElementExt.setCheckDuplicates(false);
        return sppRegElementExt;
    }
}
