/* $Id$ */
package ru.tandemservice.unipgups.base.ext.Employee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 28.01.2015
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
}