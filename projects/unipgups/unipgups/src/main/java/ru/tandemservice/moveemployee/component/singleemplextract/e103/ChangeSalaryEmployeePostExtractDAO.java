/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 28.08.2015
 */
public class ChangeSalaryEmployeePostExtractDAO extends UniBaseDao implements IExtractComponentDao<ChangeSalaryEmployeePostExtract>
{
    @Override
    public void doCommit(ChangeSalaryEmployeePostExtract extract, Map parameters)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        EmployeePost employeePost = extract.getEntity();
        extract.setOldSalary(employeePost.getSalary());
        employeePost.setSalary(extract.getNewSalary());


        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());
        if (contract == null)
        {
            UserContext.getInstance().getErrorCollector().add("Невозможно провести приказ, так как не указаны данные трудового договора на карточке сотрудника " + extract.getEntity().getFullTitle() + ". Укажите данные трудового договора на карточке сотрудника.");
            throw new ApplicationException();
        }
        ContractCollateralAgreement agreement = null;

        for (ContractCollateralAgreement collateralAgreement : UniempDaoFacade.getUniempDAO().getContractCollateralAgreementsList(contract))
            if (collateralAgreement.getNumber().equals(extract.getContractAddAgreementNumber()) && collateralAgreement.getDate().equals(extract.getContractAddAgreementDate()))
            {
                agreement = collateralAgreement;
                break;
            }

        if (agreement == null)
        {
            agreement = new ContractCollateralAgreement();

            agreement.setContract(contract);
            agreement.setDate(extract.getContractAddAgreementDate());
            agreement.setNumber(extract.getContractAddAgreementNumber());
            agreement.setDescription("Изменение должностного оклада");

            extract.setCreateCollateralAgreement(agreement);

            save(agreement);
        }

        update(extract);
    }


    @Override
    public void doRollback(ChangeSalaryEmployeePostExtract extract, Map parameters)
    {
        if (extract.getCreateCollateralAgreement() != null)
        {
            delete(extract.getCreateCollateralAgreement());
            extract.setCreateCollateralAgreement(null);
        }

        extract.getEntity().setSalary(extract.getOldSalary());
        update(extract);

    }
}