package ru.tandemservice.unipgups.entity.nsi.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Статистика по студентам для выгрузки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiUniEduProgramDisciplineStatisticsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics";
    public static final String ENTITY_NAME = "nsiUniEduProgramDisciplineStatistics";
    public static final int VERSION_HASH = 1020167774;
    private static IEntityMeta ENTITY_META;

    public static final String P_AMOUNT = "amount";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COURSE = "course";
    public static final String L_TERM = "term";
    public static final String L_QUALIFICATION = "qualification";
    public static final String L_EDUCATION_LEVELS = "educationLevels";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";

    private int _amount = 0;     // Плановое количество обучающихся по профилю
    private DevelopForm _developForm;     // Форма обучения
    private Course _course;     // Курс
    private Term _term;     // Семестр
    private EduProgramQualification _qualification;     // Квалификация обучающегося
    private EducationLevels _educationLevels;     // Направление подготовки (специальность)
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private OrgUnit _territorialOrgUnit;     // Территориальное подразделение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Плановое количество обучающихся по профилю. Свойство не может быть null.
     */
    @NotNull
    public int getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Плановое количество обучающихся по профилю. Свойство не может быть null.
     */
    public void setAmount(int amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма обучения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Семестр.
     */
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Квалификация обучающегося.
     */
    public EduProgramQualification getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификация обучающегося.
     */
    public void setQualification(EduProgramQualification qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    /**
     * @param educationLevels Направление подготовки (специальность).
     */
    public void setEducationLevels(EducationLevels educationLevels)
    {
        dirty(_educationLevels, educationLevels);
        _educationLevels = educationLevels;
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подразделение. Свойство не может быть null.
     */
    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsiUniEduProgramDisciplineStatisticsGen)
        {
            setAmount(((NsiUniEduProgramDisciplineStatistics)another).getAmount());
            setDevelopForm(((NsiUniEduProgramDisciplineStatistics)another).getDevelopForm());
            setCourse(((NsiUniEduProgramDisciplineStatistics)another).getCourse());
            setTerm(((NsiUniEduProgramDisciplineStatistics)another).getTerm());
            setQualification(((NsiUniEduProgramDisciplineStatistics)another).getQualification());
            setEducationLevels(((NsiUniEduProgramDisciplineStatistics)another).getEducationLevels());
            setFormativeOrgUnit(((NsiUniEduProgramDisciplineStatistics)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((NsiUniEduProgramDisciplineStatistics)another).getTerritorialOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiUniEduProgramDisciplineStatisticsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiUniEduProgramDisciplineStatistics.class;
        }

        public T newInstance()
        {
            return (T) new NsiUniEduProgramDisciplineStatistics();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "amount":
                    return obj.getAmount();
                case "developForm":
                    return obj.getDevelopForm();
                case "course":
                    return obj.getCourse();
                case "term":
                    return obj.getTerm();
                case "qualification":
                    return obj.getQualification();
                case "educationLevels":
                    return obj.getEducationLevels();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "amount":
                    obj.setAmount((Integer) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "qualification":
                    obj.setQualification((EduProgramQualification) value);
                    return;
                case "educationLevels":
                    obj.setEducationLevels((EducationLevels) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "amount":
                        return true;
                case "developForm":
                        return true;
                case "course":
                        return true;
                case "term":
                        return true;
                case "qualification":
                        return true;
                case "educationLevels":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "amount":
                    return true;
                case "developForm":
                    return true;
                case "course":
                    return true;
                case "term":
                    return true;
                case "qualification":
                    return true;
                case "educationLevels":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "amount":
                    return Integer.class;
                case "developForm":
                    return DevelopForm.class;
                case "course":
                    return Course.class;
                case "term":
                    return Term.class;
                case "qualification":
                    return EduProgramQualification.class;
                case "educationLevels":
                    return EducationLevels.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "territorialOrgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiUniEduProgramDisciplineStatistics> _dslPath = new Path<NsiUniEduProgramDisciplineStatistics>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiUniEduProgramDisciplineStatistics");
    }
            

    /**
     * @return Плановое количество обучающихся по профилю. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getAmount()
     */
    public static PropertyPath<Integer> amount()
    {
        return _dslPath.amount();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Семестр.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Квалификация обучающегося.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getEducationLevels()
     */
    public static EducationLevels.Path<EducationLevels> educationLevels()
    {
        return _dslPath.educationLevels();
    }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getTerritorialOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    public static class Path<E extends NsiUniEduProgramDisciplineStatistics> extends EntityPath<E>
    {
        private PropertyPath<Integer> _amount;
        private DevelopForm.Path<DevelopForm> _developForm;
        private Course.Path<Course> _course;
        private Term.Path<Term> _term;
        private EduProgramQualification.Path<EduProgramQualification> _qualification;
        private EducationLevels.Path<EducationLevels> _educationLevels;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private OrgUnit.Path<OrgUnit> _territorialOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Плановое количество обучающихся по профилю. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getAmount()
     */
        public PropertyPath<Integer> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Integer>(NsiUniEduProgramDisciplineStatisticsGen.P_AMOUNT, this);
            return _amount;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Семестр.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Квалификация обучающегося.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> qualification()
        {
            if(_qualification == null )
                _qualification = new EduProgramQualification.Path<EduProgramQualification>(L_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getEducationLevels()
     */
        public EducationLevels.Path<EducationLevels> educationLevels()
        {
            if(_educationLevels == null )
                _educationLevels = new EducationLevels.Path<EducationLevels>(L_EDUCATION_LEVELS, this);
            return _educationLevels;
        }

    /**
     * @return Формирующее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подразделение. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics#getTerritorialOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new OrgUnit.Path<OrgUnit>(L_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

        public Class getEntityClass()
        {
            return NsiUniEduProgramDisciplineStatistics.class;
        }

        public String getEntityName()
        {
            return "nsiUniEduProgramDisciplineStatistics";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
