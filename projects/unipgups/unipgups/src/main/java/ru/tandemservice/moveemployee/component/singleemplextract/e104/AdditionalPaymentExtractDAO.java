/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public class AdditionalPaymentExtractDAO extends UniBaseDao implements IExtractComponentDao<AdditionalPaymentExtract>
{
    @Override
    public void doCommit(AdditionalPaymentExtract extract, Map parameters)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);

        EmployeePost employeePost = extract.getEntity();


        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(extract.getEntity());
        if (contract == null)
        {
            UserContext.getInstance().getErrorCollector().add("Невозможно провести приказ, так как не указаны данные трудового договора на карточке сотрудника " + extract.getEntity().getFullTitle() + ". Укажите данные трудового договора на карточке сотрудника.");
            throw new ApplicationException();
        }
        ContractCollateralAgreement agreement = null;

        for (ContractCollateralAgreement collateralAgreement : UniempDaoFacade.getUniempDAO().getContractCollateralAgreementsList(contract))
            if (collateralAgreement.getNumber().equals(extract.getAddAgreementNumber()) && collateralAgreement.getDate().equals(extract.getAddAgreementDate()))
            {
                agreement = collateralAgreement;
                break;
            }

        if (agreement == null)
        {
            agreement = new ContractCollateralAgreement();

            agreement.setContract(contract);
            agreement.setDate(extract.getAddAgreementDate());
            agreement.setNumber(extract.getAddAgreementNumber());
            agreement.setDescription("Установление ежемесячной надбавки»");

            extract.setContractAgreementNew(agreement);

            save(agreement);
        }

        update(extract);

        Double salary = 0d;
//        if (extract.getEntity() != null && extract.getEntity().getSalary() != null)
//            salary = extract.getEntity().getSalary();
//        else
        if (extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary() != null)
            salary = extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getSalary();

        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(employeePost);
        List<EmployeePayment> paymentList = CommonExtractCommitUtil.createOrAssignEmployeePaymentsList(getSession(), extract, employeePost, salary, staffRateItems);
        CommonExtractCommitUtil.validateEmployeePayment(extract.getEntity(), paymentList, getSession());
        if (UserContext.getInstance().getErrorCollector().hasErrors()) return;

        for (EmployeePayment payment : paymentList)
            getSession().saveOrUpdate(payment);


    }


    @Override
    public void doRollback(AdditionalPaymentExtract extract, Map parameters)
    {
        if (extract.getContractAgreementNew() != null)
        {
            delete(extract.getContractAgreementNew());
            extract.setContractAgreementNew(null);
        }

        update(extract);

        for (EmployeeBonus bonus : MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract))
            delete(bonus.getEntity());

    }


}