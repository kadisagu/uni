/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic.*;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
@Configuration
public class PgupsPlacesManager extends BusinessObjectManager
{
    public static PgupsPlacesManager instance() {return instance(PgupsPlacesManager.class);}

    @Bean
    public IPgupsPlacesDAO dao()
    {
        return new PgupsPlacesDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler createPlacesDataSource()
    {
        return new PgupsPlacesHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler createBuildingHandler()
    {
        return new PgupsBuildingHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler createFloorHandler()
    {
        return new PgupsFloorHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler createUnitHandler()
    {
        return new PgupsUnitHandler(getName());
    }

}