/* $Id$ */
package ru.tandemservice.unipgups.base.ext.Employee.ui.PostAddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEdit;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnUpdateEmployeePostExt;


/**
 * @author Ekaterina Zvereva
 * @since 28.01.2015
 */
@Configuration
public class EmployeePostAddEditExt extends BusinessComponentExtensionManager
{

    public static final String ADDON_NAME = "unipgups" + EmployeePostAddEditExtUI.class.getSimpleName();

    @Autowired
    private EmployeePostAddEdit _employeePostAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostAddEditExtUI.class))
                .create();
    }

    @Bean
    public ItemListExtension<IOnUpdateEmployeePostExt> updateExtItemList()
    {
        return itemListExtension(_employeePostAddEdit.updateExtItemList())
                .add(EmployeePostAddEditExtUI.class.getName(), new UpdateExt())
                .create();
    }

}