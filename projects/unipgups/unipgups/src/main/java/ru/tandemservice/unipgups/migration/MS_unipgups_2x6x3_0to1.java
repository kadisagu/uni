package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipgups_2x6x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.3"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.3")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность pgupsEnrForeignEntrant

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("pgupsenrforeignentrant_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("entrant_id", DBType.LONG).setNullable(false), 
				new DBColumn("contractnumber_p", DBType.createVarchar(255)), 
				new DBColumn("contractdate_p", DBType.DATE), 
				new DBColumn("externalorgunit_id", DBType.LONG), 
				new DBColumn("transcriptionlastname_p", DBType.createVarchar(255)), 
				new DBColumn("transcriptionfirstname_p", DBType.createVarchar(255)), 
				new DBColumn("transcriptionmiddlename_p", DBType.createVarchar(255)), 
				new DBColumn("viewvisa_id", DBType.LONG), 
				new DBColumn("purposevisit_p", DBType.createVarchar(1024)), 
				new DBColumn("visanumber_p", DBType.createVarchar(255)), 
				new DBColumn("authorityissuancevisa_p", DBType.createVarchar(255)), 
				new DBColumn("visadatefrom_p", DBType.DATE), 
				new DBColumn("visadateto_p", DBType.DATE), 
				new DBColumn("residencecountryaddress_p", DBType.createVarchar(1024)), 
				new DBColumn("migrationregaddress_p", DBType.createVarchar(1024)), 
				new DBColumn("migrationregdatefrom_p", DBType.DATE), 
				new DBColumn("migrationregdateto_p", DBType.DATE), 
				new DBColumn("actualaddress_p", DBType.createVarchar(1024)), 
				new DBColumn("contactphonenumber_p", DBType.createVarchar(255)), 
				new DBColumn("migrationcardnumber_p", DBType.createVarchar(255)), 
				new DBColumn("migrationcarddate_p", DBType.DATE), 
				new DBColumn("migrationcardborderpoint_p", DBType.createVarchar(255)), 
				new DBColumn("certificatenumber_p", DBType.createVarchar(255)), 
				new DBColumn("certificatedate_p", DBType.DATE), 
				new DBColumn("authorityissuancecertificate_p", DBType.createVarchar(255)), 
				new DBColumn("certificatedatefrom_p", DBType.DATE), 
				new DBColumn("certificatedateto_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("pgupsEnrForeignEntrant");

		}


    }
}