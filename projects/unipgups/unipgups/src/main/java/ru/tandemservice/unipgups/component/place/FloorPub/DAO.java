/* $Id$ */
package ru.tandemservice.unipgups.component.place.FloorPub;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.component.place.FloorPub.Model;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 20.03.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.FloorPub.DAO implements IDAO
{
    public static String PLACE_EXT = "extPlace";


    @Override
    public void prepareListDataSource(Model model)
    {
        prepareDocumentDataSource(model);

        DynamicListDataSource dataSource = model.getDataSource();

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(UniplacesPlace.class, "place")
                .addAdditionalAlias(UniPlacesPlaceExt.class, "extPlace").addAdditionalAlias(UniPgupsPlace.class, "pgupsPlace");
        orderRegistry.setOrders(UniPlacesPlaceExt.pgupsPlace().title(), new OrderDescription("pgupsPlace", UniPgupsPlace.title()), new OrderDescription("place.number"));
        orderRegistry.setOrders(UniPlacesPlaceExt.pgupsPlace().number(), new OrderDescription("pgupsPlace", UniPgupsPlace.number()), new OrderDescription("place.number"));


        DQLSelectBuilder builder = orderRegistry.buildDQLSelectBuilder()
                .joinEntity("place", DQLJoinType.left, UniPlacesPlaceExt.class, "extPlace", eq(property("place.id"), property("extPlace", UniPlacesPlaceExt.place().id())))
                .joinPath(DQLJoinType.left, UniPlacesPlaceExt.pgupsPlace().fromAlias("extPlace"), "pgupsPlace")
                .column(property("place"))
                .where(eq(property("place", UniplacesPlace.floor()), value(model.getFloor())));

        orderRegistry.applyOrderWithLeftJoins(builder, dataSource.getEntityOrder());

        UniBaseUtils.createPage(dataSource, builder, getSession());
        List<ViewWrapper<UniplacesPlace>> resultList= ViewWrapper.getPatchedList(dataSource);
        for(ViewWrapper<UniplacesPlace> item : resultList)
            item.setViewProperty(PLACE_EXT, get(UniPlacesPlaceExt.class, UniPlacesPlaceExt.place(), item.getEntity()));
    }

    @Override
    public void preparePgupsPlaceDataSource(ru.tandemservice.unipgups.component.place.FloorPub.Model model)
    {
        List<UniPgupsPlace> listPlaces = getList(UniPgupsPlace.class, UniPgupsPlace.floor(), model.getFloor());
        DynamicListDataSource<UniPgupsPlace> dataSource = model.getDataSourcePlace();
        dataSource.setCountRow(listPlaces.size() < 3 ? 3:listPlaces.size());
        UniBaseUtils.createPage(dataSource, listPlaces);
    }
}
