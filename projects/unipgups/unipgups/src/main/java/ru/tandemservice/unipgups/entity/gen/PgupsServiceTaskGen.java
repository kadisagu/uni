package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unipgups.entity.PgupsServiceTask;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Служебное задание
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsServiceTaskGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.PgupsServiceTask";
    public static final String ENTITY_NAME = "pgupsServiceTask";
    public static final int VERSION_HASH = 1702902525;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String P_TASK_DATE = "taskDate";
    public static final String P_TARGET = "target";

    private String _number;     // Номер
    private Date _taskDate;     // Дата задания
    private String _target;     // Цель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер.
     */
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Дата задания.
     */
    public Date getTaskDate()
    {
        return _taskDate;
    }

    /**
     * @param taskDate Дата задания.
     */
    public void setTaskDate(Date taskDate)
    {
        dirty(_taskDate, taskDate);
        _taskDate = taskDate;
    }

    /**
     * @return Цель. Свойство не может быть null.
     */
    @NotNull
    public String getTarget()
    {
        return _target;
    }

    /**
     * @param target Цель. Свойство не может быть null.
     */
    public void setTarget(String target)
    {
        dirty(_target, target);
        _target = target;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PgupsServiceTaskGen)
        {
            setNumber(((PgupsServiceTask)another).getNumber());
            setTaskDate(((PgupsServiceTask)another).getTaskDate());
            setTarget(((PgupsServiceTask)another).getTarget());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsServiceTaskGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsServiceTask.class;
        }

        public T newInstance()
        {
            return (T) new PgupsServiceTask();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "taskDate":
                    return obj.getTaskDate();
                case "target":
                    return obj.getTarget();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "taskDate":
                    obj.setTaskDate((Date) value);
                    return;
                case "target":
                    obj.setTarget((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "taskDate":
                        return true;
                case "target":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "taskDate":
                    return true;
                case "target":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "taskDate":
                    return Date.class;
                case "target":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsServiceTask> _dslPath = new Path<PgupsServiceTask>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsServiceTask");
    }
            

    /**
     * @return Номер.
     * @see ru.tandemservice.unipgups.entity.PgupsServiceTask#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Дата задания.
     * @see ru.tandemservice.unipgups.entity.PgupsServiceTask#getTaskDate()
     */
    public static PropertyPath<Date> taskDate()
    {
        return _dslPath.taskDate();
    }

    /**
     * @return Цель. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsServiceTask#getTarget()
     */
    public static PropertyPath<String> target()
    {
        return _dslPath.target();
    }

    public static class Path<E extends PgupsServiceTask> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private PropertyPath<Date> _taskDate;
        private PropertyPath<String> _target;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер.
     * @see ru.tandemservice.unipgups.entity.PgupsServiceTask#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(PgupsServiceTaskGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Дата задания.
     * @see ru.tandemservice.unipgups.entity.PgupsServiceTask#getTaskDate()
     */
        public PropertyPath<Date> taskDate()
        {
            if(_taskDate == null )
                _taskDate = new PropertyPath<Date>(PgupsServiceTaskGen.P_TASK_DATE, this);
            return _taskDate;
        }

    /**
     * @return Цель. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsServiceTask#getTarget()
     */
        public PropertyPath<String> target()
        {
            if(_target == null )
                _target = new PropertyPath<String>(PgupsServiceTaskGen.P_TARGET, this);
            return _target;
        }

        public Class getEntityClass()
        {
            return PgupsServiceTask.class;
        }

        public String getEntityName()
        {
            return "pgupsServiceTask";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
