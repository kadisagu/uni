/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubController;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 31.08.2015
 */
public class Controller extends SingleEmployeeExtractPubController<ChangeSalaryEmployeePostExtract, IDAO, Model>
{
}