/* $Id$ */


package ru.tandemservice.moveemployee.component.singleemplextract.e100.AddEdit;

import org.apache.hivemind.Location;
import org.apache.hivemind.Resource;
import org.apache.tapestry.IAsset;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<CombinationPostExtract>
{
    private ISingleSelectModel _orgUnitModel;
    private ISingleSelectModel _postModel;
    private ISingleSelectModel _missingEmployeePostModel;
    private ISingleSelectModel _etksLevelModel;
    private IMultiSelectModel _employeeHRModel;
    private ISingleSelectModel _financingSourceItemModel;
    private ISingleSelectModel _financingSourceModel;
    private ISingleSelectModel _contractTypeModel;
    private ISingleSelectModel _combinationPostTypeModel;

    private DynamicListDataSource<CombinationPostStaffRateExtractRelation> _staffRateDataSource;
    private DynamicListDataSource<EmployeePostStaffRateItem> _employeeStaffRateDataSource;

    private List<CombinationPostStaffRateExtractRelation> _combinationPostStaffRateItemList = new ArrayList<>();

    private boolean _needUpdateDataSource = true;
    private boolean _thereAnyActiveStaffList = false;
    private boolean _disabledContractFields = true;

    private IAsset _asset = new IAsset()
    {
        @Override
        public String buildURL()
        {
            return "img/general/add.png";
        }

        @Override
        public InputStream getResourceAsStream()
        {
            return null;
        }

        @Override
        public Resource getResourceLocation()
        {
            return null;
        }

        @Override
        public Location getLocation()
        {
            return null;
        }
    };

    //Calculate

    public String getStaffRateColumnId()
    {
        return "staffRate_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcColumnId()
    {
        return "finSrc_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getFinSrcItmColumnId()
    {
        return "finSrcItm_id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    public String getEmployeeHRId()
    {
        return "employeeHR_Id_" + getStaffRateDataSource().getCurrentEntity().getId();
    }

    //Getters & Setters

    public ISingleSelectModel getCombinationPostTypeModel()
    {
        return _combinationPostTypeModel;
    }

    public void setCombinationPostTypeModel(ISingleSelectModel combinationPostTypeModel)
    {
        _combinationPostTypeModel = combinationPostTypeModel;
    }

    public ISingleSelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISingleSelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public ISingleSelectModel getPostModel()
    {
        return _postModel;
    }

    public void setPostModel(ISingleSelectModel postModel)
    {
        _postModel = postModel;
    }

    public ISingleSelectModel getMissingEmployeePostModel()
    {
        return _missingEmployeePostModel;
    }

    public void setMissingEmployeePostModel(ISingleSelectModel missingEmployeePostModel)
    {
        _missingEmployeePostModel = missingEmployeePostModel;
    }


    public ISingleSelectModel getEtksLevelModel()
    {
        return _etksLevelModel;
    }

    public void setEtksLevelModel(ISingleSelectModel etksLevelModel)
    {
        _etksLevelModel = etksLevelModel;
    }

    public IMultiSelectModel getEmployeeHRModel()
    {
        return _employeeHRModel;
    }

    public void setEmployeeHRModel(IMultiSelectModel employeeHRModel)
    {
        _employeeHRModel = employeeHRModel;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getContractTypeModel()
    {
        return _contractTypeModel;
    }

    public void setContractTypeModel(ISingleSelectModel contractTypeModel)
    {
        _contractTypeModel = contractTypeModel;
    }

    public DynamicListDataSource<CombinationPostStaffRateExtractRelation> getStaffRateDataSource()
    {
        return _staffRateDataSource;
    }

    public void setStaffRateDataSource(DynamicListDataSource<CombinationPostStaffRateExtractRelation> staffRateDataSource)
    {
        _staffRateDataSource = staffRateDataSource;
    }

    public DynamicListDataSource<EmployeePostStaffRateItem> getEmployeeStaffRateDataSource()
    {
        return _employeeStaffRateDataSource;
    }

    public void setEmployeeStaffRateDataSource(DynamicListDataSource<EmployeePostStaffRateItem> employeeStaffRateDataSource)
    {
        _employeeStaffRateDataSource = employeeStaffRateDataSource;
    }

    public List<CombinationPostStaffRateExtractRelation> getCombinationPostStaffRateItemList()
    {
        return _combinationPostStaffRateItemList;
    }

    public void setCombinationPostStaffRateItemList(List<CombinationPostStaffRateExtractRelation> combinationPostStaffRateItemList)
    {
        _combinationPostStaffRateItemList = combinationPostStaffRateItemList;
    }

    public boolean isNeedUpdateDataSource()
    {
        return _needUpdateDataSource;
    }

    public void setNeedUpdateDataSource(boolean needUpdateDataSource)
    {
        _needUpdateDataSource = needUpdateDataSource;
    }

    public boolean isThereAnyActiveStaffList()
    {
        return _thereAnyActiveStaffList;
    }

    public void setThereAnyActiveStaffList(boolean thereAnyActiveStaffList)
    {
        _thereAnyActiveStaffList = thereAnyActiveStaffList;
    }

    public boolean isDisabledContractFields()
    {
        return _disabledContractFields;
    }

    public void setDisabledContractFields(boolean disabledContractFields)
    {
        _disabledContractFields = disabledContractFields;
    }

    public IAsset getAsset()
    {
        return _asset;
    }

    public void setAsset(IAsset asset)
    {
        _asset = asset;
    }

}