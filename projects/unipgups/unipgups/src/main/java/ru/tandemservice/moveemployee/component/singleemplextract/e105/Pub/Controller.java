/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubController;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class Controller extends SingleEmployeeExtractPubController<PgupsEmployeeMissionExtract, IDAO, Model>
{
}