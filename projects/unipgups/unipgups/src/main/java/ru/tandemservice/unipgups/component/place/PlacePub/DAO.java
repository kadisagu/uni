/* $Id$ */
package ru.tandemservice.unipgups.component.place.PlacePub;


import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;

/**
 * @author Ekaterina Zvereva
 * @since 10.02.2015
 */
public class DAO extends ru.tandemservice.uniplaces.component.place.PlacePub.DAO
{
    @Override
    public void prepare(ru.tandemservice.uniplaces.component.place.PlacePub.Model model)
    {
        super.prepare(model);
        UniPlacesPlaceExt placeExt = getByNaturalId(new UniPlacesPlaceExt.NaturalId(model.getPlace()));
        ((ru.tandemservice.unipgups.component.place.PlacePub.Model)model).setPlaceExt(placeExt);
    }
}