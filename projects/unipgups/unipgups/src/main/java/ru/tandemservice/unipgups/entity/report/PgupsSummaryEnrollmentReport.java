package ru.tandemservice.unipgups.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.IPgupsEcReport;
import ru.tandemservice.unipgups.entity.report.gen.*;

/**
 * Сводка о ходе приема, средних баллах, договорах (ПГУПС)
 */
public class PgupsSummaryEnrollmentReport extends PgupsSummaryEnrollmentReportGen implements IPgupsEcReport
{
    @EntityDSLSupport
    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}