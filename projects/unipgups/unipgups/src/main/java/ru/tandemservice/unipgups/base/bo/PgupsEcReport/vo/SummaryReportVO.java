/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo;

import org.tandemframework.core.common.ITitledEntity;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportParam;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportParam;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.IPgupsEcReport;
import ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 18.07.2013
 */
public class SummaryReportVO implements IPgupsEcReportVO
{
    private ITitledEntity enrollmentCampaignStep;
    private IReportParam<List<StudentCategory>> studentCategory = new ReportParam<>();
    private IReportParam<List<Qualifications>> qualification = new ReportParam<>();
    private IReportParam<CompensationType> compensationType = new ReportParam<>();
    private IReportParam<List<DevelopForm>> developForm = new ReportParam<>();
    private IReportParam<List<DevelopCondition>> developCondition = new ReportParam<>();
    private IReportParam<List<DevelopTech>> developTech = new ReportParam<>();
    private IReportParam<List<DevelopPeriod>> developPeriod = new ReportParam<>();

    public void fillReport(IPgupsEcReport report)
    {
        PgupsSummaryEnrollmentReport rep = (PgupsSummaryEnrollmentReport) report;
        rep.setEnrollmentCampaignStepStr(enrollmentCampaignStep.getTitle());

        if (studentCategory.isActive())
            rep.setStudentCategoryStr(CommonBaseStringUtil.join(studentCategory.getData(), StudentCategory.P_TITLE, "; "));

        if (qualification.isActive())
            rep.setQualificationStr(CommonBaseStringUtil.join(qualification.getData(), Qualifications.P_TITLE, "; "));

        if (compensationType.isActive())
            rep.setCompensationTypeStr(compensationType.getData().getShortTitle());

        if (developForm.isActive())
            rep.setDevelopFormStr(CommonBaseStringUtil.join(developForm.getData(), DevelopForm.P_TITLE, "; "));

        if (developCondition.isActive())
            rep.setDevelopConditionStr(CommonBaseStringUtil.join(developCondition.getData(), DevelopCondition.P_TITLE, "; "));

        if (developTech.isActive())
            rep.setDevelopTechStr(CommonBaseStringUtil.join(developTech.getData(), DevelopTech.P_TITLE, "; "));

        if (developPeriod.isActive())
            rep.setDevelopPeriodStr(CommonBaseStringUtil.join(developPeriod.getData(), DevelopPeriod.P_TITLE, "; "));
    }

    public ITitledEntity getEnrollmentCampaignStep()
    {
        return enrollmentCampaignStep;
    }

    public void setEnrollmentCampaignStep(ITitledEntity enrollmentCampaignStep)
    {
        this.enrollmentCampaignStep = enrollmentCampaignStep;
    }

    public IReportParam<List<StudentCategory>> getStudentCategory()
    {
        return studentCategory;
    }

    public IReportParam<List<Qualifications>> getQualification()
    {
        return qualification;
    }

    public IReportParam<CompensationType> getCompensationType()
    {
        return compensationType;
    }

    public IReportParam<List<DevelopForm>> getDevelopForm()
    {
        return developForm;
    }

    public IReportParam<List<DevelopCondition>> getDevelopCondition()
    {
        return developCondition;
    }

    public IReportParam<List<DevelopTech>> getDevelopTech()
    {
        return developTech;
    }

    public IReportParam<List<DevelopPeriod>> getDevelopPeriod()
    {
        return developPeriod;
    }
}