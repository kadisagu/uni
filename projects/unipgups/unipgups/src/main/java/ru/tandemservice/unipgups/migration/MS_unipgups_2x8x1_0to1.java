package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipgups_2x8x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniload_demo отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniload_demo") )
				throw new RuntimeException("Module 'uniload_demo' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "uniload_demo");

		// удалить персистентный интерфейс ru.tandemservice.uniload_demo.entity.ILoadImEduGroup
		{
			// удалить view
			tool.dropView("iloadimedugroup_v");
		}

		// удалить сущность timeRule2EduGroup
		{
			// удалить таблицу
			tool.dropTable("timerule2edugroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("timeRule2EduGroup");
		}

		// удалить сущность ppsLoadType2TimeRule
		{
			// удалить таблицу
			tool.dropTable("ppsloadtype2timerule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ppsLoadType2TimeRule");
		}

		// удалить сущность ppsLoadType
		{
			// удалить таблицу
			tool.dropTable("ppsloadtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ppsLoadType");
		}

		// удалить сущность ppsLoadGroup2TimeRule
		{
			// удалить таблицу
			tool.dropTable("ppsloadgroup2timerule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ppsLoadGroup2TimeRule");
		}

		// удалить сущность ppsLoadGroup
		{
			// удалить таблицу
			tool.dropTable("ppsloadgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ppsLoadGroup");
		}

		// удалить сущность ppsLoadCathedraPart
		{
			// удалить таблицу
			tool.dropTable("ppsloadcathedrapart_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ppsLoadCathedraPart");
		}

		// удалить сущность otherLoad
		{
			// удалить таблицу
			tool.dropTable("otherload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("otherLoad");
		}

		// удалить сущность orgUnitLoadState
		{
			// удалить таблицу
			tool.dropTable("orgunitloadstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("orgUnitLoadState");
		}

		// удалить сущность loadTimeRule
		{
			// удалить таблицу
			tool.dropTable("loadtimerule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadTimeRule");
		}

		// удалить сущность loadSize
		{
			// удалить таблицу
			tool.dropTable("loadsize_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadSize");
		}

		// удалить сущность loadRecordOwner
		{
			// удалить таблицу
			tool.dropTable("loadrecordowner_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadRecordOwner");
		}

		// удалить сущность loadRealPpsEntry
		{
			// удалить таблицу
			tool.dropTable("loadrealppsentry_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadRealPpsEntry");
		}

		// удалить сущность loadImEduGroupTimeRuleRel
		{
			// удалить таблицу
			tool.dropTable("loadimedugrouptimerulerel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadImEduGroupTimeRuleRel");
		}

		// удалить сущность loadFirstCourseImEduGroup
		{
			// удалить таблицу
			tool.dropTable("loadfirstcourseimedugroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadFirstCourseImEduGroup");
		}

		// удалить сущность loadFakePpsEntry
		{
			// удалить таблицу
			tool.dropTable("loadfakeppsentry_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadFakePpsEntry");
		}

		// удалить сущность loadEduGroupPpsDistr
		{
			// удалить таблицу
			tool.dropTable("loadedugroupppsdistr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadEduGroupPpsDistr");
		}

		// удалить сущность loadDeviation
		{
			// удалить таблицу
			tool.dropTable("loaddeviation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadDeviation");
		}

		// удалить сущность loadCathedraPart2EppPlanStructure
		{
			// удалить таблицу
			tool.dropTable("cathedrapart2planstruct_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadCathedraPart2EppPlanStructure");
		}
    }
}