//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.07.02 at 01:39:16 PM YEKT 
//


package ru.tandemservice.nsiclient.datagram;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;


/**
 * Элемент справочника Информация о контингенте, изучающем дисциплины учебной программы
 *
 * <p>Java class for EduProgramDisciplineStatisticsType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EduProgramDisciplineStatisticsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="ID" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}Guid" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticsDevelopFormName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticsCourseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticsTermNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticsQualificationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticsOKSO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticsProfileOKSO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticDepartmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EduProgramDisciplineStatisticPlanStudentsAmount" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}NlInteger" minOccurs="0"/>
 *         &lt;element name="EducationalProgramID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="EducationalProgram" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EducationalProgramType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DepartmentID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Department" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DepartmentType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CourseID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Course" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CourseType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="QualificationID" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="Qualification" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}QualificationType" minOccurs="0"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *       &lt;attGroup ref="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CommonAttributeGroup"/>
 *       &lt;anyAttribute namespace='##other'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EduProgramDisciplineStatisticsType", propOrder = {

})
public class EduProgramDisciplineStatisticsType implements IDatagramObject
{

    @XmlElement(name = "ID")
    protected String id;
    @XmlElement(name = "EduProgramDisciplineStatisticsDevelopFormName")
    protected String eduProgramDisciplineStatisticsDevelopFormName;
    @XmlElement(name = "EduProgramDisciplineStatisticsCourseNumber")
    protected String eduProgramDisciplineStatisticsCourseNumber;
    @XmlElement(name = "EduProgramDisciplineStatisticsTermNumber")
    protected String eduProgramDisciplineStatisticsTermNumber;
    @XmlElement(name = "EduProgramDisciplineStatisticsQualificationName")
    protected String eduProgramDisciplineStatisticsQualificationName;
    @XmlElement(name = "EduProgramDisciplineStatisticsOKSO")
    protected String eduProgramDisciplineStatisticsOKSO;
    @XmlElement(name = "EduProgramDisciplineStatisticsProfileOKSO")
    protected String eduProgramDisciplineStatisticsProfileOKSO;
    @XmlElement(name = "EduProgramDisciplineStatisticDepartmentName")
    protected String eduProgramDisciplineStatisticDepartmentName;
    @XmlElement(name = "EduProgramDisciplineStatisticPlanStudentsAmount")
    protected String eduProgramDisciplineStatisticPlanStudentsAmount;
    @XmlElement(name = "EducationalProgramID")
    protected EducationalProgramID educationalProgramID;
    @XmlElement(name = "DepartmentID")
    protected DepartmentID departmentID;
    @XmlElement(name = "CourseID")
    protected CourseID courseID;
    @XmlElement(name = "ProgramQualificationID")
    protected ProgramQualificationID programQualificationID;
    @XmlAttribute(name = "oid")
    protected String oid;
    @XmlAttribute(name = "new")
    protected Short _new;
    @XmlAttribute(name = "delete")
    protected Short delete;
    @XmlAttribute(name = "change")
    protected Short change;
    @XmlAttribute(name = "ts")
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger ts;
    @XmlAttribute(name = "mergeDublicates")
    protected String mergeDublicates;
    @XmlAttribute(name = "error")
    protected Short error;
    @XmlAttribute(name = "analogs")
    protected String analogs;
    @XmlAttribute(name = "isNotConsistent")
    protected Short isNotConsistent;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the id property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setID(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticsDevelopFormName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticsDevelopFormName() {
        return eduProgramDisciplineStatisticsDevelopFormName;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticsDevelopFormName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticsDevelopFormName(String value) {
        this.eduProgramDisciplineStatisticsDevelopFormName = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticsCourseNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticsCourseNumber() {
        return eduProgramDisciplineStatisticsCourseNumber;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticsCourseNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticsCourseNumber(String value) {
        this.eduProgramDisciplineStatisticsCourseNumber = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticsTermNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticsTermNumber() {
        return eduProgramDisciplineStatisticsTermNumber;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticsTermNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticsTermNumber(String value) {
        this.eduProgramDisciplineStatisticsTermNumber = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticsQualificationName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticsQualificationName() {
        return eduProgramDisciplineStatisticsQualificationName;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticsQualificationName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticsQualificationName(String value) {
        this.eduProgramDisciplineStatisticsQualificationName = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticsOKSO property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticsOKSO() {
        return eduProgramDisciplineStatisticsOKSO;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticsOKSO property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticsOKSO(String value) {
        this.eduProgramDisciplineStatisticsOKSO = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticsProfileOKSO property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticsProfileOKSO() {
        return eduProgramDisciplineStatisticsProfileOKSO;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticsProfileOKSO property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticsProfileOKSO(String value) {
        this.eduProgramDisciplineStatisticsProfileOKSO = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticDepartmentName property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticDepartmentName() {
        return eduProgramDisciplineStatisticDepartmentName;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticDepartmentName property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticDepartmentName(String value) {
        this.eduProgramDisciplineStatisticDepartmentName = value;
    }

    /**
     * Gets the value of the eduProgramDisciplineStatisticPlanStudentsAmount property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getEduProgramDisciplineStatisticPlanStudentsAmount() {
        return eduProgramDisciplineStatisticPlanStudentsAmount;
    }

    /**
     * Sets the value of the eduProgramDisciplineStatisticPlanStudentsAmount property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setEduProgramDisciplineStatisticPlanStudentsAmount(String value) {
        this.eduProgramDisciplineStatisticPlanStudentsAmount = value;
    }

    /**
     * Gets the value of the educationalProgramID property.
     *
     * @return
     *     possible object is
     *     {@link EduProgramDisciplineStatisticsType.EducationalProgramID }
     *
     */
    public EducationalProgramID getEducationalProgramID() {
        return educationalProgramID;
    }

    /**
     * Sets the value of the educationalProgramID property.
     *
     * @param value
     *     allowed object is
     *     {@link EduProgramDisciplineStatisticsType.EducationalProgramID }
     *
     */
    public void setEducationalProgramID(EducationalProgramID value) {
        this.educationalProgramID = value;
    }

    /**
     * Gets the value of the departmentID property.
     *
     * @return
     *     possible object is
     *     {@link EduProgramDisciplineStatisticsType.DepartmentID }
     *
     */
    public DepartmentID getDepartmentID() {
        return departmentID;
    }

    /**
     * Sets the value of the departmentID property.
     *
     * @param value
     *     allowed object is
     *     {@link EduProgramDisciplineStatisticsType.DepartmentID }
     *
     */
    public void setDepartmentID(DepartmentID value) {
        this.departmentID = value;
    }

    /**
     * Gets the value of the courseID property.
     *
     * @return
     *     possible object is
     *     {@link EduProgramDisciplineStatisticsType.CourseID }
     *
     */
    public CourseID getCourseID() {
        return courseID;
    }

    /**
     * Sets the value of the courseID property.
     *
     * @param value
     *     allowed object is
     *     {@link EduProgramDisciplineStatisticsType.CourseID }
     *
     */
    public void setCourseID(CourseID value) {
        this.courseID = value;
    }

    /**
     * Gets the value of the qualificationID property.
     *
     * @return
     *     possible object is
     *     {@link EduProgramDisciplineStatisticsType.ProgramQualificationID }
     *
     */
    public ProgramQualificationID getProgramQualificationID() {
        return programQualificationID;
    }

    /**
     * Sets the value of the qualificationID property.
     *
     * @param value
     *     allowed object is
     *     {@link EduProgramDisciplineStatisticsType.ProgramQualificationID }
     *
     */
    public void setProgramQualificationID(ProgramQualificationID value) {
        this.programQualificationID = value;
    }

    /**
     * Gets the value of the oid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOid() {
        return oid;
    }

    /**
     * Sets the value of the oid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOid(String value) {
        this.oid = value;
    }

    /**
     * Gets the value of the new property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setNew(Short value) {
        this._new = value;
    }

    /**
     * Gets the value of the delete property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getDelete() {
        return delete;
    }

    /**
     * Sets the value of the delete property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setDelete(Short value) {
        this.delete = value;
    }

    /**
     * Gets the value of the change property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getChange() {
        return change;
    }

    /**
     * Sets the value of the change property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setChange(Short value) {
        this.change = value;
    }

    /**
     * Gets the value of the ts property.
     *
     * @return
     *     possible object is
     *     {@link java.math.BigInteger }
     *
     */
    public BigInteger getTs() {
        return ts;
    }

    /**
     * Sets the value of the ts property.
     *
     * @param value
     *     allowed object is
     *     {@link java.math.BigInteger }
     *
     */
    public void setTs(BigInteger value) {
        this.ts = value;
    }

    /**
     * Gets the value of the mergeDublicates property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMergeDublicates() {
        return mergeDublicates;
    }

    /**
     * Sets the value of the mergeDublicates property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMergeDublicates(String value) {
        this.mergeDublicates = value;
    }

    /**
     * Gets the value of the error property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setError(Short value) {
        this.error = value;
    }

    /**
     * Gets the value of the analogs property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAnalogs() {
        return analogs;
    }

    /**
     * Sets the value of the analogs property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAnalogs(String value) {
        this.analogs = value;
    }

    /**
     * Gets the value of the isNotConsistent property.
     *
     * @return
     *     possible object is
     *     {@link Short }
     *
     */
    public Short getIsNotConsistent() {
        return isNotConsistent;
    }

    /**
     * Sets the value of the isNotConsistent property.
     *
     * @param value
     *     allowed object is
     *     {@link Short }
     *
     */
    public void setIsNotConsistent(Short value) {
        this.isNotConsistent = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     *
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     *
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     *
     *
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Course" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}CourseType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class CourseID {

        @XmlElement(name = "Course")
        protected CourseType course;

        /**
         * Gets the value of the course property.
         *
         * @return
         *     possible object is
         *     {@link CourseType }
         *
         */
        public CourseType getCourse() {
            return course;
        }

        /**
         * Sets the value of the course property.
         *
         * @param value
         *     allowed object is
         *     {@link CourseType }
         *
         */
        public void setCourse(CourseType value) {
            this.course = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Department" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}DepartmentType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class DepartmentID {

        @XmlElement(name = "Department")
        protected DepartmentType department;

        /**
         * Gets the value of the department property.
         *
         * @return
         *     possible object is
         *     {@link DepartmentType }
         *
         */
        public DepartmentType getDepartment() {
            return department;
        }

        /**
         * Sets the value of the department property.
         *
         * @param value
         *     allowed object is
         *     {@link DepartmentType }
         *
         */
        public void setDepartment(DepartmentType value) {
            this.department = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="EducationalProgram" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}EducationalProgramType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class EducationalProgramID {

        @XmlElement(name = "EducationalProgram")
        protected EducationalProgramType educationalProgram;

        /**
         * Gets the value of the educationalProgram property.
         *
         * @return
         *     possible object is
         *     {@link EducationalProgramType }
         *
         */
        public EducationalProgramType getEducationalProgram() {
            return educationalProgram;
        }

        /**
         * Sets the value of the educationalProgram property.
         *
         * @param value
         *     allowed object is
         *     {@link EducationalProgramType }
         *
         */
        public void setEducationalProgram(EducationalProgramType value) {
            this.educationalProgram = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="Qualification" type="{http://www.tandemservice.ru/Schemas/Tandem/Nsi/Datagram/1.0}QualificationType" minOccurs="0"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class ProgramQualificationID {

        @XmlElement(name = "ProgramQualification")
        protected EduProgramQualificationType programQualification;

        /**
         * Gets the value of the qualification property.
         *
         * @return
         *     possible object is
         *     {@link EduProgramQualificationType }
         *
         */
        public EduProgramQualificationType getProgramQualification() {
            return programQualification;
        }

        /**
         * Sets the value of the qualification property.
         *
         * @param value
         *     allowed object is
         *     {@link EduProgramQualificationType }
         *
         */
        public void setProgramQualification(EduProgramQualificationType value) {
            this.programQualification = value;
        }

    }

}
