/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public class PgupsPlacesDAO extends UniBaseDao implements IPgupsPlacesDAO
{
    @Override
    public boolean existRelatedObjects(Long placeId)
    {
        return existsEntity(UniPlacesPlaceExt.class, UniPlacesPlaceExt.L_PGUPS_PLACE, placeId);
    }
}