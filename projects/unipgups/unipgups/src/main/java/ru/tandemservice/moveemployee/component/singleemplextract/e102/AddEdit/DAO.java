/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<ProlongationContractExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    @Override
    protected ProlongationContractExtract createNewInstance()
    {
        return new ProlongationContractExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.isAddForm())
            model.getExtract().setLabourContract(UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost()));
        else
            model.setEmployeePost(model.getExtract().getLabourContract().getEmployeePost());
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (!model.getExtract().getNewEndDate().after(model.getExtract().getLabourContract().getEndDate()))
            errors.add("Новая дата окончания действия трудового договора должна быть позже текущей даты окончания.", "newEndDate");
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());
        model.getExtract().setOldEndDate(model.getExtract().getLabourContract().getEndDate());
    }
}