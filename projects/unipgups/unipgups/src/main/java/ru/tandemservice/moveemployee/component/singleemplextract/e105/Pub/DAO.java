/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105.Pub;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class DAO extends SingleEmployeeExtractPubDAO<PgupsEmployeeMissionExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.getExtract().getExternalOrgUnit().getFactAddress() != null && model.getExtract().getExternalOrgUnit().getFactAddress().getSettlement()!= null)
            model.setSettlement(model.getExtract().getExternalOrgUnit().getFactAddress().getSettlement().getTitleWithType());
        StringBuilder taskBuilder = new StringBuilder().append(model.getExtract().getTaskNumber() != null? "№ " + model.getExtract().getTaskNumber() : "")
                .append(model.getExtract().getTaskDate() != null? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getTaskDate()) : "");
        if (taskBuilder.length() > 0)
            taskBuilder.append(", ");
        taskBuilder.append("цель - ").append(model.getExtract().getPurposeText());
        model.setPurposeText(taskBuilder.toString());

        StringBuilder warrantBuilder = new StringBuilder().append(model.getExtract().getWarrantNumber() != null? "№ " + model.getExtract().getWarrantNumber() : "")
                .append(model.getExtract().getWarrantDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getWarrantDate()) : "");
        model.setWarrant(warrantBuilder.toString());

        if (model.getExtract().isInternalFinancingSource())
            model.setFinansingSource("Финансируется отправляющей стороной");
        else
            model.setFinansingSource(model.getExtract().getFinancingSourceOrgUnit().getTitleWithLegalForm());
    }
}