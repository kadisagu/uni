package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об установлении нового должностного оклада
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ChangeSalaryEmployeePostExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract";
    public static final String ENTITY_NAME = "changeSalaryEmployeePostExtract";
    public static final int VERSION_HASH = 806750821;
    private static IEntityMeta ENTITY_META;

    public static final String P_NEW_SALARY = "newSalary";
    public static final String P_OLD_SALARY = "oldSalary";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_CONTRACT_ADD_AGREEMENT_NUMBER = "contractAddAgreementNumber";
    public static final String P_CONTRACT_ADD_AGREEMENT_DATE = "contractAddAgreementDate";
    public static final String L_CREATE_COLLATERAL_AGREEMENT = "createCollateralAgreement";

    private double _newSalary;     // Новый оклад
    private Double _oldSalary;     // Оклад до проведения приказа
    private Date _startDate;     // Дата начала действия нового оклада
    private Date _endDate;     // Дата окончания действия оклада
    private String _contractAddAgreementNumber;     // Номер доп. соглашения
    private Date _contractAddAgreementDate;     // Дата доп. соглашения
    private ContractCollateralAgreement _createCollateralAgreement;     // Доп. соглашение созданное при проведении выписки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Новый оклад. Свойство не может быть null.
     */
    @NotNull
    public double getNewSalary()
    {
        return _newSalary;
    }

    /**
     * @param newSalary Новый оклад. Свойство не может быть null.
     */
    public void setNewSalary(double newSalary)
    {
        dirty(_newSalary, newSalary);
        _newSalary = newSalary;
    }

    /**
     * @return Оклад до проведения приказа.
     */
    public Double getOldSalary()
    {
        return _oldSalary;
    }

    /**
     * @param oldSalary Оклад до проведения приказа.
     */
    public void setOldSalary(Double oldSalary)
    {
        dirty(_oldSalary, oldSalary);
        _oldSalary = oldSalary;
    }

    /**
     * @return Дата начала действия нового оклада. Свойство не может быть null.
     */
    @NotNull
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала действия нового оклада. Свойство не может быть null.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания действия оклада.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания действия оклада.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractAddAgreementNumber()
    {
        return _contractAddAgreementNumber;
    }

    /**
     * @param contractAddAgreementNumber Номер доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementNumber(String contractAddAgreementNumber)
    {
        dirty(_contractAddAgreementNumber, contractAddAgreementNumber);
        _contractAddAgreementNumber = contractAddAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    public Date getContractAddAgreementDate()
    {
        return _contractAddAgreementDate;
    }

    /**
     * @param contractAddAgreementDate Дата доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementDate(Date contractAddAgreementDate)
    {
        dirty(_contractAddAgreementDate, contractAddAgreementDate);
        _contractAddAgreementDate = contractAddAgreementDate;
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     */
    public ContractCollateralAgreement getCreateCollateralAgreement()
    {
        return _createCollateralAgreement;
    }

    /**
     * @param createCollateralAgreement Доп. соглашение созданное при проведении выписки.
     */
    public void setCreateCollateralAgreement(ContractCollateralAgreement createCollateralAgreement)
    {
        dirty(_createCollateralAgreement, createCollateralAgreement);
        _createCollateralAgreement = createCollateralAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof ChangeSalaryEmployeePostExtractGen)
        {
            setNewSalary(((ChangeSalaryEmployeePostExtract)another).getNewSalary());
            setOldSalary(((ChangeSalaryEmployeePostExtract)another).getOldSalary());
            setStartDate(((ChangeSalaryEmployeePostExtract)another).getStartDate());
            setEndDate(((ChangeSalaryEmployeePostExtract)another).getEndDate());
            setContractAddAgreementNumber(((ChangeSalaryEmployeePostExtract)another).getContractAddAgreementNumber());
            setContractAddAgreementDate(((ChangeSalaryEmployeePostExtract)another).getContractAddAgreementDate());
            setCreateCollateralAgreement(((ChangeSalaryEmployeePostExtract)another).getCreateCollateralAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ChangeSalaryEmployeePostExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ChangeSalaryEmployeePostExtract.class;
        }

        public T newInstance()
        {
            return (T) new ChangeSalaryEmployeePostExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "newSalary":
                    return obj.getNewSalary();
                case "oldSalary":
                    return obj.getOldSalary();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "contractAddAgreementNumber":
                    return obj.getContractAddAgreementNumber();
                case "contractAddAgreementDate":
                    return obj.getContractAddAgreementDate();
                case "createCollateralAgreement":
                    return obj.getCreateCollateralAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "newSalary":
                    obj.setNewSalary((Double) value);
                    return;
                case "oldSalary":
                    obj.setOldSalary((Double) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "contractAddAgreementNumber":
                    obj.setContractAddAgreementNumber((String) value);
                    return;
                case "contractAddAgreementDate":
                    obj.setContractAddAgreementDate((Date) value);
                    return;
                case "createCollateralAgreement":
                    obj.setCreateCollateralAgreement((ContractCollateralAgreement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newSalary":
                        return true;
                case "oldSalary":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "contractAddAgreementNumber":
                        return true;
                case "contractAddAgreementDate":
                        return true;
                case "createCollateralAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "newSalary":
                    return true;
                case "oldSalary":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "contractAddAgreementNumber":
                    return true;
                case "contractAddAgreementDate":
                    return true;
                case "createCollateralAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "newSalary":
                    return Double.class;
                case "oldSalary":
                    return Double.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "contractAddAgreementNumber":
                    return String.class;
                case "contractAddAgreementDate":
                    return Date.class;
                case "createCollateralAgreement":
                    return ContractCollateralAgreement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ChangeSalaryEmployeePostExtract> _dslPath = new Path<ChangeSalaryEmployeePostExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ChangeSalaryEmployeePostExtract");
    }
            

    /**
     * @return Новый оклад. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getNewSalary()
     */
    public static PropertyPath<Double> newSalary()
    {
        return _dslPath.newSalary();
    }

    /**
     * @return Оклад до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getOldSalary()
     */
    public static PropertyPath<Double> oldSalary()
    {
        return _dslPath.oldSalary();
    }

    /**
     * @return Дата начала действия нового оклада. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания действия оклада.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getContractAddAgreementNumber()
     */
    public static PropertyPath<String> contractAddAgreementNumber()
    {
        return _dslPath.contractAddAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getContractAddAgreementDate()
     */
    public static PropertyPath<Date> contractAddAgreementDate()
    {
        return _dslPath.contractAddAgreementDate();
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getCreateCollateralAgreement()
     */
    public static ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
    {
        return _dslPath.createCollateralAgreement();
    }

    public static class Path<E extends ChangeSalaryEmployeePostExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<Double> _newSalary;
        private PropertyPath<Double> _oldSalary;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _contractAddAgreementNumber;
        private PropertyPath<Date> _contractAddAgreementDate;
        private ContractCollateralAgreement.Path<ContractCollateralAgreement> _createCollateralAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Новый оклад. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getNewSalary()
     */
        public PropertyPath<Double> newSalary()
        {
            if(_newSalary == null )
                _newSalary = new PropertyPath<Double>(ChangeSalaryEmployeePostExtractGen.P_NEW_SALARY, this);
            return _newSalary;
        }

    /**
     * @return Оклад до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getOldSalary()
     */
        public PropertyPath<Double> oldSalary()
        {
            if(_oldSalary == null )
                _oldSalary = new PropertyPath<Double>(ChangeSalaryEmployeePostExtractGen.P_OLD_SALARY, this);
            return _oldSalary;
        }

    /**
     * @return Дата начала действия нового оклада. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(ChangeSalaryEmployeePostExtractGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания действия оклада.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(ChangeSalaryEmployeePostExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getContractAddAgreementNumber()
     */
        public PropertyPath<String> contractAddAgreementNumber()
        {
            if(_contractAddAgreementNumber == null )
                _contractAddAgreementNumber = new PropertyPath<String>(ChangeSalaryEmployeePostExtractGen.P_CONTRACT_ADD_AGREEMENT_NUMBER, this);
            return _contractAddAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getContractAddAgreementDate()
     */
        public PropertyPath<Date> contractAddAgreementDate()
        {
            if(_contractAddAgreementDate == null )
                _contractAddAgreementDate = new PropertyPath<Date>(ChangeSalaryEmployeePostExtractGen.P_CONTRACT_ADD_AGREEMENT_DATE, this);
            return _contractAddAgreementDate;
        }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract#getCreateCollateralAgreement()
     */
        public ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
        {
            if(_createCollateralAgreement == null )
                _createCollateralAgreement = new ContractCollateralAgreement.Path<ContractCollateralAgreement>(L_CREATE_COLLATERAL_AGREEMENT, this);
            return _createCollateralAgreement;
        }

        public Class getEntityClass()
        {
            return ChangeSalaryEmployeePostExtract.class;
        }

        public String getEntityName()
        {
            return "changeSalaryEmployeePostExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
