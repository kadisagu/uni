/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubController;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class Controller extends SingleEmployeeExtractPubController<CancelCombinationPostExtract, IDAO, Model>
{
}