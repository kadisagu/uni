/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfBox;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.TextAlignment;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryAdd.PgupsEcReportSummaryAdd;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.IPgupsEcReportVO;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.SummaryReportVO;
import ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport;

import java.math.BigDecimal;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 18.07.2013
 */
public class SummaryReportBuilder implements IPgupsEcReportBuilder
{
    private static enum RowType
    {
        TITLE_ROW,
        MERGED_ROW,

        BUDJET_MINISTERIAL_PLAN_ROW,
        BUDJET_ENTRANT_REQUESTS_ROW,
        BUDJET_SUMMARY_RATING_ROW,
        BUDJET_RATING_WITHOUT_TARGET_ADMISSION_ROW,
        BUDJET_TARGET_ADMISSION_PLAN_ROW,
        BUDJET_TARGET_ADMISSION_ENTRANT_REQUESTS_ROW,
        BUDJET_TARGET_ADMISSION_RATING_ROW,
        BUDJET_ORIGINALS_ROW,
        BUDJET_OUT_OF_RATING_ROW,
        BUDJET_ALL_AVERAGE_BALL_ROW,
        BUDJET_AVERAGE_BALL_ROW,
        BUDJET_TARGET_ADMISSION_AVERAGE_BALL_ROW,

        CONTRACT_PLAN_ROW,
        CONTRACT_REQUEST_COUNT_ROW,
        CONTRACT_ORIGINALS_ROW,
        CONTRACT_CONTRACT_COUNT_ROW,
        CONTRACT_PAID_CONTRACT_COUNT_ROW
    }

    private static final Map<RowType, String> ROW_NAMES;

    static
    {
        ROW_NAMES = new LinkedHashMap<>();

        ROW_NAMES.put(RowType.TITLE_ROW, "");

        ROW_NAMES.put(RowType.BUDJET_MINISTERIAL_PLAN_ROW, "План приёма г/б");
        ROW_NAMES.put(RowType.BUDJET_ENTRANT_REQUESTS_ROW, "Подано заявлений г/б");
        ROW_NAMES.put(RowType.BUDJET_SUMMARY_RATING_ROW, "Общий конкурс г/б");
        ROW_NAMES.put(RowType.BUDJET_RATING_WITHOUT_TARGET_ADMISSION_ROW, "Конкурс г/б без целевиков");
        ROW_NAMES.put(RowType.BUDJET_TARGET_ADMISSION_PLAN_ROW, "План приёма ц");
        ROW_NAMES.put(RowType.BUDJET_TARGET_ADMISSION_ENTRANT_REQUESTS_ROW, "Подано заявлений ц");
        ROW_NAMES.put(RowType.BUDJET_TARGET_ADMISSION_RATING_ROW, "Конкурс ц");
        ROW_NAMES.put(RowType.BUDJET_ORIGINALS_ROW, "Подали оригиналы докум.");
        ROW_NAMES.put(RowType.BUDJET_OUT_OF_RATING_ROW, "В категории «вне конкурса»");
        ROW_NAMES.put(RowType.BUDJET_ALL_AVERAGE_BALL_ROW, "Средний балл ЕГЭ");
        ROW_NAMES.put(RowType.BUDJET_AVERAGE_BALL_ROW, "Средний балл ЕГЭ (г/б)");
        ROW_NAMES.put(RowType.BUDJET_TARGET_ADMISSION_AVERAGE_BALL_ROW, "Средний балл ЕГЭ (ц)");

        ROW_NAMES.put(RowType.CONTRACT_PLAN_ROW, "План приёма с оплатой");
        ROW_NAMES.put(RowType.CONTRACT_REQUEST_COUNT_ROW, "Подано заявлений");
        ROW_NAMES.put(RowType.CONTRACT_ORIGINALS_ROW, "Подали оригиналы докум.");
        ROW_NAMES.put(RowType.CONTRACT_CONTRACT_COUNT_ROW, "Кол-во заключенных договоров");
        ROW_NAMES.put(RowType.CONTRACT_PAID_CONTRACT_COUNT_ROW, "Кол-во оплаченных договоров");
    }

    private static final int COLUMN_OFFSET = 2;

    List<RowObject> valuesTable;

    private PgupsSummaryEnrollmentReport report;
    private SummaryReportVO params;
    private List<Object> commonList;
    private Session session;
    private IRtfElementFactory elementFactory = RtfBean.getElementFactory();

    public SummaryReportBuilder(IPgupsEcReport report, IPgupsEcReportVO params, Session session)
    {
        this.report = (PgupsSummaryEnrollmentReport) report;
        this.params = (SummaryReportVO) params;
        this.session = session;
    }

    @Override
    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, IPgupsEcReport.SUMMARY_ENROLLMENT_REPORT_CODE);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        loadEnrollmentDirectionsAndProfiles();

        valuesTable = new ArrayList<>();
        valuesTable.add(new RowObject(RowType.TITLE_ROW, getTitleRow()));

        if (params.getCompensationType().isActive())
        {
            if (params.getCompensationType().getData().isBudget())
                fillBudjetRows();
            else
                fillContractRows();
        }
        else
        {
            addRow(RowType.MERGED_ROW, new Object[]{getBoldRtfString("БЮДЖЕТ")});
            fillBudjetRows();
            addRow(RowType.MERGED_ROW, new Object[]{getBoldRtfString("КОНТРАКТ")});
            fillContractRows();
        }

        // Сортируем строки по порядку, заданному в ROW_NAMES
        final List<RowType> nameList = new ArrayList<>(ROW_NAMES.keySet());
        Collections.sort(valuesTable, new Comparator<RowObject>()
        {
            @Override
            public int compare(RowObject o1, RowObject o2)
            {
                int i1 = nameList.indexOf(o1.type);
                int i2 = nameList.indexOf(o2.type);
                if (i1 >= 0 && i2 >= 0)
                    return Integer.compare(i1, i2);
                else
                    return 0;
            }
        });

        fillTable((RtfTable) UniRtfUtil.findElement(template.getElementList(), "T1"));
        fillLegend((RtfTable) UniRtfUtil.findElement(template.getElementList(), "legend"));

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getFormingDate()));
        modifier.put("params", getParams());
        modifier.put("direction", getDirectionType());
        modifier.modify(template);

        return RtfUtil.toByteArray(template);
    }

    private boolean isSpecialityQualification(Qualifications qualification)
    {
        return qualification.getCode().equals(QualificationsCodes.SPETSIALIST);
    }

    private boolean isDirectionsQualification(Qualifications qualification)
    {
        return qualification.getCode().equals(QualificationsCodes.MAGISTR) || qualification.getCode().equals(QualificationsCodes.BAKALAVR);
    }

    private String getDirectionType()
    {
        final String bySpeciality = "ПО СПЕЦИАЛЬНОСТЯМ";
        final String byDirections = "ПО НАПРАВЛЕНИЯМ";
        if (params.getQualification().isActive())
        {
            List<Qualifications> list = params.getQualification().getData();
            if (list.size() == 1)
            {
                if (isSpecialityQualification(list.get(0)))
                    return bySpeciality;

                if (isDirectionsQualification(list.get(0)))
                    return byDirections;
            }
            else if (list.size() == 2)
            {
                if (isDirectionsQualification(list.get(0)) && isDirectionsQualification(list.get(1)))
                    return byDirections;
            }
        }
        return "";
    }

    private void fillLegend(RtfTable table)
    {
        List<IRtfElement> list = table.getCell(0, 0).getElementList();
        for (IRtfElement element : list)
        {
            if (element instanceof RtfField)
            {
                // Удаляем метку
                list.remove(element);
                break;
            }
        }

        RtfString rtfString;
        List<RtfString> legend = new ArrayList<>(commonList.size());
        EducationOrgUnit educationOrgUnit;
        for (Object obj : commonList)
        {
            boolean isProfile = obj instanceof ProfileEducationOrgUnit;
            educationOrgUnit = (isProfile) ? ((ProfileEducationOrgUnit) obj).getEducationOrgUnit() : ((EnrollmentDirection) obj).getEducationOrgUnit();

            rtfString = new RtfString();

            if (!isProfile)
                rtfString.boldBegin();

            rtfString.append(educationOrgUnit.getShortTitle());

            if (!isProfile)
                rtfString.boldEnd();

            rtfString.append(" — " + educationOrgUnit.getEducationLevelHighSchool().getTitle());

            legend.add(rtfString);
        }

        rtfString = new RtfString().fontSize(24);
        int half = (legend.size() + 1) / 2;
        for (int i = 0; i < half; i++)
        {
            if (i > 0) rtfString.par();
            rtfString.toList().addAll(legend.get(i).toList());
        }
        table.getCell(0, 0).addElements(rtfString.toList());

        rtfString = new RtfString().fontSize(24);
        for (int i = half; i < legend.size(); i++)
        {
            if (i > half) rtfString.par();
            rtfString.toList().addAll(legend.get(i).toList());
        }
        table.getCell(0, 1).addElements(rtfString.toList());
    }

    private void fillTable(RtfTable table)
    {
        final int[] scales = new int[commonList.size() + 1];
        Arrays.fill(scales, 1);

        RtfRow row = table.getRowList().get(0);
        RtfUtil.splitRow(row, 1, null, scales);

        List<RtfCell> cells = row.getCellList();
        cells.get(0).getElementList().clear();
        RtfRow rowTemplate = row.getClone();

        RtfString rtfString;
        Object obj;
        RtfCell cell;
        String content;
        IRtfControl qcElement = elementFactory.createRtfControl(IRtfData.QC);
        boolean bold, isTitle, isMerged;
        RtfBox cellMargin = RtfBox.getInstance(60, 60, 80, 80);

        for (RowObject rowObject : valuesTable)
        {
            isTitle = rowObject.type == RowType.TITLE_ROW;
            isMerged = rowObject.type == RowType.MERGED_ROW;

            // Название строки
            obj = ROW_NAMES.get(rowObject.type);
            if (obj != null && !((String) obj).isEmpty())
                rowObject.values[0] = obj;

            if (!isTitle)
            {
                // Добавляем новую строку
                row = rowTemplate.getClone();
                cells = row.getCellList();
                table.addRow(row);
            }

            if (isMerged)
            {
                cells.get(0).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
                for (int i = 1; i < cells.size(); i++)
                    cells.get(i).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
            }

            if (rowObject.values.length == 1)
            {
                cells.get(0).addElement(qcElement);
            }

            for (int i = 0; i < rowObject.values.length; i++)
            {
                cell = cells.get(i);
                bold = (i == 0) || (i >= COLUMN_OFFSET && commonList.get(i - COLUMN_OFFSET) instanceof EnrollmentDirection);

                obj = rowObject.values[i];
                if (!(obj instanceof RtfString))
                {
                    if (obj != null)
                    {
                        if (!(obj instanceof Double))
                            content = obj.toString();
                        else
                            content = String.format(i >= COLUMN_OFFSET ? "%.2f" : "%.3f", (Double) obj);
                    }
                    else
                        content = "";

                    if (content.isEmpty() && !isTitle && !isMerged)
                        content = "−";

                    rtfString = new RtfString();
                    if (bold)
                        rtfString.boldBegin();

                    rtfString.append(content);

                    if (bold)
                        rtfString.boldEnd();
                }
                else
                    rtfString = (RtfString) obj;

                // Отступы
                cell.setCellMargin(cellMargin);

                // Выравнивание по вертикали по центру
                cell.setTextAlignment(TextAlignment.MIDDLE);

                // Горизонтальное выравнивание по центру
                if ((i > 0 && !isTitle) || (isTitle && i != 1))
                    cell.addElement(qcElement);

                cell.addElements(rtfString.toList());
            }
        }
    }

    private RtfString getParams()
    {
        RtfString rtfString = new RtfString();

        rtfString.append("Приёмная кампания: ").append(report.getEnrollmentCampaign().getTitle()).append(", ").append(report.getEnrollmentCampaignStepStr());
        rtfString.par().append("Заявления:")
                .append(" с ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getDateFrom()))
                .append(" по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getDateTo()));

        if (params.getStudentCategory().isActive())
            rtfString.par().append("Категория поступающего: ").append(report.getStudentCategoryStr());

        if (params.getQualification().isActive())
            rtfString.par().append("Квалификация: ").append(report.getQualificationStr());

        if (params.getCompensationType().isActive())
            rtfString.par().append("Вид возмещения затрат: ").append(report.getCompensationTypeStr());

        if (params.getDevelopForm().isActive())
            rtfString.par().append("Форма освоения: ").append(report.getDevelopFormStr());

        if (params.getDevelopCondition().isActive())
            rtfString.par().append("Условие освоения: ").append(report.getDevelopConditionStr());

        if (params.getDevelopTech().isActive())
            rtfString.par().append("Технология освоения: ").append(report.getDevelopTechStr());

        if (params.getDevelopPeriod().isActive())
            rtfString.par().append("Срок освоения: ").append(report.getDevelopPeriodStr());

        return rtfString;
    }

    private void loadEnrollmentDirectionsAndProfiles()
    {
        Boolean isBudjet = params.getCompensationType().isActive() ? params.getCompensationType().getData().isBudget() : null;
        // Получаем направления подготовки для приема
        List<EnrollmentDirection> directionList = getRequestedEnrollmentDirectionBuilder("e", "d", isBudjet, false, false, false)
                .predicate(DQLPredicateType.distinct)
                .column(property(RequestedEnrollmentDirection.enrollmentDirection().fromAlias("e")))
                .createStatement(session).list();

        // Сортируем по ОКСО
        Collections.sort(directionList, new Comparator<EnrollmentDirection>()
        {
            @Override
            public int compare(EnrollmentDirection o1, EnrollmentDirection o2)
            {
                String code1 = o1.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix();
                String code2 = o2.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix();
                return code1.compareTo(code2);
            }
        });

        // Получаем профили для этих направлений
        List<ProfileEducationOrgUnit> profileList = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "p").column("p")
                .where(in(
                        property(ProfileEducationOrgUnit.enrollmentDirection().id().fromAlias("p")),
                        getRequestedEnrollmentDirectionBuilder("e", "d", isBudjet, false, false, false)
                                .column(property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias("e")))
                                .buildQuery()
                )).createStatement(session).list();

        // Сортируем по сокр. названию НПП
        Collections.sort(profileList, new Comparator<ProfileEducationOrgUnit>()
        {
            @Override
            public int compare(ProfileEducationOrgUnit o1, ProfileEducationOrgUnit o2)
            {
                return o1.getEducationOrgUnit().getShortTitle().compareTo(o2.getEducationOrgUnit().getShortTitle());
            }
        });

        // Группируем профили по направлениям
        Map<EnrollmentDirection, List<ProfileEducationOrgUnit>> enrDirections = new LinkedHashMap<>(directionList.size());
        for (EnrollmentDirection dir : directionList)
            enrDirections.put(dir, new ArrayList<ProfileEducationOrgUnit>());
        for (ProfileEducationOrgUnit prof : profileList)
            enrDirections.get(prof.getEnrollmentDirection()).add(prof);

        // Заполняем общий список колонок - сначала список профилей, потом направление, к которому они относятся
        commonList = new ArrayList<>();
        for (EnrollmentDirection dir : enrDirections.keySet())
        {
            for (ProfileEducationOrgUnit prof : enrDirections.get(dir))
                commonList.add(prof);
            commonList.add(dir);
        }
    }

    private void addRow(RowType type, Object[] values)
    {
        valuesTable.add(new RowObject(type, values));
    }

    private void fillBudjetRows()
    {
        addRow(RowType.BUDJET_MINISTERIAL_PLAN_ROW, getPlanRow(false, true));
        addRow(RowType.BUDJET_ENTRANT_REQUESTS_ROW, getEntrantRequestCountRow(true, false, false, false));
        addRow(RowType.BUDJET_SUMMARY_RATING_ROW, getRatingRow(RowType.BUDJET_MINISTERIAL_PLAN_ROW, RowType.BUDJET_ENTRANT_REQUESTS_ROW));
        addRow(RowType.BUDJET_TARGET_ADMISSION_PLAN_ROW, getPlanRow(true, true));
        addRow(RowType.BUDJET_TARGET_ADMISSION_ENTRANT_REQUESTS_ROW, getEntrantRequestCountRow(true, true, false, false));
        addRow(RowType.BUDJET_TARGET_ADMISSION_RATING_ROW, getRatingRow(RowType.BUDJET_TARGET_ADMISSION_PLAN_ROW, RowType.BUDJET_TARGET_ADMISSION_ENTRANT_REQUESTS_ROW));
        addRow(RowType.BUDJET_RATING_WITHOUT_TARGET_ADMISSION_ROW, getBudgetRatingWOTargetAdmissionRow());
        //addRow(RowType.MERGED_ROW, new Object[]{""});
        addRow(RowType.BUDJET_ORIGINALS_ROW, getEntrantRequestCountRow(true, false, true, false));
        addRow(RowType.BUDJET_OUT_OF_RATING_ROW, getEntrantRequestCountRow(true, false, false, true));

        addRow(RowType.BUDJET_ALL_AVERAGE_BALL_ROW, getEgeAverageBalls(null));
        addRow(RowType.BUDJET_AVERAGE_BALL_ROW, getEgeAverageBalls(false));
        addRow(RowType.BUDJET_TARGET_ADMISSION_AVERAGE_BALL_ROW, getEgeAverageBalls(true));
    }

    private void fillContractRows()
    {
        addRow(RowType.CONTRACT_PLAN_ROW, getPlanRow(false, false));
        addRow(RowType.CONTRACT_REQUEST_COUNT_ROW, getEntrantRequestCountRow(false, false, false, false));
        //addRow(RowType.MERGED_ROW, new Object[]{""});
        addRow(RowType.CONTRACT_ORIGINALS_ROW, getEntrantRequestCountRow(false, false, true, false));
        addRow(RowType.CONTRACT_CONTRACT_COUNT_ROW, getContractsCountRow(false));
        addRow(RowType.CONTRACT_PAID_CONTRACT_COUNT_ROW, getContractsCountRow(true));
    }

    private Object[] getTitleRow()
    {
        Object[] row = new Object[commonList.size() + COLUMN_OFFSET];
        if (params.getCompensationType().isActive())
        {
            row[0] = new RtfString().fontSize(40).boldBegin()
                    .append(params.getCompensationType().getData().isBudget() ? "Г/Б" : "К")
                    .boldEnd();
        }
        row[1] = new RtfString().boldBegin().append("Всего по университету").boldEnd();
        for (int i = COLUMN_OFFSET; i < row.length; i++)
        {
            row[i] = getColumnTitle(commonList.get(i - COLUMN_OFFSET));
        }
        return row;
    }

    private Set<RequestedEnrollmentDirection> getREDForProfile(ProfileEducationOrgUnit profile)
    {
        List<RequestedEnrollmentDirection> list = new DQLSelectBuilder()
                .column("red")
                .fromEntity(RequestedEnrollmentDirection.class, "red")
                .joinEntity("red", DQLJoinType.inner, PriorityProfileEduOu.class, "prof",
                            eq(
                                    property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias("prof")),
                                    property(RequestedEnrollmentDirection.id().fromAlias("red"))
                            )
                )
                .where(eq(
                        property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias("prof")),
                        value(profile.getId())
                ))
                .where(eq(
                        property(PriorityProfileEduOu.priority().fromAlias("prof")),
                        value(1)
                )).createStatement(session).list();
        return new HashSet<>(list);
    }

    private Double calcAvg(EntrantDataUtil dataUtil, Set<RequestedEnrollmentDirection> dirSet)
    {
        Double result = 0.0d;
        int count = 0;
        String code;
        for (RequestedEnrollmentDirection direction : dirSet)
        {
            for (Map.Entry<Discipline2RealizationWayRelation, Double> entry : dataUtil.getMarkMap(direction).entrySet())
            {
                code = entry.getKey().getSubjectPassWay().getCode();
                if (UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL.equals(code) || UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM.equals(code))
                {
                    result += entry.getValue();
                    count++;
                }
            }
        }

        if (count == 0)
            return null;

        result = result / (double) count;

        BigDecimal x = new java.math.BigDecimal(result);
        x = x.setScale(2, BigDecimal.ROUND_HALF_UP);
        result = x.doubleValue();

        return result;
    }

    private Object[] getEgeAverageBalls(Boolean targetAdmission)
    {
        Object[] row = new Object[commonList.size() + COLUMN_OFFSET];

        List<Long> ids = getRequestedEnrollmentDirectionBuilder("red", "dir", true, false, false, false)
                .column("red.id")
                .createStatement(session).list();

        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "rd");
        directionBuilder.add(MQExpression.in("rd", RequestedEnrollmentDirection.id().s(), ids));
        if (targetAdmission != null)
            directionBuilder.add(MQExpression.eq("rd", RequestedEnrollmentDirection.targetAdmission().s(), targetAdmission));

        EntrantDataUtil dataUtil = new EntrantDataUtil(session, report.getEnrollmentCampaign(), directionBuilder);

        Double sum = 0.0d, value;
        int count = 0;
        for (int i = 0; i < commonList.size(); i++)
        {
            Object obj = commonList.get(i);
            if (obj instanceof EnrollmentDirection)
                value = calcAvg(dataUtil, dataUtil.getDirectionSet((EnrollmentDirection) obj));
            else
                value = calcAvg(dataUtil, getREDForProfile((ProfileEducationOrgUnit) obj));

            if (value != null)
            {
                row[i + COLUMN_OFFSET] = value;
                sum += value;
                count++;
            }
        }
        if (count > 0)
            row[1] = String.format("%.2f", sum / (double) count);

        return row;
    }

    private Object[] getContractsCountRow(boolean onlyPaid)
    {
        Object[] row = new Object[commonList.size() + COLUMN_OFFSET];
        Long sum = 0L;

        String alias = "red";
        // Считаем по направлениям приема
        DQLSelectBuilder builder = getEnrDirCountBuilder(alias, "r", false, false, false, false)
                .joinEntity(alias, DQLJoinType.inner, UniscEduAgreement2Entrant.class, "con",
                eq(
                        property(EntrantRequest.entrant().id().fromAlias("r")),
                        property(UniscEduAgreement2Entrant.entrant().id().fromAlias("con"))
                ))
                .where(eq(
                        property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().id().fromAlias(alias)),
                        property(UniscEduAgreement2Entrant.agreement().config().educationOrgUnit().id().fromAlias("con"))
                ));

        if (onlyPaid)
            builder.where(eq(
                    property(UniscEduAgreement2Entrant.paymentExists().fromAlias("con")),
                    value(Boolean.TRUE)
            ));

        List<Object[]> result = builder.createStatement(session).list();

        Map<Long, Long> dirMap = new HashMap<>(result.size());
        for (Object[] item : result)
            dirMap.put((Long) item[0], (Long) item[1]);


        // Считаем по профилям
        builder = getProfCountBuilder(alias, "prior", "r", false, false, false, false)
                .joinEntity(alias, DQLJoinType.inner, UniscEduAgreement2Entrant.class, "con",
                eq(
                        property(EntrantRequest.entrant().id().fromAlias("r")),
                        property(UniscEduAgreement2Entrant.entrant().id().fromAlias("con"))
                ))
                .where(eq(
                        property(PriorityProfileEduOu.profileEducationOrgUnit().educationOrgUnit().id().fromAlias("prior")),
                        property(UniscEduAgreement2Entrant.agreement().config().educationOrgUnit().id().fromAlias("con"))
                ));

        if (onlyPaid)
            builder.where(eq(
                    property(UniscEduAgreement2Entrant.paymentExists().fromAlias("con")),
                    value(Boolean.TRUE)
            ));

        result = builder.createStatement(session).list();

        Map<Long, Long> profMap = new HashMap<>(result.size());
        for (Object[] item : result)
            profMap.put((Long) item[0], (Long) item[1]);

        Long count;
        for (int i = 0; i < commonList.size(); i++)
        {
            Object obj = commonList.get(i);
            if (obj instanceof EnrollmentDirection)
            {
                count = dirMap.get(((EnrollmentDirection) obj).getId());
                if (count != null)
                {
                    row[i + COLUMN_OFFSET] = count;
                    sum += count;
                }
            }
            else
            {
                row[i + COLUMN_OFFSET] = profMap.get(((ProfileEducationOrgUnit) obj).getId());
            }
        }
        row[1] = sum;

        return row;
    }

    private Object[] getRowValues(RowType type)
    {
        for (RowObject rowObject : valuesTable)
        {
            if (type == rowObject.type)
                return rowObject.values;
        }
        return null;
    }

    private Object[] getRatingRow(RowType planRowType, RowType countRowType)
    {
        Object[] planRow = getRowValues(planRowType);
        Object[] countRow = getRowValues(countRowType);
        Object[] row = new Object[planRow.length];
        for (int i = 1; i < planRow.length; i++)
        {
            if (planRow[i] != null && countRow[i] != null)
            {
                if (!planRow[i].equals(0L))
                    row[i] = ((Long) countRow[i]).doubleValue() / ((Long) planRow[i]).doubleValue();
                else
                    row[i] = "NaN";
            }
        }
        return row;
    }

    private Object[] getBudgetRatingWOTargetAdmissionRow()
    {
        Object[] allPlanRow = getRowValues(RowType.BUDJET_MINISTERIAL_PLAN_ROW);
        Object[] allCountRow = getRowValues(RowType.BUDJET_ENTRANT_REQUESTS_ROW);
        Object[] taPlanRow = getRowValues(RowType.BUDJET_TARGET_ADMISSION_PLAN_ROW);
        Object[] taCountRow = getRowValues(RowType.BUDJET_TARGET_ADMISSION_ENTRANT_REQUESTS_ROW);
        Object[] row = new Object[allCountRow.length];
        Long count, plan;
        for (int i = 1; i < allCountRow.length; i++)
        {
            count = (Long) allCountRow[i];
            plan = (Long) allPlanRow[i];
            if (count != null && plan != null)
            {
                count -= taCountRow[i] == null ? 0L : (Long) taCountRow[i];
                plan -= taPlanRow[i] == null ? 0L : (Long) taPlanRow[i];
                if (!plan.equals(0L))
                    row[i] = count.doubleValue() / plan.doubleValue();
                else
                    row[i] = "NaN";
            }
        }
        return row;
    }

    private Object[] getPlanRow(boolean isTargetAdmission, boolean isBudjet)
    {
        Object[] row = new Object[commonList.size() + COLUMN_OFFSET];
        Long sum = null;
        Integer plan;
        for (int i = 0; i < commonList.size(); i++)
        {
            Object obj = commonList.get(i);
            if (obj instanceof ProfileEducationOrgUnit)
            {
                if (!isTargetAdmission)
                {
                    ProfileEducationOrgUnit peou = (ProfileEducationOrgUnit) obj;
                    plan = isBudjet ? peou.getBudgetPlan() : peou.getContractPlan();
                    row[i + COLUMN_OFFSET] = plan != null ? plan.longValue() : null;
                }
            }
            else
            {
                EnrollmentDirection dir = (EnrollmentDirection) obj;
                plan = isBudjet
                        ? (isTargetAdmission ? dir.getTargetAdmissionPlanBudget() : dir.getMinisterialPlan())
                        : (isTargetAdmission ? dir.getTargetAdmissionPlanContract() : dir.getContractPlan());
                if (plan != null)
                {
                    row[i + COLUMN_OFFSET] = plan.longValue();
                    if (sum == null)
                        sum = plan.longValue();
                    else
                        sum += plan.longValue();
                }
            }
        }
        row[1] = sum;
        return row;
    }

    private Object[] getEntrantRequestCountRow(boolean budjet, boolean targetAdmission, boolean onlyOriginals, boolean beyondCompetition)
    {
        Object[] row = new Object[commonList.size() + COLUMN_OFFSET];
        Long sum = 0L;

        // Считаем по направлениям приема
        List<Object[]> result = getEnrDirCountBuilder("red", "r", budjet, targetAdmission, onlyOriginals, beyondCompetition).createStatement(session).list();

        Map<Long, Long> dirMap = new HashMap<>(result.size());
        for (Object[] item : result)
            dirMap.put((Long) item[0], (Long) item[1]);

        // Считаем по профилям
        result = getProfCountBuilder("red", "prior", "r", budjet, targetAdmission, onlyOriginals, beyondCompetition).createStatement(session).list();

        Map<Long, Long> profMap = new HashMap<>(result.size());
        for (Object[] item : result)
            profMap.put((Long) item[0], (Long) item[1]);

        Long count;
        for (int i = 0; i < commonList.size(); i++)
        {
            Object obj = commonList.get(i);
            if (obj instanceof EnrollmentDirection)
            {
                count = dirMap.get(((EnrollmentDirection) obj).getId());
                if (count != null)
                {
                    row[i + COLUMN_OFFSET] = count;
                    sum += count;
                }
            }
            else
            {
                row[i + COLUMN_OFFSET] = profMap.get(((ProfileEducationOrgUnit) obj).getId());
            }
        }
        row[1] = sum;

        return row;
    }

    private DQLSelectBuilder getEnrDirCountBuilder(String alias, String requestAlias, boolean budjet, boolean targetAdmission, boolean onlyOriginals, boolean beyondCompetition)
    {
        IDQLExpression enrDirProp = property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias(alias));
        return getRequestedEnrollmentDirectionBuilder(alias, alias + "dir", budjet, targetAdmission, onlyOriginals, beyondCompetition)
                .column(enrDirProp)
                .column(DQLFunctions.count(property(EntrantRequest.id().fromAlias(requestAlias))))
                .joinPath(DQLJoinType.inner, RequestedEnrollmentDirection.entrantRequest().fromAlias(alias), requestAlias)
                .group(enrDirProp);
    }

    private DQLSelectBuilder getProfCountBuilder(String alias, String priorAlias, String requestAlias, boolean budjet, boolean targetAdmission, boolean onlyOriginals, boolean beyondCompetition)
    {
        return getRequestedEnrollmentDirectionBuilder(alias, alias + "dir", budjet, targetAdmission, onlyOriginals, beyondCompetition)
                .column(property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias(priorAlias)))
                .column(DQLFunctions.count(property(EntrantRequest.id().fromAlias(requestAlias))))
                .joinEntity(alias, DQLJoinType.inner, PriorityProfileEduOu.class, priorAlias,
                            and(
                                    eq(
                                            property(PriorityProfileEduOu.requestedEnrollmentDirection().id().fromAlias(priorAlias)),
                                            property(RequestedEnrollmentDirection.id().fromAlias(alias))
                                    ),
                                    eq(
                                            property(PriorityProfileEduOu.priority().fromAlias(priorAlias)),
                                            value(1)
                                    )
                            ))
                .joinEntity(alias, DQLJoinType.inner, EntrantRequest.class, requestAlias,
                            eq(
                                    property(RequestedEnrollmentDirection.entrantRequest().id().fromAlias(alias)),
                                    property(EntrantRequest.id().fromAlias(requestAlias))
                            ))
                .group(property(PriorityProfileEduOu.profileEducationOrgUnit().id().fromAlias(priorAlias)));
    }

    /**
     * Выборка для выбранных направлений подготовки по параметрам отчета
     */
    private DQLSelectBuilder getRequestedEnrollmentDirectionBuilder(
            String redAlias, String dirAlias, Boolean isBudjet, boolean targetAdmission, boolean onlyOriginals, boolean beyondCompetition)
    {
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, redAlias);

        String compensationTypeCode;
        if (isBudjet != null)
            compensationTypeCode = isBudjet ? UniDefines.COMPENSATION_TYPE_BUDGET : UniDefines.COMPENSATION_TYPE_CONTRACT;
        else
            compensationTypeCode = null;

        // не архивные абитуриенты
        builder.where(ne(
                property(RequestedEnrollmentDirection.entrantRequest().entrant().archival().fromAlias(redAlias)),
                value(Boolean.TRUE)
        ));

        // Заявления с
        builder.where(ge(
                property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias(redAlias)),
                valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(report.getDateFrom()))
        ));

        // Заявления по
        builder.where(lt(
                property(RequestedEnrollmentDirection.entrantRequest().regDate().fromAlias(redAlias)),
                valueTimestamp(CoreDateUtils.getDayFirstTimeMoment(CoreDateUtils.add(report.getDateTo(), Calendar.DAY_OF_YEAR, 1)))
        ));

        if (targetAdmission)
        {
            // нужны только целевики
            builder.where(eq(
                    property(RequestedEnrollmentDirection.targetAdmission().fromAlias(redAlias)),
                    value(Boolean.TRUE)
            ));
        }

        if (onlyOriginals)
        {
            builder.where(eq(
                    property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias(redAlias)),
                    value(Boolean.TRUE)
            ));
        }

        if (beyondCompetition)
        {
            builder.where(eq(
                    property(RequestedEnrollmentDirection.competitionKind().code().fromAlias(redAlias)),
                    value(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)
            ));
        }

        // Стадия приемной кампании
        Long stepId = params.getEnrollmentCampaignStep().getId();
        if (PgupsEcReportSummaryAdd.ON_PROCESS_RECEPTION.equals(stepId))
        {
            // по ходу приема документов
            builder.where(ne(
                    property(RequestedEnrollmentDirection.state().code().fromAlias(redAlias)),
                    value(EntrantStateCodes.TAKE_DOCUMENTS_AWAY)
            ));

            if (compensationTypeCode != null)
                builder.where(eq(
                        property(RequestedEnrollmentDirection.compensationType().code().fromAlias(redAlias)),
                        value(compensationTypeCode)
                ));
        }
        else if (PgupsEcReportSummaryAdd.ON_RESULT_TEST.equals(stepId))
        {
            // по результатам сдачи вступительных испытаний
            builder.where(notIn(
                    property(RequestedEnrollmentDirection.state().code().fromAlias(redAlias)),
                    Arrays.asList(EntrantStateCodes.TAKE_DOCUMENTS_AWAY, EntrantStateCodes.OUT_OF_COMPETITION, EntrantStateCodes.ACTIVE)
            ));

            if (compensationTypeCode != null)
                builder.where(eq(
                        property(RequestedEnrollmentDirection.compensationType().code().fromAlias(redAlias)),
                        value(compensationTypeCode)
                ));
        }
        else if (PgupsEcReportSummaryAdd.ON_RESULT_ENROLLMENT.equals(stepId))
        {
            // по результатам зачисления

            String subAlias = redAlias + "001";
            DQLSelectBuilder sub = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, subAlias)
                    .column(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().fromAlias(subAlias)));

            if (compensationTypeCode != null)
                sub.where(eq(
                        // по указанному возмещению затрат (!из студента предзачисления!)
                        property(PreliminaryEnrollmentStudent.compensationType().code().fromAlias(subAlias)),
                        value(compensationTypeCode)
                ));

            builder.where(in(
                    property(RequestedEnrollmentDirection.state().code().fromAlias(redAlias)),
                    Arrays.asList(EntrantStateCodes.PRELIMENARY_ENROLLED, EntrantStateCodes.IN_ORDER, EntrantStateCodes.ENROLED)
            ));

            // должны быть подходящие студенты предзачисления
            builder.where(in(
                    property(RequestedEnrollmentDirection.id().fromAlias(redAlias)),
                    sub.buildQuery()
            ));
        }
        else
        {
            throw new IllegalStateException();
        }

        // Категория поступающего
        if (params.getStudentCategory().isActive())
        {
            builder.where(in(
                    property(RequestedEnrollmentDirection.studentCategory().fromAlias(redAlias)),
                    params.getStudentCategory().getData()
            ));
        }


        builder.where(in(
                property(RequestedEnrollmentDirection.enrollmentDirection().id().fromAlias(redAlias)),
                getEnrollmentDirectionBuilder(dirAlias + "x").buildQuery()
        ));

        return builder;
    }

    private DQLSelectBuilder getEnrollmentDirectionBuilder(String alias)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, alias)
                .predicate(DQLPredicateType.distinct)
                .column(property(EnrollmentDirection.id().fromAlias(alias)));

        // приемная кампания
        builder.where(eq(
                property(EnrollmentDirection.enrollmentCampaign().fromAlias(alias)),
                value(report.getEnrollmentCampaign())
        ));

        // ФУТС и квалификация
        if (params.getQualification().isActive() || params.getDevelopForm().isActive() || params.getDevelopCondition().isActive() || params.getDevelopTech().isActive() || params.getDevelopPeriod().isActive())
        {
            builder.joinEntity(alias, DQLJoinType.inner, EducationOrgUnit.class, "edu", eq(
                    property(EnrollmentDirection.educationOrgUnit().id().fromAlias(alias)),
                    property(EducationOrgUnit.id().fromAlias("edu"))
            ));

            // Квалификация
            if (params.getQualification().isActive())
            {
                builder.where(in(
                        property(EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().fromAlias("edu")),
                        params.getQualification().getData()
                ));
            }

            // Форма освоения
            if (params.getDevelopForm().isActive())
            {
                builder.where(in(
                        property(EducationOrgUnit.developForm().fromAlias("edu")),
                        params.getDevelopForm().getData()
                ));
            }

            // Условие освоения
            if (params.getDevelopCondition().isActive())
            {
                builder.where(in(
                        property(EducationOrgUnit.developCondition().fromAlias("edu")),
                        params.getDevelopCondition().getData()
                ));
            }

            // Технология освоения
            if (params.getDevelopTech().isActive())
            {
                builder.where(in(
                        property(EducationOrgUnit.developTech().fromAlias("edu")),
                        params.getDevelopTech().getData()
                ));
            }

            // Срок освоения
            if (params.getDevelopPeriod().isActive())
            {
                builder.where(in(
                        property(EducationOrgUnit.developPeriod().fromAlias("edu")),
                        params.getDevelopPeriod().getData()
                ));
            }
        }

        return builder;
    }

    private RtfString getBoldRtfString(String value)
    {
        return new RtfString().boldBegin().append(value).boldEnd();
    }

    private Object getColumnTitle(Object obj)
    {
        if (obj instanceof ProfileEducationOrgUnit)
            return ((ProfileEducationOrgUnit) obj).getEducationOrgUnit().getShortTitle();
        else
        {
            RtfString rtfString = new RtfString();
            EnrollmentDirection dir = (EnrollmentDirection) obj;
            rtfString//.append(IRtfData.UL)
                    .boldBegin()
                    .append(dir.getEducationOrgUnit().getShortTitle())
                    .boldEnd();
            //.append(IRtfData.ULNONE);
            //List<ProfileEducationOrgUnit> profList = enrDirections.get(dir);
            //if (profList.size() == 1)
            //    rtfString.par().append("(" + profList.get(0).getEducationOrgUnit().getShortTitle() + ")");
            return rtfString;
        }
    }

    private class RowObject
    {
        private RowType type;
        private Object[] values;

        public RowObject(RowType type, Object[] values)
        {
            this.type = type;
            this.values = values;
        }
    }
}