/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMission;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;
import ru.tandemservice.unipgups.entity.PgupsMissionToExtractRelation;
import ru.tandemservice.unipgups.entity.PgupsServiceTask;

import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class PgupsEmployeeMissionExtractDAO extends UniBaseDao implements IExtractComponentDao<PgupsEmployeeMissionExtract>
{

    @Override
    public void doCommit(PgupsEmployeeMissionExtract extract, Map parameters)
    {
        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);
        PgupsEmployeeMission mission = new PgupsEmployeeMission();
        mission.setEmployeePost(extract.getEntity());
        mission.setBeginDate(extract.getBeginDate());
        mission.setEndDate(extract.getEndDate());
        mission.setExternalOrgUnit(extract.getExternalOrgUnit());
        mission.setFinancingSourceOU(extract.getFinancingSourceOrgUnit());
        mission.setInternalFinancingSource(extract.isInternalFinancingSource());
        mission.setWarrantDate(extract.getWarrantDate());
        mission.setWarrantNumber(extract.getWarrantNumber());

        PgupsServiceTask task = new PgupsServiceTask();
                task.setNumber(extract.getTaskNumber());
        task.setTaskDate(extract.getTaskDate());
        task.setTarget(extract.getPurposeText());
        mission.setPgupsServiceTask(task);

        save(task);
        save(mission);

        PgupsMissionToExtractRelation relation = new PgupsMissionToExtractRelation();
        relation.setExtract(extract);
        relation.setMission(mission);
        save(relation);

    }


    @Override
    public void doRollback(PgupsEmployeeMissionExtract extract, Map parameters)
    {

        update(extract);

        PgupsEmployeeMission mission = new DQLSelectBuilder().fromEntity(PgupsMissionToExtractRelation.class, "rel")
                .column(property("rel", PgupsMissionToExtractRelation.mission()))
                .where(eq(property("rel", PgupsMissionToExtractRelation.extract()), value(extract))).createStatement(getSession()).uniqueResult();
        if (mission != null)
            delete(mission);

    }
}