/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.EntrantRequestTab;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 26.07.2013
 */
public class EntrantRequestPrint implements IPrintFormCreator<Long>
{
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        RtfDocument document = new RtfReader().read(template);
        EntrantRequest entrantRequest = UniDaoFacade.getCoreDao().get(EntrantRequest.class, entrantRequestId);

        Person person = entrantRequest.getEntrant().getPerson();
        IdentityCard identityCard = person.getIdentityCard();
        boolean male = identityCard.getSex().isMale();
        PersonEduInstitution lastEduInstitution = person.getPersonEduInstitution();

        List<RequestedEnrollmentDirection> directions =  UniDaoFacade.getCoreDao().getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY);
        RequestedEnrollmentDirection direction = directions.get(0);
        Qualifications qualification = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();

        String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), GrammaCase.GENITIVE, true);
        String firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), GrammaCase.GENITIVE, true);
        String middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), GrammaCase.GENITIVE, true);

        RtfInjectModifier modifier = new RtfInjectModifier();

        modifier.put("lastName", lastName);
        modifier.put("firstName", firstName);
        modifier.put("middleName", middleName);

        modifier.put("docIssuanceDate", null == lastEduInstitution ? "" : null == lastEduInstitution.getIssuanceDate() ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(lastEduInstitution.getIssuanceDate()));
        modifier.put("docIssuancePlace", null == lastEduInstitution ? "" : null == lastEduInstitution.getEduInstitution() ? "" : lastEduInstitution.getEduInstitution().getTitle() + " " + lastEduInstitution.getEduInstitution().getAddress().getSettlement().getTitleWithType());
        modifier.put("docType", null == lastEduInstitution ? "" : lastEduInstitution.getDocumentType().getTitle());
        modifier.put("docSeria", null == lastEduInstitution ? "" : lastEduInstitution.getSeria());
        modifier.put("docNumber", null == lastEduInstitution ? "" : lastEduInstitution.getNumber());

        modifier.put("takenback", male ? "забравшим" : "забравшей");
        modifier.put("qualification", null == qualification ? "" : QualificationsCodes.BAKALAVR.equals(qualification.getCode()) || QualificationsCodes.MAGISTR.equals(qualification.getCode()) ? "направление" : "специальность");
        modifier.put("qualification_D", null == qualification ? "" : QualificationsCodes.BAKALAVR.equals(qualification.getCode()) || QualificationsCodes.MAGISTR.equals(qualification.getCode()) ? "с направления" : "со специальности");

        modifier.put("eduLevel", direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getProgramSubjectTitleWithCode());
        modifier.put("requestNumber", entrantRequest.getStringNumber());
        modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));

        modifier.modify(document);

        return document;
    }
}

