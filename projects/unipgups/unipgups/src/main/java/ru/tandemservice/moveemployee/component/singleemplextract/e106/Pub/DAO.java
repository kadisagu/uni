/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;

/**
 * @author Ekaterina Zvereva
 * @since 07.09.2015
 */
public class DAO extends SingleEmployeeExtractPubDAO<PgupsTransferToDiffStaffRatesExtract, Model> implements IDAO
{
}