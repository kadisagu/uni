/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106.AddEdit;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.moveemployee.component.commons.CommonEmployeeExtractUtil;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditController;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;
import ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation;

import java.util.*;

/**
 * @author Ekaterina Zvereva
 * @since 07.09.2015
 */
public class Controller  extends CommonSingleEmployeeExtractAddEditController<PgupsTransferToDiffStaffRatesExtract, IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        Model model = getModel(component);
        prepareEmployeeStaffRateDataSource(component);

        if (model.isAddForm() && model.getStaffRateItemList().isEmpty())
            getDao().prepareStaffRateList(model);

        prepareStaffRateDataSource(component);
        fillStaffRateValueMaps(model);
    }

    public void onClickFreelance(IBusinessComponent component)
    {
        clearActiveStaffList(component);
    }

    @Override
    public void onChangeBasics(IBusinessComponent component)
    {
        Model model = getModel(component);

        Date date = model.getExtract().getContractDate();
        String number = model.getExtract().getContractNumber();
        Date agreementDate = model.getExtract().getContractAddAgreementDate();
        String agreementNumber = model.getExtract().getContractAddAgreementNumber();

        CommonEmployeeExtractUtil.fillExtractBasics(model.getSelectedBasicList(), model.getCurrentBasicMap(), date, number, agreementDate, agreementNumber);
    }


    private void prepareEmployeeStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeStaffRateDataSource() != null)
            return;


        DynamicListDataSource<EmployeePostStaffRateItem> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareEmployeeStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setEmployeeStaffRateDataSource(dataSource);
    }


    @SuppressWarnings("unchecked")
    public void onChangeStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        Set<Map.Entry> list = ((BlockColumn) model.getStaffRateDataSource().getColumn(0)).getValueMap().entrySet();
        Double staffRateSumm = 0d;
        for (Map.Entry item : list)
        {
            Object value = item.getValue();
            if(value != null && value instanceof Number)
                staffRateSumm += ((Number)value).doubleValue();
        }

        PostBoundedWithQGandQL postBoundedWithQGandQL = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

        if (model.getExtract().getSalaryRaisingCoefficient() != null)
            model.getExtract().setSalary(model.getExtract().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (postBoundedWithQGandQL.getSalary() != null)
            model.getExtract().setSalary(postBoundedWithQGandQL.getSalary() * staffRateSumm);
    }

    public void onChangeFinansingSource(IBusinessComponent component)
    {
        Model model = getModel(component);

        StaffRateToPgupsTransfDiffStaffRateExtractRelation wrapper = model.getStaffRateDataSource().getCurrentEntity();
       ((BlockColumn) model.getStaffRateDataSource().getColumn(3))
               .getValueMap().put(wrapper.getId(), model.getEmptyStaffRateAllocItemMap().get(new CoreCollectionUtils.Pair<>(wrapper.getFinancingSource(), wrapper.getFinancingSourceItem())));
    }



    public void onChangeRaisingCoefficient(IBusinessComponent component)
    {
        Model model = getModel(component);

        Double staffRateSumm = 0d;

        PostBoundedWithQGandQL postBoundedWithQGandQL = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

        if (model.getExtract().getSalaryRaisingCoefficient() != null)
            model.getExtract().setSalary(model.getExtract().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (postBoundedWithQGandQL.getSalary() != null)
            model.getExtract().setSalary(postBoundedWithQGandQL.getSalary() * staffRateSumm);
    }

    public void clearActiveStaffList(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.isThereAnyActiveStaffList())
            model.setThereAnyActiveStaffList(false);
    }

    public void prepareStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (!model.isNeedUpdateDataSource() && model.getStaffRateDataSource() != null)
            return;

        DynamicListDataSource<StaffRateToPgupsTransfDiffStaffRateExtractRelation> dataSource = new DynamicListDataSource<>(component, context -> {
            getDao().prepareStaffRateDataSource(model);
        }, 5);

        dataSource.addColumn(new BlockColumn("staffRate", "Ставка"));
        dataSource.addColumn(new BlockColumn("financingSource", "Источник финансирования"));
        dataSource.addColumn(new BlockColumn("financingSourceItem", "Источник финансирования (детально)"));
        if (model.isThereAnyActiveStaffList())
            dataSource.addColumn(new BlockColumn("employeeHR", "Кадровая расстановка"));
        ActionColumn actionColumn = new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteItem");
        actionColumn.setDisableSecondSubmit(false);
        actionColumn.setParametersResolver((entity, valueEntity) -> entity);
        dataSource.addColumn(actionColumn);

        model.setStaffRateDataSource(dataSource);
    }

    public void onClickAddStaffRate(IBusinessComponent component)
    {
        Model model = getModel(component);

        StaffRateToPgupsTransfDiffStaffRateExtractRelation item = new StaffRateToPgupsTransfDiffStaffRateExtractRelation();

        short discriminator = EntityRuntime.getMeta(StaffRateToPgupsTransfDiffStaffRateExtractRelation.class).getEntityCode();
        long id = EntityIDGenerator.generateNewId(discriminator);

        item.setId(id);
        item.setExtract(model.getExtract());
        model.getStaffRateItemList().add(item);

        model.getStaffRateDataSource().refresh();
    }

    public void onClickDeleteItem(IBusinessComponent component)
    {
        Model model = getModel(component);

        StaffRateToPgupsTransfDiffStaffRateExtractRelation item = component.getListenerParameter();

        model.getStaffRateItemList().remove(item);
        model.getStaffRateListForDelete().add(item);

        model.getStaffRateDataSource().refresh();

        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        Double staffRateSumm = 0d;
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation staffRateItem : model.getStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(staffRateItem.getId()))
            {
                Object value = staffRateMap.get(staffRateItem.getId());
                val = ((Number)value).doubleValue();
            }

            staffRateSumm += val;
        }

        PostBoundedWithQGandQL postBoundedWithQGandQL = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

        if (model.getExtract().getSalaryRaisingCoefficient() != null)
            model.getExtract().setSalary(model.getExtract().getSalaryRaisingCoefficient().getRecommendedSalary() * staffRateSumm);
        else if (postBoundedWithQGandQL.getSalary() != null)
            model.getExtract().setSalary(postBoundedWithQGandQL.getSalary() * staffRateSumm);
    }

    @SuppressWarnings("unchecked")
    public void fillStaffRateValueMaps(Model model)
    {
        Map<Long, Double> staffRateMap = new HashMap<>();
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateItemList())
            staffRateMap.put(item.getId(), item.getStaffRate());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(0)).setValueMap(staffRateMap);

        Map<Long, FinancingSource> finSrcMap = new HashMap<>();
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateItemList())
            finSrcMap.put(item.getId(), item.getFinancingSource());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(1)).setValueMap(finSrcMap);

        Map<Long, FinancingSourceItem> finSrcItmMap = new HashMap<>();
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateItemList())
            finSrcItmMap.put(item.getId(), item.getFinancingSourceItem());
        ((BlockColumn) model.getStaffRateDataSource().getColumn(2)).setValueMap(finSrcItmMap);

        if (model.isThereAnyActiveStaffList())
        {
            Map<Long, String> staffAllocItmMap = new HashMap<>();
            for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateItemList())
                staffAllocItmMap.put(item.getId(), model.getEmptyStaffRateAllocItemMap().get(new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem())));
            ((BlockColumn) model.getStaffRateDataSource().getColumn(3)).setValueMap(staffAllocItmMap);
        }
    }

    public void updateStaffRateValueMap(IBusinessComponent component)
    {
        Model model = component.getModel();
        if (!model.isThereAnyActiveStaffList())
            return;
        Map<Long, String> staffAllocItmMap = ((BlockColumn) model.getStaffRateDataSource().getColumn(3)).getValueMap();
        Map<Long, FinancingSource> finSourceMap = ((BlockColumn) model.getStaffRateDataSource().getColumn(1)).getValueMap();
        Map<Long, FinancingSourceItem> finSourceItemMap = ((BlockColumn) model.getStaffRateDataSource().getColumn(2)).getValueMap();
        StaffRateToPgupsTransfDiffStaffRateExtractRelation wrapper = model.getStaffRateDataSource().getCurrentEntity();
        staffAllocItmMap.put(wrapper.getId(), model.getEmptyStaffRateAllocItemMap().get(new CoreCollectionUtils.Pair<>(finSourceMap.get(wrapper.getId()), finSourceItemMap.get(wrapper.getId()))));
    }


}