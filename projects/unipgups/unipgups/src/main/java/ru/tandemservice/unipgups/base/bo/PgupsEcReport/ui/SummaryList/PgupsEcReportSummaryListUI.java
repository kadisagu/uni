/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.PgupsEcReportManager;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryAdd.PgupsEcReportSummaryAdd;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
public class PgupsEcReportSummaryListUI extends UIPresenter
{
    private static final String ENROLLMENT_CAMPAIGN_FILTER = "enrollmentCampaign";

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (PgupsEcReportSummaryList.REPORTS_DS.equals(dataSource.getName()))
            dataSource.put(PgupsEcReportSummaryList.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
    }

    @Override
    public void onComponentRefresh()
    {
        initDefaults();
    }

    public void onPrintReport()
    {
        getActivationBuilder().asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickAdd()
    {
        getActivationBuilder().asRegion(PgupsEcReportSummaryAdd.class).activate();
    }

    public void onDeleteEntityFromList()
    {
        PgupsEcReportManager.instance().dao().delete(getListenerParameterAsLong());
    }

    private void initDefaults()
    {
        if (getEnrollmentCampaign() == null)
        {
            getSettings().set(ENROLLMENT_CAMPAIGN_FILTER, UniecDAOFacade.getEntrantDAO().getLastEnrollmentCampaign());
            saveSettings();
        }
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return getSettings().get(ENROLLMENT_CAMPAIGN_FILTER);
    }

}