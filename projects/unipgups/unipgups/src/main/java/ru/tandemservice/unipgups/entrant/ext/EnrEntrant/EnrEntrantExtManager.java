/* $Id:$ */
package ru.tandemservice.unipgups.entrant.ext.EnrEntrant;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Denis Perminov
 * @since 23.07.2014
 */
@Configuration
public class EnrEntrantExtManager extends BusinessObjectExtensionManager
{
}
