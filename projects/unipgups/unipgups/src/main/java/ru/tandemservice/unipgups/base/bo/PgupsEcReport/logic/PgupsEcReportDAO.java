/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.IPgupsEcReportVO;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 18.07.2013
 */
@Transactional
public class PgupsEcReportDAO extends CommonDAO implements IPgupsEcReportDAO
{
    @Override
    public void saveNewReport(IPgupsEcReport report, IPgupsEcReportVO data, IPgupsEcReportBuilder reportBuilder)
    {
        report.setFormingDate(new Date());
        data.fillReport(report);

        DatabaseFile content = reportBuilder.getContent();
        save(content);

        report.setContent(content);
        save(report);
    }
}