package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Распоряжение. О направлении работника в командировку
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsEmployeeMissionExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract";
    public static final String ENTITY_NAME = "pgupsEmployeeMissionExtract";
    public static final int VERSION_HASH = -1674636476;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_EXTERNAL_ORG_UNIT = "externalOrgUnit";
    public static final String P_PURPOSE_TEXT = "purposeText";
    public static final String P_TASK_NUMBER = "taskNumber";
    public static final String P_TASK_DATE = "taskDate";
    public static final String L_FINANCING_SOURCE_ORG_UNIT = "financingSourceOrgUnit";
    public static final String P_INTERNAL_FINANCING_SOURCE = "internalFinancingSource";
    public static final String P_COMMENT_TEXT = "commentText";
    public static final String P_WARRANT_NUMBER = "warrantNumber";
    public static final String P_WARRANT_DATE = "warrantDate";

    private Date _beginDate;     // Дата начала командировки
    private Date _endDate;     // Дата завершения командировки
    private ExternalOrgUnit _externalOrgUnit;     // Организация
    private String _purposeText;     // Цель командировки
    private String _taskNumber;     // Номер служебного задания
    private Date _taskDate;     // Дата служебного задания
    private ExternalOrgUnit _financingSourceOrgUnit;     // Источник финансирования
    private boolean _internalFinancingSource = false;     // Финансируется отправляющей стороной
    private String _commentText;     // Комментарий
    private String _warrantNumber;     // Номер командировочного удостоверения
    private Date _warrantDate;     // Дата командировочного удостоверения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала командировки. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата завершения командировки. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата завершения командировки. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Организация. Свойство не может быть null.
     */
    @NotNull
    public ExternalOrgUnit getExternalOrgUnit()
    {
        return _externalOrgUnit;
    }

    /**
     * @param externalOrgUnit Организация. Свойство не может быть null.
     */
    public void setExternalOrgUnit(ExternalOrgUnit externalOrgUnit)
    {
        dirty(_externalOrgUnit, externalOrgUnit);
        _externalOrgUnit = externalOrgUnit;
    }

    /**
     * @return Цель командировки. Свойство не может быть null.
     */
    @NotNull
    public String getPurposeText()
    {
        return _purposeText;
    }

    /**
     * @param purposeText Цель командировки. Свойство не может быть null.
     */
    public void setPurposeText(String purposeText)
    {
        dirty(_purposeText, purposeText);
        _purposeText = purposeText;
    }

    /**
     * @return Номер служебного задания.
     */
    @Length(max=255)
    public String getTaskNumber()
    {
        return _taskNumber;
    }

    /**
     * @param taskNumber Номер служебного задания.
     */
    public void setTaskNumber(String taskNumber)
    {
        dirty(_taskNumber, taskNumber);
        _taskNumber = taskNumber;
    }

    /**
     * @return Дата служебного задания.
     */
    public Date getTaskDate()
    {
        return _taskDate;
    }

    /**
     * @param taskDate Дата служебного задания.
     */
    public void setTaskDate(Date taskDate)
    {
        dirty(_taskDate, taskDate);
        _taskDate = taskDate;
    }

    /**
     * @return Источник финансирования.
     */
    public ExternalOrgUnit getFinancingSourceOrgUnit()
    {
        return _financingSourceOrgUnit;
    }

    /**
     * @param financingSourceOrgUnit Источник финансирования.
     */
    public void setFinancingSourceOrgUnit(ExternalOrgUnit financingSourceOrgUnit)
    {
        dirty(_financingSourceOrgUnit, financingSourceOrgUnit);
        _financingSourceOrgUnit = financingSourceOrgUnit;
    }

    /**
     * @return Финансируется отправляющей стороной. Свойство не может быть null.
     */
    @NotNull
    public boolean isInternalFinancingSource()
    {
        return _internalFinancingSource;
    }

    /**
     * @param internalFinancingSource Финансируется отправляющей стороной. Свойство не может быть null.
     */
    public void setInternalFinancingSource(boolean internalFinancingSource)
    {
        dirty(_internalFinancingSource, internalFinancingSource);
        _internalFinancingSource = internalFinancingSource;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getCommentText()
    {
        return _commentText;
    }

    /**
     * @param commentText Комментарий.
     */
    public void setCommentText(String commentText)
    {
        dirty(_commentText, commentText);
        _commentText = commentText;
    }

    /**
     * @return Номер командировочного удостоверения.
     */
    @Length(max=255)
    public String getWarrantNumber()
    {
        return _warrantNumber;
    }

    /**
     * @param warrantNumber Номер командировочного удостоверения.
     */
    public void setWarrantNumber(String warrantNumber)
    {
        dirty(_warrantNumber, warrantNumber);
        _warrantNumber = warrantNumber;
    }

    /**
     * @return Дата командировочного удостоверения.
     */
    public Date getWarrantDate()
    {
        return _warrantDate;
    }

    /**
     * @param warrantDate Дата командировочного удостоверения.
     */
    public void setWarrantDate(Date warrantDate)
    {
        dirty(_warrantDate, warrantDate);
        _warrantDate = warrantDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PgupsEmployeeMissionExtractGen)
        {
            setBeginDate(((PgupsEmployeeMissionExtract)another).getBeginDate());
            setEndDate(((PgupsEmployeeMissionExtract)another).getEndDate());
            setExternalOrgUnit(((PgupsEmployeeMissionExtract)another).getExternalOrgUnit());
            setPurposeText(((PgupsEmployeeMissionExtract)another).getPurposeText());
            setTaskNumber(((PgupsEmployeeMissionExtract)another).getTaskNumber());
            setTaskDate(((PgupsEmployeeMissionExtract)another).getTaskDate());
            setFinancingSourceOrgUnit(((PgupsEmployeeMissionExtract)another).getFinancingSourceOrgUnit());
            setInternalFinancingSource(((PgupsEmployeeMissionExtract)another).isInternalFinancingSource());
            setCommentText(((PgupsEmployeeMissionExtract)another).getCommentText());
            setWarrantNumber(((PgupsEmployeeMissionExtract)another).getWarrantNumber());
            setWarrantDate(((PgupsEmployeeMissionExtract)another).getWarrantDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsEmployeeMissionExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsEmployeeMissionExtract.class;
        }

        public T newInstance()
        {
            return (T) new PgupsEmployeeMissionExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "externalOrgUnit":
                    return obj.getExternalOrgUnit();
                case "purposeText":
                    return obj.getPurposeText();
                case "taskNumber":
                    return obj.getTaskNumber();
                case "taskDate":
                    return obj.getTaskDate();
                case "financingSourceOrgUnit":
                    return obj.getFinancingSourceOrgUnit();
                case "internalFinancingSource":
                    return obj.isInternalFinancingSource();
                case "commentText":
                    return obj.getCommentText();
                case "warrantNumber":
                    return obj.getWarrantNumber();
                case "warrantDate":
                    return obj.getWarrantDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "externalOrgUnit":
                    obj.setExternalOrgUnit((ExternalOrgUnit) value);
                    return;
                case "purposeText":
                    obj.setPurposeText((String) value);
                    return;
                case "taskNumber":
                    obj.setTaskNumber((String) value);
                    return;
                case "taskDate":
                    obj.setTaskDate((Date) value);
                    return;
                case "financingSourceOrgUnit":
                    obj.setFinancingSourceOrgUnit((ExternalOrgUnit) value);
                    return;
                case "internalFinancingSource":
                    obj.setInternalFinancingSource((Boolean) value);
                    return;
                case "commentText":
                    obj.setCommentText((String) value);
                    return;
                case "warrantNumber":
                    obj.setWarrantNumber((String) value);
                    return;
                case "warrantDate":
                    obj.setWarrantDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "externalOrgUnit":
                        return true;
                case "purposeText":
                        return true;
                case "taskNumber":
                        return true;
                case "taskDate":
                        return true;
                case "financingSourceOrgUnit":
                        return true;
                case "internalFinancingSource":
                        return true;
                case "commentText":
                        return true;
                case "warrantNumber":
                        return true;
                case "warrantDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "externalOrgUnit":
                    return true;
                case "purposeText":
                    return true;
                case "taskNumber":
                    return true;
                case "taskDate":
                    return true;
                case "financingSourceOrgUnit":
                    return true;
                case "internalFinancingSource":
                    return true;
                case "commentText":
                    return true;
                case "warrantNumber":
                    return true;
                case "warrantDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "externalOrgUnit":
                    return ExternalOrgUnit.class;
                case "purposeText":
                    return String.class;
                case "taskNumber":
                    return String.class;
                case "taskDate":
                    return Date.class;
                case "financingSourceOrgUnit":
                    return ExternalOrgUnit.class;
                case "internalFinancingSource":
                    return Boolean.class;
                case "commentText":
                    return String.class;
                case "warrantNumber":
                    return String.class;
                case "warrantDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsEmployeeMissionExtract> _dslPath = new Path<PgupsEmployeeMissionExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsEmployeeMissionExtract");
    }
            

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата завершения командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Организация. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getExternalOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
    {
        return _dslPath.externalOrgUnit();
    }

    /**
     * @return Цель командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getPurposeText()
     */
    public static PropertyPath<String> purposeText()
    {
        return _dslPath.purposeText();
    }

    /**
     * @return Номер служебного задания.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getTaskNumber()
     */
    public static PropertyPath<String> taskNumber()
    {
        return _dslPath.taskNumber();
    }

    /**
     * @return Дата служебного задания.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getTaskDate()
     */
    public static PropertyPath<Date> taskDate()
    {
        return _dslPath.taskDate();
    }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getFinancingSourceOrgUnit()
     */
    public static ExternalOrgUnit.Path<ExternalOrgUnit> financingSourceOrgUnit()
    {
        return _dslPath.financingSourceOrgUnit();
    }

    /**
     * @return Финансируется отправляющей стороной. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#isInternalFinancingSource()
     */
    public static PropertyPath<Boolean> internalFinancingSource()
    {
        return _dslPath.internalFinancingSource();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getCommentText()
     */
    public static PropertyPath<String> commentText()
    {
        return _dslPath.commentText();
    }

    /**
     * @return Номер командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getWarrantNumber()
     */
    public static PropertyPath<String> warrantNumber()
    {
        return _dslPath.warrantNumber();
    }

    /**
     * @return Дата командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getWarrantDate()
     */
    public static PropertyPath<Date> warrantDate()
    {
        return _dslPath.warrantDate();
    }

    public static class Path<E extends PgupsEmployeeMissionExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _externalOrgUnit;
        private PropertyPath<String> _purposeText;
        private PropertyPath<String> _taskNumber;
        private PropertyPath<Date> _taskDate;
        private ExternalOrgUnit.Path<ExternalOrgUnit> _financingSourceOrgUnit;
        private PropertyPath<Boolean> _internalFinancingSource;
        private PropertyPath<String> _commentText;
        private PropertyPath<String> _warrantNumber;
        private PropertyPath<Date> _warrantDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(PgupsEmployeeMissionExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата завершения командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(PgupsEmployeeMissionExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Организация. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getExternalOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> externalOrgUnit()
        {
            if(_externalOrgUnit == null )
                _externalOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_EXTERNAL_ORG_UNIT, this);
            return _externalOrgUnit;
        }

    /**
     * @return Цель командировки. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getPurposeText()
     */
        public PropertyPath<String> purposeText()
        {
            if(_purposeText == null )
                _purposeText = new PropertyPath<String>(PgupsEmployeeMissionExtractGen.P_PURPOSE_TEXT, this);
            return _purposeText;
        }

    /**
     * @return Номер служебного задания.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getTaskNumber()
     */
        public PropertyPath<String> taskNumber()
        {
            if(_taskNumber == null )
                _taskNumber = new PropertyPath<String>(PgupsEmployeeMissionExtractGen.P_TASK_NUMBER, this);
            return _taskNumber;
        }

    /**
     * @return Дата служебного задания.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getTaskDate()
     */
        public PropertyPath<Date> taskDate()
        {
            if(_taskDate == null )
                _taskDate = new PropertyPath<Date>(PgupsEmployeeMissionExtractGen.P_TASK_DATE, this);
            return _taskDate;
        }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getFinancingSourceOrgUnit()
     */
        public ExternalOrgUnit.Path<ExternalOrgUnit> financingSourceOrgUnit()
        {
            if(_financingSourceOrgUnit == null )
                _financingSourceOrgUnit = new ExternalOrgUnit.Path<ExternalOrgUnit>(L_FINANCING_SOURCE_ORG_UNIT, this);
            return _financingSourceOrgUnit;
        }

    /**
     * @return Финансируется отправляющей стороной. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#isInternalFinancingSource()
     */
        public PropertyPath<Boolean> internalFinancingSource()
        {
            if(_internalFinancingSource == null )
                _internalFinancingSource = new PropertyPath<Boolean>(PgupsEmployeeMissionExtractGen.P_INTERNAL_FINANCING_SOURCE, this);
            return _internalFinancingSource;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getCommentText()
     */
        public PropertyPath<String> commentText()
        {
            if(_commentText == null )
                _commentText = new PropertyPath<String>(PgupsEmployeeMissionExtractGen.P_COMMENT_TEXT, this);
            return _commentText;
        }

    /**
     * @return Номер командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getWarrantNumber()
     */
        public PropertyPath<String> warrantNumber()
        {
            if(_warrantNumber == null )
                _warrantNumber = new PropertyPath<String>(PgupsEmployeeMissionExtractGen.P_WARRANT_NUMBER, this);
            return _warrantNumber;
        }

    /**
     * @return Дата командировочного удостоверения.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract#getWarrantDate()
     */
        public PropertyPath<Date> warrantDate()
        {
            if(_warrantDate == null )
                _warrantDate = new PropertyPath<Date>(PgupsEmployeeMissionExtractGen.P_WARRANT_DATE, this);
            return _warrantDate;
        }

        public Class getEntityClass()
        {
            return PgupsEmployeeMissionExtract.class;
        }

        public String getEntityName()
        {
            return "pgupsEmployeeMissionExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
