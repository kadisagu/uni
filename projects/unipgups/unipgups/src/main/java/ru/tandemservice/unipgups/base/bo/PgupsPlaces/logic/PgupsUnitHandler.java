/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

import static org.tandemframework.hibsupport.dql.DQLExpressions.commonValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public class PgupsUnitHandler extends DefaultComboDataSourceHandler
{
    public PgupsUnitHandler(String ownerId)
    {
        super(ownerId, UniplacesUnit.class, UniplacesUnit.P_TITLE);
    }

    @Override
    protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
    {
        super.prepareConditions(ep);
        Object building = ep.context.get(IPgupsPlacesDAO.BUILDING);
        if (building != null)
            ep.dqlBuilder.where(eq(property(UniplacesUnit.building().fromAlias("e")), commonValue(building)));
    }
}
