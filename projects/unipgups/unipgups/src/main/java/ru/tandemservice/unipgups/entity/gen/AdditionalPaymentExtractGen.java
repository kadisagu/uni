package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об установлении ежемесячной надбавки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AdditionalPaymentExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.AdditionalPaymentExtract";
    public static final String ENTITY_NAME = "additionalPaymentExtract";
    public static final int VERSION_HASH = -762226777;
    private static IEntityMeta ENTITY_META;

    public static final String P_ADD_AGREEMENT_NUMBER = "addAgreementNumber";
    public static final String P_ADD_AGREEMENT_DATE = "addAgreementDate";
    public static final String L_CONTRACT_AGREEMENT_NEW = "contractAgreementNew";

    private String _addAgreementNumber;     // Номер доп. соглашения
    private Date _addAgreementDate;     // Дата доп. соглашения
    private ContractCollateralAgreement _contractAgreementNew;     // Новое доп. соглашение к трудовому договору

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getAddAgreementNumber()
    {
        return _addAgreementNumber;
    }

    /**
     * @param addAgreementNumber Номер доп. соглашения. Свойство не может быть null.
     */
    public void setAddAgreementNumber(String addAgreementNumber)
    {
        dirty(_addAgreementNumber, addAgreementNumber);
        _addAgreementNumber = addAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    public Date getAddAgreementDate()
    {
        return _addAgreementDate;
    }

    /**
     * @param addAgreementDate Дата доп. соглашения. Свойство не может быть null.
     */
    public void setAddAgreementDate(Date addAgreementDate)
    {
        dirty(_addAgreementDate, addAgreementDate);
        _addAgreementDate = addAgreementDate;
    }

    /**
     * @return Новое доп. соглашение к трудовому договору.
     */
    public ContractCollateralAgreement getContractAgreementNew()
    {
        return _contractAgreementNew;
    }

    /**
     * @param contractAgreementNew Новое доп. соглашение к трудовому договору.
     */
    public void setContractAgreementNew(ContractCollateralAgreement contractAgreementNew)
    {
        dirty(_contractAgreementNew, contractAgreementNew);
        _contractAgreementNew = contractAgreementNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof AdditionalPaymentExtractGen)
        {
            setAddAgreementNumber(((AdditionalPaymentExtract)another).getAddAgreementNumber());
            setAddAgreementDate(((AdditionalPaymentExtract)another).getAddAgreementDate());
            setContractAgreementNew(((AdditionalPaymentExtract)another).getContractAgreementNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AdditionalPaymentExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AdditionalPaymentExtract.class;
        }

        public T newInstance()
        {
            return (T) new AdditionalPaymentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "addAgreementNumber":
                    return obj.getAddAgreementNumber();
                case "addAgreementDate":
                    return obj.getAddAgreementDate();
                case "contractAgreementNew":
                    return obj.getContractAgreementNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "addAgreementNumber":
                    obj.setAddAgreementNumber((String) value);
                    return;
                case "addAgreementDate":
                    obj.setAddAgreementDate((Date) value);
                    return;
                case "contractAgreementNew":
                    obj.setContractAgreementNew((ContractCollateralAgreement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "addAgreementNumber":
                        return true;
                case "addAgreementDate":
                        return true;
                case "contractAgreementNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "addAgreementNumber":
                    return true;
                case "addAgreementDate":
                    return true;
                case "contractAgreementNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "addAgreementNumber":
                    return String.class;
                case "addAgreementDate":
                    return Date.class;
                case "contractAgreementNew":
                    return ContractCollateralAgreement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AdditionalPaymentExtract> _dslPath = new Path<AdditionalPaymentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AdditionalPaymentExtract");
    }
            

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.AdditionalPaymentExtract#getAddAgreementNumber()
     */
    public static PropertyPath<String> addAgreementNumber()
    {
        return _dslPath.addAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.AdditionalPaymentExtract#getAddAgreementDate()
     */
    public static PropertyPath<Date> addAgreementDate()
    {
        return _dslPath.addAgreementDate();
    }

    /**
     * @return Новое доп. соглашение к трудовому договору.
     * @see ru.tandemservice.unipgups.entity.AdditionalPaymentExtract#getContractAgreementNew()
     */
    public static ContractCollateralAgreement.Path<ContractCollateralAgreement> contractAgreementNew()
    {
        return _dslPath.contractAgreementNew();
    }

    public static class Path<E extends AdditionalPaymentExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<String> _addAgreementNumber;
        private PropertyPath<Date> _addAgreementDate;
        private ContractCollateralAgreement.Path<ContractCollateralAgreement> _contractAgreementNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.AdditionalPaymentExtract#getAddAgreementNumber()
     */
        public PropertyPath<String> addAgreementNumber()
        {
            if(_addAgreementNumber == null )
                _addAgreementNumber = new PropertyPath<String>(AdditionalPaymentExtractGen.P_ADD_AGREEMENT_NUMBER, this);
            return _addAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.AdditionalPaymentExtract#getAddAgreementDate()
     */
        public PropertyPath<Date> addAgreementDate()
        {
            if(_addAgreementDate == null )
                _addAgreementDate = new PropertyPath<Date>(AdditionalPaymentExtractGen.P_ADD_AGREEMENT_DATE, this);
            return _addAgreementDate;
        }

    /**
     * @return Новое доп. соглашение к трудовому договору.
     * @see ru.tandemservice.unipgups.entity.AdditionalPaymentExtract#getContractAgreementNew()
     */
        public ContractCollateralAgreement.Path<ContractCollateralAgreement> contractAgreementNew()
        {
            if(_contractAgreementNew == null )
                _contractAgreementNew = new ContractCollateralAgreement.Path<ContractCollateralAgreement>(L_CONTRACT_AGREEMENT_NEW, this);
            return _contractAgreementNew;
        }

        public Class getEntityClass()
        {
            return AdditionalPaymentExtract.class;
        }

        public String getEntityName()
        {
            return "additionalPaymentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
