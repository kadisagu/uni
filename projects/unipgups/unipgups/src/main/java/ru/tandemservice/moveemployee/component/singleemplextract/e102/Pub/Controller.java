/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubController;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public class Controller extends SingleEmployeeExtractPubController<ProlongationContractExtract, IDAO, Model>
{
}