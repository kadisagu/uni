/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;

/**
 * @author Alexey Lopatin
 * @since 26.07.2013
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Controller
{
    public void onClickPrintPgupsRequestReviewOriginals(IBusinessComponent component)
    {
        UniecScriptItem template = DataAccessServices.dao().getByCode(UniecScriptItem.class, "pgupsRequestReviewOriginals");
        BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component, template), true);
    }

    public void onClickPrintPgupsRequestReceiptOriginals(IBusinessComponent component)
    {
        UniecScriptItem template = DataAccessServices.dao().getByCode(UniecScriptItem.class, "pgupsRequestReceiptOriginals");
        BusinessComponentUtils.downloadDocument(buildDocumentRenderer(component, template), true);
    }

    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component, UniecScriptItem template)
    {
        EntrantRequestPrint erp = new EntrantRequestPrint();
        RtfDocument document = erp.createPrintForm(template.getTemplate(), (Long) component.getListenerParameter());
        return new CommonBaseRenderer().rtf().fileName("pgupsRequestRROriginals.rtf").document(document);
    }
}
