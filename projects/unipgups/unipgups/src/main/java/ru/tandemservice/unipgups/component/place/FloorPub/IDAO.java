/* $Id$ */
package ru.tandemservice.unipgups.component.place.FloorPub;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public interface IDAO  extends ru.tandemservice.uniplaces.component.place.FloorPub.IDAO
{
    void preparePgupsPlaceDataSource(Model model);
}