/* $Id$ */


package ru.tandemservice.moveemployee.component.singleemplextract.e100.AddEdit;


import java.util.List;
import java.util.Map;

import org.tandemframework.core.info.InfoCollector;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.ICommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public interface IDAO extends ICommonSingleEmployeeExtractAddEditDAO<CombinationPostExtract, Model>
{
    public void prepareStaffRateDataSource(Model model);

    public void prepareEmployeeStaffRateDataSource(Model model);

    public void validateCombinationPeriods(Model model, InfoCollector infoCollector);

    public Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> getStaffRateAllocItemMap(Model model);
}