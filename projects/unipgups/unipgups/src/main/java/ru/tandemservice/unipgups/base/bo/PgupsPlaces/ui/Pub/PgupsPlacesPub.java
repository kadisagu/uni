/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
@Configuration
public class PgupsPlacesPub extends BusinessComponentManager
{
    public static final String PLACE_ID = "placeId";
    public static final String PLACES_DS = "placesDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PLACES_DS, placesDSColumn(), placesDSHandler()))
                .create();
    }


    @Bean
    public ColumnListExtPoint placesDSColumn()
    {
        return columnListExtPointBuilder(PLACES_DS)
                .addColumn(publisherColumn("name", UniplacesPlace.title()).order())
                .addColumn(textColumn("number", UniplacesPlace.number()))
                .addColumn(actionColumn("edit", CommonDefines.ICON_EDIT, "onEditPlaceClick").permissionKey("editPlace"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onDeletePlaceClick")
                                   .alert("message:placesDS.delete.alert")
                                   .permissionKey("deletePlace"))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> placesDSHandler() {
        return new DefaultSearchDataSourceHandler(getName(), UniplacesPlace.class)
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long pgupsPlaceId = context.get(PLACE_ID);
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UniplacesPlace.class, "pl")
                        .column(property("pl"))
                        .joinEntity("pl", DQLJoinType.inner, UniPlacesPlaceExt.class, "ext", eq(property("pl.id"), property("ext", UniPlacesPlaceExt.place().id())))
                        .where(eq(property("ext", UniPlacesPlaceExt.pgupsPlace().id()), value(pgupsPlaceId)));
                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
            }
        };
    }
}