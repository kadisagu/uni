/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 30.08.2015
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<ChangeSalaryEmployeePostExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    @Override
    protected ChangeSalaryEmployeePostExtract createNewInstance()
    {
        return new ChangeSalaryEmployeePostExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        model.setLabourContract(UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost()));
    }


    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (!model.getExtract().getStartDate().before(model.getExtract().getEndDate()))
            errors.add("Дата окончания действия нового оклада должна быть позже даты начала.", "startDate");
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());
        model.getExtract().setOldSalary(model.getEmployeePost().getSalary());
    }
}