package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipgups_2x9x0_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

		////////////////////////////////////////////////////////////////////////////////
		// сущность pgupsEmployeeMission

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("pgupsemployeemission_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_pgupsemployeemission"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("employeepost_id", DBType.LONG).setNullable(false),
									  new DBColumn("externalorgunit_id", DBType.LONG).setNullable(false),
									  new DBColumn("financingsourceou_id", DBType.LONG),
									  new DBColumn("internalfinancingsource_p", DBType.BOOLEAN).setNullable(false),
									  new DBColumn("begindate_p", DBType.DATE).setNullable(false),
									  new DBColumn("enddate_p", DBType.DATE).setNullable(false),
									  new DBColumn("pgupsservicetask_id", DBType.LONG).setNullable(false),
									  new DBColumn("warrantnumber_p", DBType.createVarchar(255)),
									  new DBColumn("warrantdate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("pgupsEmployeeMission");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность pgupsMissionToExtractRelation

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("pgpsmssntextrctrltn_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_23dae677"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("extract_id", DBType.LONG).setNullable(false),
									  new DBColumn("mission_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("pgupsMissionToExtractRelation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность pgupsServiceTask

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("pgupsservicetask_t",
									  new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_pgupsservicetask"),
									  new DBColumn("discriminator", DBType.SHORT).setNullable(false),
									  new DBColumn("number_p", DBType.createVarchar(255)),
									  new DBColumn("taskdate_p", DBType.DATE),
									  new DBColumn("target_p", DBType.TEXT).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("pgupsServiceTask");

		}


		////////////////////////////////////////////////////////////////////////////////
		// сущность pgupsEmployeeMissionExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("pgupsemployeemissionextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_pgupsemployeemissionextract"), 
				new DBColumn("begindate_p", DBType.DATE).setNullable(false), 
				new DBColumn("enddate_p", DBType.DATE).setNullable(false), 
				new DBColumn("externalorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("purposetext_p", DBType.TEXT).setNullable(false), 
				new DBColumn("financingsourceorgunit_id", DBType.LONG),
				new DBColumn("commenttext_p", DBType.createVarchar(255)), 
				new DBColumn("warrantnumber_p", DBType.createVarchar(255)),
				new DBColumn("warrantdate_p", DBType.DATE),
				new DBColumn("tasknumber_p", DBType.createVarchar(255)),
				new DBColumn("taskdate_p", DBType.DATE),
				new DBColumn("internalfinancingsource_p", DBType.BOOLEAN).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("pgupsEmployeeMissionExtract");

		}


    }
}