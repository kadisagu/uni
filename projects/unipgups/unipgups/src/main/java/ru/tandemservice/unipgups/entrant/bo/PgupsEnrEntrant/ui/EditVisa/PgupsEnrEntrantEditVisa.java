/* $Id:$ */
package ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditVisa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Denis Perminov
 * @since 29.07.2014
 */
@Configuration
public class PgupsEnrEntrantEditVisa extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
