package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unipgups.entity.PgupsEmployeePostExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsEmployeePostExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.PgupsEmployeePostExt";
    public static final String ENTITY_NAME = "pgupsEmployeePostExt";
    public static final int VERSION_HASH = -138234609;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_NATURE_WORK = "natureWork";

    private EmployeePost _employeePost;     // Сотрудник
    private String _natureWork;     // Характер работы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Характер работы.
     */
    @Length(max=255)
    public String getNatureWork()
    {
        return _natureWork;
    }

    /**
     * @param natureWork Характер работы.
     */
    public void setNatureWork(String natureWork)
    {
        dirty(_natureWork, natureWork);
        _natureWork = natureWork;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PgupsEmployeePostExtGen)
        {
            setEmployeePost(((PgupsEmployeePostExt)another).getEmployeePost());
            setNatureWork(((PgupsEmployeePostExt)another).getNatureWork());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsEmployeePostExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsEmployeePostExt.class;
        }

        public T newInstance()
        {
            return (T) new PgupsEmployeePostExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeePost":
                    return obj.getEmployeePost();
                case "natureWork":
                    return obj.getNatureWork();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "natureWork":
                    obj.setNatureWork((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeePost":
                        return true;
                case "natureWork":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeePost":
                    return true;
                case "natureWork":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeePost":
                    return EmployeePost.class;
                case "natureWork":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsEmployeePostExt> _dslPath = new Path<PgupsEmployeePostExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsEmployeePostExt");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeePostExt#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Характер работы.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeePostExt#getNatureWork()
     */
    public static PropertyPath<String> natureWork()
    {
        return _dslPath.natureWork();
    }

    public static class Path<E extends PgupsEmployeePostExt> extends EntityPath<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<String> _natureWork;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeePostExt#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Характер работы.
     * @see ru.tandemservice.unipgups.entity.PgupsEmployeePostExt#getNatureWork()
     */
        public PropertyPath<String> natureWork()
        {
            if(_natureWork == null )
                _natureWork = new PropertyPath<String>(PgupsEmployeePostExtGen.P_NATURE_WORK, this);
            return _natureWork;
        }

        public Class getEntityClass()
        {
            return PgupsEmployeePostExt.class;
        }

        public String getEntityName()
        {
            return "pgupsEmployeePostExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
