/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.AddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.ICommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public interface IDAO extends ICommonSingleEmployeeExtractAddEditDAO<CancelCombinationPostExtract, Model>
{
    void prepareEmployeeStaffRateDataSource(Model model);
    void prepareCombinationStaffRateDataSource(Model model);
}