/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.PgupsEcReportManager;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataView.PgupsEcReportEntrantsTADataView;
import ru.tandemservice.unipgups.entity.report.PgupsEntrantsTADataReport;

/**
 * @author nvankov
 * @since 7/19/13
 */
@Configuration
public class PgupsEcReportEntrantsTADataList extends BusinessComponentManager
{
    // dataSource
    public static String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static String REPORT_LIST_DS = "reportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(REPORT_LIST_DS, reportListColumnExtPoint(), PgupsEcReportManager.instance().entrantsTADataReportListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportListColumnExtPoint()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(indicatorColumn("ico").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("formingDate", PgupsEntrantsTADataReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(PgupsEcReportEntrantsTADataView.class).order())
                .addColumn(textColumn("period", PgupsEntrantsTADataReport.periodTitle()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onPrintReport").permissionKey("printUniecStorableReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("reportListDSHandler.delete.alert", PgupsEntrantsTADataReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME)).permissionKey("deleteUniecStorableReport"))
                .create();
    }
}
