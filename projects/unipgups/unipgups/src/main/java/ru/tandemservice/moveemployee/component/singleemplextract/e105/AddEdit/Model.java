/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<PgupsEmployeeMissionExtract>
{
    private String _cityOrgUnit;
    private ISelectModel _externalOrgUnitModel;

    public String getCityOrgUnit()
    {
        return _cityOrgUnit;
    }

    public void setCityOrgUnit(String cityOrgUnit)
    {
        _cityOrgUnit = cityOrgUnit;
    }

    public ISelectModel getExternalOrgUnitModel()
    {
        return _externalOrgUnitModel;
    }

    public void setExternalOrgUnitModel(ISelectModel externalOrgUnitModel)
    {
        _externalOrgUnitModel = externalOrgUnitModel;
    }
}