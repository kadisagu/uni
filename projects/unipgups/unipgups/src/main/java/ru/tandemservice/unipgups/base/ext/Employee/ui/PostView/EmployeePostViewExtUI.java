/* $Id$ */
package ru.tandemservice.unipgups.base.ext.Employee.ui.PostView;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostViewUI;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMission;
import ru.tandemservice.unipgups.entity.PgupsEmployeePostExt;
import ru.tandemservice.unipgups.entity.PgupsServiceTask;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Ekaterina Zvereva
 * @since 04.09.2015
 */
public class EmployeePostViewExtUI extends UIAddon
{

    DynamicListDataSource<PgupsEmployeeMission> _missionsDS;
    EmployeePost _employeePost;
    PgupsEmployeePostExt _pgupsEmployeePostExt;

    public EmployeePostViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    public void setEmployeePost(EmployeePost employeePost)
    {
        _employeePost = employeePost;
    }

    public DynamicListDataSource<PgupsEmployeeMission> getMissionsDS()
    {
        return _missionsDS;
    }

    public void setMissionsDS(DynamicListDataSource<PgupsEmployeeMission> missionsDS)
    {
        _missionsDS = missionsDS;
    }

    public PgupsEmployeePostExt getPgupsEmployeePostExt()
    {
        return _pgupsEmployeePostExt;
    }

    public void setPgupsEmployeePostExt(PgupsEmployeePostExt pgupsEmployeePostExt)
    {
        _pgupsEmployeePostExt = pgupsEmployeePostExt;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        EmployeePostViewUI presenter = getPresenter();
        _employeePost = presenter.getEmployeePost();
        _pgupsEmployeePostExt = DataAccessServices.dao().get(PgupsEmployeePostExt.class, PgupsEmployeePostExt.employeePost(), _employeePost);
        IBusinessComponent component = getPresenterConfig().getBusinessComponent();
        prepareMissionsDataSource(component);
    }

    private void prepareMissionsDataSource(IBusinessComponent component)
    {
        DynamicListDataSource<PgupsEmployeeMission> dataSource = new DynamicListDataSource<>(component, component1 -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PgupsEmployeeMission.class, "mis");
            builder.where(eq(property("mis", PgupsEmployeeMission.L_EMPLOYEE_POST), value(getEmployeePost())));
            new DQLOrderDescriptionRegistry(PgupsEmployeeMission.class, "mis").applyOrder(builder,_missionsDS.getEntityOrder());
            UniBaseUtils.createPage(_missionsDS, builder, getSession());
        }, 15);

        dataSource.addColumn(new SimpleColumn("Организация", PgupsEmployeeMission.externalOrgUnit().titleWithLegalForm()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Дата начала командировки", PgupsEmployeeMission.P_BEGIN_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата окончания командировки", PgupsEmployeeMission.P_END_DATE, DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        HeadColumn headColumn = new HeadColumn("headColumn", "Служебное задание");
        headColumn.setHeaderAlign("center");
        headColumn.addColumn(new SimpleColumn("Номер", PgupsEmployeeMission.pgupsServiceTask().number()).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Дата", PgupsEmployeeMission.pgupsServiceTask().taskDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Цель командировки", PgupsEmployeeMission.pgupsServiceTask().target()).setClickable(false).setOrderable(false));
        dataSource.addColumn(headColumn);

        HeadColumn warrantColumn = new HeadColumn("headColumn", "Командировочное удостоверение");
        warrantColumn.setHeaderAlign("center");
        warrantColumn.addColumn(new SimpleColumn("Номер", PgupsEmployeeMission.warrantNumber()).setClickable(false).setOrderable(false).setAlign("center"));
        warrantColumn.addColumn(new SimpleColumn("Дата", PgupsEmployeeMission.warrantDate(), DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(warrantColumn);

        dataSource.addColumn(new SimpleColumn("Источник финансирования", PgupsEmployeeMission.financingSourceOU().title()).setClickable(false).setOrderable(false));


       setMissionsDS(dataSource);
    }

    public String getTaskString()
    {
        PgupsServiceTask task = getMissionsDS().getCurrentEntity().getPgupsServiceTask();
        StringBuilder taskBuilder = new StringBuilder().append(task.getNumber() != null? "№ " + task.getNumber() : "")
                .append(task.getTaskDate() != null? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(task.getTaskDate()) : "");
        if (taskBuilder.length() > 0)
            taskBuilder.append(", ");
        taskBuilder.append("цель - ").append(task.getTarget());
        return taskBuilder.toString();
    }

    public String getFinancingSource()
    {
        PgupsEmployeeMission mission = getMissionsDS().getCurrentEntity();
        if (mission.isInternalFinancingSource())
            return  "Финансируется отправляющей стороной";
        else
            return  mission.getFinancingSourceOU().getTitleWithLegalForm();
    }

    public String getWarrantString()
    {
        PgupsEmployeeMission mission = getMissionsDS().getCurrentEntity();
        StringBuilder warrantBuilder = new StringBuilder().append(mission.getWarrantNumber() != null? "№ " + mission.getWarrantNumber() : "")
                .append(mission.getWarrantDate() != null ? " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(mission.getWarrantDate()) : "");
        return warrantBuilder.toString();
    }


}