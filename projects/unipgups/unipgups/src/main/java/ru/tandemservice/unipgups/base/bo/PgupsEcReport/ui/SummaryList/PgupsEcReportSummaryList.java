/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryPub.PgupsEcReportSummaryPub;
import ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
@Configuration
public class PgupsEcReportSummaryList extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_PARAM = "enrollmentCampaign";
    public static final String REPORTS_DS = "reportListDS";
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(REPORTS_DS, getReportsDS(), reportsDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getReportsDS()
    {
        return columnListExtPointBuilder(REPORTS_DS)
                .addColumn(indicatorColumn("ico").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn(PgupsSummaryEnrollmentReport.P_FORMING_DATE, PgupsSummaryEnrollmentReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(PgupsEcReportSummaryPub.class).order())
                .addColumn(textColumn(PgupsSummaryEnrollmentReport.P_PERIOD_TITLE, PgupsSummaryEnrollmentReport.P_PERIOD_TITLE))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onPrintReport").permissionKey("printUniecStorableReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("reportListDS.delete.alert", PgupsSummaryEnrollmentReport.P_FORMING_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME)).permissionKey("deleteUniecStorableReport"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> reportsDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                EnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN_PARAM);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PgupsSummaryEnrollmentReport.class, "e").column("e")
                        .where(eq(property(PgupsSummaryEnrollmentReport.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
                        .order(property("e", input.getEntityOrder().getColumnName()), input.getEntityOrder().getDirection());

                return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
            }
        };
    }
}