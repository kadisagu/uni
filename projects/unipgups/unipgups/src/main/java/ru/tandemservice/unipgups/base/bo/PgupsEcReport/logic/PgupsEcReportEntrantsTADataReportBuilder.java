/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.TargetAdmissionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.EntrantsTADataReportVO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class PgupsEcReportEntrantsTADataReportBuilder
{
    private EntrantsTADataReportVO reportVO;
    private Session session;
    private Map<EnrollmentDirection, EnrollmentDirectionExamSetData> examMap;
    private EntrantDataUtil entrantDataUtil;
    private Map<Entrant, List<String>> personForeignLanguageMap;
    private Map<RequestedEnrollmentDirection, ProfileEducationOrgUnit> requestsMap;

    public PgupsEcReportEntrantsTADataReportBuilder(EntrantsTADataReportVO reportVO, Session session)
    {
        this.reportVO = reportVO;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "pgupsEntrantsTADataReport");

        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        fillData();
        //////////////
        // создаем печатную форму
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setSettings(template.getSettings());
        result.setHeader(template.getHeader());

        injectTables(result, template, getRequestedEnrollmentDirections());

        return RtfUtil.toByteArray(result);
    }

    private void fillData()
    {
        examMap = ExamSetUtil.getDirectionExamSetDataMap(session, reportVO.getEnrollmentCampaign(), reportVO.getStudentCategoryList(), reportVO.getCompensationType());
        requestsMap = getRequestedEnrollmentDirectionsMap();

        MQBuilder mqBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");
        mqBuilder.add(MQExpression.in("e", RequestedEnrollmentDirection.P_ID, EntityUtils.getIdsFromEntityList(Lists.newArrayList(requestsMap.keySet()))));

        entrantDataUtil = new EntrantDataUtil(session, reportVO.getEnrollmentCampaign(), mqBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK);

        personForeignLanguageMap = Maps.newHashMap();

        final List<PersonForeignLanguage> languageList = Lists.newArrayList();
        List<Long> ids = CommonBaseEntityUtil.getPropertiesList(requestsMap.keySet(), RequestedEnrollmentDirection.entrantRequest().entrant().person().id());
        BatchUtils.execute(ids, 100, elements -> {
            // Языки
            DQLSelectBuilder languagesBuilder = new DQLSelectBuilder().fromEntity(PersonForeignLanguage.class, "fl");
            languagesBuilder.where(in(property("fl", PersonForeignLanguage.person().id()), elements));
            final List<PersonForeignLanguage> languages = languagesBuilder.createStatement(session).list();
            for (PersonForeignLanguage language : languages)
            {
                languageList.add(language);
            }
        });

        Map<Person, List<PersonForeignLanguage>> languageMap = Maps.newHashMap();
        for(PersonForeignLanguage language: languageList)
        {
            if(!languageMap.keySet().contains(language.getPerson()))
                languageMap.put(language.getPerson(), Lists.<PersonForeignLanguage>newArrayList());
            if(!languageMap.get(language.getPerson()).contains(language))
                languageMap.get(language.getPerson()).add(language);
        }

        List<Entrant> entrants = CommonBaseEntityUtil.getPropertiesList(requestsMap.keySet(), RequestedEnrollmentDirection.entrantRequest().entrant());

        for(Entrant entrant : entrants)
        {
            if(!personForeignLanguageMap.keySet().contains(entrant))
                personForeignLanguageMap.put(entrant, Lists.<String>newArrayList());
            if (null != languageMap.get(entrant.getPerson()))
            {
                List<PersonForeignLanguage> languages = languageMap.get(entrant.getPerson());
                for (PersonForeignLanguage language : languages)
                {
                    if (!personForeignLanguageMap.get(entrant).contains(language.getLanguage().getTitle()))
                        personForeignLanguageMap.get(entrant).add(language.getLanguage().getTitle());
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void injectTables(RtfDocument result, RtfDocument template, Map<EnrollmentDirectionVO, Map<TargetAdmissionKind, Map<ExternalOrgUnit, List<EntrantData>>>> map)
    {
        List<EnrollmentDirectionVO> directions = new ArrayList<>(map.keySet());
        Collections.sort(directions, (o1, o2) -> {
            if(o1.getEnrollmentDirection().equals(o2.getEnrollmentDirection()))
            {
                if(null != o1.getProfileEducationOrgUnit() && null != o2.getProfileEducationOrgUnit())
                    return o1.getProfileEducationOrgUnit().getEducationOrgUnit().getTitle().compareTo(o2.getProfileEducationOrgUnit().getEducationOrgUnit().getTitle());
                else
                {
                    if(null == o1.getProfileEducationOrgUnit()) return 1;
                    else return -1;
                }
            }
            else
            {
                return o1.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix().compareTo(o2.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix());
            }

        });

        RtfString preList1 = new RtfString().boldBegin().append("Данные абитуриентов целевого приема на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).boldEnd().par().par();
        RtfString preList2 = new RtfString().par().par();
        RtfString preList3 = new RtfString().par().par();
        for (EnrollmentDirectionVO direction : directions)
        {
            RtfDocument page = template.getClone();

            List<String> marks = Lists.newArrayList();
            marks.addAll(getDisciplinesShortTitles(direction.getEnrollmentDirection()));

            RtfString p1 = new RtfString();
            RtfString p2 = new RtfString();
            RtfString p3 = new RtfString();

            // локальные переменные
            EducationOrgUnit ou = direction.getEnrollmentDirection().getEducationOrgUnit();
            EducationLevelsHighSchool hs = ou.getEducationLevelHighSchool();

            if (result.getElementList().isEmpty())
            {
                p1.toList().addAll(preList1.toList());
                p2.toList().addAll(preList2.toList());
                p3.toList().addAll(preList3.toList());
            }

            p1.append(hs.getPrintTitle() + " (" + ou.getDevelopForm().getTitle().toLowerCase() + ", " + ou.getDevelopCondition().getTitle().toLowerCase() + ", " + ou.getDevelopPeriod().getTitle().toLowerCase() + ")" + " " + reportVO.getCompensationType().getShortTitle());
            p2.append(direction.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getTitle());
            if (direction.getEnrollmentDirection().getEducationOrgUnit().getTerritorialOrgUnit() != null)
                p3.append(direction.getEnrollmentDirection().getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle());

            if(null != direction.getProfileEducationOrgUnit())
            {
                p1.par().boldBegin().append(direction.getProfileEducationOrgUnit().getEducationOrgUnit().getEducationLevelHighSchool().getDisplayableTitle()).boldEnd();
                p2.toList().addAll(preList2.toList());
                p3.toList().addAll(preList3.toList());
            }

            new RtfInjectModifier().put("P1", p1.toList()).put("P2", p2.toList()).put("P3", p3.toList()).modify(page);
            TargetAdmissionTable table = getTable(map.get(direction));
            new RtfTableModifier().put("T2", table.getRows()).put("T2", new TargetAdmissionRowInterceptor(marks, table.getUnitRows(), table.getBoldRows())).modify(page);
            result.getElementList().addAll(page.getElementList());
        }
    }

    private class TargetAdmissionTable
    {
        private String[][] _rows;
        private List<Integer> _unitRows;
        private List<Integer> _boldRows;

        private String[][] getRows()
        {
            return _rows;
        }

        private void setRows(String[][] rows)
        {
            _rows = rows;
        }

        private List<Integer> getUnitRows()
        {
            return _unitRows;
        }

        private void setUnitRows(List<Integer> unitRows)
        {
            _unitRows = unitRows;
        }

        private List<Integer> getBoldRows()
        {
            return _boldRows;
        }

        private void setBoldRows(List<Integer> boldRows)
        {
            _boldRows = boldRows;
        }
    }

    private List<String> getDisciplinesShortTitles(EnrollmentDirection direction)
    {
        List<SetDiscipline> disciplines = getDisciplines(direction);
        List<String> disciplineTitles = Lists.newArrayList();
        for (SetDiscipline discipline : disciplines)
        {
            disciplineTitles.add(discipline.getShortTitle());
        }
        return disciplineTitles;
    }

    private List<SetDiscipline> getDisciplines(EnrollmentDirection direction)
    {
        List<ExamSetItem> examSetItems = examMap.get(direction).getCategoryExamSet(DataAccessServices.dao().get(StudentCategory.class, StudentCategory.code(), StudentCategoryCodes.STUDENT_CATEGORY_STUDENT));
        List<SetDiscipline> resultList = Lists.newArrayList();
        List<SetDiscipline> requiredList = Lists.newArrayList();
        List<SetDiscipline> profileList = Lists.newArrayList();
        List<SetDiscipline> highSchoolList = Lists.newArrayList();
        for (ExamSetItem item : examSetItems)
        {
            if (UniecDefines.ENTRANCE_DISCIPLINE_KIND_REQUIRED.equals(item.getKind().getCode()))
            {
                requiredList.add(item.getSubject());
                if (null != item.getChoice())
                {
                    requiredList.addAll(item.getChoice());
                }
            }
            else if (UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE.equals(item.getKind().getCode()))
            {
                profileList.add(item.getSubject());
                if (null != item.getChoice())
                {
                    profileList.addAll(item.getChoice());
                }
            }
            else if (UniecDefines.ENTRANCE_DISCIPLINE_KIND_HIGH_SCHOOL.equals(item.getKind().getCode()))
            {
                highSchoolList.add(item.getSubject());
                if (null != item.getChoice())
                {
                    highSchoolList.addAll(item.getChoice());
                }
            }
        }
        resultList.addAll(requiredList);
        resultList.addAll(profileList);
        resultList.addAll(highSchoolList);
        return resultList;
    }

    private TargetAdmissionTable getTable(Map<TargetAdmissionKind, Map<ExternalOrgUnit, List<EntrantData>>> dataMap)
    {
        TargetAdmissionTable table = new TargetAdmissionTable();
        List<Integer> unitRows = Lists.newArrayList();
        List<Integer> boldRows = Lists.newArrayList();
        List<String[]> rows = Lists.newArrayList();
        List<TargetAdmissionKind> targetAdmissionKinds = Lists.newArrayList(dataMap.keySet());
        Collections.sort(targetAdmissionKinds, TargetAdmissionKind.ALL_LEVEL_PRIORITY_COMPARATOR);
        int unitRowsInt = 2;
        for(TargetAdmissionKind targetAdmissionKind : targetAdmissionKinds)
        {
            rows.add(new String[]{targetAdmissionKind.getTitle()});
            boldRows.add(unitRowsInt);
            unitRows.add(unitRowsInt++);
            List<ExternalOrgUnit> externalOrgUnits = Lists.newArrayList(dataMap.get(targetAdmissionKind).keySet());
            Collections.sort(externalOrgUnits, ITitled.TITLED_COMPARATOR);
            for(ExternalOrgUnit externalOrgUnit : externalOrgUnits)
            {
                rows.add(new String[]{externalOrgUnit.getTitle()});
                unitRows.add(unitRowsInt++);
                List<EntrantData> entrantDatas = dataMap.get(targetAdmissionKind).get(externalOrgUnit);
                Collections.sort(entrantDatas, new EntrantDataComparator());
                for(EntrantData data : entrantDatas)
                {
                    MainRow mainRow = new MainRow(data);
                    rows.add(mainRow.toStringArray());
                    unitRowsInt++;
                }
            }
        }
        if (rows.isEmpty())
            rows.add(new String[]{""});

        table.setRows(rows.toArray(new String[][]{}));
        table.setUnitRows(unitRows);
        table.setBoldRows(boldRows);
        return table;
    }

    @SuppressWarnings("unchecked")
    private Map<EnrollmentDirectionVO, Map<TargetAdmissionKind, Map<ExternalOrgUnit, List<EntrantData>>>> getRequestedEnrollmentDirections()
    {
        Map<EnrollmentDirectionVO, Map<TargetAdmissionKind, Map<ExternalOrgUnit, List<EntrantData>>>> result = new LinkedHashMap<>();

        for(RequestedEnrollmentDirection direction : requestsMap.keySet())
        {
            EnrollmentDirectionVO enrollmentDirection = new EnrollmentDirectionVO(direction.getEnrollmentDirection(), requestsMap.get(direction));
            TargetAdmissionKind targetAdmissionKind = direction.getTargetAdmissionKind();
            ExternalOrgUnit externalOrgUnit = direction.getExternalOrgUnit();
            EntrantData entrantData = new EntrantData(direction);
            if(!result.containsKey(enrollmentDirection))
                result.put(enrollmentDirection, Maps.<TargetAdmissionKind, Map<ExternalOrgUnit, List<EntrantData>>>newHashMap());
            if(!result.get(enrollmentDirection).containsKey(targetAdmissionKind))
                result.get(enrollmentDirection).put(targetAdmissionKind, Maps.<ExternalOrgUnit, List<EntrantData>>newHashMap());
            if(!result.get(enrollmentDirection).get(targetAdmissionKind).containsKey(externalOrgUnit))
                result.get(enrollmentDirection).get(targetAdmissionKind).put(externalOrgUnit, Lists.<EntrantData>newArrayList());
            if(!result.get(enrollmentDirection).get(targetAdmissionKind).get(externalOrgUnit).contains(entrantData))
                result.get(enrollmentDirection).get(targetAdmissionKind).get(externalOrgUnit).add(entrantData);
        }


        for (EnrollmentDirectionVO enrollmentDirection : result.keySet())
        {
            if(null != result.get(enrollmentDirection))
            {
                for(TargetAdmissionKind targetAdmissionKind : result.get(enrollmentDirection).keySet())
                {
                    if(null != result.get(enrollmentDirection).get(targetAdmissionKind))
                    {
                        for(ExternalOrgUnit externalOrgUnit : result.get(enrollmentDirection).get(targetAdmissionKind).keySet())
                        {
                            fillEntrantDatas(result.get(enrollmentDirection).get(targetAdmissionKind).get(externalOrgUnit));
                        }
                    }
                }
            }
        }

        return result;
    }

    private Map<RequestedEnrollmentDirection, ProfileEducationOrgUnit> getRequestedEnrollmentDirectionsMap()
    {
        DQLSelectBuilder priorityReqBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red");
        priorityReqBuilder.joinEntity("red", DQLJoinType.left, PriorityProfileEduOu.class, "pr", "pr.requestedEnrollmentDirection=red");
        priorityReqBuilder.where(eq(property("pr", PriorityProfileEduOu.priority()), value(1)));
        priorityReqBuilder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        if (!reportVO.isAllEnrollmentDirections())
        {
            priorityReqBuilder.where(eq(property("red", RequestedEnrollmentDirection.enrollmentDirection().id()), value(reportVO.getEnrollmentDirection().getId())));
        }
        priorityReqBuilder.where(eq(property("red", RequestedEnrollmentDirection.compensationType().id()), value(reportVO.getCompensationType().getId())));
        priorityReqBuilder.where(eq(property("red", RequestedEnrollmentDirection.targetAdmission()), value(true)));
        priorityReqBuilder.where(isNotNull(property("red", RequestedEnrollmentDirection.externalOrgUnit())));
        priorityReqBuilder.where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("red"), reportVO.getFrom(), reportVO.getTo()));
        if (!reportVO.getStudentCategoryList().isEmpty())
            priorityReqBuilder.where(in(property("red", RequestedEnrollmentDirection.studentCategory().id()), EntityUtils.getIdsFromEntityList(reportVO.getStudentCategoryList())));
        priorityReqBuilder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)));

        DQLSelectBuilder profileBuilder = new DQLSelectBuilder().fromEntity(ProfileEducationOrgUnit.class, "prof");
        profileBuilder.column(property("prof", ProfileEducationOrgUnit.enrollmentDirection()));
        profileBuilder.where(eq(property("prof", ProfileEducationOrgUnit.enrollmentDirection().enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        if (!reportVO.isAllEnrollmentDirections())
        {
            profileBuilder.where(eq(property("prof", ProfileEducationOrgUnit.enrollmentDirection().id()), value(reportVO.getEnrollmentDirection().getId())));
        }

        DQLSelectBuilder dirBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "req");
        dirBuilder.where(eq(property("req", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        if (!reportVO.isAllEnrollmentDirections())
        {
            dirBuilder.where(eq(property("req", RequestedEnrollmentDirection.enrollmentDirection().id()), value(reportVO.getEnrollmentDirection().getId())));
        }
        dirBuilder.where(eq(property("req", RequestedEnrollmentDirection.compensationType().id()), value(reportVO.getCompensationType().getId())));
        dirBuilder.where(eq(property("req", RequestedEnrollmentDirection.targetAdmission()), value(true)));
        dirBuilder.where(isNotNull(property("req", RequestedEnrollmentDirection.externalOrgUnit())));
        dirBuilder.where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("req"), reportVO.getFrom(), reportVO.getTo()));
        if (!reportVO.getStudentCategoryList().isEmpty())
            dirBuilder.where(in(property("req", RequestedEnrollmentDirection.studentCategory().id()), EntityUtils.getIdsFromEntityList(reportVO.getStudentCategoryList())));
        dirBuilder.where(eq(property("req", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)));

        dirBuilder.where(notIn(property("req", RequestedEnrollmentDirection.enrollmentDirection()), profileBuilder.buildQuery()));

        List<Object[]> requestsWithProfile = priorityReqBuilder.createStatement(session).list();
        List<RequestedEnrollmentDirection> requestsWithoutProfile = dirBuilder.createStatement(session).list();

        Map<RequestedEnrollmentDirection, ProfileEducationOrgUnit> requestsMap = Maps.newHashMap();
        for(Object[] request : requestsWithProfile)
        {
            requestsMap.put((RequestedEnrollmentDirection)request[0], null != request[1] ? ((PriorityProfileEduOu)request[1]).getProfileEducationOrgUnit(): null);
        }
        for(RequestedEnrollmentDirection request : requestsWithoutProfile)
        {
            requestsMap.put(request, null);
        }

        return requestsMap;
    }

    @SuppressWarnings("deprecation")
    private void fillEntrantDatas(List<EntrantData> entrantDatas)
    {
        Collections.sort(entrantDatas, new EntrantDataComparator());
        int num = 1;
        for (EntrantData entrantData : entrantDatas)
        {
            entrantData.setNum(Integer.toString(num++));
            entrantData.setCompetition(entrantData.getRequestedEnrollmentDirection().getCompetitionKind().getTitle());

            List<Discipline2RealizationWayRelation> disciplines = getDiscipline2RealizationWayRelations(entrantData.getRequestedEnrollmentDirection().getEnrollmentDirection());
            Map<Discipline2RealizationWayRelation, Double> requestedDirDisciplines = entrantDataUtil.getMarkMap(entrantData.getRequestedEnrollmentDirection());
            Map<Discipline2RealizationWayRelation, ChosenEntranceDiscipline> chosenEntranceDisciplineMap = Maps.newHashMap();
            List<ChosenEntranceDiscipline> chosenEntranceDisciplines = Lists.newArrayList(entrantDataUtil.getChosenEntranceDisciplineSet(entrantData.getRequestedEnrollmentDirection()));
            for(ChosenEntranceDiscipline chosenEntranceDiscipline : chosenEntranceDisciplines)
            {
                chosenEntranceDisciplineMap.put(chosenEntranceDiscipline.getEnrollmentCampaignDiscipline(), chosenEntranceDiscipline);
            }
            for (Discipline2RealizationWayRelation discipline : disciplines)
            {
                ChosenEntranceDiscipline chosenEntranceDiscipline = chosenEntranceDisciplineMap.get(discipline);
                if (!requestedDirDisciplines.keySet().contains(discipline))
                {
                    entrantData.getMarks().add("-");
                }
                else
                {
                    if (new Double(-1).equals(requestedDirDisciplines.get(discipline)))
                        entrantData.getMarks().add("");
                    else
                    {
                        // FinalMarkSource
                        String finalMarkSource = "";
                        if(null != chosenEntranceDiscipline.getFinalMarkSource())
                        {
                            Integer finalMarkSourceInt = chosenEntranceDiscipline.getFinalMarkSource();
                            // по диплому олимпиады
                            if(UniecDefines.FINAL_MARK_OLYMPIAD == finalMarkSourceInt)
                                finalMarkSource = "Олимп";
                            // по дисциплине (экзамен)
                            else if(UniecDefines.FINAL_MARK_DISCIPLINE_EXAM == finalMarkSourceInt)
                                finalMarkSource = "Экз";
                            // по апелляции (экзамен)
                            else if(UniecDefines.FINAL_MARK_APPEAL_EXAM == finalMarkSourceInt)
                                finalMarkSource = "Экз";
                            // по дисциплине (тестирование)
                            else if(UniecDefines.FINAL_MARK_DISCIPLINE_TEST == finalMarkSourceInt)
                                finalMarkSource = "Тест";
                                // по апелляции (тестирование)
                            else if(UniecDefines.FINAL_MARK_APPEAL_TEST == finalMarkSourceInt)
                                finalMarkSource = "Тест";
                            // по дисциплине (собеседование)
                            else if(UniecDefines.FINAL_MARK_DISCIPLINE_INTERVIEW == finalMarkSourceInt)
                                finalMarkSource = "Собес";
                            // по апелляции (собеседование)
                            else if (UniecDefines.FINAL_MARK_APPEAL_INTERVIEW == finalMarkSourceInt)
                                finalMarkSource = "Собес";
                            // по дисциплине (ЕГЭ (вуз))
                            else if (UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL == finalMarkSourceInt)
                                finalMarkSource = "ЕГЭ (вуз)";
                            // по апелляции (ЕГЭ (вуз))
                            else if (UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL == finalMarkSourceInt)
                                finalMarkSource = "ЕГЭ (вуз)";
                            // финальная оценка вычислена по ЕГЭ (1-ая волна)
                            else if (UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1 == finalMarkSourceInt)
                                finalMarkSource = "ЕГЭ";
                            // финальная оценка вычислена по ЕГЭ (2-ая волна)
                            else if (UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2 == finalMarkSourceInt)
                                finalMarkSource = "ЕГЭ";
                        }
                        entrantData.getMarks().add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(requestedDirDisciplines.get(discipline)) + " (" + finalMarkSource +")");
                    }
                }
            }
            entrantData.setSumMarks(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(entrantDataUtil.getFinalMark(entrantData.getRequestedEnrollmentDirection())));

            //Данные об образовании
            if(null != entrantData.getEntrant().getPerson().getPersonEduInstitution())
            {
                PersonEduInstitution eduInstitution = entrantData.getEntrant().getPerson().getPersonEduInstitution();
                Double averageMark = eduInstitution.getAverageMark();
                entrantData.setEduInstitution((null != averageMark ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(averageMark) : "") + (null != eduInstitution.getGraduationHonour() ? (null != averageMark ? " - ": "") + eduInstitution.getGraduationHonour().getTitle(): ""));
            }

            //Язык
            if(null != personForeignLanguageMap.get(entrantData.getEntrant()))
            {
                entrantData.setLang(StringUtils.join(personForeignLanguageMap.get(entrantData.getEntrant()), ", "));
            }
        }
    }

    private List<Discipline2RealizationWayRelation> getDiscipline2RealizationWayRelations(EnrollmentDirection enrollmentDirection)
    {
        List<SetDiscipline> disciplines = getDisciplines(enrollmentDirection);
        List<Discipline2RealizationWayRelation> discipline2RealizationWayRelations = Lists.newArrayList();
        for (SetDiscipline discipline : disciplines)
        {
            discipline2RealizationWayRelations.addAll(discipline.getDisciplines());
        }
        return discipline2RealizationWayRelations;
    }

    private static class EntrantData
    {
        private String _num;
        private Entrant _entrant;
        private String _competition;
        private List<String> _marks = new ArrayList<>();
        private String _sumMarks;
        private String _lang;
        private String _eduInstitution;

        private RequestedEnrollmentDirection _requestedEnrollmentDirection;

        public EntrantData(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
            _entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _requestedEnrollmentDirection;
        }

        public Entrant getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Entrant entrant)
        {
            _entrant = entrant;
        }

        public List<String> getMarks()
        {
            return _marks;
        }

        public void setMarks(List<String> marks)
        {
            _marks = marks;
        }

        public String getSumMarks()
        {
            return _sumMarks;
        }

        public void setSumMarks(String sumMarks)
        {
            _sumMarks = sumMarks;
        }

        public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        public String getCompetition()
        {
            return _competition;
        }

        public void setCompetition(String competition)
        {
            _competition = competition;
        }

        private String getNum()
        {
            return _num;
        }

        private void setNum(String num)
        {
            _num = num;
        }

        private String getEduInstitution()
        {
            return _eduInstitution;
        }

        private void setEduInstitution(String eduInstitution)
        {
            _eduInstitution = eduInstitution;
        }

        private String getLang()
        {
            return _lang;
        }

        private void setLang(String lang)
        {
            _lang = lang;
        }
    }

    private static class MainRow
    {
        private EntrantData _entrantData;

        public MainRow(EntrantData entrantData)
        {
            _entrantData = entrantData;
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _entrantData.getRequestedEnrollmentDirection();
        }

        String[] toStringArray()
        {
            int columns = 12 + _entrantData.getMarks().size();
            int i = 0;
            String[] result = new String[columns];
            result[i++] = _entrantData.getNum();
            result[i++] = _entrantData.getEntrant().getPerson().getFullFio();
            result[i++] = _entrantData.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() ? "оригинал" : "копия";
            result[i++] = _entrantData.getRequestedEnrollmentDirection().getTargetAdmissionKind().getTitle();
            result[i++] = _entrantData.getRequestedEnrollmentDirection().getExternalOrgUnit().getTitle();
            result[i++] = _entrantData.getEntrant().getPersonalNumber();
            result[i++] = _entrantData.getEduInstitution();
            result[i++] = _entrantData.getEntrant().getPerson().isNeedDormitory() ? "да" : "нет";
            result[i++] = _entrantData.getLang();
            for (String mark : _entrantData.getMarks())
            {
                result[i++] = mark;
            }
            result[i++] = _entrantData.getSumMarks();
            String address = "";
            if(null != _entrantData.getEntrant().getPerson().getIdentityCard().getAddress() && _entrantData.getEntrant().getPerson().getIdentityCard().getAddress() instanceof AddressDetailed)
            {
                AddressItem settlement = ((AddressDetailed)_entrantData.getEntrant().getPerson().getIdentityCard().getAddress()).getSettlement();
                if(null != settlement)
                    address += StringUtils.isEmpty(settlement.getTitle()) ? "" : settlement.getTitleWithType();
            }
            result[i++] =  UniStringUtils.joinWithSeparator(", ",
                address,
                _entrantData.getEntrant().getPerson().getContactData().getPhoneFact(),
                _entrantData.getEntrant().getPerson().getContactData().getPhoneMobile());
            return result;
        }
    }

    class EntrantDataComparator implements Comparator<EntrantData>
    {
        @Override
        public int compare(EntrantData o1, EntrantData o2)
        {
            int i;
            if (0 != (i = Double.compare(entrantDataUtil.getFinalMark(o2.getRequestedEnrollmentDirection()), entrantDataUtil.getFinalMark(o1.getRequestedEnrollmentDirection())))) { return i; }
            if (0 != (i = CommonCollator.RUSSIAN_COLLATOR.compare(o1.getEntrant().getFio(), o2.getEntrant().getFio()))) { return i; }
            if (0 != (i = Long.compare(o1.getEntrant().getId(), o2.getEntrant().getId()))) { return i; }
            return i;
        }
    }

    class TargetAdmissionRowInterceptor extends RtfRowIntercepterBase
    {
        private List<String> _marks;
        private List<Integer> _unitRows;
        private List<Integer> _boldRows;

        TargetAdmissionRowInterceptor(List<String> marks, List<Integer> unitRows, List<Integer> boldRows)
        {
            _marks = marks;
            _unitRows = unitRows;
            _boldRows = boldRows;
        }

        @Override
        public void beforeModify(RtfTable table, int currentRowIndex)
        {

            if (null != _marks)
            {
                // разбиваем колонку по количеству вступительных испытаний
                int[] parts = new int[_marks.size()];
                Arrays.fill(parts, 1);

                // интерцептор для именования колонок с дисциплинами
                IRtfRowSplitInterceptor headerInterceptor = (newCell, index) -> {
                    IRtfElement text = RtfBean.getElementFactory().createRtfText(_marks.get(index));
                    newCell.getElementList().add(text);
                };

                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 9, headerInterceptor, parts);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 9, null, parts);
            }

        }

        @Override
        @SuppressWarnings("deprecation")
        public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
        {
            int cnt = 0;
            for (RtfRow row : newRowList)
            {
                if (_unitRows.contains(cnt))
                    RtfUtil.unitAllCell(row, 0);
                if(_boldRows.contains(cnt))
                    UniRtfUtil.setRowBold(row);
                cnt++;
            }
        }
    }

    class EnrollmentDirectionVO
    {
        private EnrollmentDirection _enrollmentDirection;
        private ProfileEducationOrgUnit _profileEducationOrgUnit;

        EnrollmentDirectionVO(EnrollmentDirection enrollmentDirection, ProfileEducationOrgUnit profileEducationOrgUnit)
        {
            _enrollmentDirection = enrollmentDirection;
            _profileEducationOrgUnit = profileEducationOrgUnit;
        }

        EnrollmentDirection getEnrollmentDirection()
        {
            return _enrollmentDirection;
        }

        void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
        {
            _enrollmentDirection = enrollmentDirection;
        }

        ProfileEducationOrgUnit getProfileEducationOrgUnit()
        {
            return _profileEducationOrgUnit;
        }

        void setProfileEducationOrgUnit(ProfileEducationOrgUnit profileEducationOrgUnit)
        {
            _profileEducationOrgUnit = profileEducationOrgUnit;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(obj instanceof EnrollmentDirectionVO)
            {
                EnrollmentDirectionVO dir = (EnrollmentDirectionVO) obj;
                if (null != _enrollmentDirection && null != _profileEducationOrgUnit && null != dir.getEnrollmentDirection() && null != dir.getProfileEducationOrgUnit())
                    return _enrollmentDirection.getId().equals(dir.getEnrollmentDirection().getId()) && _profileEducationOrgUnit.getId().equals(dir.getProfileEducationOrgUnit().getId());
                else if (null != _enrollmentDirection && null == _profileEducationOrgUnit && null != dir.getEnrollmentDirection() && null == dir.getProfileEducationOrgUnit())
                    return _enrollmentDirection.getId().equals(dir.getEnrollmentDirection().getId());
                else if (null != _enrollmentDirection && null != _profileEducationOrgUnit && null != dir.getEnrollmentDirection() && null == dir.getProfileEducationOrgUnit())
                    return false;
                else if (null != _enrollmentDirection && null == _profileEducationOrgUnit && null != dir.getEnrollmentDirection() && null != dir.getProfileEducationOrgUnit())
                    return false;
                else if (null != _enrollmentDirection && null == dir.getEnrollmentDirection()) return false;
                else if (null == _enrollmentDirection && null != dir.getEnrollmentDirection()) return false;
                else return false;
            }
            else
                return false;
        }

        @Override
        public int hashCode()
        {
            return (null != _enrollmentDirection ? _enrollmentDirection.hashCode() : 0) + (null != _profileEducationOrgUnit ? _profileEducationOrgUnit.hashCode() : 0);
        }


    }
}
