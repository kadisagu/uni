/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.component.singleemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.component.singleemplextract.ICommonExtractPrint;
import ru.tandemservice.moveemployee.entity.EmployeeSingleExtractOrder;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;
import ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation;

import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 07.09.2015
 */
public class PgupsTransferToDiffStaffRatesExtractPrint implements IPrintFormCreator<PgupsTransferToDiffStaffRatesExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, PgupsTransferToDiffStaffRatesExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        ICommonExtractPrint commonExtractPrint = (ICommonExtractPrint) ApplicationRuntime.getBean("employeeSingleOrder_orderPrint");
        commonExtractPrint.appendVisas(document, (EmployeeSingleExtractOrder) extract.getParagraph().getOrder());

        RtfInjectModifier modifier = CommonExtractPrint.createSingleExtractInjectModifier(extract);
        modifier.put("FIO", extract.getEmployee().getFullFio());
        modifier.put("orgUnit", getOrgUnitTitleWithParent(extract.getEntity().getOrgUnit()));

        modifier.put("beginDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));
        modifier.put("post", StringUtils.uncapitalize(extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()));
        modifier.put("fixedSalary", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getSalary()));

        Double staffValue = 0D;
        StringBuilder financingSource = new StringBuilder();
        List<EmployeePostStaffRateItem> staffItemList = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(extract.getEntity());
        for (EmployeePostStaffRateItem item : staffItemList)
        {
            if (financingSource.length()>0)
                financingSource.append(",");
            staffValue += item.getStaffRate();
            financingSource.append(item.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET) ? "бюджет" : "внебюджет");
            if (item.getFinancingSourceItem() != null)
                financingSource.append("(").append(item.getFinancingSourceItem().getTitle()).append(")");
        }
        modifier.put("parent", extract.getEntity().getPostRelation().getPostBoundedWithQGandQL().getPost().getEmployeeType().getTitle());

        modifier.put("staffRate", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffValue));
        modifier.put("staffRateInteger", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getStaffRateAfter()));
        modifier.put("financingSource", financingSource.toString());

        List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> staffItemRelations = DataAccessServices.dao().getList(StaffRateToPgupsTransfDiffStaffRateExtractRelation.class, StaffRateToPgupsTransfDiffStaffRateExtractRelation.extract(), extract);
        StringBuilder newFinancingBuilder = new StringBuilder();
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : staffItemRelations)
        {
            if (newFinancingBuilder.length()>0)
                newFinancingBuilder.append(",");
            newFinancingBuilder.append(item.getFinancingSource().getCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET) ? "бюджет" : "внебюджет");
            if (item.getFinancingSourceItem() != null)
                newFinancingBuilder.append("(").append(item.getFinancingSourceItem().getTitle()).append(")");
        }
        modifier.put("financingSourceNew", newFinancingBuilder.toString());
        modifier.put("workType", extract.getCharacterWork());
        modifier.put("contractAddAgreementDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getContractAddAgreementDate()));
        modifier.put("contractAddAgreementNumber", extract.getContractAddAgreementNumber());
        modifier.put("contractDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getContractDate()));
        modifier.put("contractNumber", extract.getContractNumber());

        modifier.modify(document);

        return document;
    }

    private String getOrgUnitTitleWithParent(OrgUnit orgUnit)
    {
        StringBuilder builder = new StringBuilder();
        if (orgUnit.getParent() != null)
        {
            builder.append(StringUtils.capitalize(orgUnit.getParent().getOrgUnitType().getTitle())).append(" «").append(orgUnit.getParent().getPrintTitle()).append("», ");
        }
        builder.append(StringUtils.capitalize(orgUnit.getOrgUnitType().getTitle())).append(" «").append(orgUnit.getPrintTitle()).append("»");
        return builder.toString();
    }
}