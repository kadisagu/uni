/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataView;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unipgups.entity.report.PgupsEntrantsTADataReport;

/**
 * @author nvankov
 * @since 7/19/13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class PgupsEcReportEntrantsTADataViewUI extends UIPresenter
{
    // fields

    private Long _reportId;
    private PgupsEntrantsTADataReport _report;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(PgupsEntrantsTADataReport.class, _reportId);

    }

    // Listeners

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", _reportId)
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report);
        deactivate();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public PgupsEntrantsTADataReport getReport()
    {
        return _report;
    }

    public void setReport(PgupsEntrantsTADataReport report)
    {
        _report = report;
    }

    public boolean getNotAllEnrollmentDirections()
    {
        return !_report.isAllEnrollmentDirections();
    }
}
