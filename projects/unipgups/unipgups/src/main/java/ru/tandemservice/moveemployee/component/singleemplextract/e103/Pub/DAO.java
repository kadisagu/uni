/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103.Pub;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;


/**
 * @author Ekaterina Zvereva
 * @since 31.08.2015
 */
public class DAO extends SingleEmployeeExtractPubDAO<ChangeSalaryEmployeePostExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        Double staffValue = 0D;
        for (EmployeePostStaffRateItem item: UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getExtract().getEntity()))
                staffValue +=item.getStaffRate();

        model.setStaffRateStr(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffValue));
    }
}