package ru.tandemservice.unipgups.reactors.daemon;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unipgups.entity.nsi.NsiUniEduProgramDisciplineStatistics;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

@Transactional
public class NsiUniEduProgramDisciplineStatisticsDaemon extends SharedBaseDao implements INsiUniEduProgramDisciplineStatisticsDaemon
{
    public static final int DAEMON_ITERATION_TIME = 30;

    private static boolean sync_locked;

    public static final SyncDaemon DAEMON = new SyncDaemon(NsiUniEduProgramDisciplineStatisticsDaemon.class.getName(), DAEMON_ITERATION_TIME, GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked)
                return;

            final INsiUniEduProgramDisciplineStatisticsDaemon dao = INsiUniEduProgramDisciplineStatisticsDaemon.instance.get();
            sync_locked = true;

            try
            {
                dao.initStatistics();
//                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "e")
//                        .where(eq(property("e", Student.status().active()), value(true)))
//                        .where(eq(property("e", Student.archival()), value(false)));
//                List<Student> students = DataAccessServices.dao().getList(builder);
//                for(Student student : students)
//                    dao.updateStatistics(student);
            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                logger.warn(t.getMessage(), t);
            }
            sync_locked = false;
        }
    };

    @Override
    protected void initDao() {
        DAEMON.wakeUpDaemon();
    }

    @Override
    public void initStatistics()
    {
        DQLUpdateBuilder builder = new DQLUpdateBuilder(NsiUniEduProgramDisciplineStatistics.class)
                .set(NsiUniEduProgramDisciplineStatistics.P_AMOUNT, value(0));
        createStatement(builder).execute();

        DQLSelectBuilder b = new DQLSelectBuilder().fromEntity(Student.class, "e")
                .joinEntity("e", DQLJoinType.left, EppStudent2EduPlanVersion.class, "v", and(
                        eq(property("e", Student.id()), property("v", EppStudent2EduPlanVersion.student().id())),
                        isNull(property("v", EppStudent2EduPlanVersion.removalDate()))
                ))
                .joinEntity("e", DQLJoinType.left, EducationOrgUnit.class, "eou", eq(property("e", Student.educationOrgUnit()), property("eou")))
                .joinEntity("e", DQLJoinType.left, Course.class, "c", eq(property("e", Student.course()), property("c")))
                .joinEntity("eou", DQLJoinType.left, OrgUnit.class, "fou", eq(property("eou", EducationOrgUnit.formativeOrgUnit()), property("fou")))
                .joinEntity("eou", DQLJoinType.left, OrgUnit.class, "tou", eq(property("eou", EducationOrgUnit.territorialOrgUnit()), property("tou")))
                .joinEntity("eou", DQLJoinType.left, DevelopForm.class, "df", eq(property("eou", EducationOrgUnit.developForm()), property("df")))
                .joinEntity("eou", DQLJoinType.left, EducationLevelsHighSchool.class, "school", eq(property("eou", EducationOrgUnit.educationLevelHighSchool()), property("school")))
                .joinEntity("school", DQLJoinType.left, EduProgramQualification.class, "aq", eq(property("school", EducationLevelsHighSchool.assignedQualification()), property("aq")))
                .joinEntity("school", DQLJoinType.left, EducationLevels.class, "lev", eq(property("school", EducationLevelsHighSchool.educationLevel()), property("lev")))
                .joinEntity("v", DQLJoinType.left, EppEduPlanVersion.class, "ev", eq(property("v", EppStudent2EduPlanVersion.eduPlanVersion()), property("ev")))
                .joinEntity("ev", DQLJoinType.left, DevelopGridTerm.class, "dgt", eq(property("ev", EppEduPlanVersion.developGridTerm()), property("dgt")))
                .joinEntity("dgt", DQLJoinType.left, Term.class, "t", eq(property("dgt", DevelopGridTerm.term()), property("t")))
                .joinEntity("e", DQLJoinType.left, NsiUniEduProgramDisciplineStatistics.class, "stat", eq(property("stat", DevelopGridTerm.term()), property("t")))
                .where(eq(property("e", Student.status().active()), value(true)))
                .where(eq(property("e", Student.archival()), value(false)))
                .column(DQLFunctions.count(property("e", Student.id())))
                .column(property("df", DevelopForm.id()))
                .column(property("c", Course.id()))
                .column(property("t", Term.id()))
                .column(property("aq", EduProgramQualification.id()))
                .column(property("lev", EducationLevels.id()))
                .column(property("fou", OrgUnit.id()))
                .column(property("tou", OrgUnit.id()))
                .group(property("df", DevelopForm.id()))
                .group(property("c", Course.id()))
                .group(property("t", Term.id()))
                .group(property("aq", EduProgramQualification.id()))
                .group(property("lev", EducationLevels.id()))
                .group(property("fou", OrgUnit.id()))
                .group(property("tou", OrgUnit.id()));
        List<Object[]> objs = createStatement(b).list();
        for(Object[] objects: objs)
        {
            Long count = (Long) objects[0];
            int amount = count.intValue();
            Long developFormId = (Long) objects[1];
            DevelopForm developForm = developFormId == null ? null : DataAccessServices.dao().get(DevelopForm.class, developFormId);
            Long courseId = (Long) objects[2];
            Course course = courseId == null ? null : DataAccessServices.dao().get(Course.class, courseId);
            Long termId = (Long) objects[3];
            Term term = termId == null ? null : DataAccessServices.dao().get(Term.class, termId);
            Long qualificationId = (Long) objects[4];
            EduProgramQualification qualification = qualificationId == null ? null : DataAccessServices.dao().get(EduProgramQualification.class, qualificationId);
            Long levelsId = (Long) objects[5];
            EducationLevels levels = levelsId == null ? null : DataAccessServices.dao().get(EducationLevels.class, levelsId);
            Long formativeOrgUnitId = (Long) objects[6];
            OrgUnit formativeOrgUnit = formativeOrgUnitId == null ? null : DataAccessServices.dao().get(OrgUnit.class, formativeOrgUnitId);
            Long territorialOrgUnitId = (Long) objects[7];
            OrgUnit territorialOrgUnit = territorialOrgUnitId == null ? null : DataAccessServices.dao().get(OrgUnit.class, territorialOrgUnitId);

//            if(term == null) {
//                DQLSelectBuilder b2 = new DQLSelectBuilder().fromEntity(Student.class, "e")
//                        .joinEntity("e", DQLJoinType.left, EppStudent2EduPlanVersion.class, "v", and(
//                                eq(property("e", Student.id()), property("v", EppStudent2EduPlanVersion.student().id())),
//                                isNull(property("v", EppStudent2EduPlanVersion.removalDate()))
//                        ))
//                        .column("e")
//                        .where(eq(property("e", Student.course().id()), value(courseId)))
//                        .where(eq(property("e", Student.educationOrgUnit().territorialOrgUnit().id()), value(territorialOrgUnitId)))
//                        .where(eq(property("e", Student.educationOrgUnit().formativeOrgUnit().id()), value(formativeOrgUnitId)))
//                        .where(isNull(property("v")))
//                        .where(eq(property("e", Student.educationOrgUnit().educationLevelHighSchool().assignedQualification().id()), value(qualificationId)))
//                        .where(eq(property("e", Student.educationOrgUnit().developForm().id()), value(developFormId)));
//                List<Student> students = createStatement(b2).list();
//                if(students.isEmpty())
//                    return;
//            }

            NsiUniEduProgramDisciplineStatistics statistics = findStatistics(developForm, course, term, qualification, levels, formativeOrgUnit, territorialOrgUnit);
            if(statistics == null) {
                statistics = new NsiUniEduProgramDisciplineStatistics();
                statistics.setAmount(amount);
                statistics.setDevelopForm(developForm);
                statistics.setCourse(course);
                statistics.setTerm(term);
                statistics.setQualification(qualification);
                statistics.setEducationLevels(levels);
                statistics.setFormativeOrgUnit(formativeOrgUnit);
                statistics.setTerritorialOrgUnit(territorialOrgUnit);
                DataAccessServices.dao().saveOrUpdate(statistics);
            } else if(statistics.getAmount() != amount) {
                statistics.setAmount(amount);
                DataAccessServices.dao().saveOrUpdate(statistics);
            }
        }
    }

    @Override
    public NsiUniEduProgramDisciplineStatistics findStatistics(DevelopForm developForm, Course course, Term term, EduProgramQualification qualification, EducationLevels levels, OrgUnit formativeOrgUnit, OrgUnit territorialOrgUnit) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiUniEduProgramDisciplineStatistics.class, "e");
        if(developForm != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.developForm().id()), value(developForm.getId())));
        else
            builder.where(isNull(property("e", NsiUniEduProgramDisciplineStatistics.developForm().id())));
        if(course != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.course().id()), value(course.getId())));
        else
            builder.where(isNull(property("e", NsiUniEduProgramDisciplineStatistics.course().id())));
        if(term != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.term().id()), value(term.getId())));
        else
            builder.where(isNull(property("e", NsiUniEduProgramDisciplineStatistics.term().id())));
        if(qualification != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.qualification().id()), value(qualification.getId())));
        else
            builder.where(isNull(property("e", NsiUniEduProgramDisciplineStatistics.qualification().id())));
        if(levels != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.educationLevels().id()), value(levels.getId())));
        else
            builder.where(isNull(property("e", NsiUniEduProgramDisciplineStatistics.educationLevels().id())));
        if(formativeOrgUnit != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.formativeOrgUnit().id()), value(formativeOrgUnit.getId())));
        else
            builder.where(isNull(property("e", NsiUniEduProgramDisciplineStatistics.formativeOrgUnit().id())));
        if(territorialOrgUnit != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.territorialOrgUnit().id()), value(territorialOrgUnit.getId())));
        else
            builder.where(isNull(property("e", NsiUniEduProgramDisciplineStatistics.territorialOrgUnit().id())));
        List<NsiUniEduProgramDisciplineStatistics> statisticsList = createStatement(builder).list();
        if(statisticsList.isEmpty())
            return null;
        if(statisticsList.size() == 1)
            return statisticsList.get(0);
        throw new ApplicationException("Too many statistics");
    }

    @Override
    public void updateStatistics(Student student)
    {
        EppStudent2EduPlanVersion s2epv = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
        if(s2epv == null) {
            logger.error(student.getId() + " s2epv null");
            //System.out.println(student.getId() + " s2epv null");
            return;
        }
        // находим все группы, куда он попадает
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(NsiUniEduProgramDisciplineStatistics.class, "e")
                .where(eq(property("e", NsiUniEduProgramDisciplineStatistics.developForm().id()), value(student.getEducationOrgUnit().getDevelopForm().getId())))
                .where(eq(property("e", NsiUniEduProgramDisciplineStatistics.course().id()), value(student.getCourse().getId())))
                .where(eq(property("e", NsiUniEduProgramDisciplineStatistics.term().id()), value(s2epv.getEduPlanVersion().getDevelopGridTerm().getTerm().getId())));
        EducationLevelsHighSchool highSchool = student.getEducationOrgUnit().getEducationLevelHighSchool();
        builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.educationLevels().id()), value(highSchool.getEducationLevel().getId())));
        EduProgramQualification qualification = highSchool.getAssignedQualification();
        if(qualification != null)
            builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.qualification().id()), value(qualification.getId())));
        builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.formativeOrgUnit().id()), value(student.getEducationOrgUnit().getFormativeOrgUnit().getId())));
        builder.where(eq(property("e", NsiUniEduProgramDisciplineStatistics.territorialOrgUnit().id()), value(student.getEducationOrgUnit().getTerritorialOrgUnit().getId())));
        List<NsiUniEduProgramDisciplineStatistics> list = createStatement(builder).list();
        if(list.isEmpty()) {
            if(qualification == null)
                System.out.println(student.getId() + " qualification null");
            if(highSchool == null)
                System.out.println(student.getId() + " highSchool null");
            NsiUniEduProgramDisciplineStatistics statistics = new NsiUniEduProgramDisciplineStatistics();
            statistics.setAmount(1);
            statistics.setDevelopForm(student.getEducationOrgUnit().getDevelopForm());
            statistics.setCourse(student.getCourse());
            statistics.setTerm(s2epv.getEduPlanVersion().getDevelopGridTerm().getTerm());
            statistics.setQualification(qualification);
            statistics.setEducationLevels(highSchool.getEducationLevel());
            statistics.setFormativeOrgUnit(student.getEducationOrgUnit().getFormativeOrgUnit());
            statistics.setTerritorialOrgUnit(student.getEducationOrgUnit().getTerritorialOrgUnit());
            DataAccessServices.dao().saveOrUpdate(statistics);
        } else if(list.size() == 1) {
            NsiUniEduProgramDisciplineStatistics statistics = list.get(0);
            statistics.setAmount(statistics.getAmount() + 1);
            DataAccessServices.dao().update(statistics);
        }
        else {
            Debug.message("Too many statistics for " + student.getId());
            logger.error("Too many statistics for " + student.getId());
        }
    }
}
