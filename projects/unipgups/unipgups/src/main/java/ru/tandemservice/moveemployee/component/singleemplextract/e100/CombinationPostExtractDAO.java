/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e100;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.moveemployee.MoveEmployeeDefines;
import ru.tandemservice.moveemployee.component.commons.CommonExtractCommitUtil;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.uniemp.util.StaffListPaymentsUtil;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 21.08.2015
 */
public class CombinationPostExtractDAO extends UniBaseDao implements IExtractComponentDao<CombinationPostExtract>
{
    /**
     * Внесение выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doCommit(CombinationPostExtract extract, Map parameters)
    {
        CommonExtractCommitUtil.validateCombinationPostAllocItems(extract, extract.getOrgUnit(), extract.getPostBoundedWithQGandQL(), null);

        ErrorCollector errorCollector = UserContext.getInstance().getErrorCollector();
        if (errorCollector.hasErrors())
            return;

        MoveEmployeeDaoFacade.getMoveEmployeeDao().saveExtractText(extract, MoveEmployeeDefines.EXTRACT_TEXT_CODE);
        List<CombinationPostStaffRateExtractRelation> staffRateExtractRelationList = getList(CombinationPostStaffRateExtractRelation.class, CombinationPostStaffRateExtractRelation.abstractEmployeeExtract(), extract);

        List<CombinationPostStaffRateExtAllocItemRelation> allocItemRelationList = getList(CombinationPostStaffRateExtAllocItemRelation.class, CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation(), staffRateExtractRelationList);

        for (CombinationPostStaffRateExtAllocItemRelation relation : allocItemRelationList)
        {
            if (!relation.isHasNewAllocItem() && relation.getChoseStaffListAllocationItem() != null)
                delete(relation.getChoseStaffListAllocationItem());
            relation.setChoseStaffListAllocationItem(null);
            update(relation);
        }

        CombinationPost combinationPost = new CombinationPost();
        combinationPost.setCombinationPostType(extract.getCombinationPostType());
        combinationPost.setEmployeePost(extract.getEntity());
        combinationPost.setOrgUnit(extract.getOrgUnit());
        combinationPost.setPostBoundedWithQGandQL(extract.getPostBoundedWithQGandQL());
        combinationPost.setMissingEmployeePost(extract.getMissingEmployeePost());
        combinationPost.setBeginDate(extract.getBeginDate());
        combinationPost.setEndDate(extract.getEndDate());
        combinationPost.setEtksLevels(extract.getEtksLevels());
        combinationPost.setSalary(extract.getSalary());
        combinationPost.setOrderNumber(extract.getParagraph().getOrder().getNumber());
        combinationPost.setCommitDateSystem(extract.getParagraph().getOrder().getCommitDate());
        combinationPost.setCollateralAgreementNumber(extract.getContractAddAgreementNumber());
        combinationPost.setCollateralAgreementDate(extract.getContractAddAgreementDate());
        combinationPost.setFreelance(extract.isFreelance());

        extract.setCreateCombinationPost(combinationPost);

        save(combinationPost);

        for (CombinationPostStaffRateExtractRelation relation : staffRateExtractRelationList)
        {
            CombinationPostStaffRateItem rateItem = new CombinationPostStaffRateItem();
            rateItem.setCombinationPost(combinationPost);
            rateItem.setFinancingSource(relation.getFinancingSource());
            rateItem.setFinancingSourceItem(relation.getFinancingSourceItem());
            rateItem.setStaffRate(relation.getStaffRate());

            save(rateItem);

            if (!allocItemRelationList.isEmpty())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getOrgUnit(), extract.getPostBoundedWithQGandQL(), relation.getFinancingSource(), relation.getFinancingSourceItem());

                StaffListAllocationItem allocItem = new StaffListAllocationItem();
                allocItem.setCombination(true);
                allocItem.setCombinationPost(combinationPost);
                allocItem.setStaffRate(relation.getStaffRate());
                allocItem.setFinancingSource(relation.getFinancingSource());
                allocItem.setFinancingSourceItem(relation.getFinancingSourceItem());
                allocItem.setStaffListItem(staffListItem);
                allocItem.setEmployeePost(extract.getEntity());
                if (allocItem.getRaisingCoefficient() != null)
                    allocItem.setMonthBaseSalaryFund(allocItem.getRaisingCoefficient().getRecommendedSalary() * allocItem.getStaffRate());
                else if (allocItem.getStaffListItem() != null)
                    allocItem.setMonthBaseSalaryFund(allocItem.getStaffListItem().getSalary() * allocItem.getStaffRate());

                save(allocItem);

                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocItem, getSession());
            }
        }

        MoveEmployeeDaoFacade.getMoveEmployeeDao().createOrUpdateContractAndAgreement(extract, "Установление доплат за работу по совмещению");

        update(extract);
    }

    /**
     * Отмена вносимых выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doRollback(CombinationPostExtract extract, Map parameters)
    {
        if (extract.getCreateCollateralAgreement() != null)
        {
            delete(extract.getCreateCollateralAgreement());
            extract.setCreateCollateralAgreement(null);
        }

        if (extract.getCreateEmployeeContract() != null)
        {
            delete(extract.getCreateEmployeeContract());
            extract.setCreateEmployeeContract(null);
        }

        MQBuilder allocBuilder = new MQBuilder(StaffListAllocationItem.ENTITY_CLASS, "a");
        allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.combination().s(), true));
        allocBuilder.add(MQExpression.eq("a", StaffListAllocationItem.combinationPost().s(), extract.getCreateCombinationPost()));

        for (StaffListAllocationItem allocItem : allocBuilder.<StaffListAllocationItem>getResultList(getSession()))
            delete(allocItem);

        delete(extract.getCreateCombinationPost());
        extract.setCreateCombinationPost(null);

        update(extract);

        MQBuilder staffRateBuilder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "sr");
        staffRateBuilder.add(MQExpression.eq("sr", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), extract));

        MQBuilder allocItemBuilder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "ai");
        allocItemBuilder.add(MQExpression.in("ai", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), staffRateBuilder));

        for (CombinationPostStaffRateExtAllocItemRelation relation : allocItemBuilder.<CombinationPostStaffRateExtAllocItemRelation>getResultList(getSession()))
        {
            if (!relation.isHasNewAllocItem())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(extract.getOrgUnit(), extract.getPostBoundedWithQGandQL(), relation.getCombinationPostStaffRateExtractRelation().getFinancingSource(), relation.getCombinationPostStaffRateExtractRelation().getFinancingSourceItem());

                StaffListAllocationItem allocationItem = new StaffListAllocationItem();

                allocationItem.setStaffRate(relation.getStaffRateHistory());
                allocationItem.setFinancingSource(relation.getCombinationPostStaffRateExtractRelation().getFinancingSource());
                allocationItem.setFinancingSourceItem(relation.getCombinationPostStaffRateExtractRelation().getFinancingSourceItem());
                allocationItem.setStaffListItem(staffListItem);
                allocationItem.setEmployeePost(relation.getEmployeePostHistory());
                if (relation.getCombinationPostHistory() != null && relation.isHasCombinationPost())
                {
                    allocationItem.setCombination(true);
                    allocationItem.setCombinationPost(relation.getCombinationPostHistory());
                }
                if (allocationItem.getEmployeePost() != null)
                    allocationItem.setRaisingCoefficient(relation.getEmployeePostHistory().getRaisingCoefficient());
                if (allocationItem.getRaisingCoefficient() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getRaisingCoefficient().getRecommendedSalary() * allocationItem.getStaffRate());
                else if (allocationItem.getStaffListItem() != null)
                    allocationItem.setMonthBaseSalaryFund(allocationItem.getStaffListItem().getSalary() * allocationItem.getStaffRate());

                save(allocationItem);
                StaffListPaymentsUtil.recalculateAllPaymentsValue(allocationItem, getSession());

                if (relation.getChoseStaffListAllocationItem() == null)
                    relation.setChoseStaffListAllocationItem(allocationItem);

                update(relation);
            }
        }
    }
}