package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unipgups_2x9x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность cancelCombinationPostExtract

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("cancelcombinationpostextract_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_8dbd308c"), 
				new DBColumn("combinationpost_id", DBType.LONG).setNullable(false), 
				new DBColumn("removaldate_p", DBType.DATE), 
				new DBColumn("changedstafflist_id", DBType.LONG), 
				new DBColumn("changestafflist_p", DBType.BOOLEAN), 
				new DBColumn("enddateold_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("cancelCombinationPostExtract");

		}


    }
}