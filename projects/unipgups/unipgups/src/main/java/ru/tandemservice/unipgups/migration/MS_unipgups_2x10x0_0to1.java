/* $Id$ */
package ru.tandemservice.unipgups.migration;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MergeBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * @author Andrey Avetisov
 * @since 13.04.2016
 */
public class MS_unipgups_2x10x0_0to1 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.0")
                };
    }

    @Override
    public void run(DBTool dbTool) throws Exception {

        final MergeBuilder.MergeRule mainRule = new MergeBuilder.MergeRuleBuilder("nsunedprgrmdscplnsttstcs_t")
                .key("developform_id", "course_id", "term_id", "qualification_id", "educationlevels_id", "formativeorgunit_id", "territorialorgunit_id")
                .order("id", OrderDirection.asc)
                .build();

        MigrationUtils.mergeBuilder(dbTool, mainRule).merge("nsiUniEduProgramDisciplineStatistics");
    }
}