/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e104.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.unipgups.entity.AdditionalPaymentExtract;

/**
 * @author Ekaterina Zvereva
 * @since 01.09.2015
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<AdditionalPaymentExtract>
{
    private EmployeeLabourContract _labourContract;
    private ISingleSelectModel _financingSourceItemModel;
    private ISingleSelectModel _financingSourceModel;
    private ISingleSelectModel _paymentModel;
    private EmployeeBonus _employeeBonus;

    public EmployeeLabourContract getLabourContract()
    {
        return _labourContract;
    }

    public void setLabourContract(EmployeeLabourContract labourContract)
    {
        _labourContract = labourContract;
    }

    public ISingleSelectModel getFinancingSourceItemModel()
    {
        return _financingSourceItemModel;
    }

    public void setFinancingSourceItemModel(ISingleSelectModel financingSourceItemModel)
    {
        _financingSourceItemModel = financingSourceItemModel;
    }

    public ISingleSelectModel getFinancingSourceModel()
    {
        return _financingSourceModel;
    }

    public void setFinancingSourceModel(ISingleSelectModel financingSourceModel)
    {
        _financingSourceModel = financingSourceModel;
    }

    public ISingleSelectModel getPaymentModel()
    {
        return _paymentModel;
    }

    public void setPaymentModel(ISingleSelectModel paymentModel)
    {
        _paymentModel = paymentModel;
    }

    public EmployeeBonus getEmployeeBonus()
    {
        return _employeeBonus;
    }

    public void setEmployeeBonus(EmployeeBonus employeeBonus)
    {
        _employeeBonus = employeeBonus;
    }
}