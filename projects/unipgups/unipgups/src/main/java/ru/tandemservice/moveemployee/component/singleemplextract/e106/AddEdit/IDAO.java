/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106.AddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.ICommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;

/**
 * @author Ekaterina Zvereva
 * @since 09.09.2015
 */
public interface IDAO extends ICommonSingleEmployeeExtractAddEditDAO<PgupsTransferToDiffStaffRatesExtract, Model>
{
    public void prepareEmployeeStaffRateDataSource(Model model);

    public void prepareStaffRateDataSource(Model model);
    public void prepareStaffRateList(Model model);
}