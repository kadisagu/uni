/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e100.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubController;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 20.08.2015
 */
public class Controller extends SingleEmployeeExtractPubController<CombinationPostExtract, IDAO, Model>
{
}