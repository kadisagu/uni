/* $Id:$ */
package ru.tandemservice.unipgups.entrant.bo.PgupsEnrEntrant.ui.EditMigrationCard;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unipgups.entity.entrant.PgupsEnrForeignEntrant;

/**
 * @author Denis Perminov
 * @since 29.07.2014
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrant.id", required = true))
public class PgupsEnrEntrantEditMigrationCardUI extends UIPresenter
{
    private EnrEntrant _enrEntrant = new EnrEntrant();
    private PgupsEnrForeignEntrant _foreignEntrant = new PgupsEnrForeignEntrant();

    @Override
    public void onComponentRefresh()
    {
        setEntrant(CommonDAO.DAO_CACHE.get().getNotNull(EnrEntrant.class, getEntrant().getId()));
        _foreignEntrant = CommonDAO.DAO_CACHE.get().get(PgupsEnrForeignEntrant.class, PgupsEnrForeignEntrant.L_ENTRANT, _enrEntrant);
        if (null == _foreignEntrant)
        {
            _foreignEntrant = new PgupsEnrForeignEntrant();
            _foreignEntrant.setEntrant(_enrEntrant);
        }
    }

    public void onClickApply()
    {
        CommonDAO.DAO_CACHE.get().saveOrUpdate(_foreignEntrant);
        deactivate();
    }

    public EnrEntrant getEntrant()
    {
        return _enrEntrant;
    }

    public void setEntrant(EnrEntrant entrant)
    {
        this._enrEntrant = entrant;
    }

    public PgupsEnrForeignEntrant getForeignEntrant()
    {
        return _foreignEntrant;
    }

    public void setForeignEntrant(PgupsEnrForeignEntrant foreignEntrant)
    {
        this._foreignEntrant = foreignEntrant;
    }
}
