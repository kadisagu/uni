/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubModel;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 31.08.2015
 */
public class Model extends SingleEmployeeExtractPubModel<ChangeSalaryEmployeePostExtract>
{
    private String _staffRateStr;

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }
}