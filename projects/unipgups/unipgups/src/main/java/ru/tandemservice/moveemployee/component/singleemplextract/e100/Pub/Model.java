/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e100.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubModel;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 20.08.2015
 */
public class Model  extends SingleEmployeeExtractPubModel<CombinationPostExtract>
{
    private String _staffRateStr;

    //Getters & Setters

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }

}