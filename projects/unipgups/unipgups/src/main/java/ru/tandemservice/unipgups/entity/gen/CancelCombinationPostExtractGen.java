package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.StaffList;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. Об отмене совмещения/увеличения объема работ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CancelCombinationPostExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.CancelCombinationPostExtract";
    public static final String ENTITY_NAME = "cancelCombinationPostExtract";
    public static final int VERSION_HASH = 1278558113;
    private static IEntityMeta ENTITY_META;

    public static final String L_COMBINATION_POST = "combinationPost";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String L_CHANGED_STAFF_LIST = "changedStaffList";
    public static final String P_CHANGE_STAFF_LIST = "changeStaffList";
    public static final String P_END_DATE_OLD = "endDateOld";

    private CombinationPost _combinationPost;     // Совмещение/увеличение объема работ в должности
    private Date _removalDate;     // Дата окончания совмещения
    private StaffList _changedStaffList;     // ШР активное на момент проведения приказа и в котором освобождаем ставки
    private Boolean _changeStaffList;     // Освобождаем ли ставку в ШР у выбранной должности
    private Date _endDateOld;     // Дата окончания до проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Совмещение/увеличение объема работ в должности. Свойство не может быть null.
     */
    @NotNull
    public CombinationPost getCombinationPost()
    {
        return _combinationPost;
    }

    /**
     * @param combinationPost Совмещение/увеличение объема работ в должности. Свойство не может быть null.
     */
    public void setCombinationPost(CombinationPost combinationPost)
    {
        dirty(_combinationPost, combinationPost);
        _combinationPost = combinationPost;
    }

    /**
     * @return Дата окончания совмещения.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата окончания совмещения.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * @return ШР активное на момент проведения приказа и в котором освобождаем ставки.
     */
    public StaffList getChangedStaffList()
    {
        return _changedStaffList;
    }

    /**
     * @param changedStaffList ШР активное на момент проведения приказа и в котором освобождаем ставки.
     */
    public void setChangedStaffList(StaffList changedStaffList)
    {
        dirty(_changedStaffList, changedStaffList);
        _changedStaffList = changedStaffList;
    }

    /**
     * @return Освобождаем ли ставку в ШР у выбранной должности.
     */
    public Boolean getChangeStaffList()
    {
        return _changeStaffList;
    }

    /**
     * @param changeStaffList Освобождаем ли ставку в ШР у выбранной должности.
     */
    public void setChangeStaffList(Boolean changeStaffList)
    {
        dirty(_changeStaffList, changeStaffList);
        _changeStaffList = changeStaffList;
    }

    /**
     * @return Дата окончания до проведения приказа.
     */
    public Date getEndDateOld()
    {
        return _endDateOld;
    }

    /**
     * @param endDateOld Дата окончания до проведения приказа.
     */
    public void setEndDateOld(Date endDateOld)
    {
        dirty(_endDateOld, endDateOld);
        _endDateOld = endDateOld;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof CancelCombinationPostExtractGen)
        {
            setCombinationPost(((CancelCombinationPostExtract)another).getCombinationPost());
            setRemovalDate(((CancelCombinationPostExtract)another).getRemovalDate());
            setChangedStaffList(((CancelCombinationPostExtract)another).getChangedStaffList());
            setChangeStaffList(((CancelCombinationPostExtract)another).getChangeStaffList());
            setEndDateOld(((CancelCombinationPostExtract)another).getEndDateOld());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CancelCombinationPostExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CancelCombinationPostExtract.class;
        }

        public T newInstance()
        {
            return (T) new CancelCombinationPostExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    return obj.getCombinationPost();
                case "removalDate":
                    return obj.getRemovalDate();
                case "changedStaffList":
                    return obj.getChangedStaffList();
                case "changeStaffList":
                    return obj.getChangeStaffList();
                case "endDateOld":
                    return obj.getEndDateOld();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    obj.setCombinationPost((CombinationPost) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "changedStaffList":
                    obj.setChangedStaffList((StaffList) value);
                    return;
                case "changeStaffList":
                    obj.setChangeStaffList((Boolean) value);
                    return;
                case "endDateOld":
                    obj.setEndDateOld((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                        return true;
                case "removalDate":
                        return true;
                case "changedStaffList":
                        return true;
                case "changeStaffList":
                        return true;
                case "endDateOld":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    return true;
                case "removalDate":
                    return true;
                case "changedStaffList":
                    return true;
                case "changeStaffList":
                    return true;
                case "endDateOld":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "combinationPost":
                    return CombinationPost.class;
                case "removalDate":
                    return Date.class;
                case "changedStaffList":
                    return StaffList.class;
                case "changeStaffList":
                    return Boolean.class;
                case "endDateOld":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CancelCombinationPostExtract> _dslPath = new Path<CancelCombinationPostExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CancelCombinationPostExtract");
    }
            

    /**
     * @return Совмещение/увеличение объема работ в должности. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getCombinationPost()
     */
    public static CombinationPost.Path<CombinationPost> combinationPost()
    {
        return _dslPath.combinationPost();
    }

    /**
     * @return Дата окончания совмещения.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @return ШР активное на момент проведения приказа и в котором освобождаем ставки.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getChangedStaffList()
     */
    public static StaffList.Path<StaffList> changedStaffList()
    {
        return _dslPath.changedStaffList();
    }

    /**
     * @return Освобождаем ли ставку в ШР у выбранной должности.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getChangeStaffList()
     */
    public static PropertyPath<Boolean> changeStaffList()
    {
        return _dslPath.changeStaffList();
    }

    /**
     * @return Дата окончания до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getEndDateOld()
     */
    public static PropertyPath<Date> endDateOld()
    {
        return _dslPath.endDateOld();
    }

    public static class Path<E extends CancelCombinationPostExtract> extends SingleEmployeeExtract.Path<E>
    {
        private CombinationPost.Path<CombinationPost> _combinationPost;
        private PropertyPath<Date> _removalDate;
        private StaffList.Path<StaffList> _changedStaffList;
        private PropertyPath<Boolean> _changeStaffList;
        private PropertyPath<Date> _endDateOld;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Совмещение/увеличение объема работ в должности. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getCombinationPost()
     */
        public CombinationPost.Path<CombinationPost> combinationPost()
        {
            if(_combinationPost == null )
                _combinationPost = new CombinationPost.Path<CombinationPost>(L_COMBINATION_POST, this);
            return _combinationPost;
        }

    /**
     * @return Дата окончания совмещения.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(CancelCombinationPostExtractGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @return ШР активное на момент проведения приказа и в котором освобождаем ставки.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getChangedStaffList()
     */
        public StaffList.Path<StaffList> changedStaffList()
        {
            if(_changedStaffList == null )
                _changedStaffList = new StaffList.Path<StaffList>(L_CHANGED_STAFF_LIST, this);
            return _changedStaffList;
        }

    /**
     * @return Освобождаем ли ставку в ШР у выбранной должности.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getChangeStaffList()
     */
        public PropertyPath<Boolean> changeStaffList()
        {
            if(_changeStaffList == null )
                _changeStaffList = new PropertyPath<Boolean>(CancelCombinationPostExtractGen.P_CHANGE_STAFF_LIST, this);
            return _changeStaffList;
        }

    /**
     * @return Дата окончания до проведения приказа.
     * @see ru.tandemservice.unipgups.entity.CancelCombinationPostExtract#getEndDateOld()
     */
        public PropertyPath<Date> endDateOld()
        {
            if(_endDateOld == null )
                _endDateOld = new PropertyPath<Date>(CancelCombinationPostExtractGen.P_END_DATE_OLD, this);
            return _endDateOld;
        }

        public Class getEntityClass()
        {
            return CancelCombinationPostExtract.class;
        }

        public String getEntityName()
        {
            return "cancelCombinationPostExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
