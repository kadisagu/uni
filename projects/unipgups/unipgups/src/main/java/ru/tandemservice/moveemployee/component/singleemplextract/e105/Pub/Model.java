/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubModel;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public class Model  extends SingleEmployeeExtractPubModel<PgupsEmployeeMissionExtract>
{
    private String _finansingSource;
    private String _warrant;
    private String _purposeText;
    private String _settlement;

    public String getSettlement()
    {
        return _settlement;
    }

    public void setSettlement(String settlement)
    {
        _settlement = settlement;
    }

    public String getFinansingSource()
    {
        return _finansingSource;
    }

    public void setFinansingSource(String finansingSource)
    {
        _finansingSource = finansingSource;
    }

    public String getWarrant()
    {
        return _warrant;
    }

    public void setWarrant(String warrant)
    {
        _warrant = warrant;
    }

    public String getPurposeText()
    {
        return _purposeText;
    }

    public void setPurposeText(String purposeText)
    {
        _purposeText = purposeText;
    }
}