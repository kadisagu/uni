/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.CombinationPostStaffRateItem;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<CancelCombinationPostExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.GENITIVE;
    }

    @Override
    protected CancelCombinationPostExtract createNewInstance()
    {
        return new CancelCombinationPostExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        if (model.isEditForm())
            model.setCombinationPostStaffRateItemList(getList(CombinationPostStaffRateItem.class, CombinationPostStaffRateItem.combinationPost(), model.getExtract().getCombinationPost()));

        model.setCombinationPostModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPost.class, "b")
                    .where(eq(property("b", CombinationPost.employeePost()), value(model.getEmployeePost())));
                if (o != null)
                    builder.where(eq(property("b", CombinationPost.id()), value((Long) o)));
                builder.order(property("b", CombinationPost.postBoundedWithQGandQL().title()));

                return new DQLListResultBuilder(builder);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                CombinationPost post = (CombinationPost) value;
                return post.getPostBoundedWithQGandQL().getTitle() + " (" + post.getOrgUnit().getTitle() + ") " +
                        " - " + post.getCombinationPostType().getShortTitle() +
                        "; период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(post.getBeginDate()) +
                        (post.getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(post.getEndDate()) : "");
            }
        });
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (!model.getExtract().getRemovalDate().after(model.getExtract().getCombinationPost().getBeginDate()))
            errors.add("Дата снятия доплаты должна быть позже чем дата начала работ в совмещаемой должности.", "removalDate");
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());
    }

    @Override
    public void prepareEmployeeStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getEmployeeStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmployeeStaffRateDataSource(), staffRateItems);
    }

    @Override
    public void prepareCombinationStaffRateDataSource(Model model)
    {
        model.getCombinationStaffRateDataSource().setCountRow(model.getCombinationPostStaffRateItemList().size());
        UniBaseUtils.createPage(model.getCombinationStaffRateDataSource(), model.getCombinationPostStaffRateItemList());
    }
}