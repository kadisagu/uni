/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;
import ru.tandemservice.unipgups.entity.StaffRateToPgupsTransfDiffStaffRateExtractRelation;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 07.09.2015
 */
public class DAO  extends CommonSingleEmployeeExtractAddEditDAO<PgupsTransferToDiffStaffRatesExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
        {
            model.setEmployeePost(model.getExtract().getEntity());
            model.setStaffRateItemList(getList(StaffRateToPgupsTransfDiffStaffRateExtractRelation.class, StaffRateToPgupsTransfDiffStaffRateExtractRelation.extract(), model.getExtract()));
        }
        StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit());
        model.setThereAnyActiveStaffList(null != staffList);

        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost());
        model.setDisabledContractFields(contract != null);
        if (contract != null)
        {
            model.getExtract().setContractType(contract.getType());
            model.getExtract().setContractNumber(contract.getNumber());
            model.getExtract().setContractDate(contract.getDate());
            model.getExtract().setContractBeginDate(contract.getBeginDate());
            model.getExtract().setContractEndDate(contract.getEndDate());
        }
        prepareEmptyStaffRateAllocItemMap(model, staffList);

        model.setContractTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(LabourContractType.class, "lct");
                if (o != null)
                    builder.where(eq(property("lct.id"), value((Long) o)));
                builder.order(property("lct", LabourContractType.title()));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setRaisingCoefficientListModel(new FullCheckSelectModel(SalaryRaisingCoefficient.P_FULL_TITLE)
        {
            @Override
            public ListResult findValues(String filter)
            {
                PostBoundedWithQGandQL postBoundedWithQGandQL = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

                if (null == postBoundedWithQGandQL)
                {
                    return ListResult.getEmpty();
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SalaryRaisingCoefficient.class, "rc")
                    .where(eq(property("rc", SalaryRaisingCoefficient.L_POST), value(postBoundedWithQGandQL.getPost())))
                    .where(eq(property("rc", SalaryRaisingCoefficient.L_QUALIFICATION_LEVEL), value(postBoundedWithQGandQL.getQualificationLevel())))
                    .where(likeUpper(property("rc", SalaryRaisingCoefficient.P_TITLE), value(CoreStringUtils.escapeLike(filter))))
                    .order(property("rc", SalaryRaisingCoefficient.P_RAISING_COEFFICIENT))
                    .order(property("rc", SalaryRaisingCoefficient.P_TITLE));
                return new ListResult<>(getList(builder));
            }
        });

        model.setEtksLevelModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EtksLevels.ENTITY_CLASS, "l")
                    .where(likeUpper(property("l", EtksLevels.title()), value(CoreStringUtils.escapeLike(filter))))
                    .order(property("l", EtksLevels.title()));
                if (o != null)
                    builder.where(eq(property("l.id"), value((Long) o)));

                return new DQLListResultBuilder(builder);
            }
        });


        model.setFinancingSourceModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                PostBoundedWithQGandQL postBoundedWithQGandQL = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

                if (model.getEmployeePost().getOrgUnit() == null || postBoundedWithQGandQL == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit());

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListItem.class, "sli")
                            .column(property("sli", StaffListItem.financingSource().id()))
                            .distinct()
                            .where(eq(property("sli", StaffListItem.staffList()), value(activeStaffList)))
                            .where(eq(property("sli", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL()), value(postBoundedWithQGandQL)));

                    if (!StringUtils.isEmpty(filter))
                        builder.where(like(property("sli", StaffListItem.financingSource().title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(eq(property("sli", StaffListItem.financingSource().id()), value((Long) o)));

                    return new DQLListResultBuilder(new DQLSelectBuilder().fromEntity(FinancingSource.class, "fs").where(in(property("fs.id"), builder.buildQuery())));
                }
                else
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSource.class, "fs")
                            .where(like(property("fs", FinancingSource.title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(eq(property("fs", FinancingSource.id()), value((Long) o)));

                    return new DQLListResultBuilder(builder);
                }
            }
        });

        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);

                PostBoundedWithQGandQL postBoundedWithQGandQL = model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

                if (model.getEmployeePost().getOrgUnit() == null || postBoundedWithQGandQL == null || financingSource == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());

                if (model.isThereAnyActiveStaffList())
                {
                    StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getEmployeePost().getOrgUnit());

                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListItem.class, "sl")
                            .column(property("sl", StaffListItem.financingSourceItem()))
                            .distinct()
                            .where(eq(property("sl", StaffListItem.staffList()), value(staffList)))
                            .where(eq(property("sl", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL()), value(postBoundedWithQGandQL)))
                            .where(eq(property("sl", StaffListItem.financingSource()), value(financingSource)))
                            .where(isNotNull(property("sl", StaffListItem.financingSourceItem())))
                            .where(like(property("sl", StaffListItem.financingSourceItem().title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(eq(property("sl", StaffListItem.financingSourceItem().id()), value((Long) o)));

                    return new DQLListResultBuilder(builder);
                }
                else
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSourceItem.class, "fsi")
                            .where(eq(property("fsi", FinancingSourceItem.financingSource().id()), value(financingSource.getId())))
                            .where(like(property("fsi", FinancingSourceItem.P_TITLE), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(eq(property("fsi", FinancingSourceItem.id()), value((Long) o)));

                    return new DQLListResultBuilder(builder);
                }
            }
        });


        model.setSummStaffRateBefore(0d);
        for(EmployeePostStaffRateItem staffRateItem :UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost()))
            model.setSummStaffRateBefore(model.getSummStaffRateBefore() + staffRateItem.getStaffRate());
    }

    private void prepareEmptyStaffRateAllocItemMap(Model model, StaffList staffList)
    {
        if (null == staffList)
            return;

        PostBoundedWithQGandQL postBoundedWithQGandQL= model.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL();

        String newStaffRateStr = "<Новая ставка> - ";
        //штатное расписание

        List<StaffListItem> staffListItems =  getList(new DQLSelectBuilder().fromEntity(StaffListItem.class, "sl")
                                                              .where(eq(property("sl", StaffListItem.staffList()), value(staffList)))
                                                              .where(eq(property("sl", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL()), value(postBoundedWithQGandQL))));

        List<StaffListAllocationItem> staffListAllocItems = getList(new DQLSelectBuilder().fromEntity(StaffListAllocationItem.class, "si")
                                        .where(in(property("si", StaffListAllocationItem.staffListItem()), staffListItems))
                                        .where(eq(property("si", StaffListAllocationItem.employeePost().postStatus().active()), value(Boolean.TRUE))));

        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, String> resultMap = new HashMap<>(staffListItems.size());
        Map<CoreCollectionUtils.Pair<FinancingSource, FinancingSourceItem>, Double> emptyDoubleMap = new HashMap<>(staffListItems.size());

        for (StaffListItem item : staffListItems)
        {
            StringBuilder valueBuilder = new StringBuilder();
            List<StaffListAllocationItem> sortedStaffAllocList = staffListAllocItems.stream()
                    .filter(e -> e.getStaffListItem().equals(item) && e.getFinancingSource().equals(item.getFinancingSource())
                            && ((e.getFinancingSourceItem()!= null && e.getFinancingSourceItem().equals(item.getFinancingSourceItem())) || e.getFinancingSourceItem() == null && item.getFinancingSourceItem() == null))
                    .collect(Collectors.toList());

            Double value = emptyDoubleMap.get(new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem()));
            if (value == null)
                value=0.0d;

            if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(model.getEmployeePost().getOrgUnit(), postBoundedWithQGandQL, item.getFinancingSource(), item.getFinancingSourceItem()))
            {
                Double diff = item.getStaffRate() - item.getOccStaffRate();
                if (diff > 0)
                {
                    valueBuilder.append(newStaffRateStr).append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)).append(";");
                    value +=diff;
                }
            }
            List<StaffListAllocationItem> employeeStaffAllocItems = sortedStaffAllocList.stream().filter(e -> e.getEmployeePost().equals(model.getEmployeePost())).collect(Collectors.toList());
            valueBuilder.append(employeeStaffAllocItems
                                        .stream()
                                        .map(row ->row.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row.getStaffRate()))
                                        .collect(Collectors.joining(";")));
            value += employeeStaffAllocItems.stream().map(StaffListAllocationItem::getStaffRate).collect(Collectors.summingDouble(e-> e));
            resultMap.put(new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem()), valueBuilder.toString());
            emptyDoubleMap.put(new CoreCollectionUtils.Pair<>(item.getFinancingSource(), item.getFinancingSourceItem()), value);
        }
        model.setEmptyStaffRateAllocItemMap(resultMap);
        model.setEmptyStaffRateAllocDoubleMap(emptyDoubleMap);

    }


    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        Double newStaffRate = 0d;
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation staffRateItem : model.getStaffRateItemList())
            newStaffRate += staffRateItem.getStaffRate();
        model.getExtract().setStaffRateBefore(model.getSummStaffRateBefore());

        model.getExtract().setStaffRateAfter(newStaffRate);


        actualizeStaffRateItemList(model);

        //если редактируем выписку, то перед тем как создать новые релейшены ставок - удаляем старые
        if (model.isEditForm())
        {
            new DQLDeleteBuilder(StaffRateToPgupsTransfDiffStaffRateExtractRelation.class)
                    .where(eq(property(StaffRateToPgupsTransfDiffStaffRateExtractRelation.extract()), value(model.getExtract())))
                    .createStatement(getSession()).execute();

        }

        model.getExtract().setEntity(model.getEmployeePost());
        super.update(model);

        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateItemList())
        {
            item.setExtract(model.getExtract());
            save(item);
        }

        // Сохраняем те, для которых была связь сотрудника со ставкой
        for(StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateListForDelete())
        {
            if (item.getEmployeePostStaffRateItem() != null)
                save(item);
        }
    }



    @Override
    public void prepareEmployeeStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getEmployeeStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmployeeStaffRateDataSource(), staffRateItems);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getStaffRateItemList());
        model.setNeedUpdateDataSource(false);
    }


    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.NOMINATIVE;
    }

    @Override
    protected PgupsTransferToDiffStaffRatesExtract createNewInstance()
    {
        return new PgupsTransferToDiffStaffRatesExtract();
    }

    @Override
    public void prepareStaffRateList(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        List<StaffRateToPgupsTransfDiffStaffRateExtractRelation> relationList = new ArrayList<>();
        for(EmployeePostStaffRateItem item : staffRateItems)
        {
            StaffRateToPgupsTransfDiffStaffRateExtractRelation relation = new StaffRateToPgupsTransfDiffStaffRateExtractRelation();
            relation.setExtract(model.getExtract());
            relation.setEmployeePostStaffRateItem(item);
            relation.setFinancingSource(item.getFinancingSource());
            relation.setFinancingSourceItem(item.getFinancingSourceItem());
            relation.setStaffRate(item.getStaffRate());

            short discriminator = EntityRuntime.getMeta(StaffRateToPgupsTransfDiffStaffRateExtractRelation.class).getEntityCode();
            long id = EntityIDGenerator.generateNewId(discriminator);

            relation.setId(id);
            relationList.add(relation);
        }
        model.setStaffRateItemList(relationList);

    }

    @SuppressWarnings("unchecked")
    public void actualizeStaffRateItemList(Model model)
    {
        //достаем введенные знаечения для Ставок и обновляем их в списке
        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Double> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

        final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateItemList())
        {
            Double val = 0.0d;
            if (null != staffRateMap.get(item.getId()))
            {
                Object value = staffRateMap.get(item.getId());
                if(value instanceof Long)
                    val = ((Long)value).doubleValue();
                else
                    val = (Double) value;
            }
            item.setStaffRate(val);
            item.setFinancingSource(finSrcMap.get(item.getId()));
            item.setFinancingSourceItem(finSrcItmMap.get(item.getId()));
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        actualizeStaffRateItemList(model);
        //уникальность полей источника финансирования
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item : model.getStaffRateItemList())
            for (StaffRateToPgupsTransfDiffStaffRateExtractRelation item2nd : model.getStaffRateItemList())
                if (!item.equals(item2nd))
                    if (item.getFinancingSource().equals(item2nd.getFinancingSource()))
                        if ((item.getFinancingSourceItem() == null && item2nd.getFinancingSourceItem() == null) ||
                                ((item.getFinancingSourceItem() != null && item2nd.getFinancingSourceItem() != null) && item.getFinancingSourceItem().equals(item2nd.getFinancingSourceItem())))
                            errors.add("Набор полей «Источник финансирования», «Источник финансирования (детально)» должен быть уникальным в рамках должности.",
                                       "finSrc_id_" + item.getId(), "finSrcItm_id_" + item.getId());

        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
            for (StaffRateToPgupsTransfDiffStaffRateExtractRelation staffRateItem : model.getStaffRateItemList())
            {
                if ((staffRateItem.getStaffRate() > model.getEmptyStaffRateAllocDoubleMap().get(new CoreCollectionUtils.Pair<>(staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem()))))
                    errors.add("Размер ставки " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() +
                                       " превышает суммарную доступную ставку.", "staffRate_id_" + staffRateItem.getId());
           }

        Double newStaffRate = 0d;
        List<String> staffRateFieldList = new ArrayList<>();
        for (StaffRateToPgupsTransfDiffStaffRateExtractRelation staffRateItem : model.getStaffRateItemList())
        {
            newStaffRate += staffRateItem.getStaffRate();

            staffRateFieldList.add("staffRate_id_" + staffRateItem.getId());
        }

        if (newStaffRate > 1.5)
            errors.add("Cтавка не может быть больше 1,5.", staffRateFieldList.toArray(new String[staffRateFieldList.size()]));

        if ((CommonBaseDateUtil.isBefore(model.getExtract().getBeginDate(), model.getExtract().getContractBeginDate())) ||
                (model.getExtract().getContractEndDate() != null && CommonBaseDateUtil.isAfter(model.getExtract().getBeginDate(), model.getExtract().getContractEndDate())))
            errors.add("Период изменения ставки должен попадать в период начала - окончания работы сотрудника.", "beginDate", "endDate");

    }

}