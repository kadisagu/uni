/*$Id$*/
package ru.tandemservice.unipgups.component.modularextract;

import org.tandemframework.rtf.node.IRtfElement;
import ru.tandemservice.movestudent.component.modularextract.StudentModularOrderPrint;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 24.09.2014
 */
public class PgupsStudentModularOrderPrint extends StudentModularOrderPrint
{
	@Override
	protected void addNewline(@NotNull List<IRtfElement> elements)
	{
	}
}
