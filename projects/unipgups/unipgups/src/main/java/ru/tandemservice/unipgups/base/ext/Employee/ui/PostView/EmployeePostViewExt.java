/* $Id$ */
package ru.tandemservice.unipgups.base.ext.Employee.ui.PostView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostView.EmployeePostView;

/**
 * @author Ekaterina Zvereva
 * @since 04.09.2015
 */
@Configuration
public class EmployeePostViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "pgups" + EmployeePostViewExtUI.class.getSimpleName();


    @Autowired
    private EmployeePostView _employeePostView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_employeePostView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EmployeePostViewExtUI.class))
                .create();
    }

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_employeePostView.employeePostTabPanelExtPoint())
                .addTab(htmlTab("Missions", "EmployeeMissionsTab").permissionKey("viewTabEmployeeMissions_employeePost"))
                .create();
    }


}