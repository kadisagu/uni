/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.PgupsPlacesManager;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic.IPgupsPlacesDAO;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;
import ru.tandemservice.uniplaces.entity.place.UniplacesFloor;
import ru.tandemservice.uniplaces.entity.place.UniplacesUnit;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
@Input({
        @Bind(key = "placeId", binding = "placeId"),
        @Bind(key = "floorId", binding = "floorId")
})
public class PgupsPlacesAddEditUI extends UIPresenter
{
    private Long _placeId;
    private Long _floorId;
    private UniPgupsPlace _place;
    private UniplacesBuilding _building;
    private UniplacesUnit _unit;
    private boolean _disabledChange;

    public boolean isDisabledChange()
    {
        return _disabledChange;
    }

    public Long getPlaceId()
    {
        return _placeId;
    }

    public void setPlaceId(Long placeId)
    {
        _placeId = placeId;
    }

    public UniplacesBuilding getBuilding()
    {
        return _building;
    }

    public void setBuilding(UniplacesBuilding building)
    {
        _building = building;
    }

    public UniplacesUnit getUnit()
    {
        return _unit;
    }

    public void setUnit(UniplacesUnit unit)
    {
        _unit = unit;
    }

    public UniPgupsPlace getPlace()
    {
        return _place;
    }

    public void setPlace(UniPgupsPlace place)
    {
        _place = place;
    }

    public Long getFloorId()
    {
        return _floorId;
    }

    public void setFloorId(Long floorId)
    {
        _floorId = floorId;
    }

    @Override
    public void onComponentRefresh()
    {
        if (_placeId != null)
        {
            _place = DataAccessServices.dao().getNotNull(_placeId);
            _unit = _place.getFloor().getUnit();
            _building = _place.getFloor().getUnit().getBuilding();
            _disabledChange = PgupsPlacesManager.instance().dao().existRelatedObjects(_placeId);
        }
        else
        {
            _place = new UniPgupsPlace();
            if (_floorId != null)
            {
                _place.setFloor(DataAccessServices.dao().get(UniplacesFloor.class, _floorId));
                _unit = _place.getFloor().getUnit();
                _building = _place.getFloor().getUnit().getBuilding();
                _disabledChange = true;
            }
        }



    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(PgupsPlacesAddEdit.UNITS_DS))
        {
            dataSource.put(IPgupsPlacesDAO.BUILDING, getBuilding());
        }
        else if (dataSource.getName().equals(PgupsPlacesAddEdit.FLOORS_DS))
        {
            dataSource.put(IPgupsPlacesDAO.BUILDING, getBuilding());
            dataSource.put(IPgupsPlacesDAO.UNIT, getUnit());
        }
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(getPlace());
        deactivate();
    }
}