package ru.tandemservice.unipgups.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводка о ходе приема, средних баллах, договорах (ПГУПС)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsSummaryEnrollmentReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport";
    public static final String ENTITY_NAME = "pgupsSummaryEnrollmentReport";
    public static final int VERSION_HASH = 83153472;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_ENROLLMENT_CAMPAIGN_STEP_STR = "enrollmentCampaignStepStr";
    public static final String P_STUDENT_CATEGORY_STR = "studentCategoryStr";
    public static final String P_QUALIFICATION_STR = "qualificationStr";
    public static final String P_COMPENSATION_TYPE_STR = "compensationTypeStr";
    public static final String P_DEVELOP_FORM_STR = "developFormStr";
    public static final String P_DEVELOP_CONDITION_STR = "developConditionStr";
    public static final String P_DEVELOP_TECH_STR = "developTechStr";
    public static final String P_DEVELOP_PERIOD_STR = "developPeriodStr";
    public static final String P_PERIOD_TITLE = "periodTitle";

    private DatabaseFile _content;     // Печатная форма
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _formingDate;     // Дата формирования
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _enrollmentCampaignStepStr;     // Стадия приемной кампании
    private String _studentCategoryStr;     // Категория поступающего
    private String _qualificationStr;     // Квалификация
    private String _compensationTypeStr;     // Вид возмещения затрат
    private String _developFormStr;     // Форма освоения
    private String _developConditionStr;     // Условие освоения
    private String _developTechStr;     // Технология освоения
    private String _developPeriodStr;     // Срок освоения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrollmentCampaignStepStr()
    {
        return _enrollmentCampaignStepStr;
    }

    /**
     * @param enrollmentCampaignStepStr Стадия приемной кампании. Свойство не может быть null.
     */
    public void setEnrollmentCampaignStepStr(String enrollmentCampaignStepStr)
    {
        dirty(_enrollmentCampaignStepStr, enrollmentCampaignStepStr);
        _enrollmentCampaignStepStr = enrollmentCampaignStepStr;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryStr()
    {
        return _studentCategoryStr;
    }

    /**
     * @param studentCategoryStr Категория поступающего.
     */
    public void setStudentCategoryStr(String studentCategoryStr)
    {
        dirty(_studentCategoryStr, studentCategoryStr);
        _studentCategoryStr = studentCategoryStr;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationStr()
    {
        return _qualificationStr;
    }

    /**
     * @param qualificationStr Квалификация.
     */
    public void setQualificationStr(String qualificationStr)
    {
        dirty(_qualificationStr, qualificationStr);
        _qualificationStr = qualificationStr;
    }

    /**
     * @return Вид возмещения затрат.
     */
    @Length(max=255)
    public String getCompensationTypeStr()
    {
        return _compensationTypeStr;
    }

    /**
     * @param compensationTypeStr Вид возмещения затрат.
     */
    public void setCompensationTypeStr(String compensationTypeStr)
    {
        dirty(_compensationTypeStr, compensationTypeStr);
        _compensationTypeStr = compensationTypeStr;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormStr()
    {
        return _developFormStr;
    }

    /**
     * @param developFormStr Форма освоения.
     */
    public void setDevelopFormStr(String developFormStr)
    {
        dirty(_developFormStr, developFormStr);
        _developFormStr = developFormStr;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionStr()
    {
        return _developConditionStr;
    }

    /**
     * @param developConditionStr Условие освоения.
     */
    public void setDevelopConditionStr(String developConditionStr)
    {
        dirty(_developConditionStr, developConditionStr);
        _developConditionStr = developConditionStr;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTechStr()
    {
        return _developTechStr;
    }

    /**
     * @param developTechStr Технология освоения.
     */
    public void setDevelopTechStr(String developTechStr)
    {
        dirty(_developTechStr, developTechStr);
        _developTechStr = developTechStr;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodStr()
    {
        return _developPeriodStr;
    }

    /**
     * @param developPeriodStr Срок освоения.
     */
    public void setDevelopPeriodStr(String developPeriodStr)
    {
        dirty(_developPeriodStr, developPeriodStr);
        _developPeriodStr = developPeriodStr;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PgupsSummaryEnrollmentReportGen)
        {
            setContent(((PgupsSummaryEnrollmentReport)another).getContent());
            setEnrollmentCampaign(((PgupsSummaryEnrollmentReport)another).getEnrollmentCampaign());
            setFormingDate(((PgupsSummaryEnrollmentReport)another).getFormingDate());
            setDateFrom(((PgupsSummaryEnrollmentReport)another).getDateFrom());
            setDateTo(((PgupsSummaryEnrollmentReport)another).getDateTo());
            setEnrollmentCampaignStepStr(((PgupsSummaryEnrollmentReport)another).getEnrollmentCampaignStepStr());
            setStudentCategoryStr(((PgupsSummaryEnrollmentReport)another).getStudentCategoryStr());
            setQualificationStr(((PgupsSummaryEnrollmentReport)another).getQualificationStr());
            setCompensationTypeStr(((PgupsSummaryEnrollmentReport)another).getCompensationTypeStr());
            setDevelopFormStr(((PgupsSummaryEnrollmentReport)another).getDevelopFormStr());
            setDevelopConditionStr(((PgupsSummaryEnrollmentReport)another).getDevelopConditionStr());
            setDevelopTechStr(((PgupsSummaryEnrollmentReport)another).getDevelopTechStr());
            setDevelopPeriodStr(((PgupsSummaryEnrollmentReport)another).getDevelopPeriodStr());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsSummaryEnrollmentReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsSummaryEnrollmentReport.class;
        }

        public T newInstance()
        {
            return (T) new PgupsSummaryEnrollmentReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "formingDate":
                    return obj.getFormingDate();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "enrollmentCampaignStepStr":
                    return obj.getEnrollmentCampaignStepStr();
                case "studentCategoryStr":
                    return obj.getStudentCategoryStr();
                case "qualificationStr":
                    return obj.getQualificationStr();
                case "compensationTypeStr":
                    return obj.getCompensationTypeStr();
                case "developFormStr":
                    return obj.getDevelopFormStr();
                case "developConditionStr":
                    return obj.getDevelopConditionStr();
                case "developTechStr":
                    return obj.getDevelopTechStr();
                case "developPeriodStr":
                    return obj.getDevelopPeriodStr();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "enrollmentCampaignStepStr":
                    obj.setEnrollmentCampaignStepStr((String) value);
                    return;
                case "studentCategoryStr":
                    obj.setStudentCategoryStr((String) value);
                    return;
                case "qualificationStr":
                    obj.setQualificationStr((String) value);
                    return;
                case "compensationTypeStr":
                    obj.setCompensationTypeStr((String) value);
                    return;
                case "developFormStr":
                    obj.setDevelopFormStr((String) value);
                    return;
                case "developConditionStr":
                    obj.setDevelopConditionStr((String) value);
                    return;
                case "developTechStr":
                    obj.setDevelopTechStr((String) value);
                    return;
                case "developPeriodStr":
                    obj.setDevelopPeriodStr((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "formingDate":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "enrollmentCampaignStepStr":
                        return true;
                case "studentCategoryStr":
                        return true;
                case "qualificationStr":
                        return true;
                case "compensationTypeStr":
                        return true;
                case "developFormStr":
                        return true;
                case "developConditionStr":
                        return true;
                case "developTechStr":
                        return true;
                case "developPeriodStr":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "formingDate":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "enrollmentCampaignStepStr":
                    return true;
                case "studentCategoryStr":
                    return true;
                case "qualificationStr":
                    return true;
                case "compensationTypeStr":
                    return true;
                case "developFormStr":
                    return true;
                case "developConditionStr":
                    return true;
                case "developTechStr":
                    return true;
                case "developPeriodStr":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "formingDate":
                    return Date.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "enrollmentCampaignStepStr":
                    return String.class;
                case "studentCategoryStr":
                    return String.class;
                case "qualificationStr":
                    return String.class;
                case "compensationTypeStr":
                    return String.class;
                case "developFormStr":
                    return String.class;
                case "developConditionStr":
                    return String.class;
                case "developTechStr":
                    return String.class;
                case "developPeriodStr":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsSummaryEnrollmentReport> _dslPath = new Path<PgupsSummaryEnrollmentReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsSummaryEnrollmentReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getEnrollmentCampaignStepStr()
     */
    public static PropertyPath<String> enrollmentCampaignStepStr()
    {
        return _dslPath.enrollmentCampaignStepStr();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getStudentCategoryStr()
     */
    public static PropertyPath<String> studentCategoryStr()
    {
        return _dslPath.studentCategoryStr();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getQualificationStr()
     */
    public static PropertyPath<String> qualificationStr()
    {
        return _dslPath.qualificationStr();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getCompensationTypeStr()
     */
    public static PropertyPath<String> compensationTypeStr()
    {
        return _dslPath.compensationTypeStr();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopFormStr()
     */
    public static PropertyPath<String> developFormStr()
    {
        return _dslPath.developFormStr();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopConditionStr()
     */
    public static PropertyPath<String> developConditionStr()
    {
        return _dslPath.developConditionStr();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopTechStr()
     */
    public static PropertyPath<String> developTechStr()
    {
        return _dslPath.developTechStr();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopPeriodStr()
     */
    public static PropertyPath<String> developPeriodStr()
    {
        return _dslPath.developPeriodStr();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getPeriodTitle()
     */
    public static SupportedPropertyPath<String> periodTitle()
    {
        return _dslPath.periodTitle();
    }

    public static class Path<E extends PgupsSummaryEnrollmentReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _enrollmentCampaignStepStr;
        private PropertyPath<String> _studentCategoryStr;
        private PropertyPath<String> _qualificationStr;
        private PropertyPath<String> _compensationTypeStr;
        private PropertyPath<String> _developFormStr;
        private PropertyPath<String> _developConditionStr;
        private PropertyPath<String> _developTechStr;
        private PropertyPath<String> _developPeriodStr;
        private SupportedPropertyPath<String> _periodTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(PgupsSummaryEnrollmentReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(PgupsSummaryEnrollmentReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(PgupsSummaryEnrollmentReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getEnrollmentCampaignStepStr()
     */
        public PropertyPath<String> enrollmentCampaignStepStr()
        {
            if(_enrollmentCampaignStepStr == null )
                _enrollmentCampaignStepStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_ENROLLMENT_CAMPAIGN_STEP_STR, this);
            return _enrollmentCampaignStepStr;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getStudentCategoryStr()
     */
        public PropertyPath<String> studentCategoryStr()
        {
            if(_studentCategoryStr == null )
                _studentCategoryStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_STUDENT_CATEGORY_STR, this);
            return _studentCategoryStr;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getQualificationStr()
     */
        public PropertyPath<String> qualificationStr()
        {
            if(_qualificationStr == null )
                _qualificationStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_QUALIFICATION_STR, this);
            return _qualificationStr;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getCompensationTypeStr()
     */
        public PropertyPath<String> compensationTypeStr()
        {
            if(_compensationTypeStr == null )
                _compensationTypeStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_COMPENSATION_TYPE_STR, this);
            return _compensationTypeStr;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopFormStr()
     */
        public PropertyPath<String> developFormStr()
        {
            if(_developFormStr == null )
                _developFormStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_DEVELOP_FORM_STR, this);
            return _developFormStr;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopConditionStr()
     */
        public PropertyPath<String> developConditionStr()
        {
            if(_developConditionStr == null )
                _developConditionStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_DEVELOP_CONDITION_STR, this);
            return _developConditionStr;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopTechStr()
     */
        public PropertyPath<String> developTechStr()
        {
            if(_developTechStr == null )
                _developTechStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_DEVELOP_TECH_STR, this);
            return _developTechStr;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getDevelopPeriodStr()
     */
        public PropertyPath<String> developPeriodStr()
        {
            if(_developPeriodStr == null )
                _developPeriodStr = new PropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_DEVELOP_PERIOD_STR, this);
            return _developPeriodStr;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport#getPeriodTitle()
     */
        public SupportedPropertyPath<String> periodTitle()
        {
            if(_periodTitle == null )
                _periodTitle = new SupportedPropertyPath<String>(PgupsSummaryEnrollmentReportGen.P_PERIOD_TITLE, this);
            return _periodTitle;
        }

        public Class getEntityClass()
        {
            return PgupsSummaryEnrollmentReport.class;
        }

        public String getEntityName()
        {
            return "pgupsSummaryEnrollmentReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPeriodTitle();
}
