/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.EnrollmentDirectionsComboDSHandler;

/**
 * @author nvankov
 * @since 7/19/13
 */
@Configuration
public class PgupsEcReportEntrantsTADataAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORG_UNIT_DS = "territorialOrgUnitDS";
    public static final String EDUCATION_LEVELS_HIGH_SCHOOL_DS = "educationLevelsHighSchoolDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_TECH_DS = "developTechDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.shortTitle().s()))
                .addDataSource(selectDS(STUDENT_CATEGORY_DS, studentCategoryDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_ORG_UNIT_DS, territorialOrgUnitDSHandler()).addColumn(OrgUnit.territorialTitle().s()))
                .addDataSource(selectDS(EDUCATION_LEVELS_HIGH_SCHOOL_DS, educationLevelsHighSchoolDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, developConditionDSHandler()))
                .addDataSource(selectDS(DEVELOP_TECH_DS, developTechDSHandler()))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, developPeriodDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), CompensationType.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler studentCategoryDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), StudentCategory.class);
    }

    @Bean
    public IDefaultComboDataSourceHandler formativeOrgUnitDSHandler()
    {
        return new EnrollmentDirectionsComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler territorialOrgUnitDSHandler()
    {
        return new EnrollmentDirectionsComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler educationLevelsHighSchoolDSHandler()
    {
        return new EnrollmentDirectionsComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developFormDSHandler()
    {
        return new EnrollmentDirectionsComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return new EnrollmentDirectionsComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developTechDSHandler()
    {
        return new EnrollmentDirectionsComboDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developPeriodDSHandler()
    {
        return new EnrollmentDirectionsComboDSHandler(getName());
    }
}
