/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsPlaces.logic;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import ru.tandemservice.uniplaces.entity.place.UniplacesBuilding;

/**
 * @author Ekaterina Zvereva
 * @since 18.10.2015
 */
public class PgupsBuildingHandler extends DefaultComboDataSourceHandler
{
    public PgupsBuildingHandler(String ownerId)
    {
        super(ownerId, UniplacesBuilding.class, UniplacesBuilding.P_TITLE);
        setOrderByProperty(UniplacesBuilding.P_TITLE);
    }
}