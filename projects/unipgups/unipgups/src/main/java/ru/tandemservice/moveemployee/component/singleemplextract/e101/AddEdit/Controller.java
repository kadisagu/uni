/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditController;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.employee.CombinationPost;
import ru.tandemservice.uniemp.entity.employee.EmployeePostStaffRateItem;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class Controller extends CommonSingleEmployeeExtractAddEditController<CancelCombinationPostExtract, IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareEmployeeStaffRateDataSource(component);
        prepareCombinationStaffRateDataSource(component);
    }

    public void prepareEmployeeStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getEmployeeStaffRateDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareEmployeeStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setEmployeeStaffRateDataSource(dataSource);
    }

    public void prepareCombinationStaffRateDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getCombinationStaffRateDataSource() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            getDao().prepareCombinationStaffRateDataSource(model);
        });

        dataSource.addColumn(new SimpleColumn("Ставка", EmployeePostStaffRateItem.P_STAFF_RATE));
        dataSource.addColumn(new SimpleColumn("Источник финансирования", EmployeePostStaffRateItem.financingSource().title().s()));
        dataSource.addColumn(new SimpleColumn("Источник финансирования (детально)", EmployeePostStaffRateItem.financingSourceItem().title().s()));

        model.setCombinationStaffRateDataSource(dataSource);
    }

    public void onChangeCombinationPost(IBusinessComponent component)
    {
        Model model = getModel(component);
        CombinationPost combinationPost = model.getExtract().getCombinationPost();
        if (combinationPost != null)
        {
            model.setCombinationPostStaffRateItemList(UniempDaoFacade.getUniempDAO().getCombinationPostStaffRateItemList(combinationPost));

            model.getCombinationStaffRateDataSource().refresh();
        }
    }
}