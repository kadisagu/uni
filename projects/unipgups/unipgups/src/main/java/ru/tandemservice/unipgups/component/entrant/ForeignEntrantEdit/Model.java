/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unipgups.entity.entrant.PgupsForeignEntrant;

import java.util.Date;

/**
 * @author Alexey Lopatin
 * @since 03.08.2013
 */
@Input({@org.tandemframework.core.component.Bind(key="publisherId", binding="entrant.id"),
        @org.tandemframework.core.component.Bind(key="foreignEntrantButton", binding="foreignEntrantButton")})
public class Model
{
    private String foreignEntrantButton;
    private Entrant entrant = new Entrant();
    private PgupsForeignEntrant foreignEntrant = new PgupsForeignEntrant();
    private ISingleSelectModel externalOrgUnitListModel;
    private ISelectModel viewVisaListModel;
    private DynamicListDataSource<ExternalOrgUnit> externalOrgUnit;

    public String getForeignEntrantButton()
    {
        return foreignEntrantButton;
    }

    public void setForeignEntrantButton(String foreignEntrantButton)
    {
        this.foreignEntrantButton = foreignEntrantButton;
    }

    public Entrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        this.entrant = entrant;
    }

    public PgupsForeignEntrant getForeignEntrant()
    {
        return foreignEntrant;
    }

    public void setForeignEntrant(PgupsForeignEntrant foreignEntrant)
    {
        this.foreignEntrant = foreignEntrant;
    }

    public ISingleSelectModel getExternalOrgUnitListModel()
    {
        return externalOrgUnitListModel;
    }

    public void setExternalOrgUnitListModel(ISingleSelectModel externalOrgUnitListModel)
    {
        this.externalOrgUnitListModel = externalOrgUnitListModel;
    }

    public ISelectModel getViewVisaListModel()
    {
        return viewVisaListModel;
    }

    public void setViewVisaListModel(ISelectModel viewVisaListModel)
    {
        this.viewVisaListModel = viewVisaListModel;
    }

    public DynamicListDataSource<ExternalOrgUnit> getExternalOrgUnit()
    {
        return externalOrgUnit;
    }

    public void setExternalOrgUnit(DynamicListDataSource<ExternalOrgUnit> externalOrgUnit)
    {
        this.externalOrgUnit = externalOrgUnit;
    }


    public Validator getVisaDateToValidator()
    {
        return new BaseValidator()
        {
            public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
            {
                if ((getForeignEntrant().getVisaDateFrom() != null) && (object != null))
                {
                    if (((Date) object).before(getForeignEntrant().getVisaDateFrom()))
                        throw new ValidatorException("Дата окончания периода должна быть после даты начала");
                }
            }
        };
    }

    public Validator getMigrationRegDateToValidator()
    {
        return new BaseValidator()
        {
            public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
            {
                if ((getForeignEntrant().getMigrationRegDateFrom() != null) && (object != null))
                {
                    if (((Date) object).before(getForeignEntrant().getMigrationRegDateFrom()))
                        throw new ValidatorException("Дата окончания периода должна быть после даты начала");
                }
            }
        };
    }

    public Validator getCertificateDateToValidator()
    {
        return new BaseValidator()
        {
            public void validate(IFormComponent field, ValidationMessages messages, Object object) throws ValidatorException
            {
                if ((getForeignEntrant().getCertificateDateFrom() != null) && (object != null))
                {
                    if (((Date) object).before(getForeignEntrant().getCertificateDateFrom()))
                        throw new ValidatorException("Дата окончания периода должна быть после даты начала");
                }
            }
        };
    }
}