/* $Id$ */
package ru.tandemservice.unipgups.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.IOnUpdateEmployeePostExt;

/**
 * @author Ekaterina Zvereva
 * @since 11.03.2016
 */
public class UpdateExt implements IOnUpdateEmployeePostExt
{
    @Override
    public void onUpdateExt(IUIAddon addon)
    {
        EmployeePostAddEditExtUI uiAddon = (EmployeePostAddEditExtUI) addon;
        DataAccessServices.dao().saveOrUpdate(uiAddon.getPgupsEmployeePostExt());
    }
}