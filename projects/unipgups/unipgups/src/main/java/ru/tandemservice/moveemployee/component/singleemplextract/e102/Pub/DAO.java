/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102.Pub;

import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubDAO;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public class DAO extends SingleEmployeeExtractPubDAO<ProlongationContractExtract, Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        StringBuilder builder = new StringBuilder("№ ").append(model.getExtract().getLabourContract().getNumber()).append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getExtract().getLabourContract().getDate()));
        model.setLabourContractTitle(builder.toString());
    }


}