/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;

/**
 * @author Nikolay Fedorovskih
 * @since 18.07.2013
 */
public interface IPgupsEcReportBuilder
{
    DatabaseFile getContent();
}