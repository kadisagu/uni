package ru.tandemservice.unipgups.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.ContractCollateralAgreement;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. О переводе сотрудника на иную долю ставки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PgupsTransferToDiffStaffRatesExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract";
    public static final String ENTITY_NAME = "pgupsTransferToDiffStaffRatesExtract";
    public static final int VERSION_HASH = 1022738598;
    private static IEntityMeta ENTITY_META;

    public static final String P_STAFF_RATE_BEFORE = "staffRateBefore";
    public static final String P_STAFF_RATE_AFTER = "staffRateAfter";
    public static final String L_SALARY_RAISING_COEFFICIENT = "salaryRaisingCoefficient";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String P_SALARY = "salary";
    public static final String L_CONTRACT_TYPE = "contractType";
    public static final String P_CONTRACT_NUMBER = "contractNumber";
    public static final String P_CONTRACT_DATE = "contractDate";
    public static final String P_CONTRACT_BEGIN_DATE = "contractBeginDate";
    public static final String P_CONTRACT_END_DATE = "contractEndDate";
    public static final String P_CONTRACT_ADD_AGREEMENT_NUMBER = "contractAddAgreementNumber";
    public static final String P_CONTRACT_ADD_AGREEMENT_DATE = "contractAddAgreementDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_CHARACTER_WORK = "characterWork";
    public static final String L_CREATE_EMPLOYEE_CONTRACT = "createEmployeeContract";
    public static final String L_CREATE_COLLATERAL_AGREEMENT = "createCollateralAgreement";
    public static final String P_FREELANCE = "freelance";

    private Double _staffRateBefore;     // Суммарная ставка должности до проведения
    private double _staffRateAfter;     // Суммарная ставка должности после проведения
    private SalaryRaisingCoefficient _salaryRaisingCoefficient;     // Повышающий коэффициент
    private EtksLevels _etksLevels;     // Разряд ЕТКС
    private double _salary;     // Сумма оплаты
    private LabourContractType _contractType;     // Тип трудового договора
    private String _contractNumber;     // Номер трудового договора
    private Date _contractDate;     // Дата трудового договора
    private Date _contractBeginDate;     // Дата начала
    private Date _contractEndDate;     // Дата окончания
    private String _contractAddAgreementNumber;     // Номер доп. соглашения
    private Date _contractAddAgreementDate;     // Дата доп. соглашения
    private Date _beginDate;     // Дата перевода
    private String _characterWork;     // Характер работы
    private EmployeeLabourContract _createEmployeeContract;     // Трудовой договор сотрудника созданный при проведении выписки
    private ContractCollateralAgreement _createCollateralAgreement;     // Доп. соглашение созданное при проведении выписки
    private boolean _freelance;     // Вне штата

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Суммарная ставка должности до проведения.
     */
    public Double getStaffRateBefore()
    {
        return _staffRateBefore;
    }

    /**
     * @param staffRateBefore Суммарная ставка должности до проведения.
     */
    public void setStaffRateBefore(Double staffRateBefore)
    {
        dirty(_staffRateBefore, staffRateBefore);
        _staffRateBefore = staffRateBefore;
    }

    /**
     * @return Суммарная ставка должности после проведения. Свойство не может быть null.
     */
    @NotNull
    public double getStaffRateAfter()
    {
        return _staffRateAfter;
    }

    /**
     * @param staffRateAfter Суммарная ставка должности после проведения. Свойство не может быть null.
     */
    public void setStaffRateAfter(double staffRateAfter)
    {
        dirty(_staffRateAfter, staffRateAfter);
        _staffRateAfter = staffRateAfter;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getSalaryRaisingCoefficient()
    {
        return _salaryRaisingCoefficient;
    }

    /**
     * @param salaryRaisingCoefficient Повышающий коэффициент.
     */
    public void setSalaryRaisingCoefficient(SalaryRaisingCoefficient salaryRaisingCoefficient)
    {
        dirty(_salaryRaisingCoefficient, salaryRaisingCoefficient);
        _salaryRaisingCoefficient = salaryRaisingCoefficient;
    }

    /**
     * @return Разряд ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Разряд ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getContractType()
    {
        return _contractType;
    }

    /**
     * @param contractType Тип трудового договора. Свойство не может быть null.
     */
    public void setContractType(LabourContractType contractType)
    {
        dirty(_contractType, contractType);
        _contractType = contractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractNumber()
    {
        return _contractNumber;
    }

    /**
     * @param contractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setContractNumber(String contractNumber)
    {
        dirty(_contractNumber, contractNumber);
        _contractNumber = contractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getContractDate()
    {
        return _contractDate;
    }

    /**
     * @param contractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setContractDate(Date contractDate)
    {
        dirty(_contractDate, contractDate);
        _contractDate = contractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getContractBeginDate()
    {
        return _contractBeginDate;
    }

    /**
     * @param contractBeginDate Дата начала. Свойство не может быть null.
     */
    public void setContractBeginDate(Date contractBeginDate)
    {
        dirty(_contractBeginDate, contractBeginDate);
        _contractBeginDate = contractBeginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getContractEndDate()
    {
        return _contractEndDate;
    }

    /**
     * @param contractEndDate Дата окончания.
     */
    public void setContractEndDate(Date contractEndDate)
    {
        dirty(_contractEndDate, contractEndDate);
        _contractEndDate = contractEndDate;
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getContractAddAgreementNumber()
    {
        return _contractAddAgreementNumber;
    }

    /**
     * @param contractAddAgreementNumber Номер доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementNumber(String contractAddAgreementNumber)
    {
        dirty(_contractAddAgreementNumber, contractAddAgreementNumber);
        _contractAddAgreementNumber = contractAddAgreementNumber;
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     */
    @NotNull
    public Date getContractAddAgreementDate()
    {
        return _contractAddAgreementDate;
    }

    /**
     * @param contractAddAgreementDate Дата доп. соглашения. Свойство не может быть null.
     */
    public void setContractAddAgreementDate(Date contractAddAgreementDate)
    {
        dirty(_contractAddAgreementDate, contractAddAgreementDate);
        _contractAddAgreementDate = contractAddAgreementDate;
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата перевода. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Характер работы.
     */
    @Length(max=255)
    public String getCharacterWork()
    {
        return _characterWork;
    }

    /**
     * @param characterWork Характер работы.
     */
    public void setCharacterWork(String characterWork)
    {
        dirty(_characterWork, characterWork);
        _characterWork = characterWork;
    }

    /**
     * @return Трудовой договор сотрудника созданный при проведении выписки.
     */
    public EmployeeLabourContract getCreateEmployeeContract()
    {
        return _createEmployeeContract;
    }

    /**
     * @param createEmployeeContract Трудовой договор сотрудника созданный при проведении выписки.
     */
    public void setCreateEmployeeContract(EmployeeLabourContract createEmployeeContract)
    {
        dirty(_createEmployeeContract, createEmployeeContract);
        _createEmployeeContract = createEmployeeContract;
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     */
    public ContractCollateralAgreement getCreateCollateralAgreement()
    {
        return _createCollateralAgreement;
    }

    /**
     * @param createCollateralAgreement Доп. соглашение созданное при проведении выписки.
     */
    public void setCreateCollateralAgreement(ContractCollateralAgreement createCollateralAgreement)
    {
        dirty(_createCollateralAgreement, createCollateralAgreement);
        _createCollateralAgreement = createCollateralAgreement;
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     */
    @NotNull
    public boolean isFreelance()
    {
        return _freelance;
    }

    /**
     * @param freelance Вне штата. Свойство не может быть null.
     */
    public void setFreelance(boolean freelance)
    {
        dirty(_freelance, freelance);
        _freelance = freelance;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof PgupsTransferToDiffStaffRatesExtractGen)
        {
            setStaffRateBefore(((PgupsTransferToDiffStaffRatesExtract)another).getStaffRateBefore());
            setStaffRateAfter(((PgupsTransferToDiffStaffRatesExtract)another).getStaffRateAfter());
            setSalaryRaisingCoefficient(((PgupsTransferToDiffStaffRatesExtract)another).getSalaryRaisingCoefficient());
            setEtksLevels(((PgupsTransferToDiffStaffRatesExtract)another).getEtksLevels());
            setSalary(((PgupsTransferToDiffStaffRatesExtract)another).getSalary());
            setContractType(((PgupsTransferToDiffStaffRatesExtract)another).getContractType());
            setContractNumber(((PgupsTransferToDiffStaffRatesExtract)another).getContractNumber());
            setContractDate(((PgupsTransferToDiffStaffRatesExtract)another).getContractDate());
            setContractBeginDate(((PgupsTransferToDiffStaffRatesExtract)another).getContractBeginDate());
            setContractEndDate(((PgupsTransferToDiffStaffRatesExtract)another).getContractEndDate());
            setContractAddAgreementNumber(((PgupsTransferToDiffStaffRatesExtract)another).getContractAddAgreementNumber());
            setContractAddAgreementDate(((PgupsTransferToDiffStaffRatesExtract)another).getContractAddAgreementDate());
            setBeginDate(((PgupsTransferToDiffStaffRatesExtract)another).getBeginDate());
            setCharacterWork(((PgupsTransferToDiffStaffRatesExtract)another).getCharacterWork());
            setCreateEmployeeContract(((PgupsTransferToDiffStaffRatesExtract)another).getCreateEmployeeContract());
            setCreateCollateralAgreement(((PgupsTransferToDiffStaffRatesExtract)another).getCreateCollateralAgreement());
            setFreelance(((PgupsTransferToDiffStaffRatesExtract)another).isFreelance());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PgupsTransferToDiffStaffRatesExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PgupsTransferToDiffStaffRatesExtract.class;
        }

        public T newInstance()
        {
            return (T) new PgupsTransferToDiffStaffRatesExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateBefore":
                    return obj.getStaffRateBefore();
                case "staffRateAfter":
                    return obj.getStaffRateAfter();
                case "salaryRaisingCoefficient":
                    return obj.getSalaryRaisingCoefficient();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "salary":
                    return obj.getSalary();
                case "contractType":
                    return obj.getContractType();
                case "contractNumber":
                    return obj.getContractNumber();
                case "contractDate":
                    return obj.getContractDate();
                case "contractBeginDate":
                    return obj.getContractBeginDate();
                case "contractEndDate":
                    return obj.getContractEndDate();
                case "contractAddAgreementNumber":
                    return obj.getContractAddAgreementNumber();
                case "contractAddAgreementDate":
                    return obj.getContractAddAgreementDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "characterWork":
                    return obj.getCharacterWork();
                case "createEmployeeContract":
                    return obj.getCreateEmployeeContract();
                case "createCollateralAgreement":
                    return obj.getCreateCollateralAgreement();
                case "freelance":
                    return obj.isFreelance();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "staffRateBefore":
                    obj.setStaffRateBefore((Double) value);
                    return;
                case "staffRateAfter":
                    obj.setStaffRateAfter((Double) value);
                    return;
                case "salaryRaisingCoefficient":
                    obj.setSalaryRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "contractType":
                    obj.setContractType((LabourContractType) value);
                    return;
                case "contractNumber":
                    obj.setContractNumber((String) value);
                    return;
                case "contractDate":
                    obj.setContractDate((Date) value);
                    return;
                case "contractBeginDate":
                    obj.setContractBeginDate((Date) value);
                    return;
                case "contractEndDate":
                    obj.setContractEndDate((Date) value);
                    return;
                case "contractAddAgreementNumber":
                    obj.setContractAddAgreementNumber((String) value);
                    return;
                case "contractAddAgreementDate":
                    obj.setContractAddAgreementDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "characterWork":
                    obj.setCharacterWork((String) value);
                    return;
                case "createEmployeeContract":
                    obj.setCreateEmployeeContract((EmployeeLabourContract) value);
                    return;
                case "createCollateralAgreement":
                    obj.setCreateCollateralAgreement((ContractCollateralAgreement) value);
                    return;
                case "freelance":
                    obj.setFreelance((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateBefore":
                        return true;
                case "staffRateAfter":
                        return true;
                case "salaryRaisingCoefficient":
                        return true;
                case "etksLevels":
                        return true;
                case "salary":
                        return true;
                case "contractType":
                        return true;
                case "contractNumber":
                        return true;
                case "contractDate":
                        return true;
                case "contractBeginDate":
                        return true;
                case "contractEndDate":
                        return true;
                case "contractAddAgreementNumber":
                        return true;
                case "contractAddAgreementDate":
                        return true;
                case "beginDate":
                        return true;
                case "characterWork":
                        return true;
                case "createEmployeeContract":
                        return true;
                case "createCollateralAgreement":
                        return true;
                case "freelance":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateBefore":
                    return true;
                case "staffRateAfter":
                    return true;
                case "salaryRaisingCoefficient":
                    return true;
                case "etksLevels":
                    return true;
                case "salary":
                    return true;
                case "contractType":
                    return true;
                case "contractNumber":
                    return true;
                case "contractDate":
                    return true;
                case "contractBeginDate":
                    return true;
                case "contractEndDate":
                    return true;
                case "contractAddAgreementNumber":
                    return true;
                case "contractAddAgreementDate":
                    return true;
                case "beginDate":
                    return true;
                case "characterWork":
                    return true;
                case "createEmployeeContract":
                    return true;
                case "createCollateralAgreement":
                    return true;
                case "freelance":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "staffRateBefore":
                    return Double.class;
                case "staffRateAfter":
                    return Double.class;
                case "salaryRaisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "salary":
                    return Double.class;
                case "contractType":
                    return LabourContractType.class;
                case "contractNumber":
                    return String.class;
                case "contractDate":
                    return Date.class;
                case "contractBeginDate":
                    return Date.class;
                case "contractEndDate":
                    return Date.class;
                case "contractAddAgreementNumber":
                    return String.class;
                case "contractAddAgreementDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "characterWork":
                    return String.class;
                case "createEmployeeContract":
                    return EmployeeLabourContract.class;
                case "createCollateralAgreement":
                    return ContractCollateralAgreement.class;
                case "freelance":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PgupsTransferToDiffStaffRatesExtract> _dslPath = new Path<PgupsTransferToDiffStaffRatesExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PgupsTransferToDiffStaffRatesExtract");
    }
            

    /**
     * @return Суммарная ставка должности до проведения.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getStaffRateBefore()
     */
    public static PropertyPath<Double> staffRateBefore()
    {
        return _dslPath.staffRateBefore();
    }

    /**
     * @return Суммарная ставка должности после проведения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getStaffRateAfter()
     */
    public static PropertyPath<Double> staffRateAfter()
    {
        return _dslPath.staffRateAfter();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getSalaryRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> salaryRaisingCoefficient()
    {
        return _dslPath.salaryRaisingCoefficient();
    }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractType()
     */
    public static LabourContractType.Path<LabourContractType> contractType()
    {
        return _dslPath.contractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractNumber()
     */
    public static PropertyPath<String> contractNumber()
    {
        return _dslPath.contractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractDate()
     */
    public static PropertyPath<Date> contractDate()
    {
        return _dslPath.contractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractBeginDate()
     */
    public static PropertyPath<Date> contractBeginDate()
    {
        return _dslPath.contractBeginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractEndDate()
     */
    public static PropertyPath<Date> contractEndDate()
    {
        return _dslPath.contractEndDate();
    }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractAddAgreementNumber()
     */
    public static PropertyPath<String> contractAddAgreementNumber()
    {
        return _dslPath.contractAddAgreementNumber();
    }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractAddAgreementDate()
     */
    public static PropertyPath<Date> contractAddAgreementDate()
    {
        return _dslPath.contractAddAgreementDate();
    }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Характер работы.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getCharacterWork()
     */
    public static PropertyPath<String> characterWork()
    {
        return _dslPath.characterWork();
    }

    /**
     * @return Трудовой договор сотрудника созданный при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getCreateEmployeeContract()
     */
    public static EmployeeLabourContract.Path<EmployeeLabourContract> createEmployeeContract()
    {
        return _dslPath.createEmployeeContract();
    }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getCreateCollateralAgreement()
     */
    public static ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
    {
        return _dslPath.createCollateralAgreement();
    }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#isFreelance()
     */
    public static PropertyPath<Boolean> freelance()
    {
        return _dslPath.freelance();
    }

    public static class Path<E extends PgupsTransferToDiffStaffRatesExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<Double> _staffRateBefore;
        private PropertyPath<Double> _staffRateAfter;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _salaryRaisingCoefficient;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private PropertyPath<Double> _salary;
        private LabourContractType.Path<LabourContractType> _contractType;
        private PropertyPath<String> _contractNumber;
        private PropertyPath<Date> _contractDate;
        private PropertyPath<Date> _contractBeginDate;
        private PropertyPath<Date> _contractEndDate;
        private PropertyPath<String> _contractAddAgreementNumber;
        private PropertyPath<Date> _contractAddAgreementDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<String> _characterWork;
        private EmployeeLabourContract.Path<EmployeeLabourContract> _createEmployeeContract;
        private ContractCollateralAgreement.Path<ContractCollateralAgreement> _createCollateralAgreement;
        private PropertyPath<Boolean> _freelance;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Суммарная ставка должности до проведения.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getStaffRateBefore()
     */
        public PropertyPath<Double> staffRateBefore()
        {
            if(_staffRateBefore == null )
                _staffRateBefore = new PropertyPath<Double>(PgupsTransferToDiffStaffRatesExtractGen.P_STAFF_RATE_BEFORE, this);
            return _staffRateBefore;
        }

    /**
     * @return Суммарная ставка должности после проведения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getStaffRateAfter()
     */
        public PropertyPath<Double> staffRateAfter()
        {
            if(_staffRateAfter == null )
                _staffRateAfter = new PropertyPath<Double>(PgupsTransferToDiffStaffRatesExtractGen.P_STAFF_RATE_AFTER, this);
            return _staffRateAfter;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getSalaryRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> salaryRaisingCoefficient()
        {
            if(_salaryRaisingCoefficient == null )
                _salaryRaisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_SALARY_RAISING_COEFFICIENT, this);
            return _salaryRaisingCoefficient;
        }

    /**
     * @return Разряд ЕТКС.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(PgupsTransferToDiffStaffRatesExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Тип трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractType()
     */
        public LabourContractType.Path<LabourContractType> contractType()
        {
            if(_contractType == null )
                _contractType = new LabourContractType.Path<LabourContractType>(L_CONTRACT_TYPE, this);
            return _contractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractNumber()
     */
        public PropertyPath<String> contractNumber()
        {
            if(_contractNumber == null )
                _contractNumber = new PropertyPath<String>(PgupsTransferToDiffStaffRatesExtractGen.P_CONTRACT_NUMBER, this);
            return _contractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractDate()
     */
        public PropertyPath<Date> contractDate()
        {
            if(_contractDate == null )
                _contractDate = new PropertyPath<Date>(PgupsTransferToDiffStaffRatesExtractGen.P_CONTRACT_DATE, this);
            return _contractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractBeginDate()
     */
        public PropertyPath<Date> contractBeginDate()
        {
            if(_contractBeginDate == null )
                _contractBeginDate = new PropertyPath<Date>(PgupsTransferToDiffStaffRatesExtractGen.P_CONTRACT_BEGIN_DATE, this);
            return _contractBeginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractEndDate()
     */
        public PropertyPath<Date> contractEndDate()
        {
            if(_contractEndDate == null )
                _contractEndDate = new PropertyPath<Date>(PgupsTransferToDiffStaffRatesExtractGen.P_CONTRACT_END_DATE, this);
            return _contractEndDate;
        }

    /**
     * @return Номер доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractAddAgreementNumber()
     */
        public PropertyPath<String> contractAddAgreementNumber()
        {
            if(_contractAddAgreementNumber == null )
                _contractAddAgreementNumber = new PropertyPath<String>(PgupsTransferToDiffStaffRatesExtractGen.P_CONTRACT_ADD_AGREEMENT_NUMBER, this);
            return _contractAddAgreementNumber;
        }

    /**
     * @return Дата доп. соглашения. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getContractAddAgreementDate()
     */
        public PropertyPath<Date> contractAddAgreementDate()
        {
            if(_contractAddAgreementDate == null )
                _contractAddAgreementDate = new PropertyPath<Date>(PgupsTransferToDiffStaffRatesExtractGen.P_CONTRACT_ADD_AGREEMENT_DATE, this);
            return _contractAddAgreementDate;
        }

    /**
     * @return Дата перевода. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(PgupsTransferToDiffStaffRatesExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Характер работы.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getCharacterWork()
     */
        public PropertyPath<String> characterWork()
        {
            if(_characterWork == null )
                _characterWork = new PropertyPath<String>(PgupsTransferToDiffStaffRatesExtractGen.P_CHARACTER_WORK, this);
            return _characterWork;
        }

    /**
     * @return Трудовой договор сотрудника созданный при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getCreateEmployeeContract()
     */
        public EmployeeLabourContract.Path<EmployeeLabourContract> createEmployeeContract()
        {
            if(_createEmployeeContract == null )
                _createEmployeeContract = new EmployeeLabourContract.Path<EmployeeLabourContract>(L_CREATE_EMPLOYEE_CONTRACT, this);
            return _createEmployeeContract;
        }

    /**
     * @return Доп. соглашение созданное при проведении выписки.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#getCreateCollateralAgreement()
     */
        public ContractCollateralAgreement.Path<ContractCollateralAgreement> createCollateralAgreement()
        {
            if(_createCollateralAgreement == null )
                _createCollateralAgreement = new ContractCollateralAgreement.Path<ContractCollateralAgreement>(L_CREATE_COLLATERAL_AGREEMENT, this);
            return _createCollateralAgreement;
        }

    /**
     * @return Вне штата. Свойство не может быть null.
     * @see ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract#isFreelance()
     */
        public PropertyPath<Boolean> freelance()
        {
            if(_freelance == null )
                _freelance = new PropertyPath<Boolean>(PgupsTransferToDiffStaffRatesExtractGen.P_FREELANCE, this);
            return _freelance;
        }

        public Class getEntityClass()
        {
            return PgupsTransferToDiffStaffRatesExtract.class;
        }

        public String getEntityName()
        {
            return "pgupsTransferToDiffStaffRatesExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
