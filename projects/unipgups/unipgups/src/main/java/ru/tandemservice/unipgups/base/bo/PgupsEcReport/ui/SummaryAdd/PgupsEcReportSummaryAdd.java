/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.*;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.block.entrantRequestData.EntrantRequestDataBlock;

import java.util.*;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
@Configuration
public class PgupsEcReportSummaryAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String ENROLLMENT_CAMPAIGN_STEP_DS = "enrollmentCampaignStepDS";
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_CONDITION_DS = "developConditionDS";
    public static final String DEVELOP_TECH_DS = "developTechDS";
    public static final String DEVELOP_PERIOD_DS = "developPeriodDS";

    public static final Long ON_PROCESS_RECEPTION = 1L;
    public static final Long ON_RESULT_TEST = 2L;
    public static final Long ON_RESULT_ENROLLMENT = 3L;

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_STEP_DS, enrollmentCampaignStepDSHandler()))
                .addDataSource(selectDS(STUDENT_CATEGORY_DS, studentCategoryDSHandler()))
                .addDataSource(selectDS(QUALIFICATION_DS, qualificationDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.P_SHORT_TITLE))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(DEVELOP_CONDITION_DS, developConditionDSHandler()))
                .addDataSource(selectDS(DEVELOP_TECH_DS, developTechDSHandler()))
                .addDataSource(selectDS(DEVELOP_PERIOD_DS, developPeriodDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> enrollmentCampaignStepDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(
                        new DataWrapper(ON_PROCESS_RECEPTION, "по ходу приема документов"),
                        new DataWrapper(ON_RESULT_TEST, "по результатам сдачи вступительных испытаний"),
                        new DataWrapper(ON_RESULT_ENROLLMENT, "по результатам зачисления")
                ));
    }

    @Bean
    public IDefaultComboDataSourceHandler studentCategoryDSHandler()
    {
        return EntrantRequestDataBlock.createStudentCategoryDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler qualificationDSHandler()
    {
        return EntrantRequestDataBlock.createQualificationDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler compensationTypeDSHandler()
    {
        return EntrantRequestDataBlock.createCompensationTypeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developFormDSHandler()
    {
        return EntrantRequestDataBlock.createDevelopFormDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developConditionDSHandler()
    {
        return EntrantRequestDataBlock.createDevelopConditionDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developTechDSHandler()
    {
        return EntrantRequestDataBlock.createDevelopTechDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler developPeriodDSHandler()
    {
        return EntrantRequestDataBlock.createDevelopPeriodDS(getName());
    }

}