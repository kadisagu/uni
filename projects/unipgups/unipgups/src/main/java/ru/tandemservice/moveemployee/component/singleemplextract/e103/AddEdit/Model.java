/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e103.AddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditModel;
import ru.tandemservice.uniemp.entity.employee.EmployeeLabourContract;
import ru.tandemservice.unipgups.entity.ChangeSalaryEmployeePostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 30.08.2015
 */
public class Model extends CommonSingleEmployeeExtractAddEditModel<ChangeSalaryEmployeePostExtract>
{
    private EmployeeLabourContract _labourContract;

    public EmployeeLabourContract getLabourContract()
    {
        return _labourContract;
    }

    public void setLabourContract(EmployeeLabourContract labourContract)
    {
        _labourContract = labourContract;
    }
}