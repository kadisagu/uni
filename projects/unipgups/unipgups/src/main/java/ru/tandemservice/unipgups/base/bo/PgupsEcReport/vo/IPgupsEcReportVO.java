/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo;

import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.IPgupsEcReport;

/**
 * @author Nikolay Fedorovskih
 * @since 18.07.2013
 */
public interface IPgupsEcReportVO
{
    void fillReport(IPgupsEcReport report);
}