/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.SummaryPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.PgupsEcReportManager;
import ru.tandemservice.unipgups.entity.report.PgupsSummaryEnrollmentReport;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
       })
public class PgupsEcReportSummaryPubUI extends UIPresenter
{
    private Long reportId;
    private PgupsSummaryEnrollmentReport report;

    @Override
    public void onComponentRefresh()
    {
        report = PgupsEcReportManager.instance().dao().getNotNull(getReportId());
    }

    public void onClickPrint()
    {
        _uiActivation.asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getReportId())
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickDelete()
    {
        PgupsEcReportManager.instance().dao().delete(getReport());
        deactivate();
    }

    public Long getReportId()
    {
        return reportId;
    }

    public void setReportId(Long reportId)
    {
        this.reportId = reportId;
    }

    public PgupsSummaryEnrollmentReport getReport()
    {
        return report;
    }
}