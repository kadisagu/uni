/* $Id$ */
package ru.tandemservice.unipgups.component.place.FloorPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.AddEdit.PgupsPlacesAddEdit;
import ru.tandemservice.unipgups.base.bo.PgupsPlaces.ui.Pub.PgupsPlacesPub;
import ru.tandemservice.unipgups.entity.UniPgupsPlace;
import ru.tandemservice.unipgups.entity.UniPlacesPlaceExt;
import ru.tandemservice.uniplaces.entity.place.UniplacesPlace;

/**
 * @author Ekaterina Zvereva
 * @since 20.03.2015
 */
public class Controller extends ru.tandemservice.uniplaces.component.place.FloorPub.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        preparePlacesDataSource(component);
    }

    @Override
    public void prepareDataSource(IBusinessComponent component)
    {
        super.prepareDataSource(component);
        DynamicListDataSource<UniplacesPlace> dataSource = getModel(component).getDataSource();
        if (dataSource.getColumn("place") == null)
        {
            DefaultPublisherLinkResolver resolverPlace = new DefaultPublisherLinkResolver()
            {
                @Override
                public Object getParameters(IEntity entity)
                {
                    UniPgupsPlace o = ((UniPlacesPlaceExt) ((ViewWrapper) entity).getViewProperty(DAO.PLACE_EXT)).getPgupsPlace();

                    if (o != null)
                        return new ParametersMap().add(PublisherActivator.PUBLISHER_ID_KEY, o.getId());
                    else
                        return null;
                }

                @Override
                public String getComponentName(IEntity entity)
                {
                    UniPgupsPlace o = ((UniPlacesPlaceExt) ((ViewWrapper) entity).getViewProperty(DAO.PLACE_EXT)).getPgupsPlace();
                    if (o != null)
                        return PgupsPlacesPub.class.getSimpleName();
                    else return null;
                }
            };

            HeadColumn headColumn = new HeadColumn("place", "Помещение");
            headColumn.setHeaderAlign("center");
            headColumn.addColumn(new PublisherLinkColumn("Название", DAO.PLACE_EXT + "." + UniPlacesPlaceExt.pgupsPlace().title()).setResolver(resolverPlace).setWidth("150px"));
            headColumn.addColumn(new SimpleColumn("Номер", DAO.PLACE_EXT + "." + UniPlacesPlaceExt.pgupsPlace().number()).setClickable(false).setWidth("90px"));
            dataSource.addColumn(headColumn, 3);
        }
    }

    public void onClickAddPgupsPlace(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(PgupsPlacesAddEdit.class.getSimpleName(), new ParametersMap().add("floorId", getModel(component).getFloor().getId())));
    }

    private void preparePlacesDataSource(IBusinessComponent component)
    {
        Model model = (Model)getModel(component);
        if (model.getDataSourcePlace() != null)
            return;

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            ((IDAO)getDao()).preparePgupsPlaceDataSource(model);
        });
        dataSource.addColumn(new SimpleColumn("Номер", UniPgupsPlace.number().s()));
        dataSource.addColumn(new SimpleColumn("Название", UniPgupsPlace.title()).setClickable(false));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditPgupsPlace").setPermissionKey("editPgupsPlace"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeletePgupsPlace", "Удалить помещение {0}?", UniplacesPlace.title().s()).setPermissionKey("deletePgupsPlace"));

        model.setDataSourcePlace(dataSource);

    }

    public void onClickEditPgupsPlace(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(PgupsPlacesAddEdit.class.getSimpleName(), new ParametersMap().add("placeId", (Long)component.getListenerParameter())));
    }

    public void onClickDeletePgupsPlace(IBusinessComponent component)
    {
        DataAccessServices.dao().delete((Long)component.getListenerParameter());
    }



}
