/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataAdd.PgupsEcReportEntrantsTADataAdd;

import java.util.List;

/**
 * @author nvankov
 * @since 7/19/13
 */
public class PgupsEcReportEntrantsTADataListUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
        if (getSettings().get("enrollmentCampaign") == null)
        {
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            getSettings().set("enrollmentCampaign", enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
        }
    }

    // Listeners

    public void onClickAddReport()
    {
        getActivationBuilder().asDesktopRoot(PgupsEcReportEntrantsTADataAdd.class).activate();
    }

    public void onPrintReport()
    {
        getActivationBuilder().asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "rtf")
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }

    public void onClickShow()
    {
        saveSettings();
    }

    public void onClickClear()
    {
        clearSettings();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(PgupsEcReportEntrantsTADataList.REPORT_LIST_DS))
        {
            dataSource.putAll(getSettings().getAsMap(true, "enrollmentCampaign"));
        }
    }
}
