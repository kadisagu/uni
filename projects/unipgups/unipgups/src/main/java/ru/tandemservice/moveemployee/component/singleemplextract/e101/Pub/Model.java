/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e101.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubModel;
import ru.tandemservice.unipgups.entity.CancelCombinationPostExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.08.2015
 */
public class Model extends SingleEmployeeExtractPubModel<CancelCombinationPostExtract>
{
    private String _staffRateStr;

    //Getters & Setters

    public String getStaffRateStr()
    {
        return _staffRateStr;
    }

    public void setStaffRateStr(String staffRateStr)
    {
        _staffRateStr = staffRateStr;
    }
}