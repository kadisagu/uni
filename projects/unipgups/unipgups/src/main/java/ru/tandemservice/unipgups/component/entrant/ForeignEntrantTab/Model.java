/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.ForeignEntrantTab;

import org.tandemframework.core.component.State;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unipgups.entity.entrant.PgupsForeignEntrant;

/**
 * @author Alexey Lopatin
 * @since 03.08.2013
 */
@State({@org.tandemframework.core.component.Bind(key="publisherId", binding="entrant.id")})
public class Model
{
    private Entrant entrant = new Entrant();
    private PgupsForeignEntrant foreignEntrant;

    public boolean isAccessible()
    {
      return !getEntrant().isArchival();
    }

    public Entrant getEntrant()
    {
        return entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        this.entrant = entrant;
    }

    public PgupsForeignEntrant getForeignEntrant()
    {
        return foreignEntrant;
    }

    public void setForeignEntrant(PgupsForeignEntrant foreignEntrant)
    {
        this.foreignEntrant = foreignEntrant;
    }
}
