/* $Id$ */
package ru.tandemservice.unipgups.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author Ekaterina Zvereva
 * @since 19.01.2016
 */
public class MS_unipgups_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                        new ScriptDependency("ru.tandemservice.nsiclient", "2.9.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        PreparedStatement stmt = tool.prepareStatement("select empStaffRate.id empStaffId, s.staffRelId  from employeepoststaffrateitem_t empStaffRate\n" +
                                                         "join (select staff.id staffRelId, ab.entity_id entity, staff.financingsource_id fin, staff.financingsourceitem_id finitem" +
                                                         " from stffrttpgpstrnsfdffstffrtext_t staff\n" +
                                                         "join pgpstrnsfrtdffstffrtsextrct_t ext on staff.extract_id = ext.id\n" +
                                                         "join abstractEmployeeExtract_t ab on ab.id = ext.id\n" +
                                                         "join extractStates_t state on ab.state_id=state.id\n"+
                                                         "where employeepoststaffrateitem_id is null and state.code_p='6') s\n" +
                                                         "on empStaffRate.employeePost_id = s.entity and empStaffRate.financingSource_id=s.fin and (empStaffRate.financingSourceItem_id=s.finitem \n" +
                                                               " or (empStaffRate.financingSourceItem_id is null and s.finitem is null))");
        stmt.execute();
        ResultSet src = stmt.getResultSet();

        while (src.next())
        {
            Long staffRelId = src.getLong("staffRelId");
            Long empStaffId = src.getLong("empStaffId");
            tool.executeUpdate("update stffrttpgpstrnsfdffstffrtext_t set employeepoststaffrateitem_id = ? where id=?", empStaffId, staffRelId);
        }

    }
}