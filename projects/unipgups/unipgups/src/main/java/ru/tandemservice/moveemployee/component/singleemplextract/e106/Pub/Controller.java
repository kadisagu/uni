/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e106.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.SingleEmployeeExtractPubController;
import ru.tandemservice.unipgups.entity.PgupsTransferToDiffStaffRatesExtract;

/**
 * @author Ekaterina Zvereva
 * @since 07.09.2015
 */
public class Controller extends SingleEmployeeExtractPubController<PgupsTransferToDiffStaffRatesExtract, IDAO, Model>
{
}