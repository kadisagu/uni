/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e105.AddEdit;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.ICommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.unipgups.entity.PgupsEmployeeMissionExtract;

/**
 * @author Ekaterina Zvereva
 * @since 03.09.2015
 */
public interface IDAO extends ICommonSingleEmployeeExtractAddEditDAO<PgupsEmployeeMissionExtract, Model>
{
}