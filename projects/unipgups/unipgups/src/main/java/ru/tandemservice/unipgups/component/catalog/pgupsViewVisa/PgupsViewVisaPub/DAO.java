/* $Id$ */
package ru.tandemservice.unipgups.component.catalog.pgupsViewVisa.PgupsViewVisaPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.unipgups.entity.catalog.PgupsViewVisa;

/**
 * @author Lopatin
 * @since 06.08.2013
 */
public class DAO extends DefaultCatalogPubDAO<PgupsViewVisa, Model> implements IDAO
{
}