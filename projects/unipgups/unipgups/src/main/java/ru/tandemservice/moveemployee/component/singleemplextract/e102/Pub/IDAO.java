/* $Id$ */
package ru.tandemservice.moveemployee.component.singleemplextract.e102.Pub;

import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.SingleEmployeeExtractPub.ISingleEmployeeExtractPubDAO;
import ru.tandemservice.unipgups.entity.ProlongationContractExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.08.2015
 */
public interface IDAO extends ISingleEmployeeExtractPubDAO<ProlongationContractExtract, Model>
{
}