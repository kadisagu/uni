/* $Id$ */
package ru.tandemservice.unipgups.component.entrant.ForeignEntrantEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unipgups.entity.catalog.PgupsViewVisa;
import ru.tandemservice.unipgups.entity.entrant.PgupsForeignEntrant;

/**
 * @author Alexey Lopatin
 * @since 03.08.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    public void prepare(Model model)
    {
        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        PgupsForeignEntrant foreignEntrant = get(PgupsForeignEntrant.class, PgupsForeignEntrant.entrant().s(), model.getEntrant());

        if (foreignEntrant != null)
            model.setForeignEntrant(foreignEntrant);

        model.setExternalOrgUnitListModel(new LazySimpleSelectModel<>(ExternalOrgUnit.class));
        model.setViewVisaListModel(new LazySimpleSelectModel<>(PgupsViewVisa.class));
    }

    public void update(Model model)
    {
        model.getForeignEntrant().setEntrant(model.getEntrant());
        getSession().saveOrUpdate(model.getForeignEntrant());
    }
}
