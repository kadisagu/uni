/* $Id$ */
package ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataAdd;

import org.joda.time.LocalDateTime;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.PgupsEcReportManager;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.logic.EnrollmentDirectionsComboDSHandler;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.ui.EntrantsTADataView.PgupsEcReportEntrantsTADataView;
import ru.tandemservice.unipgups.base.bo.PgupsEcReport.vo.EntrantsTADataReportVO;
import ru.tandemservice.unipgups.entity.report.PgupsEntrantsTADataReport;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 7/19/13
 */
public class PgupsEcReportEntrantsTADataAddUI extends UIPresenter
{
    private EntrantsTADataReportVO _reportVO;

    public EntrantsTADataReportVO getReportVO()
    {
        return _reportVO;
    }

    public void setReportVO(EntrantsTADataReportVO reportVO)
    {
        _reportVO = reportVO;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _reportVO)
        {
            _reportVO = new EntrantsTADataReportVO();
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            _reportVO.setEnrollmentCampaign(enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
            _reportVO.setFrom(new LocalDateTime().withDayOfYear(1).toDateTime().toDate());
            _reportVO.setTo(new Date());

        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("reportVO", _reportVO);
        if(PgupsEcReportEntrantsTADataAdd.TERRITORIAL_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
        }
        if(PgupsEcReportEntrantsTADataAdd.FORMATIVE_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if(PgupsEcReportEntrantsTADataAdd.EDUCATION_LEVELS_HIGH_SCHOOL_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.EDUCATION_LEVELS_HIGH_SCHOOL);
        }
        if(PgupsEcReportEntrantsTADataAdd.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_FORM);
        }
        if(PgupsEcReportEntrantsTADataAdd.DEVELOP_CONDITION_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_CONDITION);
        }
        if(PgupsEcReportEntrantsTADataAdd.DEVELOP_TECH_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_TECH);
        }
        if(PgupsEcReportEntrantsTADataAdd.DEVELOP_PERIOD_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_PERIOD);
        }
    }

    public boolean getNotAllEnrollmentDirections()
    {
        return !_reportVO.isAllEnrollmentDirections();
    }

    public void onClickApply()
    {
        if(_reportVO.getFrom().after(_reportVO.getTo()))
        {
            _uiSupport.error("Дата \"Заявления с\" не может быть позже даты \"Заявления по\"", "dateFrom", "dateTo");
            return;
        }

        if (null != _reportVO.getFormativeOrgUnit() && null != _reportVO.getTerritorialOrgUnit() && null != _reportVO.getEducationLevelsHighSchool() && null != _reportVO.getDevelopForm() && null != _reportVO.getDevelopCondition() && null != _reportVO.getDevelopTech() && null != _reportVO.getDevelopPeriod())
            _reportVO.setEnrollmentDirection(PgupsEcReportManager.instance().entrantsTADataReportDAO().getSelectedEnrollmentDirection(_reportVO));


        PgupsEntrantsTADataReport report = PgupsEcReportManager.instance().entrantsTADataReportDAO().createReport(_reportVO);
        deactivate();
        getActivationBuilder().asDesktopRoot(PgupsEcReportEntrantsTADataView.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }
}
