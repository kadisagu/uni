package ru.tandemservice.unipgups.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unipgups.entity.report.gen.*;

/**
 * Отчет о данных абитуриентов целевого приема
 */
public class PgupsEntrantsTADataReport extends PgupsEntrantsTADataReportGen implements IStorableReport
{
    @EntityDSLSupport
    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}