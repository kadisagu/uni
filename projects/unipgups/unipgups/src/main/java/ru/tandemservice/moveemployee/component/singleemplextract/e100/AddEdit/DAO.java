/* $Id$ */

package ru.tandemservice.moveemployee.component.singleemplextract.e100.AddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.common.component.selection.MQListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitAutocompleteModel;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.moveemployee.component.singleemplextract.abstractextract.CommonSingleEmployeeExtractAddEdit.CommonSingleEmployeeExtractAddEditDAO;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtAllocItemRelation;
import ru.tandemservice.moveemployee.entity.CombinationPostStaffRateExtractRelation;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniemp.dao.UniempDaoFacade;
import ru.tandemservice.uniemp.entity.catalog.CombinationPostType;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniemp.entity.catalog.FinancingSourceItem;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.catalog.codes.CombinationPostTypeCodes;
import ru.tandemservice.uniemp.entity.employee.*;
import ru.tandemservice.unipgups.entity.CombinationPostExtract;

import java.math.BigDecimal;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author SingleEmployeeExtractComponentGenerator
 * @since 23.09.2009
 */
public class DAO extends CommonSingleEmployeeExtractAddEditDAO<CombinationPostExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        if (model.isEditForm())
            model.setEmployeePost(model.getExtract().getEntity());

        if (model.isEditForm())
        {
             model.setCombinationPostStaffRateItemList(getList(CombinationPostStaffRateExtractRelation.class, CombinationPostStaffRateExtractRelation.abstractEmployeeExtract(), model.getExtract()));
        }
        else
        {
            model.getExtract().setCombinationPostType(getCatalogItem(CombinationPostType.class, CombinationPostTypeCodes.COMBINATION));
        }

        EmployeeLabourContract contract = UniempDaoFacade.getUniempDAO().getEmployeePostLabourContract(model.getEmployeePost());
        model.setDisabledContractFields(contract != null);
        if (contract != null)
        {
            model.getExtract().setContractType(contract.getType());
            model.getExtract().setContractNumber(contract.getNumber());
            model.getExtract().setContractDate(contract.getDate());
            model.getExtract().setContractBeginDate(contract.getBeginDate());
            model.getExtract().setContractEndDate(contract.getEndDate());
        }

        model.setContractTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(LabourContractType.class, "lct");
                builder.where(like(property("lct", LabourContractType.title()), value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(eq(property("lct", LabourContractType.id()), value((Long) o)));
                builder.order(property("lct", LabourContractType.title()));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setOrgUnitModel(new OrgUnitAutocompleteModel());

        //новый вариант модели
        model.setPostModel(new BaseSingleSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getExtract().getOrgUnit() == null)
                {
                    return ListResult.getEmpty();
                }

                if (model.getExtract().isFreelance())
                {
                    return new ListResult<>(UniempDaoFacade.getUniempDAO().getPostRelationList(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), filter, 50));
                }
                else
                {
                    int maxCount = UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnitCount(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL() != null ? model.getExtract().getPostBoundedWithQGandQL() : null, filter, null, null);
                    return new ListResult<>(UniempDaoFacade.getStaffListDAO().getFreePostsListForOrgUnit(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL() != null ? model.getExtract().getPostBoundedWithQGandQL() : null, filter, null, null, 50), maxCount);
                }
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                Object object = get((Long) primaryKey);

                if (object instanceof OrgUnitTypePostRelation) {
                    OrgUnitTypePostRelation orgUnitTypePostRelation = (OrgUnitTypePostRelation) object;
                    return orgUnitTypePostRelation.getPostBoundedWithQGandQL();
                }

                if (object instanceof PostBoundedWithQGandQL) {
                    return object;
                }

                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if (value instanceof OrgUnitTypePostRelation)
                    return ((OrgUnitTypePostRelation) value).getPostBoundedWithQGandQL().getFullTitleWithSalary();

                if (value instanceof PostBoundedWithQGandQL)
                    return ((PostBoundedWithQGandQL) value).getFullTitleWithSalary();

                return "";
            }
        });

        model.setCombinationPostTypeModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(CombinationPostType.class, "t").column(property("t"));
                builder.where(DQLExpressions.like(property("t", CombinationPostType.title()), value(CoreStringUtils.escapeLike(filter))));
                if (o != null)
                    builder.where(eq(property("t", CombinationPostType.id()), commonValue(o)));
                builder.order(property("t", CombinationPostType.title()));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setMissingEmployeePostModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if  (null == model.getExtract().getPostBoundedWithQGandQL() || null == model.getExtract().getOrgUnit())
                    return ListResult.getEmpty();

                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "ep");
                builder.add(MQExpression.eq("ep", EmployeePost.postRelation().postBoundedWithQGandQL().s(), model.getExtract().getPostBoundedWithQGandQL()));
                builder.add(MQExpression.eq("ep", EmployeePost.L_ORG_UNIT, model.getExtract().getOrgUnit()));
                if (model.getEmployeePost() != null)
                    builder.add(MQExpression.notEq("ep", EmployeePost.P_ID, model.getEmployeePost().getId()));
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_LAST_NAME);
                builder.addOrder("ep", EmployeePost.L_EMPLOYEE + "." + Employee.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FIRST_NAME);
                return new ListResult<>(builder.<EmployeePost>getResultList(getSession()));
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EmployeePost post = (EmployeePost) value;
                return post.getExtendedPostTitle();
            }
        });

        model.setEtksLevelModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EtksLevels.class, "l");
                builder.where(like(property("l", EtksLevels.title()), value(CoreStringUtils.escapeLike(filter))));
                builder.order(property("l", EtksLevels.title()));
                if (o != null)
                    builder.where(eq(property("l", EtksLevels.id()), value((Long) o)));

                return new DQLListResultBuilder(builder);
            }
        });

        model.setFinancingSourceModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                if (model.getExtract().getOrgUnit() == null || model.getExtract().getPostBoundedWithQGandQL() == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList activeStaffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StaffListItem.class, "sli")
                            .column(property("sli", StaffListItem.financingSource()))
                            .distinct()
                            .where(eq(property("sli", StaffListItem.staffList()), value(activeStaffList)))
                            .where(eq(property("sli", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL()), value(model.getExtract().getPostBoundedWithQGandQL())))
                            .where(like(property("sli", StaffListItem.financingSource().title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(eq(property("sli", StaffListItem.financingSource().id()), value((Long) o)));

                    return new DQLListResultBuilder(builder);
                }
                else
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FinancingSource.class, "fs")
                            .where(like(property("fs", FinancingSource.title()), value(CoreStringUtils.escapeLike(filter))));
                    if (o != null)
                        builder.where(eq(property("fs", FinancingSource.id()), value((Long) o)));

                    return new DQLListResultBuilder(builder);
                }
            }
        });

        model.setFinancingSourceItemModel(new CommonSingleSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);

                if (model.getExtract().getOrgUnit() == null || model.getExtract().getPostBoundedWithQGandQL() == null || financingSource == null)
                    return new SimpleListResultBuilder<>(new ArrayList<>());


                if (model.isThereAnyActiveStaffList())
                {
                    StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());

                    MQBuilder builder = new MQBuilder(StaffListItem.ENTITY_CLASS, "sl", new String[] {StaffListItem.L_FINANCING_SOURCE_ITEM});
                    builder.add(MQExpression.eq("sl", StaffListItem.staffList().s(), staffList));
                    builder.add(MQExpression.eq("sl", StaffListItem.orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL().s(), model.getExtract().getPostBoundedWithQGandQL()));
                    builder.add(MQExpression.eq("sl", StaffListItem.financingSource().s(), financingSource));
                    builder.add(MQExpression.isNotNull("sl", StaffListItem.financingSourceItem().s()));
                    builder.add(MQExpression.like("sl", StaffListItem.financingSourceItem().title().s(), CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("sl", StaffListItem.financingSourceItem().id().s(), o));
                    builder.setNeedDistinct(true);

                    return new MQListResultBuilder(builder);
                }
                else
                {
                    MQBuilder builder = new MQBuilder(FinancingSourceItem.ENTITY_CLASS, "fsi");
                    builder.add(MQExpression.eq("fsi", FinancingSourceItem.financingSource().id().s(), financingSource.getId()));
                    builder.add(MQExpression.like("fsi", FinancingSourceItem.P_TITLE, CoreStringUtils.escapeLike(filter)));
                    if (o != null)
                        builder.add(MQExpression.eq("fsi", FinancingSourceItem.id().s(), o));

                    return new MQListResultBuilder(builder);
                }
            }
        });

        model.setEmployeeHRModel(new BaseMultiSelectModel()
        {
            @Override
            public List getValues(Set primaryKeys)
            {
                List<IdentifiableWrapper> objects = findValues("").getObjects();
                List<IdentifiableWrapper> resultList = new ArrayList<>();

                for (IdentifiableWrapper wrapper : objects)
                {
                    if (primaryKeys.contains(wrapper.getId()))
                        resultList.add(wrapper);
                }

                return resultList;
            }

            @Override
            @SuppressWarnings("unchecked")
            public ListResult<IdentifiableWrapper> findValues(String filter)
            {
                String newStaffRateStr = "<Новая ставка> - ";
                long id = model.getStaffRateDataSource().getCurrentEntity().getId();

                final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
                final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

                final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
                final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

                FinancingSource financingSource = finSrcMap.get(id);
                FinancingSourceItem financingSourceItem = finSrcItmMap.get(id);

                if (financingSource == null || model.getExtract().getOrgUnit() == null || model.getExtract().getPostBoundedWithQGandQL() == null)
                    return ListResult.getEmpty();

                Set<IdentifiableWrapper> resultList = new HashSet<>();
                List<StaffListAllocationItem> staffListAllocList = new ArrayList<>();

                StaffList staffList = UniempDaoFacade.getStaffListDAO().getActiveStaffList(model.getExtract().getOrgUnit());

                DQLSelectBuilder allocBuilder = new DQLSelectBuilder().fromEntity(StaffListAllocationItem.class, "a")
                    .where(eq(property("a", StaffListAllocationItem.staffListItem().staffList()), value(staffList)))
                    .where(eq(property("a", StaffListAllocationItem.staffListItem().orgUnitPostRelation().orgUnitTypePostRelation().postBoundedWithQGandQL()), value(model.getExtract().getPostBoundedWithQGandQL())))
                    .where(eq(property("a", StaffListAllocationItem.financingSource()), value(financingSource)))
                    .where(eq(property("a", StaffListAllocationItem.financingSourceItem()), value(financingSourceItem)))
                    .where(eq(property("a", StaffListAllocationItem.employeePost().postStatus().active()), value(false)));

                staffListAllocList.addAll(getList(allocBuilder));

                if (UniempDaoFacade.getStaffListDAO().isPossibleAddNewStaffRate(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), financingSource, financingSourceItem))
                {
                    Double summStaffRate = 0.0d;
                    for (StaffListAllocationItem item : staffListAllocList)
                        summStaffRate += item.getStaffRate();

                    StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), financingSource, financingSourceItem);
                    Double diff = 0.0d;
                    if (staffListItem != null)
                        diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate() - summStaffRate;
                    if (diff > 0 && newStaffRateStr.contains(filter))
                        resultList.add(new IdentifiableWrapper(0L, newStaffRateStr + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(diff)));
                }

                if (model.isEditForm())
                {
                    MQBuilder subBuilder = new MQBuilder(CombinationPostStaffRateExtractRelation.ENTITY_CLASS, "srr");
                    subBuilder.add(MQExpression.eq("srr", CombinationPostStaffRateExtractRelation.abstractEmployeeExtract().s(), model.getExtract()));
                    subBuilder.add(MQExpression.eq("srr", CombinationPostStaffRateExtractRelation.financingSource().s(), financingSource));
                    subBuilder.add(MQExpression.eq("srr", CombinationPostStaffRateExtractRelation.financingSourceItem().s(), financingSourceItem));

                    MQBuilder builder = new MQBuilder(CombinationPostStaffRateExtAllocItemRelation.ENTITY_CLASS, "srar");
                    builder.add(MQExpression.in("srar", CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation().s(), subBuilder));

                    for (CombinationPostStaffRateExtAllocItemRelation relation : builder.<CombinationPostStaffRateExtAllocItemRelation>getResultList(getSession()))
                        if (!relation.isHasNewAllocItem() && relation.getChoseStaffListAllocationItem() != null)
                            if (relation.getChoseStaffListAllocationItem().getStaffListItem().getStaffList().getOrgUnit().equals(model.getExtract().getOrgUnit()))
                                if (relation.getChoseStaffListAllocationItem().getStaffListItem().getOrgUnitPostRelation().getOrgUnitTypePostRelation().getPostBoundedWithQGandQL().equals(model.getExtract().getPostBoundedWithQGandQL()))
                                    resultList.add(new IdentifiableWrapper(relation.getChoseStaffListAllocationItem().getId(), relation.getChoseStaffListAllocationItem().getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(relation.getChoseStaffListAllocationItem().getStaffRate()) + " - " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostType().getShortTitle() + " " + relation.getChoseStaffListAllocationItem().getEmployeePost().getPostStatus().getShortTitle()));
                }

                for (StaffListAllocationItem item : staffListAllocList)
                {
                    if (item.getEmployeePost().getPerson().getFullFio().contains(filter))
                        resultList.add(new IdentifiableWrapper(item.getId(), item.getEmployeePost().getPerson().getFio() + ", ставка - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(item.getStaffRate()) + " - " + item.getEmployeePost().getPostType().getShortTitle() + " " + item.getEmployeePost().getPostStatus().getShortTitle()));
                }

                return new ListResult<>(resultList);
            }
        });
    }

    @Override
    @SuppressWarnings("unchecked")
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        actualizeStaffRateItemList(model);

        final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
        final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());

        //если была доступна колонка Кадровой расстановки, достаем из враперов объекты Штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
        //Мапа, указывающая на то выбрана ли строчка <Новая ставка> в мультиселекте
        Map<Long, Boolean> hasNewStaffRateMap = new HashMap<>();
        if (model.isThereAnyActiveStaffList())
        {
            for (IEntity entity : model.getCombinationPostStaffRateItemList())
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entity.getId()))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().get(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    }

                }

                allocItemsMap.put(entity.getId(), allocationItemList);

                hasNewStaffRateMap.put(entity.getId(), empHRMap.get(entity.getId()).contains(new IdentifiableWrapper(0L, "")));
            }
        }

        if (model.getCombinationPostStaffRateItemList().isEmpty())
            errors.add("Необходимо указать ставку для должности по совмещению.");

        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
            for (CombinationPostStaffRateExtractRelation item2nd : model.getCombinationPostStaffRateItemList())
                if (!item.equals(item2nd))
                    if (item.getFinancingSource().equals(item2nd.getFinancingSource()))
                        if ((item.getFinancingSourceItem() == null && item2nd.getFinancingSourceItem() == null) ||
                                ((item.getFinancingSourceItem() != null && item2nd.getFinancingSourceItem() != null) && item.getFinancingSourceItem().equals(item2nd.getFinancingSourceItem())))
                            errors.add("Набор полей «Источник финансирования», «Источник финансирования (детально)» должен быть уникальным в рамках должности по совмещению.",
                                       "finSrc_id_" + item.getId(), "finSrcItm_id_" + item.getId());

        if (model.isThereAnyActiveStaffList() && !errors.hasErrors())
            for (CombinationPostStaffRateExtractRelation staffRateItem : model.getCombinationPostStaffRateItemList())
            {
                StaffListItem staffListItem = UniempDaoFacade.getStaffListDAO().getStaffListItem(model.getExtract().getOrgUnit(), model.getExtract().getPostBoundedWithQGandQL(), staffRateItem.getFinancingSource(), staffRateItem.getFinancingSourceItem());
                Double diff = 0.0d;
                if (staffListItem != null)
                    diff = staffListItem.getStaffRate() - staffListItem.getOccStaffRate();
                BigDecimal x = new BigDecimal(diff);
                x = x.setScale(3, BigDecimal.ROUND_HALF_UP);
                diff = x.doubleValue();

                for (StaffListAllocationItem allocationItem : UniempDaoFacade.getStaffListDAO().getStaffListItemAllocations(staffListItem))
                    if (allocationItem.getEmployeePost() != null && !allocationItem.getEmployeePost().getPostStatus().isActive() &&
                            !allocItemsMap.get(staffRateItem.getId()).contains(allocationItem))
                        diff -= allocationItem.getStaffRate();

                Double sumStaffRateAllocItems = 0.0d;
                for (StaffListAllocationItem allocItem : allocItemsMap.get(staffRateItem.getId()))
                {
                    sumStaffRateAllocItems += allocItem.getStaffRate();
                }

                if ((staffRateItem.getStaffRate() > sumStaffRateAllocItems && !hasNewStaffRateMap.get(staffRateItem.getId())) ||
                        (staffRateItem.getStaffRate() > diff + sumStaffRateAllocItems && hasNewStaffRateMap.get(staffRateItem.getId())))
                    errors.add("Размер ставки " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() +
                                       " превышает суммарную ставку, выбранную в поле «Кадровая расстановка».",
                               "staffRate_id_" + staffRateItem.getId());

                if (staffRateItem.getStaffRate() <= sumStaffRateAllocItems && hasNewStaffRateMap.get(staffRateItem.getId()))
                    errors.add("Необходимо точнее указать занимаемые ставки в поле «Кадровая расстановка» для "
                                       + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(staffRateItem.getStaffRate()) + " " + staffRateItem.getFinancingSource().getTitle() + ".",
                               "employeeHR_Id_" + staffRateItem.getId());
            }

        Double combinationStaffRate = 0d;
        List<String> staffRateFieldList = new ArrayList<>();
        for (CombinationPostStaffRateExtractRelation staffRateItem : model.getCombinationPostStaffRateItemList())
        {
            combinationStaffRate += staffRateItem.getStaffRate();

            staffRateFieldList.add("staffRate_id_" + staffRateItem.getId());
        }

        Double employeeStaffRate = 0d;
        for (EmployeePostStaffRateItem staffRateItem : UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost()))
            employeeStaffRate += staffRateItem.getStaffRate();

        if (combinationStaffRate > employeeStaffRate)
            errors.add("Cтавка должности по совмещению сотрудника превышает суммарный размер ставки основной должности сотрудника.", staffRateFieldList.toArray(new String[staffRateFieldList.size()]));

        if (model.getExtract().getEndDate() != null)
            if (model.getExtract().getBeginDate().getTime() > model.getExtract().getEndDate().getTime())
                errors.add("Дата начала должна быть меньше даты окончания.", "beginDate", "endDate");

        if ((CommonBaseDateUtil.isBefore(model.getExtract().getBeginDate(), model.getExtract().getContractBeginDate())) ||
                (model.getExtract().getContractEndDate() != null && CommonBaseDateUtil.isAfter(model.getExtract().getBeginDate(), model.getExtract().getContractEndDate())))
            errors.add("Период увеличения объема работ должен попадать в период начала - окончания работы сотрудника.", "beginDate", "endDate");
        else if (model.getExtract().getEndDate() != null)
            if (CommonBaseDateUtil.isBefore(model.getExtract().getEndDate(), model.getExtract().getContractBeginDate()) ||
                    (model.getExtract().getContractEndDate() != null && CommonBaseDateUtil.isAfter(model.getExtract().getEndDate(), model.getExtract().getContractEndDate())))
                errors.add("Период увеличения объема работ должен попадать в период начала - окончания работы сотрудника.", "beginDate", "endDate");

    }

    @Override
    @SuppressWarnings("unchecked")
    public void update(Model model)
    {
        super.update(model);

        if (model.getExtract().getEntity() == null)
            model.getExtract().setEntity(model.getEmployeePost());

        actualizeStaffRateItemList(model);

        //если редактируем выписку, то перед тем как создать новые релейшены ставок - удаляем старые
        if (model.isEditForm())
        {
            List<CombinationPostStaffRateExtractRelation> relationList = getList(CombinationPostStaffRateExtractRelation.class, CombinationPostStaffRateExtractRelation.abstractEmployeeExtract(), model.getExtract());

            for (CombinationPostStaffRateExtAllocItemRelation relation : getList(CombinationPostStaffRateExtAllocItemRelation.class, CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation(), relationList))
                delete(relation);

            for (CombinationPostStaffRateExtractRelation relation : relationList)
                delete(relation);
        }

        //Мапа, id энтини сечлиста на выбранные в мультиселекте объекты штатной расстановки
        final Map<Long, List<StaffListAllocationItem>> allocItemsMap = new HashMap<>();
        if (model.isThereAnyActiveStaffList())
        {
            //достаем элементы штатной расстановки, которые выбраны в мультиселектах
            final IValueMapHolder empHRHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("employeeHR");
            final Map<Long, List<IdentifiableWrapper>> empHRMap = (null == empHRHolder ? Collections.emptyMap() : empHRHolder.getValueMap());

            //достаем из враперов объекты Штатной расстановки
            for (Long entityId : ids(model.getCombinationPostStaffRateItemList()))
            {
                List<StaffListAllocationItem> allocationItemList = new ArrayList<>();
                for (IdentifiableWrapper wrapper : empHRMap.get(entityId))
                {
                    if (wrapper.getId() != 0L)
                    {
                        StaffListAllocationItem item = IUniBaseDao.instance.get().getNotNull(StaffListAllocationItem.class, wrapper.getId());
                        allocationItemList.add(item);
                    }
                    else
                    {
                        StaffListAllocationItem allocationItem = new StaffListAllocationItem();
                        allocationItem.setId(0L);
                        allocationItemList.add(allocationItem);
                    }
                }
                allocItemsMap.put(entityId, allocationItemList);
            }
        }

        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
        {
            Long id = item.getId();//т.к. после save id поменяется, мы его сохраняем что бы достать из мапы соответствующий ставке набор allocItem

            save(item);

            if (model.isThereAnyActiveStaffList())
                for (StaffListAllocationItem allocationItem : allocItemsMap.get(id))
                {
                    CombinationPostStaffRateExtAllocItemRelation relation = new CombinationPostStaffRateExtAllocItemRelation();

                    if (allocationItem.getId() != 0L)
                    {
                        relation.setChoseStaffListAllocationItem(allocationItem);
                        relation.setHasNewAllocItem(false);
                        relation.setEmployeePostHistory(allocationItem.getEmployeePost());
                        relation.setStaffRateHistory(allocationItem.getStaffRate());
                        if (allocationItem.isCombination() && allocationItem.getCombinationPost() != null)
                        {
                            relation.setHasCombinationPost(true);
                            relation.setCombinationPostHistory(allocationItem.getCombinationPost());
                        }
                        else
                            relation.setHasCombinationPost(false);
                    }
                    else
                    {
                        relation.setHasNewAllocItem(true);
                        relation.setHasCombinationPost(false);
                    }

                    relation.setCombinationPostStaffRateExtractRelation(item);

                    save(relation);
                }
        }
    }

    @Override
    public Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> getStaffRateAllocItemMap(Model model)
    {
        Map<CombinationPostStaffRateExtractRelation, List<CombinationPostStaffRateExtAllocItemRelation>> resultMap = new HashMap<>();
        for (CombinationPostStaffRateExtractRelation relation : model.getCombinationPostStaffRateItemList())
        {
            List<CombinationPostStaffRateExtAllocItemRelation> allocItemRelationList = resultMap.get(relation);
            if (allocItemRelationList == null)
                allocItemRelationList = new ArrayList<>();

            for (CombinationPostStaffRateExtAllocItemRelation allocItemRelation : getList(CombinationPostStaffRateExtAllocItemRelation.class, CombinationPostStaffRateExtAllocItemRelation.combinationPostStaffRateExtractRelation(), model.getCombinationPostStaffRateItemList()))
            {
                if (allocItemRelation.getCombinationPostStaffRateExtractRelation().equals(relation))
                    allocItemRelationList.add(allocItemRelation);

                resultMap.put(relation, allocItemRelationList);
            }
        }

        return resultMap;
    }

    @SuppressWarnings("unchecked")
    public void actualizeStaffRateItemList(Model model)
    {
        //достаем введенные знаечения для Ставок и обновляем их в списке
        final IValueMapHolder staffRateHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("staffRate");
        final Map<Long, Number> staffRateMap = (null == staffRateHolder ? Collections.emptyMap() : staffRateHolder.getValueMap());

        final IValueMapHolder finSrcHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSource");
        final Map<Long, FinancingSource> finSrcMap = (null == finSrcHolder ? Collections.emptyMap() : finSrcHolder.getValueMap());

        final IValueMapHolder finSrcItmHolder = (IValueMapHolder) model.getStaffRateDataSource().getColumn("financingSourceItem");
        final Map<Long, FinancingSourceItem> finSrcItmMap = (null == finSrcItmHolder ? Collections.emptyMap() : finSrcItmHolder.getValueMap());

        for (CombinationPostStaffRateExtractRelation item : model.getCombinationPostStaffRateItemList())
        {
            Number value = staffRateMap.get(item.getId());
            item.setStaffRate(value != null ? value.doubleValue() : 0.0d);
            item.setFinancingSource(finSrcMap.get(item.getId()));
            item.setFinancingSourceItem(finSrcItmMap.get(item.getId()));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void prepareStaffRateDataSource(Model model)
    {
        UniBaseUtils.createPage(model.getStaffRateDataSource(), model.getCombinationPostStaffRateItemList());
        model.setNeedUpdateDataSource(false);
    }

    @Override
    public void prepareEmployeeStaffRateDataSource(Model model)
    {
        List<EmployeePostStaffRateItem> staffRateItems = UniempDaoFacade.getUniempDAO().getEmployeePostStaffRateItemList(model.getEmployeePost());
        model.getEmployeeStaffRateDataSource().setCountRow(staffRateItems.size());
        UniBaseUtils.createPage(model.getEmployeeStaffRateDataSource(), staffRateItems);
    }

    @Override
    public void validateCombinationPeriods(Model model, InfoCollector infoCollector)
    {
        OrgUnit orgUnit = model.getExtract().getOrgUnit();
        PostBoundedWithQGandQL post = model.getExtract().getPostBoundedWithQGandQL();
        Date beginDate = model.getExtract().getBeginDate();
        Date endDate = model.getExtract().getEndDate();

        if (orgUnit == null || post == null || beginDate == null)
            return;

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(CombinationPost.class, "c")
                .column(property("c"))
                .where(eq(property(CombinationPost.orgUnit().fromAlias("c")), value(orgUnit)))
                .where(eq(property(CombinationPost.postBoundedWithQGandQL().fromAlias("c")), value(post)))
                .where(eq(property(CombinationPost.employeePost().fromAlias("c")), value(model.getEmployeePost())));

        if (model.getExtract().getCreateCombinationPost() != null)
            builder.where(ne(property("c"), value(model.getExtract().getCreateCombinationPost())));

        for (CombinationPost combinationPost: builder.createStatement(getSession()).<CombinationPost>list())
        {
            Date begin = combinationPost.getBeginDate();
            Date end = combinationPost.getEndDate();

            if (end != null)
            {
                if (endDate != null)
                {
                    if (CommonBaseDateUtil.isBetween(beginDate, begin, end) || CommonBaseDateUtil.isBetween(endDate, begin, end))
                        infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(end) + ") уже добавлена в список должностей по совмещению.");
                    else {
                        if (CommonBaseDateUtil.isBetween(begin, beginDate, endDate) || CommonBaseDateUtil.isBetween(end, beginDate, endDate))
                            infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(end) + ") уже добавлена в список должностей по совмещению.");
                    }
                }
                else
                {
                    if (CommonBaseDateUtil.isBefore(beginDate, end) || beginDate.equals(end))
                        infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(end) + ") уже добавлена в список должностей по совмещению.");
                }
            }
            else
            {
                if (endDate != null)
                {
                    if (CommonBaseDateUtil.isAfter(endDate, begin) || endDate.equals(begin))
                        infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + ") уже добавлена в список должностей по совмещению.");
                }
                else
                {
                    infoCollector.add("Должность " + combinationPost.getPostBoundedWithQGandQL().getFullTitle() + " (период работы с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(begin) + ") уже добавлена в список должностей по совмещению.");
                }
            }
        }
    }

    @Override
    protected GrammaCase getEmployeeTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    protected CombinationPostExtract createNewInstance()
    {
        return new CombinationPostExtract();
    }
}