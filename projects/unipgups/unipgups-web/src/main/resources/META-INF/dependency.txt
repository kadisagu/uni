org.tandemframework core 1.6.18-SNAPSHOT 
org.tandemframework db-support 1.6.18-SNAPSHOT 
org.tandemframework hib-support 1.6.18-SNAPSHOT 
org.tandemframework rtf 1.6.18-SNAPSHOT 
org.tandemframework caf 1.6.18-SNAPSHOT 
org.tandemframework __classreplace 1.6.18-SNAPSHOT 
org.tandemframework tap-support 1.6.18-SNAPSHOT 
org.tandemframework common 1.6.18-SNAPSHOT 
org.tandemframework sec 1.6.18-SNAPSHOT 
org.tandemframework.shared sandbox 1.11.2-SH-SNAPSHOT Sandbox
org.tandemframework.shared commonbase 1.11.2-SH-SNAPSHOT Общие прикладные функции
org.tandemframework.shared fias 1.11.2-SH-SNAPSHOT Реестр адресов
org.tandemframework.shared organization 1.11.2-SH-SNAPSHOT Оргструктура
org.tandemframework.shared person 1.11.2-SH-SNAPSHOT Персоны
org.tandemframework.shared cxfws 1.11.2-SH-SNAPSHOT Веб-сервисы CXF
org.tandemframework.shared image 1.11.2-SH-SNAPSHOT Работа с изображениями
org.tandemframework.shared archive 1.11.2-SH-SNAPSHOT Архив
org.tandemframework ldap 1.6.18-SNAPSHOT 
ru.tandemservice.uni.product unibase 2.11.2-UNI-SNAPSHOT Базовый модуль
ru.tandemservice.uni.product uniedu 2.11.2-UNI-SNAPSHOT Образовательные программы
org.tandemframework.shared employeebase 1.11.2-SH-SNAPSHOT Кадровый реестр
org.tandemframework.shared ctr 1.11.2-SH-SNAPSHOT Базовые договоры
org.tandemframework.shared survey 1.11.2-SH-SNAPSHOT Анкетирование
ru.tandemservice.uni.product uni 2.11.2-UNI-SNAPSHOT Студенты
ru.tandemservice.uni.product unimv 2.11.2-UNI-SNAPSHOT Визирование
ru.tandemservice.uni.product unimove 2.11.2-UNI-SNAPSHOT Базовое движение
ru.tandemservice.uni.product movestudent 2.11.2-UNI-SNAPSHOT Движение студентов
ru.tandemservice.uni.product unictr 2.11.2-UNI-SNAPSHOT Контрагенты (старый)
ru.tandemservice.uni.product uniec 2.11.2-UNI-SNAPSHOT Абитуриент (старый)
ru.tandemservice.uni.product unisc 2.11.2-UNI-SNAPSHOT Договоры студентов (старый)
ru.tandemservice.uni.product uniecc 2.11.2-UNI-SNAPSHOT Договоры абитуриентов (старый)
ru.tandemservice.uni.product uniec_fis 2.11.2-UNI-SNAPSHOT ФИС (старый)
ru.tandemservice.uni.product uniplaces 2.11.2-UNI-SNAPSHOT Здания и помещения (реестр)
ru.tandemservice.uni.product unischedule 2.11.2-UNI-SNAPSHOT Базовое расписание
ru.tandemservice.uni.product unienr14 2.11.2-UNI-SNAPSHOT Абитуриенты
ru.tandemservice.uni.product unienr14_fis 2.11.2-UNI-SNAPSHOT Интеграция с ФИС
ru.tandemservice.uni.product unieductr 2.11.2-UNI-SNAPSHOT Договоры на обучение
ru.tandemservice.uni.product unienr14_ctr 2.11.2-UNI-SNAPSHOT Договоры абитуриентов
ru.tandemservice.uni.product uniepp 2.11.2-UNI-SNAPSHOT Учебный процесс
ru.tandemservice.uni.product unisession 2.11.2-UNI-SNAPSHOT Сессия
ru.tandemservice.uni.product unidip 2.11.2-UNI-SNAPSHOT Дипломирование
ru.tandemservice.uni.product uniemp 2.11.2-UNI-SNAPSHOT Кадры
ru.tandemservice.uni.product moveemployee 2.11.2-UNI-SNAPSHOT Движение кадрового состава
ru.tandemservice.uni.product unisnpps 2.11.2-UNI-SNAPSHOT Внештатные преподаватели
ru.tandemservice.uni.product unipractice 2.11.2-UNI-SNAPSHOT Практики студентов
ru.tandemservice.uni.product uniepp_load 2.11.2-UNI-SNAPSHOT Нагрузка
ru.tandemservice.uni.product unispp 2.11.2-UNI-SNAPSHOT Расписание
ru.tandemservice.uni.product unisp 2.11.2-UNI-SNAPSHOT Выплаты студентам
ru.tandemservice.uni.product unisettle 2.11.2-UNI-SNAPSHOT Общежития и поселение
ru.tandemservice.uni.product unitraining 2.11.2-UNI-SNAPSHOT БРС и журналы
ru.tandemservice.uni.product unitjsakai 2.11.2-UNI-SNAPSHOT Журналы для Сакая
org.tandemframework.shared __replace_alfresco 1.11.2-SH-SNAPSHOT Replace some alfresco classes
org.tandemframework.shared dsign 1.11.2-SH-SNAPSHOT Цифровая подпись
org.tandemframework.shared bpms 1.11.2-SH-SNAPSHOT СЭД
ru.tandemservice.nsiclient nsiclient 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (базовые механизмы для обмена с НСИ)
ru.tandemservice.nsiclient nsifias 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Реестр адресов)
ru.tandemservice.nsiclient nsiperson 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Персоны)
ru.tandemservice.nsiclient nsiorganization 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Оргструктура)
ru.tandemservice.nsiclient nsiemployeebase 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Кадровый реестр)
ru.tandemservice.nsiclient nsibpms 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль СЭД)
ru.tandemservice.nsiclient nsiuni 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Базовые модули УНИ)
ru.tandemservice.nsiclient nsictr 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Базовые договоры)
ru.tandemservice.nsiclient nsiuniedu 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Образовательные программы)
ru.tandemservice.nsiclient nsiuniepp 2.11.2-UNI-SNAPSHOT Интеграция с НСИ (модуль Учебный процесс)
ru.tandemservice.uni.project unipgups 2.11.2-UNI-SNAPSHOT 
ru.tandemservice.uni.project unipgups-web 2.11.2-UNI-SNAPSHOT 
